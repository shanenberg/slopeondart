import 'dart:io';

import '../lib/lib/grammarparser/GrammarParser.dart';
import '../lib/lib/grammarparser/tree/GrammarObject.dart';
import '../lib/lib/parser/ResultObject.dart';

class SLOPE_Bib {

  static String bibPath = Directory.current.path + '\\bib\\' ;

  static loadStringFromFileNamed(String aFileName) {
    File f = new File(aFileName);
    return f.readAsStringSync();
  }

  static SLOPEGrammarObject loadGrammarNamed(String aGrammarFileName) {
    String grammarString = loadStringFromFileNamed(bibPath + aGrammarFileName + ".slogr");
    ResultObject<SLOPEGrammarObject> result = new ResultObject<SLOPEGrammarObject>();
    var g = new GrammarParser();
    g.parseString(grammarString, result);
    return result.getResult();
  }
}