import '../../../../lib/slot/runtime/SLOTHTMLProject.dart';
import '../../examples/SLOTProjectConstructor.dart';
import 'dart:html';
import 'SimpleGrammarToHTML.slp.dart';

main() {

  SLOTHTMLProject project =
    new SLOTProjectConstructor().constructHTMLProject(new SimpleGrammarToHTML());
  var slotResult = project.runProject();

}

