import '../../../lib/lib/grammarparser/GrammarParser.dart';
import '../../../lib/lib/grammarparser/tree/GrammarObject.dart';
import '../../../lib/lib/parser/ResultObject.dart';
import '../../../lib/slot/parser/SLOTParser.dart';
import '../../../lib/slot/runtime/SLOTProject.dart';
import '../../../lib/slot/structure/SLOTProgram.dart';
import 'dart:io';

import 'package:path/path.dart';

/*
  This class will somewhere in the near future .....
 */
main() {

  for(File aFile in allLibraryFiles()) {
    print(basename(aFile.path));
    toDartFile(aFile);
  }
}

toDartFile(File f) {


    String slpString = f.readAsStringSync();

    List<String> elements = slpString.split("###");
    String slopeString = elements[1].replaceFirst(" SLOPE", "");
    String inputString = elements[2].replaceFirst(" INPUT", "").replaceAll("\n", "");
    String slotString = elements[3].replaceFirst(" SLOT", "");


    SLOPEGrammarObject grammarObject = createGrammarObject(slopeString);
    SLOTProgram slotProgram = createProgram(slotString);

    File dartFile = new File(f.path.toString() + ".dart");
    if(dartFile.existsSync()) { dartFile.delete(); }

    var s = dartFile.openWrite();
  s.writeln(
      '''/***********************************************************
I am purely generated!!!! Don't touch me, touch the slp files 
***************************************************************/'''
  );
  String className = basename(f.path).replaceAll(".slp", "");
  if(basename(f.parent.parent.path) == "examples")
  s.writeln('''
      import '../HTMLExample.dart';''');
  else
    s.writeln('''
      import '../../../html/examples/HTMLExample.dart';''');

  s.writeln('''
class $className extends HTMLExample {
   List<String> getExampleProjectStrings() {
  List<String> exampleStrings = new List<String>();''');
  s.writeln("exampleStrings.add('''${slopeString.replaceAll("\\", "\\\\")}''');");
  s.writeln("exampleStrings.add('''$slotString''');");
  s.writeln("exampleStrings.add('''$inputString''');");
  s.writeln("  return exampleStrings;");
  s.writeln("}");
  s.writeln("}");


  s.close();

}

SLOTProgram createProgram(String slotString) {
  ResultObject<SLOTProgram> result2 = new ResultObject<SLOTProgram>();
  SLOTParser slotParser = new SLOTParser();
  slotParser.parseString(slotString, result2);
  return result2.getResult();
}

createGrammarObject(slopeString) {
  ResultObject<SLOPEGrammarObject> result1 = new ResultObject<SLOPEGrammarObject>();
  var slopeParser = new GrammarParser();
  print(slopeString);
  slopeParser.parseString(slopeString, result1);
  return result1.getResult();
}

List<String> allLibraryFilesAsStrings() {
  List<String> ret = new List<String>();
  for(File aFile in allLibraryFiles()) {
    ret.add(aFile.readAsStringSync());
  }
  return ret;
}

List<File> allLibraryFiles() {
    Directory currentDir =
    new Directory(
        Directory.current.path + "\\web\\html\\examples");

    List<FileSystemEntity> l = currentDir.listSync(recursive: true, followLinks: false);

    return
      l
          .map((FileSystemEntity e) {return new File(e.path);}).toList()
          .where((File file) {
        return file.existsSync() && file.path.endsWith(".slp");}).toList();
}