import 'package:slopeondart/lib/util/GrammarUtils.dart';
import 'package:slopeondart/lib/parser/tree/TreeNode.dart';

main() {
  TreeNode bla = parseStringFromGrammar("BUB", '''
Bla
  Rules: A
  
  A => "B" Z ("B"|Z).

  Z => "U".



  Scanner:

  space => SEPARATOR " " BOTTOM.
  newline => SEPARATOR "\\n" BOTTOM.
  newlineWindows => SEPARATOR "\\r" BOTTOM.


''');

  var slotBla = bla.toSLOTTreeNode();

  print("hi");
}