/***********************************************************
I am purely generated!!!! Don't touch me, touch the slp files 
***************************************************************/
      import '../HTMLExample.dart';
class exploration extends HTMLExample {
   List<String> getExampleProjectStrings() {
  List<String> exampleStrings = new List<String>();
exampleStrings.add('''
Bla
  Rules: A

  A => "B" Z ("B"|Z)+.

  Z => "U".


  Scanner:

  space => SEPARATOR " " BOTTOM.
  newline => SEPARATOR "\\\\n" BOTTOM.
  newlineWindows => SEPARATOR "\\\\r" BOTTOM.

''');
exampleStrings.add('''

load library util;
load library htmlConstruction;


var nodesToHTML;
var htmlString;
var printOffset;
var lastButton;



main {
    redraw();
}

redraw {
    nodesToHTML = [];
    contentNode = newRootNode();
    htmlNode = newRootNode();
    htmlString = "";

    ruleString = "";

    ->(activeDocumentConstruction)([rootGrammarNode], contentNode);
    ->(htmlTreeConstruction|doThisOnChildren1)([contentNode], htmlNode);
    ->(htmlStringConstruction|doThisOnChildren1_OneParam)([htmlNode], htmlNode, [nodesToHTML]);
/*
    printTree(rootGrammarNode);
    printTree(contentNode);
*/
    return htmlString;
}

handleEvent anEventTreeNode anID anHTMLObject {
    if (isNotNull(anID)) {
        var slotTreeNode = elementAt(nodesToHTML, anID);
        ->(doMouseHandleEvent)([slotTreeNode, anEventTreeNode], rootResultNode, [anHTMLObject]);
    } else {
        ->(doKeyboardHandleEvent)([anEventTreeNode], rootResultNode, [anHTMLObject]);
    };
}

/************************ */

TreeFunction doKeyboardHandleEvent

KEYBOARDEVENT.KEY.A anHTMLObject {
    printString("HALLO KEYBOARD");
}

ALL anHTMLObject {
    forEach(aChild in children(thisGrammarNode)) {
        thisTreeFunction([aChild], thisResultNode, [anHTMLObject]);
    };
}

/*************************** */

TreeFunction doMouseHandleEvent

BUTTON e:MOUSEEVENT anHTMLObject {
  var slopeNode = slotOriginNode(thisGrammarNode);
  var source = slotSourceNode(thisGrammarNode);

  ->(doButtonCommand)([source, slopeNode], thisResultNode, [anHTMLObject]);


}

ALL e:KEYBOARDEVENT anHTMLObject {
printString("HALLO KEYBOARD");
}


/****************************** */


TreeFunction doButtonCommand
SWITCH anOrNode:OR anHTMLObject {
  newTreeNode = switchToNextNode(anOrNode);
  redrawAll();
}

DELETE aNode:ALL anHTMLObject {
  deleteNode(aNode);
  redrawAll();
}

ADDALTERNATIVE anOrNode:ALL anHTMLObject {
/*
    find alternative strings

    showAlternatives();
*/
}


ADDNODE anOrNode:ALL anHTMLObject {
    printString(slotNodeName(anOrNode.#1));
}

SETALTERNATIVE anOrNode:ALL anHTMLObject {
    printString("isch bin SLOT");
    ruleString = "";
    ->(constructRuleString)([rootGrammarNode], rootResultNode);
    printString(ruleString);

    switchTo(anOrNode, slotNodeName(thisGrammarNode.#2));
    redrawAll();

}

/************************ */

TreeFunction htmlStringConstruction

NULL aNodeList {
/* (!)*/
    htmlString =
        concatAll(htmlString,[
            concatAll(
                "<DIV class=~"letter~" style=~"background:#f00;display:inline-block;~"",
                [html_generateIDString(thisGrammarNode, aNodeList),
                ">"]),
            "&nbsp", "</DIV>"
        ]);
}

Text aNodeList {

    htmlString =
        concatAll(htmlString,[
            concatAll(
                "<DIV class=~"letter~" style=~"color:blue;text-align:center;color:blue;display:inline-block;~"",
                [html_generateIDString(thisGrammarNode, aNodeList),
                ">"]),
            slotNodeName(thisGrammarNode.#1), "</DIV>"
        ]);
}

BUTTON aNodeList {
    lastButton = thisGrammarNode;
    htmlString =
        concatAll(htmlString, [
            html_createOpenTagFromNode(thisGrammarNode, aNodeList),
            slotNodeName(thisGrammarNode.#1.#1),
            html_createCloseTagFromNode(thisGrammarNode, aNodeList)
        ]);

}

HTML aNodeList {
    htmlString = concat(
                    htmlString,
                    html_createOpenTag("HTML"));
    continue([thisGrammarNode], thisResultNode, [aNodeList]);
    htmlString = concat(
                    htmlString,
                    html_createCloseTag("HTML"));
}

ROOT aNodeList {
    continue([thisGrammarNode], thisResultNode, [aNodeList]);
}


ALL aNodeList {

    htmlString = concat(
                    htmlString,
                    html_createOpenTagFromNode(thisGrammarNode, aNodeList));
    continue([thisGrammarNode], thisResultNode, [aNodeList]);
    htmlString = concat(
                    htmlString,
                    html_createCloseTagFromNode(thisGrammarNode, aNodeList));

}




/********************************* */

TreeFunction constructRuleString

OR {
    var numAlts = numAlternatives(thisGrammarNode);
    ruleString = concat(ruleString, "...");

    var counter = 1;

    while(smallerThan(counter, numAlts)) {
        ruleString = concat(ruleString, "| ...");
        counter = plus(numAlt, 1);
    };
}

OPTIONAL {
    ruleString = concat(ruleString, "(...)?");
}

BRACKET {
    printString(concat("reached ", slotNodeName(thisGrammarNode)));

    ruleString = concat(ruleString, "(");
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    ruleString = concat(ruleString, ")");
}

NFOLD {
    ruleString = concat(ruleString, "(...)+*");
}

AND {
    forEach(aChild in children(thisGrammarNode)) {
                thisTreeFunction([aChild], thisResultNode);
                ruleString = concat(ruleString, " ");
    };
}

NULL {
    ruleString = concat(ruleString, "NULL");
}

SLOPETOKEN {
    ruleString = concatAll(ruleString, ["~"",slotNodeName(thisGrammarNode.#1), "~""]);
}

ALL {
    ruleString = concat(ruleString, slotNodeName(thisGrammarNode));
}

/************************************ */
TreeFunction htmlTreeConstruction

ROOT {
    continue ([thisGrammarNode], thisResultNode);
}

Letter {
    +#"Text" {
        +#slotNodeName(thisGrammarNode.#1);
    };
}

NULL {
    +#"NULL";
}

ACTIONS.COMMANDS.ALL {
    +#"BUTTON" {
        +#"LABEL" {
            +#slotNodeName(thisGrammarNode.#1);
        };
    };
}

ACTIONS {
    +#"DIV style=~"text-align:center;color:blue;display:inline-block;~"" {
        +#"DIV" {
            thisTreeFunction([thisGrammarNode.#1], thisResultNode);
        };
        +#"DIV style=~"text-align:center;color:blue;~"" {
            thisTreeFunction([thisGrammarNode.#2], thisResultNode);
        };
    };
}

ALL {
    forEach(aChild in children(thisGrammarNode)) {
        thisTreeFunction([aChild], thisResultNode);
    };
}

/**************************************************** */


TreeFunction activeDocumentConstruction
OR {
/*
    if (largerThan(length(children(thisGrammarNode)),1)) {*/
        if (largerThan(length(children(thisGrammarNode.#1)),0)) {
                +#"ACTIONS"{
                    +#"COMMANDS"{
                        +#"SWITCH" {+#"->";};
                        +#"DELETE" {+#"X";};
                    };
                    +#"TARGET"{
                        thisTreeFunction([thisGrammarNode.#1], thisResultNode);
                    };
                };

        } else {
                +#"ACTIONS"{
                    var alt = numAlternatives(thisGrammarNode);
                    var counter = 0;
                    +#"COMMANDS"{
                        while(smallerThan(counter, alt)) {
                                +#"SETALTERNATIVE" {+#"INSERT"; +#counter;};
                            counter = plus(counter, 1);
                        };
                    };

                    +#"TARGET"{
                        thisTreeFunction([thisGrammarNode.#1], thisResultNode);
                    };
                };
        };
    /*};*/
}

/*
NFOLD.BRACKET {

    forEach(child in children(thisGrammarNode)) {
        +#"ACTIONS"{
            +#"COMMANDS"{
                +#"ADDNODE"{+#"+";};
                +#"REMOVENODE" {+#"-";};
            };
            +#"TARGET"{
                thisTreeFunction([child], thisResultNode);
            };
        };
    };
}*/
/*
NFOLD {
    if (equalsInt(length(children(thisGrammarNode)), 0)) {
        return thisGrammarNode;
    };

    var nextNodeIsBracket = nodeNameEquals(thisGrammarNode.#1, "BRACKET");
    if (nextNodeIsBracket) {
        forEach(child in children(thisGrammarNode)) {
            thisTreeFunction([child], thisResultNode);
        };
    } else {
        forEach(child in children(thisGrammarNode)) {
            +#"ACTIONS"{
                +#"COMMANDS"{
                    +#"ADDNODE" [+#"+";];
                    +#"REMOVENODE" [+#"-";];
                };
                +#"TARGET"{
                    thisTreeFunction([child], thisResultNode);
                };
            };
        };
    };
}*/

NFOLD {
    if (equalsInt(length(children(thisGrammarNode)), 0)) {
        return thisGrammarNode;
    };

    forEach(child in children(thisGrammarNode)) {
        +#"ACTIONS"{
            +#"COMMANDS"{
                +#"ADDNODE" [+#"+";];
                +#"REMOVENODE" [+#"-";];
            };
            +#"TARGET"{
                thisTreeFunction([child], thisResultNode);
            };
        };
    };

}

SLOPETOKEN {
    +#"Letter" {
        /*Fragliche Zeilen
        aName = slotNodeName(thisGrammarNode.#1);
        newNode = #aName;
        */
        +#slotNodeName(thisGrammarNode.#1);
    };
}


NULL {
    +#"NULL";
}


ALL {
    forEach(aChild in children(thisGrammarNode)) {
        thisTreeFunction([aChild], thisResultNode);
    };
}
''');
exampleStrings.add('''BUBU''');
  return exampleStrings;
}
}
