import '../../../../lib/slot/runtime/SLOTHTMLProject.dart';
import '../../examples/SLOTProjectConstructor.dart';
import 'dart:html';
import 'exploration.slp.dart';

main() {

  SLOTHTMLProject project =
    new SLOTProjectConstructor().constructHTMLProject(new exploration());
  var slotResult = project.runProject();

}

