import '../../../../lib/slot/runtime/SLOTHTMLProject.dart';
import '../../examples/SLOTProjectConstructor.dart';
import 'TreePrint.slp.dart';
import 'dart:html';

main() {

  SLOTHTMLProject project =
    new SLOTProjectConstructor().constructHTMLProject(new TreePrint());
  var slotResult = project.runProject();

}

