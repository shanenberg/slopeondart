import '../../../lib/lib/grammarparser/GrammarParser.dart';
import '../../../lib/lib/grammarparser/tree/GrammarObject.dart';
import '../../../lib/lib/parser/ResultObject.dart';
import '../../../lib/slot/parser/SLOTParser.dart';
import '../../../lib/slot/runtime/SLOTProject.dart';
import '../../../lib/slot/structure/SLOTProgram.dart';
import 'HTMLExample.dart';

class SLOTHTMLToConsoleConverter {

  SLOTProject constructProject(HTMLExample example) {
    List<String> exampleString = example.getExampleProjectStrings();
    String slopeString = exampleString[0];
    String slotString = exampleString[1];
    String input = exampleString[2];

    ResultObject<SLOPEGrammarObject> result1 = new ResultObject<SLOPEGrammarObject>();
    var slopeParser = new GrammarParser();
    slopeParser.parseString(slopeString, result1);
    SLOPEGrammarObject grammarObject = result1.getResult();

    ResultObject<SLOTProgram> result2 = new ResultObject<SLOTProgram>();
    SLOTParser slotParser = new SLOTParser();
    //print(slotString);
    slotParser.parseString(slotString, result2);
    SLOTProgram slotProgram = result2.getResult();

    SLOTProject project = new SLOTProject(slotProgram, grammarObject, input);

    return project;

  }


}