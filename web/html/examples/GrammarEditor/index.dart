import '../../../../lib/slot/runtime/SLOTHTMLProject.dart';
import '../../examples/SLOTProjectConstructor.dart';
import 'dart:html';
import 'GrammarEditor.slp.dart';

main() {

  SLOTHTMLProject project =
    new SLOTProjectConstructor().constructHTMLProject(new GrammarEditor());
  var slotResult = project.runProject();

}

