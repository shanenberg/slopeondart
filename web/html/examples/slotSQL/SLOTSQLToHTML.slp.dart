/***********************************************************
I am purely generated!!!! Don't touch me, touch the slp files 
***************************************************************/
      import '../HTMLExample.dart';
class SLOTSQLToHTML extends HTMLExample {
   List<String> getExampleProjectStrings() {
  List<String> exampleStrings = new List<String>();
exampleStrings.add('''
SQL

Rules: query

query => bracketableQueryspecification ";"?.

bracketableQueryspecification =>  "\\(" bracketableQueryspecification "\\)" | queryspecification.

queryspecification => "SELECT" ("DISTINCT" | "ALL")? selectlist tableexpression
(("UNION"|"EXCEPT"|"INTERSECT") "ALL"? "SELECT" ("DISTINCT" | "ALL")? selectlist tableexpression )*.

selectlist => 		"\\*" |	term ("AS"? identifier)? ( "," term ("AS"? identifier)?)*.



term => booleanterm ("OR" booleanterm)*.

booleanterm => booleanfactor ("AND" booleanfactor)*.

booleanfactor => ( "NOT" )? booleantest.

booleantest => booleanprimary ("IS" ("NOT")? truthvalue)?.
truthvalue => "TRUE" | "FALSE" | "UNKNOWN".

booleanprimary =>
exists |
termexpr (comparison
         | between
         | in
         | like
         | nullcheck
         | match
         | overlaps
         | quantifiedcomparison
         | quantifiedlike)?.


comparison => compop termexpr.
compop => "=" | notequalsoperator | "<" | ">" | "<=" | ">=".
notequalsoperator => "<>" | "!=".

quantifiedcomparison => compop quantifier subquery.

between => "NOT"? "BETWEEN" termexpr "AND" termexpr.

in => "NOT"? "IN" ("\\(" queryspecification "\\)" | invaluelist).
invaluelist => "\\(" termexpr ("," termexpr)* "\\)".

like => "NOT"? "LIKE" termexpr.
nullcheck => "IS" "NOT"? "NULL".
match => "MATCH" "UNIQUE"? ("PARTIAL" | "FULL")? subquery.
overlaps => "OVERLAPS" termexpr.

quantifiedlike => "NOT"? "LIKE" quantifier subquery.

quantifier => some | all.
some => "SOME" | "ANY".
all => "ALL".

exists => "EXISTS" subquery.

termexpr => valueexpr (("\\+" | "\\*" | "/" | "\\-" | "\\|\\|")  valueexpr)*.

column => qualifier ("\\."  "\\*")? | "\\?". /* verkettete Zugriffe (mehrere Datenbanken? ) (?) */
number => UnsignedNumber.

valueexpr => ("\\+"|"\\-")? (functionspec | dateliteral | timeliteral | timestampliteral | intervalliteral | identifier stringLiteralQuote | stringLiteralQuote | "NULL" | column
                           | number | casespecification | castspecification | "\\(" queryspecification "\\)" | "\\(" term "\\)" | rowvalueconstructor).

rowvalueconstructor => "\\(" termexpr ("," termexpr)* "\\)".

qualifier => identifier ("\\." identifier)*.

castspecification => "CAST" termexpr "AS" qualifier
                   | "CAST" "\\(" termexpr "AS" qualifier "\\)".


dateliteral => "DATE" stringLiteralQuote.
timeliteral => "TIME" stringLiteralQuote.
timestampliteral => "TIMESTAMP" stringLiteralQuote.
intervalliteral => "INTERVAL" sign? stringLiteralQuote intervalqualifier.
sign => "\\+" | "\\-".


intervalqualifier => startfield "TO" endfield | singledatetimefield.
startfield => non_seconddatetimefield ("\\(" number "\\)")?.
non_seconddatetimefield => "YEAR" | "MONTH" | "DAY" | "HOUR" | "MINUTE".
endfield => non_seconddatetimefield | "SECOND" ("\\(" number "\\)")?.
singledatetimefield => non_seconddatetimefield ("\\(" number "\\)")? | "SECOND" ("\\(" unsignedinteger ("," "\\(" unsignedinteger)? "\\)")?.




functionspec => "COUNT" "\\(" ("\\*" | ("DISTINCT"|"ALL")? termexpr) "\\)" |
("AVG"|"MAX"|"MIN"|"SUM") "\\(" ("DISTINCT"|"ALL")? termexpr "\\)" |
identifier "\\(" (term ("," term)*) "\\)". /* (?)*/

tableexpression => "FROM"		fromclause		("WHERE" term )?		( "GROUP" "BY" groupbyclause )?		( "HAVING" term )?       ("ORDER" "BY" orderbyclause)?.
fromclause => tablereference  ( "," tablereference )*.

/* Direktverschachtelte AS nicht möglich?.... */

tablereference => tablereference0 ("," tablereference0)*.

tablereference0 =>

                tablereference1
                (
                    ("CROSS" | "NATURAL" (("LEFT" | "RIGHT" | "FULL") "OUTER"? | "INNER")?) "JOIN"
                    tablereference1
                    |

                    (("LEFT" | "RIGHT" | "FULL") "OUTER"? | "INNER")? "JOIN"
                    tablereference1
                    ("ON" term
                    | "USING" "\\(" identifier ("," identifier)* "\\)")
                )*.


tablereference1 => (qualifier | subquery)
                   ("AS"? identifier ("\\(" identifier ("," identifier)* "\\)")? )?.


subquery => "\\(" (queryspecification | tablereference) "\\)".


casespecification => "CASE" term? ("WHEN" term "THEN" term)+ ("ELSE" term)? "END".

groupbyclause => column ("COLLATE" identifier ("\\." identifier)? )? ("," column ("COLLATE" identifier ("\\." identifier)?)?)*.


orderbyclause => column ("ASC"|"DESC")? ("," column ("ASC"|"DESC")?)*.



Scanner:

/*
  dateStringLiteralQuote => "'(0|[1-9][0-9]*)\\-(0|[1-9][0-9]*)\\-(0|[1-9][0-9]*)'".

  timeStringLiteralQuote => "'(0|[1-9][0-9]*):(0|[1-9][0-9]*)(\\.(0|[1-9][0-9]*)?)?((\\-|\\+)(0|[1-9][0-9]*):(0|[1-9][0-9]*))?'".
*/
/* (?)
  dateStringLiteralQuote => "'(0|[1-9][0-9]*)\\-(0|[1-9][0-9]*)\\-(0|[1-9][0-9]*)'".
  timeStringLiteralQuote => "'(0|[1-9][0-9]*):(0|[1-9][0-9]*)(\\.(0|[1-9][0-9]*)?)?:".
*/
  stringLiteralQuote => "'(^')*'" .

  identifier => "([a-z]|[A-Z]|_|ä|ö|ü|Ä|Ö|Ü|ß)([a-z]|[A-Z]|[0-9]|_|ä|ö|ü|Ä|Ö|Ü|ß)*" BOTTOM.

  UnsignedNumber => "(0|[1-9][0-9]*)(\\.[0-9]+)?((e|E)(\\+|\\-)?[0-9]+)?".

  multilineComment => SEPARATOR "/\\*(^\\*|\\*^/)*\\*/" BOTTOM.
  comment => SEPARATOR "\\-\\-(^\\n)*" TOP.

  separator => SEPARATOR " " BOTTOM.
  newline => SEPARATOR "\\n" BOTTOM.
  newLineWindows => SEPARATOR "\\r" BOTTOM.


''');
exampleStrings.add('''

load library util;
load library htmlConstruction;


var nodesToHTML;
var htmlString;
var printOffset;
var lastButton;

main {
    redraw();
}

redraw {
    nodesToHTML = [];
    contentNode = newRootNode();
    htmlNode = newRootNode();
    htmlString = "";

    ->(activeDocumentConstruction)([rootGrammarNode], contentNode);
    ->(htmlTreeConstruction|doThisOnChildren1)([contentNode], htmlNode);
    ->(htmlStringConstruction|doThisOnChildren1_OneParam)([htmlNode], htmlNode, [nodesToHTML]);
/*
    printTree(rootGrammarNode);
    printTree(contentNode);
*/
    return htmlString;
}

handleEvent anEventTreeNode anID anHTMLObject {
    if (isNotNull(anID)) {
        var slotTreeNode = elementAt(nodesToHTML, anID);
        ->(doMouseHandleEvent)([slotTreeNode, anEventTreeNode], rootResultNode, [anHTMLObject]);
    } else {
        ->(doKeyboardHandleEvent)([anEventTreeNode], rootResultNode, [anHTMLObject]);
    };
}

TreeFunction doKeyboardHandleEvent
KEYBOARDEVENT.KEY.A anHTMLObject {
    printString("HALLO KEYBOARD");
}

ALL anHTMLObject {
    forEach(aChild in children(thisGrammarNode)) {
        thisTreeFunction([aChild], thisResultNode, [anHTMLObject]);
    };
}

TreeFunction doMouseHandleEvent

BUTTON e:MOUSEEVENT anHTMLObject {
  var slopeNode = slotOriginNode(thisGrammarNode);
  var source = slotSourceNode(thisGrammarNode);

  ->(doButtonCommand)([source, slopeNode], thisResultNode, [anHTMLObject]);

}

ALL e:KEYBOARDEVENT anHTMLObject {
printString("HALLO KEYBOARD");
}


TreeFunction doButtonCommand
SWITCH anOrNode:OR anHTMLObject {
  newTreeNode = switchToNextNode(anOrNode);
  redrawAll();
}

DELETE aNode:ALL anHTMLObject {
  deleteNode(aNode);
  redrawAll();
}

ADDALTERNATIVE anOrNode:ALL anHTMLObject {
/*
    find alternative strings

    showAlternatives();
*/
}


ADDNODE anOrNode:ALL anHTMLObject {
    printString(slotNodeName(anOrNode.#1));
}

INSERT anOrNode:ALL anHTMLObject {

}



TreeFunction htmlStringConstruction

NULL aNodeList {

    htmlString =
        concatAll(htmlString,[
            concatAll(
                "<DIV class=~"letter~" style=~"background:#f00;display:inline-block;~"",
                [html_generateIDString(thisGrammarNode, aNodeList),
                ">"]),
            "&nbsp", "</DIV>"
        ]);
}

Text aNodeList {

    htmlString =
        concatAll(htmlString,[
            concatAll(
                "<DIV class=~"letter~" style=~"color:blue;text-align:center;color:blue;display:inline-block;~"",
                [html_generateIDString(thisGrammarNode, aNodeList),
                ">"]),
            slotNodeName(thisGrammarNode.#1), "</DIV>"
        ]);
}

BUTTON aNodeList {
    lastButton = thisGrammarNode;
    htmlString =
        concatAll(htmlString, [
            html_createOpenTagFromNode(thisGrammarNode, aNodeList),
            slotNodeName(thisGrammarNode.#1.#1),
            html_createCloseTagFromNode(thisGrammarNode, aNodeList)
        ]);

}

HTML aNodeList {
    htmlString = concat(
                    htmlString,
                    html_createOpenTag("HTML"));
    continue([thisGrammarNode], thisResultNode, [aNodeList]);
    htmlString = concat(
                    htmlString,
                    html_createCloseTag("HTML"));
}

ROOT aNodeList {
    continue([thisGrammarNode], thisResultNode, [aNodeList]);
}


ALL aNodeList {

    htmlString = concat(
                    htmlString,
                    html_createOpenTagFromNode(thisGrammarNode, aNodeList));
    continue([thisGrammarNode], thisResultNode, [aNodeList]);
    htmlString = concat(
                    htmlString,
                    html_createCloseTagFromNode(thisGrammarNode, aNodeList));

}

TreeFunction htmlTreeConstruction

ROOT {
    continue ([thisGrammarNode], thisResultNode);
}

Letter {
    +#"Text" {
        +#slotNodeName(thisGrammarNode.#1);
    };
}

NULL {
    +#"NULL";
}

ACTIONS.COMMANDS.ALL {
    +#"BUTTON" {
        +#"LABEL" {
            +#slotNodeName(thisGrammarNode.#1);
        };
    };
}

ACTIONS {
    +#"DIV style=~"text-align:center;color:blue;display:inline-block;~"" {
        +#"DIV" {
            thisTreeFunction([thisGrammarNode.#1], thisResultNode);
        };
        +#"DIV style=~"text-align:center;color:blue;~"" {
            thisTreeFunction([thisGrammarNode.#2], thisResultNode);
        };
    };
}

ALL {
    forEach(aChild in children(thisGrammarNode)) {
        thisTreeFunction([aChild], thisResultNode);
    };
}

/**************************************************** */
TreeFunction activeDocumentConstruction
OR {

    if (largerThan(length(children(thisGrammarNode)),1)) {
        if (largerThan(length(children(thisGrammarNode.#1)),0)) {
                +#"ACTIONS"{
                    +#"COMMANDS"{
                        +#"SWITCH" {+#"->";};
                        +#"DELETE" {+#"X";};
                    };
                    +#"TARGET"{
                        thisTreeFunction([thisGrammarNode.#1], thisResultNode);
                    };
                };

        } else {
                +#"ACTIONS"{
                    var alt = numAlternatives(thisGrammarNode);
                    var counter = 0;
                    +#"COMMANDS"{
                    while(smallerThan(counter, alt)) {
                            +#"SETALTERNATIVE" {+#"INSERT";};
                        counter = plus(counter, 1);
                    };
                    };

                    +#"TARGET"{
                        thisTreeFunction([thisGrammarNode.#1], thisResultNode);
                    };
                };
        };
    };
}

NFOLD.BRACKET {

    forEach(child in children(thisGrammarNode)) {
        +#"ACTIONS"{
            +#"COMMANDS"{
                +#"ADDNODE"{+#"+";};
                +#"REMOVENODE" {+#"-";};
            };
            +#"TARGET"{
                thisTreeFunction([child], thisResultNode);
            };
        };
    };
}

NFOLD {
    if (equalsInt(length(children(thisGrammarNode)), 0)) {
        return thisGrammarNode;
    };
    var nextNodeIsBracket = nodeNameEquals(thisGrammarNode.#1, "BRACKET");
    if (nextNodeIsBracket) {
        forEach(child in children(thisGrammarNode)) {
            thisTreeFunction([child], thisResultNode);
        };
    } else {
        forEach(child in children(thisGrammarNode)) {
            +#"ACTIONS"{
                +#"COMMANDS"{
                    +#"ADDNODE" [+#"+";];
                    +#"REMOVENODE" [+#"-";];
                };
                +#"TARGET"{
                    thisTreeFunction([child], thisResultNode);
                };
            };
        };
    };
}

SLOPETOKEN {
    +#"Letter" {
        aName = slotNodeName(thisGrammarNode.#1);
        newNode = #aName;
        +#slotNodeName(thisGrammarNode.#1);
    };
}


NULL {
    +#"NULL";
}


ALL {
    forEach(aChild in children(thisGrammarNode)) {
        thisTreeFunction([aChild], thisResultNode);
    };
}
''');
exampleStrings.add('''SELECT stefan FROM hanenberg''');
  return exampleStrings;
}
}
