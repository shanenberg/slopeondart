import '../../../../lib/slot/runtime/SLOTHTMLProject.dart';
import '../../examples/SLOTProjectConstructor.dart';
import 'dart:html';
import 'SLOTSQLToHTML.slp.dart';

main() {

  SLOTHTMLProject project =
    new SLOTProjectConstructor().constructHTMLProject(new SLOTSQLToHTML());
  var slotResult = project.runProject();

}

