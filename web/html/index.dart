import '../../lib/slot/runtime/SLOTHTMLProject.dart';
import '../../lib/slot/runtime/SLOTRuntimeResult.dart';
import '../../lib/slot/runtime/objects/SLOTInteger.dart';
import '../../lib/slot/runtime/objects/SLOTTreeNode.dart';
import 'dart:html';
import 'examples/Example1EasyGrammar/SimpleGrammarToHTML.slp.dart';
import 'examples/SLOTProjectConstructor.dart';

main() {

  SLOTHTMLProject project =
    new SLOTProjectConstructor().constructHTMLProject(new SimpleGrammarToHTML());
  var slotResult = project.runProject();




}

