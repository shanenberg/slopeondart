import 'dart:io';
import 'dart:math';


List<String> variables = ["A", "B", "C", "D", "E", "F"];
List<int> nums =         [10,  50,  11,  40,   41,  66];


void main() {
  File file = new File("web\\SQLExperiment\\booleanExpressions\\booleanPlayground1.txt");

  String content = file.readAsStringSync();





  variables.shuffle();


  RegExp booleanExpr = new RegExp(r'TRUE|FALSE|UNKNOWN');


  var hi = anyItem(["A", "B", "C"]);

  print(hi);


  List<Function> funcs = [];

  String replacedContent =
  content.replaceAllMapped(booleanExpr, (Match m) {
    return "${anyItem(["A", "B", "C", "D", "E", "F", 1, 50, 70, 90, 10])} ${anyItem(["=","!=", ">", "<"])} ${anyItem(["A", "B", "C", "D", "E", "F", 1, 50, 70, 90, 10])}";

  });

  print(replacedContent);
}







Var_UnEq_Num(List<String> listVar, List<int> listNum) {
  String s1 = anyItem(listVar);
  int i2 = anyItem(listNum);

  return "$s1 != $i2";
}



replaceMatch(Match m, String replacement) {
  return m.input.replaceRange(m.start, m.end, replacement);
}

matchedSequence(Match m) {
  return m.input.substring(m.start, m.end);
}




anyItem(List list) {
  int randomInt = new Random().nextInt(list.length);


  return list[randomInt];
}

anyItemExcept(List list, {int exceptIndex = -1, var exceptItem = "§"}) {
  int randomInt;
  do {
    randomInt = new Random().nextInt(list.length);
  } while(randomInt != exceptIndex && list[randomInt] != exceptItem);


  return list[randomInt];
}