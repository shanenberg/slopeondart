import 'dart:html';

main() {
  print("hi");

  querySelector("#startTextual").onClick.listen((MouseEvent ev) {
    querySelector("#taskText").style
        .setProperty("display", "block");

    querySelector("#tutorial").style
        .setProperty("display", "none");

  });

  querySelector("#startVisual").onClick.listen((MouseEvent ev) {
    querySelector("#taskVisual").style
        .setProperty("display", "block");

    querySelector("#tutorial").style
        .setProperty("display", "none");

  });

  querySelector("#visualDone").onClick.listen((MouseEvent ev) {
    querySelector("#taskVisual").style
        .setProperty("display", "none");

    querySelector("#tutorial").style
        .setProperty("display", "block");


  });

  querySelector("#textDone").onClick.listen((MouseEvent ev) {
    querySelector("#taskText").style
        .setProperty("display", "none");

    querySelector("#tutorial").style
        .setProperty("display", "block");


  });
}