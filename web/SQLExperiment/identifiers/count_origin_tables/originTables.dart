import 'dart:html';

HTMLSwapper swapper;
main() {
  swapper = new HTMLSwapper("tutorial");
  print("hi");

  querySelector("#startTextual").onClick.listen((MouseEvent ev) {
    swapper.swapWith("taskText");
  });

  querySelector("#startVisual").onClick.listen((MouseEvent ev) {
    swapper.swapWith("taskVisual");
  });

  querySelector("#visualDone").onClick.listen((MouseEvent ev) {
    swapper.swapWith("tutorial");
  });

  querySelector("#textDone").onClick.listen((MouseEvent ev) {
    swapper.swapWith("tutorial");
  });
}

class HTMLSwapper {
  String currentID;

  HTMLSwapper(String currentID) {
    querySelector("#$currentID").style.setProperty("display", "block");
  }

  swapWith(String newID) {
    querySelector("#$currentID").style.setProperty("display", "none");
    querySelector("#$newID").style.setProperty("display", "block");
    currentID = newID;

  }
}