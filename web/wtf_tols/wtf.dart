import '../../lib/slot/runtime/objects/SLOTString.dart';
import '../../lib/slot/runtime/objects/SLOTTreeNode.dart';
import 'TOLSFunctionFamily.dart';
import 'package:slopeondart/lib/parser/features/and/AND_TreeNode.dart';
import 'package:slopeondart/slot/runtime/objects/SLOTSLOPETreeNode.dart';

main() {
  PathExp path = n("Hi").n("LOL").ALL;

  SLOTTreeNode tn = new SLOTTreeNode()
    ..value = new SLOTString("Hi")
    ..addChild(new SLOTTreeNode()
      ..value = new SLOTString("lol")
      ..addChild(new SLOTTreeNode()..value = new SLOTString("leeel")));

  TreeNodeFunctionFamily fam = new TreeNodeFunctionFamily({
    n("Hi"): (TOLSTreeNodeFrame thiz) {
      print("BOO");
    }
  });






}


AllPathExp ALL = new AllPathExp(null);


NamePathExp n(String name) {
  return new NamePathExp(null, name);
}

TypePathExp t(Type type) {
  return new TypePathExp(null, type);
}

FunctionPathExp f(Function(SLOTTreeNode) check) {
  return new FunctionPathExp(null, check);
}

NamedAndExp namedAnd() {
  return new NamedAndExp(null);
}


abstract class PathExp {
  PathExp parent;

  PathExp(this.parent);

  NamePathExp n(String name) {
    return new NamePathExp(this, name);
  }

  NamePathExp get AND => new NamePathExp(this, "AND");

  TypePathExp t(Type type) {
    return new TypePathExp(this, type);
  }

  FunctionPathExp f(Function(SLOTTreeNode) check) {
    return new FunctionPathExp(this, check);
  }

  AllPathExp get ALL => new AllPathExp(this);

  NamedAndExp get namedAnd => new NamedAndExp(this);

  bool SUCCESS(SLOTTreeNode tn) {
    return this.parent == null ? true
           : tn.parent == null ? false
           : parent.apply(tn.parent);
  }

  bool apply(SLOTTreeNode tn);
}

class FunctionPathExp extends PathExp {
  Function(SLOTTreeNode) check;

  FunctionPathExp(PathExp parent, this.check) : super(parent);

  @override
  bool apply(SLOTTreeNode tn) {
    if (check(tn))
      return SUCCESS(tn);
    else
      return false;
  }
}

class TypePathExp extends PathExp {
  Type type;

  TypePathExp(PathExp parent, this.type) : super(parent);

  @override
  bool apply(SLOTTreeNode tn) {
    // TODO: implement apply
    if (tn.runtimeType == type) {} else
      return false;
  }
}

class NamePathExp extends PathExp {
  String name;

  NamePathExp(PathExp parent, this.name) : super(parent);

  @override
  bool apply(SLOTTreeNode tn) {
    bool ret = tn.value is SLOTString && name == (tn.value as SLOTString).value;
    if (ret)
      return SUCCESS(tn);
    else
      return false;
  }
}

class NamedAndExp extends PathExp {
  NamedAndExp(PathExp parent) : super(parent);

  bool apply(SLOTTreeNode tn) {
    if(tn is SLOTSLOPETreeNode
    && (tn as SLOTSLOPETreeNode).slopeTreeNode is AND_TreeNode
    && tn.value is SLOTString
    && tn.valueToString() != "AND") {
      return SUCCESS(tn);
    } else {
      return false;
    }
  }
}

class AllPathExp extends PathExp {
  AllPathExp(PathExp parent) : super(parent);


  @override
  bool apply(SLOTTreeNode tn) {
    return SUCCESS(tn);
  }
}
