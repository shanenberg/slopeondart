import '../../lib/slot/runtime/objects/SLOTTreeNode.dart';
import '../../lib/lib/parser/ResultObject.dart';
import '../../lib/slot/runtime/objects/SLOTString.dart';
import '../../lib/slot/runtime/objects/SLOTSLOPETreeNode.dart';
import 'wtf.dart';

class FunctionFamily {
  Map<PathExp, Function(TOLSFrame)> tf;

  FunctionFamily(this.tf);


  run(TOLSFrame frame) {
    var done = false;
    tf.forEach(
            (PathExp pathExp, Function(TOLSFrame) singleTreeFunction) {
          if(done) return;
          if(pathExp.apply(frame.grammarNode)) {
            singleTreeFunction(frame);

            done = true;
          }
        });
    if(!done) {
      throw "not found";
    }
  }
}

class TreeNodeFunctionFamily extends FunctionFamily {

  TreeNodeFunctionFamily(Map<PathExp, Function> tf) : super(tf);

  runSingle(SLOTTreeNode grammarNode, SLOTTreeNode resultNode) {
    run(new TOLSTreeNodeFrame(grammarNode, resultNode, this));
  }
}

typedef ResultFunction(TOLSResultFrame);

class ResultFunctionFamily extends FunctionFamily {

  ResultFunctionFamily(Map<PathExp, ResultFunction> tf) : super(tf);

  runSingle(SLOTTreeNode grammarNode, ResultObject resultObject) {
    run(new TOLSResultFrame(grammarNode, resultObject, this));
  }
}






abstract class TOLSFrame {
  FunctionFamily _currentFunctionFamily;
  List<Object> additionalParams;
  SLOTTreeNode grammarNode;


  TOLSFrame(this.grammarNode, this._currentFunctionFamily);
}



class TOLSTreeNodeFrame extends TOLSFrame{
  SLOTTreeNode resultNode;

  SLOTSLOPETreeNode slopeResultNode() => this.resultNode as SLOTSLOPETreeNode;
  SLOTSLOPETreeNode slopeGrammarNode() => this.grammarNode;

  TOLSTreeNodeFrame(SLOTTreeNode grammarNode, this.resultNode, TreeNodeFunctionFamily family)
      : super(grammarNode, family);

  a(SLOTTreeNode node) {
    resultNode.addChild(node);
  }

  treeFunction(SLOTTreeNode grammarNode, SLOTTreeNode resultNode) {
    _currentFunctionFamily.run(new TOLSTreeNodeFrame(grammarNode, resultNode, _currentFunctionFamily));
  }

  treeFunctionParams(SLOTTreeNode grammarNode, SLOTTreeNode resultNode, List<Object> params) {
    _currentFunctionFamily.run(new TOLSTreeNodeFrame(grammarNode, resultNode, _currentFunctionFamily)..additionalParams=params);
  }

  advance(SLOTTreeNode grammarNode, SLOTTreeNode resultNode) {

  }
  advanceParams(SLOTTreeNode grammarNode, SLOTTreeNode resultNode, List<Object> params) {

  }
}


class TOLSResultFrame<T> extends TOLSFrame {
  ResultObject<T> resultObject;
  TOLSResultFrame(SLOTTreeNode grammarNode, this.resultObject, ResultFunctionFamily currentFunctionFamily)
      : super(grammarNode,currentFunctionFamily);


  setResult(T newResult) {
    resultObject.setResult(newResult);
  }

  T getResult() {
    return resultObject.getResult();
  }


  treeFunction(SLOTTreeNode grammarNode, ResultObject resultObject) {
    _currentFunctionFamily.run(new TOLSResultFrame(grammarNode, resultObject, _currentFunctionFamily));
  }

  advance(SLOTTreeNode grammarNode, SLOTTreeNode resultNode) {

  }


}

class TFunctionChain {
  List<TreeNodeFunctionFamily> chain;
  int currentIndex = 0;

  TFunctionChain(this.chain);

  run(SLOTTreeNode grammarNode, SLOTTreeNode resultNode, [List params]) {
    var frame = new ChainedTOLSFrame(grammarNode, resultNode, chain[currentIndex], currentIndex, this);
    if(params != null)
      frame.additionalParams = params;
    runOnFrame(frame);
    return frame;
  }



  runOnFrame(ChainedTOLSFrame frame) {
    chain[currentIndex].run(frame);
  }

  reset() {
    currentIndex = 0;
  }

  next() {
    currentIndex++;
  }


  advance(SLOTTreeNode grammarNode, SLOTTreeNode resultNode, [List params]) {
    currentIndex++;
    run(grammarNode, resultNode);
  }
}

class ChainedTOLSFrame extends TOLSTreeNodeFrame {
  int currentChainIndex;
  TFunctionChain chain;

  ChainedTOLSFrame(SLOTTreeNode grammarNode, SLOTTreeNode resultNode, TreeNodeFunctionFamily currentFunctionFamily, this.currentChainIndex, this.chain)
                 : super(grammarNode, resultNode, currentFunctionFamily);

  treeFunction(SLOTTreeNode grammarNode, SLOTTreeNode resultNode) {
    chain.reset();
    chain.run(grammarNode, resultNode);
  }
  treeFunctionParams(SLOTTreeNode grammarNode, SLOTTreeNode resultNode, List<Object> params) {
    chain.reset();
    chain.runOnFrame(new ChainedTOLSFrame(grammarNode, resultNode, chain.chain[chain.currentIndex], chain.currentIndex, chain)
      ..additionalParams = params);
  }

  advance(SLOTTreeNode grammarNode, SLOTTreeNode resultNode) {
    chain.next();
    chain.run(grammarNode, resultNode);
  }
  advanceParams(SLOTTreeNode grammarNode, SLOTTreeNode resultNode, List<Object> params) {
    chain.next();
    chain.runOnFrame(new ChainedTOLSFrame(grammarNode, resultNode, chain.chain[chain.currentIndex], chain.currentIndex, chain)
                    ..additionalParams = params);
  }


}

