import 'dart:html' as html;

import 'package:slopeondart/lib/util/GrammarUtils.dart';
import 'package:slopeondart/lib/grammarparser/tree/GrammarObject.dart';

html.Element visualReggaeDisplay;
html.ElementList<html.SpanElement> placeholderSelectors;
html.ButtonElement lefto;
html.ButtonElement righto;
html.ButtonElement moveButton;
html.InputElement moveTo;

int currentIndex;

List<html.Element> realSelectors = [];

void main()
{

  visualReggaeDisplay = html.querySelector(".visualReggaeDisplay");

  placeholderSelectors = html.querySelectorAll(".selector");

  currentIndex = 0;
  lefto = html.querySelector("#lefto");
  righto = html.querySelector("#righto");
  moveButton = html.querySelector("#moveButton");
  moveTo = html.querySelector("#moveTo");

  realSelectors.add(new html.SpanElement()..classes = ["selector", "real", "steady"]..innerHtml = "&hairsp;");

  visualReggaeDisplay.append(realSelectors[0]);

  print("${placeholderSelectors[0].offsetTo(visualReggaeDisplay).y}px");
  print("${placeholderSelectors[0].offsetTo(visualReggaeDisplay).x}px");

  realSelectors[0].style.top = "${placeholderSelectors[0].offsetTo(visualReggaeDisplay).y}px";
  realSelectors[0].style.left = "${placeholderSelectors[0].offsetTo(visualReggaeDisplay).x}px";


  righto.onClick.listen((html.MouseEvent ev) {
    currentIndex = (currentIndex + 1) % placeholderSelectors.length;
    moveSelectorsToPlaceholders([placeholderSelectors[currentIndex]]);
  });

  lefto.onClick.listen((html.MouseEvent ev) {
    currentIndex = currentIndex == 0 ?
                    placeholderSelectors.length-1
                    : currentIndex - 1;
    moveSelectorsToPlaceholders([placeholderSelectors[currentIndex]]);

  });

  moveButton.onClick.listen((html.MouseEvent ev) {
    List<int> indeces = moveTo.value.split(" ").map((String val) => int.parse(val));

    moveSelectorsToPlaceholders
      (placeholderSelectors
        .where((html.SpanElement el) => indeces.contains(
                                          placeholderSelectors.indexOf(el)
                                        )
              )
      );

  });

}


moveSelectorsToPlaceholders(List<html.SpanElement> placeholderSelectors) {
  bool first = true;
  List<html.SpanElement> nextPlaceholders;
  for(html.SpanElement realSelector in realSelectors) {
    if(first) {
      nextPlaceholders = moveSelectorToPlaceholders(realSelector, placeholderSelectors);
      first = false;
    }
    realSelector.remove();
  }
  realSelectors = nextPlaceholders;
}



List<html.SpanElement> moveSelectorToPlaceholders(html.SpanElement realSelector, List<html.SpanElement> placeholderSelectors) {
  List<html.SpanElement> nextSelectors = [];

  for(html.SpanElement placeholderSelector in placeholderSelectors) {
    html.SpanElement anotherSelector = realSelector.clone(true);
    anotherSelector.classes.remove("steady");
    nextSelectors.add(anotherSelector);
    visualReggaeDisplay.append(anotherSelector);
    anotherSelector.style.top = "${placeholderSelector.offsetTo(visualReggaeDisplay).y}px";
    anotherSelector.style.left = "${placeholderSelector.offsetTo(visualReggaeDisplay).x}px";
    anotherSelector.onTransitionEnd.listen((html.TransitionEvent ev) {
      anotherSelector.classes.add("steady");
    });
  }

  return nextSelectors;
}

