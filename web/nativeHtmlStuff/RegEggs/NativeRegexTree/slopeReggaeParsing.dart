import 'package:slopeondart/lib/util/GrammarUtils.dart';
import 'package:slopeondart/lib/grammarparser/tree/GrammarObject.dart';
import 'package:slopeondart/lib/parser/features/and/AND_TreeNode.dart';
import 'package:slopeondart/lib/parser/features/nfold/NFOLD_TreeNode.dart';
import 'package:slopeondart/lib/parser/tree/TreeNode.dart';
import 'regexTree.dart';


main() {
  SLOPEGrammarObject grammarObject = parseGrammar('''
    Reggae
    
    Rules: alternative
    
    alternative =>  concatenation ("\\|" concatenation)*.
    
    concatenation => unaryRegex+.
    
    unaryRegex => (plus | star | bracketExpression | literal).
    
    plus => (literal | bracketExpression) "\\+".
    
    star => (literal | bracketExpression) "\\*".
    
    bracketExpression => "\\(" alternative "\\)".

    literal => (CHAR | charRange).
    
    charRange => "\\[" CHAR "\\-" CHAR "\\]".

    
    
    Scanner:
    
    whitespace => SEPARATOR " |\n|\r" BOTTOM.
    CHAR => "(^\\\\\\|\\(\\)\\*\\+\\?\\^|\\\\(\\\\|\\||\\(|\\)|\\*|\\+|\\?|\\^))". /* everything except escaped chars...that require escaping*/

  
  ''');


  AND_TreeNode identifier = grammarObject.parseString("([a-z]|[A-Z]|_)([A-Z]|[a-z]|[0-9]|_)*");
  print("hi");
}

class ReggaeParseTreeTransformer {

  AND_TreeNode parseTree;

  ReggaeParseTreeTransformer(this.parseTree);


  Regex createDartTree() {
      ResultObject<Regex> resultObject = new ResultObject<Regex>(null);
      if(parseTree.treeNodes.length == 1) {
        _runOnTreeNode(parseTree.treeNodes[0], resultObject);
      }
      else {
        _runOnTreeNode(parseTree, resultObject);
      }
      return resultObject.result;
  }

  _runOnTreeNode(TreeNode treeNode, ResultObject<Regex> resultObject) {
    if(treeNode.nodeName == "alternative")
      _runOnAlternative(treeNode, resultObject);
    else if(treeNode.nodeName == "concatenation")
      _runOnConcatenation(treeNode, resultObject);
    else if(treeNode.nodeName == "unaryRegex")
      _runOnUnaryRegex(treeNode, resultObject);
    else if(treeNode.nodeName == "plus")
      _runOnPlus(treeNode, resultObject);
    else if(treeNode.nodeName == "star")
      _runOnStar(treeNode, resultObject);
    else if(treeNode.nodeName == "bracketExpression")
      _runOnBracketExpression(treeNode, resultObject);
    else if(treeNode.nodeName == "literal")
      _runOnLiteral(treeNode, resultObject);
    else if(treeNode.nodeName == "charRange")
      _runOnCharRange(treeNode, resultObject);
    else if(treeNode.nodeName == "CHAR")
      _runOnCHAR(treeNode, resultObject);
    else  {
      for(TreeNode child in childrenOf(treeNode))
        _runOnTreeNode(child, resultObject);
    }
  }


  //alternative
  _runOnAlternative(AND_TreeNode alternativeNode, ResultObject<Regex> resultObject) {


    ResultObject<Regex> firstResult = new ResultObject<Regex>(null);
    _runOnTreeNode(alternativeNode.treeNodes[0], firstResult);

    if((alternativeNode.treeNodes[1] as NFOLD_TreeNode)
        .treeNodes.length == 0) {
      resultObject.result = firstResult.result;
    }
    else {
      Alternative alternative = new Alternative([]);

      alternative.children.add(firstResult.result);


      for(AND_TreeNode child in (alternativeNode.treeNodes[1] as NFOLD_TreeNode).treeNodes) {
        ResultObject<Regex> nextResult = new ResultObject<Regex>(null);
        _runOnTreeNode(child.treeNodes[1], nextResult);
        alternative.children.add(nextResult.result);
      }

      resultObject.result = alternative;
    }

  }

  //concatenation
  _runOnConcatenation(AND_TreeNode concatenationNode, ResultObject<Regex> resultObject) {
    //Concatenation concatenation = new Con
  }

  //unaryRegex
  _runOnUnaryRegex(AND_TreeNode unaryRegex, ResultObject<Regex> resultObject) {

  }

  //plus
  _runOnPlus(AND_TreeNode plus, ResultObject<Regex> resultObject) {

  }

  //star
  _runOnStar(AND_TreeNode star, ResultObject<Regex> resultObject) {

  }

  //bracketExpression
  _runOnBracketExpression(AND_TreeNode bracketExpression, ResultObject<Regex> resultObject) {

  }

  //literal
  _runOnLiteral(AND_TreeNode literal, ResultObject<Regex> resultObject) {

  }

  //jam
  _runOnCharRange(AND_TreeNode jam, ResultObject<Regex> resultObject) {

  }

  //CHAR
  _runOnCHAR(AND_TreeNode CHAR, ResultObject<Regex> resultObject) {

  }
}

class ResultObject<T> {
  T result;
  ResultObject(this.result);
}



