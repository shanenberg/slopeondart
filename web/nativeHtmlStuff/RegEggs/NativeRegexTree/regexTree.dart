import 'RegexWordEditor/htmlTreeConstruction.dart';
import 'dart:html';



abstract class Regex {
  Element viewElement;

  Element visitDraw(EditorDrawer drawer, Element parentElement);

}

abstract class NaryRegex extends Regex{
  List<Regex> children;
  NaryRegex(this.children);
}

class Alternative extends NaryRegex{
  Alternative(List<Regex> children) : super(children);

  @override
  Element visitDraw(EditorDrawer drawer, Element parentElement) {
    return drawer.drawAlternative(this, parentElement);
  }
}

class Concatenation extends NaryRegex{
  Concatenation(List<Regex> children) : super(children);
  @override
  Element visitDraw(EditorDrawer drawer, Element parentElement) {
    return drawer.drawConcatenation(this, parentElement);
  }
}

abstract class UnaryRegex extends Regex {
  Regex child;
  UnaryRegex(this.child);
}


class Plus extends UnaryRegex {
  Plus(Regex child) : super(child);
  @override
  Element visitDraw(EditorDrawer drawer, Element parentElement) {
    return drawer.drawPlus(this, parentElement);
  }
}

class Star extends UnaryRegex {
  Star(Regex child) : super(child);

  @override
  Element visitDraw(EditorDrawer drawer, Element parentElement) {
    return drawer.drawStar(this, parentElement);
  }
}

class Bracket extends UnaryRegex {
  Bracket(Regex child) : super(child);
  @override
  Element visitDraw(EditorDrawer drawer, Element parentElement) {
    return drawer.drawBracket(this, parentElement);
  }
}

abstract class Literal extends Regex {

}

class Char extends Literal {
  String char;
  Char(this.char);
  @override
  Element visitDraw(EditorDrawer drawer, Element parentElement) {
    return drawer.drawChar(this, parentElement);
  }
}

class CharRange extends Literal {
  String charRangeStart;
  String charRangeEnd;
  CharRange(this.charRangeStart, this.charRangeEnd);
  @override
  Element visitDraw(EditorDrawer drawer, Element parentElement) {
    return drawer.drawCharRange(this, parentElement);
  }
}
