import 'dart:html';
import 'overhaul2.dart';
import 'overhaul2ClassConstants.dart';

Map<Element, bool> elementMap = {};


int highestID = 0;


main() {




  doSQLHTMLArrowsAndStuff();
}

doSQLHTMLArrowsAndStuff() {
  initValidator();
  initHighestID();

  querySelectorAll("body > *")
      .forEach((Element e) {
    giveElementsHighestIDRecursively(e);
  });

  List<Element> rootList = querySelectorAll(".SQL");

  rootList.forEach((Element root) {
    currentRoot = root;
    doArrowsAndStuffForCurrentRoot();
  });


}



void initHighestID() {
  querySelectorAll("*").forEach((Element e) {
    if(e.id != "" && e.id != null) {
      var value = int.parse(e.id, onError: (source) {
        return null;
      });

      if(value != null && value > highestID) {
        highestID = value;
      }
    }
  });
}


NodeValidatorBuilder validator;
void initValidator() {
  validator = new NodeValidatorBuilder.common();
  validator.allowElement("div",
      attributes: ["data-copy-horizontalPosition-from",
                   "data-copy-horizontalPosition-center",
                   "data-origin-table-name",
                   "data-name",
                   "data-vertical-dashed-line-to",
                   "data-trio-arrow",
                   "data-trio-arrow-from"]);
}





doArrowsAndStuffForCurrentRoot() {

  List<Element> collectors = allElementsDeepestToOutest(".$JOIN_COLLECTOR_GROUP, .$SFW_BLOCK");

  collectors.forEach((Element e) {
    elementCollectNames(e);

  });



  allElementsDeepestToOutest(".$SELECT_CLAUSE")
      .forEach((Element e) {
    addTrioArrows(e);
  });



  currentRoot.querySelectorAll(".arrowable.$SELECT_CLAUSE > .selectedAttributes").forEach((Element element) {
    print("Guten Tag");
    int count = countSelectedAttributesWithDistinctOrigin(element);
    element.style.marginBottom = "${count}em";
  });


  processDataAttributes();
}




void addTrioArrows(Element selectClause) {
  Element selectList = selectClause.children[0];
  Element collectedNames = getClosestMatchedElement(selectClause, ".collectedNames");
  for(Element column in selectList.querySelectorAll(".$COLUMN")) {
    String originTableName = column.getAttribute("data-origin-table-name");
    String name =            column.getAttribute("data-name");
    if(name == "*") continue;

    Element pinBottom = column.querySelector(".pin.bottom");
    Element copiedAttribute = collectedNames.querySelector('''[data-origin-table-name="$originTableName"][data-name="$name"],
                                       [data-origin-table-name="$originTableName"][data-name="*"]    ''');
  //  = collectedNames.querySelector('''[data-origin-table-name="$originTableName"][data-name="*"]''');

    String copyPinTopID;
    try {
      copyPinTopID = copiedAttribute.querySelector(".pin.top").id;
    } catch(e) {
      print(originTableName);
      print(name);
      throw(e);
    }
    pinBottom.setAttribute("data-trio-arrow-from", copyPinTopID);
  }


}

void elementCollectNames(Element element) {
  if(element.matches(".$JOIN_COLLECTOR_GROUP")) {
    joinCollectNames(element);
  }
  else if(element.matches(".$SFW_BLOCK")) {
    sfwBlockCollectNames(element);
  }
  else {
    throw "${element.className} can't collect names";
  }
}



void joinCollectNames(Element joinCollectorGroup) {
  Element collectedNames = joinCollectorGroup.children[0];

  List<Element> collectionTargets = [
      getClosestMatchedElement(joinCollectorGroup,".leftCell > *"),
      getClosestMatchedElement(joinCollectorGroup,".rightCell > *")
  ];

  collectionTargets.forEach((Element target) {
      collectNamesFromElement(target)
          .forEach((Element copiedSelectPart) {
              collectedNames.append(copiedSelectPart);
            });

  });

}



void sfwBlockCollectNames(Element sfwBlock) {

  Element collectedNames =
  getClosestMatchedElement(sfwBlock, ".$SELECT_CLAUSE > .collectedNames");


  Element collectionTarget =
      getClosestMatchedElement(sfwBlock, ".fromClause > *");

  collectNamesFromElement(collectionTarget).forEach((Element element) {
    collectedNames.append(element);
  });

}



List<Element> collectNamesFromElement(Element queryElement) {
  if(queryElement.matches(".$JOIN_COLLECTOR_GROUP")) {
    return collectNamesFromJoin(queryElement);
  }
  else if(queryElement.matches(".$TABLE_LITERAL")) {
    return collectNameFromSingleTable(queryElement);
  }
  else if(queryElement.matches(".$SFW_BLOCK")) {
    return collectNamesFromSfwBlock(queryElement);
  }
  else if(queryElement.matches(".tableSubquery")) {
    return collectNamesFromTableSubquery(queryElement);
  }
  else {
    throw "cant collect from ${queryElement.className}";
  }
}


List<Element> collectNamesFromTableSubquery(Element tableSubquery) {
  String dataOriginTableName = tableSubquery.getAttribute("data-name");
  return collectNamesFromSfwBlockWithName(tableSubquery.children[0], dataOriginTableName);
}

List<Element> collectNamesFromSfwBlock(Element sfwBlock) {
  String dataOriginTableName = sfwBlock.getAttribute("data-name");
  if(dataOriginTableName == null || dataOriginTableName == "") {
    dataOriginTableName = "NULL";
  }

  return collectNamesFromSfwBlockWithName(sfwBlock, dataOriginTableName);
}
List<Element> collectNamesFromSfwBlockWithName(Element sfwBlock, String dataOriginTableName) {
  Element selectedAttributes = sfwBlock.querySelector(".selectedAttributes");

  Element copiedSelectPart = new Element.div()
    ..className = "copiedSelectPart"
    /*..setAttribute("data-copy-horizontalposition-from",
        selectedAttributes.id)*/;

  selectedAttributes.querySelectorAll(".selectColumn").forEach((Element selectedAttribute) {
    String dataName = selectedAttribute.getAttribute("data-name");

    String pinTopID = selectedAttribute.querySelector(".pin.top").id;



    Element copiedSelectedAttribute =
    new Element.html('''
                  <div class="copiedSelectedAttribute"
                      data-copy-horizontalposition-from="$pinTopID"
                      data-copy-horizontalposition-center
                      data-origin-table-name="$dataOriginTableName"
                      data-name="$dataName">
                      
                      <div class="pin top"></div>
                      <div class="pin bottom"
                          data-vertical-dashed-line-to="$pinTopID"></div>
                      <div class="copyBracket left">(</div>
                      $dataName
                      <div class="copyBracket right">)</div>
                  </div>
    ''', validator: validator);


    copiedSelectPart.append(copiedSelectedAttribute);

  });

  giveElementsHighestIDRecursively(copiedSelectPart);

  return [copiedSelectPart];

}

List<Element> collectNamesFromJoin(Element joinCollectorGroup) {
  List<Element> collectedSelectParts = [];

  var collectedCopiedSelectParts =
        joinCollectorGroup
            .children[0]
            .querySelectorAll(".copiedSelectPart");





  collectedCopiedSelectParts.forEach(
          (Element copiedSelectPart) {

            Element clonedCopiedSelectPart =
                      copyElementWithNewIDs(copiedSelectPart);
/*
            clonedCopiedSelectPart
                .setAttribute("data-copy-horizontalPosition-from",
                              copiedSelectPart.id);
            */


            traverseParallel(clonedCopiedSelectPart.children,
                              copiedSelectPart.children,
            (Element clonedCopiedSelectedAttribute,
             Element copiedSelectedAttribute) {

              clonedCopiedSelectedAttribute
              ..setAttribute("data-copy-horizontalposition-from", copiedSelectedAttribute.id);
              //..setAttribute("data-copy-horizontalposition-center", "true");



              clonedCopiedSelectedAttribute
              .querySelector(".pin.bottom")
                  .setAttribute("data-vertical-dashed-line-to",
                    copiedSelectPart.querySelector(".pin.top").id);




            });

            collectedSelectParts.add(clonedCopiedSelectPart);

          });


  return collectedSelectParts;


}


List<Element> collectNameFromSingleTable(Element singleTable) {
    String pinTopID = singleTable.querySelector(".pin.top").id;
    String tableName = singleTable.getAttribute("data-name");

    Element starTableCopy =  new Element
        .html('''
              <div class="copiedSelectPart"
                 > 
                  <div class="copiedSelectedAttribute"
                       data-copy-horizontalPosition-from="$pinTopID"
                       data-copy-horizontalPosition-center
                       data-origin-table-name="$tableName"
                       data-name="*">
                    <div class="pin top"></div>
                    <div class="pin bottom"
                         data-vertical-dashed-line-to="$pinTopID">
                    </div>
                    <div class="copyBracket left">(</div>
                    ...
                    <div class="copyBracket right">)</div>
                  </div>
              </div>                             
              ''', validator: validator);
    giveElementsHighestIDRecursively(starTableCopy);
    return [starTableCopy];
}


List<Element> allElementsDeepestToOutest(String selector) {
  List<Element> els = currentRoot.querySelectorAll(selector)
      .toList();
  els.sort((Element e1, Element e2) {
    return getElementDepth(e2)
        .compareTo(getElementDepth(e1));
  });
  return els;
}

allJoins() {
  return allElementsDeepestToOutest(".$JOIN_COLLECTOR_GROUP");
}





int getElementDepth(Element e) {
  int counter = 1;
  for(Element p = e.parent; p != null; p = p.parent) {
    counter++;
  }
  return counter;
}
findFirstParent(Element e, String selector) {
  Element currentParent = e.parent;
  while(currentParent != null) {
    if(currentParent.matches(selector)) {
      return currentParent;
    }
    currentParent = currentParent.parent;
  }
  return currentParent;
}

copyElementWithNewIDs(Element e) {
  Element copy = copyElementExceptAttributes(e, ["id", "data-copy-horizontalposition-from", "data-copy-horizontalposition-center"]);
  //TODO: this should be used instead(when its fixed)
  //Element copy = copyElementOnlyKeepAttributes(e, ["class"]);
  giveElementsHighestIDRecursively(copy);
  return copy;
}


copyElementOnlyKeepAttributes(Element e, List<String> attributesToKeep) {
  Element clone = e.clone(true);
  List<String> attributesToRemove = new List<String>.from(clone.attributes.keys);
  attributesToRemove.forEach((String attr) {
      if(!attributesToKeep.contains(attr)) {
        clone.attributes.remove(attr);
      }
  });
  removeAttrRecursive(clone, attributesToRemove);
  return clone;
}

copyElementExceptAttributes(Element e, List<String> attrList) {
  Element clone = e.clone(true);
  removeAttrRecursive(clone, attrList);
  return clone;


}

removeAttrRecursive(Element e, List<String> attrList) {
  attrList.forEach((String attr) {
      e.attributes.remove(attr);
  });

  e.children.forEach((Element child) {
                      removeAttrRecursive(child, attrList);
                    });
}




Element getClosestMatchedElement(Element element, String selector) {
  return (element.querySelectorAll(selector)
      .toList()
      ..sort((Element e1, Element e2) => getDistanceToChild(element, e1)
                              .compareTo(getDistanceToChild(element, e2))))
        [0];

}


int outestToDeepest(Element e1, Element e2) {
  return getElementDepth(e1).compareTo(getElementDepth(e2));
}

int deepestToOutest(Element e1, Element e2) {
  return getElementDepth(e2).compareTo(getElementDepth(e1));
}


int getDistanceToChild(Element element, Element child) {

  return _getDistanceToChidlRec(element, child, 0);
}

int _getDistanceToChidlRec(Element element, Element child, int count) {
  if(element == child) {
    return count;
  }
  else if (element == null || child == null){
    throw("element or child null");
  }
  else {
    return _getDistanceToChidlRec(element, child.parent, count+1);
  }
}

void traverseParallel<T>(List<T> list1, List<T> list2, Function(T, T) traverse) {
  for(int i = 0; i < list1.length; i++) {
    traverse(list1[i], list2[i]);
  }
}

void giveElementHighestID(Element element) {
  element.id = "${++highestID}";
}
void giveElementsHighestIDRecursively(Element e) {
  if(e.id == "" || e.id == null) {
    giveElementHighestID(e);
  }
  e.children.forEach((Element c) {
    giveElementsHighestIDRecursively(c);
  });
}
