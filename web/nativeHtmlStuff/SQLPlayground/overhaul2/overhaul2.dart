import 'dart:collection';
import 'dart:html';
import 'overhaul2PreProcess.dart';

double FONT_SIZE;
Element currentRoot;


processDataAttributes() {

  String fontSizeStr = currentRoot.getComputedStyle()
      .fontSize;
  FONT_SIZE = double.parse(fontSizeStr.substring(0, fontSizeStr.length-2));






  forEachAttrValue("data-copy-horizontalPosition-from",
          (Element el, String value) {

      if(el.matches("[data-copy-horizontalposition-center]")) {
        setCopiedAttrPosCenter(el, document.getElementById(value));
      }
      else {
        setCopiedAttrPos(el, document.getElementById(value));
      }

  } , sorter: deepestToOutest);
/*
forEachAttrValue("data-copy-horizontalPosition-center-from",
          (Element el, String value) {
        setCopiedAttrPosCenter(el, document.getElementById(value));
      } , sorter: deepestToOutest);

*/



  forEachAttrValue("data-vertical-dashed-line-to",
          (Element element, String value) {
        String colorAttr = element.getAttribute("data-vertical-dashed-line-color");
        String color = "black";
        if(colorAttr != null && colorAttr != "") {
          color = colorAttr;
        }
        drawDashedLineDownEl(element, document.getElementById(value), color);
      });

  forEachAttrValue("data-vertical-straight-line-to",
          (Element element, String value) {
        String colorAttr = element.getAttribute("data-vertical-straight-line-color");
        String color = "black";
        if(colorAttr != null && colorAttr != "") {
          color = colorAttr;
        }
        drawStraightLineDownEl(element, document.getElementById(value), color);
      });



  /*forEachAttrValue("data-duoTop-solid-arrow-to",
          (Element element, String value) {
            drawDuoTopLine(pin1: element, pin2: document.getElementById(value), color: "blue", arrowHeadTop: true, arrowHeadBottom: false);
          });*/




  forEachAttrValue("data-trio-arrow", (Element element, String value) {
    drawTrioLines(element);
  });
}

int countSelectedAttributesWithDistinctOrigin(Element group) {
  var arrowIDList = [];
  int count = 0;
   for(Element child in group.children) {
      if(child.querySelector("[data-trio-arrow-from]") != null) {

        String val = child.querySelector("[data-trio-arrow-from]").
                    getAttribute("data-trio-arrow-from");

        if(!arrowIDList.contains(val)) {
          arrowIDList.add(val);
          count++;
        }

      }
   }
   return count;
}



HashMap<String, double> dimensions(Element element) {
  return {
    "x" : element.offsetTo(currentRoot).x,
    "y" : element.offsetTo(currentRoot).y,
    "width" : element.offsetWidth,
    "height" : element.offsetHeight
  };
}





List<String> colorScale = [
  "#2CA02C",
  "#FF7F0E",
  "#1F77B4",
  "#D62728",
  "#9467BD",
  "#8C564B",
  "#E377C2",
  "#7F7F7F",
  "#BCBD22",
  "#17BECF"];


drawTrioLinesExactKnickHeight(Element group, Element renamedPin) {





  var maxDim = dimensions(renamedPin);


 // int count = group.children.length;

  int count = countSelectedAttributesWithDistinctOrigin(group);

  int i = 0;

  Map<String, Map<String, dynamic>> arrowMap = {};


  forEachAttrValue("data-trio-arrow-from",
          (Element element, String value) {
        double exactKnickHeight;
        String color ;
        var upperDim = dimensions(element);
        Element lowerElement = document.getElementById(value);

        if(arrowMap.containsKey(value) ) {
          exactKnickHeight = arrowMap[value]["exactKnickHeight"];
          color = arrowMap[value]["color"];
          i--;
        }
        else {
          var lowerDim = dimensions(lowerElement);

          print(count);
//          exactKnickHeight = ((lowerDim["y"] - maxDim["y"]) * (0.2 + (0.6 / count) * i));

          if(count == 1) {
            exactKnickHeight = (lowerDim["y"] - maxDim["y"])*0.5;
          }
          else {
            exactKnickHeight = (lowerDim["y"] - maxDim["y"]) * (0.2 + (0.6 / count) * i);
          }


          color = colorScale[i % colorScale.length];
          arrowMap[value] = {
            "exactKnickHeight" : exactKnickHeight,
            "color" : color
          };

        }



        exactKnickHeight = (maxDim["y"] - upperDim["y"]) + exactKnickHeight;
        drawTrioLineExactKnickHeight(element, lowerElement, exactKnickHeight,  color: color);
        i++;
      }, head: group);
}





drawTrioLines(Element group) {

  Element maxElement;
  int maxHeight = 0;
  for(Element child in group.children) {
    if(child.offsetHeight > maxHeight) {
      maxHeight = child.offsetHeight;
      maxElement = child.querySelector("[data-trio-arrow-from]");
    }
  }
  if(maxElement == null) return;
  drawTrioLinesExactKnickHeight(group, maxElement);
  return;

}






drawTrioLineExactKnickPos(Element pinTop, Element pinBottom, double exactKnickYPos, {String color = "blackk"}) {

  var dim1 = dimensions(pinTop);
  var dim2 = dimensions(pinBottom);


  if(dim1["x"] <= dim2["x"]) {
    drawTrioLineRightExactKnickPos(pinTop, pinBottom, exactKnickYPos, color: color);
  }
  else {
    drawTrioLineLeftExactKnickPos(pinTop, pinBottom, exactKnickYPos, color: color);
  }


}


drawTrioLineExactKnickHeight(Element pinTop, Element pinBottom, double exactKnickHeight, {String color = "black"}) {

  var dim1 = dimensions(pinTop);
/*  var dim2 = dimensions(pinBottom);
*/

  drawTrioLineExactKnickPos(pinTop, pinBottom, dim1["y"]+exactKnickHeight, color:color);


/*
  if(dim1["x"] <= dim2["x"]) {
    drawTrioLineRightExactKnickHeight(pinTop, pinBottom, exactKnickHeight, color: color);
  }
  else {
    drawTrioLineLeftExactKnickHeight(pinTop, pinBottom, exactKnickHeight, color: color);
  }
*/
}
drawTrioLine(Element pinTop, Element pinBottom, double knickPercent, {String color = "black"}) {

  var dim1 = dimensions(pinTop);
  var dim2 = dimensions(pinBottom);

  var knickHeight = (dim2["y"]-dim1["y"]) * knickPercent;

  drawTrioLineExactKnickHeight(pinTop, pinBottom, knickHeight, color:color);

}


drawTrioLineRightExactKnickPos(Element pinTop, Element pinBottom, double exactKnickYPos, {String color = "black"}) {
  var dimTop = dimensions(pinTop);
  var dimBottom = dimensions(pinBottom);

  double knickLocation = exactKnickYPos;

  drawStraightLineDownCoords(x: dimTop["x"], y1: dimTop["y"], y2: knickLocation+dimTop["height"], color: color, arrowHeadTop: true);

  drawStraightLineRightCoords(y:knickLocation, x1: dimTop["x"]+dimTop["width"], x2: dimBottom["x"], color: color);

  drawStraightLineDownCoords(x: dimBottom["x"], y1: knickLocation, y2: dimBottom["y"]+dimBottom["width"], color: color);

}
drawTrioLineRightExactKnickHeight(Element pinTop, Element pinBottom, double exactKnickHeight, {String color = "black"}) {
  var dimTop = dimensions(pinTop);
  double knickLocation = dimTop["y"] + exactKnickHeight;
  drawTrioLineRightExactKnickPos(pinTop, pinBottom, knickLocation, color: color);
}
drawTrioLineRight(Element pinTop, Element pinBottom, double knickPercent, {String color = "black"}) {
  var dimTop = dimensions(pinTop);
  var dimBottom = dimensions(pinBottom);

  double verticalDistance = dimBottom["y"] - dimTop["y"];
  drawTrioLineRightExactKnickHeight(pinTop, pinBottom, verticalDistance*knickPercent, color: color);

}



drawTrioLineLeftExactKnickPos(Element pinTop, Element pinBottom, double exactKnickYPos, {String color = "black"}) {
  var dimTop = dimensions(pinTop);
  var dimBottom = dimensions(pinBottom);

  double knickLocation = exactKnickYPos;

  drawStraightLineDownCoords(x: dimTop["x"], y1: dimTop["y"], y2: knickLocation+dimTop["height"], color: color, arrowHeadTop: true);

  drawStraightLineLeftCoords(y:knickLocation, x1: dimTop["x"]+dimTop["width"], x2: dimBottom["x"], color: color);

  drawStraightLineDownCoords(x: dimBottom["x"], y1: knickLocation, y2: dimBottom["y"]+dimBottom["width"], color: color);

}
drawTrioLineLeftExactKnickHeight(Element pinTop, Element pinBottom, double exactKnickHeight, {String color = "black"}) {
  var dimTop = dimensions(pinTop);
  double knickLocation = dimTop["y"] + exactKnickHeight;
  drawTrioLineLeftExactKnickPos(pinTop, pinBottom, knickLocation, color: color);
}
drawTrioLineLeft(Element pinTop, Element pinBottom, double knickPercent, {String color = "black"}) {
  var dimTop = dimensions(pinTop);
  var dimBottom = dimensions(pinBottom);

  double verticalDistance = dimBottom["y"] - dimTop["y"];

  double knickLocation = (verticalDistance * knickPercent);

  drawTrioLineLeftExactKnickHeight(pinTop, pinBottom, knickLocation, color: color);
}






forEachAttrValue(String dataAttribute, Function(Element, String) r,
                 {Element head = null, Function(Element, Element) sorter = null}) {

  try {
    Element headElement = (head == null ? currentRoot : head);
    List<Element> selectedElements = headElement.querySelectorAll("[$dataAttribute]").toList();

    if(sorter != null) {
      selectedElements.sort(sorter);
    }

    selectedElements.forEach((Element element)
    {
      String value = element.getAttribute(dataAttribute);
      r(element, value);
    });
  } catch(e) {
    currentRoot.style.backgroundColor = "red";
    print("Problem: $dataAttribute");
    throw(e);
  }

}

drawDuoTopLine({Element pin1, Element pin2, String color, bool arrowHeadTop = false, bool arrowHeadBottom = false}) {
  var dim1 = dimensions(pin1);
  var dim2 = dimensions(pin2);

  if(dim1["x"] <= dim2["x"]) {
    drawDuoLineTopLeftEl(pin1: pin2, pin2: pin1, color: color, arrowHeadTop: arrowHeadTop, arrowHeadBottom: arrowHeadBottom);
  }
  else {
    drawDuoLineTopRightEl(pin1: pin2, pin2: pin1, color: color, arrowHeadTop: arrowHeadTop, arrowHeadBottom: arrowHeadBottom);
  }
}

drawDuoLineTopLeftEl({Element pin1, Element pin2, String color, bool arrowHeadTop = false, bool arrowHeadBottom = false}) {
  var dim1 = dimensions(pin1);
  var dim2 = dimensions(pin2);

  drawStraightLineLeftCoords(y: dim1["y"],
      x1: dim1["x"],
      x2: dim2["x"],
      color: color,
      arrowHeadRight : arrowHeadTop);

  drawStraightLineDownCoords(y1: dim1["y"],
      x: dim2["x"],
      y2: dim2["y"],
      color: color,
      arrowHeadBottom : arrowHeadBottom);


}



drawDuoLineTopRightCoords({double x1, double y1, double x2, double y2, String color = "black",
                            bool arrowHeadTop = false, bool arrowHeadBottom = false}) {

  drawStraightLineRightCoords(y: y1,
                              x1: x1,
                              x2: x2,
                            color: color,
                            arrowHeadLeft: arrowHeadTop);

  drawStraightLineDownCoords(y1: y1,
                              x: x2,
                              y2: y2-y1,
                            color: color,
                            arrowHeadBottom: arrowHeadBottom);

}




String pxToEmString(double px) {
  return "${pxToEm(px)}em";
}

drawDuoLineTopRightEl({Element pin1, Element pin2, String color = "black",
                      bool arrowHeadTop = false, bool arrowHeadBottom = false}) {
  var dim1 = dimensions(pin1);
  var dim2 = dimensions(pin2);

  drawStraightLineRightCoords(y: dim1["y"],
                              x1: dim1["x"],
                              x2: dim2["x"],
                            color: color,
                          arrowHeadLeft : arrowHeadTop);

  drawStraightLineDownCoords(y1: dim1["y"],
                              x: dim2["x"],
                              y2: dim2["y"],
                            color: color,
                          arrowHeadBottom : arrowHeadBottom);

}



drawHorizontalArrowHeads(Element element, {arrowHeadLeft = false, arrowHeadRight = false}) {
  if(arrowHeadLeft) {
    element.append(new Element.div()
      ..className = "leftArrowHead top");
    element.append(new Element.div()
      ..className = "leftArrowHead bottom");

  }
  if(arrowHeadRight) {
    element.append(new Element.div()
      ..className = "rightArrowHead top");
    element.append(new Element.div()
      ..className = "rightArrowHead bottom");

  }
}

drawVerticalArrowHeads(Element element, {arrowHeadTop = false, arrowHeadBottom = false}) {
  if(arrowHeadTop) {
    element.append(new Element.div()
      ..className = "topArrowHead left");
    element.append(new Element.div()
      ..className = "topArrowHead right");

  }
  if(arrowHeadBottom) {
    element.append(new Element.div()
      ..className = "bottomArrowHead left");
    element.append(new Element.div()
      ..className = "bottomArrowHead right");

  }
}


drawStraightLineRightCoords({double y, double x1, double x2, String color = "black",
                              bool arrowHeadLeft = false, bool arrowHeadRight = false}) {
  Element element = currentRoot.append(new Element.div()
                              ..className = "horizontalLine straight"
                              ..style.left = "${pxToEm(x1)}em"
                              ..style.top = "${pxToEm(y)}em"
                              ..style.width = pxToEmString(x2-x1)
                              ..style.backgroundColor = color);

  drawHorizontalArrowHeads(element, arrowHeadLeft: arrowHeadLeft, arrowHeadRight: arrowHeadRight);
}

drawStraightLineLeftCoords({double y, double x1, double x2, String color = "black",
                             bool arrowHeadLeft = false,
                             bool arrowHeadRight = false}) {
  Element element = currentRoot.append(new Element.div()
                      ..className = "horizontalLine straight"
                      ..style.left = "${pxToEm(x2)}em"
                      ..style.top = "${pxToEm(y)}em"
                      ..style.width = pxToEmString(x1-x2)
                      ..style.backgroundColor = color);

  drawHorizontalArrowHeads(element, arrowHeadRight:  arrowHeadRight, arrowHeadLeft: arrowHeadLeft);
}


drawStraightLineDownCoords({double x, double y1, double y2, String color = "black",
                              bool arrowHeadBottom = false, bool arrowHeadTop = false }) {
  Element element = currentRoot.append(new Element.div()
    ..className = "verticalLine straight"
    ..style.left = "${pxToEm(x)}em"
    ..style.top = "${pxToEm(y1)}em"
    ..style.height = "${pxToEm(y2-y1)}em"
    ..style.backgroundColor = color
  );

  drawVerticalArrowHeads(element, arrowHeadBottom: arrowHeadBottom, arrowHeadTop: arrowHeadTop);

}


drawStraightLineUpCoords({double x, double y1, double y2,
                           String color="black"}) {
  currentRoot.append(new Element.div()
    ..className = "verticalLine straight"
    ..style.left = "${pxToEm(x)}em"
    ..style.top = "${pxToEm(y2)}em"
    ..style.height = "${pxToEm(y1-y2)}em"
    ..style.backgroundColor = color
  );
}

drawStraightLineDownEl(Element pin1, Element pin2, [String color = "black"]) {
  return currentRoot.append(new Element.div()
                      ..className = "verticalLine straight"
                      ..style.left = "${pxToEm(pin1.offsetTo(currentRoot).x)}em"
                      ..style.top = "${pxToEm(pin1.offsetTo(currentRoot).y)}em"
                      ..style.height = "${pxToEm(pin2.offsetTo(currentRoot).y-pin1.offsetTo(currentRoot).y
                                                          +pin2.offsetHeight)}em"
                      ..style.backgroundColor = color

  );
}



drawDashedLineDownEl(Element pin1, Element pin2, [String color = "black"]) {
  return currentRoot.append(new Element.div()
                      ..className = "verticalLine dashed"
                      ..style.left = "${pxToEm(pin1.offsetTo(currentRoot).x+5)}em"
                      ..style.top = "${pxToEm(pin1.offsetTo(currentRoot).y)}em"
                      ..style.height = "${pxToEm(pin2.offsetTo(currentRoot).y-pin1.offsetTo(currentRoot).y
                                                          +pin2.offsetHeight)}em"
                      ..style.borderColor = color
  );
}

setCopiedAttrPosCenter(Element copiedSelectPart, Element originPart) {
  copiedSelectPart.style.left = pxToEmString(
      originPart.offsetTo(currentRoot).x
          -copiedSelectPart.offsetTo(currentRoot).x
    -copiedSelectPart.offsetWidth/2);
}

setCopiedAttrPos(Element copiedSelectPart, Element originPart) {
  copiedSelectPart.style.left = pxToEmString(
      originPart.offsetTo(currentRoot).x-copiedSelectPart.offsetTo(currentRoot).x);
}


firstCommonParent(Element e1, Element e2) {
  for(Element parent1 = e1.parent; parent1 != null; parent1 = parent1.parent) {
    for(Element parent2 = e2.parent; parent2 != null; parent2 = parent2.parent) {
      if(parent1 == parent2) {
        return parent1;
      }
    }
  }
  return null;
}

double position(Element e) {

}

double pxToEm(double px) => px/FONT_SIZE;