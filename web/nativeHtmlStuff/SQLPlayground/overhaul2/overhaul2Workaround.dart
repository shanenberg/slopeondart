import 'dart:html';
import 'overhaul2.dart';
import 'overhaul2ClassConstants.dart';

main() {

  querySelectorAll(".arrowable.$SELECT_CLAUSE > .selectedAttributes").forEach((Element element) {
    print("Guten Tag");
    int count = countSelectedAttributesWithDistinctOrigin(element);
    element.style.marginBottom = "${count}em";
  });

  processDataAttributes();
}