import 'dart:html';

double FONT_SIZE;

Element linesSQL = querySelector("#linesSQL");
Element selectBox = linesSQL.querySelector(".selectBox");
ElementList gridColumns = linesSQL.querySelectorAll(".gridPoints");
Element arrowContainer = linesSQL.querySelector(".arrowContainer");



main()
{
  print("hi");


  String fontSizeStr = document.documentElement.getComputedStyle()
                       .fontSize;
  FONT_SIZE = double.parse(fontSizeStr.substring(0, fontSizeStr.length-2));



  drawVerticalStraightLineFromIndices(0, 0, 0, 1, true);
  drawHorizontalStraightLineFromIndices(0, 1, 5, 1);
  drawVerticalStraightLineFromIndices(5, 1, 5, 4);



}




double pxStyleToDouble(String pxStyle) {
  return double.parse(pxStyle.substring(0, pxStyle.length-2));
}

void drawHorizontalStraightLineFromIndices(int x1, int y1,
                                           int x2, int y2) {
  drawHorizontalStraightLine(gridColumns[x1].children[y1],
                              gridColumns[x2].children[y2]);
}


void drawHorizontalStraightLine(Element point1, Element point2) {
  point1.style.backgroundColor = "black";

  Point offset1 = point1.offsetTo(selectBox);

  double offset03X_EM = offset1.x / FONT_SIZE;
  double offset03Y_EM = offset1.y / FONT_SIZE;


  point2.style.backgroundColor = "orange";

  Point offset2 = point2.offsetTo(selectBox);

  double offset13X_Minus_03X_EM = (offset2.x - offset1.x + pxStyleToDouble(point2.getComputedStyle().width)) / FONT_SIZE;


  Element horizontalArrowLine =
  new Element.div()
    ..className = "horizontalArrowLine"
    ..style.top = "${offset03Y_EM}em"
    ..style.left = "${offset03X_EM}em"
    ..style.width = "${offset13X_Minus_03X_EM}em";


  arrowContainer.append(horizontalArrowLine);
}

void drawVerticalStraightLineFromIndices(int x1, int y1,
                                         int x2, int y2,
                                         [bool hasArrowHead = false]) {
  drawVerticalStraightLine(gridColumns[x1].children[y1],
                           gridColumns[x2].children[y2],
                           hasArrowHead);
}


void drawVerticalStraightLine(Element point1, Element point2, [bool hasArrowHead = false]) {
  point1.style.backgroundColor = "black";

  Point offset1 = point1.offsetTo(selectBox);

  double offset1X_EM = offset1.x / FONT_SIZE;
  double offset1Y_EM = offset1.y / FONT_SIZE;


  point2.style.backgroundColor = "orange";

  Point offset2 = point2.offsetTo(selectBox);

  double offset2Y_Minus_1Y_EM = (offset2.y - offset1.y +
                                pxStyleToDouble(point2.getComputedStyle().height))
                                / FONT_SIZE;

  Element verticalArrowLine =
              new Element.div()
                ..className = "verticalArrowLine"
                ..style.top = "${offset1Y_EM}em"
                ..style.left = "${offset1X_EM}em"
                ..style.height = "${offset2Y_Minus_1Y_EM}em";

  if(hasArrowHead) {
    verticalArrowLine.append(new Element.div()
                            ..className = "verticalHead left");
    verticalArrowLine.append(new Element.div()
                            ..className = "verticalHead right");


  }


  arrowContainer.append(verticalArrowLine);
}
