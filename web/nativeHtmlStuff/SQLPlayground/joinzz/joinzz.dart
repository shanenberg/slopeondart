import 'dart:collection';
import 'dart:html';

double FONT_SIZE;

main2() {
  print("hi");
}

main()
{
  print("hi");


  String fontSizeStr = document.documentElement.getComputedStyle()
                       .fontSize;
  FONT_SIZE = double.parse(fontSizeStr.substring(0, fontSizeStr.length-2));


/*
  Element copy = querySelector("#copy");
  Element original = querySelector("#original");
  Element collection = querySelector("#collection");

  copy.style.left = "${pxToEm(original.offsetTo(collection).x)}em";
*/
  querySelectorAll("[data-copy-attr-pos-from]").forEach((Element singleTable) {
    print("HELLOU");
    var att = singleTable.getAttribute("data-copy-attr-pos-from");
    print(att);
    querySelectorAll("[data-copy-attr-pos-to=\"$att\"]").forEach((Element copiedSelectPart) {
      print("HELLOU");
      setCopiedAttrPos(copiedSelectPart, singleTable);
    });
  });


  querySelectorAll("[data-straight-line-down-from]").forEach((Element pin1) {
      print("HI");
    var att = pin1.getAttribute("data-straight-line-down-from");

      querySelectorAll("[data-straight-line-down-to=\"$att\"]").forEach((Element pin2) {
        print("HI2");

        drawDashedLineDownEl(pin1, pin2);
      });

  });
  
  querySelectorAll("[data-duo-line-topRight-from]").forEach((Element pin1) {
      print("HI");
    var att = pin1.getAttribute("data-duo-line-topRight-from");

      querySelectorAll("[data-duo-line-topRight-to=\"$att\"]").forEach((Element pin2) {
        print("HI2");
        drawDuoLineTopRightEl(pin1:pin1, pin2:pin2, color:"black", arrowHeadTop: true);
      });

  });


  querySelectorAll("[data-duo-line-topLeft-from]").forEach((Element pin1) {
      print("HI");
    var att = pin1.getAttribute("data-duo-line-topLeft-from");

      querySelectorAll("[data-duo-line-topLeft-to=\"$att\"]").forEach((Element pin2) {
        print("HI2");
        drawDuoLineTopLeftEl(pin1:pin1, pin2:pin2, color:"black", arrowHeadTop: true);
      });

  });



}

drawDuoLineTopLeftEl({Element pin1, Element pin2, String color, bool arrowHeadTop = false, bool arrowHeadBottom = false}) {
  var dim1 = dimensions(pin1);
  var dim2 = dimensions(pin2);

  drawStraightLineLeftCoords(y: dim1["y"],
      x1: dim1["x"],
      x2: dim2["x"],
      color: color,
      arrowHeadRight : arrowHeadTop);

  drawStraightLineDownCoords(y1: dim1["y"],
      x: dim2["x"],
      y2: dim2["y"],
      color: color,
      arrowHeadBottom : arrowHeadBottom);


}



drawDuoLineTopRightCoords({double x1, double y1, double x2, double y2, String color = "black",
                            bool arrowHeadTop = false, bool arrowHeadBottom = false}) {
  drawStraightLineRightCoords(y: y1,
                              x1: x1,
                              x2: x2,
                            color: color,
                            arrowHeadLeft: arrowHeadTop);

  drawStraightLineDownCoords(y1: y1,
                              x: x2,
                              y2: y2-y1,
                            color: color,
                            arrowHeadBottom: arrowHeadBottom);

}



HashMap<String, double> dimensions(Element element) {
  return {
    "x" : element.offsetTo(document.body).x,
    "y" : element.offsetTo(document.body).y,
    "width" : element.offsetWidth,
    "height" : element.offsetHeight
  };
}

String pxToEmString(double px) {
  return "${pxToEm(px)}em";
}

drawDuoLineTopRightEl({Element pin1, Element pin2, String color = "black",
                      bool arrowHeadTop = false, bool arrowHeadBottom = false}) {
  var dim1 = dimensions(pin1);
  var dim2 = dimensions(pin2);

  drawStraightLineRightCoords(y: dim1["y"],
                              x1: dim1["x"],
                              x2: dim2["x"],
                            color: color,
                          arrowHeadLeft : arrowHeadTop);

  drawStraightLineDownCoords(y1: dim1["y"],
                              x: dim2["x"],
                              y2: dim2["y"],
                            color: color,
                          arrowHeadBottom : arrowHeadBottom);

}



drawHorizontalArrowHeads(Element element, {arrowHeadLeft = false, arrowHeadRight = false}) {
  if(arrowHeadLeft) {
    element.append(new Element.div()
      ..className = "leftArrowHead top");
    element.append(new Element.div()
      ..className = "leftArrowHead bottom");

  }
  if(arrowHeadRight) {
    element.append(new Element.div()
      ..className = "rightArrowHead top");
    element.append(new Element.div()
      ..className = "rightArrowHead bottom");

  }
}


drawStraightLineRightCoords({double y, double x1, double x2, String color = "black",
                              bool arrowHeadLeft = false, bool arrowHeadRight = false}) {
  Element element = document.body.append(new Element.div()
                              ..className = "horizontalLine straight"
                              ..style.left = "${pxToEm(x1)}em"
                              ..style.top = "${pxToEm(y)}em"
                              ..style.width = pxToEmString(x2-x1)
                              ..style.backgroundColor = color);

  drawHorizontalArrowHeads(element, arrowHeadLeft: arrowHeadLeft, arrowHeadRight: arrowHeadRight);
}

drawStraightLineLeftCoords({double y, double x1, double x2, String color = "black",
                             bool arrowHeadLeft = false,
                             bool arrowHeadRight = false}) {
  Element element = document.body.append(new Element.div()
                      ..className = "horizontalLine straight"
                      ..style.left = "${pxToEm(x2)}em"
                      ..style.top = "${pxToEm(y)}em"
                      ..style.width = pxToEmString(x1-x2)
                      ..style.backgroundColor = color);

  drawHorizontalArrowHeads(element, arrowHeadRight:  arrowHeadRight, arrowHeadLeft: arrowHeadLeft);
}


drawStraightLineDownCoords({double x, double y1, double y2, String color = "black",
                              bool arrowHeadBottom }) {
  return document.body.append(new Element.div()
    ..className = "verticalLine straight"
    ..style.left = "${pxToEm(x)}em"
    ..style.top = "${pxToEm(y1)}em"
    ..style.height = "${pxToEm(y2-y1)}em"
    ..style.backgroundColor = color
  );
}


drawStraightLineUpCoords({double x, double y1, double y2,
                           String color="black"}) {
  document.body.append(new Element.div()
    ..className = "verticalLine straight"
    ..style.left = "${pxToEm(x)}em"
    ..style.top = "${pxToEm(y2)}em"
    ..style.height = "${pxToEm(y1-y2)}em"
    ..style.backgroundColor = color
  );
}

drawStraightLineDownEl(Element pin1, Element pin2, [String color = "black"]) {
  return document.body.append(new Element.div()
                      ..className = "verticalLine straight"
                      ..style.left = "${pxToEm(pin1.offsetTo(document.body).x)}em"
                      ..style.top = "${pxToEm(pin1.offsetTo(document.body).y)}em"
                      ..style.height = "${pxToEm(pin2.offsetTo(document.body).y-pin1.offsetTo(document.body).y
                                                          +pin2.offsetHeight)}em"
                      ..style.backgroundColor = color

  );
}

drawDashedLineDownEl(Element pin1, Element pin2, [String color = "black"]) {
  return document.body.append(new Element.div()
                      ..className = "verticalLine dashed"
                      ..style.left = "${pxToEm(pin1.offsetTo(document.body).x)}em"
                      ..style.top = "${pxToEm(pin1.offsetTo(document.body).y)}em"
                      ..style.height = "${pxToEm(pin2.offsetTo(document.body).y-pin1.offsetTo(document.body).y
                                                          +pin2.offsetHeight)}em"
                      ..style.borderColor = color
  );
}

setCopiedAttrPos(Element copiedSelectPart, Element originPart) {
  copiedSelectPart.style.left =
  "${pxToEm(originPart.offsetTo(firstCommonParent(copiedSelectPart, originPart)).x
//      "${pxToEm(originPart.offsetTo(copiedSelectPart.parent.parent).x

      //)}em";
    +(originPart.offsetWidth - copiedSelectPart.offsetWidth))}em";

  //"${pxToEm(copiedSelectPart.offsetTo(document.body).x * (-1))}";
}


firstCommonParent(Element e1, Element e2) {
  for(Element parent1 = e1.parent; parent1 != null; parent1 = parent1.parent) {
    for(Element parent2 = e2.parent; parent2 != null; parent2 = parent2.parent) {
      if(parent1 == parent2) {
        return parent1;
      }
    }
  }
  return null;
}



double pxToEm(double px) => px/FONT_SIZE;