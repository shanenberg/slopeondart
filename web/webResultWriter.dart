import 'dart:io';

import 'package:path/path.dart';

void createWebResultFolderIfNotExists(Directory projectDir) {
  String projectName = basename(projectDir.path);

  Directory webResult = new Directory(projectDir.path + "\\webResult");
  if(!webResult.existsSync()) {
    webResult.createSync();
    File htmlFile = new File(webResult.path + "\\$projectName.html");
    htmlFile.createSync();
    htmlFile.writeAsStringSync(_createHtmlFileTemplate(projectName));

    File cssFile = new File(webResult.path + "\\$projectName.css");
    cssFile.createSync();

    File dartFile = new File(webResult.path + "\\$projectName.dart");
    dartFile.createSync();
    dartFile.writeAsStringSync(_createDartFileTemplate(projectName));

    File consoleDartFile = new File(webResult.path + "\\${projectName}_console.dart");
    consoleDartFile.createSync();
    consoleDartFile.writeAsStringSync(_createConsoleDartFileTemplate(projectName));
  }
}

String _createHtmlFileTemplate(String projectName)
=> '''<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <script type="application/dart" src="$projectName.dart"></script>
    <script data-pub-inline src="packages/browser/dart.js"></script>
    <link rel="stylesheet" type="text/css" href="$projectName.css" />
</head>
<body>


</body>
</html>
''';


String _createConsoleDartFileTemplate(String projectName)
=> '''import '../../../html/examples/SLOTHTMLToConsoleConverter.dart';
import '../../../../lib/slot/runtime/SLOTProject.dart';
import '$projectName.slp.dart';

main() {
  SLOTProject project =
  new SLOTHTMLToConsoleConverter().constructProject(new $projectName());
  var slotResult = project.runProject();
}
''';

String _createDartFileTemplate(String projectName)
=> '''import '../../../../lib/slot/runtime/SLOTHTMLProject.dart';
import '../../../html/examples/SLOTProjectConstructor.dart';
import '$projectName.slp.dart';

main() {

  SLOTHTMLProject project =
  new SLOTProjectConstructor().constructHTMLProject(new $projectName());
  var slotResult = project.runProject();

}
''';