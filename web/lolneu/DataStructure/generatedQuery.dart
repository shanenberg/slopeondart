import 'dart:html';

Map<Element, Object> currentMap;


main() {
  print("HI");
  currentMap = {};

  Query query = new Query(
                    new SelectClause(
                      new SelectList(
                        [
                          new SelectColumnOriginal(
                            new Column(
                              "ding", "hi"
                            )
                          )
                        ]
                      )
                    ),
                    new FromClause(
                      new TableLiteral(
                        "ding"
                      )
                    )
                );

  query.addExtras();
  document.body.append(query.draw());

}



abstract class SQLElement {
  Element element;
  Element draw() {
    element = drawElement();
    currentMap[element] = this;
    return element;
  }

  Element drawElement();
}

class Query extends SQLElement {
  SelectClause selectClause;
  FromClause fromClause;

  Query(this.selectClause, this.fromClause);

  @override
  Element drawElement() {
    return new Element.div()
            ..className="subQuery"
            ..children = [
              selectClause.draw(),
              fromClause.draw()
            ];
  }

  void addExtras() {
    fromClause.addExtrasFor(selectClause.selectList.selectColumns);
  }
}

class FromClause extends SQLElement{
  TableLiteral tableLiteral;
  FromClause(this.tableLiteral);
  @override
  Element drawElement() {
    return new DivElement()
              ..className="from clause"
              ..children = [
                tableLiteral.draw()
              ];
  }

  void addExtrasFor(List<SelectColumn> selectColumns) {
    tableLiteral.addExtrasFor(selectColumns);
  }
}

class SelectClause extends SQLElement{
  SelectList selectList;
  SelectClause(this.selectList);

  Element drawElement() {
    return new Element.div()
            ..className="select clause"
            ..children=[
                        selectList.draw(),
                        new Element.div()
                          ..className="selectArrowSpace"
                       ];
  }
}

class SelectList extends SQLElement{
  List<SelectColumn> selectColumns;
  SelectList(this.selectColumns);
  @override
  Element drawElement() {
    return new Element.div()
        ..className = "selectList"
        ..children = new List.generate(
                                selectColumns.length,
                                (int index) {
                                  return selectColumns[index].draw();
                                }
        );
  }
}

abstract class SelectColumn extends SQLElement {
  SQLElement selectReference;
  Column column;
  SelectColumn(this.column);
}

class SelectColumnOriginal extends SelectColumn{
  SelectColumnOriginal(Column column) : super(column);
  @override
  Element drawElement() {
    return new Element.div()
          ..className = "selectColumn original"
          ..append(new Element.div()..className="originalColumn"
                    ..text = column.columnName);
  }
}

class SelectColumnRenamed extends SelectColumn {
  String as;
  SelectColumnRenamed(Column column, this.as) : super(column);



  @override
  Element drawElement() {
    return new Element.div()
            ..className="selectColumnRenamed"
            ..children = [
                new Element.div()
                  ..className="renamedColumn"
                  ..text=as,
                new Element.div()
                  ..className="as"
                  ..nodes = [
                       new Text("↑"),
                       new DivElement()
                        ..className="asWordContainer"
                        ..append(
                          new DivElement()
                              ..className="asWord"
                              ..text="as"
                        )

                  ],
              new DivElement()
                ..className="originalColumn"
                ..text = column.columnName
            ];
  }
}


class Column {
  String tableName;
  String columnName;
  Column(this.tableName, this.columnName);
}



class TableLiteral extends SQLElement{

  Element element;

  String tableName;
  TableLiteral(this.tableName);

  List<SelectColumn> selectedBy;

  Element drawElement() {
    return new Element.div()
              ..className = "tableLiteral"
              ..text = tableName;
  }

  void addExtrasFor(List<SelectColumn> selectColumns) {
    for(SelectColumn selectColumn in selectColumns) {
        selectColumn.column.tableName;

    }
  }

}