import 'dart:html';
import 'pg.dart';



drawPastaPhysicsLines({Element root = null}) {


  currentSeed = niceSeeds[1];
  if(root == null)
  currentRoot = querySelector("#pastaPhysicsJoin");
  else
    currentRoot = root;

  ColorScale colorScale = new ColorScale.scheme20(shuffled: true, seed: currentSeed);
  currentColorScale = colorScale;

  Element tableNegotiation = currentRoot.querySelector("#negotiation");
  var dimNegotiation = dims(tableNegotiation);

  print(dimNegotiation.x);
  print(dimNegotiation.y);

  //List<double> arrowStartPoints = splitLineIntoCenterPoints(dimNegotiation.width/3, 2);
  List<double> arrowStartPoints = splitLineIntoCenterPoints(dimNegotiation.width, 2);

  Element colPasta = currentRoot.querySelector("#pasta");
  var dimPasta = dims(colPasta);

  String pastaColor = colorScale.nextColor();
  print(pastaColor);
  drawDuoLineUpRightCoords(
      x1: arrowStartPoints[0] + dimNegotiation.x,
      y1: dimNegotiation.y,
      x2: dimPasta.x,
      y2: dimPasta.y + dimPasta.height/2,
      color: pastaColor,
      arrowHeadTopRight: true
  );


  Element colPhysics = currentRoot.querySelector("#physics");
  var dimPhysics = dims(colPhysics);

  String colorPhysics = colorScale.nextColor();
  double physics1X1 = arrowStartPoints[1]+dimNegotiation.x;
  double physics1Y1 = dimNegotiation.y;

  drawDuoLineUpRightCoords(
      x1: physics1X1,
      y1: physics1Y1,
      x2: dimPhysics.x,
      y2: dimPhysics.y + dimPhysics.height/2,
      color: colorPhysics,
      arrowHeadTopRight: true
  );



  Dimensions monstrous = dims(currentRoot.querySelector("#monstrous"));

  Dimensions thesis1 = dims(currentRoot.querySelector("#thesis1"));
  String colorThesis1 = colorScale.nextColor();

  Dimensions dangerous1 = dims(currentRoot.querySelector("#dangerous1"));
  String colorDangerous1 = colorScale.nextColor();


  List<double> monstrousXOffsets = splitLineIntoCenterPoints(monstrous.width, 4);



  double dangerous1X1 = monstrous.x + monstrousXOffsets[0];
  double dangerous1Y1 = monstrous.y;

  drawDuoLineUpLeftCoords(
      x1: dangerous1X1,
      y1: dangerous1Y1,
      x2: dangerous1.x+dangerous1.width,
      y2: dangerous1.y + dangerous1.height/2,
      color: colorDangerous1,
      arrowHeadTopLeft: true
  );

  drawDuoLineUpLeftCoords(
      x1: monstrous.x + monstrousXOffsets[1],
      y1: monstrous.y,
      x2: thesis1.x+thesis1.width,
      y2: thesis1.y + thesis1.height/2,
      color: colorThesis1,
      arrowHeadTopLeft: true
  );

  Dimensions performer = dims(currentRoot.querySelector("#performer"));
  Dimensions theater = dims(currentRoot.querySelector("#theater"));
  Dimensions seat = dims(currentRoot.querySelector("#seat"));

  List<double> performerOffsets = splitLineIntoCenterPoints(performer.width, 2);

  drawDuoLineUpLeftCoords(
      x1: performer.x + performerOffsets[0],
      y1: performer.y,
      x2: seat.x+seat.width+2,
      y2: seat.y + seat.height/2,
      color: colorScale.nextColor(),
      arrowHeadTopLeft: true
  );


  drawDuoLineUpLeftCoords(
      x1: performer.x + performerOffsets[1],
      y1: performer.y,
      x2: theater.x+theater.width+2,
      y2: theater.y + theater.height/2,
      color: colorScale.nextColor(),
      arrowHeadTopLeft: true
  );



  dimNegotiation;
  physics1X1;
  physics1Y1;
  colorPhysics;

  Dimensions physics2 = dims(currentRoot.querySelector("#physics2"));
  String colorPhysics2 = colorPhysics;

  Dimensions space = dims(currentRoot.querySelector("#verticalArrowSpace"));
  List<double> spacePoints = splitLineIntoCenterPoints(space.height, 2)
      .reversed.toList();

  monstrous;


  drawQuatroArrowTopRight(
      x1: physics1X1,
      y1: physics1Y1,
      x2: physics2.x,
      y2: physics2.y+physics2.height/2,
      firstHeight: spacePoints[0],
      secondWidth: monstrous.x + monstrousXOffsets[3] - physics1X1,
      color: colorPhysics,
      arrowHeadTopRight: true
  );





  Dimensions dangerous2 = dims(currentRoot.querySelector("#dangerous2"));
  String colorDangerous2 = colorDangerous1;

  dangerous2;
  physics2;

  dangerous1X1;
  dangerous1Y1;

  drawQuatroArrowTopRight(
      x1: dangerous1X1,
      y1: dangerous1Y1,
      x2: dangerous2.x,
      y2: dangerous2.y+dangerous2.height/2,
      firstHeight: spacePoints[1],
      secondWidth: monstrous.x + monstrousXOffsets[2] - dangerous1X1,
      color: colorDangerous1,
      arrowHeadTopRight: true
  );
}