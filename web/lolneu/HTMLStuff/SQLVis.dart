import 'SQL.dart';
import 'dart:html';

main() {
  VisualizationMapper mapper = new VisualizationMapper( {
    TableLiteral : (TableLiteral tl) => new TableLiteralVisualization(tl)
  });

  ElementVisualization visualization = mapper.createVisualization(new TableLiteral("hi"));
  visualization.draw();
  document.body.append(visualization.htmlElement);

}


class VisualizationMapper {
  Map<Type, Function> map;

  VisualizationMapper(this.map);

  createVisualization(Object object) {
    print(map[object.runtimeType].runtimeType.toString());

    return map[object.runtimeType](object);
  }
}


abstract class ElementVisualization<T> {

  T dataElement;
  Element htmlElement;

  ElementVisualization(this.dataElement, [this.htmlElement]);

  draw();

}


class TableLiteralVisualization extends ElementVisualization<TableLiteral> {

  TableLiteralVisualization(TableLiteral dataElement) : super(dataElement);

  @override
  draw() {
    htmlElement = new Element.div()..className="tableLiteral"
                    ..text = dataElement.tableName;
    return htmlElement;
  }
}

