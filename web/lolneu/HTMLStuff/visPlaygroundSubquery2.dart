import 'dart:html';
import 'exampleLines.dart';
import 'pg.dart';
import 'dart:math' as Math;
import 'visPlaygroundSubquery.dart';

main() {
  print("hi") ;
  initLineDrawer();

  drawPlaygroundSubquery();

  ColorScale scale = new ColorScale.scheme20(shuffled: true, seed: currentSeed);

  Dimensions chicken= select("#chicken");
  Dimensions equalsWings = select("#equalsWings");

  drawDuoLineUpRightCoords(
    x1: chicken.widthCenterX,
    y1: chicken.y,
    x2: equalsWings.x,
    y2: equalsWings.heightCenterCoord,
    color: scale.nextColor(),
    arrowHeadTopRight: true
  );


  Dimensions selectThesisAsExpert = select("#selectThesisAsExpert");
  Dimensions equalsExpert = select("#equalsExpert");

  drawDuoLineUpLeftCoords(
      x1: selectThesisAsExpert.widthCenterX,
      y1: selectThesisAsExpert.y,
      x2: equalsExpert.rightX,
      y2: equalsExpert.heightCenterCoord,
      color: scale.nextColor(),
      arrowHeadTopLeft: true
  );


  Dimensions selectWings = select("#selectWings");
  var selectArrowSpace2 = select("#selectArrowSpace2").splitHeightCoords(2);

  drawTrioLineUp(
    x1: chicken.widthCenterX,
    y1: chicken.y,
    x2: selectWings.widthCenterX,
    y2: selectWings.bottomY,
    knickY: selectArrowSpace2[0],
    color: scale.scale[0],
    arrowHeadTop: true
  );

  Dimensions selectExpert = select("#selectExpert");

  drawTrioLineUp(
    x1: selectThesisAsExpert.widthCenterX,
    y1: selectThesisAsExpert.y,
    x2: selectExpert.widthCenterX,
    y2: selectExpert.bottomY,
    knickY: selectArrowSpace2[1],
    color: scale.scale[1],
    arrowHeadTop: true
  );




  /*window.onResize.listen((Event ev) {



    querySelectorAll(".verticalLine,.horizontalLine,.topArrowHead,.rightArrowHead,.leftArrowHead,.bottomArrowHead").forEach((Element el) {
      el.remove();
    });

    var currentZoomLevel = window.devicePixelRatio * 100;

    //if(currentZoomLevel > 40) {
      drawEverything();
    //}

  });*/





}


