import 'dart:math';


main() {

  SfwBlock bl2 = new SfwBlock(
                    new SelectClause(
                      new SelectList(
                        [
                          new SelectColumn(
                            new Column("tb1", "col1"), "Chicken"
                          )

                        ]
                      )
                    ),
                    new FromClause(
                      new CrossJoin(
                        new TableLiteral("tb1"),
                        new TableLiteral("tb2")
                      )
                    )
  );

  new CrossJoin(
    new TableLiteral("tb1"),
    new TableLiteral("tb2")
  );

}

Random random;

class SfwBlock{
  SelectClause selectClause;
  FromClause fromClause;

  SfwBlock([this.selectClause, this.fromClause]);
}

class FromClause {
  TableExpression tableExpression;

  FromClause([this.tableExpression]);
}



class SelectClause {
  SelectList selectList;
  SelectClause([this.selectList]);
}

class SelectList {
  List<SelectColumn> selectColumns;
  SelectList([this.selectColumns]);
}

class SelectColumn {
  Expression selectExpr;
  bool renamed;
  String as;


  SelectColumn([this.selectExpr, this.as]);
}




abstract class Expression {

}

class Column extends Expression{
  String tableName;
  String columnName;

  Column([this.tableName, this.columnName]);
  Object targetExpression;
}

abstract class TableExpression {

}

class TableSubquery extends TableExpression{
  SfwBlock sfwBlock;
  String renamedAs;
  TableSubquery([this.sfwBlock, this.renamedAs]);
}

class TableLiteral extends TableExpression{
  String tableName;
  List<String> generatedColumns;

  TableLiteral(this.tableName, [this.generatedColumns]);
  TableLiteral.generate();
}


abstract class Join extends TableExpression {
  TableExpression expr1;
  TableExpression expr2;

  Join([this.expr1, this.expr2]);
}

class CrossJoin extends Join {
  CrossJoin([TableExpression expr1, TableExpression expr2])
      : super(expr1, expr2);
}

class OnJoin extends Join {
  List<Comparison> onConditions;

  OnJoin([TableExpression expr1, TableExpression expr2, this.onConditions])
      : super(expr1, expr2);
}

class Comparison {
  Column col1;
  Column col2;
  ComparisonType comparisonType;

  Comparison([this.col1, this.col2, this.comparisonType]);
}

enum ComparisonType {
  EQUAL,
  UNEQUAL
}