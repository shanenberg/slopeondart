import 'dart:html';
import 'dart:math';
import 'exampleLines.dart';


List<String> colorScale10 = [
  "#1776B6",
  "#FF7F0E",
  "#2CA02C",
  "#1F77B4",
  "#D62728",
  "#9467BD",
  "#8C564B",
  "#E377C2",
  "#7F7F7F",
  "#BCBD22",
  "#17BECF"];


List<String> colorScale20 = [
  "#1F77B4",
  "#AEC7E8",
  "#FF7F0E",
  "#FFBB78",
  "#2CA02C",
  "#98DF8A",
  "#D62728",
  "#FF9896",
  "#9467BD",
  "#C5B0D5",
  "#8C564B",
  "#C49C94",
  "#E377C2",
  "#F7B6D2",
  "#7F7F7F",
  "#C7C7C7",
  "#BCBD22",
  "#DBDB8D",
  "#17BECF",
  "#9EDAE5"
];

List<int> niceSeeds = [
  1533471056906,
  1533569758261,
  1534098427549
];

int currentSeed = null;

ColorScale currentColorScale = null;

double FONT_SIZE;
Element currentRoot;


class ColorScale {
  int currentColor = 0;
  List<String> scale;

  ColorScale.scheme10({bool shuffled = false, int seed}) {
    if(!shuffled)
      scale = colorScale10;
    else {
      if(seed == null) {
        seed = new DateTime.now().millisecondsSinceEpoch;
      }
      print(seed);

      scale = new List.from(colorScale10)
        ..shuffle(new Random(seed));
    }
  }

  ColorScale.scheme20({bool shuffled = false, int seed}) {
    if(!shuffled)
      scale = colorScale20;
    else {
      if(seed == null) {
        seed = new DateTime.now().millisecondsSinceEpoch;
      }
      print(seed);

      scale = new List.from(colorScale20)
        ..shuffle(new Random(seed));
    }
  }

  nextColor() => scale[ (currentColor++) % scale.length];
  reset() => currentColor = 0;
}

Dimensions select(String selector) {
  return dims(currentRoot.querySelector(selector));
}

initLineDrawer() {
  currentRoot = document.documentElement;
  String fontSizeStr = currentRoot.getComputedStyle()
      .fontSize;
  FONT_SIZE = double.parse(fontSizeStr.substring(0, fontSizeStr.length-2));

  querySelectorAll("[id]").forEach((Element element) {
    element.onMouseEnter.listen((event) {

      if(element.id.endsWith("_idElement")
       ||element.id== null)
        return;

      Element idElement = new Element.div()
          ..text = element.id;
      idElement.style.position="absolute";
      idElement.id = element.id+"_idElement";

      Point p = element.offsetTo(document.body);

      idElement.style.position="absolute";
      idElement.style.pointerEvents ="none";
      idElement.style.left=pxToEmString(p.x);
      idElement.style.top=pxToEmString(p.y);
      idElement.style.color="red";

      document.body.append(idElement);

    });

    element.onMouseOut.listen((event) {
      Element idElement = querySelector("#${element.id}_idElement");
      if(idElement != null) {
        idElement.remove();
      }
    });
  });

}




main() {
  print("helloas");
  initLineDrawer();
  drawPastaPhysicsLines(root:document.body);

}



/*
Dimensions dims(Element el) {
  return new Dimensions(
      offset(el).left,
    offset(el).top,
    el.offsetWidth as double,
    el.offsetHeight as double
  );
}
*/


class Position {
  num left, top;
  Position(this.left, this.top);
}

Position offset(Element elem) {
  final docElem = document.documentElement;
  final box = elem.getBoundingClientRect();
  return new Position(box.left + window.pageXOffset - docElem.clientLeft,
      box.top  + window.pageYOffset - docElem.clientTop);
}


Dimensions dims(Element el) {
  Point p = el.offsetTo(currentRoot);

  double realYOffset = p.y > 1000 ? 0.031 : 0.029;


  double realXOffset = 0.018;
  return new Dimensions(
    p.x + p.y*realXOffset,
    p.y + p.y*realYOffset,
      el.offsetWidth as double,
    el.offsetHeight as double,
    el
  );
}




class Dimensions {
  double x;
  double y;
  double width;
  double height;
  Element element;

  Dimensions(this.x, this.y, this.width, this.height, [this.element]);


  double get bottomY => y+height;
  double get rightX => x+width;


  double get widthCenterX => splitWidthCoords(1)[0];

  double get heightCenterCoord => splitHeightCoords(1)[0];

  List<double> splitWidthCoords(int parts) {
    List<double> split = splitLineIntoCenterPoints(width, parts);
    for(int i = 0; i< split.length; i++)
      split[i]+=x;
    return split;
  }

  List<double> splitHeightCoords(int parts) {
    List<double> split = splitLineIntoCenterPoints(height, parts);
    for(int i = 0; i< split.length; i++)
      split[i]+=y;
    return split;
  }



  List<double> splitWidth(int parts) {
    return splitLineIntoCenterPoints(width, parts);
  }

  List<double> splitHeight(int parts) {
    return splitLineIntoCenterPoints(height, parts);
  }


}

List<double> splitLineIntoCenterPoints(double length, int parts) {
  List ret = [];

  double partWidth = length/parts;

  double currentX = partWidth/2;

  for(int i = 0; i< parts; i++, currentX+=partWidth)
    ret.add(currentX);


  return ret;

}






drawTrioLineUp({double x1, double y1,
double x2, double y2,
double knickY,
String color = "black",
bool arrowHeadTop = false, bool arrowHeadBottom = false}) {


  if(x1 < x2) {
    drawTrioLineUpRightUp(
      x1: x1,
      y1: y1,
      x2: x2,
      y2: y2,
      knickY: knickY,
      color: color,
      arrowHeadTop: arrowHeadTop,
      arrowHeadBottom: arrowHeadBottom
    );
  }
  else {
    drawTrioLineUpLeftUp(
        x1: x1,
        y1: y1,
        x2: x2,
        y2: y2,
        knickY: knickY,
        color: color,
        arrowHeadTop: arrowHeadTop,
        arrowHeadBottom: arrowHeadBottom
    );
  }

}


drawTrioLineUpRightUp(
    {double x1, double y1,
    double x2, double y2,
    double knickY,
    String color = "black",
    bool arrowHeadTop = false, bool arrowHeadBottom = false}) {

  drawStraightLineUpCoords(
      x:  x1,
      y1: y1,
      y2: knickY,
      color: color,
      arrowHeadBottom: arrowHeadBottom
  );

  drawStraightLineRightCoords(
      y: knickY,
      x1: x1,
      x2: x2+0.15*FONT_SIZE,
      color: color
  );

  drawStraightLineUpCoords(
      x: x2,
      y1: knickY,
      y2: y2,
      color: color,
      arrowHeadTop: arrowHeadTop
  );



}

drawTrioLineUpLeftUp({double x1, double y1,
                      double x2, double y2,
                      double knickY,
                      String color = "black",
                      bool arrowHeadTop = false, bool arrowHeadBottom = false}) {

  drawStraightLineUpCoords(
    x:  x1,
    y1: y1,
    y2: knickY,
    color: color,
    arrowHeadBottom: arrowHeadBottom
  );

  drawStraightLineLeftCoords(
    y: knickY,
    x1: x1,
    x2: x2,
    color: color
  );

  drawStraightLineUpCoords(
      x: x2,
      y1: knickY,
      y2: y2,
      color: color,
      arrowHeadTop: arrowHeadTop
  );

}


drawQuatroArrowTopRight({double x1, double y1,
                          double x2, double y2,
                          double firstHeight,
                          double secondWidth,
                          String color = "black",
                          bool arrowHeadTopRight = false, bool arrowHeadBottom = false}) {

  drawStraightLineUpCoords(
    x: x1,
    y1: y1,
    y2: y1-firstHeight,
    color: color,
    arrowHeadBottom: arrowHeadBottom
  );

  drawStraightLineRightCoords(
    y: y1-firstHeight,
    x1: x1,
    x2: x1+secondWidth,
    color: color
  );

  drawStraightLineUpCoords(
    x: x1+secondWidth-2,
    y1: y1-firstHeight,
    y2: y2,
    color: color
  );

  drawStraightLineRightCoords(
    y: y2,
    x1: x1+secondWidth,
    x2: x2,
    color: color,
    arrowHeadRight: arrowHeadTopRight
  );

}



drawDuoLineUpLeftCoords({double x1, double y1, double x2, double y2, String color = "black",
bool arrowHeadTopLeft = false, bool arrowHeadBottom = false}) {
  drawStraightLineUpCoords(
      x: x1,
      y1: y1,
      y2: y2,
      color: color,
      arrowHeadBottom: arrowHeadBottom
  );


  drawStraightLineLeftCoords(
      y: y2,
      x1: x1,
      x2: x2,
      color: color,
      arrowHeadLeft: arrowHeadTopLeft
  );


}


drawDuoLineUpRightCoords({double x1, double y1, double x2, double y2, String color = "black",
                            bool arrowHeadTopRight = false, bool arrowHeadBottom = false}) {

  drawStraightLineUpCoords(
    x: x1,
    y1: y1,
    y2: y2,
    color: color,
    arrowHeadBottom: arrowHeadBottom
  );


  drawStraightLineRightCoords(
    y: y2,
    x1: x1,
    x2: x2,
    color: color,
    arrowHeadRight: arrowHeadTopRight
  );

}




drawStraightLineRightCoords({double y, double x1, double x2, String color = "black",
                              bool arrowHeadLeft = false, bool arrowHeadRight = false}) {
  Element element = currentRoot.append(new Element.div()
    ..className = "horizontalLine straight"
    ..style.left = "${pxToEm(x1)}em"
    ..style.top = "${pxToEm(y)}em"
    ..style.width = pxToEmString(x2-x1)
    ..style.backgroundColor = color);

  drawHorizontalArrowHeads(element, arrowHeadLeft: arrowHeadLeft, arrowHeadRight: arrowHeadRight);
}

drawStraightLineLeftCoords({double y, double x1, double x2, String color = "black",
                             bool arrowHeadLeft = false,
                             bool arrowHeadRight = false}) {
  Element element = currentRoot.append(new Element.div()
    ..className = "horizontalLine straight"
    ..style.left = "${pxToEm(x2)}em"
    ..style.top = "${pxToEm(y)}em"
    ..style.width = pxToEmString(x1-x2)
    ..style.backgroundColor = color);

  drawHorizontalArrowHeads(element, arrowHeadRight:  arrowHeadRight, arrowHeadLeft: arrowHeadLeft);
}


drawStraightLineDownCoords({double x, double y1, double y2, String color = "black",
                             bool arrowHeadBottom = false, bool arrowHeadTop = false }) {
  Element element = currentRoot.append(new Element.div()
    ..className = "verticalLine straight"
    ..style.left = "${x}px"
    ..style.top = "${y1}px"
    ..style.height = "${y2-y1}px"
    ..style.backgroundColor = color
  );

  drawVerticalArrowHeads(element, arrowHeadBottom: arrowHeadBottom, arrowHeadTop: arrowHeadTop);

}


//em variant
drawStraightLineUpCoords({double x, double y1, double y2,
                           String color="black", bool arrowHeadBottom = false, bool arrowHeadTop = false}) {
  Element element = currentRoot.append(new Element.div()
    ..className = "verticalLine straight"
    ..style.left = "${pxToEm(x)}em"
    ..style.top = "${pxToEm(y2)}em"
    ..style.height = "${pxToEm(y1-y2)}em"
    ..style.backgroundColor = color
  );

  drawVerticalArrowHeads(element, arrowHeadBottom: arrowHeadBottom, arrowHeadTop: arrowHeadTop);

}


//px variant
/*
drawStraightLineUpCoords({double x, double y1, double y2,
                           String color="black", bool arrowHeadBottom = false, bool arrowHeadTop = false}) {
  Element element = currentRoot.append(new Element.div()
    ..className = "verticalLine straight"
    ..style.left = "${x}px"
    ..style.top = "${y2}px"
    ..style.height = "${y1-y2}px"
    ..style.backgroundColor = color
  );

  drawVerticalArrowHeads(element, arrowHeadBottom: arrowHeadBottom, arrowHeadTop: arrowHeadTop);

}
*/


drawHorizontalArrowHeads(Element element, {arrowHeadLeft = false, arrowHeadRight = false}) {
  if(arrowHeadLeft) {
    element.append(new Element.div()
      ..className = "leftArrowHead top");
    element.append(new Element.div()
      ..className = "leftArrowHead bottom");

  }
  if(arrowHeadRight) {
    element.append(new Element.div()
      ..className = "rightArrowHead top");
    element.append(new Element.div()
      ..className = "rightArrowHead bottom");

  }
}

drawVerticalArrowHeads(Element element, {arrowHeadTop = false, arrowHeadBottom = false}) {
  if(arrowHeadTop) {
    element.append(new Element.div()
      ..className = "topArrowHead left");
    element.append(new Element.div()
      ..className = "topArrowHead right");

  }
  if(arrowHeadBottom) {
    element.append(new Element.div()
      ..className = "bottomArrowHead left");
    element.append(new Element.div()
      ..className = "bottomArrowHead right");

  }
}



double pxToEm(double px) => px/FONT_SIZE;

String pxToEmString(double px) {
  return "${pxToEm(px)}em";
}