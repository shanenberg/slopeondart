import 'dart:html';
import 'exampleLines.dart';
import 'pg.dart';
import 'dart:math' as Math;

main() {
  print("hi") ;
  initLineDrawer();

  drawPlaygroundSubquery();

  /*window.onResize.listen((Event ev) {



    querySelectorAll(".verticalLine,.horizontalLine,.topArrowHead,.rightArrowHead,.leftArrowHead,.bottomArrowHead").forEach((Element el) {
      el.remove();
    });

    var currentZoomLevel = window.devicePixelRatio * 100;

    //if(currentZoomLevel > 40) {
      drawEverything();
    //}

  });*/





}


drawPlaygroundSubquery() {

  currentRoot = querySelector("#currentSQ1");
  drawPastaPhysicsLines(root: querySelector("#currentSQ1"));

  Dimensions selectArrowSpace = select("#selectArrowSpace1");

  Dimensions monstrous = select("#monstrous");
  print("monstrousY = ${monstrous.element.offsetTo(currentRoot).y}");

  
  List<double> splitMonstrousWidthCoords = monstrous.splitWidthCoords(4);

  Dimensions selectThesisAsExpert = select("#selectThesisAsExpert");

  List<double> splitSelectArrowHeightCoords = selectArrowSpace.splitHeightCoords(2);




  String selectThesisAsExpertColor = currentColorScale.scale[2];
  double selThesisCenter = selectThesisAsExpert.widthCenterX;
  double monstrousArrowStart = splitMonstrousWidthCoords[1];
  double selThesisKnick = splitSelectArrowHeightCoords[0];



  drawTrioLineUpLeftUp(
      x1: monstrousArrowStart,
      y1: monstrous.y,
      x2: selThesisCenter,
      y2: selectThesisAsExpert.bottomY,
      knickY: selThesisKnick,
      color: selectThesisAsExpertColor,
      arrowHeadTop: true
  );




  /*
  Table: performer (2)
  Column: selectTheater (!)
   */


  Dimensions tablePerformer = select("#performer");
  Dimensions selTheater = select("#selectTheater");


  drawTrioLineUpLeftUp(
    x1: tablePerformer.splitWidthCoords(2)[1],
    y1: tablePerformer.y,
    x2: selTheater.widthCenterX,
    y2: selTheater.bottomY,
    knickY: splitSelectArrowHeightCoords[1],
    color: currentColorScale.scale[5],
    arrowHeadTop: true
  );

}
