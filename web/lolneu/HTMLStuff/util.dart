import 'dart:math';

import 'package:english_words/english_words.dart';

String randomWord(Random random) => all[random.nextInt(all.length)];
String randomNoun(Random random) => nouns[random.nextInt(nouns.length)];
String randomAdjective(Random random) => adjectives[random.nextInt(adjectives.length)];


List takeRandomElementsFromList(List list, int elNum) {
  List<int> indexList = createIndexList(list);

}

List<int> createIndexList(List list) {
  List<int> ret = [];
  for(int i = 0; i<list.length; i++)
    ret.add(i);
  return ret;
}



int randomIntFromRange(int min, int max, Random random) {
  return min + random.nextInt(max - min + 1);
}