import '../../../html/examples/SLOTHTMLToConsoleConverter.dart';
import 'explorationPlayground.slp.dart';
import '../../../../lib/slot/runtime/SLOTProject.dart';

main() {

  SLOTProject project =
  new SLOTHTMLToConsoleConverter().constructProject(new explorationPlayground());
  var slotResult = project.runProject();

}

