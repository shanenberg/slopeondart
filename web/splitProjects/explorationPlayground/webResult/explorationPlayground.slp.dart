/***********************************************************
I am purely generated!!!! Don't touch me, touch the slp files 
***************************************************************/
      import '../../../html/examples/HTMLExample.dart';
class explorationPlayground extends HTMLExample {
   List<String> getExampleProjectStrings() {
  List<String> exampleStrings = new List<String>();
exampleStrings.add('''


Reggae
Rules: start
start => "[a-g]"+ .


''');
exampleStrings.add('''


load library util;
load library htmlConstruction;



var htmlString;
var nodesToHTML;
var printOffset;
var lastButton;

var cursor;
var cursorIndexPath;



var dffText;



var regexWordGrammarString;




constructActiveDocument slopeTree {
    var root = #"ROOT";
    ->(_constructActiveDocument)([slopeTree], root);


    var current = root;

    forEach(index in cursorIndexPath) {

        current = ->(getActiveDocumentNode)([current], rootResultNode, [index]);
    };

    cursor = current;

    return root;
}

setCursorToNode actionsNode {
    cursor = actionsNode;
    clearElements(cursorIndexPath);
    var current = actionsNode;
    while(not(equals(parent(current), null))) {
        setElementAt(cursorIndexPath, indexInParent(current), 1);
        current = parent(current);
    };
}


findNextActionIndex cursor {
    var theParent = parent(cursor);

    var firstIndex = indexInParent(cursor);
    var index = plus(indexInParent(cursor),1);
    while(smallerEqualsThan(index, length(children(theParent)))) {
        var theChild = ->(getActiveDocumentNode)([theParent], rootResultNode, [index]);
        if(equals(slotNodeName(theChild), "ACTIONS")) {
            return index;
        };
        index = plus(index, 1);
    };
    return firstIndex;
}

findPreviousActionIndex cursor {
    var theParent = parent(cursor);
    var firstIndex = indexInParent(cursor);
    var index = minus(indexInParent(cursor),1);
    while(largerThan(index, 0)) {
        var theChild = ->(getActiveDocumentNode)([theParent], rootResultNode, [index]);
        if(equals(slotNodeName(theChild), "ACTIONS")) {
            return index;
        };
        index = minus(index, 1);
    };
    return firstIndex;
}




constructActiveDocumentHTMLTree activeDocument {
    var root = #"ROOT";
    ->(_constructActiveDocumentHTMLTree
      |_constructActiveDocumentHTMLTree_Focus)
      ([activeDocument, slotOriginNode(activeDocument)], root);
    return root;
}

isNFOLDNoChildren slopeNode {
    if(and(equals(length(children(slopeNode)), 0),
           equals(slotNodeName(slopeNode), "NFOLD")
          )
      ) {
        printString("found NFOLD no Children");
        return true;
    }
    else {
        return false;
    };
}

isNFOLD_1_First slopeNode actionChild {
    if(and(isNFOLD_1(slopeNode),
           equals(indexInParent(actionChild), 1)
          )
      ) {
        return true;
    }
    else {
        return false;
    };
}

createButtonAdd {
    return #"buttonAdd" [
                +#"buttonAddLetter" {
                    +#"TEXT" {
                        +#"+";
                    };
                };
            ];
}

createButtonDelete {
    return #"buttonDelete" [
                +#"buttonDeleteLetter" {
                    +#"TEXT" {
                        +#"-";
                    };
                };
            ];
}

createButtonInfo {
    return #"buttonInfo" [
                +#"buttonInfoLetter" {
                    +#"TEXT" {
                        +#"i";
                    };
                };
            ];
}

createButtonCloseInfo {
    return #"buttonCloseInfo" [
                +#"buttonCloseInfoLetter" {
                    +#"TEXT" {
                        +#"x";
                    };
                };
            ];
}

createButtonSwitch {
    return #"buttonSwitch" [
                +#"buttonSwitchLetter" {
                    +#"TEXT" {
                        +#"&darr;";
                    };
                };
            ];
}



handleEvent anEventTreeNode anID anHTMLObject {
    if (isNotNull(anID)) {
        var slotTreeNode = elementAt(nodesToHTML, anID);
        ->(doMouseHandleEvent)([slotTreeNode, anEventTreeNode], rootResultNode, [anHTMLObject]);
    } else {
        ->(doKeyboardHandleEvent)([anEventTreeNode], rootResultNode, [anHTMLObject]);
    };
}

handleKeyboardEvent anEvent {
    printString("guten Tag");
    var eventNode = #"KEYBOARDEVENT" [
                        +#"KEY" {
                            +#htmlAtt(anEvent, "key");
                        };
                    ];
    ->(doKeyboardHandleEvent | doKeyboardEventFromCursor)([eventNode], rootResultNode, [anEvent]);
}


IS_NUMBER node {
    if(equalsAny(intToString(slotNodeName(node)),
        ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"])) {
        return true;
    }
    else {
        return false;
    };
}



addClass resultNode className {
    setNodeName(resultNode,
                    concatAll(slotNodeName(resultNode),
                              [" ", className]
                             )
               );
}

constructHTMLString aNode aNodeList {
    htmlString = "";
    ->(_constructHTMLString)([aNode], rootResultNode, [aNodeList]);
    return htmlString;
}

startOpenTag aNode aNodeList {

    htmlString = concatAll(htmlString, ["<",
                                      slotNodeName(aNode),
                                      " ",
                                      html_generateIDString(aNode, aNodeList)
                                      ]
                 );

}

startDivTagClass aNode aNodeList {
    htmlString = concatAll(htmlString, ["<div class='",
                                      slotNodeName(aNode),
                                      "' ",
                                      html_generateIDString(aNode, aNodeList)
                                      ]
                 );

}

addAttribute attName attValue {
    htmlString = concatAll(htmlString, [
                                " ",
                                attName,
                                "='",
                                attValue,
                                "'"
                           ]
                 );
}

endOpenTag {
    htmlString = concat(htmlString, ">");
}


closeTag aNode {
    htmlString = concat(htmlString, html_createCloseTagFromNode(aNode));
}

closeTagString aString {
    htmlString = concatAll(htmlString, ["</", aString, "> "]);
}

nodeListToString nodeList {
    var theString = "";
    forEach(child in children(nodeList)) {
        theString = concatAll(theString, ["#", slotNodeName(child)]);
    };
    return theString;
}




constructHTMLTree activeDocument {
    var root = #"ROOT" [
        +#"regexEditor" {
            forEach(child in children(activeDocument)) {
                ->(_constructHTMLTree)([child, slotSourceNode(child)], thisResultNode);
            };
        };
    ];
    return root;
}

makeFocusableBox grammarNode className {
   if(equals(cursor, grammarNode)) {
        return #concat(className, " focused");
   } else {
        return #className;
   };
}



main {
    cursorIndexPath = [1];
    redraw();


}


redraw {
    printString("lets go boom");
    nodesToHTML = [];
    htmlNode = newRootNode();
    htmlString = "";

    contentNode = constructActiveDocument(rootGrammarNode);

    htmlNode = constructActiveDocumentHTMLTree(contentNode);
    return constructHTMLString(htmlNode, nodesToHTML);
}






isEditable regexNode {
    return ->(t_isEditable)([regexNode], rootResultNode);
}



constructSmallerSlopeTree slopeNode {
    var root = #"";
    ->(_constructSmallerSlopeTree)([slopeNode], root);
    return root.#1;
}

isNamedAnd andTreeNode {
    if(equalsAny(andTreeNode, [
        "OR",
        "OPTIONAL",
        "BRACKET",
        "NFOLD",
        "AND",
        "NULL",
        "SLOPETOKEN",
         "INDEX"])) {
        return false;
    } else {
        return true;
    };
}

equalsAny pNode nameList {
    forEach(name in nameList) {
        if(nodeNameEquals(pNode, name)) {
            return true;
        };
    };
    return false;
}



constructDFFText aNode {
    dffText = "";
    ->(constructDFFTextTree)([aNode], thisResultNode, ["ROOT"]);
     return dffText;
}



constructRegexWordGrammarString parseTree {
    regexWordGrammarString = "";
    ->(_constructRegexWordGrammarString)(parseTree, rootResultNode);
    return regexWordGrammarString;
}

appendRegexWordGrammarString str {
    regexWordGrammarString = concat(regexWordGrammarString, str);
}



constructRuleString aNode {
    var ruleTree = #"ROOT";
    ->(constructRuleTree)([aNode], ruleTree);
    return constructDFFText(ruleTree);
}



constructTreeString node {
    var stringTree = #"";
    ->(t_constructTreeString)([node], stringTree, [0]);
    return constructDFFText(stringTree);
}



indexInParent node {
   if(equals(parent(node), null)) {
        return 0;
   } else {
        var counter = 1;
        forEach(child in children(parent(node))) {
            if(equals(child, node)) {
                return counter;
            };
            counter = plus(counter, 1);
        };
   };
}

nextNode theNode {
    var index = indexInParent(theNode);
    var theParent = parent(theNode);
    if(equals(length(children(theParent)), index)) {
        return null;
    }
    else {
        return childAt(theParent, plus(index, 1));
    };
}

pair a b {
    return #a [+#b;];
}

previousNode theNode {
    var index = indexInParent(theNode);
    var theParent = parent(theNode);
    if(equals(index, 1)) {
        return null;
    }
    else {
        return childAt(theParent, minus(index, 1));
    };
}

TreeFunction getActiveDocumentChildren

ROOT {
    return children(thisGrammarNode);
}

ACTIONS {
    return children(thisGrammarNode.#2);
}

TreeFunction getActiveDocumentNode

ROOT index {
    var child = childAt(thisGrammarNode, index);
}

ACTIONS index {
    return childAt(thisGrammarNode.#2, index);
}


TreeFunction _constructActiveDocument
OR {

    if (largerThan(length(children(thisGrammarNode.#1)),0)) {
        +#"ACTIONS"{
            +#"COMMANDS" {
                +#"SWITCH" {
                    +#"->";
                };
                +#"DELETE" {
                    +#"X";
                };
            };
            +#"TARGET" {
                thisTreeFunction([thisGrammarNode.#1], thisResultNode);
            };
        };
    } else {
        +#"ACTIONS"{
            var alt = numAlternatives(thisGrammarNode);
            var counter = 0;
            +#"COMMANDS"{
                +#"INFO" {
                    +#"i";
                };
                while(smallerThan(counter, alt)) {
                    +#"SETALTERNATIVE" {
                        +#constructRuleString(createORChildTemplateAt(thisGrammarNode, counter));
                        /*+#"%";
                        */
                        +#counter;
                    };
                    counter = plus(counter, 1);
                };
            };
            +#"TARGET"{
                thisTreeFunction([thisGrammarNode.#1], thisResultNode);
            };
        };
    };

}

OPT {
    if(not(equals("NULL", slotNodeName(thisGrammarNode.#1)))) {
        +#"ACTIONS" {
            +#"COMMANDS" {
                +#"REMOVEOPT" {
                    +#"-";
                };
            };
            +#"TARGET" {
                thisTreeFunction([thisGrammarNode.#1], thisResultNode);
            };
        };
    } else {
        +#"ACTIONS" {
            +#"COMMANDS" {
                +#"INSERTOPT" {
                    /*+#constructRuleString(thisGrammarNode);
                    */
                    +#"%";
                };
                +#"INFO" {
                    +#"i";
                };
            };
            +#"TARGET" {
                +#"NULL";
            };
        };
    };
}


/*
GROUP
B    A  Z*/

/*

(AX)*

("A" "X" ("D"|"C"))*


*/

GROUP nullz:GROUP.NULL {
    +#"ACTIONS" {
        +#"COMMANDS" {
            +#"SETCHAR" {
              +#concatAll("SET ", [slotNodeName(thisGrammarNode.#2), " - ",slotNodeName(thisGrammarNode.#3)]);
            };

        };
        +#"TARGET" {
            +#"NULL";
        };
    };
}

GROUP token:GROUP.TOKEN {
    +#"ACTIONS" {
        +#"COMMANDS" {
            +#"SETCHAR" {
                +#"+";
            };
            +#"REMOVECHAR" {
                +#"-";
            };
        };
        +#"TARGET" {
            +#"Letter" {
                +#slotNodeName(token.#1);
            };
        };
    };
}

GROUP {
    thisTreeFunction([thisGrammarNode, thisGrammarNode.#1],
                    thisResultNode);
}

GROUP.NULL {
    +#"NULL";
}

GROUP.TOKEN {
    +#"Letter" {
        +#slotNodeName(thisGrammarNode.#1);
    };
}





NFOLD {
    if(equals(length(children(thisGrammarNode)), 0)) {
        +#"ACTIONS" {
            +#"COMMANDS" {
                +#"ADDNODE" {
                    +#"+";
                    +#1;
                };
                +#"INFO" {
                    +#"i";
                };
            };
            +#"TARGET" {
                +#"EMPTY";
            };
        };
    } else {

        var counter = 1;
        forEach(child in children(thisGrammarNode)) {
            printString("adding nfold");
           +#"ACTIONS" {
                +#"COMMANDS"{
                    if(equals(counter, 1)) {
                        +#"ADDLEFT" {
                            +#"+(..)";
                            +#counter;
                        };
                    };
                    +#"REMOVENODE"  {
                        +#"-";
                        +#counter;
                    };
                    +#"ADDRIGHT" {
                        +#"(..)+";
                        +#counter;
                    };
                };
                +#"TARGET" {
                    thisTreeFunction([child], thisResultNode);
                };

           };

           counter = plus(counter, 1);

        };
    };
}

ANY_EXCEPT node:ANY_EXCEPT.NULL {
    +#"ACTIONS" {
        +#"COMMANDS" {
            +#"SETCHAR" {
                +#"+";
            };
        };
        +#"TARGET" {
            +#"NULL";
        };
    };
}

ANY_EXCEPT node:ANY_EXCEPT.ALL {
    +#"ACTIONS" {
        +#"COMMANDS" {
            +#"SETCHAR" {
                +#"+";
            };
            +#"REMOVECHAR" {
                +#"-";
            };
        };
        +#"TARGET" {
            +#"Letter" {
                +#slotNodeName(thisGrammarNode.#1.#1);
            };
        };
    };
}

ANY_EXCEPT {
    thisTreeFunction([thisGrammarNode, thisGrammarNode.#1], thisResultNode);
}


REGEX {
    thisTreeFunction([thisGrammarNode.#3], thisResultNode);
}



SLOPETOKEN {
    +#"Letter" {
        +#slotNodeName(thisGrammarNode.#1);
    };
}


NULL {
    +#"NULL";
}


ALL {
    forEach(aChild in children(thisGrammarNode)) {
        thisTreeFunction([aChild], thisResultNode);
    };
}



TreeFunction _constructActiveDocumentHTMLTree

ROOT slopeNode:ALL {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child, slotOriginNode(child)],
                          thisResultNode);

    };
}


ACTIONS slopeNode:isNFOLDNoChildren() {
    +#"element nfoldNull" {
        continue([thisGrammarNode, slopeNode], thisResultNode);

        +#"box" {
            +#"TEXT" {
                +#"&nbsp;";
            };
        };
        if(equals(cursor, thisGrammarNode)) {
            +#"rightPart" {
                ->(constructButtonActiveDocumentHTML)
                    ([childByName(thisGrammarNode.#1, "ADDNODE")],
                    thisResultNode);

                ->(constructButtonActiveDocumentHTML)
                    ([childByName(thisGrammarNode.#1, "INFO")],
                    thisResultNode);
            };
        };
    };
}

ACTIONS slopeNode:NFOLD {

    +#"element nfold" {
        /*if(isNFOLD_0(slopeNode)) {
            addClass(thisResultNode, "nfold0");
        } else {
            addClass(thisResultNode, "nfold");
        };*/

        continue([thisGrammarNode, slopeNode], thisResultNode);

        if(hasChildWithName(thisGrammarNode.#1, "ADDLEFT")) {
                +#"leftPart" {
                    ->(constructButtonActiveDocumentHTML)([childByName(thisGrammarNode.#1, "ADDLEFT")],
                                                            thisResultNode);
                };
        };

        +#"box" {
            forEach(child in children(thisGrammarNode.#2)) {
                thisTreeFunction([child, slotOriginNode(child)],
                                 thisResultNode
                                );
            };
        };
        if(equals(cursor, thisGrammarNode)) {
            +#"rightPart" {
                    ->(constructButtonActiveDocumentHTML)
                   ([childByName(thisGrammarNode.#1, "ADDRIGHT")], thisResultNode);
                    ->(constructButtonActiveDocumentHTML)
                   ([childByName(thisGrammarNode.#1, "REMOVENODE")], thisResultNode);
            };
        };
    };
}

ACTIONS slopeNode:OR {
    ->(constructActiveDocumentHTMLTree_OR
     | _constructActiveDocumentHTMLTree_Focus)
        ([thisGrammarNode, thisGrammarNode.#2.#1], thisResultNode);
}

ACTIONS slopeNode:OPT {
    ->(constructActiveDocumentHTMLTree_OPT
     | _constructActiveDocumentHTMLTree_Focus)
        ([thisGrammarNode, thisGrammarNode.#2.#1], thisResultNode);
}

ACTIONS slopeNode:GROUP {
    ->(constructActiveDocumentHTMLTree_GROUP
     | _constructActiveDocumentHTMLTree_Focus)
        ([thisGrammarNode, thisGrammarNode.#2.#1], thisResultNode);
}

ACTIONS slopeNode:ANY_EXCEPT {
    ->(constructActiveDocumentHTMLTree_ANY_EXCEPT)
        ([thisGrammarNode, thisGrammarNode.#2.#1], thisResultNode);
}



NULL slopeNode:ALL {
    +#"TEXT" {
        +#"&nbsp;";
    };
}

Letter slopeNode:SLOPETOKEN {
    +#"TEXT" {
        +#slotNodeName(slopeNode.#1);
    };
}

Letter slopeNode:ALL {
    +#"TEXT" {
        +#slotNodeName(thisGrammarNode.#1);
    };
}



TreeFunction _constructActiveDocumentHTMLTree_Focus

ACTIONS slopeNode:ALL {
    if(equals(cursor, thisGrammarNode)) {
        addClass(thisResultNode, "focus");
    };
}

TreeFunction constructActiveDocumentHTMLTree_OR

ACTIONS target:TARGET.NULL {
    +#"element or" {
        continue([thisGrammarNode, target], thisResultNode);
        +#"box" {
            +#"TEXT" {
                +#"&nbsp;";
            };
        };
        if(equals(cursor, thisGrammarNode)) {
            +#"rightPart" {
                ->(constructButtonActiveDocumentHTML)
                    ([childByName(thisGrammarNode.#1, "INFO")],
                     thisResultNode);
            };
        };
    };
}


ACTIONS target:TARGET.ALL {
    +#"element or chosen" {
        continue([thisGrammarNode, target], thisResultNode);
        +#"box" {
            forEach(child in children(thisGrammarNode.#2)) {
                ->(_constructActiveDocumentHTMLTree
                  |_constructActiveDocumentHTMLTree_Focus)
                  ([child, slotOriginNode(child)],
                   thisResultNode);
            };

        };
        if(equals(cursor, thisGrammarNode)) {
            +#"rightPart" {
                ->(constructButtonActiveDocumentHTML)
                    ([childByName(thisGrammarNode.#1, "SWITCH")],
                     thisResultNode);
                ->(constructButtonActiveDocumentHTML)
                    ([childByName(thisGrammarNode.#1, "DELETE")],
                     thisResultNode);
            };
        };
    };
}




TreeFunction constructActiveDocumentHTMLTree_OPT

ACTIONS target:TARGET.NULL {
    +#"element opt" {
        continue([thisGrammarNode, target], thisResultNode);
        +#"box" {
            +#"TEXT" {
                +#"&nbsp;";
            };
        };
        if(equals(cursor, thisGrammarNode)) {
            +#"rightPart" {
                ->(constructButtonActiveDocumentHTML)
                    ([childByName(thisGrammarNode.#1, "INSERTOPT")],
                     thisResultNode);
                ->(constructButtonActiveDocumentHTML)
                    ([childByName(thisGrammarNode.#1, "INFO")],
                     thisResultNode);
            };
        };
    };
}

ACTIONS target:TARGET.ALL {
    +#"element opt" {
        continue([thisGrammarNode, target], thisResultNode);
        addClass(thisResultNode, "set");
        +#"box" {
             forEach(child in children(thisGrammarNode.#2)) {
                 ->(_constructActiveDocumentHTMLTree
                   |_constructActiveDocumentHTMLTree_Focus)
                   ([child, slotOriginNode(child)],
                    thisResultNode);
             };
        };
        if(equals(cursor, thisGrammarNode)) {
            +#"rightPart" {
                ->(constructButtonActiveDocumentHTML)
                    ([childByName(thisGrammarNode.#1, "REMOVEOPT")],
                     thisResultNode);
            };
        };
    };
}

TreeFunction constructActiveDocumentHTMLTree_GROUP

ACTIONS target:TARGET.NULL {
    +#"element group" {
        if(equals(cursor, thisGrammarNode)) {
            addClass(thisResultNode, "focus");
        };
        +#"box" {
            var origin = slotOriginNode(thisGrammarNode);
            +#"TEXT" {
                +#slotNodeName(origin.#2);
            };
            +#"TEXT" {
                +#"-";
            };
            +#"TEXT" {
                +#slotNodeName(origin.#3);
            };
        };

    };
}
ACTIONS target:TARGET.ALL {
    +#"element group chosen" {
        if(equals(cursor, thisGrammarNode)) {
            addClass(thisResultNode, "focus");
        };
        +#"box" {
            +#"TEXT" {
                +#slotNodeName(target.#1);
            };
        };
        if(equals(cursor, thisGrammarNode)) {
            +#"rightPart" {
                ->(constructButtonActiveDocumentHTML)
                ([childByName(thisGrammarNode.#1, "REMOVECHAR")],
                 thisResultNode);
            };
        };

    };
}

TreeFunction constructActiveDocumentHTMLTree_ANY_EXCEPT

ACTIONS target:TARGET.NULL {
    +#"element anyExcept" {
        if(equals(cursor, thisGrammarNode)) {
            addClass(thisResultNode, "focus");
        };
        +#"box" {
            var origin = slotOriginNode(thisGrammarNode);
            +#"TEXT" {
                forEach(child in children(slotOriginNode(thisGrammarNode).#2)) {
                    +#slotNodeName(child);
                };
            };
        };
    };
}
ACTIONS target:TARGET.Letter {
    +#"element anyExcept chosen" {
        if(equals(cursor, thisGrammarNode)) {
            addClass(thisResultNode, "focus");
        };
        +#"box" {
            +#"TEXT" {
                +#slotNodeName(target.#1);
            };
        };
        if(equals(cursor, thisGrammarNode)) {
            +#"rightPart" {
                ->(constructButtonActiveDocumentHTML)
                ([childByName(thisGrammarNode.#1, "REMOVECHAR")],
                 thisResultNode);
            };
        };

    };
}

ALL {
}


TreeFunction constructButtonActiveDocumentHTML


ADDLEFT {
    +createButtonAdd();
}

REMOVENODE {
    +createButtonDelete();
}

ADDRIGHT {
    +createButtonAdd();
}


ADDNODE {
    +createButtonAdd();
}

INFO {
    +createButtonInfo();
}

CLOSEINFO {
    +createButtonCloseInfo();
}

SWITCH {
    +createButtonSwitch();
}

DELETE {
    +createButtonDelete();
}

INSERTOPT {
    +createButtonAdd();
}

REMOVEOPT {
    +createButtonDelete();
}

REMOVECHAR {
    +createButtonDelete();
}


TreeFunction doKeyboardHandleEvent

KEYBOARDEVENT.KEY.ArrowRight anEvent {
    var currentIndex = elementAt(cursorIndexPath, length(cursorIndexPath));

    var nextIndex = findNextActionIndex(cursor);
    setElementAt(cursorIndexPath, nextIndex, length(cursorIndexPath));

    redrawAll();
}

KEYBOARDEVENT.KEY.ArrowLeft anEvent {

    var previousIndex = findPreviousActionIndex(cursor);

    setElementAt(cursorIndexPath, previousIndex, length(cursorIndexPath));
    redrawAll();
}


KEYBOARDEVENT.KEY.w anEvent {
    if(htmlAtt(anEvent, "altKey")) {
        var childrens = ->(getActiveDocumentChildren)([cursor], rootResultNode);
        if(not(equals(length(childrens), 0))) {
            addElement(cursorIndexPath, 1);
        };
    };
    redrawAll();
}

KEYBOARDEVENT.KEY.q anEvent {
    if(htmlAtt(anEvent, "altKey")) {
        var theParent = parent(cursor);
        if(not(equals(slotNodeName(theParent), "ROOT"))) {
            removeElementAt(cursorIndexPath, length(cursorIndexPath));
        };
    };
    redrawAll();
}

KEYBOARDEVENT.KEY."!" anEvent {

    var index = indexInParent(cursor);
    var origin = slotOriginNode(cursor);
    if(equals(slotNodeName(origin), "NFOLD")) {
        insertNFOLDChildTemplateAt(origin, index);
        printString("done insert");
    };
    redrawAll();
}

KEYBOARDEVENT.KEY."~"" anEvent {
    var index = indexInParent(cursor);
    var origin = slotOriginNode(cursor);
    if(equals(slotNodeName(origin), "NFOLD")) {
        insertNFOLDChildTemplateAt(origin, plus(index,1));
    };
    redrawAll();
}



KEYBOARDEVENT.KEY.ALL anEvent {
    var index = indexInParent(cursor);
    /*var source = slotSourceNode(cursor);*/
    var origin = slotOriginNode(cursor);

    continue([thisGrammarNode, cursor, origin], thisResultNode);
}

ALL anEvent {
    forEach(aChild in children(thisGrammarNode)) {
        thisTreeFunction([aChild], thisResultNode, [anEvent]);
    };
}


TreeFunction doKeyboardEventFromCursor

ALL cursor:ACTIONS slopeNode:GROUP {
    ->(doGROUPEvents)([thisGrammarNode, cursor, slopeNode], thisResultNode);
}


ALL cursor:ACTIONS slopeNode:ANY_EXCEPT {
    ->(doANYEvents)([thisGrammarNode, cursor, slopeNode], thisResultNode);
}

ALL cursor:ACTIONS slopeNode:OR {
    ->(doOREvents)([thisGrammarNode, cursor, slopeNode], thisResultNode);
}

ALL cursor:ALL slopeNode:ALL {
    printString("to be continued...");
}


TreeFunction doOREvents

ArrowDown cursor:ACTIONS slopeNode:OR {
    if(hasChildWithName(cursor.#1, "SWITCH")) {
        ->(doButtonCommand)([childByName(cursor.#1, "SWITCH"), slopeNode], thisResultNode, [null]);
    };
}
/*
IS_NUMBER() cursor:ACTIONS slopeNode:OR {
    printString("IN NUMBER");
    var index = plus(stringToInt(slotNodeName(thisGrammarNode)), 1);

    if(and(
        hasChildWithName(cursor.#1, "SETALTERNATIVE"),
        smallerEqualsThan(index, length(children(thisGrammarNode.#1)))
        )) {
        ->(doButtonCommand)([childAt(cursor.#1, index), slopeNode], thisResultNode, [null]);
    };
}*/

ALL cursor:ACTIONS slopeNode:OR {
}


TreeFunction doANYEvents


Delete cursor:ACTIONS slopeNode:ANY_EXCEPT {
    if(hasChildWithName(cursor.#1, "REMOVECHAR")) {
            ->(doButtonCommand)([childByName(cursor.#1, "REMOVECHAR"), slopeNode],
                                thisResultNode,
                                [slotNodeName(thisGrammarNode)]);
    };
}

ALL cursor:ACTIONS slopeNode:ANY_EXCEPT {
    ->(doButtonCommand)([childByName(cursor.#1, "SETCHAR"), slopeNode],
                        thisResultNode,
                        [slotNodeName(thisGrammarNode)]);
}


TreeFunction doGROUPEvents


Delete cursor:ACTIONS slopeNode:GROUP {
    if(hasChildWithName(cursor.#1, "REMOVECHAR")) {
        ->(doButtonCommand)([childByName(cursor.#1, "REMOVECHAR"), slopeNode],
                            thisResultNode,
                            [slotNodeName(thisGrammarNode)]);
    };
}

ALL cursor:ACTIONS slopeNode:GROUP {
    ->(doButtonCommand)([cursor.#1.#1, slopeNode],
                        thisResultNode,
                        [slotNodeName(thisGrammarNode)]);
}


/*************************** */

TreeFunction doMouseHandleEvent

ALL e:MOUSEEVENT anHTMLObject {
  var source = slotSourceNode(thisGrammarNode);

  /*var slopeNode = slotOriginNode(thisGrammarNode);
*/

  var slopeNode = slotSourceNode(source);
  ->(doButtonCommand)([source, slopeNode], thisResultNode, [anHTMLObject]);


}

/******************** */

TreeFunction doButtonCommand

SWITCH anOrNode:OR anHTMLObject {
  newTreeNode = switchToNextNode(anOrNode);
  redrawAll();
}

DELETE aNode:OR anHTMLObject {
      deleteNode(aNode);
      redrawAll();
}

ADDNODE aNode:NFOLD anHTMLObject {
    insertNFOLDChildTemplateAt(aNode, slotNodeName(thisGrammarNode.#2));
    redrawAll();
}

ADDLEFT aNode:NFOLD anHTMLObject {
    insertNFOLDChildTemplateAt(aNode, slotNodeName(thisGrammarNode.#2));
    redrawAll();
}

ADDRIGHT aNode:NFOLD anHTMLObject {
    insertNFOLDChildTemplateAt(aNode, plus(slotNodeName(thisGrammarNode.#2),1));
    setElementAt(cursorIndexPath,
                 plus(elementAt(cursorIndexPath, length(cursorIndexPath)),1),
                 length(cursorIndexPath)
                );
    redrawAll();
}

REMOVENODE aNode:NFOLD anHTMLObject {
    /* Nope XD : removeNodeFromNFold(aNode); */
    var at = slotNodeName(thisGrammarNode.#2);
    removeNFOLDChildAt(aNode, at);
    if(not(equals(at, 1))) {
        setElementAt(
            cursorIndexPath,
            minus(elementAt(cursorIndexPath, length(cursorIndexPath)),1),
            length(cursorIndexPath)
        );
    };

    redrawAll();
}

SETALTERNATIVE anOrNode:OR anHTMLObject {
    switchTo(anOrNode, slotNodeName(thisGrammarNode.#2));
    redrawAll();
}

INSERTOPT aNode:OPT anHTMLObject {
    printString("lets a go");
    insertSingleChildTemplate(aNode);
    printString("lets a draw");
    redrawAll();
}

REMOVEOPT aNode:OPT anHTMLObject {
    removeOPTChild(aNode);
    redrawAll();
}

SETCHAR aNode:ANY_EXCEPT key {
    setCharAnyExcept(aNode, key);
    redrawAll();
}

SETCHAR aNode:GROUP key {
    setCharGroup(aNode, key);
    redrawAll();
}

REMOVECHAR aNode:ANY_EXCEPT key {
    removeCharAnyExcept(aNode);
    redrawAll();
}


REMOVECHAR aNode:GROUP key {
    removeCharGroup(aNode);
    redrawAll();
}

SHOWALTERNATIVES bla:ALL anHTMLObject {

}

ALL aNode:ALL anHTMLObject {
    continue([thisGrammarNode, aNode], thisResultNode, [anHTMLObject]);
}

TreeFunction _constructHTMLString

TEXT aNodeList {


    var i = 0;
    forEach(child in children(thisGrammarNode)) {
        if(largerThan(i, 0)) {
            htmlString =
                    concat(htmlString, " ");
        };
        htmlString =
                concat(htmlString, slotNodeName(child));
        i = plus(i, 1);
    };


}

ROOT aNodeList {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode, [aNodeList]);
    };
}

att {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode);
    };
}

att.ALL {
    /*htmlString = concatAll(htmlString, [" ", slotNodeName(thisGrammarNode),
                                        "='",
                                        slotNodeName(thisGrammarNode.#1),
                                        "'"
                                     ]
                 );*/


    var attributeValue = "";
    var addSpace = false;
    forEach(child in children(thisGrammarNode)) {
        if(addSpace) {
            attributeValue = concat(attributeValue, " ");
        };
        attributeValue = concat(attributeValue, slotNodeName(child));
        addSpace = true;
    };
    addAttribute(slotNodeName(thisGrammarNode), attributeValue);
}


DIV aNodeList {

    if(and(
        largerThan(length(children(thisGrammarNode)), 0),
        equals(slotNodeName(thisGrammarNode.#1), "att")
       )
    ) {
        printString(concat("In ", slotNodeName(thisGrammarNode)));
        startOpenTag(thisGrammarNode, aNodeList);
            thisTreeFunction([thisGrammarNode.#1], thisResultNode);
        endOpenTag();

        printString(htmlString);

        var i = 2;
        while(smallerEqualsThan(i, length(children(thisGrammarNode)))) {
            thisTreeFunction([childAt(thisGrammarNode, i)], thisResultNode, [aNodeList]);
            i = plus(i, 1);
        };

        closeTag(thisGrammarNode);

    };



}

BR aNodeList {

    if(and(
        largerThan(length(children(thisGrammarNode)), 0),
        equals(slotNodeName(thisGrammarNode.#1), "att")
       )
    ) {
        printString(concat("In ", slotNodeName(thisGrammarNode)));
        startOpenTag(thisGrammarNode, aNodeList);
            thisTreeFunction([thisGrammarNode.#1], thisResultNode);
        endOpenTag();

        printString(htmlString);

        var i = 2;
        while(smallerEqualsThan(i, length(children(thisGrammarNode)))) {
            thisTreeFunction([childAt(thisGrammarNode, i)], thisResultNode, [aNodeList]);
            i = plus(i, 1);
        };

        closeTag(thisGrammarNode);

    };



}

SPAN aNodeList {

    if(and(
        largerThan(length(children(thisGrammarNode)), 0),
        equals(slotNodeName(thisGrammarNode.#1), "att")
       )
    ) {
        printString(concat("In ", slotNodeName(thisGrammarNode)));
        startOpenTag(thisGrammarNode, aNodeList);
            thisTreeFunction([thisGrammarNode.#1], thisResultNode);
        endOpenTag();

        printString(htmlString);

        var i = 2;
        while(smallerEqualsThan(i, length(children(thisGrammarNode)))) {
            thisTreeFunction([childAt(thisGrammarNode, i)], thisResultNode, [aNodeList]);
            i = plus(i, 1);
        };

        closeTag(thisGrammarNode);

    };



}

ALL aNodeList {

           /* printString(concat("In ", slotNodeName(thisGrammarNode)));*/
            startDivTagClass(thisGrammarNode, aNodeList);
            endOpenTag();

   /*         printString(htmlString);*/

            var i = 1;
            while(smallerEqualsThan(i, length(children(thisGrammarNode)))) {
                thisTreeFunction([childAt(thisGrammarNode, i)], thisResultNode, [aNodeList]);
                i = plus(i, 1);
            };

            closeTagString("DIV");


            /*

        if(and(
                    largerThan(length(children(thisGrammarNode)), 0),
                    equals(slotNodeName(thisGrammarNode.#1), "att")
                   )
                ) {
                    printString(concat("In ", slotNodeName(thisGrammarNode)));
                    startDivTagClass(thisGrammarNode, aNodeList);


                    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
                    endOpenTag();

                    printString(htmlString);

                    var i = 2;
                    while(smallerEqualsThan(i, length(children(thisGrammarNode)))) {
                        thisTreeFunction([childAt(thisGrammarNode, i)], thisResultNode, [aNodeList]);
                        i = plus(i, 1);
                    };

                    closeTagString("DIV");

                } else {

                ........
        };*/
}



TreeFunction _constructHTMLTree

ROOT a:ALL {
    +#"regexEditor" {
        forEach(child in children(thisGrammarNode)) {
            thisTreeFunction([child, slotSourceNode(a)], thisResultNode);
        };
    };
}

Letter a:ALL {
    +#"TEXT" {
        +#slotNodeName(thisGrammarNode.#1);
    };
}

NULL a:ALL {
    +#"TEXT" {
        +#"NULL";
    };
}


EMPTY a:ALL {
    +#"TEXT" {
        +#"&nbsp;";
    };
}



ACTIONS slopeNode:OR {
    if(equals(slotNodeName(slopeNode.#1), "NULL")) {
        +makeFocusableBox(thisGrammarNode, "orBox") {
            forEach(aChild in children(thisGrammarNode.#1)) {
                +#"orBoxAlt" {
                    thisTreeFunction([aChild, slotSourceNode(aChild)], thisResultNode);
                };
            };
        };
    } else {
        +makeFocusableBox(thisGrammarNode, "box") {
            thisTreeFunction([thisGrammarNode.#2, slotSourceNode(thisGrammarNode.#2)], thisResultNode);
        };
    };

}

COMMANDS.SETALTERNATIVE slopeNode:OR {
    +#"TEXT" {
        +#slotNodeName(thisGrammarNode.#1);
    };
}

ACTIONS slopeNode:GROUP {
    +makeFocusableBox(thisGrammarNode, "box literal") {
        +#"TEXT" {
            +#concatAll(slotNodeName(slopeNode.#2),
                  ["-", slotNodeName(slopeNode.#3)]);
        };
    };
}

ACTIONS slopeNode:ALL {
    +makeFocusableBox(thisGrammarNode, "box") {
        thisTreeFunction([thisGrammarNode.#2, slotSourceNode(thisGrammarNode.#2)], thisResultNode);
    };
}


ALL slopeNode:ALL {
    forEach(aChild in children(thisGrammarNode)) {
        thisTreeFunction([aChild, slotSourceNode(aChild)], thisResultNode);
    };
}


TreeFunction htmlStringConstruction

NULL aNodeList {
/* (!)*/
    htmlString =
        concatAll(htmlString,[
            concatAll(
                "<DIV class=~"letter~" style=~"background:#f00;display:inline-block;~"",
                [html_generateIDString(thisGrammarNode, aNodeList),
                ">"]),
            "&nbsp", "</DIV>"
        ]);
}

Text aNodeList {

    htmlString =
        concatAll(htmlString,[
            concatAll(
                "<DIV class=~"letter~"
                 style=~"color:blue;
                 text-align:center;
                 color:blue;
                 display:inline-block;~"",
                [html_generateIDString(thisGrammarNode, aNodeList),
                ">"]),
            slotNodeName(thisGrammarNode.#1), "</DIV>"
        ]);
}


BUTTON aNodeList {
    lastButton = thisGrammarNode;
    htmlString =
        concatAll(htmlString, [
            html_createOpenTagFromNode(thisGrammarNode, aNodeList),
            slotNodeName(thisGrammarNode.#1.#1),
            html_createCloseTagFromNode(thisGrammarNode, aNodeList)
        ]);

}

/*
ALT_BUTTON aNodeList {
    htmlString =
            concatAll(htmlString, [
                html_createOpenTagFromNode(thisGrammarNode, aNodeList),
                slotNodeName(thisGrammarNode.#1.#1),
                html_createCloseTagFromNode(thisGrammarNode, aNodeList)
            ]);
}*/

HTML aNodeList {
    htmlString = concat(
                    htmlString,
                    html_createOpenTag("HTML"));
    continue([thisGrammarNode], thisResultNode, [aNodeList]);
    htmlString = concat(
                    htmlString,
                    html_createCloseTag("HTML"));
}

ROOT aNodeList {
    continue([thisGrammarNode], thisResultNode, [aNodeList]);
}


ALL aNodeList {

    htmlString = concat(
                    htmlString,
                    html_createOpenTagFromNode(thisGrammarNode, aNodeList));
    continue([thisGrammarNode], thisResultNode, [aNodeList]);
    htmlString = concat(
                    htmlString,
                    html_createCloseTagFromNode(thisGrammarNode, aNodeList));

}


TreeFunction htmlStringConstruction2

NULL aNodeList {
/* (!)*/
    htmlString =
        concatAll(htmlString,[
            concatAll(
                "<DIV class=~"letter~" style=~"background:#f00;display:inline-block;~"",
                [html_generateIDString(thisGrammarNode, aNodeList),
                ">"]),
            "&nbsp", "</DIV>"
        ]);
}

Text aNodeList {

    htmlString =
        concatAll(htmlString,[
            concatAll(
                "<DIV class=~"letter~" style=~"color:blue;text-align:center;color:blue;display:inline-block;~"",
                [html_generateIDString(thisGrammarNode, aNodeList),
                ">"]),
            slotNodeName(thisGrammarNode.#1), "</DIV>"
        ]);
}

PLAIN aNodeList {
    htmlString = concatAll(htmlString, constructDFFText(thisGrammarNode));
}

BUTTON aNodeList {
    lastButton = thisGrammarNode;
    htmlString =
        concatAll(htmlString, [
            html_createOpenTagFromNode(thisGrammarNode, aNodeList),
            slotNodeName(thisGrammarNode.#1.#1),
            html_createCloseTagFromNode(thisGrammarNode, aNodeList)
        ]);

}


ALT_BUTTON aNodeList {
    htmlString =
            concatAll(htmlString, [
                html_createOpenTagFromNode(thisGrammarNode, aNodeList),
                slotNodeName(thisGrammarNode.#1.#1),
                html_createCloseTagFromNode(thisGrammarNode, aNodeList)
            ]);
}


HTML aNodeList {
    htmlString = concat(
                    htmlString,
                    html_createOpenTag("HTML"));
    continue([thisGrammarNode], thisResultNode, [aNodeList]);
    htmlString = concat(
                    htmlString,
                    html_createCloseTag("HTML"));
}

ROOT aNodeList {
    continue([thisGrammarNode], thisResultNode, [aNodeList]);
}


ALL aNodeList {

    htmlString = concat(
                    htmlString,
                    html_createOpenTagFromNode(thisGrammarNode, aNodeList));
    continue([thisGrammarNode], thisResultNode, [aNodeList]);
    htmlString = concat(
                    htmlString,
                    html_createCloseTagFromNode(thisGrammarNode, aNodeList));

}


TreeFunction htmlTreeConstruction

ROOT {
    continue ([thisGrammarNode], thisResultNode);
}

Letter {
    +#"Text" {
        +#slotNodeName(thisGrammarNode.#1);
    };
}

NULL {
    +#"NULL";
}


INPUT {
    +#"DIV style=~"text-align:center;color:blue;display:inline-block;~"" {

        forEach(aChild in children(thisGrammarNode)) {
                thisTreeFunction([aChild], thisResultNode);
        };


    };
}

INPUT.INFO {
    +#"DIV style=~"text-align:center;color:blue;~"" {
        forEach(child in children(thisGrammarNode)) {
            thisTreeFunction([child], thisResultNode);
        };
    };
}

INPUT.CONTENT {
    +#"DIV style=~"border: 1px solid black;
                    text-align=center;
                 ~"
          contenteditable=~"true~"
      "
{
        +#slotNodeName(thisGrammarNode.#1);
    };
}


ACTIONS {
    +#"DIV style=~"text-align:center;color:blue;display:inline-block;~"" {

        forEach(aChild in children(thisGrammarNode)) {
                thisTreeFunction([aChild], thisResultNode);
        };


    };
}
/*

ACTIONS.INFO {
    +#"DIV"
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode);
    };

}*/



ACTIONS.COMMANDS {
    +#"DIV" {
        forEach(aChild in children(thisGrammarNode)) {
            thisTreeFunction([aChild], thisResultNode);
        };
    };
}

ACTIONS.COMMANDS.ALL {
    +#"BUTTON" {
        +#"LABEL" {
            +#slotNodeName(thisGrammarNode.#1);
        };
    };
}

ACTIONS.TARGET {
    +#"DIV style=~"text-align:center;color:blue;~"" {
         forEach(aChild in children(thisGrammarNode)) {
                thisTreeFunction([aChild], thisResultNode);
         };
    };
}

ALL {
    forEach(aChild in children(thisGrammarNode)) {
        thisTreeFunction([aChild], thisResultNode);
    };
}


TreeFunction t_isEditable

REGEX {
    return thisTreeFunction([thisGrammarNode.#1], thisResultNode);
}


Alternative {
    return true;
}

Concatenation {
    forEach(child in children(thisGrammarNode)) {
        if(thisTreeFunction([child], thisResultNode)) {
            return true;
        };
    };
    return false;
}

Plus {
    return true;
}

Star {
    return true;
}

Bracket {
    return thisTreeFunction([thisGrammarNode.#1]
                            , thisResultNode);
}

CharRange {
    return true;
}

Token {
    return false;
}




TreeFunction _constructSmallerSlopeTree

isNamedSLOTSLOPETreeNode().AND {
    forEach(childChild in children(thisGrammarNode.#1)) {
        thisTreeFunction([childChild], thisResultNode);
    };
}



ALL {
    +#slotNodeName(thisGrammarNode) {
        forEach(child in children(thisGrammarNode)) {
            thisTreeFunction([child], thisResultNode);
        };
    };
}

TreeFunction constructDFFTextTree

ALL exceptString {



   var stringToAppend = "";
   if(not(equals(slotNodeName(thisGrammarNode), exceptString))) {
       if(isString(slotNodeName(thisGrammarNode))) {
            stringToAppend = slotNodeName(thisGrammarNode);
       };
       if(isInteger(slotNodeName(thisGrammarNode))) {
            stringToAppend = intToString(slotNodeName(thisGrammarNode));
       };
   };
   dffText = concat(dffText, stringToAppend);


   forEach(aChild in children(thisGrammarNode)) {
        thisTreeFunction([aChild], thisResultNode, [exceptString]);
   };
}

TreeFunction _constructRegexWordGrammarString

/*alternative ALL*/

alternativeTail {
    appendRegexWordGrammarString(" | ");
    thisTreeFunction([thisGrammarNode.#2], thisResultNode);
}

anyExcept {

}

ALL {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode);
    };
}

TreeFunction constructRuleTree

OR {
    var numAlts = numAlternatives(thisGrammarNode);


    thisTreeFunction([createORChildTemplateAt(thisGrammarNode, 0)], thisResultNode);


    var counter = 1;

    while(smallerThan(counter, numAlts)) {

        +#" | ";
        thisTreeFunction([createORChildTemplateAt(thisGrammarNode, counter)], thisResultNode);
        counter = plus(counter, 1);
    };

}

OPT {
    var childTemplate = createSingleChildTemplate(thisGrammarNode);
    thisTreeFunction([childTemplate], thisResultNode);
    +#"?";
}

BRACKET {

    +#"(";
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    +#")";
}

isNFOLD_0() {
    var childTemplate = createNFOLDChildTemplate(thisGrammarNode);
    thisTreeFunction([childTemplate], thisResultNode);
    +#"*";
}

isNFOLD_1() {
    var childTemplate = createNFOLDChildTemplate(thisGrammarNode);
    thisTreeFunction([childTemplate], thisResultNode);
    +#"+";
}

AND {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);

    var counter = 2;
    var childrenLength = length(children(thisGrammarNode));
    while(or(smallerThan(counter, childrenLength),
             equals(counter,childrenLength))) {
        +#" ";
        thisTreeFunction([childAt(thisGrammarNode, counter)], thisResultNode);

        counter = plus(counter, 1);
    };
}

NULL {
    +#"NULL";
}

SLOPETOKEN {
    +#"~"";
    +#slotNodeName(thisGrammarNode.#1) ;
    +#"~"";
}

ALL {
    +#slotNodeName(thisGrammarNode);
}

TreeFunction t_constructTreeString

ALL tabNumber {

    var i = 0;
    while(smallerThan(i, tabNumber)) {
        +#"    ";
        i = plus(i,1);
    };

    +#"#";
    +#slotNodeName(thisGrammarNode);
    +#"(";

    if(hasChildren(thisGrammarNode)) {
        +#"\n";

        forEach(child in children(thisGrammarNode)) {
            thisTreeFunction([child], thisResultNode, [plus(tabNumber, 1)]);
            +#"\n";
        };

        i = 0;
        while(smallerThan(i, tabNumber)) {
                +#"    ";
                i = plus(i,1);
        };
    };

    +#")";
}''');
exampleStrings.add('''a''');
  return exampleStrings;
}
}
