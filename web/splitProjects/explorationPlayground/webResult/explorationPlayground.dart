import '../../../../lib/slot/runtime/SLOTHTMLProject.dart';
import '../../../html/examples/SLOTProjectConstructor.dart';
import 'explorationPlayground.slp.dart';

main() {

  SLOTHTMLProject project =
  new SLOTProjectConstructor().constructHTMLProject(new explorationPlayground());
  var slotResult = project.runProject();

}

