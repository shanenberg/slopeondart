import 'dart:io';

import 'package:slopeondart/lib/util/GrammarUtils.dart';

main() {
  List<String> sqlStrings = new File
    ("web\\splitProjects\\sqlEditor\\slot\\slopeFile")
      .readAsStringSync().split("###");

  parseGrammar(sqlStrings[1].substring(" SLOPE".length)).doTests();
}