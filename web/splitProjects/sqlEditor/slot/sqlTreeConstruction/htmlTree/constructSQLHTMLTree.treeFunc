### FUNCTION

constructSQLHTMLTree dataTree {
    var root = #"ROOT";
    ->(constructSQLHTMLTree_t
     | constructSQLBooleanTermHTMLTree
     | constructSQLFROMClauseHTMLTree
     | constructSQLExpressionHTMLTree)
     ([dataTree], root);

    return root;
}


generateRoundBlock description {
    var block =
    #"block round" [
        +#"blockDescriptor" [
            +#"blockDescriptorText" [
                +#"TEXT" {
                    +#description;
                };
            ];
        ];
        +#"blockContent" [

        ];
    ];
    return block;
}



### TREE

TreeFunction constructSQLHTMLTree_t

SQL {

    +#"SQLInput" {
        +#"TEXTAREA" {
            +#"att" {
                +pairNode("title", "SQL Input");
                +pairNode("rows", "9");
                +pairNode("cols", "70");
            };
        };
        +#"BR" {
            +#"att";
        };
        +#"BUTTON" {
            +#"att";
            +#"TEXT" {
                +#"Generate";
            };
        };
    };
    +#"editor" {
        forEach(child in children(thisGrammarNode)) {
            thisTreeFunction([child], thisResultNode);
        };
    };
}





EXCEPT {
    +#"block round" {
            +#"blockDescriptor" {
                +#"blockDescriptorText" {
                    +#"TEXT" {
                        +#"EXCEPT";
                    };
                };
            };
            +#"blockContent" {
                thisTreeFunction([thisGrammarNode.#1], thisResultNode);
                +#"tableOp" {
                    +#"TEXT" {
                        +#"\\";
                    };
                };
                thisTreeFunction([thisGrammarNode.#2], thisResultNode);
            };

    };
}

UNION {
    +#"verticalBlockContainer" {
        +#"block round" {
                +#"blockDescriptor" {
                    +#"blockDescriptorText" {
                        +#"TEXT" {
                            +#"UNION";
                        };
                    };
                };
                +#"blockContent" {
                    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
                    +#"tableOp" {
                        +#"TEXT" {
                            +#"&cup;";
                        };
                    };
                    thisTreeFunction([thisGrammarNode.#2], thisResultNode);
                };

        };
    };

}

INTERSECT {
    +#"verticalBlockContainer" {
        +#"block round" {
                +#"blockDescriptor" {
                    +#"blockDescriptorText" {
                        +#"TEXT" {
                            +#"INTERSECT";
                        };
                    };
                };
                +#"blockContent" {
                    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
                    +#"tableOp" {
                        +#"TEXT" {
                            +#"&cap;";
                        };
                    };
                    thisTreeFunction([thisGrammarNode.#2], thisResultNode);
                };

        };
    };
}

/* Alternative with bordered queries (for example except):
EXCEPT {
    +#"block round" {
            +#"blockDescriptor" {
                +#"blockDescriptorText" {
                    +#"TEXT" {
                        +#"EXCEPT";
                    };
                };
            };
            +#"blockContent" {
                +#"table" {
                    +#"tableColumns" {
                        thisTreeFunction([thisGrammarNode.#1], thisResultNode);
                    };
                };
                +#"tableOp" {
                    +#"TEXT" {
                        +#"\\";
                    };
                };
                +#"table" {
                    +#"tableColumns" {
                        thisTreeFunction([thisGrammarNode.#2], thisResultNode);
                    };
                };
            };

    };
}
*/


SQL.QUERY {
    +#"verticalBlockContainer" {
        forEach(child in children(thisGrammarNode)) {
            thisTreeFunction([child], thisResultNode);
        };
    };
}

QUERY {
    +#"innerQueryContainer" {
        +#"verticalBlockContainer" {
            forEach(child in children(thisGrammarNode)) {
                thisTreeFunction([child], thisResultNode);
            };
        };
    };
}

SELECT {
    +#"block round transBorderBlock" {
            +#"blockDescriptor left" {
                +#"blockDescriptorText" {
                    +#"TEXT" {
                        +#"SELECT";
                    };
                };
            };
            +#"blockContent" {
               thisTreeFunction([thisGrammarNode.#1], thisResultNode);
            };
    };
}

SELECT_LIST {
    forEach(child in children(thisGrammarNode)) {
        +#"selectColumn" {
            thisTreeFunction([child], thisResultNode);
            +#"TEXT" {
                +#" ";
            };
        };
    };
}

SELECT_COLUMN {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
}

SELECT_COLUMN_RENAMED {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    +#"TEXT" {
        +#" ";
    };
    thisTreeFunction([thisGrammarNode.#2], thisResultNode);
}

SELECT_COLUMN_RENAMED.AS {
    +#"TEXT" {
        +#"AS";
        +#slotNodeName(thisGrammarNode.#1);
    };
}

ALL_COLUMNS {
    +#"TEXT" {
        +#"*";
    };
}


FROM {
    printString("reached FROM");
    +#"block round transBorderBlock" {
        +#"blockDescriptor left" {
            +#"blockDescriptorText" {
                +#"TEXT" {
                    +#"FROM";
                };
            };
        };
        +#"blockContent" {
            forEach(child in children(thisGrammarNode)) {
                printString("reached FROMChild:");
                printString(slotNodeName(child));
                thisTreeFunction([child], thisResultNode);
            };
        };

    };
}

WHERE {
    +#"block round transBorderBlock" {
        +#"blockDescriptor left" {
            +#"blockDescriptorText" {
                +#"TEXT" {
                    +#"WHERE";
                };
            };
        };
        +#"blockContent" {
            forEach(child in children(thisGrammarNode)) {
                +#"whereLine" {
                    thisTreeFunction([child], thisResultNode);
                };
            };
        };

    };
}

"GROUP BY" {
    +#"block round transBorderBlock" {
        +#"blockDescriptor left" {
            +#"blockDescriptorText" {
                +#"TEXT" {
                    +#"GROUP BY";
                };
            };
        };
        +#"blockContent" {
            forEach(child in children(thisGrammarNode)) {
                thisTreeFunction([child], thisResultNode);
            };
        };
    };
}

HAVING {
    +#"block round transBorderBlock" {
        +#"blockDescriptor left" {
            +#"blockDescriptorText" {
                +#"TEXT" {
                    +#"HAVING";
                };
            };
        };
        +#"blockContent" {
            forEach(child in children(thisGrammarNode)) {
                thisTreeFunction([child], thisResultNode);
            };
        };
    };
}

"ORDER BY" {
    +#"block round transBorderBlock" {
        +#"blockDescriptor left" {
            +#"blockDescriptorText" {
                +#"TEXT" {
                    +#"ORDER BY";
                };
            };
        };
        +#"blockContent" {
            forEach(child in children(thisGrammarNode)) {
                thisTreeFunction([child], thisResultNode);
            };
        };
    };
}


ALL {
    continue([thisGrammarNode], thisResultNode);
}