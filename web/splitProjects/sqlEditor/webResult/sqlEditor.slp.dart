/***********************************************************
I am purely generated!!!! Don't touch me, touch the slp files 
***************************************************************/
      import '../../../html/examples/HTMLExample.dart';
class sqlEditor extends HTMLExample {
   List<String> getExampleProjectStrings() {
  List<String> exampleStrings = new List<String>();
exampleStrings.add('''


SQL

Rules: queryStatement

queryStatement => query ";"?.

query => intersectableQuery
         (queryExcept | queryUnion)*.

queryExcept => EXCEPT ALL? intersectableQuery.
queryUnion  => UNION ALL? intersectableQuery.

intersectableQuery => bracketableQuery
                      queryIntersect*.
queryIntersect => INTERSECT ALL? bracketableQuery.

bracketableQuery => ("\\(" query "\\)" | select).


select => selectclause
           fromclause?
           whereclause?
           groupbyclause?
           havingclause?
           orderbyclause?.

selectclause => SELECT (DISTINCT | ALL)? selectlist.

selectlist => (allColumns | selectColumn)
              ( "," selectColumn)*.

allColumns => "\\*".

selectColumn => term selectColumnRename?.

selectColumnRename => AS? identifier.

fromclause => FROM tablereference  ( "," tablereference )*.

whereclause => WHERE term.

groupbyclause => GROUP BY column collateClause?
                 groupbyclauseTail*.

groupbyclauseTail => "," column collateClause?.

collateClause => COLLATE identifier ("\\." identifier)?.

havingclause => HAVING term.

orderbyclause => ORDER BY column (ASC|DESC)? orderbyclauseTail*.
orderbyclauseTail => "," column (ASC|DESC)?.




term => booleanterm (OR booleanterm)*.

booleanterm => booleanfactor (AND booleanfactor)*.

booleanfactor => ( NOT )? booleantest.

booleantest => booleanprimary booleantestOp?.
booleantestOp => IS (NOT)? truthvalue.

truthvalue => (TRUE | FALSE | UNKNOWN).

booleanprimary => exists | predicate.

predicate => termexpr (comparison
                      | between
                      | in
                      | like
                      | nullcheck
                      | match
                      | overlaps
                      | quantifiedcomparison
                      | quantifiedlike)?.

comparison => compop termexpr.
compop => "=" | notequalsoperator | "<" | ">" | "<=" | ">=".
notequalsoperator => "<>" | "!=".

quantifiedcomparison => compop quantifier subquery.

between => NOT? BETWEEN termexpr AND termexpr.

in => NOT? IN ("\\(" query "\\)" | invaluelist).
invaluelist => "\\(" termexpr ("," termexpr)* "\\)".

like => NOT? LIKE termexpr.
nullcheck => IS NOT? NULL.
match => MATCH UNIQUE? (PARTIAL | FULL)? subquery.
overlaps => OVERLAPS termexpr.

quantifiedlike => NOT? LIKE quantifier subquery.

quantifier => some | all.
some => SOME | ANY.
all => ALL.

exists => EXISTS subquery.

termexpr => termexpr1 termexprPlusMinusConcat*.
termexprPlusMinusConcat => ("\\+" | "\\-" | "\\|\\|")  termexpr1.

termexpr1 => valueexpr termexpr1MultDivide*.
termexpr1MultDivide => ("\\*" | "/") valueexpr.

valueexpr => sign? (functionspec | dateliteral | timeliteral | timestampliteral | intervalliteral | identifier stringLiteralQuote | stringLiteralQuote | NULL | TRUE | FALSE | UNKNOWN | column
                           | number | casespecification | castspecification |extractspec | "\\(" query "\\)" | "\\(" term "\\)" | rowvalueconstructor).

functionspec => countspec |
                aggrspec |
                customfunctionspec. /* (?)*/

countspec => COUNT "\\(" (countAllTuples | aggrparameter) "\\)".
countAllTuples => "\\*".

aggrspec => (AVG|MAX|MIN|SUM) "\\(" aggrparameter "\\)".
aggrparameter => (DISTINCT|ALL)? termexpr.

customfunctionspec => identifier "\\(" (term ("," term)*)? "\\)".

column => qualifier ("\\."  "\\*")? | "\\?".
number => UnsignedNumber.

rowvalueconstructor => "\\(" termexpr ("," termexpr)* "\\)".

qualifier => identifier qualifierTail*.
qualifierTail => "\\." identifier.

castspecification => nonBracketCast
                   | bracketCast.
nonBracketCast => CAST termexpr AS qualifier.
bracketCast => CAST "\\(" termexpr AS qualifier "\\)".

dateliteral => DATE stringLiteralQuote.
timeliteral => TIME stringLiteralQuote.
timestampliteral => TIMESTAMP stringLiteralQuote.
intervalliteral => INTERVAL sign? stringLiteralQuote intervalqualifier.
sign => "\\+" | "\\-".


intervalqualifier => startfield TO endfield | singledatetimefield.
startfield => non_seconddatetimefield ("\\(" number "\\)")?.
non_seconddatetimefield => "YEAR" | "MONTH" | "DAY" | "HOUR" | "MINUTE".
endfield => non_seconddatetimefield | "SECOND" ("\\(" number "\\)")?.
singledatetimefield => non_seconddatetimefield ("\\(" number "\\)")? | "SECOND" ("\\(" unsignedinteger ("," "\\(" unsignedinteger)? "\\)")?.

timeField => "YEAR" | "MONTH" | "DAY" | "HOUR" | "MINUTE" | "SECOND".


extractspec => EXTRACT "\\(" timeField FROM termexpr "\\)" .


/* Direktverschachtelte AS nicht möglich?.... */

tablereference => tablereference0 tablereferenceComma*.
tablereferenceComma => "," tablereference0.


tablereference0 => tablereference1
                    join*.

join => crossJoin |
        naturalJoin |
        conditionalJoin.

crossJoin => CROSS JOIN tablereference1.
naturalJoin => NATURAL joinSetSpecification? JOIN tablereference1.
conditionalJoin => joinSetSpecification? JOIN tablereference1
                        (onSpecification |
                         usingSpecification).


joinSetSpecification => (LEFT | RIGHT | FULL) OUTER? | INNER.
onSpecification => ON term.
usingSpecification => USING "\\(" identifier usingAdditional* "\\)".

usingAdditional => "," identifier.

tablereference1 => tableLiteral | tableSubquery.


tableLiteral => qualifier tableAsRename?.

tableSubquery => subquery tableAsRename?.

tableAsRename => AS? identifier tableAsRenameColumns?.

tableAsRenameColumns => "\\(" identifier tableAsRenameColumnAdditional* "\\)".

tableAsRenameColumnAdditional => "," identifier.



subquery => "\\(" (tablereference | query) "\\)".





casespecification => CASE term? (WHEN term THEN term)+ (ELSE term)? END.






Scanner:

/*
  dateStringLiteralQuote => "'(0|[1-9][0-9]*)\\-(0|[1-9][0-9]*)\\-(0|[1-9][0-9]*)'".

  timeStringLiteralQuote => "'(0|[1-9][0-9]*):(0|[1-9][0-9]*)(\\.(0|[1-9][0-9]*)?)?((\\-|\\+)(0|[1-9][0-9]*):(0|[1-9][0-9]*))?'".
*/
/* (?)
  dateStringLiteralQuote => "'(0|[1-9][0-9]*)\\-(0|[1-9][0-9]*)\\-(0|[1-9][0-9]*)'".
  timeStringLiteralQuote => "'(0|[1-9][0-9]*):(0|[1-9][0-9]*)(\\.(0|[1-9][0-9]*)?)?:".
*/
  stringLiteralQuote => "'(^')*'" .

  UNION => "(U|u)(N|n)(I|i)(O|o)(N|n)".
  EXCEPT => "(E|e)(X|x)(C|c)(E|e)(P|p)(T|t)".
  INTERSECT => "(I|i)(N|n)(T|t)(E|e)(R|r)(S|s)(E|e)(C|c)(T|t)".
  ALL => "(A|a)(L|l)(L|l)".
  SELECT => "(S|s)(E|e)(L|l)(E|e)(C|c)(T|t)".
  DISTINCT => "(D|d)(I|i)(S|s)(T|t)(I|i)(N|n)(C|c)(T|t)".
  AS => "(A|a)(S|s)".
  OR => "(O|o)(R|r)".
  AND => "(A|a)(N|n)(D|d)".
  NOT => "(N|n)(O|o)(T|t)".
  IS => "(I|i)(S|s)".
  TRUE => "(T|t)(R|r)(U|u)(E|e)".
  FALSE => "(F|f)(A|a)(L|l)(S|s)(E|e)".
  UNKNOWN => "(U|u)(N|n)(K|k)(N|n)(O|o)(W|w)(N|n)".
  BETWEEN => "(B|b)(E|e)(T|t)(W|w)(E|e)(E|e)(N|n)".
  IN => "(I|i)(N|n)".
  LIKE => "(L|l)(I|i)(K|k)(E|e)".
  NULL => "(N|n)(U|u)(L|l)(L|l)".
  MATCH => "(M|m)(A|a)(T|t)(C|c)(H|h)".
  UNIQUE => "(U|u)(N|n)(I|i)(Q|q)(U|u)(E|e)".
  PARTIAL => "(P|p)(A|a)(R|r)(T|t)(I|i)(A|a)(L|l)".
  FULL => "(F|f)(U|u)(L|l)(L|l)".
  OVERLAPS => "(O|o)(V|v)(E|e)(R|r)(L|l)(A|a)(P|p)(S|s)".
  SOME => "(S|s)(O|o)(M|m)(E|e)".
  ANY => "(A|a)(N|n)(Y|y)".
  EXISTS => "(E|e)(X|x)(I|i)(S|s)(T|t)(S|s)".
  CAST => "(C|c)(A|a)(S|s)(T|t)".
  DATE => "(D|d)(A|a)(T|t)(E|e)".
  TIME => "(T|t)(I|i)(M|m)(E|e)".
  TIMESTAMP => "(T|t)(I|i)(M|m)(E|e)(S|s)(T|t)(A|a)(M|m)(P|p)".
  INTERVAL => "(I|i)(N|n)(T|t)(E|e)(R|r)(V|v)(A|a)(L|l)".
  TO => "(T|t)(O|o)".
  COUNT => "(C|c)(O|o)(U|u)(N|n)(T|t)".
  AVG => "(A|a)(V|v)(G|g)".
  MAX => "(M|m)(A|a)(X|x)".
  MIN => "(M|m)(I|i)(N|n)".
  SUM => "(S|s)(U|u)(M|m)".
  FROM => "(F|f)(R|r)(O|o)(M|m)".
  WHERE => "(W|w)(H|h)(E|e)(R|r)(E|e)".
  GROUP => "(G|g)(R|r)(O|o)(U|u)(P|p)".
  BY => "(B|b)(Y|y)".
  HAVING => "(H|h)(A|a)(V|v)(I|i)(N|n)(G|g)".
  ORDER => "(O|o)(R|r)(D|d)(E|e)(R|r)".
  CROSS => "(C|c)(R|r)(O|o)(S|s)(S|s)".
  NATURAL => "(N|n)(A|a)(T|t)(U|u)(R|r)(A|a)(L|l)".
  LEFT => "(L|l)(E|e)(F|f)(T|t)".
  RIGHT => "(R|r)(I|i)(G|g)(H|h)(T|t)".
  OUTER => "(O|o)(U|u)(T|t)(E|e)(R|r)".
  INNER => "(I|i)(N|n)(N|n)(E|e)(R|r)".
  JOIN => "(J|j)(O|o)(I|i)(N|n)".
  ON => "(O|o)(N|n)".
  USING => "(U|u)(S|s)(I|i)(N|n)(G|g)".
  CASE => "(C|c)(A|a)(S|s)(E|e)".
  WHEN => "(W|w)(H|h)(E|e)(N|n)".
  THEN => "(T|t)(H|h)(E|e)(N|n)".
  ELSE => "(E|e)(L|l)(S|s)(E|e)".
  END => "(E|e)(N|n)(D|d)".
  COLLATE => "(C|c)(O|o)(L|l)(L|l)(A|a)(T|t)(E|e)".
  ASC => "(A|a)(S|s)(C|c)".
  DESC => "(D|d)(E|e)(S|s)(C|c)".
  EXTRACT => "(E|e)(X|x)(T|t)(R|r)(A|a)(C|c)(T|t)".

  identifier => "([a-z]|[A-Z]|_|ä|ö|ü|Ä|Ö|Ü|ß)([a-z]|[A-Z]|[0-9]|_|ä|ö|ü|Ä|Ö|Ü|ß)*" BOTTOM.

  UnsignedNumber => "(0|[1-9][0-9]*)(\\.[0-9]+)?((e|E)(\\+|\\-)?[0-9]+)?".

  multilineComment => SEPARATOR "/\\*(^\\*|\\*^/)*\\*/" BOTTOM.
  comment => SEPARATOR "\\-\\-(^\\n)*" TOP.

  separator => SEPARATOR " " BOTTOM.
  newline => SEPARATOR "\\n" BOTTOM.
  newLineWindows => SEPARATOR "\\r" BOTTOM.
  tab => SEPARATOR "\\t" BOTTOM.

Tests:
    T6e "SELECT DISTINCT matrNo FROM

         leistung
         	JOIN
         (SELECT t1.datum AS anfang, t2.datum AS ende
         	FROM semesterbeitrag t1, semesterbeitrag t2
         		WHERE  t2.datum <= ALL (SELECT datum FROM semesterbeitrag AS s WHERE s > t1.datum)
         ) semester
             ON 	datum >= anfang AND datum < ende

         WHERE matrNo IN

         (SELECT matrNo
         FROM
         	(SELECT matrNo, SUM(betrag) AS gezahlt
         	FROM zahlung
         	WHERE datum < semester.ende
         	GROUP BY matrNo) gezahltSummen

         	NATURAL JOIN

         	(SELECT matrNo, SUM(geforderterBetrag) AS gefordert
         	FROM semesterbeitrag
         	WHERE datum < semester.ende
         	GROUP BY matrNo) gefordertSummen
         WHERE gefordert > gezahlt

         UNION

         SELECT matrNo, SUM(geforderterBetrag) AS differenz
         FROM semesterbeitrag
         WHERE matrNo NOT IN (SELECT matrNo FROM semesterbeitrag)
         AND datum < semester.ende
         GROUP BY matrNo)".

    TWTF "SELECT matrNo FROM
          	(SELECT matrNo
          	FROM zahlung)".

    TCustomFunction "SELECT Alexander(Sadowski)".

    TCustomFunction2 "SELECT HUHAHA(), HAhUHI(gutenTag, 'guten Morgen') FROM abc".

    TAllColumnsBegin "SELECT *, Alex FROM Alexander, Sadowski".
    TAllColumnsEndFail "SELECT Alex, Alexander, * FROM Sadowski, Sadowski" FAIL_PARSING.
    _ "selEcT ALEXANDER fRoM Sadowski".
    _ "SELECT stefan FROM hanenberg".
    TColumnDot "( SELECT B FROM TEACHES B)".
/*
    TColumnDot12 "( SELECT B FROM TEACHES WHERE YEAR = 1998 )".
    TColumnDot1 "( SELECT B FROM TEACHES B WHERE YEAR = 1998 )".
    TColumnDot2 "( SELECT B.COURSEID FROM TEACHES B WHERE YEAR = 1998 )".
*/
    TComment "SELECT
                -- blabla
                * FROM dings WHERE
                he
                -- lol
                IS NOT NULL".

    TComma "SELECT lal FROM lil, lel, lol, lal".

    TCommaAs "SELECT lal FROM lil, lel, lol lal".

    TComma_Fail "SELECT lal FROm lil, lel, lal," FAIL_PARSING.

    TVL7_1 "SELECT * FROM Auftrag WHERE datum = DATE '1919-12-19'".
    TVL7_1 "SELECT * FROM Auftrag WHERE datum = INTERVAL '1919-12-19' MONTH".

    TVL7 "SELECT * FROM Auftrag WHERE datum IN (DATE '1919-12-19', DATE '1919-12-19' + INTERVAL '1' MONTH)".

    TVL1 "SELECT auftragsNr, datum FROM Auftrag WHERE zuliefererNr=121212".

    TVL2 "SELECT * FROM Auftrag WHERE datum BETWEEN DATE '2001-01-01' AND DATE '2001-12-31'".

    TVL3 "SELECT * FROM Auftrag WHERE datum BETWEEN DATE '2001-12-31' AND DATE '2001-01-01'".

    TVL4 "SELECT * FROM Kunden WHERE rabatt IN (5.50, 7.50, 10.00)".

    TVL5 "SELECT * FROM Kunden WHERE rabatt NOT IN (5.50, 7.50, 10.00)".

    TVL6 "SELECT * FROM Kunden WHERE NOT rabatt IN (5.50, 7.50, 10.00)".

    TVL7 "SELECT * FROM Auftrag WHERE datum IN (DATE '1919-12-19', DATE '1919-12-19' + INTERVAL '1' MONTH)".

    TVL8 "SELECT * FROM Kunden WHERE kundeName LIKE 'M_ _er'".

    TVL9 "SELECT * FROM Kunden WHERE kundeName LIKE 'M%'".

    TVL10 "SELECT * FROM dingsbums WHERE (TIME '09:00:00', INTERVAL '50' MINUTE) OVERLAPS (TIME '02:15:00', INTERVAL '6' HOUR)".

    TVL11 "SELECT * FROM dingsbums WHERE (TIME '09:00:00', INTERVAL '50' MINUTE) OVERLAPS (TIME '02:15:00', INTERVAL '6' HOUR)".

    TVL12 "SELECT * FROM dingsbums WHERE a OVERLAPS b".

    TVL13_5 "SELECT CAST ShortString AS SMALLINT FROM dingsbums".

    TVL13 "SELECT * FROM dingsbums WHERE CAST ShortString AS SMALLINT = 1".

    TVL13 "SELECT * FROM dingsbums WHERE plz = CAST((SELECT plz FROM Tabelle WHERE ort='Ilmenau') AS SMALLINT)".

    TVL14 "SELECT mitarbeiterNr, mitarbeiterName, mitarbeiterVorname, einstellung FROM Mitarbeiter WHERE einstellung<DATE '1965-01-01'".

    TVL15 "SELECT * FROM Mitarbeiter".

    TVL16 "SELECT Mitarbeiter.*, abteilungsName FROM Mitarbeiter NATURAL JOIN Abteilung".

    TVL17 "SELECT ProduktInformation.*, istBestand * stueckKosten AS nettoWert, istBestand * nettoPreis AS bruttoWert FROM (Produkt NATURAL JOIN ProduktLagertIn NATURAL JOIN ProduktLager) AS ProduktInformation".

    TVL18 "SELECT mitarbeiterName, (CURRENT_DATE - einstellung) AS betriebszugehörigkeit FROM Mitarbeiter".

    TVL19 "SELECT mitarbeiterName, (CURRENT_DATE - einstellung) betriebszugehörigkeit FROM Mitarbeiter".



    TVL20_1 "SELECT mitarbeiterName AS neueMitarbeiter

            FROM Mitarbeiter ".

    TVL20_2 "SELECT a

            FROM Mitarbeiter

            WHERE CURRENT_DATE - einstellung < INTERVAL '24' MONTH".

    TVL20_3_1 "SELECT
                                  CURRENT_DATE - einstellung AS betriebszugehörigkeit
                          FROM Mitarbeiter".

    TVL20_3 "SELECT
                    (CURRENT_DATE - einstellung) AS betriebszugehörigkeit
            FROM Mitarbeiter".



    TVL20_4 "SELECT mitarbeiterName AS neueMitarbeiter,
                    (CURRENT_DATE - einstellung) AS betriebszugehörigkeit

            FROM Mitarbeiter

            WHERE CURRENT_DATE - einstellung < INTERVAL '24' MONTH".



/*
    TVL20 "SELECT mitarbeiterName AS neueMitarbeiter, (CURRENT_ DATE - einstellung) AS betriebszugehörigkeit FROM Mitarbeiter WHERE CURRENT_DATE - einstellung < INTERVAL '24' MONTH".
*/


/* (?)
    TVL21 "SELECT Mitarbeiter.*, (gehalt*13 + 500,00)/(220*8) AS stundenlohn FROM Mitarbeiter".
*/
    TVL21 "SELECT Mitarbeiter.*, (gehalt*13 + 500.00)/(220*8) AS stundenlohn FROM Mitarbeiter".


    TVL22 "SELECT COUNT(mitarbeiterNr) AS anzahlMitarbeiter FROM Mitarbeiter".

    TVL23 "SELECT MAX(gehalt) AS höchstesGehalt FROM Mitarbeiter".

    TVL24 "SELECT MIN(einstellung) AS ersteEinstellung, MAX(einstellung) AS letzteEinstellung FROM Mitarbeiter".

    TVL25 "SELECT SUM(gehalt) AS monatlicheGehaltsBelastung FROM Mitarbeiter".

    TVL26 "SELECT (AVG(nettoPreis)-AVG(stueckKosten))/AVG(nettoPreis)*100 AS gewinnMarge FROM Produkt".

    TVL27 "SELECT ((SUM(nettoPreis)-SUM(stueckKosten))/SUM(nettoPreis)*100) AS gewinnMarge FROM Produkt".

    TVL28 "SELECT AVG(rabatt) AS rabatt FROM Kunden".

    TVL29 "SELECT AVG(DISTINCT rabatt) AS rabatt FROM Kunden".

    TVL30 "SELECT COUNT(*) AS gesamtzahlKunden, COUNT(rabatt) AS gesamtzahlKundenMitRabatt, COUNT(DISTINCT ort) AS anzahlWohnorte FROM Kunden".

    TVL31 "SELECT mitarbeiterName, mitarbeiterVorname, gehalt FROM Mitarbeiter".

    TVL32 "SELECT auftragsNr, mitarbeiterName, mitarbeiterVorname FROM Auftrag, Mitarbeiter WHERE bearbeiterNr=mitarbeiterNr".

    TVL33 "SELECT auftragsNr, mitarbeiterName, mitarbeiterVorname FROM Auftrag CROSS JOIN Mitarbeiter WHERE bearbeiterNr=mitarbeiterNr".

    TVL34 "SELECT zuliefererName, teilNr, teilBez FROM Teil CROSS JOIN Zulieferer".

    TVL35 "SELECT mitarbeiterName, mitarbeiterVorname, abteilungsName FROM Mitarbeiter CROSS JOIN Abteilung WHERE Mitarbeiter.abteilungsNr=Abteilung.abteilungsNr".

    TVL36 "SELECT DISTINCT produktBez, abteilungsName FROM Produkt CROSS JOIN ArbeitetAn CROSS JOIN Abteilung WHERE Produkt.produktNr=ArbeitetAn.produktNr AND ArbeitetAn.abteilungsNr=Abteilung.abteilungsNr AND Produkt.produktTyp='Waschmaschine'".

    TVL37 "SELECT DISTINCT produktBez, abteilungsName FROM Produkt NATURAL JOIN ArbeitetAn NATURAL JOIN Abteilung WHERE Produkt.produktTyp='Waschmaschine'".

    TVL38 "SELECT Produkt.*, Abteilung.* FROM Produkt, ArbeitetAn, Abteilung WHERE Produkt.produktNr=ArbeitetAn.produktNr AND ArbeitetAn.abteilungsNr=Abteilung.abteilungsNr".

    TVL39 "SELECT P.*, A.* FROM Produkt P, ArbeitetAn AA, Abteilung A WHERE P.produktNr=AA.produktNr AND AA.abteilungsNr=A.abteilungsNr".

    TVL40 "SELECT * FROM Teil T, TeileLagertIn TLI, TeileLager TL
    WHERE T.teilNr=TLI.teilNr AND TLI.teileLagerNr=TL.teileLagerNr AND TL.teileLagerBez='Hochstapel'".

    TVL41 "SELECT T.teilNr, T.teilBez FROM Teil T, TeileLagertIn TLI, TeileLager TL WHERE T.teilNr=TLI.teilNr AND TLI.teileLagerNr=TL.teileLagerNr AND TL.teileLagerBez='Hochstapel'".

    TVL42 "SELECT Zulieferer.*, Teil.* FROM Zulieferer INNER JOIN Liefert USING (zuliefererNr) INNER JOIN Teil USING (teilNr)".

    TVL43 "SELECT Mitarbeiter.* FROM Mitarbeiter INNER JOIN Kunden ON (mitarbeiterNr=kundenNr)".

    TVL44 "SELECT * FROM Mitarbeiter AS M INNER JOIN Auftrag AS A ON M.mitarbeiterNr=A.mitarbeiterNr".

    TVL45 "SELECT * FROM Mitarbeiter INNER JOIN Auftrag USING (mitarbeiterNr)".

    TVL46 "SELECT M.* FROM Mitarbeiter AS M INNER JOIN Auftrag AS A ON (M.mitarbeiterNr=A.mitarbeiterNr AND A.datum=M.einstellung)".

    TVL47 "SELECT Z.* FROM Zulieferer AS Z INNER JOIN Auftrag AS A ON (Z.zuliefererNr=A.zuliefererNr) WHERE datum<DATE '2000-01-01'".

    TVL48 "SELECT Z.* FROM Zulieferer AS Z INNER JOIN Auftrag USING (zuliefererNr) WHERE datum<DATE '2000-01-01'".

    TVL49 "SELECT Z.* FROM Zulieferer AS Z NATURAL JOIN Auftrag WHERE datum<DATE '2000-01-01'".

    TVL50 "SELECT R.*, produktBez, stueckKosten, (betrag/menge) AS verkaufsPreis FROM Produkt AS P INNER JOIN Rechnungsposition AS R ON (P.produktNr=R.produktNr AND stueckKosten>(betrag/menge))".

    TVL51 "SELECT R.*, produktBez, stueckKosten, (betrag/menge) AS verkaufsPreis FROM Produkt".

    TVL52 "SELECT R.*, produktBez, stueckKosten, (betrag/menge) AS verkaufsPreis FROM Produkt AS P INNER JOIN Rechnungsposition AS R ON (stueckKosten>(betrag/menge)) WHERE P.produktNr=R.produktNr".

    TVL53 "SELECT Rechnungsposition.*, produktBez, stueckKosten, (betrag/menge) AS verkaufsPreis FROM Produkt NATURAL JOIN Rechnungsposition WHERE stueckKosten>(betrag/menge)".

    TVL54 "SELECT * FROM T1 LEFT OUTER JOIN T2 ON (a=d)".

    TVL55 "SELECT * FROM T1 INNER JOIN T2 ON (a=d)".

    TVL56 "SELECT DISTINCT T1.*, NULL AS d, NULL AS e, NULL
           AS f FROM T1 CROSS JOIN T2 WHERE a!=d".

    TVL57 "SELECT * FROM Produkt NATURAL LEFT OUTER JOIN ArbeitetAn NATURAL LEFT OUTER JOIN Abteilung".

    TVL58 "SELECT * FROM Produkt LEFT OUTER JOIN ArbeitetAn USING (produktNr) LEFT OUTER JOIN Abteilung USING (abteilungsNr)".

    TVL59 "SELECT * FROM Produkt LEFT OUTER JOIN ArbeitetAn ON (Produkt.produktNr=ArbeitetAn.produktNr) LEFT OUTER JOIN Abteilung ArbeitetAn ON (ArbeitetAn.abteilungsNr= Abteilung.abteilungsNr)".

    TVL60 "SELECT SUM(menge) FROM Zulieferer INNER JOIN Rechnung USING (zuliefererNr) INNER JOIN RechnungsPosition USING (rechnungsNr) WHERE zuliefererName='PasstNix GmbH' AND datum=DATE '2000-02-01'".

    TVL61 "SELECT Teil.* FROM Teil INNER JOIN SindBestandteilVon USING (teilNr) INNER JOIN Produkt USING (produktNr) WHERE produktBez='WM53'".

    TVL62 "SELECT Teil.* FROM Teil NATURAL JOIN SindBestandteilVon NATURAL JOIN Produkt WHERE produktBez='WM53'".

    TVL63 "SELECT teilNr, teilBez, teileLagerNr, teileLagerBez, zuliefererNr, zuliefererName FROM Zulieferer NATURAL JOIN Liefert NATURAL JOIN Teil NATURAL JOIN TeilLagertIn NATURAL JOIN TeileLager WHERE istBestand<50".

    TVL64 "SELECT * FROM VerheiratetePers INNER JOIN VerheiratetePers ON ehepartner=persNr".

    TVL65 "SELECT * FROM VerheiratetePers AS P1 INNER JOIN VerheiratetePers AS P2 ON (P1.ehepartner=P2.persNr)".

    TVL66 "SELECT * FROM Mitarbeiter WHERE mitarbeiterVorname=mitarbeiterName".

    TVL67 "SELECT produktNr FROM ProduktLagertIn WHERE istBestand<100".

    TVL67 "SELECT produktNr FROM ProduktLagertIn WHERE (a-e) <= (b*c+d)".

    TVL68 "SELECT * FROM Teil WHERE istBestand NOT BETWEEN 10 AND 1000".

    TVL69 "SELECT mitarbeiterName, mitarbeiterVorname FROM Mitarbeiter WHERE CURRENT_DATE-einstellung >= INTERVAL '25' YEAR AND CURRENT_DATE-einstellung < INTERVAL '26' YEAR".

    TVL70_1_FAIL "SELECT hi
            FROM ding INNER JOIN bing" FAIL_PARSING.

    TVL70_FAIL "SELECT M1.mitarbeiterNr, M1.mitarbeiterName, M1.mitarbeiterVorname, M2.mitarbeiterNr, M2.mitarbeiterName, M2.mitarbeiterVorname
                FROM Mitarbeiter AS M1 INNER JOIN Mitarbeiter AS M2
                WHERE (M1.mitarbeiterName=M2.mitarbeiterName) AND (M1.mitarbeiterNr!=M2.mitarbeiterNr)" FAIL_PARSING.

    TVL71 "SELECT M1.mitarbeiterNr, M1.mitarbeiterName, M1.mitarbeiterVorname, M2.mitarbeiterNr, M2.mitarbeiterName, M2.mitarbeiterVorname FROM Mitarbeiter AS M1 INNER JOIN Mitarbeiter AS M2 ON (M1.mitarbeiterName=M2.mitarbeiterName) AND (M1.mitarbeiterNr>M2.mitarbeiterNr)".

    TVL72 "SELECT Rechnung.* FROM Rechnung NATURAL JOIN RechnungsPosition NATURAL JOIN AuftragsPosition NATURAL JOIN Zulieferer WHERE zuliefererName='VersprechViel GmbH' AND datum BETWEEN DATE '2001-01-01' AND DATE '2001-12-31' AND teilNr=123321".

    TVL73 "SELECT * FROM Mitarbeiter WHERE abteilungsNr IS NULL".

    TVL74 "SELECT * FROM Zulieferer WHERE (ansprechPartner, ansprechpartTelNr) IS NULL".

    TVL75 "SELECT hi FROM bla WHERE a IS TRUE".

    TVL76 "SELECT hi FROM bla WHERE a IS NOT TRUE".

    TVL77 "SELECT hi FROM bla WHERE a IS FALSE".

    TVL78 "SELECT hi FROM bla WHERE a IS NOT FALSE".

    TVL79 "SELECT hi FROM bla WHERE a IS UNKNOWN".

    TVL80 "SELECT hi FROM bla WHERE a IS NOT UNKNOWN".

    TVL81 "SELECT produktLagerBez AS lager, COUNT(produktNr) AS produkte FROM ProduktLagertIn GROUP BY produktLagerBez".

    TVL82 "SELECT produktLagerBez AS lager, produktNr FROM ProduktLagertIn ORDER BY produktLagerBez".

    TVL83 "SELECT A.abteilungsName, M.mitarbeiterName, M.mitarbeiterVorname FROM Mitarbeiter AS M NATURAL JOIN Abteilung AS A GROUP BY A.abteilungsName".

    TVL84_09GROUPBYLOl "SELECT A.abteilungsName, M.mitarbeiterName, M.mitarbeiterVorname FROM Mitarbeiter AS M NATURAL JOIN Abteilung AS A ORDER BY A.abteilungsName, M.mitarbeiterName, M.mitarbeiterVorname".

    TVL85 "SELECT A.abteilungsName, M.mitarbeiterName, M.mitarbeiterVorname FROM Mitarbeiter AS M NATURAL JOIN Abteilung AS A ORDER BY A.abteilungsName, M.mitarbeiterName, M.mitarbeiterVorname".

    TVL86 "SELECT produktTyp, COUNT(kundenNr) AS kundenProProdukttyp FROM Kunden NATURAL JOIN Kauft NATURAL JOIN Produkt GROUP BY produktTyp".

    TVL87 "SELECT produktLagerBez AS Lager, COUNT(produktNr) AS produkte FROM ProduktLagertIn GROUP BY produktLagerBez HAVING produktLagerBez='München' OR produktLagerBez='Berlin'".

    TVL88 "SELECT produktLagerBez AS lager, COUNT(produktNr) AS produkte FROM ProduktLagertIn GROUP BY produktLagerBez HAVING istBestand<500".

    TVL89 "SELECT produktLagerBez AS lager, COUNT(produktNr) AS produkte FROM ProduktLagertIn WHERE istBestand<500 GROUP BY produktLagerBez".

    TVL90 "SELECT produktLagerBez AS lager, COUNT(produktNr) AS produkte FROM ProduktLagertIn GROUP BY produktLagerNr HAVING produktLagerBez='München' OR produktLagerBez='Berlin'".

    TVL91 "SELECT produktLagerBez AS lager, COUNT(produktNr) AS produkte, SUM(istBestand) AS gesamtMenge FROM ProduktLagertIn NATURAL JOIN ProduktLager GROUP BY produktLagerBez HAVING COUNT(produktNr)>300 AND SUM(istBestand)<10000".

    TVL92 "SELECT produktTyp, AVG(nettoPreis) AS nettoPreis FROM Produkt WHERE produktTyp NOT IN ('Geschirrspüler', 'Waschmaschine') GROUP BY produktTyp".

    TVL93 "SELECT produktTyp, AVG(nettoPreis) AS nettoPreis FROM Produkt GROUP BY produktTyp HAVING produktTyp NOT IN ('Geschirrspüler', 'Waschmaschine')".

    TVL94 "SELECT produktTyp, AVG(nettoPreis) AS nettoPreis FROM Produkt GROUP BY produktTyp HAVING AVG(nettoPreis)>500".

    TVL95 "SELECT COUNT(*) AS anzahlUmfangreicherRechnungen FROM RechnungsPosition GROUP BY rechnungsNr HAVING COUNT(rechnungsPos)>10".

    TVL96 "SELECT produktTyp, produktLagerNr, produktNr FROM ProduktLagertIn NATURAL JOIN Produkt WHERE istBestand>0 GROUP BY produktTyp, produktLagerNr".

    TVL97 "SELECT produktTyp, produktLagerNr FROM ProduktLagertIn NATURAL JOIN Produkt WHERE istBestand>0 GROUP BY produktTyp, produktLagerNr".

    TVL98 "SELECT produktTyp, produktLagerNr, AVG(nettoPreis) AS nettoPreis FROM ProduktLagertIn NATURAL JOIN Produkt WHERE istBestand>0 GROUP BY produktTyp, produktLagerNr".

    TVL99 "SELECT produktTyp, AVG(nettoPreis) AS nettoPreisÜberAlleLager
    FROM (SELECT produktTyp, produktLagerNr, AVG(nettoPreis) AS nettoPreis
            FROM ProduktLagertIn NATURAL JOIN Produkt
            WHERE istBestand>0 GROUP BY produktTyp, produktLagerNr)
    GROUP BY produktTyp".

    TVL100 "SELECT * FROM Mitarbeiter WHERE gehalt = (SELECT MAX(gehalt) FROM Mitarbeiter)".

    TVL101_FAIL "SELECT * FROM Mitarbeiter WHERE ort IN (SELECT ort FROM Mitarbeiter WHERE gehalt= (SELECT MIN(gehalt) FROM Mitarbeiter)" FAIL_PARSING.
    TVL101 "SELECT * FROM Mitarbeiter WHERE ort IN (SELECT ort FROM Mitarbeiter WHERE gehalt= (SELECT MIN(gehalt) FROM Mitarbeiter))".

    TVL102 "SELECT * FROM Mitarbeiter AS MA1 WHERE MA1.gehalt > (SELECT MIN(gehalt)*1,5 FROM Mitarbeiter AS MA2 WHERE MA1.abteilungsNr=MA2.abteilungsNr)".

    TVL103 "SELECT abteilungsNr, AVG(gehalt) FROM Mitarbeiter AS M1 WHERE gehalt >(SELECT AVG(gehalt) FROM Mitarbeiter AS M2 WHERE M1.abteilungsNr=M2.abteilungsNr) GROUP BY abteilungsNr".

    TVL104 "SELECT COUNT(produktLagerNr) FROM ProduktLagertIn GROUP BY produktLagerNr HAVING COUNT (produktNr)>(SELECT COUNT(*) FROM Produkt)/2".

    TVL105 "SELECT COUNT(produktLagerNr) FROM ProduktLagertIn NATURAL JOIN Produkt WHERE stueckKosten>500 GROUP BY produktLagerNr HAVING COUNT (produktNr)>(SELECT COUNT(*) FROM Produkt WHERE stueckKosten>500)/2".

    TVL106 "SELECT Mitarbeiter.*, gehalt - (SELECT AVG(gehalt) FROM Mitarbeiter) AS unterschiedZumDurchschnittsgehalt FROM Mitarbeiter".

    TVL107 "SELECT P.produktBez, P.produktNr, (SELECT SUM(istBestand) FROM ProduktLagertIn AS PLI GROUP BY produktNr HAVING P.produktNr=PLI.produktNr) AS lagerBestand FROM Produkt AS P".

    TVL108 "SELECT P.produktBez, P.produktNr, lagerBestand FROM Produkt NATURAL JOIN (SELECT produktNr, SUM(istBestand) AS lagerBestand FROM ProduktLagertIn GROUP BY produktNr)".

    TVL109 "SELECT produktBez, produktNr, SUM(istBestand) AS lagerBestand FROM Produkt NATURAL JOIN ProduktLagertIn GROUP BY produktNr".

    TVL110 "SELECT AVG(nettoPreis) AS nettoPreis FROM Produkt NATURAL JOIN (SELECT DISTINCT produktNr FROM ProduktLagertIn WHERE istBestand>0)".

    TVL111 "SELECT *
            FROM Abteilung
            WHERE budget> (SELECT budget
            FROM Abteilung
            WHERE abteilungsNr=121212)".

    TVL112 "SELECT *
            FROM Abteilung
            WHERE (budget, mitarbeiterZahl)=
            (SELECT budget, mitarbeiterZahl
            FROM Abteilung
            WHERE abteilungsNr=121212)".



    TVL113 "SELECT *
            FROM Abteilung
            WHERE budget= (SELECT budget
            FROM Abteilung
            WHERE abteilungsNr=121212
            AND
            mitarbeiterZahl=(SELECT mitarbeiterZahl
            FROM Abteilung
            WHERE abteilungsNr=121212))".

    TVL113_FAIL "SELECT *
            FROM Abteilung
            WHERE budget= (SELECT budget
            FROM Abteilung
            WHERE abteilungsNr=121212;
            AND
            mitarbeiterZahl=(SELECT mitarbeiterZahl
            FROM Abteilung
            WHERE abteilungsNr=121212" FAIL_PARSING.
    TVL114 "SELECT *
            FROM Rechnung
            WHERE zuliefererNr IN (SELECT zuliefererNr
            FROM Zulieferer
            WHERE ort='GroßKleckersdorf')".

    TVL115 "SELECT Rechnung.*
             FROM Rechnung NATURAL JOIN Zulieferer
              WHERE (zuliefererName, datum) IN
                       (('Abzock', CURRENT_DATE-INTERVAL '1' DAY),
                       ('Abzock', CURRENT_DATE-INTERVAL '2' DAY),
                       ('Chaotikus', CURRENT_DATE-INTERVAL '1' DAY),
                       ('Chaotikus', CURRENT_DATE-INTERVAL '2' DAY))".

    TVL115_FAIL "SELECT Rechnung.*
            FROM Rechnung NATURAL JOIN Zulieferer
            WHERE (zuliefererName, datum) IN
            (('Abzock', CURRENT_DATE-INTERVALL '1' DAY),
            ('Abzock', CURRENT_DATE-INTERVALL '2' DAY),
            ('Chaotikus', CURRENT_DATE-INTERVALL '1' DAY),
            ('Chaotikus', CURRENT_DATE-INTERVALL '2' DAY))" FAIL_PARSING.

    TVL116 "SELECT Rechnung.*
            FROM Rechnung NATURAL JOIN Zulieferer
            WHERE datum IN (CURRENT_DATE-INTERVAL '1' DAY,
            CURRENT_DATE-INTERVAL '2' DAY) AND
            zuliefererName IN ('Abzock', 'Chaotikus')".

    TVL116_FAIL "SELECT Rechnung.*
                FROM Rechnung NATURAL JOIN Zulieferer
                WHERE datum IN (CURRENT_DATE-INTERVALL '1' DAY,
                CURRENT_DATE-INTERVALL '2' DAY) AND
                zuliefererName IN ('Abzock', 'Chaotikus')" FAIL_PARSING.

    TVL117 "SELECT mitarbeiterName, mitarbeiterVorname
            FROM Mitarbeiter
            WHERE (mitarbeiterName, mitarbeiterVorname) IN
            (SELECT kundenName, kundenVorname
            FROM Kunden
            WHERE rabatt<10 AND
            kundenVorname LIKE 'G%')".

    TVL118 "SELECT mitarbeiterName, mitarbeiterVorname
            FROM Mitarbeiter
            WHERE mitarbeiterVorname LIKE 'G%' AND
            mitarbeiterNr IN
            (SELECT kundenNr
            FROM Kunden
            WHERE rabatt<10)".

    TVL119 "SELECT mitarbeiterName, mitarbeiterVorname
            FROM Mitarbeiter
            WHERE (mitarbeiterName, mitarbeiterVorname)=ANY
            (SELECT kundenName, kundenVorname
            FROM Kunden
            WHERE rabatt<10 AND
            kundenVorname LIKE 'G%')".

    TVL120 "SELECT mitarbeiterName, mitarbeiterVorname
            FROM Mitarbeiter
            WHERE mitarbeiterVorname LIKE 'G%' AND
            mitarbeiterNr IN
            (SELECT kundenNr
            FROM Kunden
            WHERE rabatt<10)".

    TVL121 "SELECT mitarbeiterName, mitarbeiterVorname
            FROM Mitarbeiter
            WHERE (mitarbeiterName, mitarbeiterVorname)=ANY
            (SELECT kundenName, kundenVorname
            FROM Kunden
            WHERE rabatt<10 AND
            kundenVorname LIKE 'G%')".


    TVL122 "SELECT mitarbeiterName, mitarbeiterVorname
            FROM Mitarbeiter
            WHERE mitarbeiterNr IN
            (SELECT bearbeiterNr
            FROM Auftrag
            WHERE datum BETWEEN DATE '2000-01-01'
            AND DATE '2000-12-31')".


    TVL123 "SELECT mitarbeiterName, mitarbeiterVorname
            FROM Mitarbeiter
            WHERE EXISTS
            (SELECT *
            FROM Auftrag
            WHERE datum BETWEEN DATE '2000-01-01'
            AND DATE '2000-12-31'
            AND mitarbeiterNr=bearbeiterNr)".

    TVL124 "SELECT produktTyp, AVG(nettoPreis)
            AS nettoPreisÜberAlleLager
            FROM (SELECT produktTyp, produktLagerNr,
            AVG(nettoPreis) AS nettoPreis
            FROM ProduktLagertIn NATURAL JOIN Produkt
            WHERE lagerbestand>0
            GROUP BY produktTyp, produktLagerNr)
            GROUP BY produktTyp".


    TVL125_FAIL "
            (SELECT produktTyp, produktLagerNr, AVG(nettoPreis)
            AS nettoPreis
            FROM ProduktLagertIn NATURAL JOIN Produkt
            WHERE lagerbestand>0
            GROUP BY produktTyp, produktLagerNr) AS
            ProdukttypLager" FAIL_PARSING.

      TVL125 "SELECT * FROM (SELECT produktTyp, produktLagerNr, AVG(nettoPreis)
                AS nettoPreis
                FROM ProduktLagertIn NATURAL JOIN Produkt
                WHERE lagerbestand>0
                GROUP BY produktTyp, produktLagerNr) AS
                ProdukttypLager".


    TVL126 "SELECT produktTyp, AVG(nettoPreis) AS nettoPreisÜberAlleLager FROM ProdukttypLager GROUP BY produktTyp".

    TVL127 "SELECT *
            FROM Kunden AS K1
            WHERE rabatt > (SELECT AVG(rabatt)
            FROM Kunden AS K2
            WHERE K1
            .ort=K2
            .ort AND
            K1
            .kundenNr <> K2.kundenNr)".

    TVL128 "SELECT kundenName AS name, plz, ort, strasse, hausNr
            FROM Kunden
            UNION
            SELECT zuliefererName AS name, plz, ort, strasse,
            hausNr
            FROM Zulieferer
            UNION
            SELECT mitarbeiterName AS name, plz, ort, strasse, hausNr
            FROM Mitarbeiter".


    TVL129 "SELECT kundenNr AS identifikationsNr, kundenName AS
            name, kundenVorname AS vorname
            FROM Kunden
            INTERSECT
            SELECT mitarbeiterNr AS identifikationsNr, mitarbeiterName
            AS name, mitarbeiterVorname AS vorname
            FROM Mitarbeiter".

    TVL130 "SELECT kundenNr, kundenName, kundenVorname
            FROM Kunden
            WHERE EXISTS
            (SELECT *
            FROM Mitarbeiter
            WHERE kundenNr=mitarbeiterNr)".

    TVL131 "SELECT kundenNr, kundenName, kundenVorname
            FROM Kunden
            WHERE kundenNr = SOME
            (SELECT kundenNr AS identifikationsNr
            FROM Kunden
            INTERSECT
            SELECT mitarbeiterNr AS identifikationsNr
            FROM Mitarbeiter)".

    TVL132 "SELECT kundenNr, kundenName, kundenVorname
            FROM Kunden
            WHERE kundenNr= SOME
            (SELECT mitarbeiterNr
            FROM Mitarbeiter)".

    TVL133 "SELECT kundenNr AS identifikationsNr, kundenName AS
            name, kundenVorname AS vorname
            FROM Kunden
            EXCEPT
            SELECT mitarbeiterNr AS identifikationsNr, mitarbeiterName
            AS name, mitarbeiterVorname AS vorname
            FROM Mitarbeiter".

    TVL134 "SELECT kundenNr, kundenName, kundenVorname
            FROM Kunden
            WHERE NOT EXISTS (SELECT *
            FROM Mitarbeiter
            WHERE kundenNr=mitarbeiterNr)".

    TVL135 "SELECT kundenNr, kundenName, kundenVorname
            FROM Kunden
            WHERE kundenNr = SOME (SELECT kundenNr AS
            identifikationsNr
            FROM Kunden
            EXCEPT
            SELECT mitarbeiterNr AS
            identifikationsNr
            FROM Mitarbeiter)".

    TVL136 "SELECT kundenNr, kundenName, kundenVorname
            FROM Kunden
            WHERE kundenNr != ALL (SELECT mitarbeiterNr
            FROM Mitarbeiter)".

    TVL137 "SELECT mitarbeiterName, mitarbeiterVorname
            FROM Mitarbeiter
            ORDER BY mitarbeiterName, mitarbeiterVorname".

    TVL138 "SELECT datum, bearbeiterNr, auftragsNr,
            auftragsPos, menge
            FROM Auftrag NATURAL JOIN Auftragsposition
            WHERE datum BETWEEN DATE '2000-01-01' AND
            DATE '2000-12-31'
            ORDER BY datum, bearbeiterNr".



    TVL_FINAL_1 "SELECT produktTyp, produktLagerBez, AVG(nettoPreis) AS nettoPreis,
                 COUNT(produktNr) AS anzahlUnterschiedlicherProdukte

                 FROM Produkt NATURAL JOIN ProduktLagertIn NATURAL JOIN ProduktLager
                 ".

    TVL_FINAL_2_3 "SELECT *
                        FROM A
                        WHERE produktTyp='Geschirrspüler'
                         AND produktBez LIKE 'GS%'
                         AND produktBez LIKE 'WM%' ".

    TVL_FINAL_2_2 "SELECT *
                    FROM A
                    WHERE produktTyp='Geschirrspüler'
                     AND produktBez LIKE 'GS%'
            OR produktTyp='Waschmaschine'
                     AND produktBez LIKE 'WM%' ".



    TVL_FINAL_2_1 "SELECT *
                  FROM A
                  WHERE (produktTyp='Geschirrspüler' AND produktBez LIKE 'GS%' OR produktTyp='Waschmaschine'
                                     AND produktBez LIKE 'WM%')".

    TVL_FINAL_2 "SELECT *
                  FROM A
                  WHERE (produktTyp='Geschirrspüler' AND produktBez LIKE 'GS%' OR produktTyp='Waschmaschine'
                                     AND produktBez LIKE 'WM%') AND
                                     produktLagerBez NOT IN
                                     ('Essen', 'München')
                     ".


    TVL_FINALBOSS "SELECT produktTyp, produktLagerBez, AVG(nettoPreis) AS nettoPreis,
                          COUNT(produktNr) AS anzahlUnterschiedlicherProdukte

                   FROM Produkt NATURAL JOIN ProduktLagertIn NATURAL JOIN ProduktLager

                   WHERE (produktTyp='Geschirrspüler' AND produktBez LIKE 'GS%' OR produktTyp='Waschmaschine'
                   AND produktBez LIKE 'WM%') AND
                   pro - duktLagerBez NOT IN
                   ('Essen', 'München')
                   GROUP BY produktTyp, produktLagerBez
                   HAVING COUNT(produktNr) BETWEEN 5 AND 20
                   ORDER BY nettoPreis, produktTyp DESC".




    TDings "SELECT " FAIL_PARSING.


    TComment "SELECT /* asldkjfs///dklfj */ lol FROM ding".
    T0 "SELECT 1dfk  FROM a" . /* (?) <--- Syntaktisch korrekte Kacke! :D */


    T123 "SELECT dings FROM ((SELECT dings FROM dangs))".

    T123 "SELECT dings FROM ((SELECT dings FROM dangs) AS da)".




    T999 "SELECT lol FROM ((SELECT hi FROM lal))".
    T55 "SELECT hi FROM ding".

    TJOIN_USING "SELECT hi FROM ding JOIN bing USING(a,b,c)".

    TWHERE_1 "SELECT hi FROM ding WHERE a = 1".

    TWHERE_LIKE "SELECT hi FROM ding WHERE a LIKE 'dings'".

    TWHERE_IN "SELECT hi FROM ding WHERE lol IN ( (SELECT hi FROM dings),(SELECT ding FROM Dingding) )".
    TWHERE_IN2 "SELECT hi FROM ding WHERE a IN (SELECT hi FROM (lol NATURAL JOIN beb) JOIN ding ON true WHERE lol = 1)".

    TWHERE_QUANT_ALL "SELECT hi FROM ding WHERE lol = ALL(SELECT dingsbums FROM lolol)". /* (?) ALL mit eigener Liste möglich? */
    TWHERE_QUANT_LIKE_ANY "SELECT lol FROM lala WHERE lol LIKE ANY (SELECT hi FROM (ding JOIN LAL ON (true-true)) WHERE (xD - (2 + 1) = 100) AND 1 = 1)".

    TWHERE_EXISTS "SELECT lolwiegehts FROM ding WHERE EXISTS (SELECT dingdong FROM hahaha)".

    THeftigeKlammern "SELECT lolwiegehts FROM (ding JOIN bla ON ((NOT (bla) AND ble) OR blo IS NOT NULL))".

    T_Using "SELECT lol FROM lol JOIN a USING(a,b,c)".

    T1 "§" FAIL_PARSING FAIL_SCANNING.
    T2 "SELECT hi FROM asd".
    T12 "SELECT ? FROM lal".
    T55 "SELECT" FAIL_PARSING.
    T10 "SELECT 10000 + hopplaboppla - dingsbums+ -10505 + lol - -hi FROM wiegehtsdirso".
    T3 "SELECT * FROM LAL".
    T4 "SELECT hi, di, bye FROM lel".
    T5 "SELECT hi+54 FROM dai".
    T6 "SELECT hi+0 FROM dings".
    T7 "SELECT hi-100, hi-1043 FROM dingdong".
    T8 "SELECT 10+10, 10, hi-555 FROM tralala".
    T9 "SELECT 10-10+2130-tralala+dingdong - -5 FROM dingsbums".
    T11 "SELECT FF FROM wiegehtsdirso".
    T13 "SELECT ?, hi, ?, 5+dings FROM dong".
    T14 "SELECT 'lal' FROM lol".

    T26 "SELECT '0110' FROM yey".
    T27 "SELECT 'FFA' FROM yup".
    T28 "SELECT hi.* FROM dings".
    T29 "SELECT hi.*, '0110', dindgong FROM dings".

    T33 "SELECT asd    FROM lalas".
    T34 "SELECT lol.hi FROM lelel".
    T35 "SELECT COUNT(*) FROM lal".

    T37 "SELECT AVG ( dongs ) FROM lel".

    T38 "SELECT AVG('WTF') FROM lal".


    T39_1 "SELECT AVG('WTF'), MAX(B '1101') FROM dings".
    T39 "SELECT AVG('WTF'), MAX(f+a+N'bob'), MIN(dingsbums-dingdong*wiegehts), SUM(B'1001' * hi) FROM dings".
    T40 "SELECT 'AVG' FROM dings".
    T41 "SELECT dingdong AS bingbong, '1001' AS dingsbums FROM dingdong".
    T42 "SELECT (SELECT dings FROM dings) FROM dings".
    T43 "SELECT NULLIF(1, 2) FROM dings".
    T44 "SELECT COALESCE('hi','bye', dong) FROM dingdong".

    T45 "SELECT firstjoin FROM a CROSS JOIN b".

    T46 "SELECT secondjoin FROM a JOIN b ON c JOIN b ON f".


    T47 "SELECT thirdjoin FROM abc NATURAL JOIN abc NATURAL LEFT JOIN baba".
    T48 "SELECT thirdjoin FROM (SELECT dings FROM dangs) bum NATURAL JOIN baba".
    T49 "SELECT a FROM b JOIN c ON a".
    T50 "SELECT a FROM b NATURAL JOIN c".
    T51 "SELECT dingdong FROM (a NATURAL JOIN b) AS c(a,b,c)".


    T52 "SELECT dingdong FROM (a NATURAL JOIN b) AS a NATURAL JOIN c".





    T53 "SELECT dongdong FROM (a JOIN b ON c) AS b NATURAL JOIN (dingdongs NATURAL JOIN b) AS c".
    T54 "SELECT a FROM b JOIN c ON z = z".


    T55 "SELECT a FROM b NATURAL JOIN (c NATURAL JOIN b) AS bum".

    T56 "SELECT a FROM b NATURAL JOIN (SELECT a FROM b) AS c".

    T57 "SELECT (SELECT (SELECT a FROM bbb) FROM aaa) AS lal FROM dingdong NATURAL JOIN dingdong".
    T58 "SELECT dingsbums FROM (b FULL OUTER JOIN a ON b) JOIN a ON c".
    T59 "SELECT lal FROM ((a JOIN b ON c IN (a, b, c)) AS c NATURAL JOIN b) JOIN (SELECT hi FROM ding) AS lol ON bla.a = ble.a".

    T60 "SELECT hi FROM a WHERE a = b".
    T61 "SELECT hi FROM a WHERE a = b AND a = c".
    T62 "SELECT hi FROM a WHERE A = 5 AND a+b=5".
    T63 "SELECT hi FROM a WHERE (a=5 OR a-5=5) AND NOT a =5".
    T64 "SELECT hi FROM a WHERE 'hi' = my-5".
    T65 "SELECT hi FROM b WHERE a IS NOT NULL".
    T66 "SELECT byebye FROM b WHERE (a IS NOT NULL OR b=a) AND (b-10 IN (SELECT hi FROM b NATURAL JOIN c))".
    T67 "SELECT dingdong FROM b WHERE (a = 'bingbong') AND dingdong=5".
    T68 "SELECT bongdong FROM bangbang WHERE a =1 GROUP BY bingbong HAVING bingbong > COUNT(dings)".
    T69 "SELECT dings FROM dongs GROUP BY a,b,c".
    T70 "SELECT lal FROM dingsbums ORDER BY lal ASC, dings DESC, lol".
    T1 "SELECT B '0110' FROM lol".
    T100 "SELECT hi FROM dings WHERE bom > bim".


''');
exampleStrings.add('''


load library util;
load library htmlConstruction;



var nodesToHTML;
var htmlString;
var bla;
var htmlTree;




var treeTestsFailed;



var dffText;



var genericTreeGrammar;



drawEditor aNode aNodeList {
    htmlString = "";
    ->(constructHTMLString)([aNode], rootResultNode, [aNodeList]);
    return htmlString;
}

startOpenTag aNode aNodeList {

    htmlString = concatAll(htmlString, ["<",
                                      slotNodeName(aNode),
                                      " ",
                                      html_generateIDString(aNode, aNodeList)
                                      ]
                 );

}

startDivTagClass aNode aNodeList {
    htmlString = concatAll(htmlString, ["<div class='",
                                      slotNodeName(aNode),
                                      "' ",
                                      html_generateIDString(aNode, aNodeList)
                                      ]
                 );

}

addAttribute attName attValue {
    htmlString = concatAll(htmlString, [
                                " ",
                                attName,
                                "='",
                                attValue,
                                "'"
                           ]
                 );
}

endOpenTag {
    htmlString = concat(htmlString, ">");
}


closeTag aNode {
    htmlString = concat(htmlString, html_createCloseTagFromNode(aNode));
}

closeTagString aString {
    htmlString = concatAll(htmlString, ["</", aString, ">"]);
}

nodeListToString nodeList {
    var theString = "";
    forEach(child in children(nodeList)) {
        theString = concatAll(theString, ["#", slotNodeName(child)]);
    };
    return theString;
}




main {
    initConstructFromTreeString();
    /*
    callReplaceResultFunction();
    testEqualTrees();
    testConstructFromTreeString();*/

    /*runTreeTests();*/

    printString("Done Slope");
    printString(constructTreeString(rootGrammarNode));
    var smallerSlopeTree = constructSmallerSlopeTree(rootGrammarNode);
    printString("Done Smaller Slope");
    printString(constructTreeString(smallerSlopeTree));
    var sqlTree = constructSQLTree(smallerSlopeTree);
    printString("Done Data Tree");
    printString(constructTreeString(sqlTree));
    htmlTree = constructSQLHTMLTree(sqlTree);

/*
    htmlTree = #"ROOT" [
                +#"block round" [
                   +#"blockDescriptor" {
                       +#"blockDescriptorText" {
                           +#"TEXT" {
                               +#"abc";
                           };
                       };
                   };
                   +#"blockContent" {

                   };
                ];
              ];

    htmlTree = #"ROOT" [
                    +#"block round" [
                        +#"blockDescriptorText" {
                            +#"TEXT" {
                                +#"abc";
                            };
                        };
                    ];
                    +#"blockContent";
                ];*/
    printString(constructTreeString(htmlTree));
    printString("Done HTML Tree");
    bla = false;
    return redraw();

}

redraw {
        nodesToHTML = [];
        return drawEditor(htmlTree, nodesToHTML);
}

handleEvent anEventTreeNode anID anHTMLObject {
    if (isNotNull(anID)) {
        var slotTreeNode = elementAt(nodesToHTML, anID);
        ->(doMouseHandleEvent)([slotTreeNode, anEventTreeNode], rootResultNode, [anHTMLObject]);
    } else {
        ->(doKeyboardHandleEvent)([anEventTreeNode], rootResultNode, [anHTMLObject]);
    };
}



constructSmallerSlopeTree slopeNode {
    var root = #"";
    ->(_constructSmallerSlopeTree)([slopeNode], root);
    return root.#1;
}

isNamedAnd andTreeNode {
    if(equalsAny(andTreeNode, [
        "OR",
        "OPTIONAL",
        "BRACKET",
        "NFOLD",
        "AND",
        "NULL",
        "SLOPETOKEN",
         "INDEX"])) {
        return false;
    } else {
        return true;
    };
}

equalsAny pNode nameList {
    forEach(name in nameList) {
        if(nodeNameEquals(pNode, name)) {
            return true;
        };
    };
    return false;
}



isAnyTermExpr node {
    return or(
        equals("termexpr",  slotNodeName(node)),
        equals("termexpr1", slotNodeName(node))
    );
}

isPlusMinusConcatMultDivide node {
    return or(
        equals("termexprPlusMinusConcat", slotNodeName(node)),
        equals("termexpr1MultDivide", slotNodeName(node))
      );
}



constructSQLTree parseTree {
    var sql = #"SQL";
    ->(constructSQLTree_t
     | constructSQLBooleanTermTree
     | constructSQLExpressionTree
     | constructSQLFROMClauseTree
     | sql_doForChildren)([parseTree], sql);
    return sql;
}

isExceptOrUnionOrIntersect node {
    return or(
            equals(slotNodeName(node), "queryUnion"),
            equals(slotNodeName(node), "queryExcept"),
            equals(slotNodeName(node), "queryIntersect")
          );
}

isQueryOrIntersectableQuery node {
    return or(
                equals(slotNodeName(node), "query"),
                equals(slotNodeName(node), "intersectableQuery")
              );
}



constructSQLHTMLTree dataTree {
    var root = #"ROOT";
    ->(constructSQLHTMLTree_t
     | constructSQLBooleanTermHTMLTree
     | constructSQLFROMClauseHTMLTree
     | constructSQLExpressionHTMLTree)
     ([dataTree], root);

    return root;
}


generateRoundBlock description {
    var block =
    #"block round" [
        +#"blockDescriptor" [
            +#"blockDescriptorText" [
                +#"TEXT" {
                    +#description;
                };
            ];
        ];
        +#"blockContent" [

        ];
    ];
    return block;
}





runFromClauseTests {


        runSQLTreeTest(
                        "select alexander FROM sadowski CROSS JOIN (alex, lol)",
                        "#SELECT()"
                );

        runSQLTreeTest(
                             "select alexander
                             FROM sadowski NATURAL JOIN alex",
                             "#SELECT()"
                 );

        runSQLTreeTest(
             "select alexander FROM sadowski",
             "#SELECT(
                 #selectList(

                 )
             )"
         );

        runSQLTreeTest(
                     "select alexander
                     FROM sadowski NATURAL JOIN alex",
                     "#SELECT()"
         );






}

runSelectExpressionTests {

runSQLTreeTest(
      "select cOuNT(*), COUNT(hi), sUm(dIsTinCT wieGehts)",
          "#SELECT(
              #selectList(

              )
  )");

    runSQLTreeTest(
      "select EXTRACT(YEAR FROM sadowski)",
          "#SELECT(
              #selectList(

              )
  )");

     runSQLTreeTest(
                          "select EXTRACT(YEAR FROM sadowski)",
                              "#SELECT(
                                  #selectList(

                                  )
                      )");


     runSQLTreeTest(
                     "select -42, -64++47",
                         "#SELECT(
                             #selectList(

                             )
                 )");

     runSQLTreeTest(
                "select (mister, bean) = (alexander.sadowski, 42)",
                    "#SELECT(
                        #selectList(

                        )
            )");

    runSQLTreeTest(
            "select CAST(ble AS int),
                    CAST ble AS int",
                "#SELECT(
                    #selectList(

                    )
        )");

    runSQLTreeTest(
        "select customfunc(gd), AVG(DISTINCT human),
        COUNT(*)",
            "#SELECT(
                #selectList(

                )
    )");


    runSQLTreeTest(
                    "select
                            abc NOT LIKE 1",
                    "#SELECT(
                        #selectList(

                        )
        )");


    runSQLTreeTest("select abc IS NOT FALSE",
                            "#SELECT(
                                #selectList(

                                )
                )");

    runSQLTreeTest("select abc IS TRUE",
                        "#SELECT(
                            #selectList(

                            )
            )");

    runSQLTreeTest("select abc IS NOT NULL",
                        "#SELECT(
                            #selectList(

                            )
            )");

    runSQLTreeTest("select abc IS NULL",
                        "#SELECT(
                            #selectList(

                            )
            )");



    runSQLTreeTest(
                "select
                        abc LIKE (1, 2, (SELECT hallo))",
                "#SELECT(
                    #selectList(

                    )
    )");

    runSQLTreeTest(
            "select
                    abc NOT IN (1, 2, (SELECT hallo))",
            "#SELECT(
                #selectList(

                )
            )");

    runSQLTreeTest(
        "select
                abc IN (1, 2, gutenTag)",
        "#SELECT(
            #selectList(

            )
        )");




    runSQLTreeTest(
        "select
                abc NOT IN (SELECT gutenTag)",
        "#SELECT(
            #selectList(

            )
        )");

    runSQLTreeTest(
    "select
            abc IN (SELECT gutenTag)",
    "#SELECT(
        #selectList(

        )
    )");

    runSQLTreeTest(
        "select
                abc NOT BETWEEN a AND b,
                2 = 3,
                4 != 42",
        "#SELECT(
            #selectList(

            )
        )");

    runSQLTreeTest(
    "select
            abc BETWEEN a AND b,
            2 = 3,
            4 != 42",
    "#SELECT(
        #selectList(

        )
    )");


    runSQLTreeTest("select abc = ALL (SELECT hi FROM ding),
                                2 = 3,
                                4 != 42",
                            "#SELECT(
                                #selectList(

                                )
                            )");

    runSQLTreeTest("select abc",
                "#SELECT(
                    #selectList(

                    )
                )");

    runSQLTreeTest("select abc AS bla,
                            cd,
                            2+1 as exp",
                "#SELECT(
                    #selectList(

                    )
                )");

    runSQLTreeTest("select abc AS bla,
                                cd AND bd AS boo,
                                2+1 OR ble as exp",
                    "#SELECT(
                        #selectList(

                        )
                    )");

    runSQLTreeTest("select abc = bcd,
                            2 = 3,
                            4 != 42",
                        "#SELECT(
                            #selectList(

                            )
                        )");


}

runSetOperationTests {


    runSQLTreeTest(
    "SELECT Alexander FROM Sadowski
    uNiON
    SELECT Sadowski FROM Alexander
    exCePT
    SELECT hallo",
    "#SQL(#SELECT())");


    runSQLTreeTest(
    "SELECT Alexander FROM Sadowski",
    "#SQL(#SELECT())");


    runSQLTreeTest(
    "SELECT Alexander FROM Sadowski
    UNION
    SELECT Alexander FROM Sadowski",

    "#SQL(
        #UNION(
            #SELECT()
            #SELECT()
            #all(#false())
        )
    )");

    runSQLTreeTest(
    "SELECT Alexander FROM Sadowski
     UNION
     SELECT Alexander FROM Sadowski
     EXCEPT
     SELECT Alexander FROM Sadowski",

    "#SQL(
        #EXCEPT(
            #UNION(
                #SELECT()
                #SELECT()
                #all(#false())
            )
            #SELECT(
            )
            #all(#false())
        )
    )");

    runSQLTreeTest(
    "SELECT Alexander FROM Sadowski
     UNION
        SELECT Alexander FROM Sadowski
     INTERSECT
     SELECT Alexander FROM Sadowski",

    "#SQL(
        #UNION(
            #SELECT()
            #INTERSECT(
                #SELECT()
                #SELECT()
                #all(#false())

            )
            #all(#false())

        )
    )");

    runSQLTreeTest(
    "(SELECT Alexander FROM Sadowski
      UNION ALL
      SELECT Alexander FROM Sadowski
      )
      INTERSECT
      SELECT Alexander FROM Sadowski",

    "#SQL(
        #INTERSECT(
            #UNION(
                #SELECT()
                #SELECT()
                #all(#true())
            )
            #SELECT()
            #all(#false())

        )
    )");
}



runTreeTests {
    treeTestsFailed = false;
    runSetOperationTests();

    runFromClauseTests();

    runSelectExpressionTests();


    if(treeTestsFailed) {
        throw("some tests have failed");
    };
}

runSQLTreeTest slopeString expectedString {


    var slopeTree = parseSlopeWord(slopeString);
    var resultTree = constructSQLTree(slopeTree);

    var expectedTree = constructFromTreeString(expectedString);

    printString(concat("Testing: \n", slopeString));

    printString(concat("Expected: \n", constructTreeString(expectedTree)));

    printString(concat("Result: \n", constructTreeString(resultTree)));



    if(equalTrees(expectedTree, resultTree)) {
        printString("Test PASSED");
    } else {
        printString("Test FAILED");
        treeTestsFailed = true;
    };

    printString("\n\n");

}


getTableAttributes tableName {
    var root = #"";
    ->(_getTableAttributes)([#tableName], root);
    return children(root);
}




getTokenString node {
    return ->(getTokenString_t)([node], rootResultNode);
}

getTokenStringWithDefault node default{
    var tokenString = ->(getTokenString_t)([node], rootResultNode);
    if(equals(tokenString, "NULL")) {
        return default;
    } else {
        return tokenString;
    };
}



constructDFFText aNode {
    dffText = "";
    ->(constructDFFTextTree)([aNode], thisResultNode, ["ROOT"]);
     return dffText;
}



constructRuleString aNode {
    var ruleTree = #"ROOT";
    ->(constructRuleTree)([aNode], ruleTree);
    return constructDFFText(ruleTree);
}



constructTreeString node {
    var stringTree = #"";
    ->(t_constructTreeString)([node], stringTree, [0]);
    return constructDFFText(stringTree);
}



initConstructFromTreeString {
    genericTreeGrammar = parseSlopeGrammar("

        Generic
        Rules: treeNode

        treeNode => ~"#~" value ~"\\(~" treeNode* ~"\\)~".

        value => (identifier | number | boolean).

        identifier => IDENTIFIER.
        number => NUMBER.
        boolean => ~"true|false~".

        Scanner:


        NUMBER => ~"[1-9][0-9]*~".

        whitespace => SEPARATOR ~"\\n|\\r| ~" BOTTOM.
        IDENTIFIER => ~"([a-z]|[A-Z]|_)([a-z]|[A-Z]|[0-9]|_)*~" BOTTOM.

    ");
}

testConstructFromTreeString {
    var tree = constructFromTreeString("#SELECT()");
}

constructFromTreeString treeString {
    var grammarTree = parseSlopeWordFromGrammar(treeString, genericTreeGrammar);
    return ->(t_constructFromTreeString)([grammarTree], rootResultNode);
}





equalTrees tree1 tree2 {

    if(or(and(equals(tree1, null),
              not(equals(tree2, null))),
          and(equals(tree2, null),
              not(equals(tree1, null))))) {

        return false;

  };

  if(and(equals(tree1, null),
        equals(tree2, null))) {

        return true;
        };

    return ->(t_equalTrees)([tree1, tree2], rootResultNode);
}


testEqualTrees {

    var a = #"HI";
    addSlotNode(a, #"HI3");

    var b = #"HI";
    addSlotNode(b, #"HI3");

    printString(toString(equalTrees(a, b)));

    var hi = #"HI";
    addSlotNode(hi, #true);

    var hi2 = #"HI";
    addSlotNode(hi2, #true);

    printString(toString(equalTrees(hi, hi2)));
}



childByName node name {
    forEach(child in children(node)) {
        if(equals(slotNodeName(child), name)) {
            return child;
        };
    };
    return null;
}

pairNode parentValue childValue {
    var parentNode = #parentValue;
    addSlotNode(parentNode, #childValue);
    return parentNode;
}

pair parentNode childNode {
    addSlotNode(parentNode, childNode);
    return parentNode;
}


createHtmlFromNode aNode aNodeList aString {

}

html_createOpenTagFromNodeWithAdditional aNode aNodeList aString {
    return html_createOpenTag(
        concatAll(
            slotNodeName(aNode),
            [html_generateIDString (aNode, aNodeList), " ", aString]
        )
    );
}

TreeFunction constructHTMLString

TEXT aNodeList {


    var i = 0;
    forEach(child in children(thisGrammarNode)) {
        if(largerThan(i, 0)) {
            htmlString =
                    concat(htmlString, " ");
        };
        htmlString =
                concat(htmlString, slotNodeName(child));
        i = plus(i, 1);
    };


}

ROOT aNodeList {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode, [aNodeList]);
    };
}

att {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode);
    };
}

att.ALL {
    /*htmlString = concatAll(htmlString, [" ", slotNodeName(thisGrammarNode),
                                        "='",
                                        slotNodeName(thisGrammarNode.#1),
                                        "'"
                                     ]
                 );*/
    addAttribute(slotNodeName(thisGrammarNode), slotNodeName(thisGrammarNode.#1));
}

BUTTON aNodeList {

     if(and(
         largerThan(length(children(thisGrammarNode)), 0),
         equals(slotNodeName(thisGrammarNode.#1), "att")
        )
     ) {
         printString(concat("In ", slotNodeName(thisGrammarNode)));
         startOpenTag(thisGrammarNode, aNodeList);
             thisTreeFunction([thisGrammarNode.#1], thisResultNode);
         endOpenTag();

         printString(htmlString);

         var i = 2;
         while(smallerEqualsThan(i, length(children(thisGrammarNode)))) {
             thisTreeFunction([childAt(thisGrammarNode, i)], thisResultNode, [aNodeList]);
             i = plus(i, 1);
         };

         closeTag(thisGrammarNode);

     };


}

BR aNodeList {

     if(and(
         largerThan(length(children(thisGrammarNode)), 0),
         equals(slotNodeName(thisGrammarNode.#1), "att")
        )
     ) {
         printString(concat("In ", slotNodeName(thisGrammarNode)));
         startOpenTag(thisGrammarNode, aNodeList);
             thisTreeFunction([thisGrammarNode.#1], thisResultNode);
         endOpenTag();

         printString(htmlString);

         var i = 2;
         while(smallerEqualsThan(i, length(children(thisGrammarNode)))) {
             thisTreeFunction([childAt(thisGrammarNode, i)], thisResultNode, [aNodeList]);
             i = plus(i, 1);
         };

         closeTag(thisGrammarNode);

     };


}

TEXTAREA aNodeList {

     if(and(
         largerThan(length(children(thisGrammarNode)), 0),
         equals(slotNodeName(thisGrammarNode.#1), "att")
        )
     ) {
         printString(concat("In ", slotNodeName(thisGrammarNode)));
         startOpenTag(thisGrammarNode, aNodeList);
             thisTreeFunction([thisGrammarNode.#1], thisResultNode);
         endOpenTag();

         printString(htmlString);

         var i = 2;
         while(smallerEqualsThan(i, length(children(thisGrammarNode)))) {
             thisTreeFunction([childAt(thisGrammarNode, i)], thisResultNode, [aNodeList]);
             i = plus(i, 1);
         };

         closeTag(thisGrammarNode);

     };


}

DIV aNodeList {

    if(and(
        largerThan(length(children(thisGrammarNode)), 0),
        equals(slotNodeName(thisGrammarNode.#1), "att")
       )
    ) {
        printString(concat("In ", slotNodeName(thisGrammarNode)));
        startOpenTag(thisGrammarNode, aNodeList);
            thisTreeFunction([thisGrammarNode.#1], thisResultNode);
        endOpenTag();

        printString(htmlString);

        var i = 2;
        while(smallerEqualsThan(i, length(children(thisGrammarNode)))) {
            thisTreeFunction([childAt(thisGrammarNode, i)], thisResultNode, [aNodeList]);
            i = plus(i, 1);
        };

        closeTag(thisGrammarNode);

    };



}

SPAN aNodeList {

    if(and(
        largerThan(length(children(thisGrammarNode)), 0),
        equals(slotNodeName(thisGrammarNode.#1), "att")
       )
    ) {
        printString(concat("In ", slotNodeName(thisGrammarNode)));
        startOpenTag(thisGrammarNode, aNodeList);
            thisTreeFunction([thisGrammarNode.#1], thisResultNode);
        endOpenTag();

        printString(htmlString);

        var i = 2;
        while(smallerEqualsThan(i, length(children(thisGrammarNode)))) {
            thisTreeFunction([childAt(thisGrammarNode, i)], thisResultNode, [aNodeList]);
            i = plus(i, 1);
        };

        closeTag(thisGrammarNode);

    };



}

ALL aNodeList {

        printString(concat("In ", slotNodeName(thisGrammarNode)));
        startDivTagClass(thisGrammarNode, aNodeList);
        endOpenTag();

        printString(htmlString);

        var i = 1;
        while(smallerEqualsThan(i, length(children(thisGrammarNode)))) {
            thisTreeFunction([childAt(thisGrammarNode, i)], thisResultNode, [aNodeList]);
            i = plus(i, 1);
        };

        closeTagString("DIV");


        /*

    if(and(
                largerThan(length(children(thisGrammarNode)), 0),
                equals(slotNodeName(thisGrammarNode.#1), "att")
               )
            ) {
                printString(concat("In ", slotNodeName(thisGrammarNode)));
                startDivTagClass(thisGrammarNode, aNodeList);


                thisTreeFunction([thisGrammarNode.#1], thisResultNode);
                endOpenTag();

                printString(htmlString);

                var i = 2;
                while(smallerEqualsThan(i, length(children(thisGrammarNode)))) {
                    thisTreeFunction([childAt(thisGrammarNode, i)], thisResultNode, [aNodeList]);
                    i = plus(i, 1);
                };

                closeTagString("DIV");

            } else {

            ........
    };*/
}



TreeFunction doMouseHandleEvent




BUTTON event:ALL htmlObject {
    printString("hi");
}

ALL event:ALL htmlObject{
}

TreeFunction _constructSmallerSlopeTree

isNamedSLOTSLOPETreeNode().AND {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode);
    };
}

ALL {
    printString(slotNodeName(thisGrammarNode));
    +#slotNodeName(thisGrammarNode) {
        forEach(child in children(thisGrammarNode)) {
            thisTreeFunction([child], thisResultNode);
        };
    };
}

/* Continues From constructSQLTree_t */
TreeFunction constructSQLBooleanTermTree

term {
    if(equals(0, length(children(thisGrammarNode.#2)))) {
      thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    }
    else {
       +#"OR" {
          thisTreeFunction([thisGrammarNode.#1], thisResultNode);
          continue([thisGrammarNode.#2], thisResultNode);
       };
    };
}

booleanterm {
    if(equals(0, length(children(thisGrammarNode.#2)))) {
      thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    }
    else {
       +#"AND" {
          thisTreeFunction([thisGrammarNode.#1], thisResultNode);
          continue([thisGrammarNode.#2], thisResultNode);
       };
    };
}

booleanfactor {

    if(hasOPTChild(slotOriginNode(thisGrammarNode.#1))) {
        +#"NOT" {
            thisTreeFunction([thisGrammarNode.#2], thisResultNode);
        };
    } else {
        thisTreeFunction([thisGrammarNode.#2], thisResultNode);
    };
}


booleantest {
    if(hasOPTChild(slotOriginNode(thisGrammarNode.#2))) {
        +#"BOOL_TEST" {
            thisTreeFunction([thisGrammarNode.#1], thisResultNode);
            thisTreeFunction([thisGrammarNode.#2], thisResultNode);
        };
    } else {
        thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    };
}

booleantestOp {
    if(hasOPTChild(slotOriginNode(thisGrammarNode.#2))) {
        +#"NOT" {
            thisTreeFunction([thisGrammarNode.#3], thisResultNode);
        };
    }
    else {
        thisTreeFunction([thisGrammarNode.#3], thisResultNode);
    };
}

truthvalue {
    +#"TRUTH_VALUE" {
        +#getTokenString(thisGrammarNode.#1);
    };
}

exists {
    +#"EXISTS" {
        thisTreeFunction([thisGrammarNode.#2], thisResultNode);
    };
}

predicate {
    var temp_result = #"";

    thisTreeFunction([thisGrammarNode.#1], temp_result);
    thisTreeFunction([thisGrammarNode.#2], temp_result);

    +temp_result.#1;

}



comparison {
    var leftExpression = removeNode(thisResultNode.#1);

    +#getTokenString(thisGrammarNode.#1) {
        +leftExpression;
        thisTreeFunction([thisGrammarNode.#2], thisResultNode);
    };

}

quantifiedcomparison {

    var leftExpression = removeNode(thisResultNode.#1);

    +#getTokenString(thisGrammarNode.#1){
        +leftExpression;
        +#getTokenString(thisGrammarNode.#2) {
                thisTreeFunction([thisGrammarNode.#3], thisResultNode);
        };
    };

}


between {
    var leftExpression = removeNode(thisResultNode.#1);

    var between = #"BETWEEN" [
        +leftExpression;
        +#"range" {
            thisTreeFunction([thisGrammarNode.#3], thisResultNode);
            thisTreeFunction([thisGrammarNode.#5], thisResultNode);
        };
    ];

    if(hasOPTChild(slotOriginNode(thisGrammarNode.#1))) {
        +#"NOT" [
            +between;
        ];
    } else {
        +between;
    };

}

"in" {
/*
   var leftExpression = removeNode(thisResultNode.#1);


   var inNode = #"IN" [
        +leftExpression;
        thisTreeFunction([thisGrammarNode.#3], thisResultNode);
   ];

   if(hasOPTChild(slotOriginNode(thisGrammarNode.#1))) {
       +#"NOT" {
           +inNode;
       };
   } else {
       +inNode;
   };
*/

   var leftExpression = removeNode(thisResultNode.#1);

    if(hasOPTChild(slotOriginNode(thisGrammarNode.#1))) {
       +#"NOT_IN" {
           +leftExpression;
           thisTreeFunction([thisGrammarNode.#3], thisResultNode);
       };
    } else {
       +#"IN" [
           +leftExpression;
           thisTreeFunction([thisGrammarNode.#3], thisResultNode);
       ];
    };

}

invaluelist {
    +#"inValueList" {
        continue([thisGrammarNode], thisResultNode);
    };
}

like {
    var leftExpression = removeNode(thisResultNode.#1);

    var like = #"LIKE" [
        +leftExpression;
        thisTreeFunction([thisGrammarNode.#3], thisResultNode);
    ];

    if(hasOPTChild(slotOriginNode(thisGrammarNode.#1))) {
       +#"NOT" {
           +like;
       };
    } else {
       +like;
    };
}

nullcheck {
    var leftExpression = removeNode(thisResultNode.#1);

    var isnull = #"ISNULL" [
        +leftExpression;
    ];

    if(hasOPTChild(slotOriginNode(thisGrammarNode.#1))) {
       +#"NOT" {
           +isnull;
       };
    } else {
       +isnull;
    };
}

match {
   /* TODO */
}

overlaps {
    var leftExpression = removeNode(thisResultNode.#1);

    +#"OVERLAPS" {
        thisTreeFunction([thisGrammarNode.#2], thisResultNode);
    };
}

quantifiedlike {
    var leftExpression = removeNode(thisResultNode.#1);

    var like = #"LIKE" [
        +leftExpression;
        +#getTokenString(thisGrammarNode.#3) {
            thisTreeFunction([thisGrammarNode.#4], thisResultNode);
        };
    ];

    if(hasOPTChild(slotOriginNode(thisGrammarNode.#1))) {
       +#"NOT" {
           +like;
       };
    } else {
       +like;
    };
}

ALL {
    continue([thisGrammarNode], thisResultNode);
}

TreeFunction constructSQLExpressionTree

isAnyTermExpr() {
    var temp_result = #"";

    thisTreeFunction([thisGrammarNode.#1], temp_result);
    thisTreeFunction([thisGrammarNode.#2], temp_result);

    +temp_result.#1;
}

isPlusMinusConcatMultDivide() {
    var previousResult = removeNode(thisResultNode.#1);

    +#getTokenString(thisGrammarNode.#1) {
        +previousResult;
        thisTreeFunction([thisGrammarNode.#2], thisResultNode);
    };
}

valueexpr {
    if(hasOPTChild(slotOriginNode(thisGrammarNode.#1))) {
        /* POSITIVIE/NEGATIVE */
        +#getTokenString(thisGrammarNode.#1) {
            thisTreeFunction([thisGrammarNode.#2], thisResultNode);
        };

    }
    else {
       thisTreeFunction([thisGrammarNode.#2], thisResultNode);
    };
}


number {
    +#"NUMBER" {
        +#getTokenString(thisGrammarNode.#1);
    };
}

column {
    +#"COLUMN" {
        thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    };
}


countspec {
    +#"FUNCTION" {
        +#"NAME" {
            +#"COUNT";
        };
        +#"PARAMS" {
            thisTreeFunction([thisGrammarNode.#3], thisResultNode);
        };
    };
}

countAllTuples {
    +#"COUNT_EVERYTHING";
}

aggrparameter {
    var quantifier = #"ALL";
    if(hasOPTChild(slotOriginNode(thisGrammarNode.#1))) {
        quantifier = #getTokenString(thisGrammarNode.#1);
    };
    +#"AGGR_PARAM" {
        +quantifier;
        thisTreeFunction([thisGrammarNode.#2], thisResultNode);
    };
}

aggrspec {
    +#"FUNCTION" {
        +#"NAME" {
            +#getTokenString(thisGrammarNode.#1);
        };
        +#"PARAMS" {
            thisTreeFunction([thisGrammarNode.#3], thisResultNode);
        };
    };
}

customfunctionspec {
    +#"FUNCTION" {
        +#"NAME" {
            +#getTokenString(thisGrammarNode.#1);
        };
        +#"PARAMS" {
            thisTreeFunction([thisGrammarNode.#3], thisResultNode);
        };
    };
}


rowvalueconstructor {
    +#"ROW_VALUE_CONSTRUCTOR" {
        thisTreeFunction([thisGrammarNode.#2], thisResultNode);
        thisTreeFunction([thisGrammarNode.#3], thisResultNode);
    };
}

nonBracketCast {
    +#"CAST" {
        thisTreeFunction([thisGrammarNode.#2], thisResultNode);
        thisTreeFunction([thisGrammarNode.#4], thisResultNode);
    };
}

bracketCast {
    +#"CAST" {
        thisTreeFunction([thisGrammarNode.#3], thisResultNode);
        thisTreeFunction([thisGrammarNode.#5], thisResultNode);
    };
}

extractspec {
    +#"EXTRACT" {
        +#getTokenString(thisGrammarNode.#3);
        thisTreeFunction([thisGrammarNode.#5], thisResultNode);
    };
}

qualifier {
    +#"QUALIFIER" {
        +#getTokenString(thisGrammarNode.#1);
        thisTreeFunction([thisGrammarNode.#2], thisResultNode);
    };
}

qualifierTail {
    +#getTokenString(thisGrammarNode.#2);
}

dateliteral {
    +#"DATE" {
        +#getTokenString(thisGrammarNode.#2);
    };
}

timeliteral {
    +#"TIME" {
        +#getTokenString(thisGrammarNode.#2);
    };
}

timestampliteral {
    +#"TIMESTAMP" {
        +#getTokenString(thisGrammarNode.#2);
    };
}

intervalliteral {
    +#"INTERVAL" {
        /* TODO */
        +#"some value...";
    };
}


ALL {
    continue([thisGrammarNode], thisResultNode);
}






TreeFunction constructSQLFROMClauseTree

fromclause {
    +#"FROM" {
        thisTreeFunction([thisGrammarNode.#2], thisResultNode);
        thisTreeFunction([thisGrammarNode.#3], thisResultNode);
    };
}

tablereference {
    var temp_result = #"";
    thisTreeFunction([thisGrammarNode.#1], temp_result);
    thisTreeFunction([thisGrammarNode.#2], temp_result);
    +temp_result.#1;
}

tablereferenceComma {
    var leftExpression = removeNode(thisResultNode.#1);

    +#"JOIN" {
        +leftExpression;
        +#"CROSS";
        thisTreeFunction([thisGrammarNode.#2], thisResultNode);
    };
}

tablereference0 {
    var temp_result = #"";
    thisTreeFunction([thisGrammarNode.#1], temp_result);
    thisTreeFunction([thisGrammarNode.#2], temp_result);
    +temp_result.#1;
}

crossJoin {
    var leftExpression = removeNode(thisResultNode.#1);

    +#"JOIN" {
        +leftExpression;
        +#"CROSS";
        thisTreeFunction([thisGrammarNode.#3], thisResultNode);
    };
}

naturalJoin {

    var leftExpression = removeNode(thisResultNode.#1);



    +#"JOIN" {
        +leftExpression;

        +#"NATURAL" {
            +#getTokenStringWithDefault(thisGrammarNode.#2, "INNER");
            +#"USING" {
                +#"column1"; /* TODO: Need to hardcode columns somehow...*/
                +#"column2";
            };
        };

        thisTreeFunction([thisGrammarNode.#4], thisResultNode);
    };
}

conditionalJoin {
    var leftExpression = removeNode(thisResultNode.#1);

    +#"JOIN" {
        +leftExpression;
        +#"CONDITION" {
            +#getTokenStringWithDefault(thisGrammarNode.#1, "INNER");
            thisTreeFunction([thisGrammarNode.#4], thisResultNode);
        };
        thisTreeFunction([thisGrammarNode.#3], thisResultNode);
    };
}

onSpecification {
    +#"ON" {
        thisTreeFunction([thisGrammarNode.#2], thisResultNode);
    };
}

usingSpecification {
    +#"USING" {
        +#getTokenString(thisGrammarNode.#3);
        thisTreeFunction([thisGrammarNode.#4], thisResultNode);
    };
}

usingAdditional {
    +#getTokenString(thisGrammarNode.#2);
}

tableLiteral {
    +#"TABLE" {
        +#"TABLE_NAME" {
            if(hasOPTChild(slotOriginNode(thisGrammarNode.#2))) {
                +#"RENAMED" {
                    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
                    thisTreeFunction([thisGrammarNode.#2], thisResultNode);

                };
            } else {
                +#"ORIGINAL" {
                    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
                };
            };
        };
        +#"TABLE_COLUMNS" {
                forEach(attributeNode in getTableAttributes(getTokenString(thisGrammarNode.#1))) {
                    +#slotNodeName(attributeNode);
                };
        };
    };
}

tableSubquery {
    +#"TABLE" {
        if(hasOPTChild(slotOriginNode(thisGrammarNode.#2))) {
            +#"TABLE_NAME" {
                +#"RENAMED" {
                    thisTreeFunction([thisGrammarNode.#2], thisResultNode);
                    +#"(subquery)";
                };
            };
        };
        +#"TABLE_CONTENT" {
            thisTreeFunction([thisGrammarNode.#1], thisResultNode);
        };
    };
}

tableAsRename {
    +#getTokenString(thisGrammarNode.#2);
    /*TODO: Renamed Columns*/
}

ALL {
    continue([thisGrammarNode], thisResultNode);
}

TreeFunction constructSQLTree_t

queryStatement {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
}

isQueryOrIntersectableQuery() {
    var temp_result = #"temp_result";

    thisTreeFunction([thisGrammarNode.#1], temp_result);

    thisTreeFunction([thisGrammarNode.#2], temp_result);

    +temp_result.#1; /* #1 !! */
}

isExceptOrUnionOrIntersect() {
    var leftExpression = removeNode(thisResultNode.#1);

    +#getTokenString(thisGrammarNode) {
        +leftExpression;
        thisTreeFunction([thisGrammarNode.#3], thisResultNode);
        +#"DUPLICATES" {
            if(hasOPTChild(slotOriginNode(thisGrammarNode.#2))) {
                +#"TRUE";
            } else {
                +#"FALSE";
            };
        };
    };

}


select {
    +#"QUERY" {
        forEach(child in children(thisGrammarNode)) {
            thisTreeFunction([child], thisResultNode);
        };
    };
}


selectclause {
    +#"SELECT" {
      thisTreeFunction([thisGrammarNode.#3], thisResultNode);

      if(hasOPTChild(slotOriginNode(thisGrammarNode.#2))) {
        +#getTokenString(thisGrammarNode.#2);
      } else {
        +#"ALL";
      };

    };
}

selectlist {
    +#"SELECT_LIST" {
        thisTreeFunction([thisGrammarNode.#1], thisResultNode);
        thisTreeFunction([thisGrammarNode.#2], thisResultNode);
    };
}

allColumns {
    +#"ALL_COLUMNS";
}

selectColumn {
    if(hasOPTChild(slotOriginNode(thisGrammarNode.#2))) {
        +#"SELECT_COLUMN_RENAMED" { /*FROM TO*/
            thisTreeFunction([thisGrammarNode.#1], thisResultNode);
            thisTreeFunction([thisGrammarNode.#2], thisResultNode);
        };
    } else {
        +#"SELECT_COLUMN" {
            thisTreeFunction([thisGrammarNode.#1], thisResultNode);
        };
    };

}

selectColumnRename {
    +#"AS" {
        +#getTokenString(thisGrammarNode.#2);
    };
}

whereclause {
    +#"WHERE" {
        thisTreeFunction([thisGrammarNode.#2], thisResultNode);
    };
}

groupbyclause {
    +#"GROUP BY" {
        thisTreeFunction([thisGrammarNode.#3], thisResultNode);
        /*TODO: CollateClause */
        thisTreeFunction([thisGrammarNode.#5], thisResultNode);
    };
}

groupbyclauseTail {
    thisTreeFunction([thisGrammarNode.#2], thisResultNode);
    /*TODO: CollateClause*/
}

havingclause {
    +#"HAVING" {
        thisTreeFunction([thisGrammarNode.#2], thisResultNode);
    };
}

orderbyclause {
    +#"ORDER BY" {

    };
}


OR {
    continue([thisGrammarNode.#1], thisResultNode);
}

ALL {
    continue([thisGrammarNode], thisResultNode);
}



TreeFunction sql_doForChildren

ALL {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode);
    };
}


TreeFunction constructSQLBooleanTermHTMLTree

OR {
    +#"block round" {
        +#"blockDescriptor" {
            +#"blockDescriptorText" {
                +#"TEXT" {
                    +#"OR";
                };
            };
        };
        +#"blockContent" {
            forEach(child in children(thisGrammarNode)) {
                +#"whereLine" {
                    thisTreeFunction([child], thisResultNode);
                };
            };
        };

    };


}


AND {/*
    +generateRoundBlock("AND") {
        forEach(child in children(thisGrammarNode)) {
            +#"whereLine" {
                thisTreeFunction([child], thisResultNode.#2);
            };
        };
    };*/
    +#"block round" {
        +#"blockDescriptor" {
            +#"blockDescriptorText" {
                +#"TEXT" {
                    +#"AND";
                };
            };
        };
        +#"blockContent" {
            forEach(child in children(thisGrammarNode)) {
                +#"whereLine" {
                    thisTreeFunction([child], thisResultNode);
                };
            };
        };

    };
}

NOT {
    +#"block round" {
        +#"blockDescriptor" {
            +#"blockDescriptorText" {
                +#"TEXT" {
                    +#"NOT";
                };
            };
        };
        +#"blockContent" {
            forEach(child in children(thisGrammarNode)) {
                thisTreeFunction([child], thisResultNode);
            };
        };

    };
}

BOOL_TEST {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    +#"tableOp" {
        +#"TEXT" {
            +#"IS";
            thisTreeFunction([thisGrammarNode.#2], thisResultNode);
        };
    };
}

TRUTH_VALUE {
    +#slotNodeName(thisGrammarNode.#1);
}

EXISTS {
    +#"block round" {
        +#"blockDescriptor" {
            +#"blockDescriptorText" {
                +#"TEXT" {
                    +#"EXISTS";
                };
            };
        };
        +#"blockContent" {
            forEach(child in children(thisGrammarNode)) {
                thisTreeFunction([child], thisResultNode);
            };
        };

    };
}
/*
"=" {
    +#"DIV" {
        +#"att" {
            +pairNode("style", "display:inline-block;");
        };
        thisTreeFunction([thisGrammarNode.#1], thisResultNode);

    };
    +#"DIV" {
            +#"att" {
                +pairNode("style", "display:inline-block;");
            };
            +#"tableOp" {
                +#"TEXT" {
                    +#"=";
                };
            };
    };

    +#"DIV" {
            +#"att" {
                +pairNode("style", "display:inline-block;");
            };
            thisTreeFunction([thisGrammarNode.#2], thisResultNode);

    };

}
*/
"=" {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    +#"tableOp" {
        +#"TEXT" {
            +#"=";
        };
    };
    thisTreeFunction([thisGrammarNode.#2], thisResultNode);

}


"!=" {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    +#"tableOp" {
        +#"TEXT" {
            +#"!=";
        };
    };
    thisTreeFunction([thisGrammarNode.#2], thisResultNode);

}

"<" {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    +#"tableOp" {
        +#"TEXT" {
            +#"<";
        };
    };
    thisTreeFunction([thisGrammarNode.#2], thisResultNode);

}

">" {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    +#"tableOp" {
        +#"TEXT" {
            +#">";
        };
    };
    thisTreeFunction([thisGrammarNode.#2], thisResultNode);

}

"<=" {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    +#"tableOp" {
        +#"TEXT" {
            +#"<=";
        };
    };
    thisTreeFunction([thisGrammarNode.#2], thisResultNode);

}

">=" {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    +#"tableOp" {
        +#"TEXT" {
            +#">=";
        };
    };
    thisTreeFunction([thisGrammarNode.#2], thisResultNode);

}

"ALL" {
    +#"verticalBlockContainer" {
        +#"block round" {
            +#"blockDescriptor" {
                +#"blockDescriptorText" {
                    +#"TEXT" {
                        +#"ALL";
                    };
                };
            };
            +#"blockContent" {
                forEach(child in children(thisGrammarNode)) {
                    thisTreeFunction([child], thisResultNode);
                };
            };

        };
    };
}

BETWEEN {
    +#"tableOp" {
        +#"TEXT" {
            +#"BETWEEN... TODO";
        };
    };
}

"IN" {
   thisTreeFunction([thisGrammarNode.#1], thisResultNode);
   +#"tableOp" {
       +#"TEXT" {
           +#"&#8712;";
       };
   };
   thisTreeFunction([thisGrammarNode.#2], thisResultNode);
}

NOT_IN {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
   +#"tableOp" {
       +#"TEXT" {
           +#"&#8713;";
       };
   };
   thisTreeFunction([thisGrammarNode.#2], thisResultNode);
}

inValueList {
    +#"TEXT" {
        +#"inValueList... TODO";
    };
}

LIKE {
    +#"TEXT" {
        +#"like... TODO";
    };
} /* +TODO: Quantified Like */

ISNULL {
    +#"TEXT" {
        +#"ISNOTNULL... TODO";
    };
}

OVERLAPS {
    +#"TEXT" {
        +#"overlaps... TODO";
    };
}



ALL {
    continue([thisGrammarNode], thisResultNode);
}

TreeFunction constructSQLExpressionHTMLTree

"+" {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);

        +#"TEXT"{
            +#"+";
        };
    thisTreeFunction([thisGrammarNode.#2], thisResultNode);
}

"-" {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    +#"literal" {
        +#"TEXT"{
            +#"-";
        };
    };
    thisTreeFunction([thisGrammarNode.#2], thisResultNode);
}

"*" {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    +#"TEXT"{
        +#"*";
    };
    thisTreeFunction([thisGrammarNode.#2], thisResultNode);
}

"/" {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    +#"TEXT"{
        +#"/";
    };
    thisTreeFunction([thisGrammarNode.#2], thisResultNode);
}

"||" {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    +#"TEXT"{
        +#"||";
    };
    thisTreeFunction([thisGrammarNode.#2], thisResultNode);
}

POSITIVE {
    +#"TEXT" {
        +#"+";
    };
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
}

NEGATIVE {
    +pairNode("TEXT", "-");
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
}

NUMBER {
    +#"literal" {
        +pairNode("TEXT", slotNodeName(thisGrammarNode.#1));
    };
}

COLUMN {
    +#"TEXT" {
        +#" ";
    };
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
}

COUNT {
    +#"block round" {
        +#"blockDescriptor" {
            +#"blockDescriptorText" {
                +#"TEXT" {
                    +#"COUNT";
                };
            };
        };
        +#"blockContent" {
            forEach(child in children(thisGrammarNode)) {
                thisTreeFunction([child], thisResultNode);
            };
        };

    };
}

COUNT.EVERYTHING {
    +#"literal" {
        +#"TEXT" {
            +#"*";
        };
    };
}

AGGR_QUANTIFIER {
    +#"TEXT" {
        +#slotNodeName(thisGrammarNode);
    };
}

FUNCTION {
    /*+#"TEXT" {
        +#"FUNCTION... Todo";
    };*/
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);

    thisTreeFunction([thisGrammarNode.#2], thisResultNode);

}

FUNCTION.NAME {
    +#"TEXT" {
        +#slotNodeName(thisGrammarNode.#1);
    };

}

FUNCTION.PARAMS {
    +#"TEXT" {
        +#"(";
    };

    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode);
    };

    +#"TEXT" {
        +#")";
    };
}

AGGR_PARAM {
    +#"TEXT" {
        +#slotNodeName(thisGrammarNode.#1);
        +#" ";
    };
    thisTreeFunction([thisGrammarNode.#2], thisResultNode);
}

ROW_VALUE_CONSTRUCTOR {
    +#"TEXT" {
        +#"Row Value Constructor... TODO";
    };
}

CAST {
    +#"TEXT" {
        +#"CAST ";
    };

}

QUALIFIER {
    +#"literal" {
        +#"TEXT" {
            +#slotNodeName(thisGrammarNode.#1);
        };
    };
}

ALL {
    printString(concat("Reached All: ", slotNodeName(thisGrammarNode)));
    /*forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode);
    };*/
    +#"TEXT" {
        +#slotNodeName(thisGrammarNode);
    };
}


TreeFunction constructSQLFROMClauseHTMLTree

TABLE {
    +#"table" {
        +#"tableDescriptor" {
            thisTreeFunction([thisGrammarNode.#1], thisResultNode);
        };
        thisTreeFunction([thisGrammarNode.#2], thisResultNode);
    };
}

TABLE_NAME {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    printString("done table_name");
}

TABLE_NAME.ORIGINAL {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
}

TABLE_NAME.RENAMED {
    /*+#"newTableName" {
        thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    };
    printString("done table_name.renamed.first");
    +#"realTableName" {
        thisTreeFunction([thisGrammarNode.#2], thisResultNode);
    };
    printString("done table_name.renamed");
    */

    +#"TEXT" {
        +#slotNodeName(thisGrammarNode.#1);
        +#slotNodeName(thisGrammarNode.#2);
    };
}

TABLE_CONTENT {
    +#"tableContent" {
        thisTreeFunction([thisGrammarNode.#1], thisResultNode);
        printString("done table_content");
    };
}

TABLE_COLUMNS {
    +#"tableColumns" {
        forEach(child in children(thisGrammarNode)) {
            +#"tableColumnName" {
                +#"TEXT" {
                   +#slotNodeName(child);
                };
            };
        };
    };
}

JOIN {
    +#"block transBorderBlock" {
        +#"blockDescriptor" {
            +#"blockDescriptor" {
                +#"blockDescriptorText" {
                    +#"TEXT" {
                        ->(constructJoinDescriptorHTML)([thisGrammarNode.#2], thisResultNode);
                    };
                };
            };
        };
        +#"blockContent" {
            thisTreeFunction([thisGrammarNode.#1], thisResultNode);
            thisTreeFunction([thisGrammarNode.#2], thisResultNode);
            thisTreeFunction([thisGrammarNode.#3], thisResultNode);
        };
    };
}

JOIN.CROSS {
    +#"JOIN" {
        +#"joinSymbol" {
            +#"TEXT" {
                +#"&#215;";
            };
        };
    };
}

JOIN.NATURAL {
    +#"JOIN" {
        +#"joinSymbol" {
            thisTreeFunction([thisGrammarNode.#1], thisResultNode);
        };
        thisTreeFunction([thisGrammarNode.#2], thisResultNode);
    };
}

JOIN.CONDITION {
    +#"JOIN" {
        +#"joinSymbol" {
            thisTreeFunction([thisGrammarNode.#1], thisResultNode);
        };
        thisTreeFunction([thisGrammarNode.#2], thisResultNode);
    };
}

USING {
    +#"block transBorderBlock" {
        +#"blockDescriptor" {
            +#"blockDescriptorText" {
                +#"TEXT" {
                    +#"USING";
                };
            };
        };
        +#"blockContent decreaseFontSize" {
            forEach(child in children(thisGrammarNode)) {
                +#"DIV" {
                    +#"att" {
                    };
                    +#"TEXT" {
                        +#slotNodeName(child);
                    };
                };
            };
        };
    };
}

ON {
    +#"block transBorderBlock" {
            +#"blockDescriptor" {
                +#"blockDescriptorText" {
                    +#"TEXT" {
                        +#"ON";
                    };
                };
            };
            +#"blockContent" {
                thisTreeFunction([thisGrammarNode.#1], thisResultNode);
            };
        };
}

INNER {
    +#"TEXT" {
        +#"&#10781;";
    };
}

LEFT {
    +#"TEXT" {
        +#"&#10197;";
    };
}

RIGHT {
    +#"TEXT" {
        +#"&#10198;";
    };
}

FULL {
    +#"TEXT" {
        +#"&#10199;";
    };
}





ALL {
    continue([thisGrammarNode], thisResultNode);
}

TreeFunction constructJoinDescriptorHTML

CROSS {
    +#"CROSS JOIN";
}

CONDITION {
    +#slotNodeName(thisGrammarNode.#1);
    +#"JOIN";
}

NATURAL {
    +#"NATURAL";
    +#slotNodeName(thisGrammarNode.#1);
    +#"JOIN";
}




TreeFunction constructSQLHTMLTree_t

SQL {

    +#"SQLInput" {
        +#"TEXTAREA" {
            +#"att" {
                +pairNode("title", "SQL Input");
                +pairNode("rows", "9");
                +pairNode("cols", "70");
            };
        };
        +#"BR" {
            +#"att";
        };
        +#"BUTTON" {
            +#"att";
            +#"TEXT" {
                +#"Generate";
            };
        };
    };
    +#"editor" {
        forEach(child in children(thisGrammarNode)) {
            thisTreeFunction([child], thisResultNode);
        };
    };
}





EXCEPT {
    +#"block round" {
            +#"blockDescriptor" {
                +#"blockDescriptorText" {
                    +#"TEXT" {
                        +#"EXCEPT";
                    };
                };
            };
            +#"blockContent" {
                thisTreeFunction([thisGrammarNode.#1], thisResultNode);
                +#"tableOp" {
                    +#"TEXT" {
                        +#"\\";
                    };
                };
                thisTreeFunction([thisGrammarNode.#2], thisResultNode);
            };

    };
}

UNION {
    +#"verticalBlockContainer" {
        +#"block round" {
                +#"blockDescriptor" {
                    +#"blockDescriptorText" {
                        +#"TEXT" {
                            +#"UNION";
                        };
                    };
                };
                +#"blockContent" {
                    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
                    +#"tableOp" {
                        +#"TEXT" {
                            +#"&cup;";
                        };
                    };
                    thisTreeFunction([thisGrammarNode.#2], thisResultNode);
                };

        };
    };

}

INTERSECT {
    +#"verticalBlockContainer" {
        +#"block round" {
                +#"blockDescriptor" {
                    +#"blockDescriptorText" {
                        +#"TEXT" {
                            +#"INTERSECT";
                        };
                    };
                };
                +#"blockContent" {
                    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
                    +#"tableOp" {
                        +#"TEXT" {
                            +#"&cap;";
                        };
                    };
                    thisTreeFunction([thisGrammarNode.#2], thisResultNode);
                };

        };
    };
}

/* Alternative with bordered queries (for example except):
EXCEPT {
    +#"block round" {
            +#"blockDescriptor" {
                +#"blockDescriptorText" {
                    +#"TEXT" {
                        +#"EXCEPT";
                    };
                };
            };
            +#"blockContent" {
                +#"table" {
                    +#"tableColumns" {
                        thisTreeFunction([thisGrammarNode.#1], thisResultNode);
                    };
                };
                +#"tableOp" {
                    +#"TEXT" {
                        +#"\\";
                    };
                };
                +#"table" {
                    +#"tableColumns" {
                        thisTreeFunction([thisGrammarNode.#2], thisResultNode);
                    };
                };
            };

    };
}
*/


SQL.QUERY {
    +#"verticalBlockContainer" {
        forEach(child in children(thisGrammarNode)) {
            thisTreeFunction([child], thisResultNode);
        };
    };
}

QUERY {
    +#"innerQueryContainer" {
        +#"verticalBlockContainer" {
            forEach(child in children(thisGrammarNode)) {
                thisTreeFunction([child], thisResultNode);
            };
        };
    };
}

SELECT {
    +#"block round transBorderBlock" {
            +#"blockDescriptor left" {
                +#"blockDescriptorText" {
                    +#"TEXT" {
                        +#"SELECT";
                    };
                };
            };
            +#"blockContent" {
               thisTreeFunction([thisGrammarNode.#1], thisResultNode);
            };
    };
}

SELECT_LIST {
    forEach(child in children(thisGrammarNode)) {
        +#"selectColumn" {
            thisTreeFunction([child], thisResultNode);
            +#"TEXT" {
                +#" ";
            };
        };
    };
}

SELECT_COLUMN {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
}

SELECT_COLUMN_RENAMED {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    +#"TEXT" {
        +#" ";
    };
    thisTreeFunction([thisGrammarNode.#2], thisResultNode);
}

SELECT_COLUMN_RENAMED.AS {
    +#"TEXT" {
        +#"AS";
        +#slotNodeName(thisGrammarNode.#1);
    };
}

ALL_COLUMNS {
    +#"TEXT" {
        +#"*";
    };
}


FROM {
    printString("reached FROM");
    +#"block round transBorderBlock" {
        +#"blockDescriptor left" {
            +#"blockDescriptorText" {
                +#"TEXT" {
                    +#"FROM";
                };
            };
        };
        +#"blockContent" {
            forEach(child in children(thisGrammarNode)) {
                printString("reached FROMChild:");
                printString(slotNodeName(child));
                thisTreeFunction([child], thisResultNode);
            };
        };

    };
}

WHERE {
    +#"block round transBorderBlock" {
        +#"blockDescriptor left" {
            +#"blockDescriptorText" {
                +#"TEXT" {
                    +#"WHERE";
                };
            };
        };
        +#"blockContent" {
            forEach(child in children(thisGrammarNode)) {
                +#"whereLine" {
                    thisTreeFunction([child], thisResultNode);
                };
            };
        };

    };
}

"GROUP BY" {
    +#"block round transBorderBlock" {
        +#"blockDescriptor left" {
            +#"blockDescriptorText" {
                +#"TEXT" {
                    +#"GROUP BY";
                };
            };
        };
        +#"blockContent" {
            forEach(child in children(thisGrammarNode)) {
                thisTreeFunction([child], thisResultNode);
            };
        };
    };
}

HAVING {
    +#"block round transBorderBlock" {
        +#"blockDescriptor left" {
            +#"blockDescriptorText" {
                +#"TEXT" {
                    +#"HAVING";
                };
            };
        };
        +#"blockContent" {
            forEach(child in children(thisGrammarNode)) {
                thisTreeFunction([child], thisResultNode);
            };
        };
    };
}

"ORDER BY" {
    +#"block round transBorderBlock" {
        +#"blockDescriptor left" {
            +#"blockDescriptorText" {
                +#"TEXT" {
                    +#"ORDER BY";
                };
            };
        };
        +#"blockContent" {
            forEach(child in children(thisGrammarNode)) {
                thisTreeFunction([child], thisResultNode);
            };
        };
    };
}


ALL {
    continue([thisGrammarNode], thisResultNode);
}

TreeFunction _getTableAttributes

person {
    +#"personID";
    +#"vorname";
    +#"nachname";
}

student {
    +#"personID";
    +#"matrNo";
    +#"fachbereich";
    +#"studienfach";
    +#"semester";
    +#"fachsemester";
}

lehrveranstaltung {
    +#"lv_nr";
    +#"bezeichnung";
    +#"raum";
    +#"gehalten_seit";
    +#"status";
}

leistung {
    +#"matrNo";
    +#"lv_nr";
    +#"note";
    +#"datum";
}

semesterbeitrag {
    +#"matrNo";
    +#"datum";
    +#"geforderterBetrag";
}

zahlung {
    +#"matrNo";
    +#"datum";
    +#"betrag";
}

ALL {
    +#"col1";
    +#"col2";
    +#"col3";
}



TreeFunction getTokenString_t


queryExcept {
    return "EXCEPT";
}

queryIntersect {
    return "INTERSECT";
}

queryUnion {
    return "UNION";
}

some {
    return "ANY";
}

sign {
    var hi = thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    if(equals(hi, "+")) {
        return "POSITIVE";
    }
    else {
        return "NEGATIVE";
    };
}

notequalsoperator {
    return "!=";
}

SLOPETOKEN {
    return slotNodeName(thisGrammarNode.#1);
}

NULL {
    return "NULL";
}

ALL {
    if(hasChildren(thisGrammarNode)) {
        return thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    };
}

TreeFunction constructDFFTextTree

ALL exceptString {
   var stringToAppend = "";
   if(not(equals(slotNodeName(thisGrammarNode), exceptString))) {
       stringToAppend = toString(slotNodeName(thisGrammarNode));
   };
   dffText = concat(dffText, stringToAppend);


   forEach(aChild in children(thisGrammarNode)) {
        thisTreeFunction([aChild], thisResultNode, [exceptString]);
   };
}

TreeFunction constructRuleTree

OR {
    var numAlts = numAlternatives(thisGrammarNode);


    thisTreeFunction([createORChildTemplateAt(thisGrammarNode, 0)], thisResultNode);


    var counter = 1;

    while(smallerThan(counter, numAlts)) {

        +#" | ";
        thisTreeFunction([createORChildTemplateAt(thisGrammarNode, counter)], thisResultNode);
        counter = plus(counter, 1);
    };

}

OPT {
    var childTemplate = createSingleChildTemplate(thisGrammarNode);
    thisTreeFunction([childTemplate], thisResultNode);
    +#"?";
}

BRACKET {

    +#"(";
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    +#")";
}

isNFOLD_0() {
    var childTemplate = createNFOLDChildTemplate(thisGrammarNode);
    thisTreeFunction([childTemplate], thisResultNode);
    +#"*";
}

isNFOLD_1() {
    var childTemplate = createNFOLDChildTemplate(thisGrammarNode);
    thisTreeFunction([childTemplate], thisResultNode);
    +#"+";
}

AND {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);

    var counter = 2;
    var childrenLength = length(children(thisGrammarNode));
    while(or(smallerThan(counter, childrenLength),
             equals(counter,childrenLength))) {
        +#" ";
        thisTreeFunction([childAt(thisGrammarNode, counter)], thisResultNode);

        counter = plus(counter, 1);
    };
}

NULL {
    +#"NULL";
}

SLOPETOKEN {
    +#"~"";
    +#slotNodeName(thisGrammarNode.#1) ;
    +#"~"";
}

ALL {
    +#slotNodeName(thisGrammarNode);
}

TreeFunction t_constructTreeString

ALL tabNumber {

    var i = 0;
    while(smallerThan(i, tabNumber)) {
        +#"    ";
        i = plus(i,1);
    };

    +#"#";
    +#slotNodeName(thisGrammarNode);
    +#"(";

    if(hasChildren(thisGrammarNode)) {
        +#"\n";

        forEach(child in children(thisGrammarNode)) {
            thisTreeFunction([child], thisResultNode, [plus(tabNumber, 1)]);
            +#"\n";
        };

        i = 0;
        while(smallerThan(i, tabNumber)) {
                +#"    ";
                i = plus(i,1);
        };
    };

    +#")";
}

TreeFunction t_constructFromTreeString

treeNode {
    var node = thisTreeFunction([thisGrammarNode.#2], thisResultNode);

    forEach(child in children(thisGrammarNode.#4)) {
        addSlotNode(node, thisTreeFunction([child], thisResultNode));
    };
    return node;
}

identifier {
    return #thisTreeFunction([thisGrammarNode.#1], thisResultNode);
}

number {
    return #stringToInt(
            thisTreeFunction([thisGrammarNode.#1], thisResultNode)
           );
}

boolean {
    return #stringToBool(
            thisTreeFunction([thisGrammarNode.#1], thisResultNode)
           );
}

SLOPETOKEN {
    return slotNodeName(thisGrammarNode.#1);
}

ALL {
    if(hasChildren(thisGrammarNode)) {
        return thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    };
}


TreeFunction t_equalTrees

ALL secondGrammarNode:ALL {
    if(or(
            not(
                equals(slotNodeName(thisGrammarNode),
                      slotNodeName(secondGrammarNode)
                )
            ),
            not(
                equals(length(children(thisGrammarNode)),
                       length(children(secondGrammarNode))
                )
            )
        )
    ) {
        return false;
    }
    else {
        var i = 1;
        while(smallerEqualsThan(i, length(children(thisGrammarNode)))) {
            if(not(
                thisTreeFunction([childAt(thisGrammarNode, i),
                                  childAt(secondGrammarNode, i)],
                                 rootResultNode)
            )) {
                return false;
            };

            i = plus(i,1);
        };
        return true;
    };
}''');
exampleStrings.add('''SELECT lv_nr, COUNT(DISTINCT matrNo)FROM leistungWHERE matrNo IN(        SELECT matrNo        FROM            (SELECT matrNo, SUM(betrag) AS gezahlt            FROM zahlung            GROUP BY matrNo) gezahltSummen            NATURAL JOIN            (SELECT matrNo, SUM(geforderterBetrag) AS gefordert            FROM semesterbeitrag            GROUP BY matrNo) gefordertSummen        WHERE gefordert > gezahlt    UNION        SELECT matrNo        FROM semesterbeitrag        WHERE matrNo NOT IN (SELECT matrNo FROM semesterbeitrag)        GROUP BY matrNo)GROUP BY lv_nrHAVINGlv_nr >= ALL (	SELECT COUNT(DISTINCT matrNo)	FROM leistung	WHERE matrNo IN (		SELECT matrNo		FROM			(SELECT matrNo, SUM(betrag) AS gezahlt			FROM zahlung			GROUP BY matrNo) gezahltSummen			NATURAL JOIN			(SELECT matrNo, SUM(geforderterBetrag) AS gefordert			FROM semesterbeitrag			GROUP BY matrNo) gefordertSummen		WHERE gefordert > gezahlt		UNION		SELECT matrNo		FROM semesterbeitrag		WHERE matrNo NOT IN (SELECT matrNo FROM semesterbeitrag)		GROUP BY matrNo	)	GROUP BY lv_nr)''');
  return exampleStrings;
}
}
