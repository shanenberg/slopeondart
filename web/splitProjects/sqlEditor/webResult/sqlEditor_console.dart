import '../../../html/examples/SLOTHTMLToConsoleConverter.dart';
import '../../../../lib/slot/runtime/SLOTProject.dart';
import 'sqlEditor.slp.dart';

main() {
  SLOTProject project =
  new SLOTHTMLToConsoleConverter().constructProject(new sqlEditor());
  var slotResult = project.runProject();
}