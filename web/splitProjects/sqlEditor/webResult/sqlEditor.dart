import '../../../../lib/slot/runtime/SLOTHTMLProject.dart';
import '../../../html/examples/SLOTProjectConstructor.dart';
import 'sqlEditor.slp.dart';


main() {

  SLOTHTMLProject project =
  new SLOTProjectConstructor().constructHTMLProject(new sqlEditor());
  var slotResult = project.runProject();

}

