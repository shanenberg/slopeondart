import '../../../html/examples/SLOTHTMLToConsoleConverter.dart';
import '../../sqlVisualization/dartExperiment/projectStuff/projectStuff.dart';
import 'dart:io';

import '../../../../lib/lib/grammarparser/tree/GrammarObject.dart';
import '../../../../lib/lib/grammarparser/tree/GrammarTest.dart';
import '../../../../lib/lib/util/GrammarUtils.dart';
import '../../../../lib/slot/runtime/objects/SLOTTreeNode.dart';
import 'sqlEditor2020.slp.dart';


String outputString = "";


main() {
  project =
      new SLOTHTMLToConsoleConverter().constructProject(new sqlEditor2020());
  project.initProject();


  List<String> sqlStrings = new File
    ("web\\splitProjects\\sqlEditor2020\\slot\\slopeFile")
      .readAsStringSync().split("###");
  SLOPEGrammarObject grammar = parseGrammar(sqlStrings[1].substring(" SLOPE".length));

  print("Awaiting Input");
  bool startTest = false;
  var input = stdin.readLineSync();
  if(input == "") {
    startTest = true;
  }

  outputString += "var testMap = {\n";

  for(GrammarTest test in grammar.tests) {
    if(startTest || test.testname == input) {
      startTest = true;
      if(!test.fail_parsing && !test.fail_scanning)
        performGrammarTest(grammar, test);
      print("##############################");
      print("##############################");
    }
  }

  outputString += "};";

  print(outputString);
}
performGrammarTest(SLOPEGrammarObject grammar, GrammarTest test) {
  SLOTTreeNode node = grammar.parseString(test.teststring).toSLOTTreeNode();
  String prettyString = project.runFunctionUnwrap("prettyPrintNode", [node]);
  String paramNodeName = "tableSubquery";
  String paramNodeName1 = "tableBracketExpression";

  bool hasNodeName = project.runFunctionWrapUnwrap("hasNodeName", [node, paramNodeName]);
  bool hasNodeName1 = project.runFunctionWrapUnwrap("hasNodeName", [node, paramNodeName1]);



  print(test.testname);
  print(prettyString);
  outputString += "\"${test.testname}\" : '''\n$prettyString''',\n";

  if(hasNodeName) print("CONTAINS "+ paramNodeName);
  if(hasNodeName1) print("CONTAINS "+ paramNodeName1);
}

