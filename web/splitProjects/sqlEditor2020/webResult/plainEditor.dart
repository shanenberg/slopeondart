import 'dart:html';

Element current;
Element caret;
main(){
  current = querySelector(".letter");
  appendCaret(current);
  document.onKeyDown.listen((KeyboardEvent ev) {
      if(
              ev.key == "ArrowRight"
          &&  getNextLetter(current) != null
      )
        {
          caret.remove();
          current = getNextLetter(current);
          print(current.text);
          appendCaret(current);
        }

      else if(
              ev.key == "ArrowLeft"
          &&  getPreviousLetter(current) != null
      ) {
        caret.remove();
        current = getPreviousLetter(current);
        print(current.text);
        appendCaret(current);
      }
  }



  );
}

appendCaret(Element letter) {
  caret = new Element.span()..className = "caret";
  letter.append(caret);
}

getPreviousLetter(Element letter) {
  for(Element cur = letter != null?letter.previousElementSibling:null; cur != null; cur = cur.previousElementSibling) {
    if(cur.classes.contains("letter")) {
      return cur;
    }
  }
  return null;
}

getNextLetter(Element letter) {
  for(Element cur = letter != null?letter.nextElementSibling:null; cur != null; cur = cur.nextElementSibling) {
    if(cur.classes.contains("letter")) {
      return cur;
    }
  }
  return null;
}

