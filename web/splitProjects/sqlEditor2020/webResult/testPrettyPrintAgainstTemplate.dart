import '../../../html/examples/SLOTHTMLToConsoleConverter.dart';
import 'dart:io';
import 'package:test/test.dart';
import '../../../../lib/lib/grammarparser/tree/GrammarObject.dart';
import '../../../../lib/lib/grammarparser/tree/GrammarTest.dart';
import '../../../../lib/lib/util/GrammarUtils.dart';
import '../../../../lib/slot/runtime/objects/SLOTTreeNode.dart';
import 'sqlEditor2020.slp.dart';
import 'sqlEditor2020_console.dart';

main() {

  var testMap = {
    "T6e" : '''
''',
    "TWTF" : '''
SELECT matrNo
FROM (SELECT matrNo
      FROM zahlung
     )''',
    "TCustomFunction" : '''
SELECT Alexander(Sadowski)''',
    "TCustomFunction2" : '''
SELECT HUHAHA(),
       HAhUHI(gutenTag, 'guten Morgen')
FROM abc''',
    "TUnsigned" : '''
selEcT ALEXANDER
fRoM Sadowski''',
    "_" : '''
SELECT stefan
FROM hanenberg''',
    "TColumnDot" : '''
(SELECT B
 FROM TEACHES B
)''',
    "TComment" : '''
SELECT *
FROM dings
WHERE he IS NOT NULL''',
    "TComma" : '''
SELECT lal
FROM lil,
     lel,
     lol,
     lal''',
    "TCommaAs" : '''
SELECT lal
FROM lil,
     lel,
     lol lal''',
    "TVL7_1" : '''
SELECT *
FROM Auftrag
WHERE datum = DATE '1919-12-19\'''',
    "TVL7_2" : '''
SELECT *
FROM Auftrag
WHERE datum = INTERVAL '1919-12-19' MONTH''',
    "TVL7" : '''
SELECT *
FROM Auftrag
WHERE datum IN (DATE '1919-12-19', DATE '1919-12-19' + INTERVAL '1' MONTH)''',
    "TVL1" : '''
SELECT auftragsNr,
       datum
FROM Auftrag
WHERE zuliefererNr = 121212''',
    "TVL2" : '''
SELECT *
FROM Auftrag
WHERE datum BETWEEN DATE '2001-01-01' AND DATE '2001-12-31\'''',
    "TVL3" : '''
SELECT *
FROM Auftrag
WHERE datum BETWEEN DATE '2001-12-31' AND DATE '2001-01-01\'''',
    "TVL4" : '''
SELECT *
FROM Kunden
WHERE rabatt IN (5.50, 7.50, 10.00)''',
    "TVL5" : '''
SELECT *
FROM Kunden
WHERE rabatt NOT IN (5.50, 7.50, 10.00)''',
    "TVL6" : '''
SELECT *
FROM Kunden
WHERE NOT rabatt IN (5.50, 7.50, 10.00)''',
    "TVL7A" : '''
SELECT *
FROM Auftrag
WHERE datum IN (DATE '1919-12-19', DATE '1919-12-19' + INTERVAL '1' MONTH)''',
    "TVL8" : '''
SELECT *
FROM Kunden
WHERE kundeName LIKE 'M_ _er\'''',
    "TVL9" : '''
SELECT *
FROM Kunden
WHERE kundeName LIKE 'M%\'''',
    "TVL10" : '''
SELECT *
FROM dingsbums
WHERE (TIME '09:00:00', INTERVAL '50' MINUTE) OVERLAPS (TIME '02:15:00', INTERVAL '6' HOUR)''',
    "TVL11" : '''
SELECT *
FROM dingsbums
WHERE (TIME '09:00:00', INTERVAL '50' MINUTE) OVERLAPS (TIME '02:15:00', INTERVAL '6' HOUR)''',
    "TVL12" : '''
SELECT *
FROM dingsbums
WHERE a OVERLAPS b''',
    "TVL13_5" : '''
SELECT CAST ShortString AS SMALLINT
FROM dingsbums''',
    "TVL13" : '''
SELECT *
FROM dingsbums
WHERE CAST ShortString AS SMALLINT = 1''',
    "TVL13_1" : '''
SELECT *
FROM dingsbums
WHERE plz = CAST((SELECT plz
     FROM Tabelle
     WHERE ort = 'Ilmenau') AS SMALLINT)''',
    "TVL14" : '''
SELECT mitarbeiterNr,
       mitarbeiterName,
       mitarbeiterVorname,
       einstellung
FROM Mitarbeiter
WHERE einstellung < DATE '1965-01-01\'''',
    "TVL15" : '''
SELECT *
FROM Mitarbeiter''',
    "TVL16" : '''
SELECT Mitarbeiter.*,
       abteilungsName
FROM             Mitarbeiter
    NATURAL JOINAbteilung''',
    "TVL17" : '''
SELECT ProduktInformation.*,
       istBestand * stueckKosten AS nettoWert,
       istBestand * nettoPreis AS bruttoWert
FROM (            Produkt
      NATURAL JOINProduktLagertIn
      NATURAL JOINProduktLager
     ) AS ProduktInformation''',
    "TVL18" : '''
SELECT mitarbeiterName,
       (CURRENT_DATE - einstellung) AS betriebszugehörigkeit
FROM Mitarbeiter''',
    "TVL19" : '''
SELECT mitarbeiterName,
       (CURRENT_DATE - einstellung) betriebszugehörigkeit
FROM Mitarbeiter''',
    "TVL20_1" : '''
SELECT mitarbeiterName AS neueMitarbeiter
FROM Mitarbeiter''',
    "TVL20_2" : '''
SELECT a
FROM Mitarbeiter
WHERE CURRENT_DATE - einstellung < INTERVAL '24' MONTH''',
    "TVL20_3_1" : '''
SELECT CURRENT_DATE - einstellung AS betriebszugehörigkeit
FROM Mitarbeiter''',
    "TVL20_3" : '''
SELECT (CURRENT_DATE - einstellung) AS betriebszugehörigkeit
FROM Mitarbeiter''',
    "TVL20_4" : '''
SELECT mitarbeiterName AS neueMitarbeiter,
       (CURRENT_DATE - einstellung) AS betriebszugehörigkeit
FROM Mitarbeiter
WHERE CURRENT_DATE - einstellung < INTERVAL '24' MONTH''',
    "TVL21" : '''
SELECT Mitarbeiter.*,
       (gehalt * 13 + 500.00) / (220 * 8) AS stundenlohn
FROM Mitarbeiter''',
    "TVL22" : '''
SELECT COUNT(mitarbeiterNr) AS anzahlMitarbeiter
FROM Mitarbeiter''',
    "TVL23" : '''
SELECT MAX(gehalt) AS höchstesGehalt
FROM Mitarbeiter''',
    "TVL24" : '''
SELECT MIN(einstellung) AS ersteEinstellung,
       MAX(einstellung) AS letzteEinstellung
FROM Mitarbeiter''',
    "TVL25" : '''
SELECT SUM(gehalt) AS monatlicheGehaltsBelastung
FROM Mitarbeiter''',
    "TVL26" : '''
SELECT (AVG(nettoPreis) - AVG(stueckKosten)) / AVG(nettoPreis) * 100 AS gewinnMarge
FROM Produkt''',
    "TVL27" : '''
SELECT ((SUM(nettoPreis) - SUM(stueckKosten)) / SUM(nettoPreis) * 100) AS gewinnMarge
FROM Produkt''',
    "TVL28" : '''
SELECT AVG(rabatt) AS rabatt
FROM Kunden''',
    "TVL29" : '''
SELECT AVG(DISTINCTrabatt) AS rabatt
FROM Kunden''',
    "TVL30" : '''
SELECT COUNT(*) AS gesamtzahlKunden,
       COUNT(rabatt) AS gesamtzahlKundenMitRabatt,
       COUNT(DISTINCTort) AS anzahlWohnorte
FROM Kunden''',
    "TVL31" : '''
SELECT mitarbeiterName,
       mitarbeiterVorname,
       gehalt
FROM Mitarbeiter''',
    "TVL32" : '''
SELECT auftragsNr,
       mitarbeiterName,
       mitarbeiterVorname
FROM Auftrag,
     Mitarbeiter
WHERE bearbeiterNr = mitarbeiterNr''',
    "TVL33" : '''
SELECT auftragsNr,
       mitarbeiterName,
       mitarbeiterVorname
FROM          Auftrag
   CROSS JOINMitarbeiter
WHERE bearbeiterNr = mitarbeiterNr''',
    "TVL34" : '''
SELECT zuliefererName,
       teilNr,
       teilBez
FROM          Teil
   CROSS JOINZulieferer''',
    "TVL35" : '''
SELECT mitarbeiterName,
       mitarbeiterVorname,
       abteilungsName
FROM          Mitarbeiter
   CROSS JOINAbteilung
WHERE Mitarbeiter.abteilungsNr = Abteilung.abteilungsNr''',
    "TVL36" : '''
SELECT DISTINCT produktBez,
                abteilungsName
FROM          Produkt
   CROSS JOINArbeitetAn
   CROSS JOINAbteilung
WHERE    Produkt.produktNr = ArbeitetAn.produktNr
     AND ArbeitetAn.abteilungsNr = Abteilung.abteilungsNr
     AND Produkt.produktTyp = 'Waschmaschine\'''',
    "TVL37" : '''
SELECT DISTINCT produktBez,
                abteilungsName
FROM             Produkt
    NATURAL JOINArbeitetAn
    NATURAL JOINAbteilung
WHERE Produkt.produktTyp = 'Waschmaschine\'''',
    "TVL38" : '''
SELECT Produkt.*,
       Abteilung.*
FROM Produkt,
     ArbeitetAn,
     Abteilung
WHERE    Produkt.produktNr = ArbeitetAn.produktNr
     AND ArbeitetAn.abteilungsNr = Abteilung.abteilungsNr''',
    "TVL39" : '''
SELECT P.*,
       A.*
FROM Produkt P,
     ArbeitetAn AA,
     Abteilung A
WHERE    P.produktNr = AA.produktNr
     AND AA.abteilungsNr = A.abteilungsNr''',
    "TVL40" : '''
SELECT *
FROM Teil T,
     TeileLagertIn TLI,
     TeileLager TL
WHERE    T.teilNr = TLI.teilNr
     AND TLI.teileLagerNr = TL.teileLagerNr
     AND TL.teileLagerBez = 'Hochstapel\'''',
    "TVL41" : '''
SELECT T.teilNr,
       T.teilBez
FROM Teil T,
     TeileLagertIn TLI,
     TeileLager TL
WHERE    T.teilNr = TLI.teilNr
     AND TLI.teileLagerNr = TL.teileLagerNr
     AND TL.teileLagerBez = 'Hochstapel\'''',
    "TVL42" : '''
SELECT Zulieferer.*,
       Teil.*
FROM            Zulieferer
     INNER JOINLiefertUSING(zuliefererNr)
     INNER JOINTeilUSING(teilNr)''',
    "TVL43" : '''
SELECT Mitarbeiter.*
FROM            Mitarbeiter
     INNER JOINKunden
             ON(mitarbeiterNr = kundenNr)''',
    "TVL44" : '''
SELECT *
FROM            Mitarbeiter AS M
     INNER JOINAuftrag AS A
             ONM.mitarbeiterNr = A.mitarbeiterNr''',
    "TVL45" : '''
SELECT *
FROM            Mitarbeiter
     INNER JOINAuftragUSING(mitarbeiterNr)''',
    "TVL46" : '''
SELECT M.*
FROM            Mitarbeiter AS M
     INNER JOINAuftrag AS A
             ON(   M.mitarbeiterNr = A.mitarbeiterNr
                            AND A.datum = M.einstellung)''',
    "TVL47" : '''
SELECT Z.*
FROM            Zulieferer AS Z
     INNER JOINAuftrag AS A
             ON(Z.zuliefererNr = A.zuliefererNr)
WHERE datum < DATE '2000-01-01\'''',
    "TVL48" : '''
SELECT Z.*
FROM            Zulieferer AS Z
     INNER JOINAuftragUSING(zuliefererNr)
WHERE datum < DATE '2000-01-01\'''',
    "TVL49" : '''
SELECT Z.*
FROM             Zulieferer AS Z
    NATURAL JOINAuftrag
WHERE datum < DATE '2000-01-01\'''',
    "TVL50" : '''
SELECT R.*,
       produktBez,
       stueckKosten,
       (betrag / menge) AS verkaufsPreis
FROM            Produkt AS P
     INNER JOINRechnungsposition AS R
             ON(   P.produktNr = R.produktNr
                            AND stueckKosten > (betrag / menge))''',
    "TVL51" : '''
SELECT R.*,
       produktBez,
       stueckKosten,
       (betrag / menge) AS verkaufsPreis
FROM Produkt''',
    "TVL52" : '''
SELECT R.*,
       produktBez,
       stueckKosten,
       (betrag / menge) AS verkaufsPreis
FROM            Produkt AS P
     INNER JOINRechnungsposition AS R
             ON(stueckKosten > (betrag / menge))
WHERE P.produktNr = R.produktNr''',
    "TVL53" : '''
SELECT Rechnungsposition.*,
       produktBez,
       stueckKosten,
       (betrag / menge) AS verkaufsPreis
FROM             Produkt
    NATURAL JOINRechnungsposition
WHERE stueckKosten > (betrag / menge)''',
    "TVL54" : '''
SELECT *
FROM                 T1
     LEFT OUTER JOINT2
                  ON(a = d)''',
    "TVL55" : '''
SELECT *
FROM            T1
     INNER JOINT2
             ON(a = d)''',
    "TVL56" : '''
SELECT DISTINCT T1.*,
                NULL AS d,
                NULL AS e,
                NULL AS f
FROM          T1
   CROSS JOINT2
WHERE a != d''',
    "TVL57" : '''
SELECT *
FROM                        Produkt
    NATURAL LEFT OUTER JOINArbeitetAn
    NATURAL LEFT OUTER JOINAbteilung''',
    "TVL58" : '''
SELECT *
FROM                 Produkt
     LEFT OUTER JOINArbeitetAnUSING(produktNr)
     LEFT OUTER JOINAbteilungUSING(abteilungsNr)''',
    "TVL59" : '''
SELECT *
FROM                 Produkt
     LEFT OUTER JOINArbeitetAn
                  ON(Produkt.produktNr = ArbeitetAn.produktNr)
     LEFT OUTER JOINAbteilung ArbeitetAn
                  ON(ArbeitetAn.abteilungsNr = Abteilung.abteilungsNr)''',
    "TVL60" : '''
SELECT SUM(menge)
FROM            Zulieferer
     INNER JOINRechnungUSING(zuliefererNr)
     INNER JOINRechnungsPositionUSING(rechnungsNr)
WHERE    zuliefererName = 'PasstNix GmbH'
     AND datum = DATE '2000-02-01\'''',
    "TVL61" : '''
SELECT Teil.*
FROM            Teil
     INNER JOINSindBestandteilVonUSING(teilNr)
     INNER JOINProduktUSING(produktNr)
WHERE produktBez = 'WM53\'''',
    "TVL62" : '''
SELECT Teil.*
FROM             Teil
    NATURAL JOINSindBestandteilVon
    NATURAL JOINProdukt
WHERE produktBez = 'WM53\'''',
    "TVL63" : '''
SELECT teilNr,
       teilBez,
       teileLagerNr,
       teileLagerBez,
       zuliefererNr,
       zuliefererName
FROM             Zulieferer
    NATURAL JOINLiefert
    NATURAL JOINTeil
    NATURAL JOINTeilLagertIn
    NATURAL JOINTeileLager
WHERE istBestand < 50''',
    "TVL64" : '''
SELECT *
FROM            VerheiratetePers
     INNER JOINVerheiratetePers
             ONehepartner = persNr''',
    "TVL65" : '''
SELECT *
FROM            VerheiratetePers AS P1
     INNER JOINVerheiratetePers AS P2
             ON(P1.ehepartner = P2.persNr)''',
    "TVL66" : '''
SELECT *
FROM Mitarbeiter
WHERE mitarbeiterVorname = mitarbeiterName''',
    "TVL67" : '''
SELECT produktNr
FROM ProduktLagertIn
WHERE istBestand < 100''',
    "TVL67_1" : '''
SELECT produktNr
FROM ProduktLagertIn
WHERE (a - e) <= (b * c + d)''',
    "TVL68" : '''
SELECT *
FROM Teil
WHERE istBestand NOT BETWEEN 10 AND 1000''',
    "TVL69" : '''
SELECT mitarbeiterName,
       mitarbeiterVorname
FROM Mitarbeiter
WHERE    CURRENT_DATE - einstellung >= INTERVAL '25' YEAR
     AND CURRENT_DATE - einstellung < INTERVAL '26' YEAR''',
    "TVL71" : '''
SELECT M1.mitarbeiterNr,
       M1.mitarbeiterName,
       M1.mitarbeiterVorname,
       M2.mitarbeiterNr,
       M2.mitarbeiterName,
       M2.mitarbeiterVorname
FROM            Mitarbeiter AS M1
     INNER JOINMitarbeiter AS M2
             ON   (M1.mitarbeiterName = M2.mitarbeiterName)
                            AND (M1.mitarbeiterNr > M2.mitarbeiterNr)''',
    "TVL72" : '''
SELECT Rechnung.*
FROM             Rechnung
    NATURAL JOINRechnungsPosition
    NATURAL JOINAuftragsPosition
    NATURAL JOINZulieferer
WHERE    zuliefererName = 'VersprechViel GmbH'
     AND datum BETWEEN DATE '2001-01-01' AND DATE '2001-12-31'
     AND teilNr = 123321''',
    "TVL73" : '''
SELECT *
FROM Mitarbeiter
WHERE abteilungsNr IS NULL''',
    "TVL74" : '''
SELECT *
FROM Zulieferer
WHERE (ansprechPartner, ansprechpartTelNr) IS NULL''',
    "TVL75" : '''
SELECT hi
FROM bla
WHERE a IS TRUE''',
    "TVL76" : '''
SELECT hi
FROM bla
WHERE a IS NOT TRUE''',
    "TVL77" : '''
SELECT hi
FROM bla
WHERE a IS FALSE''',
    "TVL78" : '''
SELECT hi
FROM bla
WHERE a IS NOT FALSE''',
    "TVL79" : '''
SELECT hi
FROM bla
WHERE a IS UNKNOWN''',
    "TVL80" : '''
SELECT hi
FROM bla
WHERE a IS NOT UNKNOWN''',
    "TVL81" : '''
SELECT produktLagerBez AS lager,
       COUNT(produktNr) AS produkte
FROM ProduktLagertIn
GROUP BY produktLagerBez''',
    "TVL82" : '''
SELECT produktLagerBez AS lager,
       produktNr
FROM ProduktLagertIn
ORDERBYproduktLagerBez''',
    "TVL83" : '''
SELECT A.abteilungsName,
       M.mitarbeiterName,
       M.mitarbeiterVorname
FROM             Mitarbeiter AS M
    NATURAL JOINAbteilung AS A
GROUP BY A.abteilungsName''',
    "TVL84_09GROUPBYLOl" : '''
SELECT A.abteilungsName,
       M.mitarbeiterName,
       M.mitarbeiterVorname
FROM             Mitarbeiter AS M
    NATURAL JOINAbteilung AS A
ORDERBYA.abteilungsName,M.mitarbeiterName,M.mitarbeiterVorname''',
    "TVL85" : '''
SELECT A.abteilungsName,
       M.mitarbeiterName,
       M.mitarbeiterVorname
FROM             Mitarbeiter AS M
    NATURAL JOINAbteilung AS A
ORDERBYA.abteilungsName,M.mitarbeiterName,M.mitarbeiterVorname''',
    "TVL86" : '''
SELECT produktTyp,
       COUNT(kundenNr) AS kundenProProdukttyp
FROM             Kunden
    NATURAL JOINKauft
    NATURAL JOINProdukt
GROUP BY produktTyp''',
    "TVL87" : '''
SELECT produktLagerBez AS Lager,
       COUNT(produktNr) AS produkte
FROM ProduktLagertIn
GROUP BY produktLagerBez
HAVING  produktLagerBez = 'München'
OR produktLagerBez = 'Berlin\'''',
    "TVL88" : '''
SELECT produktLagerBez AS lager,
       COUNT(produktNr) AS produkte
FROM ProduktLagertIn
GROUP BY produktLagerBez
HAVINGistBestand < 500''',
    "TVL89" : '''
SELECT produktLagerBez AS lager,
       COUNT(produktNr) AS produkte
FROM ProduktLagertIn
WHERE istBestand < 500
GROUP BY produktLagerBez''',
    "TVL90" : '''
SELECT produktLagerBez AS lager,
       COUNT(produktNr) AS produkte
FROM ProduktLagertIn
GROUP BY produktLagerNr
HAVING  produktLagerBez = 'München'
OR produktLagerBez = 'Berlin\'''',
    "TVL91" : '''
SELECT produktLagerBez AS lager,
       COUNT(produktNr) AS produkte,
       SUM(istBestand) AS gesamtMenge
FROM             ProduktLagertIn
    NATURAL JOINProduktLager
GROUP BY produktLagerBez
HAVING   COUNT(produktNr) > 300
AND SUM(istBestand) < 10000''',
    "TVL92" : '''
SELECT produktTyp,
       AVG(nettoPreis) AS nettoPreis
FROM Produkt
WHERE produktTyp NOT IN ('Geschirrspüler', 'Waschmaschine')
GROUP BY produktTyp''',
    "TVL93" : '''
SELECT produktTyp,
       AVG(nettoPreis) AS nettoPreis
FROM Produkt
GROUP BY produktTyp
HAVINGproduktTyp NOT IN ('Geschirrspüler', 'Waschmaschine')''',
    "TVL94" : '''
SELECT produktTyp,
       AVG(nettoPreis) AS nettoPreis
FROM Produkt
GROUP BY produktTyp
HAVINGAVG(nettoPreis) > 500''',
    "TVL95" : '''
SELECT COUNT(*) AS anzahlUmfangreicherRechnungen
FROM RechnungsPosition
GROUP BY rechnungsNr
HAVINGCOUNT(rechnungsPos) > 10''',
    "TVL96" : '''
SELECT produktTyp,
       produktLagerNr,
       produktNr
FROM             ProduktLagertIn
    NATURAL JOINProdukt
WHERE istBestand > 0
GROUP BY produktTyp, produktLagerNr''',
    "TVL97" : '''
SELECT produktTyp,
       produktLagerNr
FROM             ProduktLagertIn
    NATURAL JOINProdukt
WHERE istBestand > 0
GROUP BY produktTyp, produktLagerNr''',
    "TVL98" : '''
SELECT produktTyp,
       produktLagerNr,
       AVG(nettoPreis) AS nettoPreis
FROM             ProduktLagertIn
    NATURAL JOINProdukt
WHERE istBestand > 0
GROUP BY produktTyp, produktLagerNr''',
    "TVL99" : '''
SELECT produktTyp,
       AVG(nettoPreis) AS nettoPreisÜberAlleLager
FROM (SELECT produktTyp,
            produktLagerNr,
            AVG(nettoPreis) AS nettoPreis
     FROM             ProduktLagertIn
         NATURAL JOINProdukt
     WHERE istBestand > 0
GROUP BY produktTyp, produktLagerNr
    )
GROUP BY produktTyp''',
    "TVL100" : '''
SELECT *
FROM Mitarbeiter
WHERE gehalt = (SELECT MAX(gehalt)
     FROM Mitarbeiter)''',
    "TVL101" : '''
SELECT *
FROM Mitarbeiter
WHERE ort IN (SELECT ort
     FROM Mitarbeiter
     WHERE gehalt = (SELECT MIN(gehalt)
          FROM Mitarbeiter))''',
    "TVL102" : '''
SELECT *
FROM Mitarbeiter AS MA1
WHERE MA1.gehalt > (SELECT MIN(gehalt) * 1,
            5
     FROM Mitarbeiter AS MA2
     WHERE MA1.abteilungsNr = MA2.abteilungsNr)''',
    "TVL103" : '''
SELECT abteilungsNr,
       AVG(gehalt)
FROM Mitarbeiter AS M1
WHERE gehalt > (SELECT AVG(gehalt)
     FROM Mitarbeiter AS M2
     WHERE M1.abteilungsNr = M2.abteilungsNr)
GROUP BY abteilungsNr''',
    "TVL104" : '''
SELECT COUNT(produktLagerNr)
FROM ProduktLagertIn
GROUP BY produktLagerNr
HAVINGCOUNT(produktNr) > (SELECT COUNT(*)
FROM Produkt) / 2''',
    "TVL105" : '''
SELECT COUNT(produktLagerNr)
FROM             ProduktLagertIn
    NATURAL JOINProdukt
WHERE stueckKosten > 500
GROUP BY produktLagerNr
HAVINGCOUNT(produktNr) > (SELECT COUNT(*)
FROM Produkt
WHERE stueckKosten > 500) / 2''',
    "TVL106" : '''
SELECT Mitarbeiter.*,
       gehalt - (SELECT AVG(gehalt)
       FROM Mitarbeiter) AS unterschiedZumDurchschnittsgehalt
FROM Mitarbeiter''',
    "TVL107" : '''
SELECT P.produktBez,
       P.produktNr,
       (SELECT SUM(istBestand)
       FROM ProduktLagertIn AS PLI
GROUP BY produktNr
HAVINGP.produktNr = PLI.produktNr) AS lagerBestand
FROM Produkt AS P''',
    "TVL108" : '''
SELECT P.produktBez,
       P.produktNr,
       lagerBestand
FROM             Produkt
    NATURAL JOIN(SELECT produktNr,
                        SUM(istBestand) AS lagerBestand
                 FROM ProduktLagertIn
GROUP BY produktNr
                )''',
    "TVL109" : '''
SELECT produktBez,
       produktNr,
       SUM(istBestand) AS lagerBestand
FROM             Produkt
    NATURAL JOINProduktLagertIn
GROUP BY produktNr''',
    "TVL110" : '''
SELECT AVG(nettoPreis) AS nettoPreis
FROM             Produkt
    NATURAL JOIN(SELECT DISTINCT produktNr
                 FROM ProduktLagertIn
                 WHERE istBestand > 0
                )''',
    "TVL111" : '''
SELECT *
FROM Abteilung
WHERE budget > (SELECT budget
     FROM Abteilung
     WHERE abteilungsNr = 121212)''',
    "TVL112" : '''
SELECT *
FROM Abteilung
WHERE (budget, mitarbeiterZahl) = (SELECT budget,
            mitarbeiterZahl
     FROM Abteilung
     WHERE abteilungsNr = 121212)''',
    "TVL113" : '''
SELECT *
FROM Abteilung
WHERE budget = (SELECT budget
     FROM Abteilung
     WHERE    abteilungsNr = 121212
          AND mitarbeiterZahl = (SELECT mitarbeiterZahl
             FROM Abteilung
             WHERE abteilungsNr = 121212))''',
    "TVL114" : '''
SELECT *
FROM Rechnung
WHERE zuliefererNr IN (SELECT zuliefererNr
     FROM Zulieferer
     WHERE ort = 'GroßKleckersdorf')''',
    "TVL115" : '''
SELECT Rechnung.*
FROM             Rechnung
    NATURAL JOINZulieferer
WHERE (zuliefererName, datum) IN (('Abzock', CURRENT_DATE - INTERVAL '1' DAY), ('Abzock', CURRENT_DATE - INTERVAL '2' DAY), ('Chaotikus', CURRENT_DATE - INTERVAL '1' DAY), ('Chaotikus', CURRENT_DATE - INTERVAL '2' DAY))''',
    "TVL116" : '''
SELECT Rechnung.*
FROM             Rechnung
    NATURAL JOINZulieferer
WHERE    datum IN (CURRENT_DATE - INTERVAL '1' DAY, CURRENT_DATE - INTERVAL '2' DAY)
     AND zuliefererName IN ('Abzock', 'Chaotikus')''',
    "TVL117" : '''
SELECT mitarbeiterName,
       mitarbeiterVorname
FROM Mitarbeiter
WHERE (mitarbeiterName, mitarbeiterVorname) IN (SELECT kundenName,
            kundenVorname
     FROM Kunden
     WHERE    rabatt < 10
          AND kundenVorname LIKE 'G%')''',
    "TVL118" : '''
SELECT mitarbeiterName,
       mitarbeiterVorname
FROM Mitarbeiter
WHERE    mitarbeiterVorname LIKE 'G%'
     AND mitarbeiterNr IN (SELECT kundenNr
        FROM Kunden
        WHERE rabatt < 10)''',
    "TVL119" : '''
SELECT mitarbeiterName,
       mitarbeiterVorname
FROM Mitarbeiter
WHERE (mitarbeiterName, mitarbeiterVorname) =ANY(SELECT kundenName,
            kundenVorname
     FROM Kunden
     WHERE    rabatt < 10
          AND kundenVorname LIKE 'G%')''',
    "TVL120" : '''
SELECT mitarbeiterName,
       mitarbeiterVorname
FROM Mitarbeiter
WHERE    mitarbeiterVorname LIKE 'G%'
     AND mitarbeiterNr IN (SELECT kundenNr
        FROM Kunden
        WHERE rabatt < 10)''',
    "TVL121" : '''
SELECT mitarbeiterName,
       mitarbeiterVorname
FROM Mitarbeiter
WHERE (mitarbeiterName, mitarbeiterVorname) =ANY(SELECT kundenName,
            kundenVorname
     FROM Kunden
     WHERE    rabatt < 10
          AND kundenVorname LIKE 'G%')''',
    "TVL122" : '''
SELECT mitarbeiterName,
       mitarbeiterVorname
FROM Mitarbeiter
WHERE mitarbeiterNr IN (SELECT bearbeiterNr
     FROM Auftrag
     WHERE datum BETWEEN DATE '2000-01-01' AND DATE '2000-12-31')''',
    "TVL123" : '''
SELECT mitarbeiterName,
       mitarbeiterVorname
FROM Mitarbeiter
WHERE EXISTS(SELECT *
     FROM Auftrag
     WHERE    datum BETWEEN DATE '2000-01-01' AND DATE '2000-12-31'
          AND mitarbeiterNr = bearbeiterNr)''',
    "TVL124" : '''
SELECT produktTyp,
       AVG(nettoPreis) AS nettoPreisÜberAlleLager
FROM (SELECT produktTyp,
            produktLagerNr,
            AVG(nettoPreis) AS nettoPreis
     FROM             ProduktLagertIn
         NATURAL JOINProdukt
     WHERE lagerbestand > 0
GROUP BY produktTyp, produktLagerNr
    )
GROUP BY produktTyp''',
    "TVL125" : '''
SELECT *
FROM (SELECT produktTyp,
            produktLagerNr,
            AVG(nettoPreis) AS nettoPreis
     FROM             ProduktLagertIn
         NATURAL JOINProdukt
     WHERE lagerbestand > 0
GROUP BY produktTyp, produktLagerNr
    ) AS ProdukttypLager''',
    "TVL126" : '''
SELECT produktTyp,
       AVG(nettoPreis) AS nettoPreisÜberAlleLager
FROM ProdukttypLager
GROUP BY produktTyp''',
    "TVL127" : '''
SELECT *
FROM Kunden AS K1
WHERE rabatt > (SELECT AVG(rabatt)
     FROM Kunden AS K2
     WHERE    K1.ort = K2.ort
          AND K1.kundenNr <> K2.kundenNr)''',
    "TVL128" : '''
SELECT kundenName AS name,
       plz,
       ort,
       strasse,
       hausNr
FROM KundenUNIONSELECT zuliefererName AS name,
       plz,
       ort,
       strasse,
       hausNr
FROM ZuliefererUNIONSELECT mitarbeiterName AS name,
       plz,
       ort,
       strasse,
       hausNr
FROM Mitarbeiter''',
    "TVL129" : '''
SELECT kundenNr AS identifikationsNr,
       kundenName AS name,
       kundenVorname AS vorname
FROM KundenINTERSECTSELECT mitarbeiterNr AS identifikationsNr,
       mitarbeiterName AS name,
       mitarbeiterVorname AS vorname
FROM Mitarbeiter''',
    "TVL130" : '''
SELECT kundenNr,
       kundenName,
       kundenVorname
FROM Kunden
WHERE EXISTS(SELECT *
     FROM Mitarbeiter
     WHERE kundenNr = mitarbeiterNr)''',
    "TVL131" : '''
SELECT kundenNr,
       kundenName,
       kundenVorname
FROM Kunden
WHERE kundenNr =SOME(SELECT kundenNr AS identifikationsNr
     FROM KundenINTERSECTSELECT mitarbeiterNr AS identifikationsNr
     FROM Mitarbeiter)''',
    "TVL132" : '''
SELECT kundenNr,
       kundenName,
       kundenVorname
FROM Kunden
WHERE kundenNr =SOME(SELECT mitarbeiterNr
     FROM Mitarbeiter)''',
    "TVL133" : '''
SELECT kundenNr AS identifikationsNr,
       kundenName AS name,
       kundenVorname AS vorname
FROM KundenEXCEPTSELECT mitarbeiterNr AS identifikationsNr,
       mitarbeiterName AS name,
       mitarbeiterVorname AS vorname
FROM Mitarbeiter''',
    "TVL134" : '''
SELECT kundenNr,
       kundenName,
       kundenVorname
FROM Kunden
WHERE NOT EXISTS(SELECT *
     FROM Mitarbeiter
     WHERE kundenNr = mitarbeiterNr)''',
    "TVL135" : '''
SELECT kundenNr,
       kundenName,
       kundenVorname
FROM Kunden
WHERE kundenNr =SOME(SELECT kundenNr AS identifikationsNr
     FROM KundenEXCEPTSELECT mitarbeiterNr AS identifikationsNr
     FROM Mitarbeiter)''',
    "TVL136" : '''
SELECT kundenNr,
       kundenName,
       kundenVorname
FROM Kunden
WHERE kundenNr !=ALL(SELECT mitarbeiterNr
     FROM Mitarbeiter)''',
    "TVL137" : '''
SELECT mitarbeiterName,
       mitarbeiterVorname
FROM Mitarbeiter
ORDERBYmitarbeiterName,mitarbeiterVorname''',
    "TVL138" : '''
SELECT datum,
       bearbeiterNr,
       auftragsNr,
       auftragsPos,
       menge
FROM             Auftrag
    NATURAL JOINAuftragsposition
WHERE datum BETWEEN DATE '2000-01-01' AND DATE '2000-12-31'
ORDERBYdatum,bearbeiterNr''',
    "TVL_FINAL_1" : '''
SELECT produktTyp,
       produktLagerBez,
       AVG(nettoPreis) AS nettoPreis,
       COUNT(produktNr) AS anzahlUnterschiedlicherProdukte
FROM             Produkt
    NATURAL JOINProduktLagertIn
    NATURAL JOINProduktLager''',
    "TVL_FINAL_2_3" : '''
SELECT *
FROM A
WHERE    produktTyp = 'Geschirrspüler'
     AND produktBez LIKE 'GS%'
     AND produktBez LIKE 'WM%\'''',
    "TVL_FINAL_2_2" : '''
SELECT *
FROM A
WHERE      produktTyp = 'Geschirrspüler'
       AND produktBez LIKE 'GS%'
     OR    produktTyp = 'Waschmaschine'
       AND produktBez LIKE 'WM%\'''',
    "TVL_FINAL_2_1" : '''
SELECT *
FROM A
WHERE (     produktTyp = 'Geschirrspüler'
       AND produktBez LIKE 'GS%'
     OR    produktTyp = 'Waschmaschine'
       AND produktBez LIKE 'WM%')''',
    "TVL_FINAL_2" : '''
SELECT *
FROM A
WHERE    (     produktTyp = 'Geschirrspüler'
          AND produktBez LIKE 'GS%'
        OR    produktTyp = 'Waschmaschine'
          AND produktBez LIKE 'WM%')
     AND produktLagerBez NOT IN ('Essen', 'München')''',
    "TVL_FINALBOSS" : '''
SELECT produktTyp,
       produktLagerBez,
       AVG(nettoPreis) AS nettoPreis,
       COUNT(produktNr) AS anzahlUnterschiedlicherProdukte
FROM             Produkt
    NATURAL JOINProduktLagertIn
    NATURAL JOINProduktLager
WHERE    (     produktTyp = 'Geschirrspüler'
          AND produktBez LIKE 'GS%'
        OR    produktTyp = 'Waschmaschine'
          AND produktBez LIKE 'WM%')
     AND pro - duktLagerBez NOT IN ('Essen', 'München')
GROUP BY produktTyp, produktLagerBez
HAVINGCOUNT(produktNr) BETWEEN 5 AND 20
ORDERBYnettoPreis,produktTypDESC''',
    "TComment_1" : '''
SELECT lol
FROM ding''',
    "T0" : '''
SELECT 1 dfk
FROM a''',
    "T123" : '''
SELECT dings
FROM ((SELECT dings
      FROM dangs
     )
    )''',
    "T999" : '''
SELECT lol
FROM ((SELECT hi
      FROM lal
     )
    )''',
    "T55" : '''
SELECT hi
FROM ding''',
    "TJOIN_USING" : '''
SELECT hi
FROM      ding
     JOINbingUSING(a,b,c)''',
    "TWHERE_1" : '''
SELECT hi
FROM ding
WHERE a = 1''',
    "TWHERE_LIKE" : '''
SELECT hi
FROM ding
WHERE a LIKE 'dings\'''',
    "TWHERE_IN" : '''
SELECT hi
FROM ding
WHERE lol IN ((SELECT hi
     FROM dings), (SELECT ding
     FROM Dingding))''',
    "TWHERE_IN2" : '''
SELECT hi
FROM ding
WHERE a IN (SELECT hi
     FROM      (            lol
                NATURAL JOINbeb
               )
          JOINding
            ONtrue
     WHERE lol = 1)''',
    "TWHERE_QUANT_ALL" : '''
SELECT hi
FROM ding
WHERE lol =ALL(SELECT dingsbums
     FROM lolol)''',
    "TWHERE_QUANT_LIKE_ANY" : '''
SELECT lol
FROM lala
WHERE lol LIKEANY(SELECT hi
     FROM (     ding
            JOINLAL
              ON(true - true)
          )
     WHERE    (xD - (2 + 1) = 100)
          AND 1 = 1)''',
    "TWHERE_EXISTS" : '''
SELECT lolwiegehts
FROM ding
WHERE EXISTS(SELECT dingdong
     FROM hahaha)''',
    "THeftigeKlammern" : '''
SELECT lolwiegehts
FROM (     ding
       JOINbla
         ON(  (   NOT (bla)
                      AND ble)
                    OR blo IS NOT NULL)
     )''',
    "T_Using" : '''
SELECT lol
FROM      lol
     JOINaUSING(a,b,c)''',
    "T2" : '''
SELECT hi
FROM asd''',
    "T10" : '''
SELECT 10000 + hopplaboppla - dingsbums + -10505 + lol - -hi
FROM wiegehtsdirso''',
    "T3" : '''
SELECT *
FROM LAL''',
    "T4" : '''
SELECT hi,
       di,
       bye
FROM lel''',
    "T5" : '''
SELECT hi + 54
FROM dai''',
    "T6" : '''
SELECT hi + 0
FROM dings''',
    "T7" : '''
SELECT hi - 100,
       hi - 1043
FROM dingdong''',
    "T8" : '''
SELECT 10 + 10,
       10,
       hi - 555
FROM tralala''',
    "T9" : '''
SELECT 10 - 10 + 2130 - tralala + dingdong - -5
FROM dingsbums''',
    "T11" : '''
SELECT FF
FROM wiegehtsdirso''',
    "T14" : '''
SELECT 'lal'
FROM lol''',
    "T26" : '''
SELECT '0110'
FROM yey''',
    "T27" : '''
SELECT 'FFA'
FROM yup''',
    "T28" : '''
SELECT hi.*
FROM dings''',
    "T29" : '''
SELECT hi.*,
       '0110',
       dindgong
FROM dings''',
    "T33" : '''
SELECT asd
FROM lalas''',
    "T34" : '''
SELECT lol.hi
FROM lelel''',
    "T35" : '''
SELECT COUNT(*)
FROM lal''',
    "T37" : '''
SELECT AVG(dongs)
FROM lel''',
    "T38" : '''
SELECT AVG('WTF')
FROM lal''',
    "T39_1" : '''
SELECT AVG('WTF'),
       MAX(B'1101')
FROM dings''',
    "T39" : '''
SELECT AVG('WTF'),
       MAX(f + a + N'bob'),
       MIN(dingsbums - dingdong * wiegehts),
       SUM(B'1001' * hi)
FROM dings''',
    "T40" : '''
SELECT 'AVG'
FROM dings''',
    "T41" : '''
SELECT dingdong AS bingbong,
       '1001' AS dingsbums
FROM dingdong''',
    "T42" : '''
SELECT (SELECT dings
       FROM dings)
FROM dings''',
    "T43" : '''
SELECT NULLIF(1, 2)
FROM dings''',
    "T44" : '''
SELECT COALESCE('hi', 'bye', dong)
FROM dingdong''',
    "T45" : '''
SELECT firstjoin
FROM          a
   CROSS JOINb''',
    "T46" : '''
SELECT secondjoin
FROM      a
     JOINb
       ONc
     JOINb
       ONf''',
    "T47" : '''
SELECT thirdjoin
FROM                  abc
         NATURAL JOINabc
    NATURAL LEFT JOINbaba''',
    "T48" : '''
SELECT thirdjoin
FROM             (SELECT dings
                 FROM dangs
                ) bum
    NATURAL JOINbaba''',
    "T49" : '''
SELECT a
FROM      b
     JOINc
       ONa''',
    "T50" : '''
SELECT a
FROM             b
    NATURAL JOINc''',
    "T51" : '''
SELECT dingdong
FROM (            a
      NATURAL JOINb
     ) AS c ( a , b , c )''',
    "T52" : '''
SELECT dingdong
FROM             (            a
                  NATURAL JOINb
                 ) AS a
    NATURAL JOINc''',
    "T53" : '''
SELECT dongdong
FROM             (     a
                   JOINb
                     ONc
                 ) AS b
    NATURAL JOIN(            dingdongs
                  NATURAL JOINb
                 ) AS c''',
    "T54" : '''
SELECT a
FROM      b
     JOINc
       ONz = z''',
    "T55_1" : '''
SELECT a
FROM             b
    NATURAL JOIN(            c
                  NATURAL JOINb
                 ) AS bum''',
    "T56" : '''
SELECT a
FROM             b
    NATURAL JOIN(SELECT a
                 FROM b
                ) AS c''',
    "T57" : '''
SELECT (SELECT (SELECT a
              FROM bbb)
       FROM aaa) AS lal
FROM             dingdong
    NATURAL JOINdingdong''',
    "T58" : '''
SELECT dingsbums
FROM      (                b
            FULL OUTER JOINa
                         ONb
          )
     JOINa
       ONc''',
    "T59" : '''
SELECT lal
FROM      (            (     a
                          JOINb
                            ONc IN (a, b, c)
                        ) AS c
           NATURAL JOINb
          )
     JOIN(SELECT hi
          FROM ding
         ) AS lol
       ONbla.a = ble.a''',
    "T60" : '''
SELECT hi
FROM a
WHERE a = b''',
    "T61" : '''
SELECT hi
FROM a
WHERE    a = b
     AND a = c''',
    "T62" : '''
SELECT hi
FROM a
WHERE    A = 5
     AND a + b = 5''',
    "T63" : '''
SELECT hi
FROM a
WHERE    (  a = 5
        OR a - 5 = 5)
     AND NOT a = 5''',
    "T64" : '''
SELECT hi
FROM a
WHERE 'hi' = my - 5''',
    "T65" : '''
SELECT hi
FROM b
WHERE a IS NOT NULL''',
    "T66" : '''
SELECT byebye
FROM b
WHERE    (  a IS NOT NULL
        OR b = a)
     AND (b - 10 IN (SELECT hi
        FROM             b
            NATURAL JOINc))''',
    "T67" : '''
SELECT dingdong
FROM b
WHERE    (a = 'bingbong')
     AND dingdong = 5''',
    "T68" : '''
SELECT bongdong
FROM bangbang
WHERE a = 1
GROUP BY bingbong
HAVING bingbong > COUNT(dings)''',
    "T69" : '''
SELECT dings
FROM dongs
GROUP BY a, b, c''',
    "T70" : '''
SELECT lal
FROM dingsbums
ORDER BY lal ASC, dings DESC, lol''',
    "T1" : '''
SELECT B'0110'
FROM lol''',
    "T100" : '''
SELECT hi
FROM dings
WHERE bom > bim''',
    "TPrettyPrint" : '''
SELECT hello.bing,
       hello.ding
FROM dingdangdong''',
    "TSubQueryLOL" : '''
SELECT hallo
FROM (SELECT lel
     FROM ding
    ) AS dong''',
    "TSubQueryLOL2" : '''
SELECT hallo
FROM      (SELECT lel
          FROM ding
         ) AS dong
     JOIN(SELECT lel
          FROM ding
         ) AS dong
       ONdings.hi = lol.bye''',
    "TSubQueryLOL3" : '''
SELECT hallo
FROM      (SELECT lel
          FROM ding
         ) AS dong
     JOIN(SELECT lel
          FROM ding
         ) AS dong
       ON   dings.hi = lol.bye
                AND dings.hi = lol.bye''',
    "TDistinct" : '''
SELECT DISTINCT hi,
                hey,
                ho
FROM lol''',
    "TAll" : '''
SELECT ALL hi,
           hey,
           ho
FROM lol''',
    "TWTFRename" : '''
SELECT lol
FROM             lel
    NATURAL JOIN(SELECT lol
                 FROM ding
                ) hi''',
    "TCrossJoin" : '''
SELECT hi
FROM          lal
   CROSS JOIN(SELECT leeel
              FROM hi
             ) dool''',
    "TCommaSubQuery" : '''
SELECT hi
FROM lal,
     (SELECT lilolol
      FROM dingdong
     ) hi'''
  };

/*

  */
  project =
      new SLOTHTMLToConsoleConverter().constructProject(new sqlEditor2020());
  project.initProject();

  List<String> sqlStrings = new File
    ("web\\splitProjects\\sqlEditor2020\\slot\\slopeFile")
      .readAsStringSync().split("###");
  SLOPEGrammarObject grammar = parseGrammar(sqlStrings[1].substring(" SLOPE".length));

  for(GrammarTest grammarTest in grammar.tests) {
    if(!grammarTest.fail_scanning
    && !grammarTest.fail_parsing
    && testMap.containsKey(grammarTest.testname)) {
      test(grammarTest.testname, () {
        SLOTTreeNode node = grammar.parseString(grammarTest.teststring).toSLOTTreeNode();
        String prettyString = project.runFunctionUnwrap("prettyPrintNode", [node]);
        //print(prettyString);
        expect(prettyString, (testMap[grammarTest.testname] as String));

      });
    }
  }


}

