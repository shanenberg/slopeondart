import '../../../html/examples/SLOTHTMLToConsoleConverter.dart';
import '../../../../lib/slot/runtime/SLOTProject.dart';
import 'sqlEditor2020.slp.dart';
import 'dart:io' as IO;

SLOTProject project;


main() {
  project =
    new SLOTHTMLToConsoleConverter().constructProject(new sqlEditor2020());
  project.initProject();

  /*var input = IO.stdin.readLineSync();
  if(input == "prettyPrint") {
    prettyPrint();
  }*/
  var slotResult = project.runProject();
}

prettyPrint() {
  var result = project.runFunctionUnwrap("prettyPrint");
  print(result);
}



