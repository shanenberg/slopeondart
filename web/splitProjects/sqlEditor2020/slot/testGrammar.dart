import 'dart:io';

import '../../../../lib/lib/grammarparser/tree/GrammarObject.dart';
import '../../../../lib/lib/util/GrammarUtils.dart';

main() {
  List<String> sqlStrings = new File
    ("web\\splitProjects\\sqlEditor2020\\slot\\slopeFile")
      .readAsStringSync().split("###");

  SLOPEGrammarObject go = parseGrammar(sqlStrings[1].substring(" SLOPE".length));
  go.doTests();

}