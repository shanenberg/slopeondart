import '../../../wtf_tols/TOLSFunctionFamily.dart';
import '../../../wtf_tols/wtf.dart';
import 'dart:io';

import '../../../../lib/lib/parser/tree/TreeNode.dart';
import '../../../../lib/lib/util/GrammarUtils.dart';

import '../../../../lib/slot/runtime/objects/SLOTTreeNode.dart';


main() {
  List<String> sqlStrings = new File
    ("web\\splitProjects\\sqlEditor2020\\slot\\slopeFile")
      .readAsStringSync().split("###");

  var grammar = parseGrammar(sqlStrings[1].substring(" SLOPE".length));

  TreeNode hello = grammar.parseString('''
      START;
      GEHE vor;
      SAGE hallo;
      ENDE;
  ''');

  SLOTTreeNode node = hello.toSLOTTreeNode();
  print("hi");

  TreeNodeFunctionFamily fam = new TreeNodeFunctionFamily({
    n("start").AND:(ChainedTOLSFrame thiz) {
      var result = new SLOTTreeNode.fromName("PROGRAM");
      thiz.resultNode.addChild(result);
      thiz.advance(thiz.grammarNode, result);
    },
    ALL : (ChainedTOLSFrame thiz) {
      thiz.advance(thiz.grammarNode, thiz.resultNode);
    }
  });




  TreeNodeFunctionFamily all = new TreeNodeFunctionFamily({
    ALL : (TOLSTreeNodeFrame thiz) {
      for(var child in thiz.grammarNode.children.asSLOTTreeNodeList()) {
        thiz.treeFunction(child, thiz.resultNode);
      }
    }
  });

  TFunctionChain chain = new TFunctionChain([fam, all]);
  var frame = chain.run(node, new SLOTTreeNode.fromName("ROOT"), []);



}