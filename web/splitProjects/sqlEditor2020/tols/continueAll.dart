import '../../../wtf_tols/TOLSFunctionFamily.dart';
import '../../../wtf_tols/wtf.dart';

TreeNodeFunctionFamily continueAll = new TreeNodeFunctionFamily({
  ALL : (TOLSTreeNodeFrame thiz) {
    for(var child in thiz.grammarNode.children.asSLOTTreeNodeList()) {
      thiz.treeFunction(child, thiz.resultNode);
    }
  }
});
