import '../../../../lib/slot/runtime/objects/SLOTTreeNode.dart';
import '../../../wtf_tols/TOLSFunctionFamily.dart';
import '../../../wtf_tols/wtf.dart';

import '../../../../lib/lib/parser/tree/TreeNode.dart';
import '../../../../lib/lib/util/GrammarUtils.dart';

import '../../../../lib/slot/runtime/objects/SLOTTreeNode.dart';
import 'continueAll.dart';


generateHTML(SLOTTreeNode node) {
  TreeNodeFunctionFamily functionFamily = new TreeNodeFunctionFamily({
    namedAnd() : (ChainedTOLSFrame frame) {
      frame.advanceParams(frame.grammarNode, frame.resultNode, frame.additionalParams);
    }

    ,
    ALL : (ChainedTOLSFrame frame) {
      frame.advanceParams(frame.grammarNode, frame.resultNode, frame.additionalParams);
    }
  });

  TFunctionChain fct_generateHTML = new TFunctionChain([functionFamily, continueAll]);
}