import '../webResultWriter.dart';
import 'dart:io';

import '../html/examples/SLOTLibToFileGenerator.dart';
import 'package:path/path.dart';



/**
 *
 *
 */



main(List<String> arguments) {
  if(arguments.length == 0) {
    generateAllProjects();
  } else {
    generateProject(arguments[0]);
  }

}


generateProject(String projectName) {

  Directory projectDir = new Directory(Directory.current.path + "\\web\\splitProjects\\$projectName");
  createWebResultFolderIfNotExists(projectDir);
  String bla = generateFullProjectString(projectDir);
  new File(projectDir.path + "\\webResult\\$projectName.slp").writeAsStringSync(bla);


  for(File aFile in allSlpFiles("\\web\\splitProjects\\$projectName")) {
    print(basename(aFile.path));
    toDartFile(aFile);
  }
}

generateAllProjects() {
  List<Directory> directories = new Directory(Directory.current.path + "\\web\\splitProjects")
      .listSync(recursive: false, followLinks: false)
      .where((FileSystemEntity e) {
    return e is Directory;
  }).map((FileSystemEntity e)
  { return e as Directory;}).toList();




  for(Directory projectDir in directories) {
    createWebResultFolderIfNotExists(projectDir);
    String fullProjectString = generateFullProjectString(projectDir);
    new File(projectDir.path + "\\webResult\\${projectDir.path.split("\\").last}.slp").writeAsStringSync(fullProjectString);
  }

  for(File aFile in allSlpFiles("\\web\\splitProjects")) {
    print(basename(aFile.path));
    toDartFile(aFile);
  }
}


List<File> allSlpFiles(String pathSuffix) {
  Directory currentDir =
  new Directory(
      Directory.current.path + pathSuffix);

  List<FileSystemEntity> l = currentDir.listSync(recursive: true, followLinks: false);

  return
    l
        .map((FileSystemEntity e) {return new File(e.path);}).toList()
        .where((File file) {
          return file.existsSync()
              && file.path.endsWith(".slp");
        }).toList();
}


String generateFullProjectString(Directory currentDir) {

  String retString = "";

  List<SLOTProjectPart> parts = [];

  for(File file in allPartFiles(currentDir)) {
    parts.add(parsePart(file.readAsStringSync()));
  }

  retString += "### SLOPE\n";
  for(SLOTProjectPart part in parts) {
    retString += part.slope;
  }

  retString += "\n### INPUT\n";
  for(SLOTProjectPart part in parts) {
    retString += part.input;
  }

  retString += "\n### SLOT\n";


  for(SLOTProjectPart part in parts) {
    retString += part.lib;
  }

  for(SLOTProjectPart part in parts) {
    retString += part.include;
  }

  for(SLOTProjectPart part in parts) {
    retString += part.varDef;
  }

  for(SLOTProjectPart part in parts) {
    retString += part.function;
  }

  for(SLOTProjectPart part in parts) {
    retString += part.tree;
  }

  return retString;
}


List<File> allPartFiles(Directory currentDir) {
  /*Directory currentDir =
  new Directory(
      Directory.current.path + "\\web\\html\\splitProjects\\$directoryName");*/

  List<FileSystemEntity> l = currentDir.listSync(recursive: true, followLinks: false);

  return
    l
        .map((FileSystemEntity e) {return new File(e.path);}).toList()
        .where((File file) {
      return file.existsSync() && !file.path.endsWith(".slp") && !file.path.contains("webResult");}).toList();
}

SLOTProjectPart parsePart(String part) {
  /*SLOPEGrammarObject grammar = parseGrammar('''
    SLOTPartFile

    Rules: Start

    Start => (Block)+.
    Block => (SlopeBlock | InputBlock | VarDefBlock | FuncBlock | TreeFuncBlock).


    Scanner:

    SlopeBlock => "### SLOPE(^#|#^#|##^#)+".
    InputBlock => "### INPUT(^#|#^#|##^#)+".
    VarDefBlock => "### VARDEF(^#|#^#|##^#)+".
    FuncBlock => "### FUNCTION(^#|#^#|##^#)+".
    TreeFuncBlock => "### TREE(^#|#^#|##^#)+".
  ''');
*/



  List<String> miniParts = part.split("###");

  SLOTProjectPart partObject = new SLOTProjectPart();

  for(String miniPart in miniParts) {
    if(miniPart.startsWith(" SLOPE")) {
      partObject.slope += miniPart.substring(" SLOPE".length);
    }
    else if(miniPart.startsWith(" INPUT")) {
      partObject.input += miniPart.substring(" INPUT".length);
    }
    else if(miniPart.startsWith(" VARDEF")) {
      partObject.varDef += miniPart.substring(" VARDEF".length);
    }
    else if(miniPart.startsWith(" FUNCTION")) {
      partObject.function += miniPart.substring(" FUNCTION".length);
    }
    else if(miniPart.startsWith(" TREE")) {
      partObject.tree += miniPart.substring(" TREE".length);
    }
    else if(miniPart.startsWith(" LIB")) {
      partObject.lib += miniPart.substring(" LIB".length);
    }
    else if(miniPart.startsWith(" INCLUDE")) {
      partObject.include += miniPart.substring(" INCLUDE".length);
    }

    print(miniPart);
  }


  return partObject;
}



class SLOTProjectPart {
  String slope = "";
  String input = "";
  String varDef = "";
  String function = "";
  String tree = "";
  String lib = "";
  String include = "";
}
