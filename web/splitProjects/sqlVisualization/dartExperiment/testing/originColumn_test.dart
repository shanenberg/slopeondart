import '../projectStuff/projectStuff.dart';
import 'package:test/test.dart';

main() {
  initProject();
  testFirstColName();
  testOriginColumn();
}

testFirstColName() {


  test('''stuff''', () {
    expectOriginColumn('''
            SELECT sq.Col
            FROM (SELECT sq2.Col
                  FROM (SELECT tb.Col
                          FROM tb
                       )sq2
                      JOIN
                       (SELECT afg.LOL
                          FROM afg
                       )sq3
                       ON
                       sq2.Col = sq3.Col
                                    
                       
                       
                 ) sq
            ''',
        "tb.Col");
  });



  test("1 hi -> hi", () {
    expectFirstColName(
        "SELECT hi FROM ding",
        "hi"
    );
  });
  test("2 ding.hi -> hi", () {
    expectFirstColName(
        "SELECT ding.hi FROM ding",
        "hi"
    );
  });
  test("3 ding.hi fx wy -> hi", () {
    expectFirstColName(
        '''SELECT ding.hi 
           FROM ding NATURAL JOIN bing
           WHERE ding.bye = bing.ciao''',
        "hi"
    );
  });
  test("4 ding.hi AS HEY -> HEY", () {
    expectFirstColName(
        '''SELECT ding.hi AS HEY 
           FROM ding''',
        "HEY"
    );
  });
  test("5 ding.hi HEY -> HEY", () {
    expectFirstColName(
        '''SELECT ding.hi HEY 
           FROM ding''',
        "HEY"
    );
  });

  test("6 ding.hi AS HEY fx wy -> HEY", () {
    expectFirstColName(
        '''SELECT ding.hi AS HEY 
           FROM ding NATURAL JOIN bing
           WHERE ding.bye = bing.ciao''',
        "HEY"
    );
  });

}

testOriginColumn() {
  test("ding.hi -> ding.hi", () {
    expectOriginColumn('''
          SELECT ding.hi
          FROM ding
    ''', "ding.hi");
  });

  test("0 Subqueries, 1 Table", () {
    expectOriginColumn('''
          SELECT ding.hi AS hey
          FROM ding
    ''', "ding.hi");
  });

  test("0 Subqueries, 3 Joined Tables", () {
    expectOriginColumn('''
          SELECT ding.hi AS hey
          FROM ding NATURAL JOIN bong JOIN hung ON bong.boo = hung.boo
    ''', "ding.hi");
  });



  test('''1 Subquery with 1 renamed column''', () {
    expectOriginColumn('''
          SELECT ding.hi 
          FROM (SELECT bing.hey AS hi
                FROM bing) ding
          ''',
          "bing.hey");
  });

  test('''1 Subquery with 2 Columns''', () {
    expectOriginColumn('''
            SELECT sq.Col
            FROM (SELECT tb.Col, tb.LOL
                  FROM tb) sq
            ''',
            "tb.Col");
  });

  test('''     sq.COLL 
            F (  tb.lel, tb.COLL
                 F tb 
              ) sq -> tb.COLL''', () {
    expectOriginColumn('''
            SELECT sq.Col
            FROM (SELECT tb.LOL, tb.Col
                  FROM tb
                 ) sq
            ''',
            "tb.Col");
  });

  test('''     sq.COLL 
            F (  tb.lel, tb.COLL
                 F tb 
              ) sq -> tb.COLL''', () {
    expectOriginColumn('''
            SELECT sq.Col
            FROM (SELECT tb.Col, tb.LOL
                  FROM tb
                  ) sq
            ''',
            "tb.Col");
  });


  test('''2 nested subqueries, each 2 columns''', () {
    expectOriginColumn('''
            SELECT sq.Col
            FROM (SELECT sq2.LOL, sq2.Col
                  FROM (SELECT tb.Col, tb.LOL
                          FROM tb
                       )sq2
                 ) sq
            ''',
        "tb.Col");
  });


  test('''2 nested subqueries, each 2 columns''', () {
    expectOriginColumn('''
            SELECT sq.Col
            FROM (SELECT sq2.LOL, sq2.Col
                  FROM (SELECT tb.Col, tb.LOL
                          FROM tb
                       )sq2
                 ) sq
            ''',
        "tb.Col");
  });


  test('''2 nested subqueries, each 2 columns''', () {
    expectOriginColumn('''
            SELECT sq.Col
            FROM (SELECT sq2.LOL, sq2.Col
                  FROM (SELECT tb.Col, tb.LOL
                          FROM tb
                       )sq2
                 ) sq
            ''',
        "tb.Col");
  });



  test('''Subqueries 1-2j''', () {
    expectOriginColumn('''
            SELECT sq.Collz
            FROM (SELECT sq2.LOL, sq2.Col Collz
                  FROM (SELECT tb.Col, tb.LOL
                        FROM   tb
                       )sq2
                       JOIN
                       (SELECT tb.Col, tb.LOL
                          FROM tb
                       )sq3
                       ON sq2.Col = sq3.LOL
                 ) sq
            ''',
        "tb.Col");
  });








}

expectFirstColName(String sqlStr, String colName) {
  expect(slotFindFirstColName(sqlStr), colName);
}

String slotFindFirstColName(String sqlStr) {
  return project.runFunctionUnwrap("findFirstColName", [parseSQL(sqlStr)]);
}

expectOriginColumn(String str, String colName) {
  print(str);
  expect(slotOriginColumn(str), colName);
}

String slotOriginColumn(String str) {
  return project.runFunctionUnwrap("originColumns", [parseSQL(str)]);
}