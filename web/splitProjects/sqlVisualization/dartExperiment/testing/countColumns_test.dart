import '../projectStuff/projectStuff.dart';
import 'package:test/test.dart';

main() {
  initProject();
  test("1_FROM_literal", () {
    expectCountColumns(
        "SELECT hi FROM ding",
        1
    );
  });

  test("1_FROM_join_literals", () {
    expectCountColumns(
        "SELECT hi FROM ding NATURAL JOIN bing",
        1
    );
  });

  test("2_FROM_join_literals", () {
    expectCountColumns(
        "SELECT hi, bye FROM ding NATURAL JOIN bing",
        2
    );
  });

  test("*(2)_FROM_join_subqueries", () {
    expectCountColumns(
        '''
        SELECT * 
        FROM        (SELECT hi   
                     FROM ding) mickey
               JOIN (SELECT bye
                      FROM bing) mouse
               ON mickey.hi = mouse.bye''',
        2
    );
  });


  test("*(3)_FROM_join_subqueries", () {
      expectCountColumns(
          '''
          SELECT * 
          FROM        (SELECT hi, my
                       FROM ding) mickey
                 JOIN (SELECT bye
                        FROM bing) mouse
                 ON mickey.hi = mouse.bye''',
          3
      );
    });





}

expectCountColumns(String str, int number) {
  expect(slotCountColumns(str), number);
}

int slotCountColumns(String str){
  return project.runFunctionUnwrap("countColumns", [parseSQL(str)]);
}