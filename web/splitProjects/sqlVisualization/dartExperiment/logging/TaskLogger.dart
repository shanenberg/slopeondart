import '../projectStuff/projectStuff.dart';
import '../tasks/Task.dart';
import 'TaskAction.dart';
import 'dart:html';

enum TaskLoggerState {
  NOT_STARTED,
  STARTED,
  FINISHED
}

class TaskLogger {
  DateTime startTime;
  DateTime endTime;
  TaskLoggerState state;
  List<TaskAction> actions = [];

  Task task;
  TaskLogger(this.task) {
    this.state = TaskLoggerState.NOT_STARTED;
  }


  logRightAnswer(String answer) {
    actions.add(new TaskActionRightAnswer(answer, new DateTime.now()));
  }

  logRightAnswerTime(String answer, DateTime time) {
    actions.add(new TaskActionRightAnswer(answer, time));
  }


  logTextInput(String currentText) {
    actions.add(new TextInputAction(currentText, new DateTime.now()));
  }


  logWrongAnswer(String answer) {
    actions.add(new TaskActionWrongAnswer(answer, new DateTime.now()));
  }

  start() {
    this.startTime = new DateTime.now();
    this.state = TaskLoggerState.STARTED;
  }


  stop() {
    this.endTime = new DateTime.now();
    this.state = TaskLoggerState.FINISHED;
  }

  stopAnswer(String answer) {
    this.endTime = new DateTime.now();
    this.logRightAnswerTime(answer, endTime);
    this.state = TaskLoggerState.FINISHED;
  }


  Duration taskDuration() {
    return endTime
        .difference(startTime);
  }

  printLog() {
    print("${task.runtimeType}");
    String sqlStatement = project.runFunctionUnwrap("constructStringFromParseTree", [task.node]);
    print(sqlStatement);
    try {
      print(endTime
              .difference(startTime)
              .toString());
    } catch(e) {
      print("lol");
    }
    print("");
  }

  void saveLog() {

  }




}

