import '../projectStuff/projectStuff.dart';
import '../randomTasks.dart';
import '../tasks/Task.dart';
import '../tasks/break/Break.dart';
import 'TaskAction.dart';
import 'dart:convert';
import 'dart:html';


Blob createTaskLogBlob(List<Task> taskList, {fullText = true}) {
  List<Map> jsonObjects = [];

  for(Task task in taskList) {
    if(task is Break) {
      continue;
    }

    jsonObjects.add(
      createLogMapFromTask(task, fullText: fullText)
    );
  }
  return new Blob([JSON.encode(jsonObjects)], "application/json");


}



createLogMapFromTask(Task task, {fullText = true})
{
  Map ret = {};
  project.runFunctionUnwrap("setNodeMaps", [task.node]);
  project.runFunctionUnwrap("setFromExtras", [task.node]);
  if (fullText) {
    ret["type"] = task.runtimeType.toString();
    ret["displayType"] = task.displayMethod.toString();
    ret["statement"] = task.prettyPrintString();
    ret["time"] = task.logger.taskDuration().toString();
  }



  int length = project.runFunctionUnwrap("amountOfStuff", [task.node]);
  int depth = project.runFunctionUnwrap("pathDepth", [task.node]);
  int renameCount = project.runFunctionUnwrap("pathRenameCount", [task.node]);

  ret["displayTypeNum"] = task.displayMethod == DisplayMethod.VISUAL ? 1 : 0;
  ret["length"] = length;
  ret["depth"] = depth;
  ret["renameCount"] = renameCount;
  ret["inSeconds"] = task.logger.taskDuration().inSeconds;

  TextInputAction action = task.logger.actions.firstWhere((TaskAction ac) => (ac is TextInputAction));
  ret["firstTextInput"] = action.time.difference(task.logger.startTime).inSeconds;

  return ret;
}
