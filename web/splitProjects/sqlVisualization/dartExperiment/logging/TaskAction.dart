abstract class TaskAction {
  DateTime time;
  String answer;
  TaskAction(this.answer, this.time);





}

class TaskActionWrongAnswer extends TaskAction{
  TaskActionWrongAnswer(String answer, DateTime time) : super(answer, time);
}

class TaskActionRightAnswer extends TaskAction{
  TaskActionRightAnswer(String answer, DateTime time) : super(answer, time);
}

class TextInputAction extends TaskAction {
  TextInputAction(String answer, DateTime time) : super(answer, time);

}