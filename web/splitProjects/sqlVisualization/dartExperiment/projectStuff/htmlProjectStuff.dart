import '../../../../html/examples/SLOTProjectConstructor.dart';
import '../../../../nativeHtmlStuff/SQLPlayground/overhaul2/overhaul2PreProcess.dart';
import '../../webResult/sqlVisualization.slp.dart';
import '../../../../../lib/slot/runtime/objects/SLOTSLOPETreeNode.dart';
import 'dart:html';
import 'projectStuff.dart';







initHTMLProject() {

  project =
      new SLOTProjectConstructor()
          .constructHTMLProject(new sqlVisualization());

  project.initProject();
  project.runFunctionUnwrap("initNodes");
  initValidator();
}



doSQLVisualizationElement(SLOTSLOPETreeNode node) {
  String stringRet = doSQLVisualization(node);
  return new Element.html(stringRet, validator: validator);
}


doPrettyPrintElement(SLOTSLOPETreeNode node) {
  String prettyPrintString = doPrettyPrint(node);

  return new Element.html(
    '''
      <code>
        <pre>$prettyPrintString</pre>     
      </code>
      
    ''', validator: validator
  );



}