import '../../../../../lib/slot/runtime/objects/SLOTSLOPETreeNode.dart';
import '../../../../html/examples/SLOTHTMLToConsoleConverter.dart';
import '../../webResult/sqlVisualization.slp.dart';
import '../../../../../lib/slot/runtime/SLOTProject.dart';
import '../../../../../lib/slot/runtime/builtInFunction/BuiltInFunction.dart';
import '../../../../../lib/slot/runtime/objects/SLOTString.dart';
import '../../../../../lib/lib/grammarparser/ScannerRuleObject.dart';

SLOTProject project;


initProject() {
  project = new SLOTHTMLToConsoleConverter().constructProject(new sqlVisualization());
  project.initProject();
  project.runFunctionUnwrap("initNodes");
}






SLOTSLOPETreeNode parseSQL(String sqlString) {
  return project.slopeGrammarObject
      .parseString(sqlString).toSLOTTreeNode();
}

String doPrettyPrint(SLOTSLOPETreeNode node) {
  return project.runFunctionUnwrap("createPrettyPrintedString", [node]);


}



String doSQLVisualization(SLOTSLOPETreeNode node) {
  return project.runFunctionUnwrap("createSQLVisualization", [node]);
}
