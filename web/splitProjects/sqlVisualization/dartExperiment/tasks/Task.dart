import '../logging/TaskLogger.dart';
import '../projectStuff/htmlProjectStuff.dart';
import '../projectStuff/projectStuff.dart';
import 'dart:html';
import '../../../../../lib/slot/runtime/objects/SLOTSLOPETreeNode.dart';
enum DisplayMethod {
  VISUAL,
  TEXTUAL
}

enum InputMethod {
  TEXTFIELD,
  TABLE
}


abstract class Task {

  SLOTSLOPETreeNode node;
  DisplayMethod displayMethod;

  TaskLogger logger;

  Task(this.node, this.displayMethod) {
    this.logger = new TaskLogger(this);
  }

  var answer;
  Element textual;
  Element visual;

  initTask() {
    answer = preCalculateAnswer();
    _initTextual();
    _initVisual();
  }

  String get slotFunction;
  String get description;
  InputMethod get inputMethod;

  List<List<String>> get tableInfo => null;

  preCalculateAnswer() {
    return project.runFunctionUnwrap(slotFunction, [node]);
  }


  void startTask() {
    logger.start();
  }

  void stopTask() {
    logger.stop();
  }

  bool inputAnswer(answer) {
    if(answer == this.answer) {
      logger.logRightAnswer(answer);
      print(logger.actions.last.runtimeType);
      return true;
    }
    else {
      logger.logWrongAnswer(answer);
      print(logger.actions.last.runtimeType);
      return false;
    }
  }

  prettyPrintString() {
    return doPrettyPrint(node);
  }

  prettyPrintTextual() {
    print(doPrettyPrint(node));
  }

  _initTextual() {
    textual = prettyPrintElement();
  }


  prettyPrintElement() {
    return doPrettyPrintElement(node);
  }


  _initVisual() {
    visual = sqlVisualizationElement();
  }

  sqlVisualizationElement() {
    return doSQLVisualizationElement(node);
  }

  printLog() {
    logger.printLog();
  }

  saveLog() {
    logger.saveLog();
  }

}