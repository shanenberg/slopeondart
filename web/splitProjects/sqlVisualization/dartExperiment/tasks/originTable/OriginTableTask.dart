import '../Task.dart';
import '../../../../../../lib/slot/runtime/objects/SLOTSLOPETreeNode.dart';

class OriginTableTask  extends Task {

  OriginTableTask(SLOTSLOPETreeNode node, DisplayMethod displayMethod) : super(node, displayMethod);

  String get slotFunction => "originTable";
  String get description => "Gib die Ursprungs-Tabelle an.";


  @override
  preCalculateAnswer()
  {
    return "${super.preCalculateAnswer()}";
  }

  /*@override
  bool inputAnswer(answer)
  {
    return answer == this.answer;
  }
*/

  @override
  InputMethod get inputMethod => InputMethod.TEXTFIELD;



}



