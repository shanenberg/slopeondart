import '../Task.dart';
import '../../../../../../lib/slot/runtime/objects/SLOTSLOPETreeNode.dart';


class NameColumnsTask  extends Task {

  NameColumnsTask(SLOTSLOPETreeNode node, DisplayMethod displayMethod) : super(node, displayMethod);

  String get slotFunction => "nameColumns";
  String get description => "Nenne die Spalten, die ausgegeben werden.";


  @override
  bool inputAnswer(String answer)
  {
    print(answer.split(new RegExp(r' +')));
    return false;
  }


  @override
  InputMethod get inputMethod => null;
}



