import '../../../../../nativeHtmlStuff/SQLPlayground/overhaul2/overhaul2PreProcess.dart';
import '../Task.dart';
import 'dart:html';
import 'mockConsts.dart';

class MockTask extends Task {
  MockTask(DisplayMethod displayMethod) : super(null, displayMethod);

  String _desc = "Mach ma dies und das";

  @override
  String get description => _desc;

  set description(desc) => _desc = desc;

  @override
  String get slotFunction => null;

  initTask() {
    textual = new Element.html(mockTextual, validator: validator);
    visual = new Element.html(mockVisual, validator: validator);
  }

  @override
  bool inputAnswer(answer) => answer == "1";

  @override
  prettyPrintTextual()
  {
    return mockPrettyText;
  }


  @override
  InputMethod get inputMethod => InputMethod.TEXTFIELD;
}