String mockVisual = '''<div class="SQL" id="1"><div class="hidePins" id="2"><div class="sfwBlock" id="3"><div class="sfwBlockRow" id="4"><div class="selectClause arrowable" id="5"><div class="selectedAttributes" id="6" data-trio-arrow="" style="margin-bottom: 1em;"><div class="selectColumn" id="7"><div class="column" id="8" data-origin-table-name="ding" data-name="hi"><div class="pin top" id="9"></div> <div class="pin bottom" id="10" data-trio-arrow-from="34"></div> <div class="originalName" id="11">hi</div> </div> </div> </div> <div class="collectedNames" id="12"><div class="copiedSelectPart" data-copy-horizontalposition-from="17" data-copy-horizontalposition-center="" id="32" style="left: 0.410256em;"> 
                  <div class="copiedSelectedAttribute" data-origin-table-name="ding" data-name="*" id="33">
                    <div class="pin top" id="34"></div>
                    <div class="pin bottom" data-vertical-dashed-line-to="17" id="35">
                    </div>
                    <div class="copyBracket left" id="36">(</div>
                    ...
                    <div class="copyBracket right" id="37">)</div>
                  </div>
              </div></div> </div> <div class="subqueryCellPlaceholder" id="13"></div> </div> <div class="sfwBlockRow" id="14"><div class="fromClause" id="15"><div class="tableLiteral" id="16" data-name="ding"><div class="pin top" id="17"></div> <div class="pin left" id="18"></div> <div class="pin right" id="19"></div> <div class="pin bottom" id="20"></div> ding</div> </div> <div class="subqueryCellPlaceholder" id="21"></div> </div> </div> </div> <div class="verticalLine dashed" style="left: 1.69231em; top: 4.20513em; height: 1.79487em; border-color: black;"></div><div class="verticalLine straight" style="left: 0.769231em; top: 2em; height: 0.615385em; background-color: rgb(44, 160, 44);"><div class="topArrowHead left"></div><div class="topArrowHead right"></div></div><div class="horizontalLine straight" style="left: 0.923077em; top: 2.51282em; width: 0.512821em; background-color: rgb(44, 160, 44);"></div><div class="verticalLine straight" style="left: 1.4359em; top: 2.51282em; height: 0.615385em; background-color: rgb(44, 160, 44);"></div></div>''';
String mockTextual = '''<code>
        <pre>SELECT ding. hi
FROM ding</pre>     
      </code>''';

String mockPrettyText = '''SELECT ding. hi
FROM ding''';