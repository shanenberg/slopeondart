import '../Task.dart';
import '../../../../../../lib/slot/runtime/objects/SLOTSLOPETreeNode.dart';

class CountColumnsTask  extends Task {

  CountColumnsTask(SLOTSLOPETreeNode node, DisplayMethod displayMethod) : super(node, displayMethod);

  String get slotFunction => "countColumns";
  String get description => "Gib die Anzahl der Spalten an, die ausgegeben werden.";


  @override
  preCalculateAnswer()
  {
    return "${super.preCalculateAnswer()}";
  }

  @override
  bool inputAnswer(answer)
  {
    return answer == this.answer;
  }


  @override
  InputMethod get inputMethod => InputMethod.TEXTFIELD;
}



