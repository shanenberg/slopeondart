import '../Task.dart';
import '../../../../../../lib/slot/runtime/objects/SLOTSLOPETreeNode.dart';

class OriginColumnTask  extends Task {

  OriginColumnTask(SLOTSLOPETreeNode node, DisplayMethod displayMethod) : super(node, displayMethod);

  String get slotFunction => "originColumns";
  String get description => curDescr;

  String curDescr = "Gib die Ursprungs-Spalte an.";

  set description(String descr) => curDescr = descr;
  @override
  preCalculateAnswer()
  {
    return "${super.preCalculateAnswer()}";
  }

  /*@override
  bool inputAnswer(answer)
  {
    return answer == this.answer;
  }
*/

  @override
  InputMethod get inputMethod => InputMethod.TEXTFIELD;



}



