import '../../../../../../lib/slot/runtime/objects/SLOTSLOPETreeNode.dart';
import '../../../../../nativeHtmlStuff/SQLPlayground/overhaul2/overhaul2PreProcess.dart';
import '../Task.dart';
import 'dart:html';

class Break extends Task {
  Break() : super(null, DisplayMethod.TEXTUAL);

  // TODO: implement description
  @override
  String get description => _curDescr;


  set description(String descr) => _curDescr = descr;
  String _curDescr = "Bitte bearbeite den TLX-Fragebogen.";


  // TODO: implement inputMethod
  @override
  InputMethod get inputMethod => InputMethod.TEXTFIELD;

  // TODO: implement slotFunction
  @override
  String get slotFunction => null;

  @override
  sqlVisualizationElement()
  {
    return new Element.div();
  }

  @override
  preCalculateAnswer()
  {
    return true;
  }

  @override
  prettyPrintTextual()
  {
    return "BREAK";
  }

  @override
  prettyPrintElement()
  {
    return new Element.html(
        '''
      <code>
        <pre>$prettyPrintString</pre>     
      </code>
      
    ''', validator: validator
    );
  }

  @override
  prettyPrintString()
  {
    return "BREAK";
  }

  @override
  bool inputAnswer(answer)
  {
    return true;
  }




}




class NormalBreak extends Break {
  String _curDescr = "Take a Break";
}