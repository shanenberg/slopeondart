import '../Task.dart';
import '../../../../../../lib/slot/runtime/objects/SLOTSLOPETreeNode.dart';

class AnswerOneTask extends Task {

  AnswerOneTask(SLOTSLOPETreeNode node, DisplayMethod displayMethod) : super(node, displayMethod);

  String get slotFunction => null;
  String get description => "Gib für jede Ausgabespalte die Ursprungsspalte an.";


  @override
  preCalculateAnswer()
  {
    return "1";
  }



  @override
  bool inputAnswer(answer)
  {
    return false;
  }


  @override
  InputMethod get inputMethod => null;

  List<List<String>> get tableInfo => [["Hi ", "Bye"],
                                       ["Hey", "Ho" ]
                                      ];




}



