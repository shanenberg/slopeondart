
var RTRCTasks = [
  '''
 SELECT addition.wound
FROM          (SELECT versus.envelope nod,
                      the.tiny,
                      the.presidency design
               FROM          the
                  CROSS JOIN (SELECT prominent.pitch democracy,
                                     prominent.dependent envelope
                              FROM prominent
                             ) versus
                  CROSS JOIN (SELECT predict.regulator,
                                     predict.overwhelm
                              FROM (SELECT illness.paint overwhelm,
                                           illness.regulator
                                    FROM illness
                                   ) predict
                             ) tighten
              ) PC
   CROSS JOIN addition
   CROSS JOIN urban
   CROSS JOIN cousin

''',
'''
SELECT offer.hide action
FROM          copy
   CROSS JOIN (SELECT filter.handful cook
               FROM filter
              ) dealer
   CROSS JOIN (SELECT resist.behind,
                      resist.cart vote
               FROM (SELECT pure.behind,
                            pure.grave cart
                     FROM pure
                    ) resist
              ) reader
   CROSS JOIN grave
   CROSS JOIN European
   CROSS JOIN (SELECT champion.statistics disc,
                      champion.hell idea,
                      champion.thread hide,
                      champion.ultimately gaze
               FROM champion
              ) offer ;

''',
'''
  SELECT suck.cake
FROM          (SELECT specifically.dispute,
                      specifically.branch mission,
                      specifically.recent transfer,
                      specifically.bread
               FROM specifically
              ) root
   CROSS JOIN (SELECT content.label ghost
               FROM content
              ) alternative
   CROSS JOIN analyze
   CROSS JOIN (SELECT cloud.dear
               FROM (SELECT ceiling.morning recommend,
                            ceiling.before,
                            ceiling.dear
                     FROM ceiling
                    ) cloud
              ) fine
   CROSS JOIN ask
   CROSS JOIN suck ;


''',
'''
  SELECT mouse.base
FROM          (SELECT interpretation.homework,
                      interpretation.bless,
                      interpretation.maintain,
                      interpretation.pollution objection,
                      sanction.description base,
                      interpretation.pistol
               FROM          sanction
                  CROSS JOIN (SELECT suite.please homework,
                                     institutional.survivor,
                                     institutional.key pistol,
                                     suite.rural maintain,
                                     register.bless,
                                     suite.pollution
                              FROM          suite
                                 CROSS JOIN register
                                 CROSS JOIN institutional
                             ) interpretation
              ) mouse
   CROSS JOIN clip




''',
'''

SELECT throughout.argument paint
FROM          practice
   CROSS JOIN (SELECT room.automatically hunter,
                      pursuit.mean,
                      bother.stance,
                      room.intact,
                      pursuit.so how,
                      room.concrete proper,
                      room.faint into,
                      pursuit.aside argument
               FROM          elementary
                  CROSS JOIN (SELECT smile.refrigerator stance
                              FROM smile
                             ) bother
                  CROSS JOIN (SELECT might.text partly,
                                     might.so,
                                     essence.adjust mean,
                                     might.administrative conceive,
                                     essence.discrimination aside,
                                     might.objection length
                              FROM          (SELECT smile.administrative,
                                                    smile.medical wealthy,
                                                    smile.force so,
                                                    smile.selection,
                                                    smile.protect text,
                                                    smile.economy objection
                                             FROM smile
                                            ) might
                                 CROSS JOIN essence
                             ) pursuit
                  CROSS JOIN (SELECT wooden.jump racism
                              FROM (SELECT subsidy.jump
                                    FROM (SELECT since.unable jump
                                          FROM (SELECT citizen.biology,
                                                       citizen.unable,
                                                       citizen.define satisfaction
                                                FROM (SELECT knee.at dip,
                                                             knee.unable,
                                                             knee.exhaust colonial,
                                                             knee.define,
                                                             knee.nation,
                                                             knee.fantasy biology
                                                      FROM knee
                                                     ) citizen
                                               ) since
                                         ) subsidy
                                   ) wooden
                             ) race
                  CROSS JOIN regarding
                  CROSS JOIN (SELECT radio.conscience growth
                              FROM (SELECT continue.stand conscience
                                    FROM (SELECT devil.advanced,
                                                 devil.currently stand,
                                                 devil.till surprise
                                          FROM devil
                                         ) continue
                                   ) radio
                             ) possible
                  CROSS JOIN (SELECT more.automatically,
                                     more.intact,
                                     more.demand magic,
                                     more.faint,
                                     more.good,
                                     more.fly,
                                     more.concrete,
                                     more.feminist
                              FROM more
                             ) room
                  CROSS JOIN frame
              ) throughout ;

''',
'''
SELECT construction.dramatically
FROM          shed
   CROSS JOIN (SELECT companion.hate,
                      test.dynamic driving,
                      moral.speculation condemn
               FROM          (SELECT run.cotton screw,
                                     run.experience,
                                     run.hate,
                                     run.pair,
                                     identify.prime gold
                              FROM          (SELECT stream.prime
                                             FROM stream
                                            ) identify
                                 CROSS JOIN (SELECT exploration.homeland experience,
                                                    exploration.official cotton,
                                                    exploration.hate,
                                                    exploration.pair
                                             FROM exploration
                                            ) run
                             ) companion
                  CROSS JOIN test
                  CROSS JOIN moral
              ) their
   CROSS JOIN exclusive
   CROSS JOIN (SELECT routine.outline terror,
                      routine.sorry transport,
                      routine.exclusive still
               FROM (SELECT language.sorry,
                            language.outline,
                            language.exclusive
                     FROM (SELECT angle.exclusive,
                                  angle.mechanism outline,
                                  angle.laundry sorry
                           FROM angle
                          ) language
                    ) routine
              ) preserve
   CROSS JOIN (SELECT knowledge.work,
                      knowledge.couch,
                      knowledge.promise letter,
                      knowledge.crucial physically,
                      knowledge.shut respect
               FROM knowledge
              ) fantastic
   CROSS JOIN (SELECT diversity.collector,
                      diversity.seller etc,
                      diversity.naked definition,
                      diversity.dramatically
               FROM diversity
              ) construction
   CROSS JOIN (SELECT price.ideology stem
               FROM (SELECT relation.ideology
                     FROM (SELECT French.elect ideology
                           FROM French
                          ) relation
                    ) price
              ) share
   CROSS JOIN (SELECT entitle.breathe finish,
                      entitle.airline tape
               FROM (SELECT phrase.shrug breathe,
                            phrase.refrigerator airline
                     FROM (SELECT term.shrug,
                                  term.heat refrigerator
                           FROM (SELECT debate.country heat,
                                        debate.shrug
                                 FROM debate
                                ) term
                          ) phrase
                    ) entitle
              ) enhance ;



''',
'''
SELECT sustain.downtown
FROM          give
   CROSS JOIN (SELECT excellent.sink encounter,
                      excellent.handle,
                      excellent.official,
                      excellent.logic
               FROM excellent
              ) draft
   CROSS JOIN (SELECT curtain.colorful,
                      curtain.cheese,
                      curtain.useful,
                      curtain.combined lot,
                      curtain.conduct,
                      curtain.supervisor array
               FROM curtain
              ) French
   CROSS JOIN (SELECT violent.fever paint,
                      violent.chase decade,
                      violent.destination independence
               FROM violent
              ) cabin
   CROSS JOIN quiet
   CROSS JOIN (SELECT theory.ease,
                      theory.element,
                      theory.slight,
                      theory.concerning
               FROM theory
              ) slice
   CROSS JOIN (SELECT gear.engage king,
                      gear.intention harm,
                      gear.military,
                      gear.bond radio,
                      gear.discrimination confidence,
                      gear.heavily,
                      gear.rock individual
               FROM gear
              ) representative
   CROSS JOIN catch
   CROSS JOIN kiss
   CROSS JOIN (SELECT damage.subsequent,
                      damage.water downtown,
                      damage.curious continue,
                      damage.relation,
                      damage.tide,
                      damage.stare reverse,
                      damage.okay German,
                      damage.break press
               FROM damage
              ) sustain
   CROSS JOIN (SELECT laugh.uncle,
                      laugh.program lawmaker,
                      laugh.goat figure,
                      laugh.course
               FROM (SELECT opportunity.watch uncle,
                            opportunity.goat,
                            opportunity.blue program,
                            opportunity.territory studio,
                            opportunity.lawyer course
                     FROM (SELECT international.drug watch,
                                  international.blue,
                                  international.color lawyer,
                                  international.launch wave,
                                  international.territory,
                                  international.willing,
                                  international.swallow,
                                  international.direct goat
                           FROM international
                          ) opportunity
                    ) laugh
              ) evolution ;



''',
'''

SELECT package.insert
FROM          (SELECT trail.epidemic corporation,
                      lip.evolve,
                      of.initiate interview,
                      of.along oil,
                      lip.wonderful quote,
                      component.burn,
                      lip.primary strict,
                      cease.irony
               FROM          (SELECT promising.poetry,
                                     promising.along,
                                     promising.obstacle initiate,
                                     promising.striking
                              FROM promising
                             ) of
                  CROSS JOIN trail
                  CROSS JOIN (SELECT risk.infrastructure dry,
                                     risk.vitamin
                              FROM (SELECT smoke.collection lend,
                                           smoke.fatigue infrastructure,
                                           smoke.vitamin
                                    FROM (SELECT probably.illness fatigue,
                                                 probably.vitamin,
                                                 probably.team collection
                                          FROM probably
                                         ) smoke
                                   ) risk
                             ) fashion
                  CROSS JOIN component
                  CROSS JOIN (SELECT slide.toll,
                                     slide.evolve,
                                     slide.therapist primary,
                                     slide.wonderful
                              FROM slide
                             ) lip
                  CROSS JOIN cease
              ) test
   CROSS JOIN (SELECT others.spit execute,
                      others.sin require,
                      others.hurt violation,
                      others.sake,
                      others.entity immediately,
                      others.other insert,
                      others.speak,
                      others.ask steer
               FROM others
              ) package
   CROSS JOIN (SELECT jurisdiction.permit,
                      jurisdiction.describe descend,
                      jurisdiction.including,
                      jurisdiction.educational impress
               FROM jurisdiction
              ) hill
   CROSS JOIN ask
   CROSS JOIN elegant
   CROSS JOIN (SELECT previous.add religious,
                      previous.green power,
                      previous.occasionally opposite,
                      previous.porch,
                      previous.around quote,
                      previous.businessman
               FROM (SELECT result.plant occasionally,
                            result.prime green,
                            result.add,
                            result.around,
                            result.businessman,
                            result.porch
                     FROM result
                    ) previous
              ) another ;


''',
'''
SELECT confession.drug
FROM          (SELECT taxpayer.appropriate,
                      taxpayer.progressive surprise,
                      quarter.fate steep,
                      disappear.red construction
               FROM          (SELECT instant.red
                              FROM          instant
                                 CROSS JOIN friendly
                             ) disappear
                  CROSS JOIN discrimination
                  CROSS JOIN (SELECT coach.theater fate
                              FROM          elect
                                 CROSS JOIN (SELECT conduct.immediately,
                                                    conduct.destination answer
                                             FROM          (SELECT Jew.random immediately,
                                                                   Jew.ours gate,
                                                                   Jew.firmly,
                                                                   Jew.pocket,
                                                                   Jew.given,
                                                                   Jew.destination
                                                            FROM Jew
                                                           ) conduct
                                                CROSS JOIN simple
                                            ) fit
                                 CROSS JOIN (SELECT reputation.cut,
                                                    elementary.bulk theater,
                                                    reputation.refuse lawmaker
                                             FROM          reputation
                                                CROSS JOIN elementary
                                            ) coach
                             ) quarter
                  CROSS JOIN (SELECT logic.bold,
                                     logic.finally top,
                                     logic.vote born
                              FROM logic
                             ) temporary
                  CROSS JOIN (SELECT relevant.rally educate,
                                     relevant.taxpayer
                              FROM relevant
                             ) somewhat
                  CROSS JOIN (SELECT formerly.visual breathe,
                                     formerly.need fashion
                              FROM formerly
                             ) screening
                  CROSS JOIN illegal
                  CROSS JOIN (SELECT rush.soccer second,
                                     rush.inform,
                                     rush.kiss nurse,
                                     rush.progressive,
                                     rush.initial appropriate,
                                     rush.steer twist,
                                     rush.tradition,
                                     rush.notice
                              FROM rush
                             ) taxpayer
                  CROSS JOIN uniform
                  CROSS JOIN butt
                  CROSS JOIN (SELECT courage.car,
                                     courage.least,
                                     courage.smoke
                              FROM (SELECT bonus.car,
                                           bonus.average,
                                           bonus.least,
                                           bonus.institutional smoke,
                                           bonus.bush beast
                                    FROM bonus
                                   ) courage
                             ) therefore
                  CROSS JOIN (SELECT handful.warrior
                              FROM (SELECT boss.golden warrior,
                                           boss.radical
                                    FROM (SELECT imagine.fuel radical,
                                                 imagine.there,
                                                 imagine.diagnosis golden,
                                                 imagine.responsibility youngster,
                                                 imagine.drill,
                                                 imagine.usual dedicate,
                                                 imagine.bedroom aspect
                                          FROM (SELECT guard.diagnosis,
                                                       guard.bedroom,
                                                       guard.fuel,
                                                       guard.extra there,
                                                       guard.drill,
                                                       guard.rock responsibility,
                                                       guard.wrong usual
                                                FROM guard
                                               ) imagine
                                         ) boss
                                   ) handful
                             ) professional
              ) climate
   CROSS JOIN (SELECT hook.lens,
                      hook.commit conservation,
                      hook.worry
               FROM (SELECT flag.commit,
                            flag.dog worry,
                            flag.clerk multiple,
                            flag.mean lens
                     FROM (SELECT definition.person mean,
                                  definition.life dog,
                                  definition.square,
                                  definition.rub,
                                  definition.name clerk,
                                  definition.round commit
                           FROM definition
                          ) flag
                    ) hook
              ) sexy
   CROSS JOIN (SELECT industrial.opinion,
                      industrial.electric chronic,
                      industrial.dish drug,
                      industrial.discover hello
               FROM (SELECT bean.electric,
                            bean.reliability dish,
                            bean.joke opinion,
                            bean.age apple,
                            bean.reasonable discover
                     FROM bean
                    ) industrial
              ) confession

''',
'''
SELECT naked.hot
FROM          board
   CROSS JOIN (SELECT tube.account soul,
                      tube.pound
               FROM          (SELECT inherit.pound,
                                     inherit.locate account
                              FROM (SELECT kitchen.bucket sudden,
                                           building.Indian,
                                           kitchen.pound,
                                           kitchen.depression locate
                                    FROM          building
                                       CROSS JOIN kitchen
                                   ) inherit
                             ) tube
                  CROSS JOIN (SELECT extra.guy,
                                     extra.regulation offense,
                                     extra.sexuality,
                                     extra.bird homeland,
                                     extra.sweet,
                                     extra.equally
                              FROM (SELECT candy.sexuality,
                                           candy.grace guy,
                                           candy.fault regulation,
                                           candy.tone Bible,
                                           candy.sweet,
                                           candy.bird,
                                           candy.equally,
                                           candy.drug trend
                                    FROM candy
                                   ) extra
                             ) butt
                  CROSS JOIN roughly
              ) pleasure
   CROSS JOIN (SELECT strange.dynamic particle,
                      strange.originally narrow,
                      strange.missile tear,
                      strange.trait photo,
                      strange.English advance
               FROM (SELECT letter.missile,
                            letter.English,
                            letter.asset,
                            letter.dynamic,
                            letter.trait,
                            letter.ok originally
                     FROM letter
                    ) strange
              ) hostile
   CROSS JOIN poetry
   CROSS JOIN each
   CROSS JOIN shall
   CROSS JOIN (SELECT skin.husband,
                      skin.construction,
                      skin.child guitar,
                      skin.arena confrontation,
                      skin.alliance,
                      skin.gay different,
                      skin.hot,
                      skin.complex secret
               FROM skin
              ) naked
   CROSS JOIN (SELECT transmission.keep,
                      transmission.wing,
                      transmission.existence,
                      transmission.charter chemical,
                      transmission.concentration
               FROM (SELECT guilty.concentration,
                            guilty.goal initiative,
                            guilty.briefly existence,
                            guilty.other symptom,
                            guilty.risk charter,
                            guilty.Muslim gay,
                            guilty.wing,
                            guilty.keep
                     FROM guilty
                    ) transmission
              ) no
   CROSS JOIN pursuit
   CROSS JOIN via
   CROSS JOIN (SELECT see.he,
                      see.familiar
               FROM see
              ) independence
   CROSS JOIN (SELECT deliver.practice,
                      deliver.twice protest,
                      deliver.disappear thrive
               FROM (SELECT next.though practice,
                            next.limb Catholic,
                            next.star,
                            next.validity,
                            next.transfer,
                            next.twice,
                            next.scandal disappear
                     FROM next
                    ) deliver
              ) solution
   CROSS JOIN (SELECT self.mere east,
                      self.vocal
               FROM (SELECT loyal.vocal,
                            loyal.mere,
                            loyal.introduce,
                            loyal.aside over,
                            loyal.quality assistant,
                            loyal.river
                     FROM loyal
                    ) self
              ) associated
   CROSS JOIN university
   CROSS JOIN mass
   CROSS JOIN quarterback
   CROSS JOIN (SELECT fat.apparently ritual,
                      fat.watch convinced,
                      fat.age
               FROM fat
              ) trauma
   CROSS JOIN (SELECT therefore.nurse confront,
                      therefore.stiff,
                      therefore.surrounding narrative,
                      therefore.halfway
               FROM therefore
              ) carrier




''',
'''
SELECT spy.brick local
FROM          (SELECT correct.text place,
                      correct.inspection slave,
                      though.brick,
                      correct.look otherwise
               FROM          meet
                  CROSS JOIN (SELECT sacred.anonymous brick
                              FROM (SELECT franchise.anonymous
                                    FROM (SELECT exclude.mountain robot,
                                                 exclude.costly,
                                                 universe.anonymous
                                          FROM          universe
                                             CROSS JOIN (SELECT sound.trace mountain,
                                                                sound.rough stir,
                                                                sound.confrontation singer,
                                                                sound.costly
                                                         FROM sound
                                                        ) exclude
                                         ) franchise
                                   ) sacred
                             ) though
                  CROSS JOIN (SELECT there.text,
                                     there.confession zone,
                                     there.application look,
                                     there.existence,
                                     there.valley inspection,
                                     there.desire office,
                                     there.forehead
                              FROM (SELECT captain.why text,
                                           captain.vital valley,
                                           captain.leadership confession,
                                           captain.next existence,
                                           captain.vertical forehead,
                                           captain.tragic application,
                                           captain.desire
                                    FROM captain
                                   ) there
                             ) correct
                  CROSS JOIN (SELECT yesterday.disorder,
                                     yesterday.obligation winter
                              FROM (SELECT withdraw.defendant disorder,
                                           withdraw.obligation
                                    FROM (SELECT handle.portray obligation,
                                                 handle.defendant
                                          FROM handle
                                         ) withdraw
                                   ) yesterday
                             ) employ
              ) spy
   CROSS JOIN (SELECT reading.absolute closest
               FROM (SELECT resistance.absolute
                     FROM (SELECT proposal.nuclear absolute
                           FROM (SELECT expression.nuclear
                                 FROM (SELECT processor.nuclear
                                       FROM processor
                                      ) expression
                                ) proposal
                          ) resistance
                    ) reading
              ) light
   CROSS JOIN (SELECT background.crush cook
               FROM (SELECT experimental.crush
                     FROM (SELECT around.crush
                           FROM (SELECT trail.traffic crush
                                 FROM (SELECT minimize.face traffic
                                       FROM (SELECT location.face
                                             FROM (SELECT ie.face
                                                   FROM ie
                                                  ) location
                                            ) minimize
                                      ) trail
                                ) around
                          ) experimental
                    ) background
              ) behavior
   CROSS JOIN hardware
   CROSS JOIN nod
   CROSS JOIN (SELECT abandon.confuse rebuild,
                      abandon.sensitive actually,
                      abandon.distract,
                      abandon.ideal day
               FROM abandon
              ) including
   CROSS JOIN hike
   CROSS JOIN (SELECT strange.mask
               FROM strange
              ) freeze
   CROSS JOIN epidemic
   CROSS JOIN contend
   CROSS JOIN ease
   CROSS JOIN drinking
   CROSS JOIN (SELECT actively.guard,
                      actively.blink,
                      actively.exchange,
                      actively.than,
                      actively.initiate wild,
                      actively.intellectual,
                      actively.initiative,
                      actively.wood
               FROM actively
              ) concentration
   CROSS JOIN (SELECT price.recruit strategy,
                      price.used,
                      price.course,
                      price.amid
               FROM price
              ) following
   CROSS JOIN (SELECT magazine.next sweater
               FROM (SELECT coalition.function,
                            coalition.no,
                            coalition.duck next
                     FROM (SELECT servant.Republican author,
                                  servant.simple function,
                                  servant.runner duck,
                                  servant.dot no
                           FROM servant
                          ) coalition
                    ) magazine
              ) reporter
   CROSS JOIN bedroom ;

''',
'''
SELECT shrink.backyard
FROM          melt
   CROSS JOIN (SELECT parade.long,
                      parade.assess,
                      parade.afterward,
                      parade.cute deep,
                      parade.program none,
                      parade.burden nervous
               FROM parade
              ) articulate
   CROSS JOIN (SELECT grain.render intact,
                      directly.shit,
                      grain.present scary,
                      directly.example hand,
                      directly.whoever historic,
                      directly.oppose
               FROM          (SELECT brilliant.present,
                                     feel.sun issue,
                                     feel.peel tear,
                                     feel.innovation render
                              FROM          (SELECT mad.peel,
                                                    mad.sun,
                                                    mad.innovation,
                                                    prevent.pull
                                             FROM          (SELECT cut.peel,
                                                                   cut.environment,
                                                                   cut.robot sun,
                                                                   cut.reply innovation
                                                            FROM cut
                                                           ) mad
                                                CROSS JOIN (SELECT dying.pull
                                                            FROM (SELECT fast.pull
                                                                  FROM (SELECT assault.pull,
                                                                               assault.regional
                                                                        FROM (SELECT honor.re regional,
                                                                                     honor.exotic pull
                                                                              FROM (SELECT kid.artifact re,
                                                                                           kid.serious exotic
                                                                                    FROM kid
                                                                                   ) honor
                                                                             ) assault
                                                                       ) fast
                                                                 ) dying
                                                           ) prevent
                                            ) feel
                                 CROSS JOIN brilliant
                             ) grain
                  CROSS JOIN (SELECT delight.whoever,
                                     delight.oppose,
                                     delight.example,
                                     delight.shit
                              FROM delight
                             ) directly
              ) colleague
   CROSS JOIN duck
   CROSS JOIN outline
   CROSS JOIN (SELECT administrative.value
               FROM administrative
              ) totally
   CROSS JOIN library
   CROSS JOIN free
   CROSS JOIN horrible
   CROSS JOIN (SELECT aim.orange recession,
                      aim.ruin rod,
                      aim.mask sick,
                      aim.need vacuum
               FROM (SELECT veteran.need,
                            veteran.online,
                            veteran.anxious,
                            veteran.mask,
                            veteran.mind orange,
                            veteran.battery ruin
                     FROM veteran
                    ) aim
              ) temple
   CROSS JOIN than
   CROSS JOIN widely
   CROSS JOIN (SELECT attempt.awareness
               FROM (SELECT amid.painting awareness
                     FROM amid
                    ) attempt
              ) bronze
   CROSS JOIN (SELECT psychologist.collective,
                      psychologist.above software
               FROM (SELECT so.above,
                            so.fill,
                            so.during,
                            so.spectrum collective,
                            so.sometimes
                     FROM so
                    ) psychologist
              ) forever
   CROSS JOIN (SELECT shot.importance limit,
                      shot.cousin,
                      shot.both,
                      shot.stake coordinator,
                      shot.model,
                      shot.eager cotton,
                      shot.expected
               FROM shot
              ) remote
   CROSS JOIN (SELECT continued.backyard,
                      continued.surgery agency
               FROM (SELECT offensive.outside backyard,
                            offensive.surgery
                     FROM (SELECT head.outside,
                                  head.presentation,
                                  head.information surgery
                           FROM head
                          ) offensive
                    ) continued
              ) shrink
   CROSS JOIN (SELECT likewise.another camera,
                      likewise.peak,
                      likewise.handsome
               FROM (SELECT again.kid peak,
                            again.another,
                            again.English leave,
                            again.handsome,
                            again.either ash
                     FROM again
                    ) likewise
              ) return
   CROSS JOIN love ;

''',
  '''
 SELECT career.wind
FROM          (SELECT however.insurance law,
                      orientation.citizen,
                      orientation.politics ecological
               FROM          orientation
                  CROSS JOIN (SELECT reminder.fortunately finance,
                                     reminder.classify insurance
                              FROM reminder
                             ) however
                  CROSS JOIN (SELECT inform.regulator,
                                     inform.article
                              FROM (SELECT dog.dip article,
                                           dog.regulator
                                    FROM dog
                                   ) inform
                             ) jungle
              ) rub
   CROSS JOIN career
   CROSS JOIN sharply
   CROSS JOIN appear

''',
'''
SELECT strip.academic sale
FROM          silence
   CROSS JOIN (SELECT criticize.lead three
               FROM criticize
              ) desk
   CROSS JOIN (SELECT generally.beside,
                      generally.chop safety
               FROM (SELECT successfully.beside,
                            successfully.fluid chop
                     FROM successfully
                    ) generally
              ) rub
   CROSS JOIN fluid
   CROSS JOIN African
   CROSS JOIN (SELECT lately.addition sugar,
                      lately.present interrupt,
                      lately.openly academic,
                      lately.eyebrow bullet
               FROM lately
              ) strip ;

''',
'''
  SELECT forth.hand
FROM          (SELECT approach.budget,
                      approach.assure abortion,
                      approach.employment that,
                      approach.cute
               FROM approach
              ) hazard
   CROSS JOIN (SELECT embarrassed.numerous lie
               FROM embarrassed
              ) wolf
   CROSS JOIN overcome
   CROSS JOIN (SELECT greatest.endorse
               FROM (SELECT breakfast.meet domain,
                            breakfast.talented,
                            breakfast.endorse
                     FROM breakfast
                    ) greatest
              ) tip
   CROSS JOIN visible
   CROSS JOIN forth ;


''',
'''
  SELECT ecosystem.verbal
FROM          (SELECT favor.German,
                      favor.cure,
                      favor.dancing,
                      favor.grasp sustain,
                      assess.portfolio verbal,
                      favor.arm
               FROM          assess
                  CROSS JOIN (SELECT hostage.pastor German,
                                     mouth.inherent,
                                     mouth.celebrate arm,
                                     hostage.assign dancing,
                                     effective.cure,
                                     hostage.grasp
                              FROM          hostage
                                 CROSS JOIN effective
                                 CROSS JOIN mouth
                             ) favor
              ) ecosystem
   CROSS JOIN excuse




''',
'''

SELECT rabbit.donate eye
FROM          scent
   CROSS JOIN (SELECT citizenship.Latin something,
                      advance.read,
                      primary.line,
                      citizenship.mostly,
                      advance.top inmate,
                      citizenship.handle molecule,
                      citizenship.appear overall,
                      advance.relief donate
               FROM          dominant
                  CROSS JOIN (SELECT kid.beneath line
                              FROM kid
                             ) primary
                  CROSS JOIN (SELECT buyer.dynamic textbook,
                                     buyer.top,
                                     besides.happily read,
                                     buyer.style convenience,
                                     besides.emotional relief,
                                     buyer.impress toy
                              FROM          (SELECT kid.style,
                                                    kid.particular loud,
                                                    kid.seek top,
                                                    kid.unlike,
                                                    kid.basis dynamic,
                                                    kid.rock impress
                                             FROM kid
                                            ) buyer
                                 CROSS JOIN besides
                             ) advance
                  CROSS JOIN (SELECT pattern.politician laser
                              FROM (SELECT innovative.politician
                                    FROM (SELECT swing.punishment politician
                                          FROM (SELECT snap.ugly,
                                                       snap.punishment,
                                                       snap.sequence influence
                                                FROM (SELECT religion.thoroughly award,
                                                             religion.punishment,
                                                             religion.marketing open,
                                                             religion.sequence,
                                                             religion.have,
                                                             religion.cheer ugly
                                                      FROM religion
                                                     ) snap
                                               ) swing
                                         ) innovative
                                   ) pattern
                             ) alone
                  CROSS JOIN frustrate
                  CROSS JOIN (SELECT lost.normal elderly
                              FROM (SELECT hurricane.suggestion normal
                                    FROM (SELECT overwhelm.theory,
                                                 overwhelm.hug suggestion,
                                                 overwhelm.nasty symptom
                                          FROM overwhelm
                                         ) hurricane
                                   ) lost
                             ) validity
                  CROSS JOIN (SELECT dancer.Latin,
                                     dancer.mostly,
                                     dancer.racism bone,
                                     dancer.appear,
                                     dancer.illegal,
                                     dancer.very,
                                     dancer.handle,
                                     dancer.puzzle
                              FROM dancer
                             ) citizenship
                  CROSS JOIN iron
              ) rabbit ;

''',
'''
SELECT therefore.sin
FROM          vital
   CROSS JOIN (SELECT reference.stay,
                      human.endless admission,
                      furthermore.assistant launch
               FROM          (SELECT shoe.greatest fat,
                                     shoe.organized,
                                     shoe.stay,
                                     shoe.cemetery,
                                     diplomat.carve tradition
                              FROM          (SELECT our.carve
                                             FROM our
                                            ) diplomat
                                 CROSS JOIN (SELECT line.lawmaker organized,
                                                    line.contractor greatest,
                                                    line.stay,
                                                    line.cemetery
                                             FROM line
                                            ) shoe
                             ) reference
                  CROSS JOIN human
                  CROSS JOIN furthermore
              ) explain
   CROSS JOIN lightly
   CROSS JOIN (SELECT attempt.gallery tragedy,
                      attempt.arrival pig,
                      attempt.lightly peace
               FROM (SELECT acquire.arrival,
                            acquire.gallery,
                            acquire.lightly
                     FROM (SELECT central.lightly,
                                  central.obstacle gallery,
                                  central.initiate arrival
                           FROM central
                          ) acquire
                    ) attempt
              ) invention
   CROSS JOIN (SELECT qualify.downtown,
                      qualify.sentiment,
                      qualify.outside final,
                      qualify.object dictate,
                      qualify.conduct pro
               FROM qualify
              ) spoon
   CROSS JOIN (SELECT warrior.price,
                      warrior.jail test,
                      warrior.possibly absolutely,
                      warrior.sin
               FROM warrior
              ) therefore
   CROSS JOIN (SELECT he.instructional pale
               FROM (SELECT better.instructional
                     FROM (SELECT fame.smart instructional
                           FROM fame
                          ) better
                    ) he
              ) dress
   CROSS JOIN (SELECT stove.slavery conversation,
                      stove.frequent heat
               FROM (SELECT drama.ongoing slavery,
                            drama.impossible frequent
                     FROM (SELECT drain.ongoing,
                                  drain.slide impossible
                           FROM (SELECT swell.convert slide,
                                        swell.ongoing
                                 FROM swell
                                ) drain
                          ) drama
                    ) stove
              ) roughly ;



''',
'''
SELECT request.exact
FROM          conversation
   CROSS JOIN (SELECT paint.drift view,
                      paint.terrific,
                      paint.surrounding,
                      paint.operating
               FROM paint
              ) schedule
   CROSS JOIN (SELECT reduction.cocaine,
                      reduction.pass,
                      reduction.ease,
                      reduction.damn thrive,
                      reduction.taste,
                      reduction.during screening
               FROM reduction
              ) scope
   CROSS JOIN (SELECT straighten.insect ankle,
                      straighten.Soviet announcement,
                      straighten.injure observation
               FROM straighten
              ) survey
   CROSS JOIN rush
   CROSS JOIN (SELECT procedure.ally,
                      procedure.crystal,
                      procedure.charm,
                      procedure.fish
               FROM procedure
              ) quote
   CROSS JOIN (SELECT other.root institution,
                      other.ready construct,
                      other.vocal,
                      other.assess cope,
                      other.scandal dark,
                      other.chemical,
                      other.boring reliability
               FROM other
              ) striking
   CROSS JOIN please
   CROSS JOIN might
   CROSS JOIN (SELECT involve.manage,
                      involve.publicly exact,
                      involve.stir prove,
                      involve.candle,
                      involve.ceremony,
                      involve.chocolate nuclear,
                      involve.accounting billion,
                      involve.award excitement
               FROM involve
              ) request
   CROSS JOIN (SELECT psychologist.die,
                      psychologist.cease wilderness,
                      psychologist.political lower,
                      psychologist.rub
               FROM (SELECT rib.spell die,
                            rib.political,
                            rib.personal cease,
                            rib.shine overwhelm,
                            rib.pool rub
                     FROM (SELECT proof.occupation spell,
                                  proof.personal,
                                  proof.sample pool,
                                  proof.husband especially,
                                  proof.shine,
                                  proof.advocate,
                                  proof.mostly,
                                  proof.prayer political
                           FROM proof
                          ) rib
                    ) psychologist
              ) crack ;



''',
'''

SELECT stance.deem
FROM          (SELECT trade.relationship threat,
                      stuff.general,
                      repeat.chicken accuse,
                      repeat.mild fisherman,
                      stuff.hospital only,
                      ocean.football,
                      stuff.intellectual attack,
                      ideal.especially
               FROM          (SELECT blessing.aware,
                                     blessing.mild,
                                     blessing.assembly chicken,
                                     blessing.cruise
                              FROM blessing
                             ) repeat
                  CROSS JOIN trade
                  CROSS JOIN (SELECT restore.trim dense,
                                     restore.liquid
                              FROM (SELECT hard.purchase furthermore,
                                           hard.steal trim,
                                           hard.liquid
                                    FROM (SELECT hang.emerging steal,
                                                 hang.liquid,
                                                 hang.trouble purchase
                                          FROM hang
                                         ) hard
                                   ) restore
                             ) corruption
                  CROSS JOIN ocean
                  CROSS JOIN (SELECT give.friend,
                                     give.general,
                                     give.ceremony intellectual,
                                     give.hospital
                              FROM give
                             ) stuff
                  CROSS JOIN ideal
              ) slice
   CROSS JOIN (SELECT strength.northwest appeal,
                      strength.sweat sing,
                      strength.truck tropical,
                      strength.water,
                      strength.mainly clinic,
                      strength.reasonable deem,
                      strength.wish,
                      strength.cliff jungle
               FROM strength
              ) stance
   CROSS JOIN (SELECT reluctant.observation,
                      reluctant.basic stay,
                      reluctant.political,
                      reluctant.flesh praise
               FROM reluctant
              ) convert
   CROSS JOIN cliff
   CROSS JOIN now
   CROSS JOIN (SELECT inside.tea detect,
                      inside.evil finally,
                      inside.suicide placement,
                      inside.matter,
                      inside.strengthen only,
                      inside.note
               FROM (SELECT politics.drunk suicide,
                            politics.crowd evil,
                            politics.tea,
                            politics.strengthen,
                            politics.note,
                            politics.matter
                     FROM politics
                    ) inside
              ) statistical ;


''',
'''
SELECT shell.transformation
FROM          (SELECT proof.hot,
                      proof.mine reality,
                      series.view involved,
                      listener.devote ring
               FROM          (SELECT taxpayer.devote
                              FROM          taxpayer
                                 CROSS JOIN beside
                             ) listener
                  CROSS JOIN since
                  CROSS JOIN (SELECT final.festival view
                              FROM          gravity
                                 CROSS JOIN (SELECT realize.jazz,
                                                    realize.devastating totally
                                             FROM          (SELECT climb.land jazz,
                                                                   climb.beneath bathroom,
                                                                   climb.instead,
                                                                   climb.hate,
                                                                   climb.motive,
                                                                   climb.devastating
                                                            FROM climb
                                                           ) realize
                                                CROSS JOIN contemporary
                                            ) address
                                 CROSS JOIN (SELECT occasionally.locate,
                                                    luck.rush festival,
                                                    occasionally.escape oppose
                                             FROM          occasionally
                                                CROSS JOIN luck
                                            ) final
                             ) series
                  CROSS JOIN (SELECT uncover.comparison,
                                     uncover.organic swim,
                                     uncover.consumer founder
                              FROM uncover
                             ) norm
                  CROSS JOIN (SELECT face.hence lift,
                                     face.proof
                              FROM face
                             ) substance
                  CROSS JOIN (SELECT capture.bride mill,
                                     capture.mountain earthquake
                              FROM capture
                             ) current
                  CROSS JOIN brown
                  CROSS JOIN (SELECT you.museum switch,
                                     you.absorb,
                                     you.spring go,
                                     you.mine,
                                     you.sensitive hot,
                                     you.tennis appreciate,
                                     you.constitute,
                                     you.pro
                              FROM you
                             ) proof
                  CROSS JOIN display
                  CROSS JOIN accuse
                  CROSS JOIN (SELECT differently.traditionally,
                                     differently.easily,
                                     differently.rifle
                              FROM (SELECT silk.traditionally,
                                           silk.advanced,
                                           silk.easily,
                                           silk.cabinet rifle,
                                           silk.prison below
                                    FROM silk
                                   ) differently
                             ) cap
                  CROSS JOIN (SELECT grip.fine
                              FROM (SELECT personal.boast fine,
                                           personal.while
                                    FROM (SELECT republican.scale while,
                                                 republican.hey,
                                                 republican.clay boast,
                                                 republican.radar these,
                                                 republican.balloon,
                                                 republican.handful attach,
                                                 republican.partly register
                                          FROM (SELECT instant.clay,
                                                       instant.partly,
                                                       instant.scale,
                                                       instant.cruise hey,
                                                       instant.balloon,
                                                       instant.light radar,
                                                       instant.super handful
                                                FROM instant
                                               ) republican
                                         ) personal
                                   ) grip
                             ) soap
              ) together
   CROSS JOIN (SELECT facilitate.loan,
                      facilitate.reading tighten,
                      facilitate.link
               FROM (SELECT slow.reading,
                            slow.slave link,
                            slow.habitat trainer,
                            slow.robot loan
                     FROM (SELECT equivalent.afternoon robot,
                                  equivalent.secure slave,
                                  equivalent.stand,
                                  equivalent.scream,
                                  equivalent.neither habitat,
                                  equivalent.say reading
                           FROM equivalent
                          ) slow
                    ) facilitate
              ) reaction
   CROSS JOIN (SELECT risk.shower,
                      risk.sponsor intelligent,
                      risk.quest transformation,
                      risk.move smooth
               FROM (SELECT collector.sponsor,
                            collector.be quest,
                            collector.average shower,
                            collector.manual century,
                            collector.message move
                     FROM collector
                    ) risk
              ) shell

''',
'''
SELECT expected.before
FROM          that
   CROSS JOIN (SELECT statue.county killer,
                      statue.too
               FROM          (SELECT glad.too,
                                     glad.arm county
                              FROM (SELECT principal.disorder trick,
                                           development.pill,
                                           principal.too,
                                           principal.call arm
                                    FROM          development
                                       CROSS JOIN principal
                                   ) glad
                             ) statue
                  CROSS JOIN (SELECT gold.thousand,
                                     gold.particularly manner,
                                     gold.liquid,
                                     gold.plan sign,
                                     gold.people,
                                     gold.dirty
                              FROM (SELECT net.liquid,
                                           net.engage thousand,
                                           net.dot particularly,
                                           net.rid folk,
                                           net.people,
                                           net.plan,
                                           net.dirty,
                                           net.mandate desire
                                    FROM net
                                   ) gold
                             ) knife
                  CROSS JOIN tooth
              ) motive
   CROSS JOIN (SELECT stability.herb retailer,
                      stability.DNA conclude,
                      stability.king bear,
                      stability.blond inspiration,
                      stability.application my
               FROM (SELECT counselor.king,
                            counselor.application,
                            counselor.transportation,
                            counselor.herb,
                            counselor.blond,
                            counselor.future DNA
                     FROM counselor
                    ) stability
              ) pursuit
   CROSS JOIN receiver
   CROSS JOIN honor
   CROSS JOIN opt
   CROSS JOIN (SELECT standard.rest,
                      standard.either,
                      standard.random daily,
                      standard.tiny box,
                      standard.trend,
                      standard.continue pet,
                      standard.before,
                      standard.obstacle plus
               FROM standard
              ) expected
   CROSS JOIN (SELECT compromise.detail,
                      compromise.last,
                      compromise.possession,
                      compromise.chase captain,
                      compromise.fail
               FROM (SELECT tend.fail,
                            tend.survive balanced,
                            tend.condition possession,
                            tend.analysis Hispanic,
                            tend.mountain chase,
                            tend.cover continue,
                            tend.last,
                            tend.detail
                     FROM tend
                    ) compromise
              ) team
   CROSS JOIN justice
   CROSS JOIN wheel
   CROSS JOIN (SELECT payment.mechanic,
                      payment.inside
               FROM payment
              ) particle
   CROSS JOIN (SELECT prominent.deny,
                      prominent.hit realize,
                      prominent.pleasure roughly
               FROM (SELECT dancing.drive deny,
                            dancing.survey secret,
                            dancing.greatest,
                            dancing.just,
                            dancing.dictate,
                            dancing.hit,
                            dancing.skin pleasure
                     FROM dancing
                    ) prominent
              ) secure
   CROSS JOIN (SELECT night.cable student,
                      night.view
               FROM (SELECT portrait.view,
                            portrait.cable,
                            portrait.reception,
                            portrait.potential plain,
                            portrait.late lie,
                            portrait.first
                     FROM portrait
                    ) night
              ) executive
   CROSS JOIN seemingly
   CROSS JOIN balance
   CROSS JOIN likely
   CROSS JOIN (SELECT fat.afterward leap,
                      fat.feature frankly,
                      fat.butter
               FROM fat
              ) starter
   CROSS JOIN (SELECT shell.hook pattern,
                      shell.classify,
                      shell.benefit themselves,
                      shell.loop
               FROM shell
              ) excellent




''',
'''
SELECT concrete.technique foreigner
FROM          (SELECT snow.alliance regain,
                      snow.rise next,
                      threaten.technique,
                      snow.provoke foreign
               FROM          desk
                  CROSS JOIN (SELECT isolated.female technique
                              FROM (SELECT during.female
                                    FROM (SELECT nice.high experimental,
                                                 nice.beyond,
                                                 equal.female
                                          FROM          equal
                                             CROSS JOIN (SELECT shift.scheme high,
                                                                shift.scared exam,
                                                                shift.fish oak,
                                                                shift.beyond
                                                         FROM shift
                                                        ) nice
                                         ) during
                                   ) isolated
                             ) threaten
                  CROSS JOIN (SELECT cheek.alliance,
                                     cheek.salary developmental,
                                     cheek.coin provoke,
                                     cheek.set,
                                     cheek.historical rise,
                                     cheek.complex indigenous,
                                     cheek.appreciation
                              FROM (SELECT creative.we alliance,
                                           creative.prime historical,
                                           creative.counseling salary,
                                           creative.favorite set,
                                           creative.fragment appreciation,
                                           creative.point coin,
                                           creative.complex
                                    FROM creative
                                   ) cheek
                             ) snow
                  CROSS JOIN (SELECT ease.supporter,
                                     ease.radar ideology
                              FROM (SELECT league.fail supporter,
                                           league.radar
                                    FROM (SELECT Olympic.must radar,
                                                 Olympic.fail
                                          FROM Olympic
                                         ) league
                                   ) ease
                             ) ever
              ) concrete
   CROSS JOIN (SELECT verdict.summary commit
               FROM (SELECT rub.summary
                     FROM (SELECT resolution.credit summary
                           FROM (SELECT civic.credit
                                 FROM (SELECT loan.credit
                                       FROM loan
                                      ) civic
                                ) resolution
                          ) rub
                    ) verdict
              ) independent
   CROSS JOIN (SELECT reduce.ie restriction
               FROM (SELECT surround.ie
                     FROM (SELECT pleased.ie
                           FROM (SELECT cooperation.pay ie
                                 FROM (SELECT train.canvas pay
                                       FROM (SELECT sacrifice.canvas
                                             FROM (SELECT season.canvas
                                                   FROM season
                                                  ) sacrifice
                                            ) train
                                      ) cooperation
                                ) pleased
                          ) surround
                    ) reduce
              ) adjust
   CROSS JOIN leading
   CROSS JOIN frankly
   CROSS JOIN (SELECT missile.Jew profession,
                      missile.easily slide,
                      missile.marriage,
                      missile.ethnic determine
               FROM missile
              ) construction
   CROSS JOIN booth
   CROSS JOIN (SELECT airport.AIDS
               FROM airport
              ) start
   CROSS JOIN solely
   CROSS JOIN gym
   CROSS JOIN staff
   CROSS JOIN lead
   CROSS JOIN (SELECT peer.electronic,
                      peer.unable,
                      peer.winner,
                      peer.pass,
                      peer.sodium target,
                      peer.consume,
                      peer.plain,
                      peer.blow
               FROM peer
              ) partner
   CROSS JOIN (SELECT award.territory ecological,
                      award.wake,
                      award.first,
                      award.I
               FROM award
              ) privately
   CROSS JOIN (SELECT hazard.favorite bid
               FROM (SELECT conflict.Mr,
                            conflict.hockey,
                            conflict.study favorite
                     FROM (SELECT organizational.favor extent,
                                  organizational.forum Mr,
                                  organizational.each study,
                                  organizational.precious hockey
                           FROM organizational
                          ) conflict
                    ) hazard
              ) assistant
   CROSS JOIN flight ;

''',
'''
SELECT fog.travel
FROM          sin
   CROSS JOIN (SELECT actor.overall,
                      actor.sort,
                      actor.concerned,
                      actor.casualty somebody,
                      actor.progressive sponsor,
                      actor.menu legislation
               FROM actor
              ) follow
   CROSS JOIN (SELECT added.even punch,
                      severe.feedback,
                      added.advanced charge,
                      severe.represent tremendous,
                      severe.consist tight,
                      severe.mode
               FROM          (SELECT print.advanced,
                                     good.frustrate national,
                                     good.upset tribal,
                                     good.breast even
                              FROM          (SELECT than.upset,
                                                    than.frustrate,
                                                    than.breast,
                                                    rack.elementary
                                             FROM          (SELECT neighborhood.upset,
                                                                   neighborhood.too,
                                                                   neighborhood.lawyer frustrate,
                                                                   neighborhood.individual breast
                                                            FROM neighborhood
                                                           ) than
                                                CROSS JOIN (SELECT gentleman.elementary
                                                            FROM (SELECT rhetoric.elementary
                                                                  FROM (SELECT mistake.elementary,
                                                                               mistake.street
                                                                        FROM (SELECT view.feeling street,
                                                                                     view.telescope elementary
                                                                              FROM (SELECT back.mood feeling,
                                                                                           back.sound telescope
                                                                                    FROM back
                                                                                   ) view
                                                                             ) mistake
                                                                       ) rhetoric
                                                                 ) gentleman
                                                           ) rack
                                            ) good
                                 CROSS JOIN print
                             ) added
                  CROSS JOIN (SELECT ban.consist,
                                     ban.mode,
                                     ban.represent,
                                     ban.feedback
                              FROM ban
                             ) severe
              ) reveal
   CROSS JOIN onto
   CROSS JOIN north
   CROSS JOIN (SELECT joke.honey
               FROM joke
              ) capability
   CROSS JOIN development
   CROSS JOIN thin
   CROSS JOIN fine
   CROSS JOIN (SELECT sweater.communicate cotton,
                      sweater.schedule hole,
                      sweater.disabled off,
                      sweater.abuse insight
               FROM (SELECT quit.abuse,
                            quit.professional,
                            quit.debt,
                            quit.disabled,
                            quit.ship communicate,
                            quit.respect schedule
                     FROM quit
                    ) sweater
              ) ordinary
   CROSS JOIN elect
   CROSS JOIN flight
   CROSS JOIN (SELECT hold.key
               FROM (SELECT topic.casual key
                     FROM topic
                    ) hold
              ) campus
   CROSS JOIN (SELECT limit.production,
                      limit.agreement trade
               FROM (SELECT deputy.agreement,
                            deputy.frequently,
                            deputy.constitute,
                            deputy.stream production,
                            deputy.urban
                     FROM deputy
                    ) limit
              ) guilty
   CROSS JOIN (SELECT magnitude.charity international,
                      magnitude.conversion,
                      magnitude.outline,
                      magnitude.kill commodity,
                      magnitude.segment,
                      magnitude.piece part,
                      magnitude.thumb
               FROM magnitude
              ) spending
   CROSS JOIN (SELECT tendency.travel,
                      tendency.desperately manufacturer
               FROM (SELECT picture.northwest travel,
                            picture.desperately
                     FROM (SELECT enthusiasm.northwest,
                                  enthusiasm.tear,
                                  enthusiasm.process desperately
                           FROM enthusiasm
                          ) picture
                    ) tendency
              ) fog
   CROSS JOIN (SELECT compose.us form,
                      compose.loyalty,
                      compose.special
               FROM (SELECT homework.back loyalty,
                            homework.us,
                            homework.reach singer,
                            homework.special,
                            homework.can reaction
                     FROM homework
                    ) compose
              ) herself
   CROSS JOIN west ;

'''];
