var exp2Statements = [
/*
  L: 20
  T: RC
 */
 '''
 SELECT only.spread
FROM          (SELECT unlike.automatic strengthen,
                      formal.respect,
                      formal.revolutionary praise
               FROM          formal
                  CROSS JOIN (SELECT standard.pad issue,
                                     standard.painting automatic
                              FROM standard
                             ) unlike
                  CROSS JOIN (SELECT regain.regulator,
                                     regain.cable
                              FROM (SELECT regulator.dismiss cable,
                                           regulator.regulator
                                    FROM regulator
                                   ) regain
                             ) cookie
              ) complete
   CROSS JOIN only
   CROSS JOIN mark
   CROSS JOIN chronic
 ''',
/*
  L: 20
  T: RC
 */
'''
SELECT break.march establish
FROM          much
   CROSS JOIN (SELECT cry.historically disabled
               FROM cry
              ) distant
   CROSS JOIN (SELECT particle.mate,
                      particle.speculation pitch
               FROM (SELECT employee.mate,
                            employee.photograph speculation
                     FROM employee
                    ) particle
              ) massive
   CROSS JOIN photograph
   CROSS JOIN steel
   CROSS JOIN (SELECT murder.everyday alike,
                      murder.nod each,
                      murder.proportion march,
                      murder.heart give
               FROM murder
              ) break ;
''',

  /*
  L: 20
  T: RT
   */
'''
  SELECT troop.wage
FROM          (SELECT marketplace.street,
                      marketplace.rich level,
                      marketplace.commodity truly,
                      marketplace.sole
               FROM marketplace
              ) palace
   CROSS JOIN (SELECT concede.pop administer
               FROM concede
              ) diagnose
   CROSS JOIN blessing
   CROSS JOIN (SELECT smell.whale
               FROM (SELECT neighboring.most tumor,
                            neighboring.composition,
                            neighboring.whale
                     FROM neighboring
                    ) smell
              ) favor
   CROSS JOIN discipline
   CROSS JOIN troop ;

''',

  /*
  L: 20
  T: RT
   */
'''
  SELECT four.retailer
FROM          (SELECT separate.ultimate,
                      separate.nest,
                      separate.strict,
                      separate.ready tune,
                      philosophical.very retailer,
                      separate.constantly
               FROM          philosophical
                  CROSS JOIN (SELECT guilt.revolution ultimate,
                                     trap.gifted,
                                     trap.amid constantly,
                                     guilt.cause strict,
                                     pleasant.nest,
                                     guilt.ready
                              FROM          guilt
                                 CROSS JOIN pleasant
                                 CROSS JOIN trap
                             ) separate
              ) four
   CROSS JOIN responsible



''',
  /*
  L: 70
  T: RC
   */
'''

SELECT identity.scratch formal
FROM          extended
   CROSS JOIN (SELECT mother.German overcome,
                      straight.live,
                      toxic.finally,
                      mother.despite,
                      straight.European ship,
                      mother.four convinced,
                      mother.I give,
                      straight.while scratch
               FROM          mood
                  CROSS JOIN (SELECT broadcast.missing finally
                              FROM broadcast
                             ) toxic
                  CROSS JOIN (SELECT religious.deliberately saving,
                                     religious.European,
                                     introduction.globe live,
                                     religious.clerk rival,
                                     introduction.user while,
                                     religious.rhythm praise
                              FROM          (SELECT broadcast.clerk,
                                                    broadcast.widespread invade,
                                                    broadcast.okay European,
                                                    broadcast.Muslim,
                                                    broadcast.convention deliberately,
                                                    broadcast.republican rhythm
                                             FROM broadcast
                                            ) religious
                                 CROSS JOIN introduction
                             ) straight
                  CROSS JOIN (SELECT democratic.punch chemistry
                              FROM (SELECT report.punch
                                    FROM (SELECT guarantee.perspective punch
                                          FROM (SELECT outdoor.locate,
                                                       outdoor.perspective,
                                                       outdoor.Greek excitement
                                                FROM (SELECT cheek.dinner bake,
                                                             cheek.perspective,
                                                             cheek.complexity away,
                                                             cheek.Greek,
                                                             cheek.dynamics,
                                                             cheek.operator locate
                                                      FROM cheek
                                                     ) outdoor
                                               ) guarantee
                                         ) report
                                   ) democratic
                             ) adapt
                  CROSS JOIN major
                  CROSS JOIN (SELECT pool.impressive coordinate
                              FROM (SELECT blanket.criminal impressive
                                    FROM (SELECT partially.complicated,
                                                 partially.ad criminal,
                                                 partially.package arise
                                          FROM partially
                                         ) blanket
                                   ) pool
                             ) neighborhood
                  CROSS JOIN (SELECT substantial.German,
                                     substantial.despite,
                                     substantial.expose passing,
                                     substantial.I,
                                     substantial.matter,
                                     substantial.prior,
                                     substantial.four,
                                     substantial.favor
                              FROM substantial
                             ) mother
                  CROSS JOIN conversation
              ) identity ;
''',

  /*
  L: 70
  T: RC
   */
 '''
SELECT describe.amid
FROM          terrain
   CROSS JOIN (SELECT shop.require,
                      general.researcher boom,
                      identification.resume quarterback
               FROM          (SELECT emotional.president tumor,
                                     emotional.regional,
                                     emotional.require,
                                     emotional.permission,
                                     stance.data processing
                              FROM          (SELECT steam.data
                                             FROM steam
                                            ) stance
                                 CROSS JOIN (SELECT usually.orange regional,
                                                    usually.distance president,
                                                    usually.require,
                                                    usually.permission
                                             FROM usually
                                            ) emotional
                             ) shop
                  CROSS JOIN general
                  CROSS JOIN identification
              ) running
   CROSS JOIN yes
   CROSS JOIN (SELECT cab.offender elbow,
                      cab.fist shareholder,
                      cab.yes usual
               FROM (SELECT chance.fist,
                            chance.offender,
                            chance.yes
                     FROM (SELECT frontier.yes,
                                  frontier.look offender,
                                  frontier.grin fist
                           FROM frontier
                          ) chance
                    ) cab
              ) laugh
   CROSS JOIN (SELECT judge.variation,
                      judge.prospect,
                      judge.itself remember,
                      judge.fund old,
                      judge.headquarters market
               FROM judge
              ) many
   CROSS JOIN (SELECT influence.consent,
                      influence.knee trim,
                      influence.everyday smell,
                      influence.amid
               FROM influence
              ) describe
   CROSS JOIN (SELECT maker.struggle therapist
               FROM (SELECT determination.struggle
                     FROM (SELECT solely.track struggle
                           FROM solely
                          ) determination
                    ) maker
              ) envision
   CROSS JOIN (SELECT first.jump housing,
                      first.near bishop
               FROM (SELECT record.coach jump,
                            record.wish near
                     FROM (SELECT drill.coach,
                                  drill.virus wish
                           FROM (SELECT participate.striking virus,
                                        participate.coach
                                 FROM participate
                                ) drill
                          ) record
                    ) first
              ) bit ;


''',


 /* AB HIER */

  /*
  L: 70
  T: RT

  73
   */
 '''
SELECT myth.architecture
FROM          drill
   CROSS JOIN (SELECT legislation.nature fellow,
                      legislation.afford,
                      legislation.carefully,
                      legislation.over
               FROM legislation
              ) few
   CROSS JOIN (SELECT clean.processing,
                      clean.lamp,
                      clean.origin,
                      clean.pretty thirty,
                      clean.doctrine,
                      clean.print chemistry
               FROM clean
              ) faster
   CROSS JOIN (SELECT gray.lemon relatively,
                      gray.spread ethics,
                      gray.likely involved
               FROM gray
              ) explanation
   CROSS JOIN melt
   CROSS JOIN (SELECT track.ha,
                      track.tradition,
                      track.serve,
                      track.choice
               FROM track
              ) afternoon
   CROSS JOIN (SELECT testify.birth wipe,
                      testify.odd enhance,
                      testify.agency,
                      testify.drama garden,
                      testify.certainly layer,
                      testify.dry,
                      testify.observe link
               FROM testify
              ) carpet
   CROSS JOIN blend
   CROSS JOIN towards
   CROSS JOIN (SELECT prompt.island,
                      prompt.blame architecture,
                      prompt.indication serving,
                      prompt.notice,
                      prompt.live,
                      prompt.donor balloon,
                      prompt.pencil although,
                      prompt.inside center
               FROM prompt
              ) myth
   CROSS JOIN (SELECT human.depressed,
                      human.airplane scream,
                      human.manufacturer rain,
                      human.warehouse
               FROM (SELECT contest.reporter depressed,
                            contest.manufacturer,
                            contest.cite airplane,
                            contest.earnings tear,
                            contest.diabetes warehouse
                     FROM (SELECT tomorrow.winter reporter,
                                  tomorrow.cite,
                                  tomorrow.offer diabetes,
                                  tomorrow.simply bare,
                                  tomorrow.earnings,
                                  tomorrow.gate,
                                  tomorrow.previous,
                                  tomorrow.credit manufacturer
                           FROM tomorrow
                          ) contest
                    ) human
              ) burning ;


''',


  /*
  L: 70
  T: RT

  fertig :)
   */
'''

SELECT ourselves.Olympics
FROM          (SELECT domestic.specialty bring,
                      mixed.property,
                      habit.provide estimate,
                      habit.famous bow,
                      mixed.senior lead,
                      teenage.teammate,
                      mixed.talent deploy,
                      panel.identify
               FROM          (SELECT mentor.anymore,
                                     mentor.famous,
                                     mentor.tax provide,
                                     mentor.intense
                              FROM mentor
                             ) habit
                  CROSS JOIN domestic
                  CROSS JOIN (SELECT tale.cloud body,
                                     tale.overall
                              FROM (SELECT classify.car mate,
                                           classify.improve cloud,
                                           classify.overall
                                    FROM (SELECT social.major improve,
                                                 social.overall,
                                                 social.engine car
                                          FROM social
                                         ) classify
                                   ) tale
                             ) bloody
                  CROSS JOIN teenage
                  CROSS JOIN (SELECT return.weave,
                                     return.property,
                                     return.age talent,
                                     return.senior
                              FROM return
                             ) mixed
                  CROSS JOIN panel
              ) wilderness
   CROSS JOIN (SELECT government.net soup,
                      government.equity historian,
                      government.eleven monthly,
                      government.capacity,
                      government.healthy action,
                      government.pasta Olympics,
                      government.significantly,
                      government.harm despite
               FROM government
              ) ourselves
   CROSS JOIN (SELECT develop.obtain,
                      develop.lay minor,
                      develop.franchise,
                      develop.notice seldom
               FROM develop
              ) meet
   CROSS JOIN harm
   CROSS JOIN everyone
   CROSS JOIN (SELECT isolated.sibling little,
                      isolated.etc comment,
                      isolated.ban uncertainty,
                      isolated.perceive,
                      isolated.reward lead,
                      isolated.neighbor
               FROM (SELECT display.distinguish ban,
                            display.rebel etc,
                            display.sibling,
                            display.reward,
                            display.neighbor,
                            display.perceive
                     FROM display
                    ) isolated
              ) worldwide ;

''',

   /*
  L: 120
  T: RC

  128
   */
 '''
SELECT rental.suck
FROM          (SELECT divorce.double,
                      divorce.shortage Persian,
                      vocal.paper test,
                      frustrate.campaign everyday
               FROM          (SELECT total.campaign
                              FROM          total
                                 CROSS JOIN plunge
                             ) frustrate
                  CROSS JOIN study
                  CROSS JOIN (SELECT competition.likelihood paper
                              FROM          emphasize
                                 CROSS JOIN (SELECT lesson.artificial,
                                                    lesson.originally favorite
                                             FROM          (SELECT initiate.major artificial,
                                                                   initiate.whip matter,
                                                                   initiate.account,
                                                                   initiate.survey,
                                                                   initiate.news,
                                                                   initiate.originally
                                                            FROM initiate
                                                           ) lesson
                                                CROSS JOIN what
                                            ) resist
                                 CROSS JOIN (SELECT modest.superior,
                                                    unless.celebrity likelihood,
                                                    modest.disturb suspend
                                             FROM          modest
                                                CROSS JOIN unless
                                            ) competition
                             ) vocal
                  CROSS JOIN (SELECT photographer.leaf,
                                     photographer.agriculture attendance,
                                     photographer.closed zone
                              FROM photographer
                             ) structure
                  CROSS JOIN (SELECT place.beef plain,
                                     place.divorce
                              FROM place
                             ) particularly
                  CROSS JOIN (SELECT exhibition.written disease,
                                     exhibition.inspection enthusiasm
                              FROM exhibition
                             ) gasoline
                  CROSS JOIN grow
                  CROSS JOIN (SELECT strong.healthy spin,
                                     strong.vacuum,
                                     strong.turkey emphasis,
                                     strong.shortage,
                                     strong.project double,
                                     strong.drive seem,
                                     strong.coordinate,
                                     strong.examination
                              FROM strong
                             ) divorce
                  CROSS JOIN manufacturer
                  CROSS JOIN examine
                  CROSS JOIN (SELECT comfort.chase,
                                     comfort.already,
                                     comfort.initially
                              FROM (SELECT terrorist.chase,
                                           terrorist.absolutely,
                                           terrorist.already,
                                           terrorist.exercise initially,
                                           terrorist.pine better
                                    FROM terrorist
                                   ) comfort
                             ) wild
                  CROSS JOIN (SELECT production.doctrine
                              FROM (SELECT mutual.romance doctrine,
                                           mutual.supplier
                                    FROM (SELECT slowly.decline supplier,
                                                 slowly.Latin,
                                                 slowly.chamber romance,
                                                 slowly.instrument fellow,
                                                 slowly.perspective,
                                                 slowly.culture taste,
                                                 slowly.pole monument
                                          FROM (SELECT fatigue.chamber,
                                                       fatigue.pole,
                                                       fatigue.decline,
                                                       fatigue.sock Latin,
                                                       fatigue.perspective,
                                                       fatigue.bee instrument,
                                                       fatigue.entity culture
                                                FROM fatigue
                                               ) slowly
                                         ) mutual
                                   ) production
                             ) deal
              ) storm
   CROSS JOIN (SELECT utility.hire,
                      utility.anywhere lobby,
                      utility.conscience
               FROM (SELECT knee.anywhere,
                            knee.shock conscience,
                            knee.debate Italian,
                            knee.simultaneously hire
                     FROM (SELECT lead.respectively simultaneously,
                                  lead.with shock,
                                  lead.somebody,
                                  lead.creation,
                                  lead.elephant debate,
                                  lead.hearing anywhere
                           FROM lead
                          ) knee
                    ) utility
              ) accounting
   CROSS JOIN (SELECT survivor.investigator,
                      survivor.tropical weapon,
                      survivor.counselor suck,
                      survivor.identify very
               FROM (SELECT store.tropical,
                            store.administrative counselor,
                            store.me investigator,
                            store.bit challenge,
                            store.vulnerable identify
                     FROM store
                    ) survivor
              ) rental
''',

  /*
  L: 120
  T: RC

121
   */
 '''
SELECT society.latter
FROM          capable
   CROSS JOIN (SELECT voting.tile dumb,
                      voting.fish
               FROM          (SELECT striking.fish,
                                     striking.French tile
                              FROM (SELECT public.momentum avoid,
                                           feeling.importantly,
                                           public.fish,
                                           public.dramatic French
                                    FROM          feeling
                                       CROSS JOIN public
                                   ) striking
                             ) voting
                  CROSS JOIN (SELECT impressive.housing,
                                     impressive.technology abstract,
                                     impressive.witness,
                                     impressive.sculpture sanction,
                                     impressive.palace,
                                     impressive.United
                              FROM (SELECT elevator.witness,
                                           elevator.stack housing,
                                           elevator.intimate technology,
                                           elevator.signal off,
                                           elevator.palace,
                                           elevator.sculpture,
                                           elevator.United,
                                           elevator.restriction disagree
                                    FROM elevator
                                   ) impressive
                             ) since
                  CROSS JOIN pure
              ) fleet
   CROSS JOIN (SELECT garlic.commercial series,
                      garlic.expect root,
                      garlic.station accent,
                      garlic.contemporary prescription,
                      garlic.even disk
               FROM (SELECT yet.station,
                            yet.even,
                            yet.general,
                            yet.commercial,
                            yet.contemporary,
                            yet.terribly expect
                     FROM yet
                    ) garlic
              ) universal
   CROSS JOIN happiness
   CROSS JOIN earthquake
   CROSS JOIN credibility
   CROSS JOIN (SELECT dirt.balloon,
                      dirt.mark,
                      dirt.nutrient behavioral,
                      dirt.billion pant,
                      dirt.draft,
                      dirt.skilled measure,
                      dirt.latter,
                      dirt.ability accelerate
               FROM dirt
              ) society
   CROSS JOIN (SELECT treatment.ranch,
                      treatment.economist,
                      treatment.feedback,
                      treatment.atmosphere slap,
                      treatment.tie
               FROM (SELECT edge.tie,
                            edge.bid nod,
                            edge.chart feedback,
                            edge.install theology,
                            edge.ancient atmosphere,
                            edge.along skilled,
                            edge.economist,
                            edge.ranch
                     FROM edge
                    ) treatment
              ) fist
   CROSS JOIN mind
   CROSS JOIN basketball
   CROSS JOIN (SELECT living.orientation,
                      living.across
               FROM living
              ) eight
   CROSS JOIN (SELECT accountability.desk,
                      accountability.compose refugee,
                      accountability.epidemic hi
               FROM (SELECT red.poor desk,
                            red.officer rent,
                            red.facility,
                            red.commission,
                            red.hill,
                            red.compose,
                            red.educator epidemic
                     FROM red
                    ) accountability
              ) dry
   CROSS JOIN (SELECT rating.weakness exposure,
                      rating.degree
               FROM (SELECT perfectly.degree,
                            perfectly.weakness,
                            perfectly.investigation,
                            perfectly.irony ride,
                            perfectly.visible flame,
                            perfectly.obvious
                     FROM perfectly
                    ) rating
              ) dress
   CROSS JOIN screen
   CROSS JOIN cup
   CROSS JOIN index
   CROSS JOIN (SELECT dealer.accusation session,
                      dealer.agree excuse,
                      dealer.web
               FROM dealer
              ) many
   CROSS JOIN (SELECT wealthy.sort choose,
                      wealthy.import,
                      wealthy.attempt monthly,
                      wealthy.diet
               FROM wealthy
              ) bus



''',

  /*
  L: 120
  T: RT

  123
   */
 '''
SELECT eye.athletic tour
FROM          (SELECT review.ban mix,
                      review.federal differently,
                      sponsor.athletic,
                      review.fourth lend
               FROM          skirt
                  CROSS JOIN (SELECT half.preference athletic
                              FROM (SELECT invite.preference
                                    FROM (SELECT even.practically impact,
                                                 even.speculate,
                                                 hand.preference
                                          FROM          hand
                                             CROSS JOIN (SELECT council.final practically,
                                                                council.violate notice,
                                                                council.verbal unfortunately,
                                                                council.speculate
                                                         FROM council
                                                        ) even
                                         ) invite
                                   ) half
                             ) sponsor
                  CROSS JOIN (SELECT attend.ban,
                                     attend.anywhere permanent,
                                     attend.housing fourth,
                                     attend.studio,
                                     attend.development federal,
                                     attend.resign scenario,
                                     attend.icon
                              FROM (SELECT cousin.adjustment ban,
                                           cousin.bird development,
                                           cousin.spouse anywhere,
                                           cousin.economics studio,
                                           cousin.bat icon,
                                           cousin.incorporate housing,
                                           cousin.resign
                                    FROM cousin
                                   ) attend
                             ) review
                  CROSS JOIN (SELECT articulate.piece,
                                     articulate.tube breathing
                              FROM (SELECT confusion.response piece,
                                           confusion.tube
                                    FROM (SELECT render.downtown tube,
                                                 render.response
                                          FROM render
                                         ) confusion
                                   ) articulate
                             ) behave
              ) eye
   CROSS JOIN (SELECT famous.recession opportunity
               FROM (SELECT survivor.recession
                     FROM (SELECT help.combat recession
                           FROM (SELECT sea.combat
                                 FROM (SELECT annual.combat
                                       FROM annual
                                      ) sea
                                ) help
                          ) survivor
                    ) famous
              ) consumption
   CROSS JOIN (SELECT rice.arrangement steady
               FROM (SELECT trip.arrangement
                     FROM (SELECT campaign.arrangement
                           FROM (SELECT chase.carve arrangement
                                 FROM (SELECT dilemma.poke carve
                                       FROM (SELECT availability.poke
                                             FROM (SELECT root.poke
                                                   FROM root
                                                  ) availability
                                            ) dilemma
                                      ) chase
                                ) campaign
                          ) trip
                    ) rice
              ) exact
   CROSS JOIN impossible
   CROSS JOIN disabled
   CROSS JOIN (SELECT curve.complain kind,
                      curve.ill jet,
                      curve.popular,
                      curve.clean tree
               FROM curve
              ) play
   CROSS JOIN requirement
   CROSS JOIN (SELECT label.delight
               FROM label
              ) participant
   CROSS JOIN note
   CROSS JOIN auction
   CROSS JOIN spare
   CROSS JOIN buyer
   CROSS JOIN (SELECT neither.tune,
                      neither.plaintiff,
                      neither.addition,
                      neither.earnings,
                      neither.demand belly,
                      neither.extra,
                      neither.mind,
                      neither.properly
               FROM neither
              ) veteran
   CROSS JOIN (SELECT find.experience mouse,
                      find.reserve,
                      find.industrial,
                      find.attempt
               FROM find
              ) expert
   CROSS JOIN (SELECT golden.economics carbohydrate
               FROM (SELECT recording.aide,
                            recording.naval,
                            recording.modest economics
                     FROM (SELECT slow.transmit client,
                                  slow.planner aide,
                                  slow.scholar modest,
                                  slow.relation naval
                           FROM slow
                          ) recording
                    ) golden
              ) difficulty
   CROSS JOIN gay ;
''',

  /*
  L: 120
  T: RT

  124
   */
'''
SELECT news.jury
FROM          truly
   CROSS JOIN (SELECT enough.screen,
                      enough.constitution,
                      enough.chief,
                      enough.tribe liability,
                      enough.bureau open,
                      enough.know sexuality
               FROM enough
              ) threaten
   CROSS JOIN (SELECT phrase.bias creation,
                      boundary.beam,
                      phrase.same sacrifice,
                      boundary.yield behind,
                      boundary.her report,
                      boundary.sky
               FROM          (SELECT coach.same,
                                     fiber.future epidemic,
                                     fiber.reader patient,
                                     fiber.hi bias
                              FROM          (SELECT puzzle.reader,
                                                    puzzle.future,
                                                    puzzle.hi,
                                                    barn.trap
                                             FROM          (SELECT transmit.reader,
                                                                   transmit.absorb,
                                                                   transmit.assist future,
                                                                   transmit.restriction hi
                                                            FROM transmit
                                                           ) puzzle
                                                CROSS JOIN (SELECT trigger.trap
                                                            FROM (SELECT morning.trap
                                                                  FROM (SELECT without.trap,
                                                                               without.integrate
                                                                        FROM (SELECT hostile.regain integrate,
                                                                                     hostile.toilet trap
                                                                              FROM (SELECT sense.hostage regain,
                                                                                           sense.participation toilet
                                                                                    FROM sense
                                                                                   ) hostile
                                                                             ) without
                                                                       ) morning
                                                                 ) trigger
                                                           ) barn
                                            ) fiber
                                 CROSS JOIN coach
                             ) phrase
                  CROSS JOIN (SELECT ill.her,
                                     ill.sky,
                                     ill.yield,
                                     ill.beam
                              FROM ill
                             ) boundary
              ) pie
   CROSS JOIN compensation
   CROSS JOIN grape
   CROSS JOIN (SELECT drama.agenda
               FROM drama
              ) cash
   CROSS JOIN entirely
   CROSS JOIN emphasis
   CROSS JOIN ironically
   CROSS JOIN (SELECT description.funny apparent,
                      description.bold depression,
                      description.drop automatically,
                      description.increase rip
               FROM (SELECT fuel.increase,
                            fuel.sector,
                            fuel.owner,
                            fuel.drop,
                            fuel.leader funny,
                            fuel.since bold
                     FROM fuel
                    ) description
              ) spite
   CROSS JOIN engage
   CROSS JOIN stability
   CROSS JOIN (SELECT swell.anonymous
               FROM (SELECT visible.horror anonymous
                     FROM visible
                    ) swell
              ) reinforce
   CROSS JOIN (SELECT instant.partially,
                      instant.liberty arm
               FROM (SELECT fur.liberty,
                            fur.twelve,
                            fur.consideration,
                            fur.liquid partially,
                            fur.couple
                     FROM fur
                    ) instant
              ) lightning
   CROSS JOIN (SELECT creativity.measure presence,
                      creativity.keep,
                      creativity.protest,
                      creativity.relax attach,
                      creativity.convert,
                      creativity.lock finance,
                      creativity.remove
               FROM creativity
              ) exhibition
   CROSS JOIN (SELECT highway.jury,
                      highway.them question
               FROM (SELECT corporate.credit jury,
                            corporate.them
                     FROM (SELECT communication.credit,
                                  communication.passage,
                                  communication.guard them
                           FROM communication
                          ) corporate
                    ) highway
              ) news
   CROSS JOIN (SELECT particular.content belong,
                      particular.appeal,
                      particular.complex
               FROM (SELECT withdrawal.sense appeal,
                            withdrawal.content,
                            withdrawal.old naturally,
                            withdrawal.complex,
                            withdrawal.vanish anything
                     FROM withdrawal
                    ) particular
              ) new
   CROSS JOIN should ;
'''
];