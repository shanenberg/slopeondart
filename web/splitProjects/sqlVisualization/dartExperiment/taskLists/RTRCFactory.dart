import '../projectStuff/projectStuff.dart';
import '../tasks/Task.dart';
import '../tasks/break/Break.dart';
import '../tasks/originColumn/OriginColumnTask.dart';
import '../tasks/originTable/OriginTableTask.dart';
import 'RTRCTasks.dart';
import '../../../../../lib/slot/runtime/objects/SLOTSLOPETreeNode.dart';

main() {
  print(RTRCTasks.length);
}






RCFactory() {
  List<Task> tasks = [];


  DisplayMethod currentMethod;
  for(int i = 0; i < RTRCTasks.length; i++) {
    if(i == 0) {
      tasks.add(new OriginColumnTask(parseSQL('''
        SELECT society.shatter
FROM          capable
   CROSS JOIN (SELECT voting.tile shatter,
                      voting.fish
               FROM          (SELECT striking.fish,
                                     striking.French tile
                              FROM (SELECT public.momentum avoid,
                                           feeling.importantly,
                                           public.fish,
                                           public.dramatic French
                                    FROM          feeling
                                       CROSS JOIN public
                                   ) striking
                             ) voting
             ) society
      '''), DisplayMethod.TEXTUAL)..description += "  (Testaufgabe)");






      tasks.add(new OriginColumnTask(parseSQL('''
        SELECT harm.bark
FROM          (SELECT government.net soup,
                      government.equity historian,
                      government.eleven monthly,
                      government.capacity,
                      government.healthy action,
                      government.pasta Olympics,
                      government.significantly,
                      government.harm despite
               FROM government
              ) ourselves
   CROSS JOIN (SELECT develop.obtain,
                      develop.lay minor,
                      develop.franchise,
                      develop.notice seldom
               FROM develop
              ) meet
   CROSS JOIN harm
      '''), DisplayMethod.TEXTUAL)..description += "  (Testaufgabe)");
    }


    if(i == 12) {
      tasks.add(new Break());

      tasks.add(new OriginColumnTask(parseSQL('''
        SELECT society.shatter
FROM          capable
   CROSS JOIN (SELECT voting.tile shatter,
                      voting.fish
               FROM          (SELECT striking.fish,
                                     striking.French tile
                              FROM (SELECT public.momentum avoid,
                                           feeling.importantly,
                                           public.fish,
                                           public.dramatic French
                                    FROM          feeling
                                       CROSS JOIN public
                                   ) striking
                             ) voting
             ) society
      '''), DisplayMethod.VISUAL)..description += "  (Testaufgabe)");




      tasks.add(new OriginColumnTask(parseSQL('''
        SELECT harm.bark
FROM          (SELECT government.net soup,
                      government.equity historian,
                      government.eleven monthly,
                      government.capacity,
                      government.healthy action,
                      government.pasta Olympics,
                      government.significantly,
                      government.harm despite
               FROM government
              ) ourselves
   CROSS JOIN (SELECT develop.obtain,
                      develop.lay minor,
                      develop.franchise,
                      develop.notice seldom
               FROM develop
              ) meet
   CROSS JOIN harm
      '''), DisplayMethod.VISUAL)..description += "  (Testaufgabe)");

    }



    if(i < 12)
      currentMethod = DisplayMethod.TEXTUAL;
    else
      currentMethod = DisplayMethod.VISUAL;

    SLOTSLOPETreeNode parsed;
    try {
      parsed = parseSQL(RTRCTasks[i]);
    }
    catch(e) {
      print(i);
      print(RTRCTasks[i]);
      throw e;
    }
    tasks.add(new OriginColumnTask(parsed, currentMethod));
  }

  return tasks;
}


RTRCFactory() {

  List<Task> tasks = [];


  int t = 0;
  DisplayMethod currentMethod;
  for(int i = 0; i < RTRCTasks.length; i+=2, t=(t+1)%2) {


    if(i < 12)
      currentMethod = DisplayMethod.VISUAL;
    else
      currentMethod = DisplayMethod.TEXTUAL;

    Task task1;
    Task task2;
    if(t == 0) {
      task1 = new OriginColumnTask(parseSQL(RTRCTasks[i]), currentMethod);
      print("Parsed Task1" + task1.runtimeType.toString());

      task2 = new OriginColumnTask(parseSQL(RTRCTasks[i+1]), currentMethod);
      print("Parsed Task2" + task2.runtimeType.toString());

    }
    else if(t == 1) {
      task1 = new OriginTableTask(parseSQL(RTRCTasks[i]), currentMethod);
      print("Parsed Task1" + task1.runtimeType.toString());
      task2 = new OriginTableTask(parseSQL(RTRCTasks[i+1]), currentMethod);
      print("Parsed Task2" + task2.runtimeType.toString());

    }

    tasks.add(task1);
    tasks.add(task2);
  }

  return tasks;

}