import '../projectStuff/projectStuff.dart';
import 'RTRCTasks.dart';
import '../../../../../lib/slot/runtime/objects/SLOTSLOPETreeNode.dart';

main() {
  initProject();
  for(String tStr in RTRCTasks) {
    SLOTSLOPETreeNode hi = parseSQL(tStr);
    print("");
    project.runFunctionUnwrap("setColumnMaps", [hi]);
    project.runFunctionUnwrap("setFromExtras", [hi]);
    project.runFunctionUnwrap("printStatementStats", [hi]);
  }
}