import '../logging/taskLogJSON.dart';
import '../tasks/Task.dart';
import 'FinishView.dart';
import 'PrologueView.dart';
import 'TaskView.dart';
import 'View.dart';
import 'dart:convert';
import 'dart:html';

List<Task> taskList;
int currentTask = 0;

View currentView;

drawTasks() {
  drawFirstPrologue();
  document.onKeyDown.listen((KeyboardEvent ev) {
    currentView.onKeyDown(ev);
  });

  window.addEventListener("keydown",(e) {
    if (e.keyCode == 114 || (e.ctrlKey && e.keyCode == 70)) {
      e.preventDefault();
    }
  });
}
drawFirstPrologue() {
  currentTask = 0;
  drawCurrentPrologue();
}
drawNextView() {
  currentTask++;
  if(currentTask < taskList.length)
    drawCurrentPrologue();
  else
    drawFinished();
}
drawCurrentPrologue() {
  View v = new PrologueView(taskList[currentTask]);
  drawView(v);
}
drawFinished() {
  drawView(new FinishView());
  saveLogs();
}

saveLogs() {
  saveStatLog();
  saveFullLog();
}


saveFullLog() {
  var blob = createTaskLogBlob(taskList);
  var a = new Element.a();
  a.download = "FullLog";
  a.href = Url.createObjectUrl(blob);
  document.body.append(a);
  a.click();
  a.remove();
}

saveStatLog() {
  var blob = createTaskLogBlob(taskList, fullText: false);
  var a = new Element.a();
  a.download = "Stats";
  a.href = Url.createObjectUrl(blob);
  document.body.append(a);
  a.click();
  a.remove();
}




drawView(View v) {
  currentView = v;
  currentView.draw();
}