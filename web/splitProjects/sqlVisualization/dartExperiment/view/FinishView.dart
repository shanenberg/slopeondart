import '../../../../nativeHtmlStuff/SQLPlayground/overhaul2/overhaul2PreProcess.dart';
import 'View.dart';
import 'dart:html';

class FinishView extends View{




  @override
  draw() {
    document.body.children = [];
    element = new Element.html(''' 
    <div class="task" style="display:none">
      <div class="taskDescription prologue">
        Vielen Dank für die Teilnahme! :)
      </div>
  
  
      <div class="taskTargetContainer">
        <div class="taskTarget">
          
        </div>
      </div>
   
    </div>''', validator: validator);
    document.body.append(element);


  }
}