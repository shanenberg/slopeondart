import '../../../../nativeHtmlStuff/SQLPlayground/overhaul2/overhaul2PreProcess.dart';
import '../logging/TaskAction.dart';
import '../tasks/Task.dart';
import 'View.dart';
import 'dart:html';
import 'viewMain.dart';

class TaskView extends View {
  Task task;
  Element inputElement;
  TaskView(this.task);

  @override
  draw() {
    document.body.children =[];
    element = new Element.html(''' 
    <div class="task" style="display:none">
      <div class="taskDescription">
        ${task.description}
      </div>
  
  
      <div class="taskTargetContainer">
        <div class="taskTarget">
          
        </div>
      </div>
  
  
  
      <div class="taskInput">
      Antwort:
      <input class="taskInputText" type="text" value="" title="lolz">
      <button class="taskOK">OK</button>
      </div>
  
      <div class="taskOutput"></div>
    </div>''', validator: validator);
    document.body.append(element);
    drawTarget();
    task.startTask();
    inputElement = element.querySelector(".taskInputText");
    this.registerEvents();
    inputElement.focus();
    window.scrollTo(0,0);

  }
  drawTarget() {
    if(task.displayMethod == DisplayMethod.TEXTUAL) {
      element
          .querySelector(".taskTarget")
          .append(task.textual);
    }
    else {
      element
          .querySelector(".taskTarget")
          .append(task.visual);
      doSQLHTMLArrowsAndStuff();
    }

  }
  inputTask() {
    String answer = (inputElement as TextInputElement).value;

    bool success = task.inputAnswer(answer);
    if(success) {
      task.stopTask();

      task.logger.actions.forEach((TaskAction ac) {
        print(ac.runtimeType.toString() + " "+ac.time.toString());
        print(ac.answer);
        print("");
      });



      this.remove();
      drawNextView();
      return true;
    }
    else {
      element.querySelector(".taskOutput").text = "Antwort Falsch.";
      return false;
    }
  }

  registerEvents() {
    super.registerEvents();
    element.querySelector(".taskOK")
      .onClick.listen((MouseEvent ev) {
        inputTask();
    });

    inputElement.onInput.listen(
    (Event ev) {
      task.logger.logTextInput((inputElement as TextInputElement).value);
    });

  }


  @override
  void onKeyDown(KeyboardEvent ev)
  {
    if(eventsRegistered && ev.key == "Enter") {
      if(ev.key == "Enter")
        inputTask();
      print(ev.key);
    }

  }


}