import 'dart:html';

abstract class View {
  Element element;
  bool eventsRegistered;
  draw();

  remove() {
    element.remove();
    element = null;
  }

  void registerEvents() {
    eventsRegistered = true;
  }
  void onKeyDown(KeyboardEvent ev) {}
}