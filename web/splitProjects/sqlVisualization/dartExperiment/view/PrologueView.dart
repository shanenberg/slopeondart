import 'dart:html';

import '../../../../nativeHtmlStuff/SQLPlayground/overhaul2/overhaul2PreProcess.dart';
import '../tasks/Task.dart';
import 'TaskView.dart';
import 'View.dart';
import 'viewMain.dart';

class PrologueView extends View {

  Task task;

  PrologueView(this.task);

  @override
  draw() {
    document.body.children =[];
    element = new Element.html(''' 
    <div class="task">
      <div class="taskDescription prologue">
        ${task.description}
      </div>
  
  
      <div class="taskTargetContainer">
        <div class="taskTarget">
          
        </div>
      </div>
  
  
      <div class="taskStart">
        <button class="buttonStart">Start</button>
      </div>
      
  
      <div class="taskOutput"></div>
    </div>''', validator: validator);
    document.body.append(element);
    this.registerEvents();
  }


  void registerEvents() {
    super.registerEvents();
    element.querySelector(".buttonStart")
        .onClick.listen((MouseEvent ev)  {
            switchToTaskView();
        });
  }


  @override
  void onKeyDown(KeyboardEvent ev)
  {
    if(eventsRegistered && ev.key == "Enter")
    switchToTaskView();
  }

  void switchToTaskView() {
    this.remove();
    drawView(new TaskView(task));
  }


}