import '../../../../lib/slot/runtime/objects/SLOTTreeNode.dart';
import 'dart:math';
import 'projectStuff/projectStuff.dart';
import 'tasks/Task.dart';
import 'tasks/break/Break.dart';
import 'tasks/originColumn/OriginColumnTask.dart';

Map renameGroups =
{ 1: { "min": 2,
  "max": 4
},
  2: {  "min": 8,
    "max": 10
  },
  3: {  "min": 14,
    "max": 17
  }
};

Map depthGroups =
     {1 : { "min": 1,
            "max": 1
          },
      2 : { "min": 3,
            "max": 3
          },
      3 : {  "min" : 5,
             "max" : 5
          }
     };

blaTasks() {
  List<Task> tasks = [];

  /*for(int i=0; i<2;i++) {
    SLOTTreeNode node = project.runFunctionUnwrap("generateRandomTemplate");
    tasks.add(new OriginColumnTask(node, DisplayMethod.TEXTUAL));
  }
*/
  for(int i=0; i<5;i++) {
    SLOTTreeNode node = project.runFunctionUnwrap("generateRandomTemplate");
    tasks.add(new OriginColumnTask(node, DisplayMethod.VISUAL));
  }

  return tasks;
}





randomTasks() {

  List<Task> tasks = [];





  List<SLOTTreeNode> firstPart = randomTaskNodes(1);
  //firstPart.sort((_, __) => new Random().nextInt(3)-1);
  firstPart.shuffle();
  for(SLOTTreeNode node in firstPart) {
    tasks.add(new OriginColumnTask(node, DisplayMethod.VISUAL));
  }


  tasks.add(new Break());

  List<SLOTTreeNode> secondPart = randomTaskNodes(1);
  secondPart.shuffle();
  //  secondPart.sort((_, __) => new Random().nextInt(3)-1);
  for(SLOTTreeNode node in secondPart) {
    tasks.add(new OriginColumnTask(node, DisplayMethod.TEXTUAL));
  }

  return tasks;
}

randomTaskNodes(int groupCount) {
  List<SLOTTreeNode> nodes = [];

  renameGroups.forEach((int renameKey, Map renameMinMax) {
    depthGroups.forEach((int depthKey, Map depthMinMax) {
      nodes.addAll(createGroupNodes(groupCount,
          () => {
              "renameCount": randomIntFromRange(renameMinMax["min"], renameMinMax["max"]),
              "exactDepth": randomIntFromRange(depthMinMax["min"], depthMinMax["max"])
          }));
    });
  });

  return nodes;
}

createGroupNodes(int groupCount, Function generateParam) {
  var nodes = [];
  for(int i = 0; i< groupCount; i++) {
    var node = project.runFunctionWrapUnwrap("generateRandomTemplateTags",
        [
          generateParam()
        ]);
    nodes.add(node);
  };
  return nodes;
}


randomIntFromRange(int lowerLimit, int upperLimit) {
  return lowerLimit + new Random().nextInt(upperLimit+1-lowerLimit);
}
