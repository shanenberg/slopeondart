import '../projectStuff/projectStuff.dart';
import 'checkSame.dart';
import 'dart:math';
import 'finalCheck.dart';
import '../../../../../lib/slot/runtime/builtInFunction/BuiltInFunction.dart';
import 'package:english_words/english_words.dart';
import 'sqlStrings.dart';

main() {
  initProject();
  oldStatements.shuffle();

  printArr("stmt1", oldStatements);
  printArr("stmt2", oldStatements);
}





List<String> statementStringsWithNewIdentifiers(List<String> strs) {
  List<String> newStrs = [];

  for(int i = 0; i<strs.length; i++) {
    String str = strs[i];

    SQLInfo beforeInfo = createSQLInfo(str);


    str = replaceAllIdentifiers(str);

    SQLInfo afterInfo = createSQLInfo(str);

    if(!beforeInfo.equals(afterInfo)) {
      print(beforeInfo.toString());
      print(afterInfo.toString());
      throw("WTF");
    }

    newStrs.add(str);


  }
  return newStrs;
}

printArr(String name, List<String> elements) {

  List<String> actualStatements = statementStringsWithNewIdentifiers(elements);
  print("var $name = [");


  for(int i = 0; i < actualStatements .length; i++) {
    print("'''");


    print(actualStatements [i]);
    if(i < actualStatements.length-1)
      print("''',");
    else
      print("'''");
  }

  print("];");
}


List<String> stringsWithNewIdentifiersUnchecked(List<String> oldStrings) {
  List<String> damnStrings = [];

  for(String oldString in oldStrings) {
    damnStrings.add(replaceAllIdentifiers(oldString));
  }

  return damnStrings;
}



replaceAllIdentifiers(String str) {
  List<String> identifiers = sqlIdentifiers(str);

  List<String> garbage = new List.from(identifiers)
    ..addAll(sqlKeywords);



  for(String foundID in identifiers) {
    String nextWord = randWordExcept(garbage);
    str = str.replaceAllMapped(new RegExp('(\\W)(${foundID})(\\W)'),
            (Match m) {


          return m.group(1) + nextWord + m.group(3);
        });
    garbage.add(nextWord);
  }
  return str;
}

String randWordExcept(List<String> except) {
  int randIndex;

  do {
    randIndex = new Random().nextInt(all.length);
  }
  while(except.contains(all[randIndex]));

  return all[randIndex];
}