import '../../../../../lib/slot/runtime/builtInFunction/BuiltInFunction.dart';
import '../taskLists/deTasks.dart';
import 'sqlStrings.dart';


RegExp id = new RegExp(r'\w+');

Iterable<String> sqlIdentifiers(String statement) {
  Iterable<String> ids = id.allMatches(statement)
      .map((Match m) => m.group(0))
      .where((String str) => !sqlKeywords.contains(str.toLowerCase()));

  List<String>  lol = [];
  for(String id in ids) {
    if(!lol.contains(id)) {
      lol.add(id);
    }
  }
  return lol;
}

main() {
 checkSame();
}

checkSame() {
  checkSameParam(exp2Statements);
}

checkSameParam(List<String> statementList) {
  for(int i = 0; i < statementList.length; i++) {
    for(int k = 0; k < statementList.length; k++) {
      if(i != k) {
        Iterable<String> first = sqlIdentifiers(statementList[i]);
        Iterable<String> second = sqlIdentifiers(statementList[k]);

        Iterable<String> lal = first.where((String str) => second.contains(str));


        if(lal.length>5) {
          print("#$i and #$k have similar identifiers");
          print(lal);
        }
      }
    }
  }


  print(statementList.length.toString() + " statements");
}




