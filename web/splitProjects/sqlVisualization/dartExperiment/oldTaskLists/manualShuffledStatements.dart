import '../tasks/Task.dart';
import '../tasks/break/Break.dart';
import '../tasks/originColumn/OriginColumnTask.dart';
import 'finalCheck.dart';
import 'printFactory.dart';
import 'shuffledStrings.dart';
import 'sqlStrings.dart';





manualPreShuffledFactory() {
  return manualStatements(shuffledStatementsVisual);
}


List<Task> manualNewStatements(List<String> stmtList) {
  List<SQLInfo> strs1 = statementsWithNewIdentifiers(stmtList);
  List<SQLInfo> strs2 = statementsWithNewIdentifiers(stmtList);

  for(SQLInfo info in strs1) {

  }

}


List<Task> manualStatements(List<String> stmtList) {

  List<SQLInfo> strs1 = statementsWithNewIdentifiers(stmtList);
  List<SQLInfo> strs2 = statementsWithNewIdentifiers(stmtList);

  List<Task> taskList = [];

  for(SQLInfo info in strs1) {
    taskList.add(new OriginColumnTask( info.node, DisplayMethod.VISUAL));
  }
  taskList.add(new Break());
  for(SQLInfo info in strs2) {
    taskList.add(new OriginColumnTask(info.node, DisplayMethod.TEXTUAL));
  }

  return taskList;
}

List<Task> manualShuffledStatements() {
  oldStatements.shuffle();
  return manualStatements(oldStatements);
}
