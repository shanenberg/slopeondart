import '../projectStuff/projectStuff.dart';
import 'checkSame.dart';
import 'dart:io';

import '../../../../../lib/slot/runtime/objects/SLOTSLOPETreeNode.dart';

main() {
  initProject();


  File file = new File(Directory.current.path + "\\web\\splitProjects\\sqlVisualization\\dartExperiment\\oldTaskLists\\exampleString.txt");
  String str = file.readAsStringSync();
  if(str.startsWith("'")) {
    str = str.substring(3);
    str = str.substring(0, str.length-3);
  }



  SLOTSLOPETreeNode node = parseSQL(str);



  project.runFunctionUnwrap("setNodeMaps", [node]);
  project.runFunctionUnwrap("setFromExtras", [node]);

  bool well = project.runFunctionUnwrap("allColumnsEndWell", [node]);

  if(!well) {
    print("fix references!");
    return;
  }

  printStatInfo(node);


  checkSame();

}

printStatInfo(node) {
  int length =project.runFunctionUnwrap("amountOfStuff", [node]);
  int resultDepth =project.runFunctionUnwrap("pathDepth", [node]);
  int renameCount =project.runFunctionUnwrap("pathRenameCount", [node]);

  print("Length: "+length.toString());
  print("Result Depth: "+resultDepth.toString());
  print("Renames on Path: "+renameCount.toString());
}

