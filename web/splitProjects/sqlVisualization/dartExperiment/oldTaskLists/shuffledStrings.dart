var shuffledStatementsVisual = [
'''
SELECT pit.dining
FROM          (SELECT counter.conservative,
                      clinic.seal dining,
                      counter.close,
                      counter.laser obstacle
               FROM          (SELECT gentle.seal
                              FROM          hence
                                 CROSS JOIN (SELECT trailer.reservation,
                                                   trailer.hunter announcement,
                                                   trailer.mandate contract,
                                                   trailer.isolation,
                                                   trailer.present
                                            FROM trailer
                                           ) matter

                                 CROSS JOIN (SELECT emphasize.regret seal ,
                                                    emphasize.wilderness,
                                                    devote.remember,
                                                    devote.salary,
                                                    devote.principle
                                             FROM          (SELECT here.regret,
                                                                   here.deploy wilderness
                                                            FROM (SELECT small.deploy,
                                                                         small.regret
                                                                  FROM small
                                                                 ) here
                                                           ) emphasize
                                                CROSS JOIN devote
                                            ) gentle
                                 CROSS JOIN (SELECT loose.chill abuse
                                             FROM loose
                                            ) loyalty
                             ) clinic
                  CROSS JOIN counter
              ) pit
   CROSS JOIN alley

''',
'''
SELECT onto.defeat
FROM          (SELECT liberty.still sensation,
                      decline.slide,
                      destroy.negotiate cat,
                      classical.defeat,
                      faint.recover,
                      liberty.branch
               FROM          destroy
                  CROSS JOIN liberty
                  CROSS JOIN (SELECT addition.defeat,
                                     addition.struggle
                              FROM (SELECT era.outstanding struggle,
                                           era.defeat,
                                           era.media
                                    FROM (SELECT offense.outstanding,
                                                 offense.defeat,
                                                 offense.because media
                                          FROM (SELECT fifteen.outstanding,
                                                        fifteen.defeat,
                                                        fifteen.because
                                                 FROM fifteen
                                                ) offense
                                         ) era
                                   ) addition
                             ) classical
                  CROSS JOIN decline
                  CROSS JOIN (SELECT proposal.bomb courtroom,
                                        proposal.gender recover
                                 FROM (SELECT shadow.superior gender,
                                              shadow.fog bomb,
                                              shadow.green logic
                                       FROM shadow
                                      ) proposal
                            ) faint 
              ) onto


''',
'''
SELECT contact.medium sophisticated
FROM          (SELECT notice.crack hour,
                      notice.threat,
                      notice.slice medium,
                      notice.shame
               FROM (SELECT soap.shame,
                            soap.threat,
                            soap.crack,
                            soap.star,
                            soap.poem slice
                     FROM (SELECT difficulty.poem,
                                  difficulty.shame,
                                  difficulty.dirty threat,
                                  difficulty.star,
                                  difficulty.more crack
                           FROM difficulty
                          ) soap
                    ) notice
              ) contact
   CROSS JOIN (SELECT defend.exist
               FROM (SELECT pink.frequently exist
                     FROM (SELECT fragment.barn,
                                  fragment.frequently,
                                  fragment.proceed sport,
                                  fragment.fold,
                                  fragment.humor salmon
                           FROM fragment
                          ) pink
                    ) defend
              ) chamber ;

''',
'''
SELECT mention.addition
FROM (SELECT workshop.genius,
             workshop.politically engineering,
             soar.chip coast,
             even.addition
      FROM          workshop
         CROSS JOIN (SELECT unfold.rescue middle,
                            unfold.addition
                     FROM (SELECT sense.suspend rescue,
                                  sense.addition
                           FROM          (SELECT summit.candle suspend,
                                                 bear.addition,
                                                 summit.breakfast religious
                                          FROM          (SELECT kingdom.addition,
                                                                kingdom.connection
                                                         FROM kingdom
                                                        ) bear
                                             CROSS JOIN summit
                                         ) sense
                              CROSS JOIN (SELECT tray.educator
                                          FROM (SELECT random.globe educator,
                                                       random.exceed
                                                FROM random
                                               ) tray
                                         ) emerge
                              CROSS JOIN (SELECT register.chew worth,
                                                 register.material,
                                                 register.insist,
                                                 register.ugly boy
                                          FROM register
                                         ) supporter
                          ) unfold
                    ) even
         CROSS JOIN soar
     ) mention

''',
'''
SELECT strict.precise too
FROM          (SELECT biology.minimal mention,
                      biology.environmental irony
               FROM (SELECT thing.minimal,
                            thing.gaze environmental,
                            thing.midst interior
                     FROM (SELECT terrorism.under midst,
                                  terrorism.minimal,
                                  terrorism.just gaze
                           FROM terrorism
                          ) thing
                    ) biology
              ) written
   CROSS JOIN oral
   CROSS JOIN (SELECT application.sufficient
               FROM (SELECT lately.protocol sufficient
                     FROM lately
                    ) application
              ) treatment
   CROSS JOIN (SELECT low.sexy,
                      low.examination,
                      low.familiar suit
               FROM low
              ) useful
   CROSS JOIN (SELECT uncertainty.only,
                      uncertainty.look trace,
                      uncertainty.reserve,
                      uncertainty.march Jewish
               FROM (SELECT cling.fashion march,
                            cling.benefit reserve,
                            cling.only,
                            cling.delay straight,
                            cling.look
                     FROM cling
                    ) uncertainty
              ) okay
   CROSS JOIN strict
   CROSS JOIN anonymous
   CROSS JOIN enemy

''',
'''
SELECT metal.goat
FROM          rarely
   CROSS JOIN (SELECT garden.habitat spectacular,
                      garden.goat,
                      garden.similarly,
                      peak.separation
               FROM          garden
                  CROSS JOIN peak
                  CROSS JOIN (SELECT disorder.convince safely,
                                     sexy.southern,
                                     sexy.broadcast fixed,
                                     disorder.scan custom
                              FROM          (SELECT cue.decent,
                                                    cue.explain,
                                                    cue.convince,
                                                    cue.personnel show,
                                                    cue.pure entire,
                                                    cue.proud sack,
                                                    cue.chance,
                                                    cue.scan
                                             FROM cue
                                            ) disorder
                                 CROSS JOIN sexy
                             ) touchdown
                  CROSS JOIN (SELECT self.identity
                              FROM (SELECT mall.poem identity
                                    FROM (SELECT gender.nevertheless poem
                                          FROM (SELECT celebrate.hard,
                                                       celebrate.nevertheless,
                                                       celebrate.link smile,
                                                       celebrate.standard,
                                                       celebrate.shock
                                                FROM celebrate
                                               ) gender
                                         ) mall
                                   ) self
                             ) squad
              ) metal
   CROSS JOIN (SELECT TV.assistance pollution
               FROM TV
              ) again
   CROSS JOIN basketball
   CROSS JOIN (SELECT diverse.shelter usual,
                      diverse.widow,
                      diverse.spread,
                      diverse.leave,
                      diverse.scheme,
                      diverse.atmosphere,
                      diverse.moral show
               FROM diverse
              ) openly
   CROSS JOIN (SELECT shore.eye inspection
               FROM (SELECT plus.beam eye
                     FROM (SELECT funeral.beam
                           FROM (SELECT weak.portrait beam
                                 FROM (SELECT physics.organism portrait
                                       FROM (SELECT doorway.alive organism
                                             FROM (SELECT account.injury ancient,
                                                          account.prediction,
                                                          account.alive,
                                                          account.elect,
                                                          account.regret
                                                   FROM account
                                                  ) doorway
                                            ) physics
                                      ) weak
                                ) funeral
                          ) plus
                    ) shore
              ) missionary ;

''',
'''
SELECT businessman.inquiry
FROM            (SELECT technical.minute inquiry
                 FROM (SELECT encounter.frozen ,
                            encounter.while once,
                            encounter.minute,
                            encounter.please wonder
                     FROM (SELECT Arab.weak frozen,
                                  stress.socially,
                                  stress.attention,
                                  Arab.while,
                                  stress.invent,
                                  stress.minute,
                                  Arab.theme please
                           FROM         Arab
                           CROSS JOIN   (SELECT   total.socially,
                                                  total.attention,
                                                  total.invent,
                                                  total.guarantee minute
                                           FROM (SELECT auto.attention,
                                                        auto.recognition,
                                                        auto.guarantee,
                                                        auto.invent,
                                                        auto.inspiration socially
                                                 FROM auto
                                                ) total
                                          ) stress
                          ) encounter
                    ) technical
              ) businessman
   CROSS JOIN (SELECT data.seminar,
                      data.home,
                      data.border,
                      data.seat
               FROM data
              ) thick

''',
'''
SELECT more.needle
FROM          tuck
   CROSS JOIN (SELECT stuff.needle,
                      stuff.wheel trial
               FROM (SELECT common.fool search,
                            common.discovery needle,
                            model.kit wheel
                     FROM          (SELECT activity.dinner classroom,
                                           activity.joint shadow,
                                           activity.beat fool,
                                           activity.supplier inquiry,
                                           activity.university discovery
                                    FROM activity
                                   ) common
                        CROSS JOIN (SELECT boast.kit
                                    FROM (SELECT language.kit,
                                                 language.hat negotiate,
                                                 language.blink
                                          FROM (SELECT host.trash,
                                                       host.blink,
                                                       host.voluntary,
                                                       host.kit,
                                                       host.war hat
                                                FROM (SELECT path.draft kit,
                                                             path.territory war,
                                                             path.confession blink,
                                                             path.voluntary,
                                                             path.suicide trash
                                                      FROM path
                                                     ) host
                                               ) language
                                         ) boast
                                   ) model
                    ) stuff
              ) more

''',
'''
SELECT conversion.crucial coffee
FROM          (SELECT boundary.this,
                      weight.item son,
                      gain.put temporary,
                      boundary.before farmer,
                      gain.depart,
                      gain.famous welcome,
                      gain.acid,
                      weight.irony jeans
               FROM          weight
                  CROSS JOIN (SELECT public.acid,
                                     crowd.depart,
                                     crowd.selection tie,
                                     crowd.prompt wake,
                                     touch.put,
                                     touch.household,
                                     touch.total famous,
                                     crowd.adventure
                              FROM          (SELECT historic.invade acid
                                             FROM (SELECT ice.invade
                                                   FROM          ice
                                                      CROSS JOIN arise
                                                  ) historic
                                            ) public
                                 CROSS JOIN touch
                                 CROSS JOIN (SELECT effectiveness.depart,
                                                    effectiveness.mandate slope,
                                                    effectiveness.at selection,
                                                    effectiveness.bulk,
                                                    effectiveness.prompt,
                                                    effectiveness.excited adventure
                                             FROM (SELECT best.roll,
                                                          best.bulk,
                                                          best.prompt,
                                                          best.at,
                                                          best.amid depart,
                                                          best.mandate,
                                                          best.telescope via,
                                                          best.rider excited
                                                   FROM best
                                                  ) effectiveness
                                            ) crowd
                             ) gain
                  CROSS JOIN boundary
              ) welfare
   CROSS JOIN (SELECT out.now,
                      out.clinical tomato,
                      out.table elephant,
                      out.take reportedly
               FROM out
              ) swim
   CROSS JOIN (SELECT ad.especially reverse,
                      ad.vanish crucial,
                      ad.freedom diary
               FROM (SELECT territory.shape distribution,
                            territory.parade,
                            territory.especially,
                            territory.freedom,
                            territory.personally stake,
                            territory.grasp,
                            territory.uncle vanish,
                            territory.ignore swimming
                     FROM territory
                    ) ad
              ) conversion

''',
'''
SELECT pack.senior correctly
FROM          elevator
   CROSS JOIN shareholder
   CROSS JOIN pack
   CROSS JOIN auction
   CROSS JOIN (SELECT area.carve entity,
                      area.clock southern,
                      link.valley,
                      link.found changing,
                      area.found,
                      rape.justice,
                      rape.vision,
                      area.shorts
               FROM          (SELECT pray.reliable evaluate,
                                     pray.vision,
                                     pray.fly,
                                     pray.course adviser,
                                     pray.decade,
                                     pray.justice
                              FROM pray
                             ) rape
                  CROSS JOIN area
                  CROSS JOIN link
              ) green
   CROSS JOIN fundamental
   CROSS JOIN (SELECT thereby.trigger specialize
               FROM (SELECT do.disturbing trigger
                     FROM (SELECT there.guard disturbing
                           FROM there
                          ) do
                    ) thereby
              ) surface
   CROSS JOIN (SELECT junior.undermine,
                      junior.exploration monitor
               FROM junior
              ) skull
   CROSS JOIN easy

''',
'''
SELECT liquid.columnist
FROM          (SELECT flying.British,
                      rural.boyfriend recipient
               FROM          rural
                  CROSS JOIN (SELECT nest.web advertising,
                                     nest.face timber,
                                     nest.matter,
                                     nest.resign several,
                                     nest.regardless,
                                     nest.legal
                              FROM nest
                             ) grocery
                  CROSS JOIN (SELECT chase.discover,
                                     chase.corruption,
                                     chase.fault British,
                                     chase.think,
                                     chase.sweet
                              FROM (SELECT affect.religion corruption,
                                           affect.handle think,
                                           affect.contribution fault,
                                           affect.orange discover,
                                           affect.center sweet
                                    FROM affect
                                   ) chase
                             ) flying
              ) signature
   CROSS JOIN (SELECT note.photograph onto,
                      note.columnist,
                      note.exchange obtain,
                      note.barrier sport,
                      note.owe radio,
                      note.clip worldwide,
                      note.museum
               FROM note
              ) liquid ;

''',
'''
SELECT clip.discover manage
FROM          compelling
   CROSS JOIN leave
   CROSS JOIN (SELECT lack.joy driver,
                      costly.spectrum discover
               FROM          (SELECT enterprise.spit,
                                     enterprise.reward
                              FROM          metal
                                 CROSS JOIN enterprise
                                 CROSS JOIN (SELECT visit.who,
                                                    visit.minor,
                                                    visit.bronze,
                                                    visit.vanish strip,
                                                    visit.gesture explosion,
                                                    visit.realm,
                                                    visit.amazing
                                             FROM visit
                                            ) joint
                                 CROSS JOIN (SELECT circle.award note,
                                                    circle.click
                                             FROM (SELECT sneak.army click,
                                                          sneak.English,
                                                          sneak.university collapse,
                                                          sneak.forehead,
                                                          sneak.unity lower,
                                                          sneak.vaccine,
                                                          sneak.appropriate award
                                                   FROM sneak
                                                  ) circle
                                            ) shock
                             ) continent
                  CROSS JOIN (SELECT longtime.struggle,
                                     longtime.implication seal,
                                     longtime.provider,
                                     longtime.reward,
                                     longtime.radar,
                                     longtime.enforce,
                                     medium.face
                              FROM          (SELECT get.reward,
                                                    get.black conclusion,
                                                    get.honor enforce,
                                                    get.him provider,
                                                    get.struggle,
                                                    get.implication,
                                                    get.radar
                                             FROM get
                                            ) longtime
                                 CROSS JOIN (SELECT voter.face
                                             FROM (SELECT naturally.face
                                                   FROM (SELECT highly.crash face
                                                         FROM highly
                                                        ) naturally
                                                  ) voter
                                            ) medium
                             ) deal
                  CROSS JOIN costly
                  CROSS JOIN lack
              ) clip
   CROSS JOIN (SELECT chamber.missionary could,
                      chamber.mobile,
                      chamber.plastic parent,
                      chamber.tone anyone
               FROM (SELECT uniform.bath sanction,
                            uniform.weakness upset,
                            uniform.psychological,
                            uniform.missionary,
                            uniform.array tone,
                            uniform.mobile,
                            uniform.prediction plastic
                     FROM uniform
                    ) chamber
              ) hazard

''',
'''
SELECT TV.bounce
FROM          (SELECT need.core,
                      die.bounce,
                      wealth.abc
               FROM          need
                  CROSS JOIN tag
                  CROSS JOIN (SELECT noon.conservative,
                                     noon.pattern AS bounce
                              FROM (SELECT marketing.costly,
                                          marketing.plus conservative,
                                          dangit.bounce AS pattern,
                                          marketing.questionnaire
                                   FROM            (SELECT expect.stuff income,
                                                            expect.deer costly,
                                                            expect.joke plus,
                                                            expect.questionnaire
                                                     FROM (SELECT film.empty teaspoon,
                                                                  film.offensive joke,
                                                                  film.stuff,
                                                                  film.resistance deer,
                                                                  film.questionnaire
                                                           FROM film
                                                          ) expect
                                                   ) marketing
                                       CROSS JOIN dangit
                                  ) noon
                             ) die
                  CROSS JOIN (SELECT tablespoon.abc,
                                     tablespoon.cde,
                                     tablespoon.efg
                              FROM            tablespoon
                                   CROSS JOIN nothin
                             ) wealth
              ) TV
   CROSS JOIN kill

''',
'''
SELECT father.landscape
FROM          father
   CROSS JOIN (SELECT southern.opponent,
                      southern.excitement element,
                      wrong.shirt article,
                      wrong.outdoor screen,
                      southern.hello their,
                      southern.suppose,
                      southern.American,
                      wrong.lucky
               FROM          (SELECT problem.American,
                                     ship.far opponent,
                                     ship.hello,
                                     ship.suppose,
                                     ship.practical study,
                                     dynamics.regime excitement
                              FROM          dynamics
                                 CROSS JOIN (SELECT democracy.negotiation,
                                                    democracy.Persian practical,
                                                    democracy.prohibit animal,
                                                    democracy.American command,
                                                    democracy.hello,
                                                    democracy.out suppose,
                                                    democracy.heavy hang,
                                                    democracy.far
                                             FROM democracy
                                            ) ship
                                 CROSS JOIN problem
                             ) southern
                  CROSS JOIN (SELECT DNA.spread lucky,
                                     DNA.frankly shirt,
                                     DNA.museum,
                                     DNA.outdoor,
                                     DNA.feature,
                                     DNA.satisfy
                              FROM (SELECT attention.frankly,
                                           image.spread,
                                           attention.feature,
                                           attention.recording satisfy,
                                           attention.opposite museum,
                                           image.second relative,
                                           attention.honey outdoor
                                    FROM          attention
                                       CROSS JOIN (SELECT still.vegetable spread,
                                                          still.collapse second
                                                   FROM (SELECT realistic.vegetable,
                                                                realistic.construct collapse
                                                         FROM (SELECT can.episode vegetable,
                                                                      can.construct
                                                               FROM can
                                                              ) realistic
                                                        ) still
                                                  ) image
                                   ) DNA
                             ) wrong
              ) midst
   CROSS JOIN (SELECT impose.behind,
                      impose.heel,
                      impose.edition,
                      impose.southeast reliability,
                      impose.afternoon industrial,
                      impose.wind danger
               FROM impose
              ) regard
   CROSS JOIN (SELECT migration.climate power
               FROM migration
              ) promise
   CROSS JOIN (SELECT panel.food,
                      panel.manufacturing
               FROM (SELECT coach.currently reluctant,
                            coach.barely manufacturing,
                            coach.please notebook,
                            coach.standard food,
                            coach.travel section
                     FROM coach
                    ) panel
              ) pink ;

''',
'''
SELECT elegant.gun eager
FROM          (SELECT evident.living,
                      seriously.spoon,
                      dish.successful control,
                      seriously.amid gun,
                      dish.monthly slow,
                      evident.pour,
                      evident.institution,
                      seriously.faith highway
               FROM          (SELECT tooth.ordinary concerning,
                                     tooth.spoon,
                                     tooth.controversy identification,
                                     tooth.forth faith,
                                     tooth.garage,
                                     tooth.disappear,
                                     tooth.amid
                              FROM tooth
                             ) seriously
                  CROSS JOIN (SELECT captain.far,
                                     captain.processor stair,
                                     captain.rating,
                                     captain.pour,
                                     captain.regular living,
                                     captain.file speculation,
                                     captain.evil institution,
                                     captain.counsel
                              FROM captain
                             ) evident
                  CROSS JOIN dish
              ) elegant
   CROSS JOIN (SELECT Israeli.container
               FROM (SELECT ambitious.female container
                     FROM (SELECT plant.revolution operator,
                                  plant.female
                           FROM (SELECT sweater.revolution,
                                        sweater.inmate female
                                 FROM (SELECT light.dealer familiar,
                                              light.winner inmate,
                                              light.bit revolution
                                       FROM light
                                      ) sweater
                                ) plant
                          ) ambitious
                    ) Israeli
              ) ambitious
   CROSS JOIN combined
   CROSS JOIN (SELECT deer.personally already
               FROM (SELECT swear.personally
                     FROM (SELECT mental.personally
                           FROM (SELECT effect.bride personally
                                 FROM (SELECT silence.bride
                                       FROM (SELECT trail.spring bride,
                                                    trail.mobile,
                                                    trail.invent dictate
                                             FROM (SELECT host.mobile,
                                                          host.spring,
                                                          host.invent
                                                   FROM host
                                                  ) trail
                                            ) silence
                                      ) effect
                                ) mental
                          ) swear
                    ) deer
              ) Internet
   CROSS JOIN convince
   CROSS JOIN (SELECT sale.whether violence
               FROM (SELECT uncover.competition hard,
                            uncover.corruption teaching,
                            uncover.fear grant,
                            uncover.import exact,
                            uncover.alter,
                            uncover.mushroom whether,
                            uncover.perform,
                            uncover.army scramble
                     FROM uncover
                    ) sale
              ) conversation
   CROSS JOIN back
   CROSS JOIN sweet

''',
'''
SELECT exploration.for
FROM          (SELECT originally.accent,
                      originally.district confrontation
               FROM originally
              ) international
   CROSS JOIN rocket
   CROSS JOIN category
   CROSS JOIN (SELECT forty.jeans
               FROM (SELECT psychological.appreciation,
                            psychological.writing,
                            psychological.note jeans,
                            psychological.fur,
                            psychological.salt explain,
                            psychological.harmony react,
                            psychological.constantly insurance,
                            psychological.curtain cell
                     FROM psychological
                    ) forty
              ) Italian
   CROSS JOIN (SELECT several.evaluation drama,
                      several.disability German,
                      several.driveway,
                      several.home swimming,
                      several.journalist
               FROM several
              ) model
   CROSS JOIN (SELECT interesting.preach for,
                      interesting.comment,
                      interesting.biology,
                      interesting.counterpart Korean,
                      interesting.fifteen supposed
               FROM interesting
              ) exploration
   CROSS JOIN (SELECT initial.debate,
                      initial.content map,
                      initial.receiver collective,
                      initial.oral
               FROM (SELECT sometime.chocolate,
                            sometime.receiver,
                            sometime.provision debate,
                            sometime.oral,
                            sometime.content
                     FROM sometime
                    ) initial
              ) custom
   CROSS JOIN (SELECT editor.rape talk
               FROM (SELECT good.rape
                     FROM (SELECT contact.rape
                           FROM (SELECT supply.rape
                                 FROM (SELECT consist.brown rape
                                       FROM (SELECT instructional.brown
                                             FROM (SELECT pie.pasta,
                                                          pie.plea,
                                                          pie.spill brown,
                                                          pie.differ allegedly,
                                                          pie.swing
                                                   FROM pie
                                                  ) instructional
                                            ) consist
                                      ) supply
                                ) contact
                          ) good
                    ) editor
              ) matter
   CROSS JOIN global ;

''',
'''
SELECT home.generate
FROM          (SELECT professor.spread,
                      spring.architecture invade,
                      professor.sixth
               FROM          (SELECT recruit.monitor
                              FROM recruit
                             ) print
                  CROSS JOIN spring
                  CROSS JOIN professor
              ) compare
   CROSS JOIN (SELECT bear.fifth,
                      bear.nobody
               FROM bear
              ) ought

   CROSS JOIN (SELECT exhaust.care direct,
                      exhaust.generate
               FROM (SELECT escape.tomorrow care,
                            escape.sector,
                            escape.generate
                     FROM (SELECT crack.legitimate,
                                  crack.tomorrow,
                                  question.generate,
                                  question.sector
                           FROM             crack
                                CROSS JOIN (SELECT mean.generate,
                                                  mean.sector
                                           FROM (SELECT instrument.generate,
                                                        instrument.democracy sector
                                                 FROM instrument
                                                ) mean
                                          ) question
                          ) escape
                    ) exhaust
              ) home
   CROSS JOIN multiple ;

''',
'''
SELECT must.abc
FROM          (SELECT cheer.stuff bright
               FROM (SELECT smell.demonstration stuff
                     FROM          smell
                        CROSS JOIN opposition
                    ) cheer
              ) possibly
   CROSS JOIN (SELECT million.hunt,
                      million.flow equity,
                      annually.legitimate glass,
                      million.intellectual
               FROM          annually
                  CROSS JOIN (SELECT politically.grass flow,
                                     politically.allegation intellectual,
                                     politically.outside hunt,
                                     politically.branch,
                                     politically.moon,
                                     politically.team documentary,
                                     politically.artist
                              FROM politically
                             ) million
              ) sun
   CROSS JOIN remote
   CROSS JOIN (SELECT absolutely.pair AS abc,
                      ill.drive
               FROM          (SELECT monkey.sharply pair
                              FROM (SELECT pause.sharply
                                    FROM pause
                                   ) monkey
                             ) absolutely
                  CROSS JOIN (SELECT arrive.drive
                              FROM arrive
                             ) ill
              ) must
   CROSS JOIN sell

''',
'''
SELECT increase.enforce
FROM          (SELECT entire.addition,
                      brother.balanced seek,
                      threshold.injury breathe,
                      entire.plant
               FROM          excited
                  CROSS JOIN rape
                  CROSS JOIN (SELECT attitude.know,
                                     attitude.link,
                                     attitude.session,
                                     attitude.request injury,
                                     attitude.virtually
                              FROM attitude
                             ) threshold
                  CROSS JOIN brother
                  CROSS JOIN entire
              ) mostly
   CROSS JOIN orange
   CROSS JOIN (SELECT working.will here,
                      opportunity.trick,
                      working.class theater,
                      working.characteristic enforce,
                      working.compound come,
                      working.concern
               FROM          working
                  CROSS JOIN (SELECT large.much,
                                     large.fatal it,
                                     large.grandfather commodity
                              FROM (SELECT drug.click fatal,
                                           drug.bomb grandfather,
                                           drug.strip readily,
                                           drug.stage,
                                           drug.much,
                                           drug.dismiss intelligence
                                    FROM drug
                                   ) large
                             ) merit
                  CROSS JOIN (SELECT river.trick
                              FROM (SELECT uncover.minute trick
                                    FROM (SELECT depressed.jaw minute
                                          FROM (SELECT sodium.my,
                                                       sodium.flying,
                                                       sodium.craft fool,
                                                       sodium.jaw,
                                                       sodium.fame scary,
                                                       sodium.solve,
                                                       sodium.incorporate,
                                                       sodium.running
                                                FROM sodium
                                               ) depressed
                                         ) uncover
                                   ) river
                             ) opportunity
              ) increase
   CROSS JOIN (SELECT elect.partly sail,
                      elect.painter,
                      elect.duck story,
                      elect.currently,
                      elect.rebuild excited
               FROM (SELECT addition.fly,
                            addition.currently,
                            addition.duck,
                            addition.need rebuild,
                            addition.partly,
                            addition.painter
                     FROM addition
                    ) elect
              ) single

''',
'''
SELECT initiate.candle
FROM          initiate
   CROSS JOIN (SELECT ie.format doorway,
                      effective.atop,
                      effective.ironically improvement,
                      ie.born feature,
                      effective.result sea
               FROM          ie
                  CROSS JOIN effective
              ) tomorrow
   CROSS JOIN conception
   CROSS JOIN vote
   CROSS JOIN (SELECT breast.thank near,
                      breast.coin shoulder,
                      breast.secure,
                      breast.optimistic cooking
               FROM (SELECT testify.thank,
                            testify.barn secure,
                            testify.wonder coin,
                            testify.dictate optimistic
                     FROM (SELECT very.bench dictate,
                                  very.past thank,
                                  very.assess barn,
                                  very.midst,
                                  very.wonder,
                                  very.fur
                           FROM very
                          ) testify
                    ) breast
              ) pizza
   CROSS JOIN (SELECT severely.quote future,
                      severely.belt,
                      severely.hopefully return
               FROM (SELECT circumstance.toilet,
                            circumstance.quote,
                            circumstance.proud,
                            circumstance.hopefully,
                            circumstance.enable description,
                            circumstance.complexity,
                            outstanding.wave belt,
                            circumstance.towel
                     FROM          circumstance
                        CROSS JOIN (SELECT installation.investor wave
                                    FROM (SELECT area.dirt investor,
                                                 area.Arab tear,
                                                 area.rescue
                                          FROM (SELECT emerging.dirt,
                                                       emerging.weight rescue,
                                                       emerging.target,
                                                       emerging.pack holy,
                                                       emerging.bike,
                                                       emerging.aid,
                                                       emerging.march Arab,
                                                       emerging.norm
                                                FROM emerging
                                               ) area
                                         ) installation
                                   ) outstanding
                    ) severely
              ) agricultural
   CROSS JOIN (SELECT sort.president coordinate,
                      sort.weakness religion,
                      sort.exclusively construct,
                      sort.hill,
                      sort.pressure,
                      sort.associate
               FROM sort
              ) arrive

''',
'''
SELECT quarterback.ignore
FROM          (SELECT severe.French,
                      vary.pleased,
                      severe.survey decrease
               FROM          (SELECT silent.frankly political,
                                     silent.pleased,
                                     silent.partner estate,
                                     silent.absence,
                                     silent.worldwide balance
                              FROM (SELECT apartment.pleased,
                                           future.agent,
                                           apartment.stop partner,
                                           future.worldwide,
                                           future.absence,
                                           fly.frankly
                                    FROM          (SELECT representative.pleased,
                                                          standard.reliable achievement,
                                                          representative.win inside,
                                                          representative.physically,
                                                          standard.gaze stop,
                                                          standard.water,
                                                          representative.fat
                                                   FROM          standard
                                                      CROSS JOIN (SELECT bring.maximum fat,
                                                                         bring.patch pleased,
                                                                         bring.participate,
                                                                         bring.win,
                                                                         bring.easily,
                                                                         bring.physically
                                                                  FROM bring
                                                                 ) representative
                                                  ) apartment
                                       CROSS JOIN (SELECT overall.success trash,
                                                          overall.evolve frankly,
                                                          overall.accomplishment stick,
                                                          western.consultant researcher
                                                   FROM          overall
                                                      CROSS JOIN (SELECT relevant.tension consultant
                                                                  FROM (SELECT modern.tension
                                                                        FROM (SELECT enforce.tension
                                                                              FROM (SELECT disabled.porch diet,
                                                                                           disabled.shake tension,
                                                                                           disabled.galaxy,
                                                                                           disabled.mechanical
                                                                                    FROM disabled
                                                                                   ) enforce
                                                                             ) modern
                                                                       ) relevant
                                                                 ) western
                                                  ) fly
                                       CROSS JOIN future
                                   ) silent
                             ) vary
                  CROSS JOIN implementation
                  CROSS JOIN (SELECT surrounding.dust bucket,
                                     surrounding.lift,
                                     surrounding.home plan,
                                     surrounding.survey,
                                     surrounding.French,
                                     surrounding.shower damn,
                                     surrounding.teenager,
                                     surrounding.emission divorce
                              FROM surrounding
                             ) severe
                  CROSS JOIN relative
                  CROSS JOIN fly
              ) chill
   CROSS JOIN quarterback

''',
'''
SELECT neat.blood
FROM          (SELECT reader.actor drink,
                      reader.search capable,
                      reader.just division,
                      aggression.regulation,
                      aggression.credit,
                      aggression.pollution outside,
                      aggression.foot pursuit
               FROM          aggression
                  CROSS JOIN reader
              ) death
   CROSS JOIN (SELECT okay.destroy,
                      okay.vessel,
                      okay.outcome replace,
                      okay.expectation shut,
                      okay.no,
                      okay.ago computer
               FROM okay
              ) launch
   CROSS JOIN nuclear
   CROSS JOIN (SELECT cook.challenge
               FROM (SELECT flee.mistake,
                            flee.short,
                            flee.customer still,
                            flee.video incredibly,
                            flee.speaker challenge,
                            flee.crowd,
                            flee.remind,
                            flee.effort
                     FROM flee
                    ) cook
              ) member
   CROSS JOIN (SELECT rank.instantly hard
               FROM (SELECT replace.sort instantly
                     FROM (SELECT electrical.commercial sort
                           FROM electrical
                          ) replace
                    ) rank
              ) stretch
   CROSS JOIN (SELECT train.afterward grandchild,
                      train.state,
                      train.preparation
               FROM (SELECT during.state,
                            during.classical preparation,
                            during.profile afterward,
                            during.professor
                     FROM (SELECT garage.asleep profile,
                                  garage.twentieth state,
                                  garage.black professor,
                                  garage.core classical
                           FROM garage
                          ) during
                    ) train
              ) treatment
   CROSS JOIN (SELECT clearly.average wear,
                      clearly.coverage contribution
               FROM clearly
              ) suspend
   CROSS JOIN (SELECT needle.chapter retain,
                      needle.concert figure,
                      needle.city depression,
                      needle.proposal,
                      needle.transform sunny
               FROM (SELECT brake.transform,
                            brake.car chapter,
                            brake.proposal,
                            brake.cloth city,
                            brake.activity concert
                     FROM brake
                    ) needle
              ) minimum
   CROSS JOIN (SELECT maybe.pitch,
                      maybe.bombing afternoon,
                      maybe.downtown functional,
                      maybe.prison,
                      maybe.write diplomatic,
                      maybe.loop known
               FROM maybe
              ) debut
   CROSS JOIN neat ;

''',
'''
SELECT every.personally
FROM          (SELECT excuse.gradually,
                      excuse.principal
               FROM excuse
              ) clear
   CROSS JOIN (SELECT prove.consider,
                      prove.be thin,
                      prove.easy attraction,
                      prove.estate,
                      prove.cartoon
               FROM prove
              ) strike
   CROSS JOIN examine
   CROSS JOIN man
   CROSS JOIN newly
   CROSS JOIN (SELECT basis.personally
               FROM basis
              ) every
   CROSS JOIN (SELECT suspend.theological,
                      suspend.deliberately,
                      suspend.wing,
                      suspend.burden his
               FROM suspend
              ) altogether
   CROSS JOIN (SELECT profit.deeply technique,
                      profit.riot calendar,
                      profit.firmly
               FROM (SELECT emotion.ranch deeply,
                            emotion.knock weaken,
                            emotion.provoke firmly,
                            emotion.senior riot,
                            emotion.greatly
                     FROM emotion
                    ) profit
              ) classify

''',
'''
SELECT vast.insert
FROM          (SELECT ought.widow,
                      strip.post,
                      strip.lifestyle maintain,
                      strip.delivery,
                      strip.demonstrate associated,
                      strip.ethics,
                      ought.population
               FROM          strip
                  CROSS JOIN ought
              ) license
   CROSS JOIN (SELECT sudden.retire employ,
                      sudden.friendly remove,
                      unusual.citizen experimental,
                      sudden.reminder,
                      sudden.journalist boyfriend,
                      sudden.issue,
                      unusual.weakness permit
               FROM          sudden
                  CROSS JOIN unusual
              ) nurse
   CROSS JOIN (SELECT park.late valley,
                      park.insert,
                      park.mortgage,
                      park.addition senator
               FROM park
              ) vast
   CROSS JOIN (SELECT argument.cookie,
                      argument.pop restaurant
               FROM (SELECT artistic.institutional cookie,
                            artistic.desperate,
                            artistic.pop
                     FROM (SELECT lots.according pop,
                                  lots.straight desperate,
                                  lots.page priest,
                                  lots.institutional
                           FROM lots
                          ) artistic
                    ) argument
              ) poverty
   CROSS JOIN (SELECT attendance.heaven,
                      attendance.challenge,
                      attendance.either sheer,
                      attendance.green bright,
                      attendance.happen disappear,
                      attendance.less,
                      attendance.unfair Dutch,
                      attendance.simple witness
               FROM attendance
              ) combined
   CROSS JOIN trail
   CROSS JOIN (SELECT eye.bowl,
                      eye.foreign oh,
                      eye.fit advise,
                      eye.mount,
                      eye.challenge
               FROM eye
              ) title
   CROSS JOIN (SELECT gasoline.sauce besides,
                      gasoline.four type
               FROM gasoline
              ) thigh
   CROSS JOIN (SELECT plastic.grass,
                      plastic.painting,
                      plastic.marriage
               FROM plastic
              ) valuable ;

''',
'''
SELECT defense.cde
FROM          (SELECT murder.somewhat,
                      murder.abc cde
               FROM (SELECT system.somewhat,
                            system.jojo abc
                     FROM (SELECT bull.somewhat,
                                  ring.jojo
                           FROM             ring
                                CROSS JOIN (SELECT fire.somewhat
                                 FROM (SELECT premise.province somewhat,
                                              premise.sugar,
                                              premise.liquid beach
                                       FROM (SELECT terrorist.interpretation,
                                                    terrorist.satisfy hopefully,
                                                    terrorist.sugar,
                                                    terrorist.agriculture liquid,
                                                    terrorist.province
                                             FROM terrorist
                                            ) premise
                                      ) fire
                                ) bull
                          ) system
                    ) murder
              ) defense
   CROSS JOIN (SELECT effort.bulk principal,
                      effort.biology contemplate,
                      effort.continue cage
               FROM (SELECT broker.bulk,
                            broker.eventually reduction,
                            broker.biology,
                            broker.compete runner,
                            broker.continue
                     FROM broker
                    ) effort
              ) dignity

''',
'''
SELECT restaurant.English
FROM          terrain

   CROSS JOIN (SELECT result.protest indigenous,
                      result.English
               FROM (SELECT resume.protest,
                            top.English,
                            top.draft
                     FROM               (SELECT poem.Soviet protest,
                                                poem.income
                                         FROM poem
                                        ) resume
                             CROSS JOIN online
                             CROSS JOIN shed
                             CROSS JOIN (SELECT stress.label legislature,
                                                globe.clinic,
                                                stress.grab ,
                                                globe.valley English,
                                                stress.separate,
                                                globe.trash,
                                                stress.rely draft,
                                                globe.among
                                         FROM               stress
                                                CROSS JOIN (SELECT bean.both valley,
                                                                  recording.soup clinic,
                                                                  recording.among,
                                                                  recording.trash
                                                           FROM          recording
                                                              CROSS JOIN (SELECT charge.both
                                                                          FROM charge
                                                                         ) bean
                                                          ) globe
                                        ) top
                    ) result
              ) restaurant
   CROSS JOIN wood

''',
'''
SELECT advocate.senior
FROM          (SELECT smart.blue,
                      smart.root,
                      bend.exact,
                      scared.stare sick,
                      speaker.equity divine,
                      smart.supervisor wealthy,
                      speaker.drug
               FROM          (SELECT supermarket.exact,
                                     supermarket.rim discovery
                              FROM (SELECT pole.rim,
                                           pole.violate exact
                                    FROM (SELECT it.implement rim,
                                                 it.terrible violate,
                                                 it.belief
                                          FROM it
                                         ) pole
                                   ) supermarket
                             ) bend
                  CROSS JOIN (SELECT hit.arrest,
                                     hit.bulk mathematics
                              FROM (SELECT please.challenge bulk,
                                           please.lean arrest
                                    FROM please
                                   ) hit
                             ) auto
                  CROSS JOIN speaker
                  CROSS JOIN (SELECT financial.stare,
                                     financial.electronics restrict,
                                     financial.trouble outcome
                              FROM (SELECT Ms.Jew,
                                           Ms.stare,
                                           Ms.trouble,
                                           Ms.electronics
                                    FROM (SELECT wake.trouble,
                                                 wake.era electronics,
                                                 wake.stare,
                                                 wake.instructional slope,
                                                 wake.coming Jew
                                          FROM wake
                                         ) Ms
                                   ) financial
                             ) scared
                  CROSS JOIN smart
              ) spare
   CROSS JOIN (SELECT lung.senior
               FROM (SELECT abstract.senior
                     FROM (SELECT cottage.real technique,
                                  cottage.inside senior,
                                  cottage.compete
                           FROM (SELECT majority.inside,
                                        majority.crack concede,
                                        majority.compete,
                                        majority.real,
                                        majority.snake,
                                        majority.they
                                 FROM (SELECT autonomy.this they,
                                              autonomy.lung crack,
                                              autonomy.assault inside,
                                              autonomy.charm far,
                                              autonomy.split snake,
                                              autonomy.favor real,
                                              autonomy.managing compete
                                       FROM autonomy
                                      ) majority
                                ) cottage
                          ) abstract
                    ) lung
              ) advocate
   CROSS JOIN nobody ;

''',
'''
SELECT required.giant
FROM          (SELECT shy.soft describe,
                      shy.positive weather,
                      shy.pain,
                      shy.exercise forgive
               FROM (SELECT price.inspector positive,
                            price.regular girl,
                            price.regain,
                            price.nine pain,
                            price.exercise,
                            price.soft
                     FROM (SELECT there.nine,
                                  there.forth regain,
                                  there.widely cook,
                                  there.exercise,
                                  there.proof inspector,
                                  there.soft,
                                  there.regarding regular
                           FROM there
                          ) price
                    ) shy
              ) hit
   CROSS JOIN (SELECT discrimination.refuse,
                      discrimination.offer fraud,
                      discrimination.responsible establishment
               FROM (SELECT inventory.silent responsible,
                            inventory.complete offer,
                            inventory.tie refuse
                     FROM inventory
                    ) discrimination
              ) birth
   CROSS JOIN (SELECT top.celebrity
               FROM (SELECT colorful.celebrity
                     FROM (SELECT suburb.celebrity
                           FROM (SELECT plastic.decorate celebrity
                                 FROM (SELECT place.elite,
                                              place.throw ha,
                                              place.so approximately,
                                              place.obligation,
                                              place.major housing,
                                              place.precisely,
                                              place.partially decorate,
                                              place.vaccine again
                                       FROM place
                                      ) plastic
                                ) suburb
                          ) colorful
                    ) top
              ) trust
   CROSS JOIN (SELECT invasion.belief,
                      invasion.giant
               FROM (SELECT foot.belief,
                            foot.giant,
                            foot.session flag
                     FROM foot
                    ) invasion
              ) required
   CROSS JOIN (SELECT remember.buyer
               FROM (SELECT member.nominee buyer,
                            member.soap
                     FROM (SELECT compete.availability nominee,
                                  compete.productivity near,
                                  compete.violent Christianity,
                                  compete.criticism government,
                                  compete.leaf soap
                           FROM compete
                          ) member
                    ) remember
              ) exposure
   CROSS JOIN worry ;

''',
'''
SELECT satisfaction.become
FROM          finding
   CROSS JOIN pocket
   CROSS JOIN (SELECT outline.nod remote
               FROM          (SELECT sentiment.suburb
                              FROM          (SELECT regulator.process notion,
                                                    regulator.addition keep,
                                                    regulator.figure complexity
                                             FROM (SELECT file.attraction figure,
                                                          file.process,
                                                          file.addition
                                                   FROM file
                                                  ) regulator
                                            ) protest
                                 CROSS JOIN (SELECT disaster.explode defendant,
                                                    disaster.suburb
                                             FROM (SELECT work.utilize,
                                                          work.explode,
                                                          work.suburb,
                                                          work.mean,
                                                          work.pit
                                                   FROM work
                                                  ) disaster
                                            ) sentiment
                             ) hardware
                  CROSS JOIN outline
              ) pipe
   CROSS JOIN start
   CROSS JOIN (SELECT practical.become
               FROM practical
              ) satisfaction
   CROSS JOIN spread
   CROSS JOIN classify
   CROSS JOIN benefit
   CROSS JOIN carry

''',
'''
SELECT speculate.juror belly
FROM          (SELECT threaten.implication,
                      innovation.monitor
               FROM          (SELECT logic.suffering monitor,
                                     logic.structural
                              FROM          separate
                                 CROSS JOIN logic
                                 CROSS JOIN (SELECT harvest.ball fit,
                                                    harvest.adapt,
                                                    recover.here breath,
                                                    yes.common,
                                                    filter.bombing,
                                                    harvest.scratch sad
                                             FROM          (SELECT thing.recording squad,
                                                                   thing.civilian twice,
                                                                   thing.here
                                                            FROM thing
                                                           ) recover
                                                CROSS JOIN harvest
                                                CROSS JOIN (SELECT helmet.common
                                                            FROM (SELECT commodity.stop common
                                                                  FROM (SELECT fundamental.barely stop
                                                                        FROM fundamental
                                                                       ) commodity
                                                                 ) helmet
                                                           ) yes
                                                CROSS JOIN (SELECT importance.agent,
                                                                   importance.ordinary,
                                                                   importance.vessel,
                                                                   importance.bombing,
                                                                   importance.gentle quote,
                                                                   importance.every,
                                                                   importance.aggressive nobody
                                                            FROM (SELECT peel.think every,
                                                                         peel.agent,
                                                                         peel.trigger gentle,
                                                                         peel.spite license,
                                                                         peel.snake vessel,
                                                                         peel.bombing,
                                                                         peel.scale aggressive,
                                                                         peel.ordinary
                                                                  FROM peel
                                                                 ) importance
                                                           ) filter
                                            ) tea
                             ) innovation
                  CROSS JOIN (SELECT paint.use most,
                                     paint.civil trading,
                                     paint.least coffee,
                                     paint.negative speak,
                                     paint.certain,
                                     paint.dried,
                                     paint.attack onto
                              FROM paint
                             ) cheer
                  CROSS JOIN threaten
                  CROSS JOIN bill
              ) pencil
   CROSS JOIN (SELECT official.strain,
                      official.juror,
                      official.opening,
                      official.encouraging feedback
               FROM (SELECT psychologist.pile juror,
                            psychologist.encouraging,
                            psychologist.opening,
                            psychologist.average fund,
                            psychologist.strain
                     FROM psychologist
                    ) official
              ) speculate ;

''',
'''
SELECT mass.strictly success
FROM          (SELECT objective.mate strictly
               FROM (SELECT tape.moderate mate,
                            tape.desperate
                     FROM (SELECT practical.moderate,
                                  practical.favor ingredient,
                                  practical.line desperate
                           FROM (SELECT poverty.dream,
                                        poverty.moderate,
                                        poverty.doubt carve,
                                        poverty.line,
                                        poverty.classic favor
                                 FROM poverty
                                ) practical
                          ) tape
                    ) objective
              ) mass
   CROSS JOIN (SELECT favor.jury reserve,
                      favor.drop,
                      favor.bowl,
                      favor.mask
               FROM favor
              ) distribution
   CROSS JOIN (SELECT middle.online other,
                      middle.helicopter strip,
                      middle.plenty substantial
               FROM (SELECT extent.own deliver,
                            extent.return,
                            extent.journal plenty,
                            extent.monument,
                            extent.concentrate helicopter,
                            extent.institutional,
                            extent.competitor,
                            extent.procedure online
                     FROM extent
                    ) middle
              ) slot

''',
'''
SELECT extreme.nonetheless
FROM          (SELECT Arab.moment duck,
                      reduce.mixture,
                      reduce.good toy,
                      reduce.compromise,
                      reduce.privacy whose,
                      reduce.square,
                      reduce.existing
               FROM          (SELECT language.compromise,
                                     language.play good,
                                     language.square,
                                     spin.refugee privacy,
                                     language.existing,
                                     language.prevention,
                                     language.mixture
                              FROM          (SELECT way.compromise,
                                                    way.scratch existing,
                                                    way.flash,
                                                    way.doubt square,
                                                    way.mixture,
                                                    way.starting prevention,
                                                    way.plan play,
                                                    way.best itself
                                             FROM way
                                            ) language
                                 CROSS JOIN (SELECT gas.trigger scenario,
                                                    gas.doorway,
                                                    gas.racism refugee
                                             FROM gas
                                            ) spin
                             ) reduce
                  CROSS JOIN (SELECT innovation.scientific moment
                              FROM          (SELECT plant.Jewish,
                                                    plant.escape exercise
                                             FROM          communicate
                                                CROSS JOIN (SELECT literally.civic escape,
                                                                   literally.Jewish
                                                            FROM (SELECT preliminary.sacrifice boyfriend,
                                                                         preliminary.feel furthermore,
                                                                         preliminary.weed Jewish,
                                                                         preliminary.scared benefit,
                                                                         preliminary.town,
                                                                         preliminary.culture friendly,
                                                                         preliminary.criteria civic
                                                                  FROM preliminary
                                                                 ) literally
                                                           ) plant
                                            ) provoke
                                 CROSS JOIN (SELECT global.confrontation difficulty,
                                                    global.low break
                                             FROM (SELECT robot.ridiculous investor,
                                                          robot.exposure confrontation,
                                                          robot.low
                                                   FROM (SELECT spill.last,
                                                                spill.literally exposure,
                                                                spill.survivor ahead,
                                                                spill.base ridiculous,
                                                                spill.leap,
                                                                spill.beyond confess,
                                                                spill.adult board,
                                                                spill.low
                                                         FROM spill
                                                        ) robot
                                                  ) global
                                            ) disability
                                 CROSS JOIN (SELECT steam.onto
                                             FROM (SELECT interview.onto
                                                   FROM interview
                                                  ) steam
                                            ) intimate
                                 CROSS JOIN innovation
                             ) Arab
              ) bus
   CROSS JOIN extreme ;

''',
'''
SELECT manufacturer.generate
FROM (SELECT hearing.govern young,
             service.generate,
             service.arrow,
             service.series,
             service.meantime,
             quote.well,
             hearing.dumb
      FROM          hearing
         CROSS JOIN (SELECT sheep.well
                     FROM (SELECT the.corridor well
                           FROM (SELECT set.oil corridor,
                                        set.producer
                                 FROM (SELECT focus.oil,
                                              focus.producer
                                       FROM focus
                                      ) set
                                ) the
                          ) sheep
                    ) quote

         CROSS JOIN (SELECT tuck.generate,
                             finance.perhaps arrow,
                             bow.series,
                             bow.meantime,
                              bow.breathing

                     FROM (SELECT apartment.generate
                           FROM apartment
                          ) tuck
                          CROSS JOIN gentle
                           CROSS JOIN finance
                           CROSS JOIN run
                           CROSS JOIN bow
                    ) service
         CROSS JOIN no
     ) manufacturer ;

''',
'''
SELECT regard.circumstance
FROM          (SELECT close.bug call,
                      close.dose guide,
                      close.seminar viewer,
                      close.out,
                      close.available,
                      close.northern automatic,
                      close.map ship,
                      close.throughout
               FROM close
              ) harmony
   CROSS JOIN (SELECT tribe.circumstance
               FROM (SELECT gap.circumstance
                     FROM (SELECT ever.recently,
                                  forever.circumstance
                           FROM             (SELECT simply.recently,
                                                    simply.if
                                             FROM (SELECT fact.under,
                                                          fact.recently,
                                                          fact.if,
                                                          fact.naval,
                                                          fact.developmental
                                                   FROM (SELECT undergraduate.angel naval,
                                                                undergraduate.wrist if,
                                                                undergraduate.recently,
                                                                undergraduate.ship developmental,
                                                                undergraduate.under
                                                         FROM undergraduate
                                                        ) fact
                                                  ) simply
                                            ) ever
                                CROSS JOIN  forever
                          ) gap
                    ) tribe
              ) regard

''',
'''
SELECT inevitable.initiative
FROM          (SELECT ad.initiative,
                      interpret.everyday clearly,
                      ad.abstract trainer
               FROM          interpret
                  CROSS JOIN enemy
                  CROSS JOIN (SELECT deposit.negotiation smooth,
                                     deposit.governor initiative,
                                     deposit.abstract
                              FROM (SELECT insight.legally negotiation,
                                           insight.abstract,
                                           light.governor,
                                           insight.resident lower,
                                           insight.bathroom
                                    FROM        insight
                                    CROSS JOIN  (SELECT rarely.quarterback governor,
                                                        rarely.fault,
                                                        rarely.survey
                                                 FROM (SELECT well.fault,
                                                              well.subsidy ,
                                                              well.number,
                                                              well.survey,
                                                              well.quarterback
                                                       FROM well
                                                      ) rarely
                                                ) light
                                   ) deposit
                             ) ad
              ) inevitable
   CROSS JOIN unite
   CROSS JOIN (SELECT code.stuff
               FROM code
              ) security
   CROSS JOIN particular
   CROSS JOIN agricultural

''',
'''
SELECT constitutional.eye
FROM          (SELECT straight.sustain chase,
                      straight.differ crowd,
                      straight.conservation entity,
                      straight.service
               FROM (SELECT ready.since conservation,
                            ready.agency,
                            ready.behavior sustain,
                            ready.list differ,
                            ready.service
                     FROM ready
                    ) straight
              ) significant
   CROSS JOIN constitutional
   CROSS JOIN (SELECT wonderful.trauma heart,
                      oil.slice,
                      oil.changing founder
               FROM          (SELECT organ.seven switch,
                                     organ.pile tone,
                                     organ.push privacy,
                                     organ.highlight trauma,
                                     organ.check
                              FROM organ
                             ) wonderful
                  CROSS JOIN (SELECT mineral.route catalog,
                                     mineral.through slice,
                                     mineral.summer,
                                     mineral.speculate changing,
                                     mineral.patient,
                                     mineral.implement,
                                     mineral.sudden environmental
                              FROM mineral
                             ) oil
              ) representation
   CROSS JOIN terrific
   CROSS JOIN burn ;

''',
'''
SELECT steal.taste spiritual
FROM          (SELECT inspection.collar retail,
                      regulatory.taste,
                      inspection.band,
                      regulatory.latter protection,
                      regulatory.earnings,
                      fighting.around harm,
                      regulatory.secretary provide,
                      regulatory.midnight secure
               FROM          regulatory
                  CROSS JOIN (SELECT onto.band,
                                     onto.collar
                              FROM (SELECT salmon.invisible collar,
                                           salmon.surprise band
                                    FROM (SELECT century.surprise,
                                                 game.invisible
                                          FROM          game
                                             CROSS JOIN (SELECT highlight.surprise,
                                                                highlight.scientific
                                                         FROM (SELECT miracle.cigarette surprise,
                                                                      miracle.aware,
                                                                      page.scientific
                                                               FROM          (SELECT reservation.rape,
                                                                                     reservation.chicken,
                                                                                     reservation.manager,
                                                                                     reservation.tremendous ancestor,
                                                                                     reservation.user,
                                                                                     reservation.profit scientific,
                                                                                     reservation.representative bureau
                                                                              FROM reservation
                                                                             ) page
                                                                  CROSS JOIN miracle
                                                              ) highlight
                                                        ) century
                                             CROSS JOIN (SELECT dirty.similarly,
                                                                dirty.platform cabinet,
                                                                dirty.lady,
                                                                dirty.football,
                                                                dirty.drain
                                                         FROM (SELECT fair.drain,
                                                                      fair.appear,
                                                                      fair.football,
                                                                      fair.resource lady,
                                                                      fair.marketing similarly,
                                                                      fair.platform
                                                               FROM fair
                                                              ) dirty
                                                        ) withdraw
                                             CROSS JOIN (SELECT uncertain.virus steel,
                                                                uncertain.future melt,
                                                                uncertain.shrimp,
                                                                uncertain.fare
                                                         FROM (SELECT convey.celebrate virus,
                                                                      convey.level future,
                                                                      convey.popularity shrimp,
                                                                      convey.fare
                                                               FROM convey
                                                              ) uncertain
                                                        ) brand
                                         ) salmon
                                   ) onto
                             ) inspection
                  CROSS JOIN (SELECT earthquake.bug around
                              FROM (SELECT similarly.temporary disagree,
                                           similarly.expense fight,
                                           similarly.bug,
                                           similarly.actress
                                    FROM similarly
                                   ) earthquake
                             ) fighting
              ) steal
   CROSS JOIN (SELECT principal.act,
                      principal.cover,
                      principal.unfair brilliant
               FROM principal
              ) spending
   CROSS JOIN uncertainty

''',
'''
SELECT greet.representation realistic
FROM          human
   CROSS JOIN (SELECT substantial.her vital,
                      substantial.native demand,
                      substantial.call hay,
                      loose.career,
                      substantial.stand
               FROM          (SELECT mirror.sometime call,
                                     mirror.foundation stand,
                                     element.native,
                                     element.barn impact,
                                     conclude.constitute film,
                                     mirror.scan her
                              FROM          mirror
                                 CROSS JOIN (SELECT box.accurate
                                             FROM (SELECT rear.result accurate
                                                   FROM (SELECT return.bill result
                                                         FROM return
                                                        ) rear
                                                  ) box
                                            ) equal
                                 CROSS JOIN (SELECT lunch.reputation constitute,
                                                    lunch.concern tennis,
                                                    lunch.Korean
                                             FROM (SELECT far.overlook,
                                                          far.persist,
                                                          far.reputation,
                                                          far.speculation,
                                                          far.triumph concern,
                                                          far.Korean
                                                   FROM far
                                                  ) lunch
                                            ) conclude
                                 CROSS JOIN element
                             ) substantial
                  CROSS JOIN (SELECT grandchild.career,
                                     grandchild.ten skin
                              FROM (SELECT claim.ten,
                                           claim.consist career
                                    FROM (SELECT middle.Indian ten,
                                                 middle.hook virtual,
                                                 middle.border effectively,
                                                 middle.officially consist
                                          FROM (SELECT poor.border,
                                                       poor.neither shock,
                                                       poor.disc similar,
                                                       poor.familiar,
                                                       poor.hook,
                                                       poor.officially,
                                                       poor.perfect comparison,
                                                       poor.coach Indian
                                                FROM poor
                                               ) middle
                                         ) claim
                                   ) grandchild
                             ) loose
              ) lover
   CROSS JOIN (SELECT vessel.spot frown,
                      vessel.victory
               FROM (SELECT thread.spot,
                            thread.victory
                     FROM thread
                    ) vessel
              ) convinced
   CROSS JOIN attorney
   CROSS JOIN greet

''',
'''
SELECT dictate.praise porch
FROM          (SELECT question.game sole,
                      question.jet,
                      sea.loyalty,
                      cocaine.processing,
                      input.alternative glance
               FROM          (SELECT highway.notice jet,
                                     illustrate.game,
                                     illustrate.introduce,
                                     attempt.lady,
                                     attempt.coast,
                                     attempt.loose commander,
                                     illustrate.category empty
                              FROM          (SELECT compete.maintain notice,
                                                    compete.window
                                             FROM compete
                                            ) highway
                                 CROSS JOIN appreciate
                                 CROSS JOIN illustrate
                                 CROSS JOIN (SELECT excessive.embarrassed,
                                                    excessive.white,
                                                    excessive.auction,
                                                    excessive.size pocket,
                                                    excessive.tired pig,
                                                    excessive.front
                                             FROM excessive
                                            ) grass
                                 CROSS JOIN (SELECT slight.human loose,
                                                    slight.another rain,
                                                    slight.lady,
                                                    slight.teaching explosion,
                                                    slight.coast,
                                                    slight.hidden age
                                             FROM slight
                                            ) attempt
                             ) question
                  CROSS JOIN (SELECT divorce.processing
                              FROM (SELECT stretch.star processing
                                    FROM stretch
                                   ) divorce
                             ) cocaine
                  CROSS JOIN (SELECT jury.teen alternative,
                                     jury.telescope because
                              FROM (SELECT fault.statue,
                                           fault.interview telescope,
                                           fault.late candy,
                                           fault.flexible,
                                           fault.teen
                                    FROM fault
                                   ) jury
                             ) input
                  CROSS JOIN (SELECT peanut.loyalty
                              FROM (SELECT survivor.obtain loyalty
                                    FROM (SELECT comfortable.homeland establish,
                                                 comfortable.oven chance,
                                                 comfortable.obtain,
                                                 comfortable.servant favor,
                                                 comfortable.anger,
                                                 comfortable.list,
                                                 comfortable.flower,
                                                 comfortable.reference formation
                                          FROM comfortable
                                         ) survivor
                                   ) peanut
                             ) sea
              ) principal
   CROSS JOIN public
   CROSS JOIN (SELECT tendency.surprise,
                      tendency.praise,
                      tendency.dig,
                      tendency.shareholder,
                      tendency.obtain
               FROM tendency
              ) dictate ;

''',
'''
SELECT cloth.detective stranger
FROM (SELECT address.articulate,
             address.coverage,
             greet.counterpart,
             address.troubled what,
             greet.hate detective,
             visit.ha,
             greet.pitcher,
             greet.corridor assign
      FROM          (SELECT art.symptom
                     FROM (SELECT mechanic.symptom
                           FROM          (SELECT window.gathering
                                          FROM (SELECT accelerate.vs gathering
                                                FROM (SELECT attraction.overlook vs
                                                      FROM (SELECT management.overlook
                                                            FROM          (SELECT collapse.neither
                                                                           FROM (SELECT justice.neither
                                                                                 FROM (SELECT Islam.neither
                                                                                       FROM (SELECT seem.neither,
                                                                                                    seem.decent bother
                                                                                             FROM (SELECT picture.glance,
                                                                                                          picture.neither,
                                                                                                          picture.decent,
                                                                                                          picture.conservative animal
                                                                                                   FROM picture
                                                                                                  ) seem
                                                                                            ) Islam
                                                                                      ) justice
                                                                                ) collapse
                                                                          ) light
                                                               CROSS JOIN (SELECT fascinating.convention clue,
                                                                                  fascinating.effectiveness pant,
                                                                                  fascinating.withdrawal,
                                                                                  fascinating.though southwest,
                                                                                  fascinating.smart,
                                                                                  fascinating.hungry overlook,
                                                                                  fascinating.night,
                                                                                  fascinating.speak
                                                                           FROM fascinating
                                                                          ) management
                                                           ) attraction
                                                     ) accelerate
                                               ) window
                                         ) elite
                              CROSS JOIN (SELECT always.channel symptom
                                          FROM (SELECT pleasant.swallow channel
                                                FROM (SELECT instruction.swallow
                                                      FROM (SELECT prefer.copy properly,
                                                                   prefer.counseling,
                                                                   prefer.jaw,
                                                                   prefer.protest swallow
                                                            FROM prefer
                                                           ) instruction
                                                     ) pleasant
                                               ) always
                                         ) mechanic
                          ) art
                    ) passenger
         CROSS JOIN address
         CROSS JOIN (SELECT exhaust.twist,
                            exhaust.flash
                     FROM (SELECT position.flash,
                                  position.diplomatic twist
                           FROM (SELECT danger.diplomatic,
                                        danger.pile flash
                                 FROM danger
                                ) position
                          ) exhaust
                    ) day
         CROSS JOIN greet
         CROSS JOIN (SELECT desperately.vital ha,
                            desperately.rabbit research
                     FROM desperately
                    ) visit
     ) cloth ;

''',
'''
SELECT cheap.bad
FROM          expectation
   CROSS JOIN air
   CROSS JOIN (SELECT blame.disappear hypothesis
               FROM          blame
                  CROSS JOIN (SELECT sibling.praise experience,
                                     sibling.ideal vacation
                              FROM (SELECT desk.offer class,
                                           desk.fresh ear,
                                           desk.praise,
                                           desk.cheat,
                                           buyer.ideal
                                    FROM          (SELECT certainly.analyze,
                                                          certainly.minute telescope,
                                                          certainly.ideal
                                                   FROM certainly
                                                  ) buyer
                                       CROSS JOIN (SELECT form.become former,
                                                          form.present Chinese,
                                                          form.offer,
                                                          form.fall,
                                                          form.exhibit weak,
                                                          form.cheat,
                                                          form.military praise,
                                                          form.fashion fresh
                                                   FROM form
                                                  ) desk
                                   ) sibling
                             ) classroom
              ) focus
   CROSS JOIN (SELECT distinction.missing give,
                      distinction.either river,
                      distinction.north beard,
                      distinction.hazard direct,
                      distinction.protection,
                      distinction.bad
               FROM distinction
              ) cheap ;

''',
'''
SELECT boss.horrible
FROM          (SELECT text.horrible
               FROM          destroy
                  CROSS JOIN (SELECT fold.building,
                                     fold.behind high,
                                     fold.horrible,
                                     fold.pool,
                                     fold.Latin,
                                     fold.dissolve,
                                     fold.compromise killer
                              FROM (SELECT frequently.building,
                                           overlook.compromise,
                                           overlook.return behind,
                                           frequently.dissolve,
                                           overlook.horrible,
                                           overlook.roughly Latin,
                                           overlook.repeatedly pool
                                    FROM          (SELECT sacred.midnight building,
                                                          sacred.today dissolve
                                                   FROM (SELECT wage.collector midnight,
                                                                wage.openly,
                                                                wage.consider today,
                                                                wage.spectacular depart,
                                                                wage.exception
                                                         FROM wage
                                                        ) sacred
                                                  ) frequently
                                       CROSS JOIN overlook
                                   ) fold
                             ) text
              ) boss
   CROSS JOIN (SELECT addition.interpret await
               FROM addition
              ) plead
   CROSS JOIN mean ;

''',
'''
SELECT second.volunteer spit
FROM          (SELECT rain.impact,
                      rain.alliance
               FROM          annually
                  CROSS JOIN (SELECT convey.office cold,
                                     convey.partly liberal,
                                     convey.content specialist,
                                     convey.gain dilemma,
                                     convey.implementation,
                                     convey.up passage
                              FROM          (SELECT store.bat folk
                                             FROM (SELECT invasion.labor bat
                                                   FROM (SELECT feature.labor
                                                         FROM feature
                                                        ) invasion
                                                  ) store
                                            ) steadily
                                 CROSS JOIN convey
                             ) rumor
                  CROSS JOIN rain
                  CROSS JOIN (SELECT wagon.politically calm
                              FROM wagon
                             ) band
                  CROSS JOIN (SELECT gaze.await,
                                     gaze.immediately stake,
                                     gaze.DNA,
                                     gaze.ethical opposition,
                                     gaze.world
                              FROM (SELECT computer.indicate drop,
                                           computer.softly re,
                                           computer.world,
                                           computer.enact ethical,
                                           computer.await,
                                           computer.immediately,
                                           computer.swell quickly,
                                           computer.DNA
                                    FROM computer
                                   ) gaze
                             ) open
                  CROSS JOIN (SELECT beautiful.household
                              FROM (SELECT simple.scream,
                                           simple.underlying household
                                    FROM (SELECT smoke.underlying,
                                                 smoke.dawn scream,
                                                 smoke.cover burn
                                          FROM (SELECT rather.pink underlying,
                                                       rather.fraction hence,
                                                       rather.alone cover,
                                                       rather.dawn
                                                FROM rather
                                               ) smoke
                                         ) simple
                                   ) beautiful
                             ) freeze
              ) character
   CROSS JOIN second
   CROSS JOIN (SELECT activity.commodity ancestor,
                      activity.sound
               FROM activity
              ) wife
   CROSS JOIN lie
   CROSS JOIN (SELECT relieve.witness
               FROM (SELECT dress.witness
                     FROM (SELECT reflection.render,
                                  reflection.educational witness,
                                  reflection.worldwide,
                                  reflection.company most
                           FROM (SELECT search.render,
                                        search.worldwide,
                                        search.form,
                                        search.opposed company,
                                        search.educational,
                                        search.Israeli privilege
                                 FROM search
                                ) reflection
                          ) dress
                    ) relieve
              ) either ;

''',
'''
SELECT English.symbol mood
FROM          dismiss
   CROSS JOIN (SELECT theoretical.intervention travel,
                      theoretical.exotic incident,
                      theoretical.ally conspiracy,
                      theoretical.courage,
                      theoretical.Hispanic,
                      strike.through symbol,
                      strike.agriculture,
                      theoretical.wish
               FROM          (SELECT damage.bus through,
                                     damage.nest agriculture
                              FROM          (SELECT movement.write
                                             FROM          (SELECT appear.broadcast rock,
                                                                   appear.purpose,
                                                                   appear.habitat,
                                                                   appear.distinctive,
                                                                   appear.distinction,
                                                                   appear.protest,
                                                                   appear.flood write,
                                                                   appear.salt science
                                                            FROM appear
                                                           ) movement
                                                CROSS JOIN (SELECT sector.purple airline
                                                            FROM sector
                                                           ) tune
                                            ) hole
                                 CROSS JOIN (SELECT river.shit assistance,
                                                    river.motor worth,
                                                    river.frame,
                                                    river.nest,
                                                    river.bus
                                             FROM river
                                            ) damage
                             ) strike
                  CROSS JOIN theoretical
              ) English
   CROSS JOIN (SELECT presumably.jar,
                      presumably.global southwest,
                      presumably.district cooperative,
                      presumably.poetry,
                      presumably.note particle,
                      presumably.pen belt
               FROM presumably
              ) clean
   CROSS JOIN (SELECT survival.crop
               FROM (SELECT increased.crop
                     FROM (SELECT prayer.crop
                           FROM prayer
                          ) increased
                    ) survival
              ) hire
   CROSS JOIN (SELECT bring.desperately place,
                      bring.surround,
                      bring.confession,
                      bring.twice,
                      bring.corn
               FROM bring
              ) homeland
   CROSS JOIN (SELECT patient.body,
                      patient.hurry bronze,
                      patient.ecological morning,
                      patient.bind hunt,
                      patient.per rapidly,
                      patient.sibling,
                      patient.combat
               FROM (SELECT block.hurry,
                            block.announcement combat,
                            block.per,
                            block.conclusion ecological,
                            block.quickly body,
                            block.sibling,
                            block.bind
                     FROM block
                    ) patient
              ) personally ;

''',
'''
SELECT bag.psychology
FROM          (SELECT mortgage.expected realm,
                      dark.metaphor away,
                      dark.shortage,
                      dark.beyond,
                      dark.laugh vertical
               FROM          (SELECT please.standard expected
                              FROM (SELECT cry.standard,
                                           terror.shape,
                                           terror.originally,
                                           terror.state
                                    FROM (SELECT helmet.standard
                                          FROM (SELECT answer.standard
                                                FROM answer
                                               ) helmet
                                         ) cry
                                         CROSS JOIN explosion
                                        CROSS JOIN (SELECT underlying.verdict shape,
                                                           underlying.design cultural,
                                                           underlying.state,
                                                           underlying.dad,
                                                           underlying.originally
                                                    FROM underlying
                                                   ) terror
                                   ) please
                             ) mortgage
                  CROSS JOIN dark
              ) heavy

   CROSS JOIN accept
   CROSS JOIN (SELECT gold.feel,
                      gold.psychology,
                      gold.hair chain
               FROM gold
              ) bag
   CROSS JOIN heritage

''',
'''
SELECT how.spot
FROM          (SELECT speculation.thumb intellectual,
                      speculation.accompany basis,
                      estimate.five,
                      speculation.peak roughly,
                      eligible.questionnaire,
                      speculation.correct,
                      speculation.cotton,
                      estimate.automatically spot
               FROM          (SELECT peer.healthy questionnaire
                              FROM (SELECT committee.check healthy
                                    FROM (SELECT hint.check
                                          FROM (SELECT similarly.check
                                                FROM (SELECT page.output check
                                                      FROM (SELECT no.survival output
                                                            FROM no
                                                           ) page
                                                     ) similarly
                                               ) hint
                                         ) committee
                                   ) peer
                             ) eligible
                  CROSS JOIN estimate
                  CROSS JOIN (SELECT replacement.steep
                              FROM (SELECT daily.best requirement,
                                           daily.amendment steep
                                    FROM (SELECT carrot.those amendment,
                                                 carrot.shake,
                                                 carrot.best
                                          FROM (SELECT model.creative shake,
                                                       model.lawmaker those,
                                                       model.decline best
                                                FROM model
                                               ) carrot
                                         ) daily
                                   ) replacement
                             ) over
                  CROSS JOIN speculation
                  CROSS JOIN definitely
              ) how
   CROSS JOIN (SELECT telescope.illustrate collaboration,
                      telescope.suck loss,
                      telescope.bathroom each
               FROM (SELECT counter.rank available,
                            counter.minister bathroom,
                            counter.suck,
                            counter.herb illustrate
                     FROM counter
                    ) telescope
              ) think
   CROSS JOIN (SELECT matter.reporting
               FROM (SELECT metaphor.mere reporting
                     FROM (SELECT chest.mere
                           FROM (SELECT dinner.lay house,
                                        dinner.principal mere,
                                        dinner.for
                                 FROM dinner
                                ) chest
                          ) metaphor
                    ) matter
              ) deliver
   CROSS JOIN (SELECT curtain.than,
                      curtain.wish
               FROM (SELECT exchange.spray,
                            exchange.either than,
                            exchange.wagon,
                            exchange.verdict wish
                     FROM (SELECT straighten.best bike,
                                  straighten.identify either,
                                  straighten.wagon,
                                  straighten.spray,
                                  straighten.verdict
                           FROM straighten
                          ) exchange
                    ) curtain
              ) cattle ;

''',
'''
SELECT eat.journey
FROM          (SELECT demonstration.journey
               FROM (SELECT counselor.journey
                     FROM (SELECT result.demographic meat,
                                  result.journey
                           FROM (SELECT grape.equipment obligation,
                                        grape.weight,
                                        grape.play consent,
                                        grape.officer demographic,
                                        grape.journey,
                                        grape.exist
                                 FROM (SELECT welcome.weight,
                                              welcome.exploration discipline,
                                              welcome.equipment,
                                              welcome.against officer,
                                              welcome.another exist,
                                              welcome.journey,
                                              welcome.play
                                       FROM welcome
                                      ) grape
                                ) result
                          ) counselor
                    ) demonstration
              ) eat
   CROSS JOIN money
   CROSS JOIN (SELECT frequently.involvement,
                      frequently.palace marketing,
                      frequently.option chocolate,
                      frequently.sibling client
               FROM frequently
              ) capital
   CROSS JOIN (SELECT curiosity.similar dough,
                      curiosity.virtually
               FROM curiosity
              ) mainstream

''',
'''
SELECT close.nut ship
FROM          close
   CROSS JOIN (SELECT ago.otherwise chip,
                      ago.month into,
                      sustainable.chocolate maximum
               FROM          ago
                  CROSS JOIN (SELECT practically.afternoon,
                                     hardly.lion,
                                     practically.due fare,
                                     hardly.surgery,
                                     practically.effective,
                                     hardly.activist,
                                     hardly.commonly,
                                     hardly.him
                              FROM          (SELECT lucky.afternoon,
                                                    lucky.effective,
                                                    lucky.reach due
                                             FROM (SELECT trend.afternoon,
                                                          trend.reach,
                                                          trend.effective
                                                   FROM trend
                                                  ) lucky
                                            ) practically
                                 CROSS JOIN hardly
                             ) religious
                  CROSS JOIN (SELECT dare.chocolate,
                                     dare.net
                              FROM (SELECT for.net,
                                           for.six funeral,
                                           for.mine chocolate
                                    FROM for
                                   ) dare
                             ) sustainable
                  CROSS JOIN (SELECT indigenous.stress
                              FROM (SELECT demonstrate.likely stress
                                    FROM (SELECT subsequent.weigh likely
                                          FROM (SELECT figure.weigh
                                                FROM (SELECT retail.tool weigh
                                                      FROM (SELECT door.pump tool
                                                            FROM (SELECT constitution.impressive pump,
                                                                         constitution.print
                                                                  FROM (SELECT sun.release,
                                                                               sun.impressive,
                                                                               sun.slice stand,
                                                                               sun.print
                                                                        FROM (SELECT cancel.owe slice,
                                                                                     cancel.foreign print,
                                                                                     cancel.impressive,
                                                                                     cancel.release
                                                                              FROM cancel
                                                                             ) sun
                                                                       ) constitution
                                                                 ) door
                                                           ) retail
                                                     ) figure
                                               ) subsequent
                                         ) demonstrate
                                   ) indigenous
                             ) offer
                  CROSS JOIN (SELECT visual.belong
                              FROM (SELECT job.belong
                                    FROM (SELECT regularly.belong,
                                                 regularly.uh,
                                                 regularly.injure
                                          FROM regularly
                                         ) job
                                   ) visual
                             ) yard
                  CROSS JOIN (SELECT disabled.prosecution explore,
                                     disabled.sue
                              FROM (SELECT attraction.embrace,
                                           attraction.Israeli sue,
                                           attraction.eastern,
                                           attraction.prosecution
                                    FROM attraction
                                   ) disabled
                             ) entry
              ) discourage ;

''',
'''
SELECT texture.wealth
FROM         (SELECT annually.network,
                      annually.but,
                      clay.wealth
               FROM          annually
                  CROSS JOIN medium
                  CROSS JOIN (SELECT lane.cemetery guarantee,
                                     railroad.elsewhere,
                                     lane.wealth,
                                     lane.stuff
                              FROM          (SELECT we.court elsewhere
                                             FROM (SELECT move.court,
                                                          move.potential,
                                                          move.differently
                                                   FROM (SELECT usually.insight rod,
                                                                usually.sign potential,
                                                                usually.input differently,
                                                                usually.past court
                                                         FROM usually
                                                        ) move
                                                  ) we
                                            ) railroad
                                 CROSS JOIN (SELECT European.wealth,
                                                    European.cemetery,
                                                    European.stuff
                                             FROM European
                                            ) lane
                             ) clay
              ) texture
   CROSS JOIN (SELECT punishment.show criminal,
                    punishment.appearance,
                    punishment.up arm
               FROM punishment
              ) mixture
   CROSS JOIN since ;

''',
'''
SELECT premium.routine
FROM          work
   CROSS JOIN (SELECT missionary.guard,
                      access.consciousness structure,
                      property.consequently confirm,
                      flying.present furniture,
                      astronomer.bunch,
                      astronomer.enroll
               FROM          astronomer
                  CROSS JOIN flying
                  CROSS JOIN (SELECT pull.criticize,
                                     pull.bet excited,
                                     pull.contemplate,
                                     pull.address peel,
                                     pull.phone web,
                                     pull.guard,
                                     pull.significantly
                              FROM (SELECT cease.cheer address,
                                           bulb.superior guard,
                                           depression.contemplate,
                                           their.otherwise criticize,
                                           bulb.phone,
                                           later.worldwide jeans,
                                           cease.bet,
                                           their.media significantly
                                    FROM          (SELECT both.strict reference,
                                                          both.rat,
                                                          both.contemplate
                                                   FROM (SELECT depth.guilty contemplate,
                                                                depth.strict,
                                                                depth.mission cooperative,
                                                                depth.divide minister,
                                                                depth.series rat,
                                                                depth.salmon,
                                                                depth.respondent obvious
                                                         FROM depth
                                                        ) both
                                                  ) depression
                                       CROSS JOIN (SELECT schedule.naval frown,
                                                          schedule.federal dining,
                                                          schedule.severe,
                                                          schedule.dynamics,
                                                          schedule.supervisor worldwide
                                                   FROM schedule
                                                  ) later
                                       CROSS JOIN cease
                                       CROSS JOIN bulb
                                       CROSS JOIN their
                                   ) pull
                             ) missionary
                  CROSS JOIN (SELECT rely.experience,
                                     rely.stomach,
                                     rely.oral unprecedented,
                                     rely.glory extensive,
                                     rely.pay,
                                     rely.rank consequently
                              FROM rely
                             ) property
                  CROSS JOIN (SELECT well.orange consciousness,
                                     well.hope,
                                     well.porch expect,
                                     well.rumor discuss,
                                     well.odd,
                                     well.guilty
                              FROM well
                             ) access
              ) educational
   CROSS JOIN premium ;

''',
'''
SELECT manufacturer.hesitate break
FROM          (SELECT Palestinian.hesitate,
                      Palestinian.educate reader
               FROM          finance
                  CROSS JOIN Palestinian
              ) manufacturer
   CROSS JOIN (SELECT pipe.enough,
                      pipe.jump,
                      pipe.unlike French,
                      bulk.therapy maybe
               FROM          (SELECT favorite.gentle intact,
                                     apply.tough,
                                     favorite.issue,
                                     favorite.therapy,
                                     undergraduate.chin,
                                     favorite.over
                              FROM          (SELECT undertake.instrument wrap,
                                                    undertake.convict,
                                                    undertake.tough
                                             FROM (SELECT bury.tough,
                                                          bury.alternative dozen,
                                                          bury.collector clerk,
                                                          bury.convict,
                                                          bury.unprecedented instrument
                                                   FROM bury
                                                  ) undertake
                                            ) apply
                                 CROSS JOIN favorite
                                 CROSS JOIN (SELECT blind.scent,
                                                    blind.essential maximum,
                                                    blind.meanwhile machine,
                                                    blind.original,
                                                    blind.discovery minute,
                                                    blind.transaction chin,
                                                    blind.bicycle bend
                                             FROM blind
                                            ) undergraduate
                                 CROSS JOIN (SELECT remind.figure
                                             FROM (SELECT surprised.supposed figure
                                                   FROM (SELECT couple.supposed
                                                         FROM (SELECT impulse.line supposed
                                                               FROM (SELECT option.rental line,
                                                                            option.athlete help
                                                                     FROM (SELECT elbow.rely rental,
                                                                                  elbow.athlete
                                                                           FROM (SELECT diet.rely,
                                                                                        diet.athlete
                                                                                 FROM diet
                                                                                ) elbow
                                                                          ) option
                                                                    ) impulse
                                                              ) couple
                                                        ) surprised
                                                  ) remind
                                            ) proceed
                             ) bulk
                  CROSS JOIN (SELECT shade.capability,
                                     shade.spiritual process,
                                     shade.seem instead,
                                     shade.birthday jump,
                                     shade.enough,
                                     shade.shell unlike,
                                     shade.boy daughter,
                                     shade.vacation
                              FROM shade
                             ) pipe
              ) stack

''',
'''
SELECT signal.reserve edge
FROM          (SELECT coat.reserve,
                      reliability.million,
                      coat.total,
                      coat.hear,
                      coat.cook enroll,
                      coat.doubt conduct,
                      reliability.level
               FROM          coat
                  CROSS JOIN (SELECT rapid.essay,
                                     rapid.touchdown,
                                     rapid.million,
                                     rapid.purchase,
                                     rapid.museum artist,
                                     rapid.within level
                              FROM (SELECT venture.hotel million,
                                           venture.internal museum,
                                           venture.within,
                                           venture.essay,
                                           venture.attribute purchase,
                                           venture.touch touchdown
                                    FROM venture
                                   ) rapid
                             ) reliability
              ) signal
   CROSS JOIN (SELECT production.tight,
                      production.advantage professional
               FROM (SELECT respect.advantage,
                            respect.either,
                            respect.metropolitan tight
                     FROM (SELECT grain.hope metropolitan,
                                  grain.chapter either,
                                  grain.via seat,
                                  grain.advantage
                           FROM (SELECT diabetes.have chapter,
                                        diabetes.out via,
                                        diabetes.hope,
                                        diabetes.aide advantage
                                 FROM (SELECT shit.hope,
                                              shit.have,
                                              shit.long out,
                                              shit.aide,
                                              shit.winner evidence
                                       FROM (SELECT happily.regarding winner,
                                                    happily.hope,
                                                    happily.have,
                                                    happily.aide,
                                                    happily.doubt long
                                             FROM (SELECT blade.interrupt century,
                                                          blade.recognize aide,
                                                          blade.hunting doubt,
                                                          blade.midst hope,
                                                          blade.jail,
                                                          blade.have,
                                                          blade.recall experimental,
                                                          blade.regarding
                                                   FROM blade
                                                  ) happily
                                            ) shit
                                      ) diabetes
                                ) grain
                          ) respect
                    ) production
              ) conviction
   CROSS JOIN (SELECT learning.harm,
                      learning.course dominate,
                      learning.crew,
                      learning.our Indian
               FROM learning
              ) lawyer
   CROSS JOIN (SELECT concerning.conception decline,
                      concerning.complicated,
                      concerning.passion,
                      concerning.tail die,
                      concerning.forever pure
               FROM concerning
              ) voting

''',
'''
SELECT minimize.banker
FROM          (SELECT offense.unlike,
                      offense.upper clock,
                      pizza.nothing industry,
                      pizza.Republican principal,
                      pizza.shrink integration,
                      pizza.operator,
                      offense.sign,
                      pizza.aircraft sake
               FROM          (SELECT practitioner.unlike,
                                     drop.sign,
                                     drop.side upper,
                                     drop.AIDS
                              FROM          (SELECT tuck.unlike
                                             FROM (SELECT Internet.artistic unlike
                                                   FROM (SELECT acquire.herself,
                                                                acquire.singer,
                                                                acquire.respond discourage,
                                                                acquire.German teacher,
                                                                acquire.preparation artistic
                                                         FROM (SELECT average.herself,
                                                                      average.shot,
                                                                      average.tire respond,
                                                                      average.ancestor German,
                                                                      average.singer,
                                                                      average.drop endorse,
                                                                      average.assume preparation,
                                                                      average.divorce
                                                               FROM average
                                                              ) acquire
                                                        ) Internet
                                                  ) tuck
                                            ) practitioner
                                 CROSS JOIN (SELECT during.initiative AIDS,
                                                    during.trace beach,
                                                    during.sign,
                                                    during.isolation map,
                                                    during.consistently,
                                                    during.evidence side
                                             FROM during
                                            ) drop
                             ) offense
                  CROSS JOIN pizza
              ) publish
   CROSS JOIN (SELECT lap.distinguish,
                      lap.revolution quietly,
                      lap.appreciation test
               FROM (SELECT adequate.appreciation,
                            adequate.appointment revolution,
                            adequate.distinguish
                     FROM (SELECT minimum.appreciation,
                                  minimum.appointment,
                                  minimum.aunt distinguish
                           FROM minimum
                          ) adequate
                    ) lap
              ) divorce
   CROSS JOIN input
   CROSS JOIN (SELECT competition.cargo,
                      competition.recover urge,
                      competition.art,
                      competition.Republican strengthen,
                      competition.confession eat,
                      competition.ha wrong
               FROM (SELECT girlfriend.cargo,
                            girlfriend.recover,
                            girlfriend.art,
                            girlfriend.severely Republican,
                            girlfriend.danger confession,
                            girlfriend.ha
                     FROM girlfriend
                    ) competition
              ) dessert
   CROSS JOIN minimize

''',
'''
SELECT brush.diminish
FROM          notebook
   CROSS JOIN nuclear
   CROSS JOIN (SELECT background.surveillance tourism
               FROM (SELECT popular.surveillance
                     FROM (SELECT amazing.result surveillance,
                                  amazing.electricity
                           FROM (SELECT appeal.content laughter,
                                        appeal.result,
                                        appeal.electricity
                                 FROM appeal
                                ) amazing
                          ) popular
                    ) background
              ) jazz
   CROSS JOIN (SELECT why.diminish
               FROM why
              ) brush
   CROSS JOIN (SELECT eyebrow.trend,
                      eyebrow.thinking tobacco,
                      eyebrow.screen invisible
               FROM (SELECT undermine.thinking,
                            undermine.pop firm,
                            undermine.trend,
                            undermine.silly screen
                     FROM undermine
                    ) eyebrow
              ) wage
   CROSS JOIN (SELECT beard.arrest,
                      beard.freedom love
               FROM beard
              ) swell
   CROSS JOIN lesson
   CROSS JOIN outlet
   CROSS JOIN perception ;

''',
'''
SELECT complain.essentially
FROM (SELECT effectively.attitude,
             shake.essentially,
             log.attract officially,
             log.telescope,
             shake.feminist encouraging,
             effectively.club tiny
      FROM          effectively
         CROSS JOIN (SELECT aggression.efficient dramatic,
                            sufficient.chamber feminist,
                            sufficient.essentially,
                            aggression.feedback mean,
                            aggression.distance
                     FROM          aggression
                        CROSS JOIN (SELECT probably.stand chamber,
                                           within.essentially,
                                           within.distribution,
                                           probably.scared belt
                                    FROM          probably
                                       CROSS JOIN (SELECT museum.sport invest,
                                                          museum.script,
                                                          museum.cattle Muslim,
                                                          museum.portfolio,
                                                          museum.distribution,
                                                          distribution.essentially
                                                   FROM         museum
                                                   CROSS JOIN  (SELECT event.essentially
                                                                  FROM          event
                                                                     CROSS JOIN cease
                                                                 ) distribution
                                                  ) within
                                   ) sufficient
                    ) shake
         CROSS JOIN log
     ) complain ;

''',
'''
SELECT vote.haul agricultural
FROM          belly
   CROSS JOIN (SELECT Israeli.at
               FROM (SELECT conservative.at
                     FROM          (SELECT truly.pile,
                                           truly.data,
                                           truly.near initially,
                                           truly.wing gaze
                                    FROM          truly
                                       CROSS JOIN (SELECT coordinator.perspective reservation
                                                   FROM (SELECT almost.bonus perspective,
                                                                almost.forest cost,
                                                                almost.profit,
                                                                almost.counterpart
                                                         FROM (SELECT willingness.part profit,
                                                                      willingness.resign priest,
                                                                      willingness.bonus,
                                                                      willingness.forest,
                                                                      willingness.counterpart
                                                               FROM willingness
                                                              ) almost
                                                        ) coordinator
                                                  ) intellectual
                                   ) well
                        CROSS JOIN conservative
                    ) Israeli
              ) encounter
   CROSS JOIN send
   CROSS JOIN (SELECT destruction.deliver middle,
                      destruction.gather,
                      destruction.dry handle,
                      destruction.front,
                      destruction.trunk,
                      destruction.wrist,
                      destruction.weekly
               FROM destruction
              ) statue
   CROSS JOIN (SELECT sight.secret broken
               FROM sight
              ) slice
   CROSS JOIN (SELECT benefit.haul,
                      benefit.uncle bull,
                      benefit.welcome loud,
                      benefit.growth,
                      benefit.fund effectiveness,
                      benefit.pizza
               FROM benefit
              ) vote
   CROSS JOIN ride
   CROSS JOIN (SELECT calculate.blank,
                      calculate.anything skilled,
                      calculate.orientation,
                      calculate.relate,
                      calculate.lamp spill,
                      calculate.spread
               FROM (SELECT scholar.orientation,
                            scholar.lamp,
                            scholar.anything,
                            scholar.remarkable spread,
                            scholar.relate,
                            scholar.frame blank
                     FROM scholar
                    ) calculate
              ) treasure
   CROSS JOIN (SELECT length.skip use,
                      length.vulnerable eye
               FROM (SELECT founder.academic lock,
                            founder.tap,
                            founder.up,
                            founder.skip,
                            founder.stick,
                            founder.station,
                            founder.vulnerable,
                            founder.without evil
                     FROM founder
                    ) length
              ) father

''',
'''
SELECT diary.asset
FROM          qualify
   CROSS JOIN (SELECT native.why,
                      native.asset,
                      native.target if,
                      political.profession reform,
                      ruling.though response,
                      political.profession horizon,
                      ruling.wheelchair
               FROM          ruling
                  CROSS JOIN (SELECT bite.dock profession,
                                     bite.harm racial,
                                     bite.handle mom
                              FROM bite
                             ) political
                  CROSS JOIN (SELECT yellow.valuable why,
                                     grandfather.stable target,
                                     surrounding.electronic,
                                     whale.asset
                              FROM          yellow
                                 CROSS JOIN (SELECT angry.Israeli hurricane,
                                                    angry.asset
                                             FROM angry
                                            ) whale
                                 CROSS JOIN grandfather
                                 CROSS JOIN (SELECT search.electronic
                                             FROM search
                                            ) surrounding
                             ) native
              ) diary
   CROSS JOIN (SELECT classify.immigrant,
                      classify.vital,
                      classify.poll priest
               FROM classify
              ) thread ;

''',
'''
SELECT safe.possible
FROM          safe
   CROSS JOIN (SELECT neighbor.condemn,
                      violence.likely,
                      Democrat.humanity,
                      neighbor.happily
               FROM          neighbor
                  CROSS JOIN (SELECT bottom.slow,
                                     pat.moment sandwich,
                                     bottom.likely,
                                     different.makeup pro
                              FROM          pat
                                 CROSS JOIN (SELECT operating.general,
                                                    operating.status makeup,
                                                    refrigerator.streak worried,
                                                    operating.panic we
                                             FROM          (SELECT scratch.advocate,
                                                                   scratch.general,
                                                                   scratch.panic,
                                                                   scratch.status
                                                            FROM scratch
                                                           ) operating
                                                CROSS JOIN (SELECT spark.wish,
                                                                   spark.toss streak
                                                            FROM (SELECT analyze.gentle meaning,
                                                                         analyze.nature severe,
                                                                         analyze.totally toss,
                                                                         analyze.individual wish,
                                                                         analyze.click,
                                                                         analyze.handle attractive
                                                                  FROM analyze
                                                                 ) spark
                                                           ) refrigerator
                                            ) different
                                 CROSS JOIN bottom
                             ) violence
                  CROSS JOIN (SELECT active.vulnerable,
                                     active.that,
                                     active.carve,
                                     active.grin excitement,
                                     active.particularly
                              FROM (SELECT acquire.currently assist,
                                           acquire.businessman that,
                                           acquire.particularly,
                                           acquire.satellite share,
                                           acquire.bowl grin,
                                           acquire.carve,
                                           acquire.vulnerable,
                                           acquire.pleasant cabinet
                                    FROM acquire
                                   ) active
                             ) bell
                  CROSS JOIN (SELECT dock.monthly direct,
                                     dock.sun,
                                     dock.quit,
                                     dock.office
                              FROM dock
                             ) fit
                  CROSS JOIN (SELECT partly.state lost,
                                     partly.breath,
                                     partly.conduct,
                                     partly.interview thoroughly,
                                     partly.damn pan,
                                     partly.humanity
                              FROM partly
                             ) Democrat
              ) poet ;

''',
'''
SELECT hour.neat delay
FROM          (SELECT whereas.five,
                      belong.legislature
               FROM          (SELECT overlook.period possess,
                                     exclusively.texture engagement,
                                     exclusively.double aide,
                                     sail.secure celebration,
                                     overlook.bolt frame,
                                     sail.sharply paper,
                                     overlook.chocolate,
                                     overlook.legislature
                              FROM          sail
                                 CROSS JOIN overlook
                                 CROSS JOIN (SELECT beyond.traditionally profession,
                                                    beyond.example double,
                                                    beyond.bicycle texture
                                             FROM (SELECT animal.example,
                                                          increasingly.bicycle,
                                                          animal.objection traditionally
                                                   FROM          animal
                                                      CROSS JOIN increasingly
                                                  ) beyond
                                            ) exclusively
                             ) belong
                  CROSS JOIN dance
                  CROSS JOIN (SELECT standing.re ha,
                                     standing.random,
                                     standing.five,
                                     standing.twist pro,
                                     standing.near whatever,
                                     standing.finding,
                                     standing.representation guarantee
                              FROM standing
                             ) whereas
                  CROSS JOIN (SELECT assault.huge,
                                     assault.artistic,
                                     assault.fool object,
                                     assault.trigger,
                                     assault.manufacturing shoe,
                                     assault.cheese sandwich,
                                     assault.theology,
                                     assault.future
                              FROM assault
                             ) rain
              ) alone
   CROSS JOIN (SELECT consecutive.corn prominent,
                      consecutive.satellite,
                      consecutive.deserve,
                      consecutive.conspiracy exclusively,
                      consecutive.dimension preliminary,
                      consecutive.classify hall,
                      consecutive.seriously,
                      consecutive.glance
               FROM consecutive
              ) expect
   CROSS JOIN refuge
   CROSS JOIN stress
   CROSS JOIN (SELECT sole.museum tropical,
                      sole.neat,
                      sole.upstairs,
                      sole.high,
                      sole.consumer popular,
                      sole.dynamics,
                      sole.solar plan
               FROM sole
              ) hour ;

''',
'''
SELECT hers.opinion
FROM          (SELECT contend.suspect,
                      contend.exceed corn,
                      contend.apology period,
                      contend.OK whose
               FROM contend
              ) servant
   CROSS JOIN (SELECT sock.tend
               FROM sock
              ) mayor
   CROSS JOIN (SELECT essential.soil opinion,
                      essential.happily,
                      essential.motive sock,
                      essential.thus alone
               FROM (SELECT summit.road,
                            summit.judge century,
                            summit.lot motive,
                            summit.illegal,
                            summit.banking thus,
                            summit.soil,
                            summit.happily
                     FROM (SELECT smart.lot,
                                  smart.happily,
                                  smart.employment road,
                                  smart.store judge,
                                  smart.banking,
                                  smart.name gang,
                                  smart.river soil,
                                  smart.producer illegal
                           FROM smart
                          ) summit
                    ) essential
              ) hers
   CROSS JOIN view
   CROSS JOIN stare ;

'''
];

var shuffledStatementsTextual = [
  '''
SELECT satisfy.agree
FROM          (SELECT tooth.migration,
                      string.cheek agree,
                      tooth.statute,
                      tooth.offer shore
               FROM          (SELECT endless.cheek
                              FROM          physical
                                 CROSS JOIN (SELECT married.threat,
                                                   married.gift infection,
                                                   married.advantage within,
                                                   married.daily,
                                                   married.uh
                                            FROM married
                                           ) competitor

                                 CROSS JOIN (SELECT grasp.baseball cheek ,
                                                    grasp.nerve,
                                                    strongly.past,
                                                    strongly.moment,
                                                    strongly.application
                                             FROM          (SELECT she.baseball,
                                                                   she.disorder nerve
                                                            FROM (SELECT neighboring.disorder,
                                                                         neighboring.baseball
                                                                  FROM neighboring
                                                                 ) she
                                                           ) grasp
                                                CROSS JOIN strongly
                                            ) endless
                                 CROSS JOIN (SELECT aim.dig landscape
                                             FROM aim
                                            ) blow
                             ) string
                  CROSS JOIN tooth
              ) satisfy
   CROSS JOIN saving


''',
  '''
SELECT forbid.tradition
FROM          (SELECT duty.virtual peak,
                      disturb.budget,
                      lack.want variety,
                      lake.tradition,
                      monument.guidance,
                      duty.enable
               FROM          lack
                  CROSS JOIN duty
                  CROSS JOIN (SELECT pin.tradition,
                                     pin.plus
                              FROM (SELECT moral.planner plus,
                                           moral.tradition,
                                           moral.ranch
                                    FROM (SELECT Persian.planner,
                                                 Persian.tradition,
                                                 Persian.reflection ranch
                                          FROM (SELECT partner.planner,
                                                        partner.tradition,
                                                        partner.reflection
                                                 FROM partner
                                                ) Persian
                                         ) moral
                                   ) pin
                             ) lake
                  CROSS JOIN disturb
                  CROSS JOIN (SELECT usual.active decline,
                                        usual.fly guidance
                                 FROM (SELECT gear.present fly,
                                              gear.frequency active,
                                              gear.step central
                                       FROM gear
                                      ) usual
                            ) monument 
              ) forbid



''',
  '''
SELECT operating.recognize significantly
FROM          (SELECT characteristic.minimum running,
                      characteristic.deck,
                      characteristic.form recognize,
                      characteristic.tiny
               FROM (SELECT seriously.tiny,
                            seriously.deck,
                            seriously.minimum,
                            seriously.string,
                            seriously.film form
                     FROM (SELECT religious.film,
                                  religious.tiny,
                                  religious.anger deck,
                                  religious.string,
                                  religious.poet minimum
                           FROM religious
                          ) seriously
                    ) characteristic
              ) operating
   CROSS JOIN (SELECT fantastic.thirty
               FROM (SELECT assessment.ceremony thirty
                     FROM (SELECT stay.shift,
                                  stay.ceremony,
                                  stay.slowly stare,
                                  stay.plot,
                                  stay.aide severely
                           FROM stay
                          ) assessment
                    ) fantastic
              ) permit ;


''',
  '''
SELECT cease.plot
FROM (SELECT maintenance.stuff,
             maintenance.kitchen alternative,
             nerve.unfortunately strange,
             organizational.plot
      FROM          maintenance
         CROSS JOIN (SELECT product.station deal,
                            product.plot
                     FROM (SELECT anywhere.host station,
                                  anywhere.plot
                           FROM          (SELECT medicine.finish host,
                                                 letter.plot,
                                                 medicine.mean embarrassed
                                          FROM          (SELECT debate.plot,
                                                                debate.frontier
                                                         FROM debate
                                                        ) letter
                                             CROSS JOIN medicine
                                         ) anywhere
                              CROSS JOIN (SELECT invent.weed
                                          FROM (SELECT nearly.standard weed,
                                                       nearly.Internet
                                                FROM nearly
                                               ) invent
                                         ) relation
                              CROSS JOIN (SELECT coming.political fortune,
                                                 coming.surprisingly,
                                                 coming.parking,
                                                 coming.gather best
                                          FROM coming
                                         ) joke
                          ) product
                    ) organizational
         CROSS JOIN nerve
     ) cease


''',
  '''
SELECT young.organ outfit
FROM          (SELECT temporary.cute grocery,
                      temporary.protest moderate
               FROM (SELECT grape.cute,
                            grape.strategic protest,
                            grape.tree severe
                     FROM (SELECT student.interpretation tree,
                                  student.cute,
                                  student.availability strategic
                           FROM student
                          ) grape
                    ) temporary
              ) rain
   CROSS JOIN structure
   CROSS JOIN (SELECT apart.veteran
               FROM (SELECT deadline.lightly veteran
                     FROM deadline
                    ) apart
              ) calendar
   CROSS JOIN (SELECT should.advance,
                      should.friendship,
                      should.pen enroll
               FROM should
              ) gender
   CROSS JOIN (SELECT different.package,
                      different.driving other,
                      different.worker,
                      different.pocket herb
               FROM (SELECT indicator.mine pocket,
                            indicator.heat worker,
                            indicator.package,
                            indicator.council finally,
                            indicator.driving
                     FROM indicator
                    ) different
              ) invisible
   CROSS JOIN young
   CROSS JOIN neighborhood
   CROSS JOIN contribution


''',
  '''
SELECT concerning.context
FROM          outside
   CROSS JOIN (SELECT fantasy.assemble less,
                      fantasy.context,
                      fantasy.change,
                      attention.platform
               FROM          fantasy
                  CROSS JOIN attention
                  CROSS JOIN (SELECT voluntary.youngster directly,
                                     invite.clip,
                                     invite.photograph resistance,
                                     voluntary.hard dumb
                              FROM          (SELECT coal.rare,
                                                    coal.protect,
                                                    coal.youngster,
                                                    coal.popular sentiment,
                                                    coal.terrible depend,
                                                    coal.apologize hero,
                                                    coal.ban,
                                                    coal.hard
                                             FROM coal
                                            ) voluntary
                                 CROSS JOIN invite
                             ) Palestinian
                  CROSS JOIN (SELECT suck.retreat
                              FROM (SELECT response.structure retreat
                                    FROM (SELECT react.somebody structure
                                          FROM (SELECT structural.resolve,
                                                       structural.somebody,
                                                       structural.coalition intervention,
                                                       structural.find,
                                                       structural.coach
                                                FROM structural
                                               ) react
                                         ) response
                                   ) suck
                             ) direct
              ) concerning
   CROSS JOIN (SELECT local.free judge
               FROM local
              ) occupation
   CROSS JOIN turn
   CROSS JOIN (SELECT trouble.club fishing,
                      trouble.craft,
                      trouble.screen,
                      trouble.special,
                      trouble.derive,
                      trouble.technology,
                      trouble.willing sentiment
               FROM trouble
              ) liquid
   CROSS JOIN (SELECT toilet.paper added
               FROM (SELECT sudden.hockey paper
                     FROM (SELECT comment.hockey
                           FROM (SELECT logic.he hockey
                                 FROM (SELECT instantly.blow he
                                       FROM (SELECT associate.gray blow
                                             FROM (SELECT honestly.stir banker,
                                                          honestly.emergency,
                                                          honestly.gray,
                                                          honestly.many,
                                                          honestly.term
                                                   FROM honestly
                                                  ) associate
                                            ) instantly
                                      ) logic
                                ) comment
                          ) sudden
                    ) toilet
              ) daily ;


''',
  '''
SELECT thought.conclude
FROM            (SELECT rush.resistance conclude
                 FROM (SELECT honor.practically ,
                            honor.beauty asleep,
                            honor.resistance,
                            honor.cling surprising
                     FROM (SELECT gut.cook practically,
                                  composition.figure,
                                  composition.eliminate,
                                  gut.beauty,
                                  composition.hot,
                                  composition.resistance,
                                  gut.belt cling
                           FROM         gut
                           CROSS JOIN   (SELECT   fragile.figure,
                                                  fragile.eliminate,
                                                  fragile.hot,
                                                  fragile.draft resistance
                                           FROM (SELECT white.eliminate,
                                                        white.style,
                                                        white.draft,
                                                        white.hot,
                                                        white.ballot figure
                                                 FROM white
                                                ) fragile
                                          ) composition
                          ) honor
                    ) rush
              ) thought
   CROSS JOIN (SELECT blade.impose,
                      blade.harm,
                      blade.abuse,
                      blade.hook
               FROM blade
              ) handle


''',
  '''
SELECT sudden.central
FROM          minister
   CROSS JOIN (SELECT bill.central,
                      bill.above swear
               FROM (SELECT cling.pretend brother,
                            cling.nail central,
                            selection.Japanese above
                     FROM          (SELECT lobby.short industrial,
                                           lobby.vacuum active,
                                           lobby.reinforce pretend,
                                           lobby.reflect Asian,
                                           lobby.shit nail
                                    FROM lobby
                                   ) cling
                        CROSS JOIN (SELECT clue.Japanese
                                    FROM (SELECT expand.Japanese,
                                                 expand.perceive Latin,
                                                 expand.decorate
                                          FROM (SELECT sleeve.glimpse,
                                                       sleeve.decorate,
                                                       sleeve.undergraduate,
                                                       sleeve.Japanese,
                                                       sleeve.puzzle perceive
                                                FROM (SELECT reverse.deeply Japanese,
                                                             reverse.let puzzle,
                                                             reverse.combination decorate,
                                                             reverse.undergraduate,
                                                             reverse.term glimpse
                                                      FROM reverse
                                                     ) sleeve
                                               ) expand
                                         ) clue
                                   ) selection
                    ) bill
              ) sudden


''',
  '''
SELECT reply.pleasant ring
FROM          (SELECT bloody.constitution,
                      aware.star mental,
                      attempt.occasion face,
                      bloody.possibility surprised,
                      attempt.that,
                      attempt.ethnic coast,
                      attempt.wife,
                      aware.cloth apparently
               FROM          aware
                  CROSS JOIN (SELECT remark.wife,
                                     analyst.that,
                                     analyst.basketball broad,
                                     analyst.wake travel,
                                     sexy.occasion,
                                     sexy.finish,
                                     sexy.ride ethnic,
                                     analyst.bench
                              FROM          (SELECT stumble.punish wife
                                             FROM (SELECT fame.punish
                                                   FROM          fame
                                                      CROSS JOIN contact
                                                  ) stumble
                                            ) remark
                                 CROSS JOIN sexy
                                 CROSS JOIN (SELECT argue.that,
                                                    argue.include cab,
                                                    argue.immune basketball,
                                                    argue.ten,
                                                    argue.wake,
                                                    argue.relative bench
                                             FROM (SELECT resort.mail,
                                                          resort.ten,
                                                          resort.wake,
                                                          resort.immune,
                                                          resort.fare that,
                                                          resort.include,
                                                          resort.than explore,
                                                          resort.armed relative
                                                   FROM resort
                                                  ) argue
                                            ) analyst
                             ) attempt
                  CROSS JOIN bloody
              ) headache
   CROSS JOIN (SELECT band.distribution,
                      band.speak dry,
                      band.screen private,
                      band.once plain
               FROM band
              ) shift
   CROSS JOIN (SELECT boot.official bite,
                      boot.greatest pleasant,
                      boot.cup integrity
               FROM (SELECT analysis.sugar associate,
                            analysis.integrate,
                            analysis.official,
                            analysis.cup,
                            analysis.warn ultimate,
                            analysis.curriculum,
                            analysis.cause greatest,
                            analysis.spy entrance
                     FROM analysis
                    ) boot
              ) reply


''',
  '''
SELECT photograph.animal narrative
FROM          justify
   CROSS JOIN glance
   CROSS JOIN photograph
   CROSS JOIN skirt
   CROSS JOIN (SELECT college.changing overlook,
                      college.pull advice,
                      court.abandon,
                      court.rough fast,
                      college.rough,
                      guide.Democrat,
                      guide.highlight,
                      college.mix
               FROM          (SELECT accurate.mortality result,
                                     accurate.highlight,
                                     accurate.however,
                                     accurate.admission jail,
                                     accurate.month,
                                     accurate.Democrat
                              FROM accurate
                             ) guide
                  CROSS JOIN college
                  CROSS JOIN court
              ) efficiency
   CROSS JOIN circuit
   CROSS JOIN (SELECT cold.western anger
               FROM (SELECT priest.council western
                     FROM (SELECT condition.mild council
                           FROM condition
                          ) priest
                    ) cold
              ) close
   CROSS JOIN (SELECT host.scheme,
                      host.poster collaboration
               FROM host
              ) mass
   CROSS JOIN onto


''',
  '''
SELECT seal.belief
FROM          (SELECT ethical.emotional,
                      silver.coordinator crime
               FROM          silver
                  CROSS JOIN (SELECT realistic.peasant cancel,
                                     realistic.against sky,
                                     realistic.public,
                                     realistic.yard talk,
                                     realistic.twenty,
                                     realistic.model
                              FROM realistic
                             ) pleased
                  CROSS JOIN (SELECT city.steel,
                                     city.gym,
                                     city.challenge emotional,
                                     city.advanced,
                                     city.tune
                              FROM (SELECT knee.below gym,
                                           knee.strategy advanced,
                                           knee.nine challenge,
                                           knee.boom steel,
                                           knee.elect tune
                                    FROM knee
                                   ) city
                             ) ethical
              ) efficient
   CROSS JOIN (SELECT loose.salary drop,
                      loose.belief,
                      loose.handsome African,
                      loose.harvest cruel,
                      loose.humanity era,
                      loose.powder gross,
                      loose.seven
               FROM loose
              ) seal ;


''',
  '''
SELECT prohibit.tournament tender
FROM          nuclear
   CROSS JOIN sanction
   CROSS JOIN (SELECT auction.completely answer,
                      improved.clue tournament
               FROM          (SELECT cemetery.this,
                                     cemetery.recommend
                              FROM          bold
                                 CROSS JOIN cemetery
                                 CROSS JOIN (SELECT access.soup,
                                                    access.tray,
                                                    access.closed,
                                                    access.drink description,
                                                    access.facilitate grant,
                                                    access.drawer,
                                                    access.fascinating
                                             FROM access
                                            ) sit
                                 CROSS JOIN (SELECT unemployment.Israeli feedback,
                                                    unemployment.herself
                                             FROM (SELECT motion.landscape herself,
                                                          motion.extent,
                                                          motion.advanced crawl,
                                                          motion.uncle,
                                                          motion.incorporate sufficient,
                                                          motion.priority,
                                                          motion.sport Israeli
                                                   FROM motion
                                                  ) unemployment
                                            ) jet
                             ) headache
                  CROSS JOIN (SELECT fully.career,
                                     fully.outside altogether,
                                     fully.momentum,
                                     fully.recommend,
                                     fully.radio,
                                     fully.home,
                                     rely.implication
                              FROM          (SELECT discrimination.recommend,
                                                    discrimination.lawmaker automobile,
                                                    discrimination.increasing home,
                                                    discrimination.blow momentum,
                                                    discrimination.career,
                                                    discrimination.outside,
                                                    discrimination.radio
                                             FROM discrimination
                                            ) fully
                                 CROSS JOIN (SELECT celebrity.implication
                                             FROM (SELECT steel.implication
                                                   FROM (SELECT rhythm.everything implication
                                                         FROM rhythm
                                                        ) steel
                                                  ) celebrity
                                            ) rely
                             ) yield
                  CROSS JOIN improved
                  CROSS JOIN auction
              ) prohibit
   CROSS JOIN (SELECT Internet.current costly,
                      Internet.action,
                      Internet.relative no,
                      Internet.seat system
               FROM (SELECT adjustment.demographic violation,
                            adjustment.inspiration cure,
                            adjustment.meanwhile,
                            adjustment.current,
                            adjustment.tuck seat,
                            adjustment.action,
                            adjustment.disclose relative
                     FROM adjustment
                    ) Internet
              ) near


''',
  '''
SELECT interview.reaction
FROM          (SELECT crash.survey,
                      provided.reaction,
                      bottom.ring
               FROM          crash
                  CROSS JOIN adjust
                  CROSS JOIN (SELECT dissolve.episode,
                                     dissolve.fear AS reaction
                              FROM (SELECT twice.account,
                                          twice.saving episode,
                                          absolute.reaction AS fear,
                                          twice.pet
                                   FROM            (SELECT private.journalist appreciation,
                                                            private.stable account,
                                                            private.temple saving,
                                                            private.pet
                                                     FROM (SELECT link.fit home,
                                                                  link.officer temple,
                                                                  link.journalist,
                                                                  link.playoff stable,
                                                                  link.pet
                                                           FROM link
                                                          ) private
                                                   ) twice
                                       CROSS JOIN absolute
                                  ) dissolve
                             ) provided
                  CROSS JOIN (SELECT wilderness.ring,
                                     wilderness.fish,
                                     wilderness.tennis
                              FROM            wilderness
                                   CROSS JOIN credit
                             ) bottom
              ) interview
   CROSS JOIN tile


''',
  '''
SELECT once.significance
FROM          once
   CROSS JOIN (SELECT range.process,
                      range.conclude progressive,
                      net.now grandfather,
                      net.at Irish,
                      range.express recipient,
                      range.shift,
                      range.aside,
                      net.curtain
               FROM          (SELECT attack.aside,
                                     permit.tuck process,
                                     permit.express,
                                     permit.shift,
                                     permit.silk revelation,
                                     death.regulatory conclude
                              FROM          death
                                 CROSS JOIN (SELECT carrot.air,
                                                    carrot.wealth silk,
                                                    carrot.pastor symptom,
                                                    carrot.aside award,
                                                    carrot.express,
                                                    carrot.organization shift,
                                                    carrot.shrug lower,
                                                    carrot.tuck
                                             FROM carrot
                                            ) permit
                                 CROSS JOIN attack
                             ) range
                  CROSS JOIN (SELECT distinction.fact curtain,
                                     distinction.puzzle now,
                                     distinction.spin,
                                     distinction.at,
                                     distinction.courage,
                                     distinction.high
                              FROM (SELECT valley.puzzle,
                                           outcome.fact,
                                           valley.courage,
                                           valley.quickly high,
                                           valley.win spin,
                                           outcome.speculate biological,
                                           valley.pad at
                                    FROM          valley
                                       CROSS JOIN (SELECT fascinating.suicide fact,
                                                          fascinating.occasional speculate
                                                   FROM (SELECT glass.suicide,
                                                                glass.conservation occasional
                                                         FROM (SELECT head.attendance suicide,
                                                                      head.conservation
                                                               FROM head
                                                              ) glass
                                                        ) fascinating
                                                  ) outcome
                                   ) distinction
                             ) net
              ) auction
   CROSS JOIN (SELECT scenario.absence,
                      scenario.convince,
                      scenario.extremely,
                      scenario.likelihood weekend,
                      scenario.interrupt civil,
                      scenario.loss dynamics
               FROM scenario
              ) plea
   CROSS JOIN (SELECT cold.alone laugh
               FROM cold
              ) coordinator
   CROSS JOIN (SELECT rider.marble,
                      rider.nature
               FROM (SELECT valuable.retailer hay,
                            valuable.ultimate nature,
                            valuable.sauce facility,
                            valuable.tourist marble,
                            valuable.well spread
                     FROM valuable
                    ) rider
              ) given ;


''',
  '''
SELECT elder.flexibility invade
FROM          (SELECT participant.dance,
                      ratio.scream,
                      bare.resolve officer,
                      ratio.battle flexibility,
                      bare.midnight perceived,
                      participant.recession,
                      participant.seemingly,
                      ratio.Japanese receiver
               FROM          (SELECT criticism.minor missile,
                                     criticism.scream,
                                     criticism.considerably life,
                                     criticism.so Japanese,
                                     criticism.bounce,
                                     criticism.favor,
                                     criticism.battle
                              FROM criticism
                             ) ratio
                  CROSS JOIN (SELECT rub.manufacturing,
                                     rub.rifle spring,
                                     rub.strong,
                                     rub.recession,
                                     rub.tighten dance,
                                     rub.more compelling,
                                     rub.yes seemingly,
                                     rub.explore
                              FROM rub
                             ) participant
                  CROSS JOIN bare
              ) elder
   CROSS JOIN (SELECT fighting.virtually
               FROM (SELECT honor.succeed virtually
                     FROM (SELECT success.fundamental concern,
                                  success.succeed
                           FROM (SELECT rarely.fundamental,
                                        rarely.beside succeed
                                 FROM (SELECT kid.adjust lift,
                                              kid.sword beside,
                                              kid.Dutch fundamental
                                       FROM kid
                                      ) rarely
                                ) success
                          ) honor
                    ) fighting
              ) honor
   CROSS JOIN dad
   CROSS JOIN (SELECT diabetes.reward contribution
               FROM (SELECT progressive.reward
                     FROM (SELECT carpet.reward
                           FROM (SELECT profile.breakfast reward
                                 FROM (SELECT suburb.breakfast
                                       FROM (SELECT severely.retailer breakfast,
                                                    severely.resolution,
                                                    severely.era firm
                                             FROM (SELECT weekly.resolution,
                                                          weekly.retailer,
                                                          weekly.era
                                                   FROM weekly
                                                  ) severely
                                            ) suburb
                                      ) profile
                                ) carpet
                          ) progressive
                    ) diabetes
              ) neighbor
   CROSS JOIN six
   CROSS JOIN (SELECT constitute.terrible ago
               FROM (SELECT coach.representative sales,
                            coach.grin description,
                            coach.rest cost,
                            coach.watch shelter,
                            coach.room,
                            coach.past terrible,
                            coach.ghost,
                            coach.cause everyday
                     FROM coach
                    ) constitute
              ) rim
   CROSS JOIN range
   CROSS JOIN lifetime


''',
  '''
SELECT motion.hook
FROM          (SELECT hurt.intelligence,
                      hurt.closest outfit
               FROM hurt
              ) squad
   CROSS JOIN similar
   CROSS JOIN ground
   CROSS JOIN (SELECT basement.hers
               FROM (SELECT cost.winter,
                            cost.largely,
                            cost.badly hers,
                            cost.lobby,
                            cost.guilt roof,
                            cost.idea worried,
                            cost.provider differently,
                            cost.fire married
                     FROM cost
                    ) basement
              ) frequency
   CROSS JOIN (SELECT action.software lack,
                      action.school flash,
                      action.rib,
                      action.succeed check,
                      action.worth
               FROM action
              ) encouraging
   CROSS JOIN (SELECT walking.eat hook,
                      walking.tendency,
                      walking.chocolate,
                      walking.contact the,
                      walking.reject enroll
               FROM walking
              ) motion
   CROSS JOIN (SELECT accelerate.attack,
                      accelerate.touch lake,
                      accelerate.interesting cliff,
                      accelerate.wonder
               FROM (SELECT filter.forward,
                            filter.interesting,
                            filter.collector attack,
                            filter.wonder,
                            filter.touch
                     FROM filter
                    ) accelerate
              ) future
   CROSS JOIN (SELECT bear.dramatic organ
               FROM (SELECT extra.dramatic
                     FROM (SELECT attitude.dramatic
                           FROM (SELECT recommendation.dramatic
                                 FROM (SELECT immigration.Ms dramatic
                                       FROM (SELECT cow.Ms
                                             FROM (SELECT notice.contributor,
                                                          notice.grateful,
                                                          notice.variation Ms,
                                                          notice.young cold,
                                                          notice.nice
                                                   FROM notice
                                                  ) cow
                                            ) immigration
                                      ) recommendation
                                ) attitude
                          ) extra
                    ) bear
              ) cooperate
   CROSS JOIN issue ;


''',
  '''
SELECT float.sample
FROM          (SELECT house.their,
                      accommodate.formal helicopter,
                      house.drunk
               FROM          (SELECT elegant.buyer
                              FROM elegant
                             ) face
                  CROSS JOIN accommodate
                  CROSS JOIN house
              ) plan
   CROSS JOIN (SELECT satisfy.glory,
                      satisfy.knife
               FROM satisfy
              ) pot

   CROSS JOIN (SELECT Japanese.spectrum discount,
                      Japanese.sample
               FROM (SELECT alter.trading spectrum,
                            alter.casualty,
                            alter.sample
                     FROM (SELECT kind.jury,
                                  kind.trading,
                                  extremely.sample,
                                  extremely.casualty
                           FROM             kind
                                CROSS JOIN (SELECT upstairs.sample,
                                                  upstairs.casualty
                                           FROM (SELECT orientation.sample,
                                                        orientation.ancestor casualty
                                                 FROM orientation
                                                ) upstairs
                                          ) extremely
                          ) alter
                    ) Japanese
              ) float
   CROSS JOIN commitment ;


''',
  '''
SELECT afraid.gender
FROM          (SELECT south.industry unfair
               FROM (SELECT smile.forbid industry
                     FROM          smile
                        CROSS JOIN lesson
                    ) south
              ) Spanish
   CROSS JOIN (SELECT fifteen.broadcast,
                      fifteen.station concert,
                      maintenance.needle enroll,
                      fifteen.aid
               FROM          maintenance
                  CROSS JOIN (SELECT assist.clothes station,
                                     assist.metal aid,
                                     assist.robot broadcast,
                                     assist.secure,
                                     assist.solve,
                                     assist.drag suitable,
                                     assist.east
                              FROM assist
                             ) fifteen
              ) volunteer
   CROSS JOIN regulatory
   CROSS JOIN (SELECT worth.grandchild AS gender,
                      cease.circle
               FROM          (SELECT admit.treaty grandchild
                              FROM (SELECT guilt.treaty
                                    FROM guilt
                                   ) admit
                             ) worth
                  CROSS JOIN (SELECT striking.circle
                              FROM striking
                             ) cease
              ) afraid
   CROSS JOIN emotion


''',
  '''
SELECT impose.here
FROM          (SELECT devastating.retain,
                      such.rely herb,
                      calm.weave isolation,
                      devastating.widespread
               FROM          supportive
                  CROSS JOIN past
                  CROSS JOIN (SELECT square.respect,
                                     square.consent,
                                     square.four,
                                     square.ridge weave,
                                     square.diary
                              FROM square
                             ) calm
                  CROSS JOIN such
                  CROSS JOIN devastating
              ) commitment
   CROSS JOIN vital
   CROSS JOIN (SELECT fact.black informal,
                      improved.nearby,
                      fact.knife top,
                      fact.bat here,
                      fact.rape summary,
                      fact.cancer
               FROM          fact
                  CROSS JOIN (SELECT rank.man,
                                     rank.learning molecule,
                                     rank.original link
                              FROM (SELECT main.behavior learning,
                                           main.rare original,
                                           main.steep characteristic,
                                           main.boost,
                                           main.man,
                                           main.other mark
                                    FROM main
                                   ) rank
                             ) travel
                  CROSS JOIN (SELECT canvas.nearby
                              FROM (SELECT beard.ridiculous nearby
                                    FROM (SELECT watch.terribly ridiculous
                                          FROM (SELECT technician.maybe,
                                                       technician.reception,
                                                       technician.quit retired,
                                                       technician.terribly,
                                                       technician.another habitat,
                                                       technician.articulate,
                                                       technician.carry,
                                                       technician.dead
                                                FROM technician
                                               ) watch
                                         ) beard
                                   ) canvas
                             ) improved
              ) impose
   CROSS JOIN (SELECT actually.arrest convinced,
                      actually.at,
                      actually.those typically,
                      actually.beginning,
                      actually.brief supportive
               FROM (SELECT retain.Chinese,
                            retain.beginning,
                            retain.those,
                            retain.forward brief,
                            retain.arrest,
                            retain.at
                     FROM retain
                    ) actually
              ) guide


''',
  '''
SELECT extended.since
FROM          extended
   CROSS JOIN (SELECT lot.plenty hook,
                      must.worldwide,
                      must.will library,
                      lot.edition trigger,
                      must.momentum trust
               FROM          lot
                  CROSS JOIN must
              ) mention
   CROSS JOIN guarantee
   CROSS JOIN kind
   CROSS JOIN (SELECT plate.independence help,
                      plate.swim maker,
                      plate.danger,
                      plate.tolerate emerge
               FROM (SELECT ready.independence,
                            ready.Asian danger,
                            ready.poor swim,
                            ready.statue tolerate
                     FROM (SELECT permit.guard statue,
                                  permit.toss independence,
                                  permit.hence Asian,
                                  permit.body,
                                  permit.poor,
                                  permit.start
                           FROM permit
                          ) ready
                    ) plate
              ) wow
   CROSS JOIN (SELECT requirement.spread occur,
                      requirement.helmet,
                      requirement.respondent hockey
               FROM (SELECT wealth.van,
                            wealth.spread,
                            wealth.likely,
                            wealth.respondent,
                            wealth.tale resist,
                            wealth.table,
                            ownership.song helmet,
                            wealth.finance
                     FROM          wealth
                        CROSS JOIN (SELECT Democrat.stay song
                                    FROM (SELECT university.preliminary stay,
                                                 university.mode rumor,
                                                 university.relieve
                                          FROM (SELECT menu.preliminary,
                                                       menu.crime relieve,
                                                       menu.unusual,
                                                       menu.gut grab,
                                                       menu.electricity,
                                                       menu.chamber,
                                                       menu.structure mode,
                                                       menu.spine
                                                FROM menu
                                               ) university
                                         ) Democrat
                                   ) ownership
                    ) requirement
              ) over
   CROSS JOIN (SELECT conclude.organ holy,
                      conclude.instrument ski,
                      conclude.up liquid,
                      conclude.play,
                      conclude.maximum,
                      conclude.generally
               FROM conclude
              ) part


''',
  '''
SELECT desire.civic
FROM          (SELECT regular.glory,
                      fierce.kiss,
                      regular.skirt signal
               FROM          (SELECT deer.reason delay,
                                     deer.kiss,
                                     deer.uncertain rehabilitation,
                                     deer.purchase,
                                     deer.hour grape
                              FROM (SELECT card.kiss,
                                           outline.clerk,
                                           card.best uncertain,
                                           outline.hour,
                                           outline.purchase,
                                           actively.reason
                                    FROM          (SELECT roll.kiss,
                                                          creature.so chamber,
                                                          roll.practical present,
                                                          roll.denial,
                                                          creature.acceptance best,
                                                          creature.unfortunately,
                                                          roll.overwhelming
                                                   FROM          creature
                                                      CROSS JOIN (SELECT removal.relative overwhelming,
                                                                         removal.painter kiss,
                                                                         removal.wow,
                                                                         removal.practical,
                                                                         removal.if,
                                                                         removal.denial
                                                                  FROM removal
                                                                 ) roll
                                                  ) card
                                       CROSS JOIN (SELECT processor.abortion touch,
                                                          processor.owe reason,
                                                          processor.pressure subsequent,
                                                          transfer.politically classify
                                                   FROM          processor
                                                      CROSS JOIN (SELECT vote.level politically
                                                                  FROM (SELECT documentary.level
                                                                        FROM (SELECT firm.level
                                                                              FROM (SELECT universal.ownership kick,
                                                                                           universal.consent level,
                                                                                           universal.lightning,
                                                                                           universal.troop
                                                                                    FROM universal
                                                                                   ) firm
                                                                             ) documentary
                                                                       ) vote
                                                                 ) transfer
                                                  ) actively
                                       CROSS JOIN outline
                                   ) deer
                             ) fierce
                  CROSS JOIN shove
                  CROSS JOIN (SELECT tight.inspiration tomato,
                                     tight.rent,
                                     tight.golden wherever,
                                     tight.skirt,
                                     tight.glory,
                                     tight.garbage poke,
                                     tight.most,
                                     tight.historian organ
                              FROM tight
                             ) regular
                  CROSS JOIN added
                  CROSS JOIN actively
              ) scratch
   CROSS JOIN desire


''',
  '''
SELECT counter.mixed
FROM          (SELECT can.escape immediate,
                      can.pure click,
                      can.wander separate,
                      broadcast.elsewhere,
                      broadcast.okay,
                      broadcast.compromise American,
                      broadcast.preliminary king
               FROM          broadcast
                  CROSS JOIN can
              ) nail
   CROSS JOIN (SELECT item.defeat,
                      item.interpretation,
                      item.precise constant,
                      item.holy harassment,
                      item.aim,
                      item.occur basis
               FROM item
              ) enormous
   CROSS JOIN rapid
   CROSS JOIN (SELECT search.you
               FROM (SELECT tax.award,
                            tax.mobile,
                            tax.bitter calm,
                            tax.animal slice,
                            tax.produce you,
                            tax.past,
                            tax.across,
                            tax.cave
                     FROM tax
                    ) search
              ) him
   CROSS JOIN (SELECT leather.journalism scientist
               FROM (SELECT constant.elder journalism
                     FROM (SELECT change.complicated elder
                           FROM change
                          ) constant
                    ) leather
              ) pasta
   CROSS JOIN (SELECT publicity.hand alternative,
                      publicity.pop,
                      publicity.transaction
               FROM (SELECT hormone.pop,
                            hormone.such transaction,
                            hormone.lamp hand,
                            hormone.track
                     FROM (SELECT biological.maintenance lamp,
                                  biological.fortunately pop,
                                  biological.murder track,
                                  biological.tune such
                           FROM biological
                          ) hormone
                    ) publicity
              ) lab
   CROSS JOIN (SELECT suspend.emotion glance,
                      suspend.nutrient mushroom
               FROM suspend
              ) slide
   CROSS JOIN (SELECT opera.try wildlife,
                      opera.opt could,
                      opera.curriculum cost,
                      opera.schedule,
                      opera.clean automatic
               FROM (SELECT gathering.clean,
                            gathering.treatment try,
                            gathering.schedule,
                            gathering.agreement curriculum,
                            gathering.similarly opt
                     FROM gathering
                    ) opera
              ) moment
   CROSS JOIN (SELECT constitutional.glad,
                      constitutional.associate flip,
                      constitutional.teach whole,
                      constitutional.opponent,
                      constitutional.legitimate homeless,
                      constitutional.visual supervisor
               FROM constitutional
              ) register
   CROSS JOIN counter ;


''',
  '''
SELECT bow.key
FROM          (SELECT veteran.insist,
                      veteran.closest
               FROM veteran
              ) committee
   CROSS JOIN (SELECT withdrawal.brother,
                      withdrawal.ideal claim,
                      withdrawal.nearby hat,
                      withdrawal.English,
                      withdrawal.too
               FROM withdrawal
              ) partner
   CROSS JOIN plot
   CROSS JOIN neighborhood
   CROSS JOIN maintain
   CROSS JOIN (SELECT player.key
               FROM player
              ) bow
   CROSS JOIN (SELECT therefore.scent,
                      therefore.delay,
                      therefore.different,
                      therefore.traditional quest
               FROM therefore
              ) club
   CROSS JOIN (SELECT government.shout tender,
                      government.myth vote,
                      government.bold
               FROM (SELECT emerging.guide shout,
                            emerging.international wise,
                            emerging.eating bold,
                            emerging.accurate myth,
                            emerging.tile
                     FROM emerging
                    ) government
              ) detail


''',
  '''
SELECT lack.few
FROM          (SELECT learning.piece,
                      trip.button,
                      trip.one personality,
                      trip.pleasure,
                      trip.counter loud,
                      trip.investigator,
                      learning.under
               FROM          trip
                  CROSS JOIN learning
              ) suspicion
   CROSS JOIN (SELECT free.soon announcement,
                      free.return play,
                      roll.steady separation,
                      free.month,
                      free.wind offensive,
                      free.major,
                      roll.immediate ring
               FROM          free
                  CROSS JOIN roll
              ) crisis
   CROSS JOIN (SELECT reward.sign lose,
                      reward.few,
                      reward.player,
                      reward.helpful constitute
               FROM reward
              ) lack
   CROSS JOIN (SELECT safety.cool,
                      safety.judge comfortable
               FROM (SELECT carpet.sexual cool,
                            carpet.Thanksgiving,
                            carpet.judge
                     FROM (SELECT arise.range judge,
                                  arise.forty Thanksgiving,
                                  arise.differ disappointment,
                                  arise.sexual
                           FROM arise
                          ) carpet
                    ) safety
              ) happily
   CROSS JOIN (SELECT entitle.shy,
                      entitle.present,
                      entitle.hook strip,
                      entitle.schedule waste,
                      entitle.nation suspicious,
                      entitle.ideology,
                      entitle.Japanese ancestor,
                      entitle.ongoing exhibition
               FROM entitle
              ) stand
   CROSS JOIN closet
   CROSS JOIN (SELECT God.due,
                      God.cup nod,
                      God.manner Jewish,
                      God.bet,
                      God.present
               FROM God
              ) become
   CROSS JOIN (SELECT spending.explain place,
                      spending.profit blond
               FROM spending
              ) intact
   CROSS JOIN (SELECT substantial.confession,
                      substantial.bug,
                      substantial.judicial
               FROM substantial
              ) strength ;


''',
  '''
SELECT announcement.ecosystem
FROM          (SELECT used.drop,
                      used.palace ecosystem
               FROM (SELECT neutral.drop,
                            neutral.general palace
                     FROM (SELECT undergraduate.drop,
                                  easy.general
                           FROM             easy
                                CROSS JOIN (SELECT importance.drop
                                 FROM (SELECT specific.scholar drop,
                                              specific.change,
                                              specific.switch distract
                                       FROM (SELECT house.particular,
                                                    house.release uncomfortable,
                                                    house.change,
                                                    house.costume switch,
                                                    house.scholar
                                             FROM house
                                            ) specific
                                      ) importance
                                ) undergraduate
                          ) neutral
                    ) used
              ) announcement
   CROSS JOIN (SELECT starter.bride occupy,
                      starter.input regard,
                      starter.hardly artificial
               FROM (SELECT driveway.bride,
                            driveway.living whoever,
                            driveway.input,
                            driveway.merely arrest,
                            driveway.hardly
                     FROM driveway
                    ) starter
              ) forget


''',
  '''
SELECT resort.hunter
FROM          integrity

   CROSS JOIN (SELECT doubt.minority cabin,
                      doubt.hunter
               FROM (SELECT patrol.minority,
                            angle.hunter,
                            angle.past
                     FROM               (SELECT fifty.uh minority,
                                                fifty.tree
                                         FROM fifty
                                        ) patrol
                             CROSS JOIN fabric
                             CROSS JOIN bid
                             CROSS JOIN (SELECT manual.calm refuge,
                                                investigation.wheel,
                                                manual.nuclear ,
                                                investigation.book hunter,
                                                manual.tent,
                                                investigation.pet,
                                                manual.quest past,
                                                investigation.repair
                                         FROM               manual
                                                CROSS JOIN (SELECT combat.winter book,
                                                                  improvement.strike wheel,
                                                                  improvement.repair,
                                                                  improvement.pet
                                                           FROM          improvement
                                                              CROSS JOIN (SELECT install.winter
                                                                          FROM install
                                                                         ) combat
                                                          ) investigation
                                        ) angle
                    ) doubt
              ) resort
   CROSS JOIN mild


''',
  '''
SELECT opposition.both
FROM          (SELECT loyalty.anxious,
                      loyalty.wide,
                      strategic.disorder,
                      aid.drink philosophical,
                      apology.because stack,
                      loyalty.entry regional,
                      apology.fluid
               FROM          (SELECT fence.disorder,
                                     fence.criminal donation
                              FROM (SELECT Dutch.criminal,
                                           Dutch.chemical disorder
                                    FROM (SELECT throw.glass criminal,
                                                 throw.elite chemical,
                                                 throw.conflict
                                          FROM throw
                                         ) Dutch
                                   ) fence
                             ) strategic
                  CROSS JOIN (SELECT waste.necessarily,
                                     waste.accessible essence
                              FROM (SELECT finance.lens accessible,
                                           finance.type necessarily
                                    FROM finance
                                   ) waste
                             ) public
                  CROSS JOIN apology
                  CROSS JOIN (SELECT big.drink,
                                     big.butter pray,
                                     big.cruise pork
                              FROM (SELECT researcher.behavioral,
                                           researcher.drink,
                                           researcher.cruise,
                                           researcher.butter
                                    FROM (SELECT cotton.cruise,
                                                 cotton.sensitivity butter,
                                                 cotton.drink,
                                                 cotton.contact technical,
                                                 cotton.inevitable behavioral
                                          FROM cotton
                                         ) researcher
                                   ) big
                             ) aid
                  CROSS JOIN loyalty
              ) no
   CROSS JOIN (SELECT gross.both
               FROM (SELECT carbon.both
                     FROM (SELECT bubble.fee commit,
                                  bubble.runner both,
                                  bubble.feature
                           FROM (SELECT Canadian.runner,
                                        Canadian.royal suspend,
                                        Canadian.feature,
                                        Canadian.fee,
                                        Canadian.profile,
                                        Canadian.fool
                                 FROM (SELECT nasty.back fool,
                                              nasty.gross royal,
                                              nasty.finally runner,
                                              nasty.stare evolve,
                                              nasty.ruin profile,
                                              nasty.odds fee,
                                              nasty.brave feature
                                       FROM nasty
                                      ) Canadian
                                ) bubble
                          ) carbon
                    ) gross
              ) opposition
   CROSS JOIN swing ;


''',
  '''
SELECT bacteria.substance
FROM          (SELECT final.adjustment salad,
                      final.pill graduation,
                      final.delicate,
                      final.child why
               FROM (SELECT tolerate.at pill,
                            tolerate.descend huh,
                            tolerate.tough,
                            tolerate.boil delicate,
                            tolerate.child,
                            tolerate.adjustment
                     FROM (SELECT constitute.boil,
                                  constitute.up tough,
                                  constitute.habitat painting,
                                  constitute.child,
                                  constitute.utility at,
                                  constitute.adjustment,
                                  constitute.launch descend
                           FROM constitute
                          ) tolerate
                    ) final
              ) workout
   CROSS JOIN (SELECT powerful.condition,
                      powerful.suppose opening,
                      powerful.reduce break
               FROM (SELECT potential.prosecution reduce,
                            potential.his suppose,
                            potential.wife condition
                     FROM potential
                    ) powerful
              ) gifted
   CROSS JOIN (SELECT specifically.phone
               FROM (SELECT course.phone
                     FROM (SELECT animal.phone
                           FROM (SELECT period.hopefully phone
                                 FROM (SELECT major.tablespoon,
                                              major.stop nonprofit,
                                              major.produce home,
                                              major.performer,
                                              major.relative premium,
                                              major.refer,
                                              major.merely hopefully,
                                              major.operation democracy
                                       FROM major
                                      ) period
                                ) animal
                          ) course
                    ) specifically
              ) handle
   CROSS JOIN (SELECT sign.struggle,
                      sign.substance
               FROM (SELECT consensus.struggle,
                            consensus.substance,
                            consensus.mixed laughter
                     FROM consensus
                    ) sign
              ) bacteria
   CROSS JOIN (SELECT girl.nobody
               FROM (SELECT general.setting nobody,
                            general.known
                     FROM (SELECT hint.male setting,
                                  hint.laugh routinely,
                                  hint.driver key,
                                  hint.stiff assign,
                                  hint.laser known
                           FROM hint
                          ) general
                    ) girl
              ) tension
   CROSS JOIN pollution ;


''',
  '''
SELECT credit.him
FROM          faculty
   CROSS JOIN classify
   CROSS JOIN (SELECT attempt.level fit
               FROM          (SELECT practice.forward
                              FROM          (SELECT camp.attend altogether,
                                                    camp.strain fame,
                                                    camp.fixed message
                                             FROM (SELECT north.AM fixed,
                                                          north.attend,
                                                          north.strain
                                                   FROM north
                                                  ) camp
                                            ) apology
                                 CROSS JOIN (SELECT resort.afford dock,
                                                    resort.forward
                                             FROM (SELECT bath.study,
                                                          bath.afford,
                                                          bath.forward,
                                                          bath.team,
                                                          bath.protocol
                                                   FROM bath
                                                  ) resort
                                            ) practice
                             ) kind
                  CROSS JOIN attempt
              ) writer
   CROSS JOIN advice
   CROSS JOIN (SELECT pen.him
               FROM pen
              ) credit
   CROSS JOIN harassment
   CROSS JOIN solely
   CROSS JOIN legacy
   CROSS JOIN matter


''',
  '''
SELECT sometimes.inside whole
FROM          (SELECT decline.better,
                      prescription.translate
               FROM          (SELECT performer.volunteer translate,
                                     performer.lifestyle
                              FROM          regulation
                                 CROSS JOIN performer
                                 CROSS JOIN (SELECT recent.outline vertical,
                                                    recent.dynamic,
                                                    maintain.pickup flash,
                                                    constitute.spark,
                                                    smart.slam,
                                                    recent.diary debate
                                             FROM          (SELECT plain.legislative prove,
                                                                   plain.present which,
                                                                   plain.pickup
                                                            FROM plain
                                                           ) maintain
                                                CROSS JOIN recent
                                                CROSS JOIN (SELECT hour.spark
                                                            FROM (SELECT suit.shape spark
                                                                  FROM (SELECT provide.star shape
                                                                        FROM provide
                                                                       ) suit
                                                                 ) hour
                                                           ) constitute
                                                CROSS JOIN (SELECT criticism.marine,
                                                                   criticism.along,
                                                                   criticism.rapidly,
                                                                   criticism.slam,
                                                                   criticism.shout reserve,
                                                                   criticism.knee,
                                                                   criticism.hurricane nature
                                                            FROM (SELECT break.horrible knee,
                                                                         break.marine,
                                                                         break.mention shout,
                                                                         break.scramble lend,
                                                                         break.flee rapidly,
                                                                         break.slam,
                                                                         break.beyond hurricane,
                                                                         break.along
                                                                  FROM break
                                                                 ) criticism
                                                           ) smart
                                            ) pink
                             ) prescription
                  CROSS JOIN (SELECT phrase.freeze theory,
                                     phrase.jungle resume,
                                     phrase.bonus beat,
                                     phrase.prior rescue,
                                     phrase.leaf,
                                     phrase.core,
                                     phrase.secretary could
                              FROM phrase
                             ) definition
                  CROSS JOIN decline
                  CROSS JOIN huge
              ) grant
   CROSS JOIN (SELECT structure.poster,
                      structure.inside,
                      structure.fur,
                      structure.payment coin
               FROM (SELECT tree.show inside,
                            tree.payment,
                            tree.fur,
                            tree.pretty whatever,
                            tree.poster
                     FROM tree
                    ) structure
              ) sometimes ;


''',
  '''
SELECT wood.playoff discrimination
FROM          (SELECT interior.celebrate playoff
               FROM (SELECT event.healthy celebrate,
                            event.combat
                     FROM (SELECT boyfriend.healthy,
                                  boyfriend.mess entertainment,
                                  boyfriend.hill combat
                           FROM (SELECT punch.ahead,
                                        punch.healthy,
                                        punch.visit reliability,
                                        punch.hill,
                                        punch.special mess
                                 FROM punch
                                ) boyfriend
                          ) event
                    ) interior
              ) wood
   CROSS JOIN (SELECT mess.apart stance,
                      mess.inside,
                      mess.modern,
                      mess.public
               FROM mess
              ) I
   CROSS JOIN (SELECT fraud.big deadly,
                      fraud.arrest at,
                      fraud.war execute
               FROM (SELECT doctrine.limb rain,
                            doctrine.ongoing,
                            doctrine.myth war,
                            doctrine.terms,
                            doctrine.undergo arrest,
                            doctrine.crack,
                            doctrine.due,
                            doctrine.occupation big
                     FROM doctrine
                    ) fraud
              ) black


''',
  '''
SELECT worried.behind
FROM          (SELECT statistical.nest elaborate,
                      borrow.edge,
                      borrow.cake institutional,
                      borrow.seat,
                      borrow.unemployment unfold,
                      borrow.example,
                      borrow.ordinary
               FROM          (SELECT shift.seat,
                                     shift.conference cake,
                                     shift.example,
                                     personnel.satisfy unemployment,
                                     shift.ordinary,
                                     shift.guide,
                                     shift.edge
                              FROM          (SELECT toy.seat,
                                                    toy.slight ordinary,
                                                    toy.nonprofit,
                                                    toy.possession example,
                                                    toy.edge,
                                                    toy.fifth guide,
                                                    toy.expensive conference,
                                                    toy.rough engineer
                                             FROM toy
                                            ) shift
                                 CROSS JOIN (SELECT foot.pioneer punish,
                                                    foot.warm,
                                                    foot.alike satisfy
                                             FROM foot
                                            ) personnel
                             ) borrow
                  CROSS JOIN (SELECT constraint.familiar nest
                              FROM          (SELECT difficulty.volume,
                                                    difficulty.can exception
                                             FROM          guarantee
                                                CROSS JOIN (SELECT counter.cousin can,
                                                                   counter.volume
                                                            FROM (SELECT promotion.valley nature,
                                                                         promotion.tremendous activity,
                                                                         promotion.pipe volume,
                                                                         promotion.gene condition,
                                                                         promotion.card,
                                                                         promotion.period fucking,
                                                                         promotion.assign cousin
                                                                  FROM promotion
                                                                 ) counter
                                                           ) difficulty
                                            ) persist
                                 CROSS JOIN (SELECT downtown.experienced phase,
                                                    downtown.survival claim
                                             FROM (SELECT eight.international drown,
                                                          eight.whether experienced,
                                                          eight.survival
                                                   FROM (SELECT central.female,
                                                                central.counter whether,
                                                                central.sentence specify,
                                                                central.organizational international,
                                                                central.globe,
                                                                central.oven part,
                                                                central.rest cemetery,
                                                                central.survival
                                                         FROM central
                                                        ) eight
                                                  ) downtown
                                            ) criticize
                                 CROSS JOIN (SELECT depend.molecule
                                             FROM (SELECT appear.molecule
                                                   FROM appear
                                                  ) depend
                                            ) bedroom
                                 CROSS JOIN constraint
                             ) statistical
              ) scale
   CROSS JOIN worried ;


''',
  '''
SELECT catch.soak
FROM (SELECT story.improved evaluation,
             bind.soak,
             bind.evaluate,
             bind.fund,
             bind.ceremony,
             supervisor.bedroom,
             story.size
      FROM          story
         CROSS JOIN (SELECT ideal.bedroom
                     FROM (SELECT cook.work bedroom
                           FROM (SELECT winter.asleep work,
                                        winter.divine
                                 FROM (SELECT enthusiasm.asleep,
                                              enthusiasm.divine
                                       FROM enthusiasm
                                      ) winter
                                ) cook
                          ) ideal
                    ) supervisor

         CROSS JOIN (SELECT few.soak,
                             institutional.violate evaluate,
                             online.fund,
                             online.ceremony,
                              online.freshman

                     FROM (SELECT respect.soak
                           FROM respect
                          ) few
                          CROSS JOIN strip
                           CROSS JOIN institutional
                           CROSS JOIN laser
                           CROSS JOIN online
                    ) bind
         CROSS JOIN environmental
     ) catch ;


''',
  '''
SELECT bloody.current
FROM          (SELECT nasty.account discourage,
                      nasty.catalog cake,
                      nasty.assignment differ,
                      nasty.know,
                      nasty.convinced,
                      nasty.behave doubt,
                      nasty.fighting bless,
                      nasty.phenomenon
               FROM nasty
              ) mess
   CROSS JOIN (SELECT solution.current
               FROM (SELECT crowded.current
                     FROM (SELECT across.comparison,
                                  public.current
                           FROM             (SELECT proper.comparison,
                                                    proper.photograph
                                             FROM (SELECT salary.vocal,
                                                          salary.comparison,
                                                          salary.photograph,
                                                          salary.foster,
                                                          salary.area
                                                   FROM (SELECT intend.shower foster,
                                                                intend.abstract photograph,
                                                                intend.comparison,
                                                                intend.bless area,
                                                                intend.vocal
                                                         FROM intend
                                                        ) salary
                                                  ) proper
                                            ) across
                                CROSS JOIN  public
                          ) crowded
                    ) solution
              ) bloody


''',
  '''
SELECT client.honey
FROM          (SELECT entry.honey,
                      bunch.soil harassment,
                      entry.astronomer damage
               FROM          bunch
                  CROSS JOIN tap
                  CROSS JOIN (SELECT nearby.romance army,
                                     nearby.literally honey,
                                     nearby.astronomer
                              FROM (SELECT session.duck romance,
                                           session.astronomer,
                                           close.literally,
                                           session.organism flow,
                                           session.inspiration
                                    FROM        session
                                    CROSS JOIN  (SELECT soon.study literally,
                                                        soon.market,
                                                        soon.exchange
                                                 FROM (SELECT stretch.market,
                                                              stretch.accomplish ,
                                                              stretch.mixed,
                                                              stretch.exchange,
                                                              stretch.study
                                                       FROM stretch
                                                      ) soon
                                                ) close
                                   ) nearby
                             ) entry
              ) client
   CROSS JOIN talent
   CROSS JOIN (SELECT occasionally.Olympics
               FROM occasionally
              ) collective
   CROSS JOIN project
   CROSS JOIN gradually


''',
  '''
SELECT transfer.part
FROM          (SELECT mail.uncertain attack,
                      mail.available fail,
                      mail.diary range,
                      mail.style
               FROM (SELECT nerve.denial diary,
                            nerve.frame,
                            nerve.complex uncertain,
                            nerve.shell available,
                            nerve.style
                     FROM nerve
                    ) mail
              ) bathroom
   CROSS JOIN transfer
   CROSS JOIN (SELECT cultural.season shoot,
                      liberal.coastal,
                      liberal.indicate spread
               FROM          (SELECT arrival.attach somehow,
                                     arrival.calculation easy,
                                     arrival.practical map,
                                     arrival.mutual season,
                                     arrival.sword
                              FROM arrival
                             ) cultural
                  CROSS JOIN (SELECT presidential.diet milk,
                                     presidential.newspaper coastal,
                                     presidential.living,
                                     presidential.decent indicate,
                                     presidential.reward,
                                     presidential.frequency,
                                     presidential.break approval
                              FROM presidential
                             ) liberal
              ) overwhelm
   CROSS JOIN inform
   CROSS JOIN mostly ;


''',
  '''
SELECT smoke.concede defeat
FROM          (SELECT deem.likewise against,
                      president.concede,
                      deem.person,
                      president.mortgage horse,
                      president.translation,
                      ceiling.personnel at,
                      president.virus crazy,
                      president.identification mate
               FROM          president
                  CROSS JOIN (SELECT test.person,
                                     test.likewise
                              FROM (SELECT aisle.denial likewise,
                                           aisle.realm person
                                    FROM (SELECT regard.realm,
                                                 tourism.denial
                                          FROM          tourism
                                             CROSS JOIN (SELECT place.realm,
                                                                place.wheelchair
                                                         FROM (SELECT cancel.toss realm,
                                                                      cancel.butterfly,
                                                                      exact.wheelchair
                                                               FROM          (SELECT drive.debate,
                                                                                     drive.secondary,
                                                                                     drive.firm,
                                                                                     drive.gaze bubble,
                                                                                     drive.turn,
                                                                                     drive.surprise wheelchair,
                                                                                     drive.sink tune
                                                                              FROM drive
                                                                             ) exact
                                                                  CROSS JOIN cancel
                                                              ) place
                                                        ) regard
                                             CROSS JOIN (SELECT oral.rather,
                                                                oral.graduate trash,
                                                                oral.forty,
                                                                oral.soften,
                                                                oral.familiar
                                                         FROM (SELECT consider.familiar,
                                                                      consider.range,
                                                                      consider.soften,
                                                                      consider.overnight forty,
                                                                      consider.monkey rather,
                                                                      consider.graduate
                                                               FROM consider
                                                              ) oral
                                                        ) wine
                                             CROSS JOIN (SELECT smart.dust refuge,
                                                                smart.through exam,
                                                                smart.part,
                                                                smart.no
                                                         FROM (SELECT taste.punch dust,
                                                                      taste.depending through,
                                                                      taste.chop part,
                                                                      taste.no
                                                               FROM taste
                                                              ) smart
                                                        ) mother
                                         ) aisle
                                   ) test
                             ) deem
                  CROSS JOIN (SELECT era.ensure personnel
                              FROM (SELECT rather.sustainable statistical,
                                           rather.publicity earn,
                                           rather.ensure,
                                           rather.expand
                                    FROM rather
                                   ) era
                             ) ceiling
              ) smoke
   CROSS JOIN (SELECT poster.fund,
                      poster.additional,
                      poster.command dancer
               FROM poster
              ) signal
   CROSS JOIN vary


''',
  '''
SELECT father.investigate male
FROM          themselves
   CROSS JOIN (SELECT scholar.accomplish contrast,
                      scholar.nomination developmental,
                      scholar.tissue oil,
                      reader.significant,
                      scholar.creative
               FROM          (SELECT chase.rank tissue,
                                     chase.growth creative,
                                     post.nomination,
                                     post.however academic,
                                     exhaust.broadcast greatest,
                                     chase.nonetheless accomplish
                              FROM          chase
                                 CROSS JOIN (SELECT pan.tonight
                                             FROM (SELECT alternative.program tonight
                                                   FROM (SELECT neck.hi program
                                                         FROM neck
                                                        ) alternative
                                                  ) pan
                                            ) exhibit
                                 CROSS JOIN (SELECT may.versus broadcast,
                                                    may.etc organism,
                                                    may.past
                                             FROM (SELECT participant.missing,
                                                          participant.leave,
                                                          participant.versus,
                                                          participant.off,
                                                          participant.resign etc,
                                                          participant.past
                                                   FROM participant
                                                  ) may
                                            ) exhaust
                                 CROSS JOIN post
                             ) scholar
                  CROSS JOIN (SELECT yet.significant,
                                     yet.beg genuine
                              FROM (SELECT copy.beg,
                                           copy.paint significant
                                    FROM (SELECT cattle.salmon beg,
                                                 cattle.chain economically,
                                                 cattle.committee soon,
                                                 cattle.mix paint
                                          FROM (SELECT boy.committee,
                                                       boy.willingness after,
                                                       boy.consecutive tall,
                                                       boy.private,
                                                       boy.chain,
                                                       boy.mix,
                                                       boy.transit vulnerable,
                                                       boy.until salmon
                                                FROM boy
                                               ) cattle
                                         ) copy
                                   ) yet
                             ) reader
              ) model
   CROSS JOIN (SELECT hour.media witness,
                      hour.waste
               FROM (SELECT alarm.media,
                            alarm.waste
                     FROM alarm
                    ) hour
              ) spending
   CROSS JOIN pill
   CROSS JOIN father


''',
  '''
SELECT processing.twelve bless
FROM          (SELECT later.arrival president,
                      later.commonly,
                      carve.conscience,
                      forbid.housing,
                      wound.fare chest
               FROM          (SELECT creativity.disappear commonly,
                                     major.arrival,
                                     major.scent,
                                     focus.father,
                                     focus.warrior,
                                     focus.counselor photography,
                                     major.injury satisfaction
                              FROM          (SELECT kiss.related disappear,
                                                    kiss.test
                                             FROM kiss
                                            ) creativity
                                 CROSS JOIN form
                                 CROSS JOIN major
                                 CROSS JOIN (SELECT method.food,
                                                    method.narrative,
                                                    method.cue,
                                                    method.convention broken,
                                                    method.whenever confirm,
                                                    method.characterize
                                             FROM method
                                            ) listener
                                 CROSS JOIN (SELECT scope.peer counselor,
                                                    scope.stiff public,
                                                    scope.father,
                                                    scope.confrontation transmit,
                                                    scope.warrior,
                                                    scope.pink controversial
                                             FROM scope
                                            ) focus
                             ) later
                  CROSS JOIN (SELECT clean.housing
                              FROM (SELECT vulnerable.absence housing
                                    FROM vulnerable
                                   ) clean
                             ) forbid
                  CROSS JOIN (SELECT blow.limit fare,
                                     blow.resist ecological
                              FROM (SELECT driveway.century,
                                           driveway.tablespoon resist,
                                           driveway.negative nearby,
                                           driveway.divorce,
                                           driveway.limit
                                    FROM driveway
                                   ) blow
                             ) wound
                  CROSS JOIN (SELECT equally.conscience
                              FROM (SELECT icon.someday conscience
                                    FROM (SELECT pursue.home coach,
                                                 pursue.report endorse,
                                                 pursue.someday,
                                                 pursue.uncertain hill,
                                                 pursue.sympathy,
                                                 pursue.youngster,
                                                 pursue.condition,
                                                 pursue.correspondent nine
                                          FROM pursue
                                         ) icon
                                   ) equally
                             ) carve
              ) design
   CROSS JOIN assistant
   CROSS JOIN (SELECT simply.apartment,
                      simply.twelve,
                      simply.rhythm,
                      simply.magic,
                      simply.someday
               FROM simply
              ) processing ;


''',
  '''
SELECT lost.appeal move
FROM (SELECT fool.complex,
             fool.tourist,
             facilitate.bag,
             fool.taste introduction,
             facilitate.pressure appeal,
             reason.slightly,
             facilitate.legacy,
             facilitate.healthy aesthetic
      FROM          (SELECT alarm.interpret
                     FROM (SELECT there.interpret
                           FROM          (SELECT gray.sort
                                          FROM (SELECT basis.irony sort
                                                FROM (SELECT disappear.a irony
                                                      FROM (SELECT otherwise.a
                                                            FROM          (SELECT sole.circle
                                                                           FROM (SELECT normal.circle
                                                                                 FROM (SELECT satisfy.circle
                                                                                       FROM (SELECT instant.circle,
                                                                                                    instant.private channel
                                                                                             FROM (SELECT environmental.departure,
                                                                                                          environmental.circle,
                                                                                                          environmental.private,
                                                                                                          environmental.undertake feature
                                                                                                   FROM environmental
                                                                                                  ) instant
                                                                                            ) satisfy
                                                                                      ) normal
                                                                                ) sole
                                                                          ) warmth
                                                               CROSS JOIN (SELECT ongoing.narrow hang,
                                                                                  ongoing.that speed,
                                                                                  ongoing.kick,
                                                                                  ongoing.organizational statue,
                                                                                  ongoing.car,
                                                                                  ongoing.file a,
                                                                                  ongoing.softly,
                                                                                  ongoing.risk
                                                                           FROM ongoing
                                                                          ) otherwise
                                                           ) disappear
                                                     ) basis
                                               ) gray
                                         ) documentary
                              CROSS JOIN (SELECT such.accuracy interpret
                                          FROM (SELECT drown.idea accuracy
                                                FROM (SELECT destroy.idea
                                                      FROM (SELECT reflect.village member,
                                                                   reflect.insight,
                                                                   reflect.line,
                                                                   reflect.shape idea
                                                            FROM reflect
                                                           ) destroy
                                                     ) drown
                                               ) such
                                         ) there
                          ) alarm
                    ) Asian
         CROSS JOIN fool
         CROSS JOIN (SELECT allegedly.fur,
                            allegedly.trace
                     FROM (SELECT association.trace,
                                  association.political fur
                           FROM (SELECT usually.political,
                                        usually.stay trace
                                 FROM usually
                                ) association
                          ) allegedly
                    ) dynamic
         CROSS JOIN facilitate
         CROSS JOIN (SELECT crisis.steal slightly,
                            crisis.department import
                     FROM crisis
                    ) reason
     ) lost ;


''',
  '''
SELECT resign.mixed
FROM          several
   CROSS JOIN learn
   CROSS JOIN (SELECT celebrate.weekly theoretical
               FROM          celebrate
                  CROSS JOIN (SELECT magic.bullet satisfaction,
                                     magic.mood Chinese
                              FROM (SELECT chance.breath classic,
                                           chance.seat background,
                                           chance.bullet,
                                           chance.brave,
                                           donate.mood
                                    FROM          (SELECT strip.best,
                                                          strip.history sodium,
                                                          strip.mood
                                                   FROM strip
                                                  ) donate
                                       CROSS JOIN (SELECT southern.fifty tale,
                                                          southern.genius tropical,
                                                          southern.breath,
                                                          southern.reach,
                                                          southern.mind heaven,
                                                          southern.brave,
                                                          southern.determine bullet,
                                                          southern.fat seat
                                                   FROM southern
                                                  ) chance
                                   ) magic
                             ) catch
              ) among
   CROSS JOIN (SELECT response.soar point,
                      response.technique administrator,
                      response.equipment stock,
                      response.welfare fee,
                      response.bike,
                      response.mixed
               FROM response
              ) resign ;


''',
  '''
SELECT truck.multiple
FROM          (SELECT automatically.multiple
               FROM          complex
                  CROSS JOIN (SELECT focus.joy,
                                     focus.beam across,
                                     focus.multiple,
                                     focus.marketplace,
                                     focus.back,
                                     focus.neighbor,
                                     focus.rank support
                              FROM (SELECT beard.joy,
                                           opt.rank,
                                           opt.sack beam,
                                           beard.neighbor,
                                           opt.multiple,
                                           opt.friend back,
                                           opt.streak marketplace
                                    FROM          (SELECT annual.several joy,
                                                          annual.discount neighbor
                                                   FROM (SELECT ally.movement several,
                                                                ally.employer,
                                                                ally.cop discount,
                                                                ally.elevator currently,
                                                                ally.piano
                                                         FROM ally
                                                        ) annual
                                                  ) beard
                                       CROSS JOIN opt
                                   ) focus
                             ) automatically
              ) truck
   CROSS JOIN (SELECT expect.age feed
               FROM expect
              ) regulate
   CROSS JOIN two ;


''',
  '''
SELECT surrounding.aim channel
FROM          (SELECT survive.clear,
                      survive.meal
               FROM          deny
                  CROSS JOIN (SELECT poet.nature suspect,
                                     poet.differently highway,
                                     poet.visit exploit,
                                     poet.medium accomplishment,
                                     poet.president,
                                     poet.laser print
                              FROM          (SELECT statue.be need
                                             FROM (SELECT exactly.concern be
                                                   FROM (SELECT cheese.concern
                                                         FROM cheese
                                                        ) exactly
                                                  ) statue
                                            ) uncle
                                 CROSS JOIN poet
                             ) sample
                  CROSS JOIN survive
                  CROSS JOIN (SELECT orange.clue bone
                              FROM orange
                             ) base
                  CROSS JOIN (SELECT politically.brand,
                                     politically.media sophisticated,
                                     politically.throat,
                                     politically.impossible sixth,
                                     politically.cycle
                              FROM (SELECT affect.amendment northwest,
                                           affect.straighten break,
                                           affect.cycle,
                                           affect.our impossible,
                                           affect.brand,
                                           affect.media,
                                           affect.sign the,
                                           affect.throat
                                    FROM affect
                                   ) politically
                             ) sponsor
                  CROSS JOIN (SELECT specifically.towards
                              FROM (SELECT after.lightly,
                                           after.wrap towards
                                    FROM (SELECT attitude.wrap,
                                                 attitude.help lightly,
                                                 attitude.fit round
                                          FROM (SELECT breathe.place wrap,
                                                       breathe.lock purple,
                                                       breathe.secular fit,
                                                       breathe.help
                                                FROM breathe
                                               ) attitude
                                         ) after
                                   ) specifically
                             ) warm
              ) makeup
   CROSS JOIN surrounding
   CROSS JOIN (SELECT toxic.thread over,
                      toxic.warning
               FROM toxic
              ) pair
   CROSS JOIN cholesterol
   CROSS JOIN (SELECT shall.gain
               FROM (SELECT past.gain
                     FROM (SELECT secure.dance,
                                  secure.instant gain,
                                  secure.promise,
                                  secure.jewelry downtown
                           FROM (SELECT realm.dance,
                                        realm.promise,
                                        realm.disappointment,
                                        realm.angel jewelry,
                                        realm.instant,
                                        realm.purchase convince
                                 FROM realm
                                ) secure
                          ) past
                    ) shall
              ) return ;


''',
  '''
SELECT German.menu value
FROM          project
   CROSS JOIN (SELECT bee.substantial helpful,
                      bee.cue Internet,
                      bee.regime remove,
                      bee.gold,
                      bee.generate,
                      rule.chemical menu,
                      rule.reform,
                      bee.prescription
               FROM          (SELECT recipient.husband chemical,
                                     recipient.tender reform
                              FROM          (SELECT enemy.retail
                                             FROM          (SELECT vendor.maintain visit,
                                                                   vendor.shelter,
                                                                   vendor.scream,
                                                                   vendor.speed,
                                                                   vendor.fellow,
                                                                   vendor.Thanksgiving,
                                                                   vendor.heaven retail,
                                                                   vendor.small hint
                                                            FROM vendor
                                                           ) enemy
                                                CROSS JOIN (SELECT exam.cousin cry
                                                            FROM exam
                                                           ) audience
                                            ) including
                                 CROSS JOIN (SELECT finally.expand exhibit,
                                                    finally.take comply,
                                                    finally.donate,
                                                    finally.tender,
                                                    finally.husband
                                             FROM finally
                                            ) recipient
                             ) rule
                  CROSS JOIN bee
              ) German
   CROSS JOIN (SELECT prosecutor.calm,
                      prosecutor.son export,
                      prosecutor.flower fixed,
                      prosecutor.age,
                      prosecutor.precisely lip,
                      prosecutor.very decide
               FROM prosecutor
              ) hit
   CROSS JOIN (SELECT see.custom
               FROM (SELECT magic.custom
                     FROM (SELECT associated.custom
                           FROM associated
                          ) magic
                    ) see
              ) lover
   CROSS JOIN (SELECT happily.sigh expedition,
                      happily.clothing,
                      happily.support,
                      happily.canvas,
                      happily.eligible
               FROM happily
              ) capital
   CROSS JOIN (SELECT training.solar,
                      training.data butt,
                      training.musical this,
                      training.life private,
                      training.luck tune,
                      training.beauty,
                      training.employ
               FROM (SELECT pitch.data,
                            pitch.insert employ,
                            pitch.luck,
                            pitch.creative musical,
                            pitch.weekend solar,
                            pitch.beauty,
                            pitch.life
                     FROM pitch
                    ) training
              ) cling ;


''',
  '''
SELECT elderly.assistance
FROM          (SELECT beyond.error interpretation,
                      down.medical notebook,
                      down.cry,
                      down.sensitivity,
                      down.resort past
               FROM          (SELECT breast.entertainment error
                              FROM (SELECT division.entertainment,
                                           flip.authorize,
                                           flip.help,
                                           flip.glimpse
                                    FROM (SELECT piano.entertainment
                                          FROM (SELECT collector.entertainment
                                                FROM collector
                                               ) piano
                                         ) division
                                         CROSS JOIN perceived
                                        CROSS JOIN (SELECT piece.play authorize,
                                                           piece.flavor hot,
                                                           piece.glimpse,
                                                           piece.beast,
                                                           piece.help
                                                    FROM piece
                                                   ) flip
                                   ) breast
                             ) beyond
                  CROSS JOIN down
              ) dress

   CROSS JOIN more
   CROSS JOIN (SELECT doubt.bankruptcy,
                      doubt.assistance,
                      doubt.vegetable readily
               FROM doubt
              ) elderly
   CROSS JOIN explode


''',
  '''
SELECT bench.someone
FROM          (SELECT will.jump friend,
                      will.verdict significantly,
                      flower.celebration,
                      will.belong glance,
                      surprised.nail,
                      will.dance,
                      will.characterize,
                      flower.elbow someone
               FROM          (SELECT forth.top nail
                              FROM (SELECT idea.textbook top
                                    FROM (SELECT conflict.textbook
                                          FROM (SELECT committee.textbook
                                                FROM (SELECT marketplace.link textbook
                                                      FROM (SELECT articulate.silk link
                                                            FROM articulate
                                                           ) marketplace
                                                     ) committee
                                               ) conflict
                                         ) idea
                                   ) forth
                             ) surprised
                  CROSS JOIN flower
                  CROSS JOIN (SELECT protection.ought
                              FROM (SELECT shower.back cognitive,
                                           shower.graduation ought
                                    FROM (SELECT stick.they graduation,
                                                 stick.point,
                                                 stick.back
                                          FROM (SELECT apology.citizen point,
                                                       apology.model they,
                                                       apology.survival back
                                                FROM apology
                                               ) stick
                                         ) shower
                                   ) protection
                             ) quarter
                  CROSS JOIN will
                  CROSS JOIN spring
              ) bench
   CROSS JOIN (SELECT street.elder plan,
                      street.chemistry deliberately,
                      street.afraid poke
               FROM (SELECT embarrassed.retreat train,
                            embarrassed.parent afraid,
                            embarrassed.chemistry,
                            embarrassed.sort elder
                     FROM embarrassed
                    ) street
              ) race
   CROSS JOIN (SELECT element.use
               FROM (SELECT wish.northern use
                     FROM (SELECT credibility.northern
                           FROM (SELECT totally.finish boundary,
                                        totally.visitor northern,
                                        totally.Olympics
                                 FROM totally
                                ) credibility
                          ) wish
                    ) element
              ) family
   CROSS JOIN (SELECT kind.wood,
                      kind.costume
               FROM (SELECT differ.other,
                            differ.rest wood,
                            differ.picture,
                            differ.boot costume
                     FROM (SELECT Iraqi.back silence,
                                  Iraqi.accurately rest,
                                  Iraqi.picture,
                                  Iraqi.other,
                                  Iraqi.boot
                           FROM Iraqi
                          ) differ
                    ) kind
              ) flag ;


''',
  '''
SELECT recession.rid
FROM          (SELECT application.rid
               FROM (SELECT nearby.rid
                     FROM (SELECT shape.neighborhood mind,
                                  shape.rid
                           FROM (SELECT wipe.vulnerable patience,
                                        wipe.Senate,
                                        wipe.career ugly,
                                        wipe.researcher neighborhood,
                                        wipe.rid,
                                        wipe.block
                                 FROM (SELECT rally.Senate,
                                              rally.agreement deposit,
                                              rally.vulnerable,
                                              rally.territory researcher,
                                              rally.minor block,
                                              rally.rid,
                                              rally.career
                                       FROM rally
                                      ) wipe
                                ) shape
                          ) nearby
                    ) application
              ) recession
   CROSS JOIN break
   CROSS JOIN (SELECT promising.pound,
                      promising.humanity diagnosis,
                      promising.imagination sheet,
                      promising.uh reality
               FROM promising
              ) absorb
   CROSS JOIN (SELECT subtle.status visit,
                      subtle.lion
               FROM subtle
              ) object


''',
  '''
SELECT charge.church relate
FROM          charge
   CROSS JOIN (SELECT murder.overall analyst,
                      murder.welfare loan,
                      manipulate.safely transport
               FROM          murder
                  CROSS JOIN (SELECT wagon.aid,
                                     small.obtain,
                                     wagon.leaf sneak,
                                     small.naturally,
                                     wagon.eye,
                                     small.daily,
                                     small.operation,
                                     small.other
                              FROM          (SELECT complex.aid,
                                                    complex.eye,
                                                    complex.inmate leaf
                                             FROM (SELECT rim.aid,
                                                          rim.inmate,
                                                          rim.eye
                                                   FROM rim
                                                  ) complex
                                            ) wagon
                                 CROSS JOIN small
                             ) maintain
                  CROSS JOIN (SELECT dad.safely,
                                     dad.shorts
                              FROM (SELECT swing.shorts,
                                           swing.circuit monthly,
                                           swing.adolescent safely
                                    FROM swing
                                   ) dad
                             ) manipulate
                  CROSS JOIN (SELECT evaluation.supplier
                              FROM (SELECT angle.leap supplier
                                    FROM (SELECT prompt.assure leap
                                          FROM (SELECT close.assure
                                                FROM (SELECT discipline.baby assure
                                                      FROM (SELECT straw.rip baby
                                                            FROM (SELECT different.crystal rip,
                                                                         different.popular
                                                                  FROM (SELECT seemingly.meaning,
                                                                               seemingly.crystal,
                                                                               seemingly.already nest,
                                                                               seemingly.popular
                                                                        FROM (SELECT wing.rape already,
                                                                                     wing.honest popular,
                                                                                     wing.crystal,
                                                                                     wing.meaning
                                                                              FROM wing
                                                                             ) seemingly
                                                                       ) different
                                                                 ) straw
                                                           ) discipline
                                                     ) close
                                               ) prompt
                                         ) angle
                                   ) evaluation
                             ) allow
                  CROSS JOIN (SELECT guess.round
                              FROM (SELECT frankly.round
                                    FROM (SELECT expansion.round,
                                                 expansion.alley,
                                                 expansion.sensitive
                                          FROM expansion
                                         ) frankly
                                   ) guess
                             ) net
                  CROSS JOIN (SELECT portray.pro sleep,
                                     portray.himself
                              FROM (SELECT sport.past,
                                           sport.prospect himself,
                                           sport.via,
                                           sport.pro
                                    FROM sport
                                   ) portray
                             ) presence
              ) executive ;


''',
  '''
SELECT prescription.father
FROM         (SELECT discuss.belly,
                      discuss.trap,
                      dangerous.father
               FROM          discuss
                  CROSS JOIN advice
                  CROSS JOIN (SELECT envision.pine defeat,
                                     still.reminder,
                                     envision.father,
                                     envision.powerful
                              FROM          (SELECT wheat.front reminder
                                             FROM (SELECT direction.front,
                                                          direction.concentrate,
                                                          direction.amid
                                                   FROM (SELECT innocent.God duty,
                                                                innocent.rock concentrate,
                                                                innocent.partnership amid,
                                                                innocent.terrific front
                                                         FROM innocent
                                                        ) direction
                                                  ) wheat
                                            ) still
                                 CROSS JOIN (SELECT ego.father,
                                                    ego.pine,
                                                    ego.powerful
                                             FROM ego
                                            ) envision
                             ) dangerous
              ) prescription
   CROSS JOIN (SELECT but.cheese reflection,
                    but.prayer,
                    but.banking cave
               FROM but
              ) focus
   CROSS JOIN ecosystem ;


''',
  '''
SELECT below.notice
FROM          fresh
   CROSS JOIN (SELECT venture.haul,
                      wake.verbal less,
                      impact.mention oil,
                      spoon.plant likely,
                      disaster.trap,
                      disaster.predator
               FROM          disaster
                  CROSS JOIN spoon
                  CROSS JOIN (SELECT straw.trust,
                                     straw.expect task,
                                     straw.most,
                                     straw.bicycle latter,
                                     straw.manufacturing hide,
                                     straw.haul,
                                     straw.protest
                              FROM (SELECT extensive.airport bicycle,
                                           drunk.provision haul,
                                           manager.most,
                                           pump.after trust,
                                           drunk.manufacturing,
                                           justice.characteristic extend,
                                           extensive.expect,
                                           pump.photo protest
                                    FROM          (SELECT fur.comparable auto,
                                                          fur.over,
                                                          fur.most
                                                   FROM (SELECT worldwide.monkey most,
                                                                worldwide.comparable,
                                                                worldwide.tribal running,
                                                                worldwide.male stable,
                                                                worldwide.restrict over,
                                                                worldwide.view,
                                                                worldwide.slot jeans
                                                         FROM worldwide
                                                        ) fur
                                                  ) manager
                                       CROSS JOIN (SELECT both.dimension she,
                                                          both.tobacco instead,
                                                          both.history,
                                                          both.fact,
                                                          both.chart characteristic
                                                   FROM both
                                                  ) justice
                                       CROSS JOIN extensive
                                       CROSS JOIN drunk
                                       CROSS JOIN pump
                                   ) straw
                             ) venture
                  CROSS JOIN (SELECT society.monthly,
                                     society.clip,
                                     society.tone driving,
                                     society.elsewhere paper,
                                     society.day,
                                     society.rest mention
                              FROM society
                             ) impact
                  CROSS JOIN (SELECT stimulus.associated verbal,
                                     stimulus.routinely,
                                     stimulus.awful enable,
                                     stimulus.add cause,
                                     stimulus.conscience,
                                     stimulus.monkey
                              FROM stimulus
                             ) wake
              ) jet
   CROSS JOIN below ;


''',
  '''
SELECT consistently.would dynamic
FROM          (SELECT AM.would,
                      AM.thus fifteen
               FROM          bear
                  CROSS JOIN AM
              ) consistently
   CROSS JOIN (SELECT isolated.inherit,
                      isolated.balloon,
                      isolated.collect ministry,
                      conclusion.pencil potential
               FROM          (SELECT cattle.banking plan,
                                     rest.until,
                                     cattle.impact,
                                     cattle.pencil,
                                     reject.experience,
                                     cattle.lovely
                              FROM          (SELECT gifted.ironically budget,
                                                    gifted.grandfather,
                                                    gifted.until
                                             FROM (SELECT addition.until,
                                                          addition.moreover management,
                                                          addition.fish opportunity,
                                                          addition.grandfather,
                                                          addition.accident ironically
                                                   FROM addition
                                                  ) gifted
                                            ) rest
                                 CROSS JOIN cattle
                                 CROSS JOIN (SELECT fact.range,
                                                    fact.electrical cab,
                                                    fact.least electricity,
                                                    fact.limitation,
                                                    fact.publish curious,
                                                    fact.donor experience,
                                                    fact.tissue contain
                                             FROM fact
                                            ) reject
                                 CROSS JOIN (SELECT strategy.indigenous
                                             FROM (SELECT Latin.infant indigenous
                                                   FROM (SELECT punch.infant
                                                         FROM (SELECT apart.stack infant
                                                               FROM (SELECT port.most stack,
                                                                            port.reportedly instead
                                                                     FROM (SELECT ride.authority most,
                                                                                  ride.reportedly
                                                                           FROM (SELECT similarity.authority,
                                                                                        similarity.reportedly
                                                                                 FROM similarity
                                                                                ) ride
                                                                          ) port
                                                                    ) apart
                                                              ) punch
                                                        ) Latin
                                                  ) strategy
                                            ) inspiration
                             ) conclusion
                  CROSS JOIN (SELECT confidence.wish,
                                     confidence.spell no,
                                     confidence.loud fate,
                                     confidence.lie balloon,
                                     confidence.inherit,
                                     confidence.passing collect,
                                     confidence.grocery confusion,
                                     confidence.midnight
                              FROM confidence
                             ) isolated
              ) instructional


''',
  '''
SELECT pie.regulation cocaine
FROM          (SELECT symptom.regulation,
                      lion.bit,
                      symptom.us,
                      symptom.grief,
                      symptom.sing God,
                      symptom.magic develop,
                      lion.abortion
               FROM          symptom
                  CROSS JOIN (SELECT home.employ,
                                     home.warm,
                                     home.bit,
                                     home.researcher,
                                     home.bathroom history,
                                     home.break abortion
                              FROM (SELECT near.marry bit,
                                           near.seemingly bathroom,
                                           near.break,
                                           near.employ,
                                           near.shared researcher,
                                           near.surprise warm
                                    FROM near
                                   ) home
                             ) lion
              ) pie
   CROSS JOIN (SELECT eager.restore,
                      eager.refugee dinner
               FROM (SELECT adapt.refugee,
                            adapt.lower,
                            adapt.literally restore
                     FROM (SELECT species.suburb literally,
                                  species.grave lower,
                                  species.chicken with,
                                  species.refugee
                           FROM (SELECT sleep.detective grave,
                                        sleep.supposedly chicken,
                                        sleep.suburb,
                                        sleep.hide refugee
                                 FROM (SELECT peer.suburb,
                                              peer.detective,
                                              peer.rather supposedly,
                                              peer.hide,
                                              peer.voice string
                                       FROM (SELECT egg.coming voice,
                                                    egg.suburb,
                                                    egg.detective,
                                                    egg.hide,
                                                    egg.magic rather
                                             FROM (SELECT watch.below prior,
                                                          watch.appropriate hide,
                                                          watch.able magic,
                                                          watch.sail suburb,
                                                          watch.manufacturer,
                                                          watch.detective,
                                                          watch.exhibit charge,
                                                          watch.coming
                                                   FROM watch
                                                  ) egg
                                            ) peer
                                      ) sleep
                                ) species
                          ) adapt
                    ) eager
              ) retailer
   CROSS JOIN (SELECT capacity.drink,
                      capacity.comfortable fitness,
                      capacity.virus,
                      capacity.rare than
               FROM capacity
              ) lifestyle
   CROSS JOIN (SELECT even.attract everything,
                      even.analyze,
                      even.fantasy,
                      even.calm tour,
                      even.stance drunk
               FROM even
              ) well


''',
  '''
SELECT concrete.laugh
FROM          (SELECT body.little,
                      body.definitely area,
                      rent.transportation first,
                      rent.likelihood short,
                      rent.chocolate mutter,
                      rent.fixed,
                      body.bullet,
                      rent.circuit consistent
               FROM          (SELECT freeze.little,
                                     shower.bullet,
                                     shower.carrot definitely,
                                     shower.surgeon
                              FROM          (SELECT lonely.little
                                             FROM (SELECT design.few little
                                                   FROM (SELECT perfect.general,
                                                                perfect.exceed,
                                                                perfect.clay traffic,
                                                                perfect.evolution primary,
                                                                perfect.produce few
                                                         FROM (SELECT crowd.general,
                                                                      crowd.nutrient,
                                                                      crowd.vote clay,
                                                                      crowd.organized evolution,
                                                                      crowd.exceed,
                                                                      crowd.shower coat,
                                                                      crowd.sixth produce,
                                                                      crowd.feminist
                                                               FROM crowd
                                                              ) perfect
                                                        ) design
                                                  ) lonely
                                            ) freeze
                                 CROSS JOIN (SELECT why.nut surgeon,
                                                    why.gross twin,
                                                    why.bullet,
                                                    why.loyalty publisher,
                                                    why.coordinate,
                                                    why.silk carrot
                                             FROM why
                                            ) shower
                             ) body
                  CROSS JOIN rent
              ) anxiety
   CROSS JOIN (SELECT circumstance.incorporate,
                      circumstance.facility floor,
                      circumstance.armed estimate
               FROM (SELECT debt.armed,
                            debt.door facility,
                            debt.incorporate
                     FROM (SELECT night.armed,
                                  night.door,
                                  night.retired incorporate
                           FROM night
                          ) debt
                    ) circumstance
              ) feminist
   CROSS JOIN outsider
   CROSS JOIN (SELECT emission.adjustment,
                      emission.reach due,
                      emission.bowl,
                      emission.likelihood train,
                      emission.customer fishing,
                      emission.file helpful
               FROM (SELECT ear.adjustment,
                            ear.reach,
                            ear.bowl,
                            ear.architect likelihood,
                            ear.onion customer,
                            ear.file
                     FROM ear
                    ) emission
              ) popularity
   CROSS JOIN concrete


''',
  '''
SELECT test.detailed
FROM          acquire
   CROSS JOIN scheme
   CROSS JOIN (SELECT butterfly.constant region
               FROM (SELECT guideline.constant
                     FROM (SELECT championship.ten constant,
                                  championship.running
                           FROM (SELECT nothing.quest technician,
                                        nothing.ten,
                                        nothing.running
                                 FROM nothing
                                ) championship
                          ) guideline
                    ) butterfly
              ) individual
   CROSS JOIN (SELECT vitamin.detailed
               FROM vitamin
              ) test
   CROSS JOIN (SELECT donor.different,
                      donor.collapse ideal,
                      donor.host illness
               FROM (SELECT eighth.collapse,
                            eighth.stop emerge,
                            eighth.different,
                            eighth.choose host
                     FROM eighth
                    ) donor
              ) bread
   CROSS JOIN (SELECT notice.suppose,
                      notice.at collective
               FROM notice
              ) pollution
   CROSS JOIN mountain
   CROSS JOIN cook
   CROSS JOIN race ;


''',
  '''
SELECT worried.architecture
FROM (SELECT rely.address,
             bottom.architecture,
             save.filter forth,
             save.association,
             bottom.burst design,
             rely.hook jar
      FROM          rely
         CROSS JOIN (SELECT stop.tight shy,
                            exceed.check burst,
                            exceed.architecture,
                            stop.analyst bread,
                            stop.movement
                     FROM          stop
                        CROSS JOIN (SELECT phase.suggest check,
                                           burden.architecture,
                                           burden.defeat,
                                           phase.organize elderly
                                    FROM          phase
                                       CROSS JOIN (SELECT pie.foot hit,
                                                          pie.shore,
                                                          pie.standard dog,
                                                          pie.below,
                                                          pie.defeat,
                                                          defeat.architecture
                                                   FROM         pie
                                                   CROSS JOIN  (SELECT frontier.architecture
                                                                  FROM          frontier
                                                                     CROSS JOIN pale
                                                                 ) defeat
                                                  ) burden
                                   ) exceed
                    ) bottom
         CROSS JOIN save
     ) worried ;


''',
  '''
SELECT fragment.brick chart
FROM          neighboring
   CROSS JOIN (SELECT theater.pitcher
               FROM (SELECT painful.pitcher
                     FROM          (SELECT assistant.chairman,
                                           assistant.rush,
                                           assistant.lawyer division,
                                           assistant.ankle dig
                                    FROM          assistant
                                       CROSS JOIN (SELECT creativity.hungry training
                                                   FROM (SELECT trailer.segment hungry,
                                                                trailer.bank typically,
                                                                trailer.automatically,
                                                                trailer.farmer
                                                         FROM (SELECT remind.pay automatically,
                                                                      remind.jail blame,
                                                                      remind.segment,
                                                                      remind.bank,
                                                                      remind.farmer
                                                               FROM remind
                                                              ) trailer
                                                        ) creativity
                                                  ) opposed
                                   ) cousin
                        CROSS JOIN painful
                    ) theater
              ) perspective
   CROSS JOIN nowhere
   CROSS JOIN (SELECT actress.opponent excited,
                      actress.vessel,
                      actress.thinking pipe,
                      actress.queen,
                      actress.command,
                      actress.sexually,
                      actress.what
               FROM actress
              ) cut
   CROSS JOIN (SELECT how.quote injury
               FROM how
              ) charge
   CROSS JOIN (SELECT journey.brick,
                      journey.rape grin,
                      journey.Roman view,
                      journey.medium,
                      journey.fluid storage,
                      journey.modest
               FROM journey
              ) fragment
   CROSS JOIN afraid
   CROSS JOIN (SELECT money.sir,
                      money.economics constantly,
                      money.invasion,
                      money.thereby,
                      money.divide maintenance,
                      money.solely
               FROM (SELECT state.invasion,
                            state.divide,
                            state.economics,
                            state.soften solely,
                            state.thereby,
                            state.national sir
                     FROM state
                    ) money
              ) loss
   CROSS JOIN (SELECT heritage.sacred hallway,
                      heritage.orientation funding
               FROM (SELECT green.please alcohol,
                            green.look,
                            green.democratic,
                            green.sacred,
                            green.horrible,
                            green.casualty,
                            green.orientation,
                            green.assault reliability
                     FROM green
                    ) heritage
              ) virtual


''',
  '''
SELECT suggest.controversy
FROM          anywhere
   CROSS JOIN (SELECT lower.killing,
                      lower.controversy,
                      lower.carbon welfare,
                      happy.poverty firm,
                      reserve.boy echo,
                      happy.poverty sugar,
                      reserve.matter
               FROM          reserve
                  CROSS JOIN (SELECT river.lifetime poverty,
                                     river.regime rival,
                                     river.this used
                              FROM river
                             ) happy
                  CROSS JOIN (SELECT wedding.lamp killing,
                                     critic.pack carbon,
                                     there.bishop,
                                     basket.controversy
                              FROM          wedding
                                 CROSS JOIN (SELECT healthy.chief smile,
                                                    healthy.controversy
                                             FROM healthy
                                            ) basket
                                 CROSS JOIN critic
                                 CROSS JOIN (SELECT gradually.bishop
                                             FROM gradually
                                            ) there
                             ) lower
              ) suggest
   CROSS JOIN (SELECT emotion.heavily,
                      emotion.force,
                      emotion.much disaster
               FROM emotion
              ) tight ;


''',
  '''
SELECT little.trunk
FROM          little
   CROSS JOIN (SELECT predator.package,
                      exceed.breath,
                      stay.outside,
                      predator.borrow
               FROM          predator
                  CROSS JOIN (SELECT opponent.defend,
                                     dance.artist weigh,
                                     opponent.breath,
                                     manual.tradition ruin
                              FROM          dance
                                 CROSS JOIN (SELECT surprising.illness,
                                                    surprising.estate tradition,
                                                    lead.sea practical,
                                                    surprising.kit perception
                                             FROM          (SELECT terrorist.interesting,
                                                                   terrorist.illness,
                                                                   terrorist.kit,
                                                                   terrorist.estate
                                                            FROM terrorist
                                                           ) surprising
                                                CROSS JOIN (SELECT handle.refuse,
                                                                   handle.community sea
                                                            FROM (SELECT rating.coverage fluid,
                                                                         rating.deck diminish,
                                                                         rating.hand community,
                                                                         rating.run refuse,
                                                                         rating.only,
                                                                         rating.immigration blank
                                                                  FROM rating
                                                                 ) handle
                                                           ) lead
                                            ) manual
                                 CROSS JOIN opponent
                             ) exceed
                  CROSS JOIN (SELECT section.magnitude,
                                     section.review,
                                     section.diamond,
                                     section.jump state,
                                     section.relax
                              FROM (SELECT inspiration.image residence,
                                           inspiration.more review,
                                           inspiration.relax,
                                           inspiration.second sustain,
                                           inspiration.may jump,
                                           inspiration.diamond,
                                           inspiration.magnitude,
                                           inspiration.Japanese parade
                                    FROM inspiration
                                   ) section
                             ) even
                  CROSS JOIN (SELECT inmate.frontier preach,
                                     inmate.appropriate,
                                     inmate.dare,
                                     inmate.scope
                              FROM inmate
                             ) bit
                  CROSS JOIN (SELECT fear.angel fruit,
                                     fear.treatment,
                                     fear.governor,
                                     fear.leader giant,
                                     fear.coordinator bear,
                                     fear.outside
                              FROM fear
                             ) stay
              ) marine ;


''',
  '''
SELECT citizen.distant most
FROM          (SELECT feeling.meet,
                      city.story
               FROM          (SELECT Hispanic.rental reservation,
                                     house.wrap late,
                                     house.shame pizza,
                                     kiss.convict return,
                                     Hispanic.soil indicate,
                                     kiss.whoever object,
                                     Hispanic.job,
                                     Hispanic.story
                              FROM          kiss
                                 CROSS JOIN Hispanic
                                 CROSS JOIN (SELECT another.endorse thought,
                                                    another.auction shame,
                                                    another.factor wrap
                                             FROM (SELECT CEO.auction,
                                                          inmate.factor,
                                                          CEO.begin endorse
                                                   FROM          CEO
                                                      CROSS JOIN inmate
                                                  ) another
                                            ) house
                             ) city
                  CROSS JOIN chunk
                  CROSS JOIN (SELECT anyone.truck short,
                                     anyone.element,
                                     anyone.meet,
                                     anyone.method far,
                                     anyone.dirt desperately,
                                     anyone.primary,
                                     anyone.technique mentally
                              FROM anyone
                             ) feeling
                  CROSS JOIN (SELECT public.address,
                                     public.dynamic,
                                     public.figure challenge,
                                     public.herb,
                                     public.table tongue,
                                     public.jump costume,
                                     public.consistent,
                                     public.assistant
                              FROM public
                             ) naturally
              ) fewer
   CROSS JOIN (SELECT talent.furniture meter,
                      talent.able,
                      talent.hard,
                      talent.detect house,
                      talent.emotional shortly,
                      talent.wheelchair blue,
                      talent.resemble,
                      talent.alike
               FROM talent
              ) point
   CROSS JOIN lead
   CROSS JOIN government
   CROSS JOIN (SELECT Palestinian.raw position,
                      Palestinian.distant,
                      Palestinian.mine,
                      Palestinian.innovation,
                      Palestinian.click officer,
                      Palestinian.power,
                      Palestinian.remind schedule
               FROM Palestinian
              ) citizen ;


''',
  '''
SELECT couple.approval
FROM          (SELECT bag.access,
                      bag.survey thirty,
                      bag.patent generally,
                      bag.apart toll
               FROM bag
              ) cage
   CROSS JOIN (SELECT command.garage
               FROM command
              ) land
   CROSS JOIN (SELECT lead.example approval,
                      lead.recording,
                      lead.disappear command,
                      lead.escape homeland
               FROM (SELECT early.dust,
                            early.cloud spend,
                            early.important disappear,
                            early.medium,
                            early.gap escape,
                            early.example,
                            early.recording
                     FROM (SELECT smell.important,
                                  smell.recording,
                                  smell.reliability dust,
                                  smell.earnings cloud,
                                  smell.gap,
                                  smell.certainly rough,
                                  smell.workshop example,
                                  smell.cemetery medium
                           FROM smell
                          ) early
                    ) lead
              ) couple
   CROSS JOIN soften
   CROSS JOIN mayor ;


'''
];

