import '../projectStuff/projectStuff.dart';
import 'checkSame.dart';
import 'lelel.dart';
import 'sqlStrings.dart';

main() {
  initProject();

  List<int> L = [35, 70];
  List<int> D = [1, 3, 5];
  List<int> R = [0, 2, 4];

  checkSame();


  int i = 0;
  for(int l in L) {
    for(int d in D) {
      for(int r in R) {
        if (r >= d) continue;
        for(int t = 0; t < 5; t++ ) {
          var node = parseSQL(oldStatements[i]);

          project.runFunctionUnwrap("setNodeMaps", [node]);
          project.runFunctionUnwrap("setFromExtras", [node]);

          int length = project.runFunctionUnwrap("amountOfStuff", [node]);
          int resultDepth = project.runFunctionUnwrap("pathDepth", [node]);
          int renameCount = project.runFunctionUnwrap("pathRenameCount", [node]);

          bool well = project.runFunctionUnwrap("allColumnsEndWell", [node]);

          if(length != l || resultDepth != d || renameCount != r || !well) {
            print(i.toString() + " not edited");
            if (length != l) {
              print("Length = $length != $l");
            }
            if (resultDepth != d) {
              print("Result Depth = $resultDepth != $d");
            }
            if (renameCount != r) {
              print("Renames On Path = $renameCount != $r");
            }
            if(!well) {
              print("Fix References!");
            }
          }



          i++;
        }
      }
    }
  }
}

class SQLInfo {
  var node;
  int length;
  int resultDepth;
  int renameCount;

  bool equals(SQLInfo other) {
    if(other == null) return false;

    if(length == other.length
    && resultDepth == other.resultDepth
    && renameCount == other.renameCount)
      return true;
    else
      return false;
  }


  String toString() {
    return "Length: $length\n"+"Result Depth: $resultDepth\n"+"Rename Count: $renameCount";
  }
}

createSQLInfo(String str) {

  var node;
  try {
    node = parseSQL(str);
  } catch (e) {
    print(str);
    throw e;
  }

  project.runFunctionUnwrap("setNodeMaps", [node]);
  project.runFunctionUnwrap("setFromExtras", [node]);

  int length = project.runFunctionUnwrap("amountOfStuff", [node]);
  int resultDepth = project.runFunctionUnwrap("pathDepth", [node]);
  int renameCount = project.runFunctionUnwrap("pathRenameCount", [node]);

  return new SQLInfo()..node = node
                      ..length = length
                      ..resultDepth = resultDepth
                      ..renameCount = renameCount;

}