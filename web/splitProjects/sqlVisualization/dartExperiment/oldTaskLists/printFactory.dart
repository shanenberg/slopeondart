import '../projectStuff/projectStuff.dart';
import '../tasks/Task.dart';
import '../tasks/break/Break.dart';
import '../tasks/originColumn/OriginColumnTask.dart';
import 'checkSame.dart';
import 'dart:math';
import 'finalCheck.dart';
import 'package:english_words/english_words.dart';
import '../../../../../lib/slot/runtime/builtInFunction/BuiltInFunction.dart';
import 'sqlStrings.dart';
import 'statementShuffler.dart';

main() {

  initProject();

}

List<Task> manualShuffledStatements() {
  oldStatements.shuffle();

  List<SQLInfo> strs1 = statementsWithNewIdentifiers(oldStatements);
  List<SQLInfo> strs2 = statementsWithNewIdentifiers(oldStatements);

  List<Task> taskList = [];

  for(SQLInfo info in strs1) {
    taskList.add(new OriginColumnTask( info.node, DisplayMethod.VISUAL));
  }
  taskList.add(new Break());
  for(SQLInfo info in strs2) {
    taskList.add(new OriginColumnTask(info.node, DisplayMethod.TEXTUAL));
  }

  return taskList;
}

List<SQLInfo> statementsWithNewIdentifiers(List<String> strs) {
  List<String> newStrs = new List.from(strs);
  List<SQLInfo> stuff = [];
  for(int i = 0; i<newStrs.length; i++) {
    String str = newStrs[i];

    SQLInfo beforeInfo = createSQLInfo(str);


    str = replaceAllIdentifiers(str);

    SQLInfo afterInfo = createSQLInfo(str);

    if(!beforeInfo.equals(afterInfo)) {
      print(beforeInfo.toString());
      print(afterInfo.toString());
      throw("WTF");
    }

    stuff.add(afterInfo);



    newStrs[i] = str;


  }
  return stuff;
}

