import '../projectStuff/projectStuff.dart';
import 'shuffledStrings.dart';
import 'statementShuffler.dart';

main() {
  initProject();
  print("Project Initialized");
  List<String> newStatements = stringsWithNewIdentifiersUnchecked(shuffledStatementsVisual);

  printArr("shuffledStatementsTextual", newStatements);

}