

List<String> oldStatements=
[
/*
#0
           Length: 36
     Result Depth: 0
  Renames On Path: 1

->         Length: 35
->   Result Depth: 1
->Renames On Path: 0
DONE
*/
'''
SELECT brush.diminish
FROM          notebook
   CROSS JOIN nuclear
   CROSS JOIN (SELECT background.surveillance tourism
               FROM (SELECT popular.surveillance
                     FROM (SELECT amazing.result surveillance,
                                  amazing.electricity
                           FROM (SELECT appeal.content laughter,
                                        appeal.result,
                                        appeal.electricity
                                 FROM appeal
                                ) amazing
                          ) popular
                    ) background
              ) jazz
   CROSS JOIN (SELECT why.diminish
               FROM why
              ) brush
   CROSS JOIN (SELECT eyebrow.trend,
                      eyebrow.thinking tobacco,
                      eyebrow.screen invisible
               FROM (SELECT undermine.thinking,
                            undermine.pop firm,
                            undermine.trend,
                            undermine.silly screen
                     FROM undermine
                    ) eyebrow
              ) wage
   CROSS JOIN (SELECT beard.arrest,
                      beard.freedom love
               FROM beard
              ) swell
   CROSS JOIN lesson
   CROSS JOIN outlet
   CROSS JOIN perception ;
''',
/*
#1
           Length: 38
     Result Depth: 1
  Renames On Path: 1

->         Length: 35
->   Result Depth: 1
->Renames On Path: 0
DONE
*/
'''
SELECT satisfaction.become
FROM          finding
   CROSS JOIN pocket
   CROSS JOIN (SELECT outline.nod remote
               FROM          (SELECT sentiment.suburb
                              FROM          (SELECT regulator.process notion,
                                                    regulator.addition keep,
                                                    regulator.figure complexity
                                             FROM (SELECT file.attraction figure,
                                                          file.process,
                                                          file.addition
                                                   FROM file
                                                  ) regulator
                                            ) protest
                                 CROSS JOIN (SELECT disaster.explode defendant,
                                                    disaster.suburb
                                             FROM (SELECT work.utilize,
                                                          work.explode,
                                                          work.suburb,
                                                          work.mean,
                                                          work.pit
                                                   FROM work
                                                  ) disaster
                                            ) sentiment
                             ) hardware
                  CROSS JOIN outline
              ) pipe
   CROSS JOIN start
   CROSS JOIN (SELECT practical.become
               FROM practical
              ) satisfaction
   CROSS JOIN spread
   CROSS JOIN classify
   CROSS JOIN benefit
   CROSS JOIN carry
''',

/*
#2
           Length: 36
     Result Depth: 0
  Renames On Path: 0

->         Length: 35
->   Result Depth: 1
->Renames On Path: 0
*/
  '''
SELECT every.personally
FROM          (SELECT excuse.gradually,
                      excuse.principal
               FROM excuse
              ) clear
   CROSS JOIN (SELECT prove.consider,
                      prove.be thin,
                      prove.easy attraction,
                      prove.estate,
                      prove.cartoon
               FROM prove
              ) strike
   CROSS JOIN examine
   CROSS JOIN man
   CROSS JOIN newly
   CROSS JOIN (SELECT basis.personally
               FROM basis
              ) every
   CROSS JOIN (SELECT suspend.theological,
                      suspend.deliberately,
                      suspend.wing,
                      suspend.burden his
               FROM suspend
              ) altogether
   CROSS JOIN (SELECT profit.deeply technique,
                      profit.riot calendar,
                      profit.firmly
               FROM (SELECT emotion.ranch deeply,
                            emotion.knock weaken,
                            emotion.provoke firmly,
                            emotion.senior riot,
                            emotion.greatly
                     FROM emotion
                    ) profit
              ) classify
''',
/*
#3
           Length: 36
     Result Depth: 1
  Renames On Path: 0

->         Length: 35
->   Result Depth: 1
->Renames On Path: 0
DONE
*/
  '''
SELECT liquid.columnist
FROM          (SELECT flying.British,
                      rural.boyfriend recipient
               FROM          rural
                  CROSS JOIN (SELECT nest.web advertising,
                                     nest.face timber,
                                     nest.matter,
                                     nest.resign several,
                                     nest.regardless,
                                     nest.legal
                              FROM nest
                             ) grocery
                  CROSS JOIN (SELECT chase.discover,
                                     chase.corruption,
                                     chase.fault British,
                                     chase.think,
                                     chase.sweet
                              FROM (SELECT affect.religion corruption,
                                           affect.handle think,
                                           affect.contribution fault,
                                           affect.orange discover,
                                           affect.center sweet
                                    FROM affect
                                   ) chase
                             ) flying
              ) signature
   CROSS JOIN (SELECT note.photograph onto,
                      note.columnist,
                      note.exchange obtain,
                      note.barrier sport,
                      note.owe radio,
                      note.clip worldwide,
                      note.museum
               FROM note
              ) liquid ;
''',
/*
#4
           Length: 32
     Result Depth: 1
  Renames On Path: 2

->         Length: 35
->   Result Depth: 1
->Renames On Path: 0
DONE
*/
  '''
SELECT bag.psychology
FROM          (SELECT mortgage.expected realm,
                      dark.metaphor away,
                      dark.shortage,
                      dark.beyond,
                      dark.laugh vertical
               FROM          (SELECT please.standard expected
                              FROM (SELECT cry.standard,
                                           terror.shape,
                                           terror.originally,
                                           terror.state
                                    FROM (SELECT helmet.standard
                                          FROM (SELECT answer.standard
                                                FROM answer
                                               ) helmet
                                         ) cry
                                         CROSS JOIN explosion
                                        CROSS JOIN (SELECT underlying.verdict shape,
                                                           underlying.design cultural,
                                                           underlying.state,
                                                           underlying.dad,
                                                           underlying.originally
                                                    FROM underlying
                                                   ) terror
                                   ) please
                             ) mortgage
                  CROSS JOIN dark
              ) heavy

   CROSS JOIN accept
   CROSS JOIN (SELECT gold.feel,
                      gold.psychology,
                      gold.hair chain
               FROM gold
              ) bag
   CROSS JOIN heritage
''',
/*
#5
           Length: 38
     Result Depth: 1
  Renames On Path: 1

->         Length: 35
->   Result Depth: 3
->Renames On Path: 0
DONE
*/
  '''
SELECT manufacturer.generate
FROM (SELECT hearing.govern young,
             service.generate,
             service.arrow,
             service.series,
             service.meantime,
             quote.well,
             hearing.dumb
      FROM          hearing
         CROSS JOIN (SELECT sheep.well
                     FROM (SELECT the.corridor well
                           FROM (SELECT set.oil corridor,
                                        set.producer
                                 FROM (SELECT focus.oil,
                                              focus.producer
                                       FROM focus
                                      ) set
                                ) the
                          ) sheep
                    ) quote

         CROSS JOIN (SELECT tuck.generate,
                             finance.perhaps arrow,
                             bow.series,
                             bow.meantime,
                              bow.breathing

                     FROM (SELECT apartment.generate
                           FROM apartment
                          ) tuck
                          CROSS JOIN gentle
                           CROSS JOIN finance
                           CROSS JOIN run
                           CROSS JOIN bow
                    ) service
         CROSS JOIN no
     ) manufacturer ;
''',
/*
#6
           Length: 35
     Result Depth: 0
  Renames On Path: 0

->         Length: 35
->   Result Depth: 3
->Renames On Path: 0
DONE
*/
  '''
SELECT boss.horrible
FROM          (SELECT text.horrible
               FROM          destroy
                  CROSS JOIN (SELECT fold.building,
                                     fold.behind high,
                                     fold.horrible,
                                     fold.pool,
                                     fold.Latin,
                                     fold.dissolve,
                                     fold.compromise killer
                              FROM (SELECT frequently.building,
                                           overlook.compromise,
                                           overlook.return behind,
                                           frequently.dissolve,
                                           overlook.horrible,
                                           overlook.roughly Latin,
                                           overlook.repeatedly pool
                                    FROM          (SELECT sacred.midnight building,
                                                          sacred.today dissolve
                                                   FROM (SELECT wage.collector midnight,
                                                                wage.openly,
                                                                wage.consider today,
                                                                wage.spectacular depart,
                                                                wage.exception
                                                         FROM wage
                                                        ) sacred
                                                  ) frequently
                                       CROSS JOIN overlook
                                   ) fold
                             ) text
              ) boss
   CROSS JOIN (SELECT addition.interpret await
               FROM addition
              ) plead
   CROSS JOIN mean ;
''',
/*
#7
           Length: 40
     Result Depth: 0
  Renames On Path: 1

->         Length: 35
->   Result Depth: 3
->Renames On Path: 0
DONE
*/
  '''
SELECT texture.wealth
FROM         (SELECT annually.network,
                      annually.but,
                      clay.wealth
               FROM          annually
                  CROSS JOIN medium
                  CROSS JOIN (SELECT lane.cemetery guarantee,
                                     railroad.elsewhere,
                                     lane.wealth,
                                     lane.stuff
                              FROM          (SELECT we.court elsewhere
                                             FROM (SELECT move.court,
                                                          move.potential,
                                                          move.differently
                                                   FROM (SELECT usually.insight rod,
                                                                usually.sign potential,
                                                                usually.input differently,
                                                                usually.past court
                                                         FROM usually
                                                        ) move
                                                  ) we
                                            ) railroad
                                 CROSS JOIN (SELECT European.wealth,
                                                    European.cemetery,
                                                    European.stuff
                                             FROM European
                                            ) lane
                             ) clay
              ) texture
   CROSS JOIN (SELECT punishment.show criminal,
                    punishment.appearance,
                    punishment.up arm
               FROM punishment
              ) mixture
   CROSS JOIN since ;
''',
/*
#8
           Length: 32
     Result Depth: 1
  Renames On Path: 2

->         Length: 35
->   Result Depth: 3
->Renames On Path: 0
DONE
*/
  '''
SELECT diary.asset
FROM          qualify
   CROSS JOIN (SELECT native.why,
                      native.asset,
                      native.target if,
                      political.profession reform,
                      ruling.though response,
                      political.profession horizon,
                      ruling.wheelchair
               FROM          ruling
                  CROSS JOIN (SELECT bite.dock profession,
                                     bite.harm racial,
                                     bite.handle mom
                              FROM bite
                             ) political
                  CROSS JOIN (SELECT yellow.valuable why,
                                     grandfather.stable target,
                                     surrounding.electronic,
                                     whale.asset
                              FROM          yellow
                                 CROSS JOIN (SELECT angry.Israeli hurricane,
                                                    angry.asset
                                             FROM angry
                                            ) whale
                                 CROSS JOIN grandfather
                                 CROSS JOIN (SELECT search.electronic
                                             FROM search
                                            ) surrounding
                             ) native
              ) diary
   CROSS JOIN (SELECT classify.immigrant,
                      classify.vital,
                      classify.poll priest
               FROM classify
              ) thread ;
''',
/*
#9
           Length: 30
     Result Depth: 6
  Renames On Path: 3

->         Length: 35
->   Result Depth: 3
->Renames On Path: 0
DONE
*/
  '''
SELECT regard.circumstance
FROM          (SELECT close.bug call,
                      close.dose guide,
                      close.seminar viewer,
                      close.out,
                      close.available,
                      close.northern automatic,
                      close.map ship,
                      close.throughout
               FROM close
              ) harmony
   CROSS JOIN (SELECT tribe.circumstance
               FROM (SELECT gap.circumstance
                     FROM (SELECT ever.recently,
                                  forever.circumstance
                           FROM             (SELECT simply.recently,
                                                    simply.if
                                             FROM (SELECT fact.under,
                                                          fact.recently,
                                                          fact.if,
                                                          fact.naval,
                                                          fact.developmental
                                                   FROM (SELECT undergraduate.angel naval,
                                                                undergraduate.wrist if,
                                                                undergraduate.recently,
                                                                undergraduate.ship developmental,
                                                                undergraduate.under
                                                         FROM undergraduate
                                                        ) fact
                                                  ) simply
                                            ) ever
                                CROSS JOIN  forever
                          ) gap
                    ) tribe
              ) regard
''',
/*
#10
           Length: 40
     Result Depth: 0
  Renames On Path: 1

->         Length: 35
->   Result Depth: 3
->Renames On Path: 2
DONE
*/
  '''
SELECT TV.bounce
FROM          (SELECT need.core,
                      die.bounce,
                      wealth.abc
               FROM          need
                  CROSS JOIN tag
                  CROSS JOIN (SELECT noon.conservative,
                                     noon.pattern AS bounce
                              FROM (SELECT marketing.costly,
                                          marketing.plus conservative,
                                          dangit.bounce AS pattern,
                                          marketing.questionnaire
                                   FROM            (SELECT expect.stuff income,
                                                            expect.deer costly,
                                                            expect.joke plus,
                                                            expect.questionnaire
                                                     FROM (SELECT film.empty teaspoon,
                                                                  film.offensive joke,
                                                                  film.stuff,
                                                                  film.resistance deer,
                                                                  film.questionnaire
                                                           FROM film
                                                          ) expect
                                                   ) marketing
                                       CROSS JOIN dangit
                                  ) noon
                             ) die
                  CROSS JOIN (SELECT tablespoon.abc,
                                     tablespoon.cde,
                                     tablespoon.efg
                              FROM            tablespoon
                                   CROSS JOIN nothin
                             ) wealth
              ) TV
   CROSS JOIN kill
''',
/*
#11
           Length: 38
     Result Depth: 2
  Renames On Path: 1

->         Length: 35
->   Result Depth: 3
->Renames On Path: 2
DONE
*/
  '''
SELECT must.abc
FROM          (SELECT cheer.stuff bright
               FROM (SELECT smell.demonstration stuff
                     FROM          smell
                        CROSS JOIN opposition
                    ) cheer
              ) possibly
   CROSS JOIN (SELECT million.hunt,
                      million.flow equity,
                      annually.legitimate glass,
                      million.intellectual
               FROM          annually
                  CROSS JOIN (SELECT politically.grass flow,
                                     politically.allegation intellectual,
                                     politically.outside hunt,
                                     politically.branch,
                                     politically.moon,
                                     politically.team documentary,
                                     politically.artist
                              FROM politically
                             ) million
              ) sun
   CROSS JOIN remote
   CROSS JOIN (SELECT absolutely.pair AS abc,
                      ill.drive
               FROM          (SELECT monkey.sharply pair
                              FROM (SELECT pause.sharply
                                    FROM pause
                                   ) monkey
                             ) absolutely
                  CROSS JOIN (SELECT arrive.drive
                              FROM arrive
                             ) ill
              ) must
   CROSS JOIN sell
''',
/*
#12
           Length: 36
     Result Depth: 0
  Renames On Path: 1

->         Length: 35
->   Result Depth: 3
->Renames On Path: 2
DONE
*/
  '''
SELECT hers.opinion
FROM          (SELECT contend.suspect,
                      contend.exceed corn,
                      contend.apology period,
                      contend.OK whose
               FROM contend
              ) servant
   CROSS JOIN (SELECT sock.tend
               FROM sock
              ) mayor
   CROSS JOIN (SELECT essential.soil opinion,
                      essential.happily,
                      essential.motive sock,
                      essential.thus alone
               FROM (SELECT summit.road,
                            summit.judge century,
                            summit.lot motive,
                            summit.illegal,
                            summit.banking thus,
                            summit.soil,
                            summit.happily
                     FROM (SELECT smart.lot,
                                  smart.happily,
                                  smart.employment road,
                                  smart.store judge,
                                  smart.banking,
                                  smart.name gang,
                                  smart.river soil,
                                  smart.producer illegal
                           FROM smart
                          ) summit
                    ) essential
              ) hers
   CROSS JOIN view
   CROSS JOIN stare ;
''',







/*
#13
           Length: 40
     Result Depth: 0
  Renames On Path: 1

->         Length: 35
->   Result Depth: 3
->Renames On Path: 2
DONE
*/
  '''
SELECT more.needle
FROM          tuck
   CROSS JOIN (SELECT stuff.needle,
                      stuff.wheel trial
               FROM (SELECT common.fool search,
                            common.discovery needle,
                            model.kit wheel
                     FROM          (SELECT activity.dinner classroom,
                                           activity.joint shadow,
                                           activity.beat fool,
                                           activity.supplier inquiry,
                                           activity.university discovery
                                    FROM activity
                                   ) common
                        CROSS JOIN (SELECT boast.kit
                                    FROM (SELECT language.kit,
                                                 language.hat negotiate,
                                                 language.blink
                                          FROM (SELECT host.trash,
                                                       host.blink,
                                                       host.voluntary,
                                                       host.kit,
                                                       host.war hat
                                                FROM (SELECT path.draft kit,
                                                             path.territory war,
                                                             path.confession blink,
                                                             path.voluntary,
                                                             path.suicide trash
                                                      FROM path
                                                     ) host
                                               ) language
                                         ) boast
                                   ) model
                    ) stuff
              ) more
''',




/*
#14
           Length: 35
     Result Depth: 0
  Renames On Path: 0

->         Length: 35
->   Result Depth: 3
->Renames On Path: 2
DONE
*/
  '''
SELECT defense.cde
FROM          (SELECT murder.somewhat,
                      murder.abc cde
               FROM (SELECT system.somewhat,
                            system.jojo abc
                     FROM (SELECT bull.somewhat,
                                  ring.jojo
                           FROM             ring
                                CROSS JOIN (SELECT fire.somewhat
                                 FROM (SELECT premise.province somewhat,
                                              premise.sugar,
                                              premise.liquid beach
                                       FROM (SELECT terrorist.interpretation,
                                                    terrorist.satisfy hopefully,
                                                    terrorist.sugar,
                                                    terrorist.agriculture liquid,
                                                    terrorist.province
                                             FROM terrorist
                                            ) premise
                                      ) fire
                                ) bull
                          ) system
                    ) murder
              ) defense
   CROSS JOIN (SELECT effort.bulk principal,
                      effort.biology contemplate,
                      effort.continue cage
               FROM (SELECT broker.bulk,
                            broker.eventually reduction,
                            broker.biology,
                            broker.compete runner,
                            broker.continue
                     FROM broker
                    ) effort
              ) dignity
''',
/*
#15
           Length: 40
     Result Depth: 0
  Renames On Path: 0

->         Length: 35
->   Result Depth: 5
->Renames On Path: 0
DONE
*/
  '''
SELECT home.generate
FROM          (SELECT professor.spread,
                      spring.architecture invade,
                      professor.sixth
               FROM          (SELECT recruit.monitor
                              FROM recruit
                             ) print
                  CROSS JOIN spring
                  CROSS JOIN professor
              ) compare
   CROSS JOIN (SELECT bear.fifth,
                      bear.nobody
               FROM bear
              ) ought

   CROSS JOIN (SELECT exhaust.care direct,
                      exhaust.generate
               FROM (SELECT escape.tomorrow care,
                            escape.sector,
                            escape.generate
                     FROM (SELECT crack.legitimate,
                                  crack.tomorrow,
                                  question.generate,
                                  question.sector
                           FROM             crack
                                CROSS JOIN (SELECT mean.generate,
                                                  mean.sector
                                           FROM (SELECT instrument.generate,
                                                        instrument.democracy sector
                                                 FROM instrument
                                                ) mean
                                          ) question
                          ) escape
                    ) exhaust
              ) home
   CROSS JOIN multiple ;
''',
/*
#16
           Length: 35
     Result Depth: 1
  Renames On Path: 2

->         Length: 35
->   Result Depth: 5
->Renames On Path: 0
DONE
*/
  '''
SELECT complain.essentially
FROM (SELECT effectively.attitude,
             shake.essentially,
             log.attract officially,
             log.telescope,
             shake.feminist encouraging,
             effectively.club tiny
      FROM          effectively
         CROSS JOIN (SELECT aggression.efficient dramatic,
                            sufficient.chamber feminist,
                            sufficient.essentially,
                            aggression.feedback mean,
                            aggression.distance
                     FROM          aggression
                        CROSS JOIN (SELECT probably.stand chamber,
                                           within.essentially,
                                           within.distribution,
                                           probably.scared belt
                                    FROM          probably
                                       CROSS JOIN (SELECT museum.sport invest,
                                                          museum.script,
                                                          museum.cattle Muslim,
                                                          museum.portfolio,
                                                          museum.distribution,
                                                          distribution.essentially
                                                   FROM         museum
                                                   CROSS JOIN  (SELECT event.essentially
                                                                  FROM          event
                                                                     CROSS JOIN cease
                                                                 ) distribution
                                                  ) within
                                   ) sufficient
                    ) shake
         CROSS JOIN log
     ) complain ;
''',
/*
#17
           Length: 39
     Result Depth: 1
  Renames On Path: 2

->         Length: 35
->   Result Depth: 5
->Renames On Path: 0
DONE
*/
'''
SELECT mention.addition
FROM (SELECT workshop.genius,
             workshop.politically engineering,
             soar.chip coast,
             even.addition
      FROM          workshop
         CROSS JOIN (SELECT unfold.rescue middle,
                            unfold.addition
                     FROM (SELECT sense.suspend rescue,
                                  sense.addition
                           FROM          (SELECT summit.candle suspend,
                                                 bear.addition,
                                                 summit.breakfast religious
                                          FROM          (SELECT kingdom.addition,
                                                                kingdom.connection
                                                         FROM kingdom
                                                        ) bear
                                             CROSS JOIN summit
                                         ) sense
                              CROSS JOIN (SELECT tray.educator
                                          FROM (SELECT random.globe educator,
                                                       random.exceed
                                                FROM random
                                               ) tray
                                         ) emerge
                              CROSS JOIN (SELECT register.chew worth,
                                                 register.material,
                                                 register.insist,
                                                 register.ugly boy
                                          FROM register
                                         ) supporter
                          ) unfold
                    ) even
         CROSS JOIN soar
     ) mention
''',

/*
#18
           Length: 36
     Result Depth: 1
  Renames On Path: 0

->         Length: 35
->   Result Depth: 5
->Renames On Path: 0
DONE
*/
  '''
SELECT eat.journey
FROM          (SELECT demonstration.journey
               FROM (SELECT counselor.journey
                     FROM (SELECT result.demographic meat,
                                  result.journey
                           FROM (SELECT grape.equipment obligation,
                                        grape.weight,
                                        grape.play consent,
                                        grape.officer demographic,
                                        grape.journey,
                                        grape.exist
                                 FROM (SELECT welcome.weight,
                                              welcome.exploration discipline,
                                              welcome.equipment,
                                              welcome.against officer,
                                              welcome.another exist,
                                              welcome.journey,
                                              welcome.play
                                       FROM welcome
                                      ) grape
                                ) result
                          ) counselor
                    ) demonstration
              ) eat
   CROSS JOIN money
   CROSS JOIN (SELECT frequently.involvement,
                      frequently.palace marketing,
                      frequently.option chocolate,
                      frequently.sibling client
               FROM frequently
              ) capital
   CROSS JOIN (SELECT curiosity.similar dough,
                      curiosity.virtually
               FROM curiosity
              ) mainstream
''',
/*
#19
           Length: 38
     Result Depth: 1
  Renames On Path: 0

->         Length: 35
->   Result Depth: 5
->Renames On Path: 0
DONE
*/
  '''
SELECT onto.defeat
FROM          (SELECT liberty.still sensation,
                      decline.slide,
                      destroy.negotiate cat,
                      classical.defeat,
                      faint.recover,
                      liberty.branch
               FROM          destroy
                  CROSS JOIN liberty
                  CROSS JOIN (SELECT addition.defeat,
                                     addition.struggle
                              FROM (SELECT era.outstanding struggle,
                                           era.defeat,
                                           era.media
                                    FROM (SELECT offense.outstanding,
                                                 offense.defeat,
                                                 offense.because media
                                          FROM (SELECT fifteen.outstanding,
                                                        fifteen.defeat,
                                                        fifteen.because
                                                 FROM fifteen
                                                ) offense
                                         ) era
                                   ) addition
                             ) classical
                  CROSS JOIN decline
                  CROSS JOIN (SELECT proposal.bomb courtroom,
                                        proposal.gender recover
                                 FROM (SELECT shadow.superior gender,
                                              shadow.fog bomb,
                                              shadow.green logic
                                       FROM shadow
                                      ) proposal
                            ) faint 
              ) onto

''',

/*
#20
           Length: 36
     Result Depth: 0
  Renames On Path: 0

->         Length: 35
->   Result Depth: 5
->Renames On Path: 2
DONE
*/
  '''
SELECT pit.dining
FROM          (SELECT counter.conservative,
                      clinic.seal dining,
                      counter.close,
                      counter.laser obstacle
               FROM          (SELECT gentle.seal
                              FROM          hence
                                 CROSS JOIN (SELECT trailer.reservation,
                                                   trailer.hunter announcement,
                                                   trailer.mandate contract,
                                                   trailer.isolation,
                                                   trailer.present
                                            FROM trailer
                                           ) matter

                                 CROSS JOIN (SELECT emphasize.regret seal ,
                                                    emphasize.wilderness,
                                                    devote.remember,
                                                    devote.salary,
                                                    devote.principle
                                             FROM          (SELECT here.regret,
                                                                   here.deploy wilderness
                                                            FROM (SELECT small.deploy,
                                                                         small.regret
                                                                  FROM small
                                                                 ) here
                                                           ) emphasize
                                                CROSS JOIN devote
                                            ) gentle
                                 CROSS JOIN (SELECT loose.chill abuse
                                             FROM loose
                                            ) loyalty
                             ) clinic
                  CROSS JOIN counter
              ) pit
   CROSS JOIN alley
''',
/*
#21
           Length: 36
     Result Depth: 0
  Renames On Path: 0

->         Length: 35
->   Result Depth: 5
->Renames On Path: 2
DONE
*/
  '''
SELECT inevitable.initiative
FROM          (SELECT ad.initiative,
                      interpret.everyday clearly,
                      ad.abstract trainer
               FROM          interpret
                  CROSS JOIN enemy
                  CROSS JOIN (SELECT deposit.negotiation smooth,
                                     deposit.governor initiative,
                                     deposit.abstract
                              FROM (SELECT insight.legally negotiation,
                                           insight.abstract,
                                           light.governor,
                                           insight.resident lower,
                                           insight.bathroom
                                    FROM        insight
                                    CROSS JOIN  (SELECT rarely.quarterback governor,
                                                        rarely.fault,
                                                        rarely.survey
                                                 FROM (SELECT well.fault,
                                                              well.subsidy ,
                                                              well.number,
                                                              well.survey,
                                                              well.quarterback
                                                       FROM well
                                                      ) rarely
                                                ) light
                                   ) deposit
                             ) ad
              ) inevitable
   CROSS JOIN unite
   CROSS JOIN (SELECT code.stuff
               FROM code
              ) security
   CROSS JOIN particular
   CROSS JOIN agricultural
''',
/*
#22
           Length: 32
     Result Depth: 1
  Renames On Path: 1

->         Length: 35
->   Result Depth: 5
->Renames On Path: 2
DONE
*/
  '''
SELECT restaurant.English
FROM          terrain

   CROSS JOIN (SELECT result.protest indigenous,
                      result.English
               FROM (SELECT resume.protest,
                            top.English,
                            top.draft
                     FROM               (SELECT poem.Soviet protest,
                                                poem.income
                                         FROM poem
                                        ) resume
                             CROSS JOIN online
                             CROSS JOIN shed
                             CROSS JOIN (SELECT stress.label legislature,
                                                globe.clinic,
                                                stress.grab ,
                                                globe.valley English,
                                                stress.separate,
                                                globe.trash,
                                                stress.rely draft,
                                                globe.among
                                         FROM               stress
                                                CROSS JOIN (SELECT bean.both valley,
                                                                  recording.soup clinic,
                                                                  recording.among,
                                                                  recording.trash
                                                           FROM          recording
                                                              CROSS JOIN (SELECT charge.both
                                                                          FROM charge
                                                                         ) bean
                                                          ) globe
                                        ) top
                    ) result
              ) restaurant
   CROSS JOIN wood
''',

/*
#23
           Length: 35
     Result Depth: 2
  Renames On Path: 1

->         Length: 35
->   Result Depth: 5
->Renames On Path: 2
DONE
*/
  '''
SELECT businessman.inquiry
FROM            (SELECT technical.minute inquiry
                 FROM (SELECT encounter.frozen ,
                            encounter.while once,
                            encounter.minute,
                            encounter.please wonder
                     FROM (SELECT Arab.weak frozen,
                                  stress.socially,
                                  stress.attention,
                                  Arab.while,
                                  stress.invent,
                                  stress.minute,
                                  Arab.theme please
                           FROM         Arab
                           CROSS JOIN   (SELECT   total.socially,
                                                  total.attention,
                                                  total.invent,
                                                  total.guarantee minute
                                           FROM (SELECT auto.attention,
                                                        auto.recognition,
                                                        auto.guarantee,
                                                        auto.invent,
                                                        auto.inspiration socially
                                                 FROM auto
                                                ) total
                                          ) stress
                          ) encounter
                    ) technical
              ) businessman
   CROSS JOIN (SELECT data.seminar,
                      data.home,
                      data.border,
                      data.seat
               FROM data
              ) thick
''',
/*
#24
           Length: 30
     Result Depth: 3
  Renames On Path: 3

->         Length: 35
->   Result Depth: 5
->Renames On Path: 2
DONE
*/
  '''
SELECT contact.chingchang
FROM          (SELECT notice.threat,
                      notice.slice medium,
                      notice.chingchang
               FROM (SELECT soap.shame,
                            soap.threat,
                            soap.chingchang,
                            soap.star,
                            soap.poem slice
                     FROM (SELECT difficulty.exist poem,
                                  difficulty.shame,
                                  difficulty.dirty threat,
                                  hour.chingchang,
                                  difficulty.star,
                                  difficulty.more crack
                           FROM             difficulty
                                CROSS JOIN (SELECT pink.frequently,
                                                    pink.frequently,
                                                    pink.sport chingchang,
                                                    pink.salmon,
                                                    pink.fold
                                            FROM (SELECT fragment.barn,
                                                        fragment.frequently,
                                                        fragment.proceed sport,
                                                        fragment.fold,
                                                        fragment.humor salmon
                                                 FROM fragment
                                                ) pink
                                           ) hour
                          ) soap
                    ) notice
              ) contact
   CROSS JOIN (SELECT defend.exist
               FROM defend
              ) chamber
''',


/*
#25
           Length: 37
     Result Depth: 4
  Renames On Path: 3

->         Length: 35
->   Result Depth: 5
->Renames On Path: 4
*/
  '''
SELECT mass.strictly
FROM          (SELECT objective.mate strictly
               FROM (SELECT tape.moderate mate,
                            tape.desperate
                     FROM (SELECT practical.moderate,
                                  practical.favor ingredient,
                                  practical.line desperate
                           FROM (SELECT poverty.dream,
                                        middle.deliver moderate,
                                        poverty.doubt carve,
                                        poverty.line,
                                        poverty.classic favor
                                 FROM           poverty
                                     CROSS JOIN (SELECT extent.own deliver,
                                                        extent.return,
                                                        extent.journal plenty,
                                                        extent.monument,
                                                        extent.concentrate helicopter,
                                                        extent.competitor
                                                 FROM extent
                                                ) middle
                                ) practical
                          ) tape
                    ) objective
              ) mass
   CROSS JOIN (SELECT favor.jury reserve,
                      favor.drop,
                      favor.bowl
               FROM favor
              ) distribution
   CROSS JOIN (SELECT middle.online other,
                      middle.helicopter strip,
                      middle.plenty substantial
               FROM middle
              ) slot
''',
/*
#26
           Length: 37
     Result Depth: 0
  Renames On Path: 1

->         Length: 35
->   Result Depth: 5
->Renames On Path: 4
*/
  '''
SELECT green.ringy
FROM          elevator
   CROSS JOIN shareholder
   CROSS JOIN pack
   CROSS JOIN auction
   CROSS JOIN (SELECT area.carve entity,
                      link.found changing,
                      area.found,
                      rape.ringy,
                      rape.vision,
                      area.shorts
               FROM          (SELECT pray.reliable evaluate,
                                     pray.vision,
                                     pray.course adviser,
                                     surface.specialize ringy,
                                     pray.decade,
                                     pray.justice
                              FROM             pray
                                    CROSS JOIN (SELECT thereby.trigger specialize
                                                 FROM (SELECT do.disturbing trigger
                                                       FROM (SELECT there.guard disturbing
                                                             FROM there
                                                            ) do
                                                      ) thereby
                                                ) surface
                             ) rape
                  CROSS JOIN area
                  CROSS JOIN link
              ) green
   CROSS JOIN fundamental

   CROSS JOIN (SELECT junior.undermine,
                      junior.exploration monitor
               FROM junior
              ) skull
   CROSS JOIN easy
''',
/*
#27
           Length: 38
     Result Depth: 1
  Renames On Path: 0

->         Length: 35
->   Result Depth: 5
->Renames On Path: 4
*/
  '''
SELECT focus.hypothesis
FROM          expectation
   CROSS JOIN (SELECT classroom.experience hypothesis
               FROM          blame
                  CROSS JOIN air
                  CROSS JOIN (SELECT sibling.dingbing experience,
                                     sibling.ideal vacation
                              FROM (SELECT desk.offer class,
                                           desk.fresh ear,
                                           desk.praise dingbing,
                                           desk.cheat ideal
                                    FROM
                                                (SELECT form.become former,
                                                          form.present Chinese,
                                                          form.offer,
                                                          buyer.analyze praise,
                                                          form.exhibit weak,
                                                          form.cheat,
                                                          form.military,
                                                          form.fashion fresh
                                                   FROM              form
                                                          CROSS JOIN (SELECT certainly.analyze,
                                                                            certainly.minute telescope,
                                                                            certainly.ideal
                                                                     FROM certainly
                                                                    ) buyer
                                                  ) desk
                                      CROSS JOIN (SELECT distinction.missing give,
                                                            distinction.either river,
                                                            distinction.protection,
                                                            distinction.bad
                                                     FROM distinction
                                                    ) cheap
                                   ) sibling
                             ) classroom
              ) focus

''',
/*
#28
           Length: 36
     Result Depth: 0
  Renames On Path: 0

->         Length: 35
->   Result Depth: 5
->Renames On Path: 4
*/
  '''
SELECT representation.coco
FROM          constitutional
   CROSS JOIN (SELECT oil.chicken coco,
                      oil.changing founder
               FROM          (SELECT mineral.route catalog,
                                     mineral.through slice,
                                     significant.chase chicken,
                                     mineral.speculate changing,
                                     mineral.patient,
                                     mineral.sudden environmental
                              FROM                  mineral
                                       CROSS JOIN (SELECT straight.tone chase,
                                                          straight.differ crowd,
                                                          straight.conservation entity,
                                                          straight.service
                                                    FROM (SELECT ready.since conservation,
                                                                 ready.agency,
                                                                 ready.behavior sustain,
                                                                 wonderful.tone,
                                                                 ready.list differ,
                                                                 ready.service
                                                              FROM              ready
                                                                    CROSS JOIN (SELECT organ.seven switch,
                                                                                         organ.pile tone,
                                                                                         organ.push privacy,
                                                                                         organ.highlight trauma,
                                                                                         organ.check
                                                                                  FROM organ
                                                                                 ) wonderful
                                                          ) straight
                                                       ) significant
                             ) oil
              ) representation
   CROSS JOIN terrific
   CROSS JOIN burn ;
''',

/*
#29
           Length: 39
     Result Depth: 0
  Renames On Path: 1

->         Length: 35
->   Result Depth: 5
->Renames On Path: 4
*/
  '''
SELECT okay.trace
FROM           oral
   CROSS JOIN (SELECT application.sufficient
               FROM (SELECT lately.protocol sufficient
                     FROM lately
                    ) application
              ) treatment
   CROSS JOIN (SELECT low.sexy,
                      low.familiar suit
               FROM low
              ) useful
   CROSS JOIN (SELECT uncertainty.only,
                      uncertainty.look trace,
                      uncertainty.march Jewish
               FROM (SELECT cling.fashion march,
                            cling.benefit reserve,
                            cling.only,
                            written.mention look
                     FROM           (SELECT biology.minimal mention,
                                            biology.environmental irony
                                        FROM (SELECT thing.bangbang minimal,
                                                      thing.gaze environmental,
                                                      thing.midst interior
                                               FROM (SELECT terrorism.under midst,
                                                           terrorism.bangbang,
                                                           terrorism.just gaze
                                                      FROM terrorism
                                                     ) thing
                                           ) biology
                                     ) written
                          CROSS JOIN cling
                         CROSS JOIN anonymous
                    ) uncertainty
              ) okay
   CROSS JOIN strict
''',

/*
#30
           Length: 76
     Result Depth: 3
  Renames On Path: 3

->         Length: 70
->   Result Depth: 1
->Renames On Path: 0
DONE
*/
  '''
SELECT clean.jar
FROM          dismiss
   CROSS JOIN (SELECT theoretical.intervention travel,
                      theoretical.exotic incident,
                      theoretical.ally conspiracy,
                      theoretical.courage,
                      theoretical.Hispanic,
                      strike.through symbol,
                      strike.agriculture,
                      theoretical.wish
               FROM          (SELECT damage.bus through,
                                     damage.nest agriculture
                              FROM          (SELECT movement.write,
                                                    tune.airline,
                                                    movement.purpose,
                                                    movement.distinction,
                                                    movement.protest
                                             FROM          (SELECT appear.broadcast rock,
                                                                   appear.purpose,
                                                                   appear.habitat,
                                                                   appear.distinctive,
                                                                   appear.distinction,
                                                                   appear.protest,
                                                                   appear.flood write,
                                                                   appear.salt science
                                                            FROM appear
                                                           ) movement
                                                CROSS JOIN (SELECT sector.purple airline
                                                            FROM sector
                                                           ) tune
                                            ) hole
                                 CROSS JOIN (SELECT river.shit assistance,
                                                    river.motor worth,
                                                    river.frame,
                                                    river.nest,
                                                    river.bus
                                             FROM river
                                            ) damage
                             ) strike
                  CROSS JOIN theoretical
              ) English
   CROSS JOIN (SELECT presumably.jar,
                      presumably.global southwest,
                      presumably.district cooperative,
                      presumably.poetry,
                      presumably.note particle,
                      presumably.pen belt
               FROM presumably
              ) clean
   CROSS JOIN (SELECT survival.crop
               FROM (SELECT increased.crop
                     FROM (SELECT prayer.crop
                           FROM prayer
                          ) increased
                    ) survival
              ) hire
   CROSS JOIN (SELECT bring.desperately place,
                      bring.twice,
                      bring.corn
               FROM bring
              ) homeland
   CROSS JOIN (SELECT patient.hurry bronze,
                      patient.bind hunt,
                      patient.combat
               FROM (SELECT block.hurry,
                            block.announcement combat,
                            block.bind
                     FROM block
                    ) patient
              ) personally ;
''',

/*
#31
           Length: 70
     Result Depth: 5
  Renames On Path: 2

->         Length: 70
->   Result Depth: 1
->Renames On Path: 0
*/
  '''
SELECT spare.equity
FROM          (SELECT smart.blue,
                      smart.root,
                      bend.exact,
                      scared.stare sick,
                      speaker.equity,
                      smart.supervisor wealthy,
                      speaker.drug
               FROM          (SELECT supermarket.exact,
                                     supermarket.rim discovery
                              FROM (SELECT pole.rim,
                                           pole.violate exact
                                    FROM (SELECT it.implement rim,
                                                 it.terrible violate,
                                                 it.belief
                                          FROM it
                                         ) pole
                                   ) supermarket
                             ) bend
                  CROSS JOIN (SELECT hit.arrest,
                                     hit.bulk mathematics
                              FROM (SELECT please.challenge bulk,
                                           please.lean arrest
                                    FROM please
                                   ) hit
                             ) auto
                  CROSS JOIN speaker
                  CROSS JOIN (SELECT financial.stare,
                                     financial.electronics restrict,
                                     financial.trouble outcome
                              FROM (SELECT Ms.Jew,
                                           Ms.stare,
                                           Ms.trouble,
                                           Ms.electronics
                                    FROM (SELECT wake.trouble,
                                                 wake.era electronics,
                                                 wake.stare,
                                                 wake.instructional slope,
                                                 wake.coming Jew
                                          FROM wake
                                         ) Ms
                                   ) financial
                             ) scared
                  CROSS JOIN smart
              ) spare
   CROSS JOIN (SELECT lung.senior
               FROM (SELECT abstract.senior
                     FROM (SELECT cottage.real technique,
                                  cottage.inside senior,
                                  cottage.compete
                           FROM (SELECT majority.inside,
                                        majority.crack concede,
                                        majority.compete,
                                        majority.real,
                                        majority.snake,
                                        majority.they
                                 FROM (SELECT autonomy.this they,
                                              autonomy.lung crack,
                                              autonomy.assault inside,
                                              autonomy.charm far,
                                              autonomy.split snake,
                                              autonomy.favor real,
                                              autonomy.managing compete
                                       FROM autonomy
                                      ) majority
                                ) cottage
                          ) abstract
                    ) lung
              ) advocate
   CROSS JOIN nobody ;
''',











/*
#32
           Length: 76
     Result Depth: 1
  Renames On Path: 1

->         Length: 70
->   Result Depth: 1
->Renames On Path: 0
*/
  '''
SELECT how.automatically
FROM          (SELECT speculation.thumb intellectual,
                      eligible.questionnaire,
                      over.steep,
                      speculation.cotton,
                      estimate.automatically
               FROM          (SELECT peer.healthy questionnaire
                              FROM (SELECT committee.check healthy
                                    FROM (SELECT hint.check
                                          FROM (SELECT similarly.check
                                                FROM (SELECT page.output check
                                                      FROM (SELECT no.survival output
                                                            FROM no
                                                           ) page
                                                     ) similarly
                                               ) hint
                                         ) committee
                                   ) peer
                             ) eligible
                  CROSS JOIN estimate
                  CROSS JOIN (SELECT replacement.steep,
                                     replacement.requirement
                              FROM (SELECT daily.best requirement,
                                           daily.amendment steep
                                    FROM (SELECT carrot.those amendment,
                                                 carrot.shake,
                                                 carrot.best
                                          FROM (SELECT model.creative shake,
                                                       model.lawmaker those,
                                                       model.decline best
                                                FROM model
                                               ) carrot
                                         ) daily
                                   ) replacement
                             ) over
                  CROSS JOIN speculation
                  CROSS JOIN definitely
              ) how
   CROSS JOIN (SELECT telescope.illustrate collaboration,
                      telescope.bathroom each
               FROM (SELECT counter.rank available,
                            counter.minister bathroom,
                            counter.herb illustrate
                     FROM counter
                    ) telescope
              ) think
   CROSS JOIN (SELECT matter.reporting
               FROM (SELECT metaphor.mere reporting
                     FROM (SELECT chest.mere
                           FROM (SELECT dinner.lay house,
                                        dinner.principal mere,
                                        dinner.for
                                 FROM dinner
                                ) chest
                          ) metaphor
                    ) matter
              ) deliver
   CROSS JOIN (SELECT curtain.than
               FROM (SELECT exchange.spray,
                            exchange.either than,
                            exchange.verdict wish
                     FROM (SELECT straighten.best bike,
                                  straighten.identify either,
                                  straighten.wagon,
                                  straighten.spray,
                                  straighten.verdict
                           FROM straighten
                          ) exchange
                    ) curtain
              ) cattle ;
''',


/*
#33
           Length: 68
     Result Depth: 0
  Renames On Path: 0

->         Length: 70
->   Result Depth: 1
->Renames On Path: 0
*/
  '''
SELECT educational.bunch
FROM          work
   CROSS JOIN (SELECT missionary.guard,
                      access.consciousness structure,
                      property.consequently confirm,
                      flying.present,
                      flying.furniture,
                      astronomer.bunch,
                      astronomer.enroll
               FROM          astronomer
                  CROSS JOIN flying
                  CROSS JOIN (SELECT pull.criticize,
                                     pull.bet excited,
                                     pull.contemplate,
                                     pull.address peel,
                                     pull.phone web,
                                     pull.guard,
                                     pull.significantly
                              FROM (SELECT cease.cheer address,
                                           bulb.superior guard,
                                           depression.contemplate,
                                           their.otherwise criticize,
                                           bulb.phone,
                                           later.worldwide jeans,
                                           cease.bet,
                                           their.media significantly
                                    FROM          (SELECT both.strict reference,
                                                          both.rat,
                                                          both.contemplate
                                                   FROM (SELECT depth.guilty contemplate,
                                                                depth.strict,
                                                                depth.mission cooperative,
                                                                depth.divide minister,
                                                                depth.series rat,
                                                                depth.salmon,
                                                                depth.respondent obvious
                                                         FROM depth
                                                        ) both
                                                  ) depression
                                       CROSS JOIN (SELECT schedule.naval frown,
                                                          schedule.federal dining,
                                                          schedule.severe,
                                                          schedule.blala,
                                                          schedule.dynamics,
                                                          schedule.supervisor worldwide
                                                   FROM schedule
                                                  ) later
                                       CROSS JOIN cease
                                       CROSS JOIN bulb
                                       CROSS JOIN their
                                   ) pull
                             ) missionary
                  CROSS JOIN (SELECT rely.experience,
                                     rely.stomach,
                                     rely.oral unprecedented,
                                     rely.glory extensive,
                                     rely.pay,
                                     rely.rank consequently
                              FROM rely
                             ) property
                  CROSS JOIN (SELECT well.orange consciousness,
                                     well.hope,
                                     well.porch expect,
                                     well.rumor discuss,
                                     well.odd,
                                     well.guilty
                              FROM well
                             ) access
              ) educational
   CROSS JOIN premium ;
''',

/*
#34
           Length: 68
     Result Depth: 0
  Renames On Path: 0

->         Length: 70
->   Result Depth: 1
->Renames On Path: 0
*/
  '''
SELECT tomorrow.born
FROM          initiate
   CROSS JOIN (SELECT ie.format doorway,
                      effective.atop,
                      effective.ironically improvement,
                      ie.born,
                      effective.stop,
                      effective.result sea
               FROM          ie
                  CROSS JOIN effective
              ) tomorrow
   CROSS JOIN conception
   CROSS JOIN vote
   CROSS JOIN (SELECT breast.thank near,
                      breast.coin shoulder,
                      breast.secure,
                      breast.optimistic cooking
               FROM (SELECT testify.thank,
                            testify.barn secure,
                            testify.wonder coin,
                            testify.dictate optimistic
                     FROM (SELECT very.bench dictate,
                                  very.past thank,
                                  very.assess barn,
                                  very.midst,
                                  very.wonder,
                                  very.fur
                           FROM very
                          ) testify
                    ) breast
              ) pizza
   CROSS JOIN (SELECT severely.quote future,
                      severely.belt,
                      severely.hopefully return
               FROM (SELECT circumstance.toilet,
                            circumstance.quote,
                            circumstance.proud,
                            circumstance.hopefully,
                            circumstance.enable description,
                            circumstance.complexity,
                            outstanding.wave belt,
                            circumstance.towel
                     FROM          circumstance
                        CROSS JOIN (SELECT installation.investor wave
                                    FROM (SELECT area.dirt investor,
                                                 area.Arab tear,
                                                 area.rescue,
                                                 area.bike
                                          FROM (SELECT emerging.dirt,
                                                       emerging.weight rescue,
                                                       emerging.target,
                                                       emerging.pack holy,
                                                       emerging.bike,
                                                       emerging.aid,
                                                       emerging.march Arab,
                                                       emerging.norm
                                                FROM emerging
                                               ) area
                                         ) installation
                                   ) outstanding
                    ) severely
              ) agricultural
   CROSS JOIN (SELECT sort.president coordinate,
                      sort.weakness religion,
                      sort.exclusively construct,
                      sort.hill,
                      sort.pressure,
                      sort.associate
               FROM sort
              ) arrive
''',

/*
#35
           Length: 80
     Result Depth: 2
  Renames On Path: 2

->         Length: 70
->   Result Depth: 3
->Renames On Path: 0
*/
  '''
SELECT ambitious.egg
FROM          (SELECT evident.living,
                      seriously.spoon,
                      seriously.garage gun,
                      evident.pour,
                      seriously.disappear,
                      evident.stair,
                      evident.rating,
                      seriously.faith highway
               FROM          (SELECT tooth.ordinary concerning,
                                     tooth.spoon,
                                     tooth.controversy identification,
                                     tooth.forth faith,
                                     tooth.garage,
                                     tooth.disappear
                              FROM tooth
                             ) seriously
                  CROSS JOIN (SELECT captain.far,
                                     captain.processor stair,
                                     captain.rating,
                                     captain.pour,
                                     captain.regular living
                              FROM captain
                             ) evident
              ) elegant
   CROSS JOIN (SELECT Israeli.egg
               FROM (SELECT ambitious.egg
                     FROM (SELECT plant.revolution operator,
                                  plant.female,
                                  chicken.egg,
                                  plant.familiar
                           FROM             (SELECT sweater.revolution,
                                                    sweater.inmate female,
                                                    sweater.familiar
                                             FROM (SELECT light.dealer familiar,
                                                          light.winner inmate,
                                                          light.bit revolution
                                                   FROM light
                                                  ) sweater
                                            ) plant
                                CROSS JOIN chicken
                          ) ambitious
                    ) Israeli
              ) ambitious
   CROSS JOIN combined
   CROSS JOIN (SELECT deer.personally already
               FROM (SELECT swear.personally
                     FROM (SELECT mental.personally
                           FROM (SELECT effect.bride personally
                                 FROM (SELECT silence.bride
                                       FROM (SELECT trail.spring bride,
                                                    trail.mobile,
                                                    trail.invent dictate
                                             FROM (SELECT host.mobile,
                                                          host.spring,
                                                          host.invent
                                                   FROM host
                                                  ) trail
                                            ) silence
                                      ) effect
                                ) mental
                          ) swear
                    ) deer
              ) Internet
   CROSS JOIN convince
   CROSS JOIN (SELECT sale.grant
               FROM  sale
              ) conversation
   CROSS JOIN back
   CROSS JOIN sweet
''',

/*
#36
           Length: 65
     Result Depth: 1
  Renames On Path: 1

->         Length: 70
->   Result Depth: 3
->Renames On Path: 0
*/
  '''
SELECT matter.talk
FROM          (SELECT originally.accent,
                      originally.district confrontation
               FROM originally
              ) international
   CROSS JOIN rocket
   CROSS JOIN category
   CROSS JOIN (SELECT forty.jeans
               FROM (SELECT psychological.appreciation,
                            psychological.writing,
                            psychological.note jeans,
                            psychological.fur,
                            psychological.salt explain,
                            psychological.harmony react,
                            psychological.constantly insurance,
                            psychological.curtain cell
                     FROM psychological
                    ) forty
              ) Italian
   CROSS JOIN (SELECT several.evaluation drama,
                      several.disability German,
                      several.driveway,
                      several.home swimming,
                      several.journalist
               FROM several
              ) model
   CROSS JOIN (SELECT interesting.preach for,
                      interesting.comment,
                      interesting.biology,
                      interesting.counterpart Korean,
                      interesting.fifteen supposed
               FROM interesting
              ) exploration
   CROSS JOIN (SELECT initial.debate,
                      initial.content map,
                      initial.receiver collective,
                      initial.oral
               FROM (SELECT sometime.chocolate,
                            sometime.receiver,
                            sometime.provision debate,
                            sometime.oral,
                            sometime.content
                     FROM sometime
                    ) initial
              ) custom
   CROSS JOIN (SELECT editor.rape,
                      editor.talk
               FROM (SELECT good.rape,
                            good.talk
                     FROM (SELECT contact.rape,
                                  alexander.talk
                           FROM             alexander
                                CROSS JOIN  (SELECT supply.rape
                                             FROM (SELECT consist.brown rape
                                                   FROM (SELECT instructional.brown,
                                                                instructional.allegedly
                                                         FROM (SELECT pie.pasta,
                                                                      pie.plea,
                                                                      pie.spill brown,
                                                                      pie.differ allegedly,
                                                                      pie.swing
                                                               FROM pie
                                                              ) instructional
                                                        ) consist
                                                  ) supply
                                            ) contact
                          ) good
                    ) editor
              ) matter
   CROSS JOIN global ;
''',
/*
#37
           Length: 66
     Result Depth: 0
  Renames On Path: 1

->         Length: 70
->   Result Depth: 3
->Renames On Path: 0
*/
  '''
SELECT lover.foundation
FROM          human
   CROSS JOIN (SELECT substantial.her vital,
                      substantial.native demand,
                      substantial.call hay,
                      loose.career,
                      substantial.foundation
               FROM          (SELECT conclude.sometime call,
                                     conclude.foundation,
                                     element.native,
                                     element.barn impact,
                                     conclude.constitute film,
                                     conclude.scan her
                              FROM          (SELECT box.accurate
                                             FROM (SELECT rear.result accurate
                                                   FROM (SELECT return.bill result,
                                                                return.assassin
                                                         FROM return
                                                        ) rear
                                                  ) box
                                            ) equal
                                 CROSS JOIN (SELECT lunch.reputation constitute,
                                                    mirror.foundation,
                                                    lunch.concern tennis,
                                                    mirror.scan,
                                                    mirror.sometime,
                                                    lunch.Korean
                                             FROM             (SELECT far.overlook,
                                                                      far.persist,
                                                                      far.reputation,
                                                                      far.speculation,
                                                                      far.triumph concern,
                                                                      far.Korean
                                                               FROM far
                                                              ) lunch
                                                  CROSS JOIN  mirror
                                            ) conclude
                                 CROSS JOIN element
                             ) substantial
                  CROSS JOIN (SELECT grandchild.career,
                                     grandchild.ten skin
                              FROM (SELECT claim.ten,
                                           claim.consist career
                                    FROM (SELECT middle.Indian ten,
                                                 middle.hook virtual,
                                                 middle.border effectively,
                                                 middle.officially consist
                                          FROM (SELECT poor.border,
                                                       poor.neither shock,
                                                       poor.disc similar,
                                                       poor.familiar,
                                                       poor.hook,
                                                       poor.officially,
                                                       poor.perfect comparison,
                                                       poor.coach Indian
                                                FROM poor
                                               ) middle
                                         ) claim
                                   ) grandchild
                             ) loose
              ) lover
   CROSS JOIN (SELECT vessel.spot frown,
                      vessel.victory
               FROM (SELECT thread.spot,
                            thread.victory
                     FROM thread
                    ) vessel
              ) convinced
   CROSS JOIN attorney
   CROSS JOIN greet
''',
/*
#38
           Length: 70
     Result Depth: 1
  Renames On Path: 0

->         Length: 70
->   Result Depth: 3
->Renames On Path: 0
*/
  '''
SELECT metal.scan
FROM          rarely
   CROSS JOIN (SELECT garden.habitat spectacular,
                      touchdown.scan,
                      garden.similarly,
                      peak.separation
               FROM          garden
                  CROSS JOIN peak
                  CROSS JOIN (SELECT disorder.convince safely,
                                     sexy.southern,
                                     sexy.broadcast fixed,
                                     disorder.scan
                              FROM          (SELECT cue.decent,
                                                    cue.explain,
                                                    cue.convince,
                                                    cue.personnel show,
                                                    cue.pure entire,
                                                    cue.proud sack,
                                                    cue.chance,
                                                    cue.scan
                                             FROM cue
                                            ) disorder
                                 CROSS JOIN sexy
                             ) touchdown
                  CROSS JOIN (SELECT self.identity
                              FROM (SELECT mall.poem identity
                                    FROM (SELECT gender.nevertheless poem
                                          FROM (SELECT celebrate.hard,
                                                       celebrate.nevertheless,
                                                       celebrate.link smile,
                                                       celebrate.standard,
                                                       celebrate.shock
                                                FROM celebrate
                                               ) gender
                                         ) mall
                                   ) self
                             ) squad
              ) metal
   CROSS JOIN (SELECT TV.assistance pollution
               FROM TV
              ) again
   CROSS JOIN basketball
   CROSS JOIN (SELECT diverse.shelter usual,
                      diverse.widow,
                      diverse.spread,
                      diverse.leave,
                      diverse.scheme,
                      diverse.atmosphere,
                      diverse.moral show
               FROM diverse
              ) openly
   CROSS JOIN (SELECT shore.eye inspection
               FROM (SELECT plus.beam eye
                     FROM (SELECT funeral.beam
                           FROM (SELECT weak.portrait beam
                                 FROM (SELECT physics.organism portrait
                                       FROM (SELECT doorway.alive organism
                                             FROM (SELECT account.injury ancient,
                                                          account.prediction,
                                                          account.alive,
                                                          account.elect,
                                                          account.regret
                                                   FROM account
                                                  ) doorway
                                            ) physics
                                      ) weak
                                ) funeral
                          ) plus
                    ) shore
              ) missionary ;
''',













/*
#39
           Length: 77
     Result Depth: 1
  Renames On Path: 1

->         Length: 70
->   Result Depth: 3
->Renames On Path: 0
*/
  '''
SELECT signal.level
FROM          (SELECT coat.reserve,
                      reliability.million,
                      coat.total,
                      coat.cook enroll,
                      reliability.level
               FROM          coat
                  CROSS JOIN (SELECT rapid.essay,
                                     rapid.touchdown,
                                     rapid.million,
                                     rapid.purchase,
                                     rapid.museum artist,
                                     rapid.level
                              FROM (SELECT venture.hotel million,
                                           venture.internal museum,
                                           venture.level,
                                           venture.essay,
                                           venture.attribute purchase,
                                           venture.touch touchdown
                                    FROM venture
                                   ) rapid
                             ) reliability
              ) signal
   CROSS JOIN (SELECT production.tight,
                      production.advantage professional
               FROM (SELECT respect.advantage,
                            respect.either,
                            respect.metropolitan tight
                     FROM (SELECT grain.hope metropolitan,
                                  grain.chapter either,
                                  grain.advantage
                           FROM (SELECT diabetes.have chapter,
                                        diabetes.hope,
                                        diabetes.aide advantage
                                 FROM (SELECT shit.hope,
                                              shit.have,
                                              shit.aide,
                                              shit.winner evidence
                                       FROM (SELECT happily.regarding winner,
                                                    happily.hope,
                                                    happily.have,
                                                    happily.aide,
                                                    happily.doubt long
                                             FROM (SELECT blade.interrupt century,
                                                          blade.recognize aide,
                                                          blade.hunting doubt,
                                                          blade.midst hope,
                                                          blade.have,
                                                          blade.regarding
                                                   FROM blade
                                                  ) happily
                                            ) shit
                                      ) diabetes
                                ) grain
                          ) respect
                    ) production
              ) conviction
   CROSS JOIN (SELECT learning.harm,
                      learning.course dominate,
                      learning.crew,
                      learning.our Indian
               FROM learning
              ) lawyer
   CROSS JOIN (SELECT concerning.conception decline,
                      concerning.complicated,
                      concerning.passion,
                      concerning.tail die,
                      concerning.forever pure
               FROM concerning
              ) voting
''',
















/*
#40
           Length: 78
     Result Depth: 0
  Renames On Path: 1

->         Length: 70
->   Result Depth: 3
->Renames On Path: 2
*/
  '''
SELECT character.dark
FROM          (SELECT rain.impact,
                      rain.alliance,
                      open.stake dark
               FROM          annually
                  CROSS JOIN (SELECT convey.office cold,
                                     convey.partly liberal,
                                     convey.content specialist,
                                     convey.implementation
                              FROM          (SELECT store.bat folk
                                             FROM (SELECT invasion.labor bat
                                                   FROM (SELECT feature.labor
                                                         FROM feature
                                                        ) invasion
                                                  ) store
                                            ) steadily
                                 CROSS JOIN convey
                             ) rumor
                  CROSS JOIN rain
                  CROSS JOIN (SELECT wagon.politically calm
                              FROM wagon
                             ) band
                  CROSS JOIN (SELECT gaze.await,
                                     gaze.immediately stake,
                                     gaze.world
                              FROM (SELECT computer.indicate drop,
                                           computer.world,
                                           computer.await,
                                           computer.immediately
                                    FROM computer
                                   ) gaze
                             ) open
                  CROSS JOIN (SELECT beautiful.household
                              FROM (SELECT simple.scream,
                                           simple.underlying household
                                    FROM (SELECT smoke.underlying,
                                                 smoke.dawn scream,
                                                 smoke.cover burn
                                          FROM (SELECT rather.pink underlying,
                                                       rather.fraction hence,
                                                       rather.alone cover,
                                                       rather.dawn
                                                FROM rather
                                               ) smoke
                                         ) simple
                                   ) beautiful
                             ) freeze
              ) character
   CROSS JOIN second
   CROSS JOIN (SELECT activity.commodity ancestor,
                      activity.sound
               FROM activity
              ) wife
   CROSS JOIN lie
   CROSS JOIN (SELECT relieve.witness
               FROM (SELECT dress.witness
                     FROM (SELECT reflection.render,
                                  reflection.educational witness,
                                  reflection.worldwide,
                                  reflection.company most
                           FROM (SELECT search.render,
                                        search.worldwide,
                                        search.opposed company,
                                        search.educational,
                                        search.Israeli privilege
                                 FROM search
                                ) reflection
                          ) dress
                    ) relieve
              ) either ;
''',
















/*
#41
           Length: 75
     Result Depth: 1
  Renames On Path: 2

->         Length: 70
->   Result Depth: 3
->Renames On Path: 2
*/
  '''
SELECT cloth.lack
FROM (SELECT address.articulate,
             address.coverage,
             greet.counterpart,
             address.troubled what,
             greet.hate detective,
             day.twist lack,
             greet.pitcher,
             greet.corridor assign
      FROM          (SELECT art.symptom
                     FROM (SELECT mechanic.symptom
                           FROM          (SELECT window.gathering
                                          FROM (SELECT accelerate.vs gathering
                                                FROM (SELECT attraction.overlook vs
                                                      FROM (SELECT management.overlook
                                                            FROM          (SELECT collapse.neither
                                                                           FROM (SELECT justice.neither
                                                                                 FROM (SELECT Islam.neither
                                                                                       FROM (SELECT seem.neither,
                                                                                                    seem.decent bother
                                                                                             FROM (SELECT picture.glance,
                                                                                                          picture.neither,
                                                                                                          picture.decent,
                                                                                                          picture.conservative animal
                                                                                                   FROM picture
                                                                                                  ) seem
                                                                                            ) Islam
                                                                                      ) justice
                                                                                ) collapse
                                                                          ) light
                                                               CROSS JOIN (SELECT fascinating.convention clue,
                                                                                  fascinating.effectiveness pant,
                                                                                  fascinating.withdrawal,
                                                                                  fascinating.though southwest,
                                                                                  fascinating.hungry overlook,
                                                                                  fascinating.speak
                                                                           FROM fascinating
                                                                          ) management
                                                           ) attraction
                                                     ) accelerate
                                               ) window
                                         ) elite
                              CROSS JOIN (SELECT always.channel symptom
                                          FROM (SELECT pleasant.swallow channel
                                                FROM (SELECT instruction.swallow
                                                      FROM (SELECT prefer.copy properly,
                                                                   prefer.counseling,
                                                                   prefer.jaw,
                                                                   prefer.protest swallow
                                                            FROM prefer
                                                           ) instruction
                                                     ) pleasant
                                               ) always
                                         ) mechanic
                          ) art
                    ) passenger
         CROSS JOIN address
         CROSS JOIN (SELECT exhaust.twist,
                            exhaust.flash
                     FROM (SELECT position.flash,
                                  position.diplomatic twist
                           FROM position
                          ) exhaust
                    ) day
         CROSS JOIN greet
         CROSS JOIN (SELECT desperately.vital ha,
                            desperately.rabbit research
                     FROM desperately
                    ) visit
     ) cloth ;
''',











/*
#42
           Length: 74
     Result Depth: 0
  Renames On Path: 0

->         Length: 70
->   Result Depth: 3
->Renames On Path: 2
*/
  '''
SELECT divorce.quietly
FROM          (SELECT offense.unlike,
                      offense.upper clock,
                      pizza.nothing industry,
                      pizza.Republican principal,
                      pizza.shrink integration,
                      pizza.operator,
                      offense.sign,
                      pizza.aircraft sake
               FROM          (SELECT practitioner.unlike,
                                     drop.sign,
                                     drop.side upper,
                                     drop.AIDS
                              FROM          (SELECT tuck.unlike
                                             FROM (SELECT Internet.artistic unlike
                                                   FROM (SELECT acquire.respond discourage,
                                                                acquire.German teacher,
                                                                acquire.preparation artistic
                                                         FROM (SELECT average.shot,
                                                                      average.tire respond,
                                                                      average.ancestor German,
                                                                      average.drop endorse,
                                                                      average.assume preparation,
                                                                      average.divorce
                                                               FROM average
                                                              ) acquire
                                                        ) Internet
                                                  ) tuck
                                            ) practitioner
                                 CROSS JOIN (SELECT during.initiative AIDS,
                                                    during.trace beach,
                                                    during.sign,
                                                    during.isolation map,
                                                    during.consistently,
                                                    during.evidence side
                                             FROM during
                                            ) drop
                             ) offense
                  CROSS JOIN pizza
              ) publish
   CROSS JOIN (SELECT lap.distinguish,
                      lap.revolution quietly,
                      lap.appreciation test
               FROM (SELECT adequate.appreciation,
                            adequate.appointment revolution,
                            adequate.distinguish
                     FROM (SELECT minimum.appreciation,
                                  minimum.appointment,
                                  minimum.aunt distinguish
                           FROM minimum
                          ) adequate
                    ) lap
              ) divorce
   CROSS JOIN input
   CROSS JOIN (SELECT competition.cargo,
                      competition.recover urge,
                      competition.art,
                      competition.Republican strengthen,
                      competition.confession eat,
                      competition.ha wrong
               FROM (SELECT girlfriend.cargo,
                            girlfriend.recover,
                            girlfriend.art,
                            girlfriend.severely Republican,
                            girlfriend.danger confession,
                            girlfriend.ha
                     FROM girlfriend
                    ) competition
              ) dessert
   CROSS JOIN minimize
''',
















/*
#43
           Length: 78
     Result Depth: 0
  Renames On Path: 1

->         Length: 70
->   Result Depth: 3
->Renames On Path: 2
*/
  '''
SELECT discourage.wet
FROM          close
   CROSS JOIN (SELECT ago.otherwise chip,
                      ago.month into,
                      sustainable.wet
               FROM          ago
                  CROSS JOIN (SELECT practically.afternoon,
                                     hardly.lion,
                                     hardly.commonly,
                                     hardly.him
                              FROM          (SELECT lucky.afternoon,
                                                    lucky.effective,
                                                    lucky.reach due
                                             FROM (SELECT trend.afternoon,
                                                          trend.reach,
                                                          trend.effective
                                                   FROM trend
                                                  ) lucky
                                            ) practically
                                 CROSS JOIN hardly
                             ) religious
                  CROSS JOIN (SELECT dare.chocolate,
                                     dare.net wet
                              FROM (SELECT for.funeral net,
                                           for.mine chocolate
                                    FROM for
                                   ) dare
                             ) sustainable
                  CROSS JOIN (SELECT indigenous.stress
                              FROM (SELECT demonstrate.likely stress
                                    FROM (SELECT subsequent.weigh likely
                                          FROM (SELECT figure.weigh
                                                FROM (SELECT retail.tool weigh
                                                      FROM (SELECT door.pump tool
                                                            FROM (SELECT constitution.impressive pump,
                                                                         constitution.print
                                                                  FROM (SELECT sun.release,
                                                                               sun.impressive,
                                                                               sun.print
                                                                        FROM (SELECT cancel.foreign print,
                                                                                     cancel.impressive,
                                                                                     cancel.release
                                                                              FROM cancel
                                                                             ) sun
                                                                       ) constitution
                                                                 ) door
                                                           ) retail
                                                     ) figure
                                               ) subsequent
                                         ) demonstrate
                                   ) indigenous
                             ) offer
                  CROSS JOIN (SELECT visual.belong
                              FROM (SELECT job.belong
                                    FROM (SELECT regularly.belong,
                                                 regularly.uh,
                                                 regularly.injure
                                          FROM regularly
                                         ) job
                                   ) visual
                             ) yard
                  CROSS JOIN (SELECT disabled.prosecution explore,
                                     disabled.sue
                              FROM (SELECT attraction.embrace,
                                           attraction.Israeli sue,
                                           attraction.prosecution
                                    FROM attraction
                                   ) disabled
                             ) entry
              ) discourage ;
''',

















/*
#44
           Length: 65
     Result Depth: 2
  Renames On Path: 3

->         Length: 70
->   Result Depth: 3
->Renames On Path: 2
*/
  '''
SELECT welfare.magnet
FROM          (SELECT boundary.this,
                      weight.item son,
                      gain.put temporary,
                      boundary.before farmer,
                      gain.depart,
                      weight.lifting,
                      weight.smurf,
                      gain.famous welcome,
                      gain.acid magnet,
                      weight.irony jeans
               FROM          weight
                  CROSS JOIN (SELECT crowd.depart,
                                     crowd.selection tie,
                                     public.acid,
                                     crowd.prompt wake,
                                     touch.put,
                                     crowd.via,
                                     touch.household,
                                     touch.total famous,
                                     crowd.adventure
                              FROM          (SELECT historic.invade acid
                                             FROM historic
                                            ) public
                                 CROSS JOIN touch
                                 CROSS JOIN (SELECT effectiveness.depart,
                                                    effectiveness.mandate slope,
                                                    effectiveness.at selection,
                                                    effectiveness.via,
                                                    effectiveness.bulk,
                                                    effectiveness.prompt,
                                                    effectiveness.excited adventure
                                             FROM (SELECT best.roll,
                                                          best.bulk,
                                                          best.prompt,
                                                          best.at,
                                                          best.amid depart,
                                                          best.mandate,
                                                          best.telescope via,
                                                          best.rider excited
                                                   FROM best
                                                  ) effectiveness
                                            ) crowd
                             ) gain
                  CROSS JOIN boundary
              ) welfare
   CROSS JOIN (SELECT out.now,
                      out.clinical tomato,
                      out.table elephant,
                      out.take reportedly
               FROM out
              ) swim
   CROSS JOIN (SELECT ad.especially reverse,
                      ad.vanish crucial,
                      ad.freedom diary,
                      ad.grasp,
                      ad.stake,
                      ad.parade,
                      ad.swimming
               FROM (SELECT territory.shape distribution,
                            territory.parade,
                            territory.especially,
                            territory.freedom,
                            territory.personally stake,
                            territory.grasp,
                            territory.uncle vanish,
                            territory.ignore swimming
                     FROM territory
                    ) ad
              ) conversion
''',












/*
#45
           Length: 74
     Result Depth: 1
  Renames On Path: 1

->         Length: 70
->   Result Depth: 5
->Renames On Path: 0
*/
  '''
SELECT principal.praise
FROM          (SELECT question.game sole,
                      question.jet,
                      sea.praise,
                      cocaine.processing,
                      input.alternative glance
               FROM          (SELECT highway.notice jet,
                                     illustrate.game,
                                     illustrate.introduce,
                                     attempt.lady,
                                     attempt.coast,
                                     attempt.loose commander,
                                     illustrate.category empty
                              FROM          (SELECT compete.maintain notice,
                                                    compete.window
                                             FROM compete
                                            ) highway
                                 CROSS JOIN appreciate
                                 CROSS JOIN illustrate
                                 CROSS JOIN (SELECT excessive.embarrassed,
                                                    excessive.white,
                                                    excessive.auction,
                                                    excessive.front
                                             FROM excessive
                                            ) grass
                                 CROSS JOIN (SELECT slight.human loose,
                                                    slight.another rain,
                                                    slight.lady,
                                                    slight.teaching explosion,
                                                    slight.coast,
                                                    slight.hidden age
                                             FROM slight
                                            ) attempt
                             ) question
                  CROSS JOIN (SELECT divorce.processing
                              FROM (SELECT stretch.star processing
                                    FROM stretch
                                   ) divorce
                             ) cocaine
                  CROSS JOIN (SELECT jury.statue alternative,
                                     jury.telescope because
                              FROM (SELECT fault.statue,
                                           fault.interview telescope,
                                           fault.late candy,
                                           fault.flexible
                                    FROM fault
                                   ) jury
                             ) input
                  CROSS JOIN (SELECT peanut.loyalty,
                                    peanut.praise
                              FROM (SELECT survivor.obtain loyalty,
                                            survivor.praise
                                    FROM (SELECT comfortable.homeland establish,
                                                 comfortable.obtain,
                                                 dictate.praise,
                                                 comfortable.list,
                                                 comfortable.reference formation
                                          FROM              comfortable
                                                CROSS JOIN (SELECT tendency.surprise,
                                                                tendency.praise,
                                                                tendency.dig,
                                                                tendency.shareholder,
                                                                tendency.obtain
                                                             FROM tendency
                                                            ) dictate
                                         ) survivor
                                   ) peanut
                             ) sea
              ) principal
   CROSS JOIN public

''',













/*
#46
           Length: 70
     Result Depth: 2
  Renames On Path: 2

->         Length: 70
->   Result Depth: 5
->Renames On Path: 0
*/
  '''
SELECT pencil.bombing
FROM          (SELECT threaten.implication,
                      innovation.structural,
                      innovation.bombing
               FROM          (SELECT logic.suffering monitor,
                                     logic.structural,
                                     tea.bombing
                              FROM          separate
                                 CROSS JOIN logic
                                 CROSS JOIN (SELECT harvest.ball fit,
                                                    harvest.adapt,
                                                    recover.here breath,
                                                    yes.common,
                                                    filter.bombing,
                                                    harvest.scratch sad
                                             FROM          (SELECT thing.recording squad,
                                                                   thing.civilian twice,
                                                                   thing.here
                                                            FROM thing
                                                           ) recover
                                                CROSS JOIN harvest
                                                CROSS JOIN (SELECT helmet.common
                                                            FROM (SELECT commodity.stop common
                                                                  FROM (SELECT fundamental.barely stop
                                                                        FROM fundamental
                                                                       ) commodity
                                                                 ) helmet
                                                           ) yes
                                                CROSS JOIN (SELECT importance.agent,
                                                                   importance.ordinary,
                                                                   importance.vessel,
                                                                   importance.bombing,
                                                                   importance.every,
                                                                   importance.aggressive nobody
                                                            FROM (SELECT peel.think every,
                                                                         peel.agent,
                                                                         peel.spite license,
                                                                         peel.snake vessel,
                                                                         peel.bombing,
                                                                         peel.scale aggressive,
                                                                         peel.ordinary
                                                                  FROM peel
                                                                 ) importance
                                                           ) filter
                                            ) tea
                             ) innovation
                  CROSS JOIN (SELECT paint.use most,
                                     paint.civil trading,
                                     paint.least coffee,
                                     paint.negative speak,
                                     paint.certain,
                                     paint.dried,
                                     paint.attack onto
                              FROM paint
                             ) cheer
                  CROSS JOIN threaten
                  CROSS JOIN bill
              ) pencil
   CROSS JOIN (SELECT official.strain,
                      official.juror,
                      official.opening,
                      official.encouraging feedback
               FROM (SELECT psychologist.pile juror,
                            psychologist.encouraging,
                            psychologist.opening,
                            psychologist.average fund,
                            psychologist.strain
                     FROM psychologist
                    ) official
              ) speculate ;
''',














/*
#47
           Length: 77
     Result Depth: 0
  Renames On Path: 0

->         Length: 70
->   Result Depth: 5
->Renames On Path: 0
*/
  '''
SELECT midst.construct
FROM          father

   CROSS JOIN (SELECT impose.behind,
                      impose.heel,
                      impose.edition,
                      impose.southeast reliability,
                      impose.afternoon industrial
               FROM impose
              ) regard
  CROSS JOIN (SELECT southern.opponent,
                        southern.excitement element,
                        wrong.shirt article,
                        wrong.outdoor screen,
                        southern.hello their,
                        southern.suppose,
                        southern.American,
                        wrong.construct
                 FROM          (SELECT problem.American,
                                       ship.far opponent,
                                       ship.hello,
                                       ship.suppose,
                                       ship.practical study,
                                       dynamics.regime excitement
                                FROM          dynamics
                                   CROSS JOIN (SELECT democracy.negotiation,
                                                      democracy.Persian practical,
                                                      democracy.prohibit animal,
                                                      democracy.American command,
                                                      democracy.hello,
                                                      democracy.out suppose,
                                                      democracy.heavy hang,
                                                      democracy.far
                                               FROM democracy
                                              ) ship
                                   CROSS JOIN problem
                               ) southern
                    CROSS JOIN (SELECT DNA.spread lucky,
                                       DNA.frankly shirt,
                                       DNA.construct,
                                       DNA.outdoor,
                                       DNA.feature,
                                       DNA.satisfy
                                FROM (SELECT attention.frankly,
                                             image.spread,
                                             attention.feature,
                                             attention.recording satisfy,
                                             attention.opposite museum,
                                             image.construct,
                                             attention.honey outdoor
                                      FROM          attention
                                         CROSS JOIN (SELECT still.vegetable spread,
                                                            still.construct
                                                     FROM (SELECT realistic.vegetable,
                                                                  realistic.construct
                                                           FROM realistic
                                                          ) still
                                                    ) image
                                     ) DNA
                               ) wrong
                ) midst
   CROSS JOIN (SELECT migration.climate power
               FROM migration
              ) promise
   CROSS JOIN (SELECT panel.food,
                      panel.manufacturing
               FROM (SELECT coach.barely manufacturing,
                            coach.standard food
                     FROM coach
                    ) panel
              ) pink ;
''',
















/*
#48
           Length: 67
     Result Depth: 0
  Renames On Path: 0

->         Length: 70
->   Result Depth: 5
->Renames On Path: 0
*/
  '''
SELECT poet.wish
FROM          safe
   CROSS JOIN (SELECT neighbor.condemn,
                      violence.likely,
                      violence.wish,
                      Democrat.humanity,
                      neighbor.happily
               FROM          neighbor
                  CROSS JOIN (SELECT bottom.slow,
                                     pat.moment sandwich,
                                     different.wish,
                                     bottom.likely,
                                     different.makeup pro
                              FROM          pat
                                 CROSS JOIN (SELECT operating.general,
                                                    operating.status makeup,
                                                    refrigerator.streak worried,
                                                    refrigerator.wish,
                                                    operating.panic we
                                             FROM          (SELECT scratch.advocate,
                                                                   scratch.general,
                                                                   scratch.panic,
                                                                   scratch.status
                                                            FROM scratch
                                                           ) operating
                                                CROSS JOIN (SELECT spark.wish,
                                                                   spark.toss streak
                                                            FROM (SELECT analyze.gentle meaning,
                                                                         analyze.nature severe,
                                                                         analyze.totally toss,
                                                                         analyze.wish,
                                                                         analyze.click,
                                                                         analyze.handle attractive
                                                                  FROM analyze
                                                                 ) spark
                                                           ) refrigerator
                                            ) different
                                 CROSS JOIN bottom
                             ) violence
                  CROSS JOIN (SELECT active.vulnerable,
                                     active.that,
                                     active.carve,
                                     active.grin excitement,
                                     active.particularly
                              FROM (SELECT acquire.currently assist,
                                           acquire.businessman that,
                                           acquire.particularly,
                                           acquire.satellite share,
                                           acquire.bowl grin,
                                           acquire.carve,
                                           acquire.vulnerable,
                                           acquire.pleasant cabinet
                                    FROM acquire
                                   ) active
                             ) bell
                  CROSS JOIN (SELECT dock.monthly direct,
                                     dock.sun,
                                     dock.quit,
                                     dock.office
                              FROM dock
                             ) fit
                  CROSS JOIN (SELECT partly.state lost,
                                     partly.breath,
                                     partly.conduct,
                                     partly.interview thoroughly,
                                     partly.damn pan,
                                     partly.humanity
                              FROM partly
                             ) Democrat
              ) poet ;
''',
















/*
#49
           Length: 74
     Result Depth: 0
  Renames On Path: 0

->         Length: 70
->   Result Depth: 5
->Renames On Path: 0
*/
  '''
SELECT bus.town
FROM          (SELECT Arab.moment duck,
                      reduce.mixture,
                      Arab.town,
                      reduce.compromise,
                      reduce.privacy whose,
                      reduce.square,
                      reduce.existing
               FROM          (SELECT language.compromise,
                                     language.play good,
                                     language.square,
                                     spin.refugee privacy,
                                     language.existing,
                                     language.prevention,
                                     language.mixture
                              FROM          (SELECT way.compromise,
                                                    way.scratch existing,
                                                    way.flash,
                                                    way.doubt square,
                                                    way.mixture,
                                                    way.starting prevention,
                                                    way.plan play,
                                                    way.best itself
                                             FROM way
                                            ) language
                                 CROSS JOIN (SELECT gas.trigger scenario,
                                                    gas.doorway,
                                                    gas.racism refugee
                                             FROM gas
                                            ) spin
                             ) reduce
                  CROSS JOIN (SELECT innovation.scientific moment,
                                     provoke.town
                              FROM          (SELECT plant.town,
                                                    plant.escape exercise
                                             FROM          communicate
                                                CROSS JOIN (SELECT literally.civic escape,
                                                                   literally.town
                                                            FROM (SELECT preliminary.sacrifice boyfriend,
                                                                         preliminary.feel furthermore,
                                                                         preliminary.weed Jewish,
                                                                         preliminary.scared benefit,
                                                                         preliminary.town,
                                                                         preliminary.culture friendly,
                                                                         preliminary.criteria civic
                                                                  FROM preliminary
                                                                 ) literally
                                                           ) plant
                                            ) provoke
                                 CROSS JOIN (SELECT global.confrontation difficulty
                                             FROM (SELECT robot.ridiculous investor,
                                                          robot.exposure confrontation
                                                   FROM (SELECT spill.last,
                                                                spill.literally exposure,
                                                                spill.base ridiculous,
                                                                spill.beyond confess,
                                                                spill.adult board
                                                         FROM spill
                                                        ) robot
                                                  ) global
                                            ) disability
                                 CROSS JOIN (SELECT steam.onto
                                             FROM (SELECT interview.onto
                                                   FROM interview
                                                  ) steam
                                            ) intimate
                                 CROSS JOIN innovation
                             ) Arab
              ) bus
   CROSS JOIN extreme ;
''',













/*
#50
           Length: 67
     Result Depth: 1
  Renames On Path: 0

->         Length: 70
->   Result Depth: 5
->Renames On Path: 2
*/
  '''
SELECT nurse.cookie
FROM          (SELECT ought.widow,
                      strip.post,
                      strip.lifestyle maintain,
                      strip.delivery,
                      strip.demonstrate associated,
                      strip.ethics,
                      ought.population
               FROM          strip
                  CROSS JOIN ought
              ) license
   CROSS JOIN (SELECT sudden.retire employ,
                      sudden.friendly remove,
                      unusual.citizen experimental,
                      sudden.reminder,
                      poverty.cookie,
                      sudden.journalist boyfriend,
                      sudden.issue,
                      unusual.weakness permit
               FROM          sudden
                  CROSS JOIN unusual
                  CROSS JOIN (SELECT park.late valley,
                                        park.insert,
                                        park.mortgage,
                                        park.addition senator
                                 FROM park
                                ) vast
                     CROSS JOIN (SELECT argument.cookie,
                                        argument.pop restaurant
                                 FROM (SELECT artistic.advise cookie,
                                              artistic.institutional,
                                              artistic.desperate,
                                              artistic.pop
                                       FROM (SELECT lots.according pop,
                                                    lots.straight desperate,
                                                    title.advise,
                                                    lots.page priest,
                                                    lots.institutional
                                             FROM           lots
                                                CROSS JOIN (SELECT eye.bowl,
                                                                      eye.foreign oh,
                                                                      eye.fit advise,
                                                                      eye.mount,
                                                                      eye.challenge
                                                               FROM eye
                                                              ) title
                                            ) artistic
                                      ) argument
                                ) poverty
                     CROSS JOIN (SELECT attendance.heaven,
                                        attendance.challenge,
                                        attendance.either sheer,
                                        attendance.green bright,
                                        attendance.happen disappear,
                                        attendance.less,
                                        attendance.unfair Dutch,
                                        attendance.simple witness
                                 FROM attendance
                                ) combined
                     CROSS JOIN trail
              ) nurse


   CROSS JOIN (SELECT gasoline.sauce besides,
                      gasoline.four type
               FROM gasoline
              ) thigh
   CROSS JOIN (SELECT plastic.grass,
                      plastic.painting,
                      plastic.marriage
               FROM plastic
              ) valuable ;
''',















/*
#51
           Length: 70
     Result Depth: 2
  Renames On Path: 0

->         Length: 70
->   Result Depth: 5
->Renames On Path: 2
*/
  '''
SELECT trust.celebrity
FROM          (SELECT shy.soft describe,
                      shy.positive weather,
                      shy.pain,
                      shy.exercise forgive
               FROM (SELECT price.inspector positive,
                            price.regular girl,
                            price.regain,
                            price.nine pain,
                            price.exercise,
                            price.soft
                     FROM (SELECT there.nine,
                                  there.forth regain,
                                  there.widely cook,
                                  there.exercise,
                                  there.proof inspector,
                                  there.soft,
                                  there.regarding regular
                           FROM there
                          ) price
                    ) shy
              ) hit
   CROSS JOIN (SELECT discrimination.refuse,
                      discrimination.offer fraud,
                      discrimination.responsible establishment
               FROM (SELECT inventory.silent responsible,
                            inventory.complete offer,
                            inventory.tie refuse
                     FROM inventory
                    ) discrimination
              ) birth
   CROSS JOIN (SELECT top.celebrity
               FROM (SELECT colorful.celebrity
                     FROM (SELECT suburb.celebrity
                           FROM (SELECT plastic.decorate celebrity
                                 FROM (SELECT place.elite,
                                              place.throw ha,
                                              place.so approximately,
                                              place.obligation,
                                              place.major housing,
                                              place.precisely,
                                              place.partially decorate,
                                              place.vaccine again
                                       FROM place
                                      ) plastic
                                ) suburb
                          ) colorful
                    ) top
              ) trust
   CROSS JOIN (SELECT invasion.belief,
                      invasion.giant
               FROM (SELECT foot.belief,
                            foot.giant,
                            foot.session flag
                     FROM foot
                    ) invasion
              ) required
   CROSS JOIN (SELECT remember.buyer
               FROM (SELECT member.nominee buyer,
                            member.soap
                     FROM (SELECT compete.availability nominee,
                                  compete.productivity near,
                                  compete.violent Christianity,
                                  compete.criticism government,
                                  compete.leaf soap
                           FROM compete
                          ) member
                    ) remember
              ) exposure
   CROSS JOIN worry ;
''',














/*
#52
           Length: 77
     Result Depth: 1
  Renames On Path: 1

->         Length: 70
->   Result Depth: 5
->Renames On Path: 2
*/
  '''
SELECT encounter.agricultural
FROM          belly
   CROSS JOIN (SELECT   Israeli.bad agricultural,
                        Israeli.at
               FROM (SELECT conservative.at,
                            well.pile bad
                     FROM          (SELECT intellectual.pile,
                                           truly.data,
                                           truly.near initially,
                                           truly.wing gaze
                                    FROM          truly
                                       CROSS JOIN (SELECT coordinator.pile
                                                   FROM (SELECT almost.bonus perspective,
                                                                almost.pile,
                                                                almost.counterpart
                                                         FROM almost
                                                        ) coordinator
                                                  ) intellectual
                                   ) well
                        CROSS JOIN conservative
                    ) Israeli
              ) encounter
   CROSS JOIN send
   CROSS JOIN (SELECT destruction.deliver middle,
                      destruction.gather,
                      destruction.dry handle,
                      destruction.wrist,
                      destruction.weekly
               FROM destruction
              ) statue
   CROSS JOIN (SELECT sight.secret broken
               FROM sight
              ) slice
   CROSS JOIN (SELECT benefit.haul,
                      benefit.uncle bull,
                      benefit.welcome loud,
                      benefit.growth,
                      benefit.fund effectiveness,
                      benefit.pizza
               FROM benefit
              ) vote
   CROSS JOIN ride
   CROSS JOIN (SELECT calculate.blank,
                      calculate.anything skilled,
                      calculate.orientation,
                      calculate.relate,
                      calculate.lamp spill,
                      calculate.spread
               FROM (SELECT scholar.orientation,
                            scholar.lamp,
                            scholar.anything,
                            scholar.remarkable spread,
                            scholar.relate,
                            scholar.frame blank
                     FROM scholar
                    ) calculate
              ) treasure
   CROSS JOIN (SELECT length.skip use,
                      length.vulnerable eye
               FROM (SELECT founder.academic lock,
                            founder.tap,
                            founder.up,
                            founder.skip,
                            founder.stick,
                            founder.station,
                            founder.vulnerable,
                            founder.without evil
                     FROM founder
                    ) length
              ) father
''',















/*
#53
           Length: 72
     Result Depth: 1
  Renames On Path: 2

->         Length: 70
->   Result Depth: 5
->Renames On Path: 2
*/
  '''
SELECT clip.hair
FROM          compelling
   CROSS JOIN leave
   CROSS JOIN (SELECT lack.joy driver,
                      costly.spectrum discover,
                      deal.face hair
               FROM          (SELECT enterprise.spit,
                                     enterprise.reward
                              FROM          metal
                                 CROSS JOIN enterprise
                                 CROSS JOIN (SELECT visit.who,
                                                    visit.minor,
                                                    visit.realm,
                                                    visit.amazing
                                             FROM visit
                                            ) joint
                                 CROSS JOIN (SELECT circle.award note,
                                                    circle.click
                                             FROM (SELECT sneak.army click,
                                                          sneak.English,
                                                          sneak.university collapse,
                                                          sneak.forehead,
                                                          sneak.unity lower,
                                                          sneak.vaccine,
                                                          sneak.appropriate award
                                                   FROM sneak
                                                  ) circle
                                            ) shock
                             ) continent
                  CROSS JOIN (SELECT longtime.struggle,
                                     longtime.implication seal,
                                     longtime.provider,
                                     longtime.reward,
                                     longtime.radar,
                                     longtime.enforce,
                                     medium.face
                              FROM          (SELECT get.reward,
                                                    get.black conclusion,
                                                    get.honor enforce,
                                                    get.him provider,
                                                    get.struggle,
                                                    get.implication,
                                                    get.radar
                                             FROM get
                                            ) longtime
                                 CROSS JOIN (SELECT voter.face
                                             FROM (SELECT naturally.face
                                                   FROM (SELECT highly.crash face
                                                         FROM highly
                                                        ) naturally
                                                  ) voter
                                            ) medium
                             ) deal
                  CROSS JOIN costly
                  CROSS JOIN lack
              ) clip
   CROSS JOIN (SELECT chamber.missionary could,
                      chamber.mobile,
                      chamber.plastic parent,
                      chamber.tone anyone
               FROM (SELECT uniform.bath sanction,
                            uniform.weakness upset,
                            uniform.psychological,
                            uniform.missionary,
                            uniform.array tone,
                            uniform.mobile,
                            uniform.prediction plastic
                     FROM uniform
                    ) chamber
              ) hazard
''',













/*
#54
           Length: 68
     Result Depth: 0
  Renames On Path: 0

->         Length: 70
->   Result Depth: 5
->Renames On Path: 2
*/
  '''
SELECT chill.researcher
FROM          (SELECT severe.French,
                      vary.researcher,
                      severe.survey decrease
               FROM          (SELECT silent.frankly political,
                                     silent.pleased,
                                     silent.partner estate,
                                     silent.researcher,
                                     silent.worldwide balance
                              FROM (SELECT apartment.pleased,
                                           future.agent,
                                           apartment.stop partner,
                                           future.worldwide,
                                           fly.researcher,
                                           fly.frankly
                                    FROM          (SELECT representative.pleased,
                                                          standard.reliable achievement,
                                                          representative.win inside,
                                                          representative.physically,
                                                          standard.gaze stop,
                                                          standard.water,
                                                          representative.fat
                                                   FROM          standard
                                                      CROSS JOIN (SELECT bring.maximum fat,
                                                                         bring.patch pleased,
                                                                         bring.participate,
                                                                         bring.win,
                                                                         bring.easily,
                                                                         bring.physically
                                                                  FROM bring
                                                                 ) representative
                                                  ) apartment
                                       CROSS JOIN (SELECT overall.success trash,
                                                          overall.evolve frankly,
                                                          overall.accomplishment stick,
                                                          western.consultant researcher
                                                   FROM          overall
                                                      CROSS JOIN (SELECT hero.existentialism consultant
                                                                  FROM              (SELECT modern.tension
                                                                                    FROM            (SELECT enforce.tension
                                                                                                     FROM (SELECT disabled.porch diet,
                                                                                                               disabled.shake tension,
                                                                                                               disabled.galaxy,
                                                                                                               disabled.mechanical,
                                                                                                               disabled.jump
                                                                                                            FROM disabled
                                                                                                           ) enforce
                                                                                                    ) modern
                                                                                   ) relevant
                                                                       CROSS JOIN hero
                                                                 ) western
                                                  ) fly
                                       CROSS JOIN future
                                   ) silent
                             ) vary
                  CROSS JOIN implementation
                  CROSS JOIN (SELECT surrounding.dust bucket,
                                     surrounding.lift,
                                     surrounding.home plan,
                                     surrounding.survey,
                                     surrounding.French,
                                     surrounding.shower damn,
                                     surrounding.teenager,
                                     surrounding.emission divorce
                              FROM surrounding
                             ) severe
                  CROSS JOIN relative
                  CROSS JOIN fly
              ) chill
   CROSS JOIN quarterback
''',









/*
#55
           Length: 80
     Result Depth: 0
  Renames On Path: 0

->         Length: 70
->   Result Depth: 5
->Renames On Path: 4
*/
  '''
SELECT treatment.tactic
FROM          (SELECT reader.actor drink,
                      reader.search capable,
                      reader.just division,
                      aggression.credit,
                      aggression.pollution outside
               FROM          aggression
                  CROSS JOIN reader
              ) death
   CROSS JOIN (SELECT okay.destroy,
                      okay.vessel,
                      okay.expectation shut,
                      okay.no,
                      okay.ago computer
               FROM okay
              ) launch
   CROSS JOIN nuclear
   CROSS JOIN (SELECT cook.challenge
               FROM (SELECT flee.mistake,
                            flee.challenge,
                            flee.customer still,
                            flee.remind,
                            flee.effort
                     FROM flee
                    ) cook
              ) member

   CROSS JOIN (SELECT train.afterward grandchild,
                      train.tactic,
                      train.preparation
               FROM (SELECT during.tank tactic,
                            during.classical preparation,
                            during.profile afterward,
                            during.professor
                     FROM (SELECT garage.asleep profile,
                                  stretch.hard tank,
                                  garage.black professor,
                                  garage.core classical
                           FROM            garage
                                CROSS JOIN (SELECT rank.instantly hard
                                            FROM (SELECT replace.sort instantly
                                                  FROM replace
                                                 ) rank
                                           ) stretch
                          ) during
                    ) train
              ) treatment
   CROSS JOIN (SELECT clearly.average wear,
                      clearly.coverage contribution
               FROM clearly
              ) suspend
   CROSS JOIN (SELECT needle.chapter retain,
                      needle.concert figure,
                      needle.city depression,
                      needle.proposal,
                      needle.transform sunny
               FROM (SELECT brake.transform,
                            brake.car chapter,
                            brake.proposal,
                            brake.cloth city,
                            brake.activity concert
                     FROM brake
                    ) needle
              ) minimum
   CROSS JOIN (SELECT maybe.pitch,
                      maybe.bombing afternoon,
                      maybe.write diplomatic,
                      maybe.loop known
               FROM maybe
              ) debut
   CROSS JOIN neat ;
''',














/*
#56
           Length: 77
     Result Depth: 1
  Renames On Path: 1

->         Length: 70
->   Result Depth: 5
->Renames On Path: 4
*/
  '''
SELECT steal.retail
FROM          (SELECT inspection.band retail,
                      regulatory.taste,
                      regulatory.latter protection,
                      regulatory.midnight secure
               FROM          regulatory
                  CROSS JOIN (SELECT onto.band,
                                     onto.collar
                              FROM (SELECT salmon.invisible collar,
                                           salmon.after band
                                    FROM (SELECT century.surprise after,
                                                 withdraw.lady invisible
                                          FROM         (SELECT  highlight.surprise,
                                                                highlight.scientific,
                                                                miracle.cigarette surprise,
                                                                miracle.aware
                                                         FROM (SELECT page.scientific,
                                                                      page.chicken surprise
                                                               FROM          (SELECT reservation.rape,
                                                                                     reservation.chicken,
                                                                                     reservation.tremendous ancestor,
                                                                                     reservation.user,
                                                                                     reservation.profit scientific,
                                                                                     reservation.representative bureau
                                                                              FROM reservation
                                                                             ) page

                                                              ) highlight
                                                              CROSS JOIN miracle
                                                        ) century
                                             CROSS JOIN (SELECT dirty.similarly,
                                                                dirty.platform cabinet,
                                                                dirty.lady,
                                                                dirty.drain
                                                         FROM (SELECT fair.drain,
                                                                      fair.appear,
                                                                      fair.resource lady,
                                                                      fair.marketing similarly,
                                                                      fair.platform
                                                               FROM             fair
                                                                     CROSS JOIN game
                                                              ) dirty
                                                        ) withdraw
                                             CROSS JOIN (SELECT uncertain.virus steel,
                                                                uncertain.future melt,
                                                                uncertain.shrimp,
                                                                uncertain.fare
                                                         FROM (SELECT convey.celebrate virus,
                                                                      convey.level future,
                                                                      convey.popularity shrimp,
                                                                      convey.fare
                                                               FROM convey
                                                              ) uncertain
                                                        ) brand
                                         ) salmon
                                   ) onto
                             ) inspection
                  CROSS JOIN (SELECT earthquake.bug around
                              FROM (SELECT similarly.temporary disagree,
                                           similarly.expense fight,
                                           similarly.bug,
                                           similarly.actress
                                    FROM similarly
                                   ) earthquake
                             ) fighting
              ) steal
   CROSS JOIN (SELECT principal.act,
                      principal.unfair brilliant
               FROM principal
              ) spending
   CROSS JOIN uncertainty
''',















/*
#57
           Length: 66
     Result Depth: 1
  Renames On Path: 1

->         Length: 70
->   Result Depth: 5
->Renames On Path: 4
*/
  '''
SELECT alone.circus
FROM          (SELECT whereas.five,
                      belong.circus,
                      whereas.whatever,
                      belong.legislature
               FROM          (SELECT overlook.period possess,
                                     exclusively.texture engagement,
                                     exclusively.double aide,
                                     sail.secure celebration,
                                     overlook.bolt frame,
                                     exclusively.profession circus,
                                     sail.sharply paper,
                                     overlook.chocolate,
                                     overlook.legislature
                              FROM          sail
                                 CROSS JOIN overlook
                                 CROSS JOIN (SELECT beyond.traditionally profession,
                                                    beyond.example double,
                                                    beyond.bicycle texture
                                             FROM (SELECT animal.example,
                                                          increasingly.bicycle,
                                                          rain.object traditionally,
                                                          animal.objection
                                                   FROM          animal
                                                        CROSS JOIN (SELECT assault.huge,
                                                                          assault.artistic,
                                                                          assault.fool object,
                                                                          assault.trigger,
                                                                          assault.manufacturing shoe,
                                                                          assault.cheese sandwich,
                                                                          assault.theology,
                                                                          assault.future
                                                                   FROM assault
                                                                  ) rain
                                                      CROSS JOIN increasingly
                                                  ) beyond
                                            ) exclusively
                             ) belong
                  CROSS JOIN dance
                  CROSS JOIN (SELECT standing.re ha,
                                     standing.random,
                                     standing.five,
                                     standing.twist pro,
                                     standing.near whatever,
                                     standing.finding,
                                     standing.representation guarantee
                              FROM standing
                             ) whereas

              ) alone
   CROSS JOIN (SELECT consecutive.corn prominent,
                      consecutive.satellite,
                      consecutive.deserve,
                      consecutive.conspiracy exclusively,
                      consecutive.dimension preliminary,
                      consecutive.classify hall,
                      consecutive.seriously,
                      consecutive.glance
               FROM consecutive
              ) expect
   CROSS JOIN refuge
   CROSS JOIN stress
   CROSS JOIN (SELECT sole.museum tropical,
                      sole.neat,
                      sole.upstairs,
                      sole.high,
                      sole.consumer popular,
                      sole.dynamics,
                      sole.solar plan
               FROM sole
              ) hour ;
''',











/*
#58
           Length: 67
     Result Depth: 1
  Renames On Path: 1

->         Length: 70
->   Result Depth: 5
->Renames On Path: 4
*/
  '''
SELECT stack.trinity
FROM          (SELECT Palestinian.hesitate,
                      Palestinian.educate
               FROM          finance
                  CROSS JOIN Palestinian
              ) manufacturer
   CROSS JOIN (SELECT pipe.enough,
                      pipe.jump,
                      bulk.pack trinity,
                      pipe.unlike French,
                      bulk.therapy maybe
               FROM          (SELECT favorite.gentle intact,
                                     apply.tough,
                                     favorite.issue,
                                     proceed.crack pack,
                                     favorite.therapy,
                                     undergraduate.chin,
                                     favorite.over
                              FROM          (SELECT undertake.instrument wrap,
                                                    undertake.convict,
                                                    undertake.tough
                                             FROM (SELECT bury.tough,
                                                          bury.alternative dozen,
                                                          bury.collector clerk,
                                                          bury.convict,
                                                          bury.unprecedented instrument
                                                   FROM bury
                                                  ) undertake
                                            ) apply
                                 CROSS JOIN favorite
                                 CROSS JOIN (SELECT blind.scent,
                                                    blind.essential maximum,
                                                    blind.meanwhile machine,
                                                    blind.discovery minute,
                                                    blind.transaction chin
                                             FROM blind
                                            ) undergraduate
                                 CROSS JOIN (SELECT remind.away crack,
                                                    remind.figure
                                             FROM (SELECT surprised.supposed figure,
                                                          surprised.final away
                                                   FROM (SELECT couple.supposed,
                                                                reader.final
                                                         FROM           (SELECT impulse.line supposed
                                                                           FROM (SELECT option.rental line,
                                                                                        option.athlete help
                                                                                 FROM (SELECT elbow.rely rental,
                                                                                              elbow.athlete
                                                                                       FROM (SELECT diet.rely,
                                                                                                    diet.athlete
                                                                                             FROM diet
                                                                                            ) elbow
                                                                                      ) option
                                                                                ) impulse
                                                                          ) couple
                                                              CROSS JOIN  reader
                                                        ) surprised
                                                  ) remind
                                            ) proceed
                             ) bulk
                  CROSS JOIN (SELECT shade.capability,
                                     shade.spiritual process,
                                     shade.birthday jump,
                                     shade.enough,
                                     shade.shell unlike,
                                     shade.boy daughter,
                                     shade.vacation
                              FROM shade
                             ) pipe
              ) stack
''',













/*
#59
           Length: 68
     Result Depth: 1
  Renames On Path: 1

->         Length: 70
->   Result Depth: 5
->Renames On Path: 4
*/
  '''
SELECT increase.keyboard
FROM          (SELECT entire.addition,
                      brother.balanced seek,
                      threshold.injury breathe,
                      entire.plant
               FROM          excited
                  CROSS JOIN rape
                  CROSS JOIN (SELECT attitude.know,
                                     attitude.link,
                                     attitude.session,
                                     attitude.request injury,
                                     attitude.virtually
                              FROM attitude
                             ) threshold
                  CROSS JOIN brother
                  CROSS JOIN entire
              ) mostly
   CROSS JOIN orange
   CROSS JOIN (SELECT working.will here,
                      opportunity.mouse keyboard,
                      working.class theater,
                      working.characteristic enforce,
                      working.compound come,
                      working.concern
               FROM          working
                  CROSS JOIN (SELECT large.much,
                                     large.fatal it,
                                     large.grandfather commodity
                              FROM (SELECT drug.click fatal,
                                           drug.bomb grandfather,
                                           drug.strip readily,
                                           drug.stage,
                                           drug.much,
                                           drug.dismiss intelligence
                                    FROM drug
                                   ) large
                             ) merit
                  CROSS JOIN (SELECT river.trick mouse
                              FROM (SELECT uncover.minute trick
                                    FROM (SELECT depressed.jaw minute,
                                                 depressed.scary,
                                                 depressed.running
                                          FROM (SELECT sodium.my,
                                                       sodium.flying,
                                                       sodium.craft fool,
                                                       sodium.jaw,
                                                       sodium.fame scary,
                                                       sodium.solve,
                                                       sodium.incorporate,
                                                       sodium.running
                                                FROM sodium
                                               ) depressed
                                         ) uncover
                                   ) river
                             ) opportunity
              ) increase
   CROSS JOIN (SELECT elect.partly sail,
                      elect.painter,
                      elect.duck story,
                      elect.currently,
                      elect.rebuild excited
               FROM (SELECT addition.fly,
                            addition.currently,
                            addition.duck,
                            addition.need rebuild,
                            addition.partly,
                            addition.painter
                     FROM addition
                    ) elect
              ) single
'''
];