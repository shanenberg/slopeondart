import '../projectStuff/projectStuff.dart';
import '../../../../../lib/slot/runtime/objects/SLOTSLOPETreeNode.dart';
import '../../../../../lib/slot/runtime/objects/SLOTTreeNode.dart';

List<int> L = [35, 70];
List<int> D = [1,3,5];
List<int> R = [0,2,4];


int k = 0;
main() {
  initProject();


  List<SLOTSLOPETreeNode> L1Array = [];
  List<SLOTSLOPETreeNode> L2Array = [];



  while(L1Array.length < 30
     || L2Array.length < 30) {


    SLOTSLOPETreeNode node = project.runFunctionUnwrap("generateRandomTemplate");

    int amount = project.runFunctionUnwrap("amountOfStuff", [node]);


    if(amount >= 30 && amount <=40 && L1Array.length < 30) {
      /*print("found L1");
      print(project.runFunctionUnwrap("prettyPrint", [node]));
      print("found L1");
      print("");*/
      L1Array.add(node);
    }
    else if(amount >= 65 && amount <= 80 && L2Array.length < 30) {
      L2Array.add(node);
      /*print("found L2");
      print(project.runFunctionUnwrap("prettyPrint", [node]));
      print("found L2");
      print("");*/
    }
  }






  print("var statements =");
  print("[");
  int i = 0;
  for (int d = 0; d < 3; d++) {
    for (int r = 0; r <= d; r++) {
      for(int t = 0; t < 5; t++) {
        printNode(L1Array[i], 35, D[d], R[r]);
        i++;
        k++;
      }
    }
  }

  i = 0;
  for (int d = 0; d < 3; d++) {
    for (int r = 0; r <= d; r++) {
      for(int t = 0; t < 5; t++) {
        printNode(L2Array[i], 80, D[d], R[r]);
        i++;
        k++;
      }
    }
  }

  print("];");

}



printNode(SLOTSLOPETreeNode node, int length, int depth, int renameCount) {
  print("/*");
  /*int currentLength =
  int currentDepth =
  int currentRenameCount =
  */
  print("#$k");
  project.runFunctionUnwrap("setFromExtras", [node]);
  int currentLength= project.runFunctionUnwrap("amountOfStuff", [node]);
  int currentDepth = project.runFunctionUnwrap("pathDepth", [node]);
  int currentRenameCount = project.runFunctionUnwrap("pathRenameCount", [node]);


  print("           Length: " + currentLength.toString());
  print("     Result Depth: " + currentDepth.toString());
  print("  Renames On Path: " + currentRenameCount.toString());

  print("");

  print("->         Length: " + length.toString());
  print("->   Result Depth: " + depth.toString());
  print("->Renames On Path: " + renameCount.toString());

  print("*/");
  print("'''");
  print(project.runFunctionUnwrap("prettyPrint", [node]));
  print("''',");
}