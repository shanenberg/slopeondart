import '../../../html/examples/SLOTHTMLToConsoleConverter.dart';
import '../../../../lib/slot/runtime/SLOTProject.dart';
import 'sqlVisualization.slp.dart';

main() {
  SLOTProject project =
  new SLOTHTMLToConsoleConverter().constructProject(new sqlVisualization());
  project.initProject();
  var slotResult = project.runFunctionUnwrap("runRandomGeneration");
}
