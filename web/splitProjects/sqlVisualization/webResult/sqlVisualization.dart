import '../../../../lib/slot/runtime/SLOTHTMLProject.dart';
import '../../../html/examples/SLOTProjectConstructor.dart';
import '../../../nativeHtmlStuff/SQLPlayground/overhaul2/overhaul2PreProcess.dart';
import '../../../../lib/slot/runtime/objects/SLOTString.dart';
import 'sqlVisualization.slp.dart';


main() {

  SLOTHTMLProject project =
  new SLOTProjectConstructor().constructHTMLProject(new sqlVisualization());

  var slotResult = project.runProject();
  doSQLHTMLArrowsAndStuff();
}
