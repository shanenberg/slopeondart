/***********************************************************
I am purely generated!!!! Don't touch me, touch the slp files 
***************************************************************/
      import '../../../html/examples/HTMLExample.dart';
class sqlVisualization extends HTMLExample {
   List<String> getExampleProjectStrings() {
  List<String> exampleStrings = new List<String>();
exampleStrings.add('''


SQL

Rules: queryStatement

queryStatement => setOpableQuery ";"?.

setOpableQuery => intersectableQuery
                  queryExceptOrUnion*.

queryExceptOrUnion => queryExcept | queryUnion.

queryExcept => EXCEPT ALL? intersectableQuery.

queryUnion  => UNION ALL? intersectableQuery.

intersectableQuery => bracketableQuery
                      queryIntersect*.

queryIntersect => INTERSECT ALL? bracketableQuery.

bracketableQuery => (bracketQuery | sfwBlock).

bracketQuery => "\\(" setOpableQuery "\\)".

sfwBlock =>  selectclause
             fromclause?
             whereclause?
             groupbyclause?
             havingclause?
             orderbyclause?.

selectclause => SELECT (DISTINCT | ALL)? allOrList.

allOrList => allColumns | selectList.

selectList => selectElement selectListTail*.

selectListTail => "," selectElement.

selectElement => tableAll | selectColumn.

tableAll => identifier "\\." "\\*".
allColumns => "\\*".

selectColumn => orAbleTerm selectColumnRename?.

selectColumnRename => AS? identifier.

fromclause => FROM tablereference
              ("," tablereference )*.

whereclause => WHERE orAbleTerm.

groupbyclause => GROUP BY column collateClause?
                 groupbyclauseTail*.

groupbyclauseTail => "," column collateClause?.

collateClause => COLLATE identifier ("\\." identifier)?.

havingclause => HAVING orAbleTerm.

orderbyclause => ORDER BY column (ASC|DESC)? orderbyclauseTail*.
orderbyclauseTail => "," column (ASC|DESC)?.

orAbleTerm => andAbleTerm orTerm*.
orTerm => OR andAbleTerm.

andAbleTerm => booleanfactor andTerm*.
andTerm => AND booleanfactor.


booleanfactor => ( NOT )? booleantest.

booleantest => booleanprimary booleantestOp?.
booleantestOp => IS (NOT)? truthvalue.

truthvalue => (TRUE | FALSE | UNKNOWN).

booleanprimary => exists | predicate.

exists => EXISTS subquery.

predicate => termexpr ( comparison
                      | between
                      | in
                      | like
                      | nullcheck
                      | match
                      | overlaps
                      | quantifiedcomparison
                      | quantifiedlike)?.

comparison => compop termexpr.
compop => "=" | notequalsoperator | "<" | ">" | "<=" | ">=".
notequalsoperator => "<>" | "!=".

quantifiedcomparison => compop quantifier subquery.

between => NOT? BETWEEN termexpr AND termexpr.

in => NOT? IN ("\\(" setOpableQuery "\\)" | invaluelist).
invaluelist => "\\(" termexpr ("," termexpr)* "\\)".

like => NOT? LIKE termexpr.
nullcheck => IS NOT? NULL.
match => MATCH UNIQUE? (PARTIAL | FULL)? subquery.
overlaps => OVERLAPS termexpr.

quantifiedlike => NOT? LIKE quantifier subquery.

quantifier => some | all.
some => SOME | ANY.
all => ALL.


termexpr => termexpr1 termexprPlusMinusConcat*.
termexprPlusMinusConcat => ("\\+" | "\\-" | "\\|\\|")  termexpr1.

termexpr1 => valueexpr termexpr1MultDivide*.
termexpr1MultDivide => ("\\*" | "/") valueexpr.

valueexpr => sign? (functionspec |                  /* 0 */
                    dateliteral  |                  /* 0 */
                    timeliteral  |                  /* 0 */
                    timestampliteral |              /* 0 */
                    intervalliteral |               /* 0 */
                    identifier stringLiteralQuote | /* 0 */
                    stringLiteralQuote |            /* 0 */
                    NULL |                          /* 0 */
                    TRUE |                          /* 0 */
                    FALSE |                         /* 0 */
                    UNKNOWN |                       /* 0 */
                    column |                        /* 1 */
                    number |                        /* 0 */
                    casespecification |             /* 0 */
                    castspecification |             /* 0 */
                    extractspec      |              /* 0 */
                    "\\(" setOpableQuery "\\)" |      /* 0 */
                    "\\(" orAbleTerm "\\)" |                /* 0 */
                    rowvalueconstructor).           /* 0 */


functionspec => countspec |
                aggrspec |
                customfunctionspec. /* (?)*/


countspec => COUNT "\\(" (countAllTuples | aggrparameter) "\\)".
countAllTuples => "\\*".


aggrspec => (AVG|MAX|MIN|SUM) "\\(" aggrparameter "\\)".
aggrparameter => (DISTINCT|ALL)? termexpr.

customfunctionspec => identifier "\\(" (orAbleTerm ("," orAbleTerm)*)? "\\)".

column => identifier columnTail?.
columnTail => "\\." identifier.

number => UnsignedNumber.

rowvalueconstructor => "\\(" termexpr ("," termexpr)* "\\)".

qualifier => identifier qualifierTail?.
qualifierTail => "\\." identifier.

castspecification => nonBracketCast
                   | bracketCast.
nonBracketCast => CAST termexpr AS qualifier.
bracketCast => CAST "\\(" termexpr AS qualifier "\\)".

dateliteral => DATE stringLiteralQuote.
timeliteral => TIME stringLiteralQuote.
timestampliteral => TIMESTAMP stringLiteralQuote.
intervalliteral => INTERVAL sign? stringLiteralQuote intervalqualifier.
sign => "\\+" | "\\-".


intervalqualifier => startfield TO endfield | singledatetimefield.
startfield => non_seconddatetimefield ("\\(" number "\\)")?.
non_seconddatetimefield => "YEAR" | "MONTH" | "DAY" | "HOUR" | "MINUTE".
endfield => non_seconddatetimefield | "SECOND" ("\\(" number "\\)")?.
singledatetimefield => non_seconddatetimefield ("\\(" number "\\)")?
| "SECOND" ("\\(" unsignedinteger ("," "\\(" unsignedinteger)? "\\)")?.

timeField => "YEAR" | "MONTH" | "DAY" | "HOUR" | "MINUTE" | "SECOND".


extractspec => EXTRACT "\\(" timeField FROM termexpr "\\)" .


/* Direktverschachtelte
   AS nicht möglich?.... */

tablereference => tablereference0 tablereferenceComma*.
tablereferenceComma => "," tablereference0.


tablereference0 => tablereference1 join*.

tablereference0InBrackets => tablereference1
                             join+.

join => crossJoin |
        naturalJoin |
        conditionalJoin.

crossJoin => CROSS JOIN tablereference1.
naturalJoin => NATURAL joinSetSpecification?
                JOIN tablereference1.
conditionalJoin => joinSetSpecification?
                    JOIN tablereference1
                        (onSpecification |
                         usingSpecification).


joinSetSpecification => (LEFT | RIGHT | FULL) OUTER? | INNER.



onSpecification => ON orAbleTerm.
usingSpecification => USING "\\(" identifier
                                 usingAdditional* "\\)".

usingAdditional => "," identifier.

tablereference1 => tableLiteral | tableBracketExpression.

tableLiteral => identifier tableAsRename?.

tableBracketExpression => tableSubquery | tableBracketReference.

tableBracketReference => "\\(" tablereference0InBrackets "\\)" tableAsRename?.

tableSubquery => bracketQuery tableAsRename?.

tableAsRename => AS? identifier tableAsRenameColumns?.

tableAsRenameColumns => "\\(" identifier tableAsRenameColumnAdditional* "\\)".

tableAsRenameColumnAdditional => "," identifier.

subquery => "\\(" setOpableQuery "\\)".

casespecification => CASE orAbleTerm? (WHEN orAbleTerm THEN orAbleTerm)+ (ELSE orAbleTerm)? END.

Scanner:

/*
  dateStringLiteralQuote => "'(0|[1-9][0-9]*)\\-(0|[1-9][0-9]*)\\-(0|[1-9][0-9]*)'".
  timeStringLiteralQuote => "'(0|[1-9][0-9]*):(0|[1-9][0-9]*)(\\.(0|[1-9][0-9]*)?)?((\\-|\\+)(0|[1-9][0-9]*):(0|[1-9][0-9]*))?'".
*/

/* (?)
  dateStringLiteralQuote => "'(0|[1-9][0-9]*)\\-(0|[1-9][0-9]*)\\-(0|[1-9][0-9]*)'".
  timeStringLiteralQuote => "'(0|[1-9][0-9]*):(0|[1-9][0-9]*)(\\.(0|[1-9][0-9]*)?)?:".
*/
  stringLiteralQuote => "'(^')*'" .

  UNION => "(U|u)(N|n)(I|i)(O|o)(N|n)".
  EXCEPT => "(E|e)(X|x)(C|c)(E|e)(P|p)(T|t)".
  INTERSECT => "(I|i)(N|n)(T|t)(E|e)(R|r)(S|s)(E|e)(C|c)(T|t)".
  ALL => "(A|a)(L|l)(L|l)".
  SELECT => "(S|s)(E|e)(L|l)(E|e)(C|c)(T|t)".
  DISTINCT => "(D|d)(I|i)(S|s)(T|t)(I|i)(N|n)(C|c)(T|t)".
  AS => "(A|a)(S|s)".
  OR => "(O|o)(R|r)".
  AND => "(A|a)(N|n)(D|d)".
  NOT => "(N|n)(O|o)(T|t)".
  IS => "(I|i)(S|s)".
  TRUE => "(T|t)(R|r)(U|u)(E|e)".
  FALSE => "(F|f)(A|a)(L|l)(S|s)(E|e)".
  UNKNOWN => "(U|u)(N|n)(K|k)(N|n)(O|o)(W|w)(N|n)".
  BETWEEN => "(B|b)(E|e)(T|t)(W|w)(E|e)(E|e)(N|n)".
  IN => "(I|i)(N|n)".
  LIKE => "(L|l)(I|i)(K|k)(E|e)".
  NULL => "(N|n)(U|u)(L|l)(L|l)".
  MATCH => "(M|m)(A|a)(T|t)(C|c)(H|h)".
  UNIQUE => "(U|u)(N|n)(I|i)(Q|q)(U|u)(E|e)".
  PARTIAL => "(P|p)(A|a)(R|r)(T|t)(I|i)(A|a)(L|l)".
  FULL => "(F|f)(U|u)(L|l)(L|l)".
  OVERLAPS => "(O|o)(V|v)(E|e)(R|r)(L|l)(A|a)(P|p)(S|s)".
  SOME => "(S|s)(O|o)(M|m)(E|e)".
  ANY => "(A|a)(N|n)(Y|y)".
  EXISTS => "(E|e)(X|x)(I|i)(S|s)(T|t)(S|s)".
  CAST => "(C|c)(A|a)(S|s)(T|t)".
  DATE => "(D|d)(A|a)(T|t)(E|e)".
  TIME => "(T|t)(I|i)(M|m)(E|e)".
  TIMESTAMP => "(T|t)(I|i)(M|m)(E|e)(S|s)(T|t)(A|a)(M|m)(P|p)".
  INTERVAL => "(I|i)(N|n)(T|t)(E|e)(R|r)(V|v)(A|a)(L|l)".
  TO => "(T|t)(O|o)".
  COUNT => "(C|c)(O|o)(U|u)(N|n)(T|t)".
  AVG => "(A|a)(V|v)(G|g)".
  MAX => "(M|m)(A|a)(X|x)".
  MIN => "(M|m)(I|i)(N|n)".
  SUM => "(S|s)(U|u)(M|m)".
  FROM => "(F|f)(R|r)(O|o)(M|m)".
  WHERE => "(W|w)(H|h)(E|e)(R|r)(E|e)".
  GROUP => "(G|g)(R|r)(O|o)(U|u)(P|p)".
  BY => "(B|b)(Y|y)".
  HAVING => "(H|h)(A|a)(V|v)(I|i)(N|n)(G|g)".
  ORDER => "(O|o)(R|r)(D|d)(E|e)(R|r)".
  CROSS => "(C|c)(R|r)(O|o)(S|s)(S|s)".
  NATURAL => "(N|n)(A|a)(T|t)(U|u)(R|r)(A|a)(L|l)".
  LEFT => "(L|l)(E|e)(F|f)(T|t)".
  RIGHT => "(R|r)(I|i)(G|g)(H|h)(T|t)".
  OUTER => "(O|o)(U|u)(T|t)(E|e)(R|r)".
  INNER => "(I|i)(N|n)(N|n)(E|e)(R|r)".
  JOIN => "(J|j)(O|o)(I|i)(N|n)".
  ON => "(O|o)(N|n)".
  USING => "(U|u)(S|s)(I|i)(N|n)(G|g)".
  CASE => "(C|c)(A|a)(S|s)(E|e)".
  WHEN => "(W|w)(H|h)(E|e)(N|n)".
  THEN => "(T|t)(H|h)(E|e)(N|n)".
  ELSE => "(E|e)(L|l)(S|s)(E|e)".
  END => "(E|e)(N|n)(D|d)".
  COLLATE => "(C|c)(O|o)(L|l)(L|l)(A|a)(T|t)(E|e)".
  ASC => "(A|a)(S|s)(C|c)".
  DESC => "(D|d)(E|e)(S|s)(C|c)".
  EXTRACT => "(E|e)(X|x)(T|t)(R|r)(A|a)(C|c)(T|t)".

  identifier => "([a-z]|[A-Z]|_|ä|ö|ü|Ä|Ö|Ü|ß)([a-z]|[A-Z]|[0-9]|_|ä|ö|ü|Ä|Ö|Ü|ß)*" BOTTOM.

  UnsignedNumber => "(0|[1-9][0-9]*)(\\.[0-9]+)?((e|E)(\\+|\\-)?[0-9]+)?".

  multilineComment => SEPARATOR "/\\*(^\\*|\\*^/)*\\*/" BOTTOM.
  comment => SEPARATOR "\\-\\-(^\\n)*" TOP.

  separator => SEPARATOR " " BOTTOM.
  newline => SEPARATOR "\\n" BOTTOM.
  newLineWindows => SEPARATOR "\\r" BOTTOM.
  tab => SEPARATOR "\\t" BOTTOM.

Tests:
    T6e "SELECT DISTINCT matrNo FROM

         leistung
         	JOIN
         (SELECT t1.datum AS anfang, t2.datum AS ende
         	FROM semesterbeitrag t1, semesterbeitrag t2
         		WHERE  t2.datum <= ALL (SELECT datum FROM semesterbeitrag AS s WHERE s > t1.datum)
         ) semester
             ON 	datum >= anfang AND datum < ende

         WHERE matrNo IN

         (SELECT matrNo
         FROM
         	(SELECT matrNo, SUM(betrag) AS gezahlt
         	FROM zahlung
         	WHERE datum < semester.ende
         	GROUP BY matrNo) gezahltSummen

         	NATURAL JOIN

         	(SELECT matrNo, SUM(geforderterBetrag) AS gefordert
         	FROM semesterbeitrag
         	WHERE datum < semester.ende
         	GROUP BY matrNo) gefordertSummen
         WHERE gefordert > gezahlt

         UNION

         SELECT matrNo, SUM(geforderterBetrag) AS differenz
         FROM semesterbeitrag
         WHERE matrNo NOT IN (SELECT matrNo FROM semesterbeitrag)
         AND datum < semester.ende
         GROUP BY matrNo)".

    TWTF "SELECT matrNo FROM
          	(SELECT matrNo
          	FROM zahlung)" .

    TCustomFunction "SELECT Alexander(Sadowski)".

    TCustomFunction2 "SELECT HUHAHA(), HAhUHI(gutenTag, 'guten Morgen') FROM abc".

    TAllColumnsBegin "SELECT *, Alex FROM Alexander, Sadowski" FAIL_PARSING.
    TAllColumnsEndFail "SELECT Alex, Alexander, * FROM Sadowski, Sadowski" FAIL_PARSING.
    _ "selEcT ALEXANDER fRoM Sadowski".
    _ "SELECT stefan FROM hanenberg".
    TColumnDot "( SELECT B FROM TEACHES B)".
/*
    TColumnDot12 "( SELECT B FROM TEACHES WHERE YEAR = 1998 )".
    TColumnDot1 "( SELECT B FROM TEACHES B WHERE YEAR = 1998 )".
    TColumnDot2 "( SELECT B.COURSEID FROM TEACHES B WHERE YEAR = 1998 )".
*/
    TComment "SELECT
                -- blabla
                * FROM dings WHERE
                he
                -- lol
                IS NOT NULL".

    TComma "SELECT lal FROM lil, lel, lol, lal".

    TCommaAs "SELECT lal FROM lil, lel, lol lal".

    TComma_Fail "SELECT lal FROm lil, lel, lal," FAIL_PARSING.

    TVL7_1 "SELECT * FROM Auftrag WHERE datum = DATE '1919-12-19'".
    TVL7_1 "SELECT * FROM Auftrag WHERE datum = INTERVAL '1919-12-19' MONTH".

    TVL7 "SELECT * FROM Auftrag WHERE datum IN (DATE '1919-12-19', DATE '1919-12-19' + INTERVAL '1' MONTH)".

    TVL1 "SELECT auftragsNr, datum FROM Auftrag WHERE zuliefererNr=121212".

    TVL2 "SELECT * FROM Auftrag WHERE datum BETWEEN DATE '2001-01-01' AND DATE '2001-12-31'".

    TVL3 "SELECT * FROM Auftrag WHERE datum BETWEEN DATE '2001-12-31' AND DATE '2001-01-01'".

    TVL4 "SELECT * FROM Kunden WHERE rabatt     IN (5.50, 7.50, 10.00)".

    TVL5 "SELECT * FROM Kunden WHERE rabatt NOT IN (5.50, 7.50, 10.00)".

    TVL6 "SELECT * FROM Kunden WHERE NOT rabatt IN (5.50, 7.50, 10.00)".

    TVL7 "SELECT * FROM Auftrag WHERE datum IN (DATE '1919-12-19', DATE '1919-12-19' + INTERVAL '1' MONTH)".

    TVL8 "SELECT * FROM Kunden WHERE kundeName LIKE 'M_ _er'".

    TVL9 "SELECT * FROM Kunden WHERE kundeName LIKE 'M%'".

    TVL10 "SELECT * FROM dingsbums WHERE (TIME '09:00:00', INTERVAL '50' MINUTE) OVERLAPS (TIME '02:15:00', INTERVAL '6' HOUR)".

    TVL11 "SELECT * FROM dingsbums WHERE (TIME '09:00:00', INTERVAL '50' MINUTE) OVERLAPS (TIME '02:15:00', INTERVAL '6' HOUR)".

    TVL12 "SELECT * FROM dingsbums WHERE a OVERLAPS b".

    TVL13_5 "SELECT CAST ShortString AS SMALLINT FROM dingsbums".

    TVL13 "SELECT * FROM dingsbums WHERE CAST ShortString AS SMALLINT = 1".

    TVL13 "SELECT * FROM dingsbums WHERE plz = CAST((SELECT plz FROM Tabelle WHERE ort='Ilmenau') AS SMALLINT)".

    TVL14 "SELECT mitarbeiterNr, mitarbeiterName, mitarbeiterVorname, einstellung FROM Mitarbeiter WHERE einstellung<DATE '1965-01-01'".

    TVL15 "SELECT * FROM Mitarbeiter".

    TVL16 "SELECT Mitarbeiter.*, abteilungsName FROM Mitarbeiter NATURAL JOIN Abteilung".

    TVL17 "SELECT ProduktInformation.*, istBestand * stueckKosten AS nettoWert, istBestand * nettoPreis AS bruttoWert FROM (Produkt NATURAL JOIN ProduktLagertIn NATURAL JOIN ProduktLager) AS ProduktInformation".

    TVL18 "SELECT mitarbeiterName, (CURRENT_DATE - einstellung) AS betriebszugehörigkeit FROM Mitarbeiter".

    TVL19 "SELECT mitarbeiterName, (CURRENT_DATE - einstellung) betriebszugehörigkeit FROM Mitarbeiter".



    TVL20_1 "SELECT mitarbeiterName AS neueMitarbeiter

            FROM Mitarbeiter ".

    TVL20_2 "SELECT a

            FROM Mitarbeiter

            WHERE CURRENT_DATE - einstellung < INTERVAL '24' MONTH".

    TVL20_3_1 "SELECT
                                  CURRENT_DATE - einstellung AS betriebszugehörigkeit
                          FROM Mitarbeiter".

    TVL20_3 "SELECT
                    (CURRENT_DATE - einstellung) AS betriebszugehörigkeit
            FROM Mitarbeiter".



    TVL20_4 "SELECT mitarbeiterName AS neueMitarbeiter,
                    (CURRENT_DATE - einstellung) AS betriebszugehörigkeit

            FROM Mitarbeiter

            WHERE CURRENT_DATE - einstellung < INTERVAL '24' MONTH".



/*
    TVL20 "SELECT mitarbeiterName AS neueMitarbeiter, (CURRENT_ DATE - einstellung) AS betriebszugehörigkeit FROM Mitarbeiter WHERE CURRENT_DATE - einstellung < INTERVAL '24' MONTH".
*/


/* (?)
    TVL21 "SELECT Mitarbeiter.*, (gehalt*13 + 500,00)/(220*8) AS stundenlohn FROM Mitarbeiter".
*/
    TVL21 "SELECT Mitarbeiter.*, (gehalt*13 + 500.00)/(220*8) AS stundenlohn FROM Mitarbeiter".


    TVL22 "SELECT COUNT(mitarbeiterNr) AS anzahlMitarbeiter FROM Mitarbeiter".

    TVL23 "SELECT MAX(gehalt) AS höchstesGehalt FROM Mitarbeiter".

    TVL24 "SELECT MIN(einstellung) AS ersteEinstellung, MAX(einstellung) AS letzteEinstellung FROM Mitarbeiter".

    TVL25 "SELECT SUM(gehalt) AS monatlicheGehaltsBelastung FROM Mitarbeiter".

    TVL26 "SELECT (AVG(nettoPreis)-AVG(stueckKosten))/AVG(nettoPreis)*100 AS gewinnMarge FROM Produkt".

    TVL27 "SELECT ((SUM(nettoPreis)-SUM(stueckKosten))/SUM(nettoPreis)*100) AS gewinnMarge FROM Produkt".

    TVL28 "SELECT AVG(rabatt) AS rabatt FROM Kunden".

    TVL29 "SELECT AVG(DISTINCT rabatt) AS rabatt FROM Kunden".

    TVL30 "SELECT COUNT(*) AS gesamtzahlKunden, COUNT(rabatt) AS gesamtzahlKundenMitRabatt, COUNT(DISTINCT ort) AS anzahlWohnorte FROM Kunden".

    TVL31 "SELECT mitarbeiterName, mitarbeiterVorname, gehalt FROM Mitarbeiter".

    TVL32 "SELECT auftragsNr, mitarbeiterName, mitarbeiterVorname FROM Auftrag, Mitarbeiter WHERE bearbeiterNr=mitarbeiterNr".

    TVL33 "SELECT auftragsNr, mitarbeiterName, mitarbeiterVorname FROM Auftrag CROSS JOIN Mitarbeiter WHERE bearbeiterNr=mitarbeiterNr".

    TVL34 "SELECT zuliefererName, teilNr, teilBez FROM Teil CROSS JOIN Zulieferer".

    TVL35 "SELECT mitarbeiterName, mitarbeiterVorname, abteilungsName FROM Mitarbeiter CROSS JOIN Abteilung WHERE Mitarbeiter.abteilungsNr=Abteilung.abteilungsNr".

    TVL36 "SELECT DISTINCT produktBez, abteilungsName FROM Produkt CROSS JOIN ArbeitetAn CROSS JOIN Abteilung WHERE Produkt.produktNr=ArbeitetAn.produktNr AND ArbeitetAn.abteilungsNr=Abteilung.abteilungsNr AND Produkt.produktTyp='Waschmaschine'".

    TVL37 "SELECT DISTINCT produktBez, abteilungsName FROM Produkt NATURAL JOIN ArbeitetAn NATURAL JOIN Abteilung WHERE Produkt.produktTyp='Waschmaschine'".

    TVL38 "SELECT Produkt.*, Abteilung.* FROM Produkt, ArbeitetAn, Abteilung WHERE Produkt.produktNr=ArbeitetAn.produktNr AND ArbeitetAn.abteilungsNr=Abteilung.abteilungsNr".

    TVL39 "SELECT P.*, A.* FROM Produkt P, ArbeitetAn AA, Abteilung A WHERE P.produktNr=AA.produktNr AND AA.abteilungsNr=A.abteilungsNr".

    TVL40 "SELECT * FROM Teil T, TeileLagertIn TLI, TeileLager TL
    WHERE T.teilNr=TLI.teilNr AND TLI.teileLagerNr=TL.teileLagerNr AND TL.teileLagerBez='Hochstapel'".

    TVL41 "SELECT T.teilNr, T.teilBez FROM Teil T, TeileLagertIn TLI, TeileLager TL WHERE T.teilNr=TLI.teilNr AND TLI.teileLagerNr=TL.teileLagerNr AND TL.teileLagerBez='Hochstapel'".

    TVL42 "SELECT Zulieferer.*, Teil.* FROM Zulieferer INNER JOIN Liefert USING (zuliefererNr) INNER JOIN Teil USING (teilNr)".

    TVL43 "SELECT Mitarbeiter.* FROM Mitarbeiter INNER JOIN Kunden ON (mitarbeiterNr=kundenNr)".

    TVL44 "SELECT * FROM Mitarbeiter AS M INNER JOIN Auftrag AS A ON M.mitarbeiterNr=A.mitarbeiterNr".

    TVL45 "SELECT * FROM Mitarbeiter INNER JOIN Auftrag USING (mitarbeiterNr)".

    TVL46 "SELECT M.* FROM Mitarbeiter AS M INNER JOIN Auftrag AS A ON (M.mitarbeiterNr=A.mitarbeiterNr AND A.datum=M.einstellung)".

    TVL47 "SELECT Z.* FROM Zulieferer AS Z INNER JOIN Auftrag AS A ON (Z.zuliefererNr=A.zuliefererNr) WHERE datum<DATE '2000-01-01'".

    TVL48 "SELECT Z.* FROM Zulieferer AS Z INNER JOIN Auftrag USING (zuliefererNr) WHERE datum<DATE '2000-01-01'".

    TVL49 "SELECT Z.* FROM Zulieferer AS Z NATURAL JOIN Auftrag WHERE datum<DATE '2000-01-01'".

    TVL50 "SELECT R.*, produktBez, stueckKosten, (betrag/menge) AS verkaufsPreis FROM Produkt AS P INNER JOIN Rechnungsposition AS R ON (P.produktNr=R.produktNr AND stueckKosten>(betrag/menge))".

    TVL51 "SELECT R.*, produktBez, stueckKosten, (betrag/menge) AS verkaufsPreis FROM Produkt".

    TVL52 "SELECT R.*, produktBez, stueckKosten, (betrag/menge) AS verkaufsPreis FROM Produkt AS P INNER JOIN Rechnungsposition AS R ON (stueckKosten>(betrag/menge)) WHERE P.produktNr=R.produktNr".

    TVL53 "SELECT Rechnungsposition.*, produktBez, stueckKosten, (betrag/menge) AS verkaufsPreis FROM Produkt NATURAL JOIN Rechnungsposition WHERE stueckKosten>(betrag/menge)".

    TVL54 "SELECT * FROM T1 LEFT OUTER JOIN T2 ON (a=d)".

    TVL55 "SELECT * FROM T1 INNER JOIN T2 ON (a=d)".

    TVL56 "SELECT DISTINCT T1.*, NULL AS d, NULL AS e, NULL
           AS f FROM T1 CROSS JOIN T2 WHERE a!=d".

    TVL57 "SELECT * FROM Produkt NATURAL LEFT OUTER JOIN ArbeitetAn NATURAL LEFT OUTER JOIN Abteilung".

    TVL58 "SELECT * FROM Produkt LEFT OUTER JOIN ArbeitetAn USING (produktNr) LEFT OUTER JOIN Abteilung USING (abteilungsNr)".

    TVL59 "SELECT * FROM Produkt LEFT OUTER JOIN ArbeitetAn ON (Produkt.produktNr=ArbeitetAn.produktNr) LEFT OUTER JOIN Abteilung ArbeitetAn ON (ArbeitetAn.abteilungsNr= Abteilung.abteilungsNr)".

    TVL60 "SELECT SUM(menge) FROM Zulieferer INNER JOIN Rechnung USING (zuliefererNr) INNER JOIN RechnungsPosition USING (rechnungsNr) WHERE zuliefererName='PasstNix GmbH' AND datum=DATE '2000-02-01'".

    TVL61 "SELECT Teil.* FROM Teil INNER JOIN SindBestandteilVon USING (teilNr) INNER JOIN Produkt USING (produktNr) WHERE produktBez='WM53'".

    TVL62 "SELECT Teil.* FROM Teil NATURAL JOIN SindBestandteilVon NATURAL JOIN Produkt WHERE produktBez='WM53'".

    TVL63 "SELECT teilNr, teilBez, teileLagerNr, teileLagerBez, zuliefererNr, zuliefererName FROM Zulieferer NATURAL JOIN Liefert NATURAL JOIN Teil NATURAL JOIN TeilLagertIn NATURAL JOIN TeileLager WHERE istBestand<50".

    TVL64 "SELECT * FROM VerheiratetePers INNER JOIN VerheiratetePers ON ehepartner=persNr".

    TVL65 "SELECT * FROM VerheiratetePers AS P1 INNER JOIN VerheiratetePers AS P2 ON (P1.ehepartner=P2.persNr)".

    TVL66 "SELECT * FROM Mitarbeiter WHERE mitarbeiterVorname=mitarbeiterName".

    TVL67 "SELECT produktNr FROM ProduktLagertIn WHERE istBestand<100".

    TVL67 "SELECT produktNr FROM ProduktLagertIn WHERE (a-e) <= (b*c+d)".

    TVL68 "SELECT * FROM Teil WHERE istBestand NOT BETWEEN 10 AND 1000".

    TVL69 "SELECT mitarbeiterName, mitarbeiterVorname FROM Mitarbeiter WHERE CURRENT_DATE-einstellung >= INTERVAL '25' YEAR AND CURRENT_DATE-einstellung < INTERVAL '26' YEAR".

    TVL70_1_FAIL "SELECT hi
            FROM ding INNER JOIN bing" FAIL_PARSING.

    TVL70_FAIL "SELECT M1.mitarbeiterNr, M1.mitarbeiterName, M1.mitarbeiterVorname, M2.mitarbeiterNr, M2.mitarbeiterName, M2.mitarbeiterVorname
                FROM Mitarbeiter AS M1 INNER JOIN Mitarbeiter AS M2
                WHERE (M1.mitarbeiterName=M2.mitarbeiterName) AND (M1.mitarbeiterNr!=M2.mitarbeiterNr)" FAIL_PARSING.

    TVL71 "SELECT M1.mitarbeiterNr, M1.mitarbeiterName, M1.mitarbeiterVorname, M2.mitarbeiterNr, M2.mitarbeiterName, M2.mitarbeiterVorname FROM Mitarbeiter AS M1 INNER JOIN Mitarbeiter AS M2 ON (M1.mitarbeiterName=M2.mitarbeiterName) AND (M1.mitarbeiterNr>M2.mitarbeiterNr)".

    TVL72 "SELECT Rechnung.* FROM Rechnung NATURAL JOIN RechnungsPosition NATURAL JOIN AuftragsPosition NATURAL JOIN Zulieferer WHERE zuliefererName='VersprechViel GmbH' AND datum BETWEEN DATE '2001-01-01' AND DATE '2001-12-31' AND teilNr=123321".

    TVL73 "SELECT * FROM Mitarbeiter WHERE abteilungsNr IS NULL".

    TVL74 "SELECT * FROM Zulieferer WHERE (ansprechPartner, ansprechpartTelNr) IS NULL".

    TVL75 "SELECT hi FROM bla WHERE a IS TRUE".

    TVL76 "SELECT hi FROM bla WHERE a IS NOT TRUE".

    TVL77 "SELECT hi FROM bla WHERE a IS FALSE".

    TVL78 "SELECT hi FROM bla WHERE a IS NOT FALSE".

    TVL79 "SELECT hi FROM bla WHERE a IS UNKNOWN".

    TVL80 "SELECT hi FROM bla WHERE a IS NOT UNKNOWN".

    TVL81 "SELECT produktLagerBez AS lager, COUNT(produktNr) AS produkte FROM ProduktLagertIn GROUP BY produktLagerBez".

    TVL82 "SELECT produktLagerBez AS lager, produktNr FROM ProduktLagertIn ORDER BY produktLagerBez".

    TVL83 "SELECT A.abteilungsName, M.mitarbeiterName, M.mitarbeiterVorname FROM Mitarbeiter AS M NATURAL JOIN Abteilung AS A GROUP BY A.abteilungsName".

    TVL84_09GROUPBYLOl "SELECT A.abteilungsName, M.mitarbeiterName, M.mitarbeiterVorname FROM Mitarbeiter AS M NATURAL JOIN Abteilung AS A ORDER BY A.abteilungsName, M.mitarbeiterName, M.mitarbeiterVorname".

    TVL85 "SELECT A.abteilungsName, M.mitarbeiterName, M.mitarbeiterVorname FROM Mitarbeiter AS M NATURAL JOIN Abteilung AS A ORDER BY A.abteilungsName, M.mitarbeiterName, M.mitarbeiterVorname".

    TVL86 "SELECT produktTyp, COUNT(kundenNr) AS kundenProProdukttyp FROM Kunden NATURAL JOIN Kauft NATURAL JOIN Produkt GROUP BY produktTyp".

    TVL87 "SELECT produktLagerBez AS Lager, COUNT(produktNr) AS produkte FROM ProduktLagertIn GROUP BY produktLagerBez HAVING produktLagerBez='München' OR produktLagerBez='Berlin'".

    TVL88 "SELECT produktLagerBez AS lager, COUNT(produktNr) AS produkte FROM ProduktLagertIn GROUP BY produktLagerBez HAVING istBestand<500".

    TVL89 "SELECT produktLagerBez AS lager, COUNT(produktNr) AS produkte FROM ProduktLagertIn WHERE istBestand<500 GROUP BY produktLagerBez".

    TVL90 "SELECT produktLagerBez AS lager, COUNT(produktNr) AS produkte FROM ProduktLagertIn GROUP BY produktLagerNr HAVING produktLagerBez='München' OR produktLagerBez='Berlin'".

    TVL91 "SELECT produktLagerBez AS lager, COUNT(produktNr) AS produkte, SUM(istBestand) AS gesamtMenge FROM ProduktLagertIn NATURAL JOIN ProduktLager GROUP BY produktLagerBez HAVING COUNT(produktNr)>300 AND SUM(istBestand)<10000".

    TVL92 "SELECT produktTyp, AVG(nettoPreis) AS nettoPreis FROM Produkt WHERE produktTyp NOT IN ('Geschirrspüler', 'Waschmaschine') GROUP BY produktTyp".

    TVL93 "SELECT produktTyp, AVG(nettoPreis) AS nettoPreis FROM Produkt GROUP BY produktTyp HAVING produktTyp NOT IN ('Geschirrspüler', 'Waschmaschine')".

    TVL94 "SELECT produktTyp, AVG(nettoPreis) AS nettoPreis FROM Produkt GROUP BY produktTyp HAVING AVG(nettoPreis)>500".

    TVL95 "SELECT COUNT(*) AS anzahlUmfangreicherRechnungen FROM RechnungsPosition GROUP BY rechnungsNr HAVING COUNT(rechnungsPos)>10".

    TVL96 "SELECT produktTyp, produktLagerNr, produktNr FROM ProduktLagertIn NATURAL JOIN Produkt WHERE istBestand>0 GROUP BY produktTyp, produktLagerNr".

    TVL97 "SELECT produktTyp, produktLagerNr FROM ProduktLagertIn NATURAL JOIN Produkt WHERE istBestand>0 GROUP BY produktTyp, produktLagerNr".

    TVL98 "SELECT produktTyp, produktLagerNr, AVG(nettoPreis) AS nettoPreis FROM ProduktLagertIn NATURAL JOIN Produkt WHERE istBestand>0 GROUP BY produktTyp, produktLagerNr".


  TVL99 "SELECT produktTyp, AVG(nettoPreis) AS nettoPreisÜberAlleLager
    FROM (SELECT produktTyp, produktLagerNr, AVG(nettoPreis) AS nettoPreis
            FROM ProduktLagertIn NATURAL JOIN Produkt
            WHERE istBestand>0 GROUP BY produktTyp, produktLagerNr)
    GROUP BY produktTyp".

    TVL100 "SELECT * FROM Mitarbeiter WHERE gehalt = (SELECT MAX(gehalt) FROM Mitarbeiter)".

    TVL101_FAIL "SELECT * FROM Mitarbeiter WHERE ort IN (SELECT ort FROM Mitarbeiter WHERE gehalt= (SELECT MIN(gehalt) FROM Mitarbeiter)" FAIL_PARSING.
    TVL101 "SELECT * FROM Mitarbeiter WHERE ort IN (SELECT ort FROM Mitarbeiter WHERE gehalt= (SELECT MIN(gehalt) FROM Mitarbeiter))".

    TVL102 "SELECT * FROM Mitarbeiter AS MA1 WHERE MA1.gehalt > (SELECT MIN(gehalt)*1,5 FROM Mitarbeiter AS MA2 WHERE MA1.abteilungsNr=MA2.abteilungsNr)".

    TVL103 "SELECT abteilungsNr, AVG(gehalt) FROM Mitarbeiter AS M1 WHERE gehalt >(SELECT AVG(gehalt) FROM Mitarbeiter AS M2 WHERE M1.abteilungsNr=M2.abteilungsNr) GROUP BY abteilungsNr".

    TVL104 "SELECT COUNT(produktLagerNr) FROM ProduktLagertIn GROUP BY produktLagerNr HAVING COUNT (produktNr)>(SELECT COUNT(*) FROM Produkt)/2".

    TVL105 "SELECT COUNT(produktLagerNr) FROM ProduktLagertIn NATURAL JOIN Produkt WHERE stueckKosten>500 GROUP BY produktLagerNr HAVING COUNT (produktNr)>(SELECT COUNT(*) FROM Produkt WHERE stueckKosten>500)/2".

    TVL106 "SELECT Mitarbeiter.*, gehalt - (SELECT AVG(gehalt) FROM Mitarbeiter) AS unterschiedZumDurchschnittsgehalt FROM Mitarbeiter".

    TVL107 "SELECT P.produktBez, P.produktNr, (SELECT SUM(istBestand) FROM ProduktLagertIn AS PLI GROUP BY produktNr HAVING P.produktNr=PLI.produktNr) AS lagerBestand FROM Produkt AS P".

    TVL108 "SELECT P.produktBez, P.produktNr, lagerBestand FROM Produkt NATURAL JOIN (SELECT produktNr, SUM(istBestand) AS lagerBestand FROM ProduktLagertIn GROUP BY produktNr)".

    TVL109 "SELECT produktBez, produktNr, SUM(istBestand) AS lagerBestand FROM Produkt NATURAL JOIN ProduktLagertIn GROUP BY produktNr".

    TVL110 "SELECT AVG(nettoPreis) AS nettoPreis FROM Produkt NATURAL JOIN (SELECT DISTINCT produktNr FROM ProduktLagertIn WHERE istBestand>0)".

    TVL111 "SELECT *
            FROM Abteilung
            WHERE budget> (SELECT budget
            FROM Abteilung
            WHERE abteilungsNr=121212)".

    TVL112 "SELECT *
            FROM Abteilung
            WHERE (budget, mitarbeiterZahl)=
            (SELECT budget, mitarbeiterZahl
            FROM Abteilung
            WHERE abteilungsNr=121212)".



    TVL113 "SELECT *
            FROM Abteilung
            WHERE budget= (SELECT budget
            FROM Abteilung
            WHERE abteilungsNr=121212
            AND
            mitarbeiterZahl=(SELECT mitarbeiterZahl
            FROM Abteilung
            WHERE abteilungsNr=121212))".

    TVL113_FAIL "SELECT *
            FROM Abteilung
            WHERE budget= (SELECT budget
            FROM Abteilung
            WHERE abteilungsNr=121212;
            AND
            mitarbeiterZahl=(SELECT mitarbeiterZahl
            FROM Abteilung
            WHERE abteilungsNr=121212" FAIL_PARSING.
    TVL114 "SELECT *
            FROM Rechnung
            WHERE zuliefererNr IN (SELECT zuliefererNr
            FROM Zulieferer
            WHERE ort='GroßKleckersdorf')".

    TVL115 "SELECT Rechnung.*
             FROM Rechnung NATURAL JOIN Zulieferer
              WHERE (zuliefererName, datum) IN
                       (('Abzock', CURRENT_DATE-INTERVAL '1' DAY),
                       ('Abzock', CURRENT_DATE-INTERVAL '2' DAY),
                       ('Chaotikus', CURRENT_DATE-INTERVAL '1' DAY),
                       ('Chaotikus', CURRENT_DATE-INTERVAL '2' DAY))".

    TVL115_FAIL "SELECT Rechnung.*
            FROM Rechnung NATURAL JOIN Zulieferer
            WHERE (zuliefererName, datum) IN
            (('Abzock', CURRENT_DATE-INTERVALL '1' DAY),
            ('Abzock', CURRENT_DATE-INTERVALL '2' DAY),
            ('Chaotikus', CURRENT_DATE-INTERVALL '1' DAY),
            ('Chaotikus', CURRENT_DATE-INTERVALL '2' DAY))" FAIL_PARSING.

    TVL116 "SELECT Rechnung.*
            FROM Rechnung NATURAL JOIN Zulieferer
            WHERE datum IN (CURRENT_DATE-INTERVAL '1' DAY,
            CURRENT_DATE-INTERVAL '2' DAY) AND
            zuliefererName IN ('Abzock', 'Chaotikus')".

    TVL116_FAIL "SELECT Rechnung.*
                FROM Rechnung NATURAL JOIN Zulieferer
                WHERE datum IN (CURRENT_DATE-INTERVALL '1' DAY,
                CURRENT_DATE-INTERVALL '2' DAY) AND
                zuliefererName IN ('Abzock', 'Chaotikus')" FAIL_PARSING.

    TVL117 "SELECT mitarbeiterName, mitarbeiterVorname
            FROM Mitarbeiter
            WHERE (mitarbeiterName, mitarbeiterVorname) IN
            (SELECT kundenName, kundenVorname
            FROM Kunden
            WHERE rabatt<10 AND
            kundenVorname LIKE 'G%')".

    TVL118 "SELECT mitarbeiterName, mitarbeiterVorname
            FROM Mitarbeiter
            WHERE mitarbeiterVorname LIKE 'G%' AND
            mitarbeiterNr IN
            (SELECT kundenNr
            FROM Kunden
            WHERE rabatt<10)".

    TVL119 "SELECT mitarbeiterName, mitarbeiterVorname
            FROM Mitarbeiter
            WHERE (mitarbeiterName, mitarbeiterVorname)=ANY
            (SELECT kundenName, kundenVorname
            FROM Kunden
            WHERE rabatt<10 AND
            kundenVorname LIKE 'G%')".

    TVL120 "SELECT mitarbeiterName, mitarbeiterVorname
            FROM Mitarbeiter
            WHERE mitarbeiterVorname LIKE 'G%' AND
            mitarbeiterNr IN
            (SELECT kundenNr
            FROM Kunden
            WHERE rabatt<10)".

    TVL121 "SELECT mitarbeiterName, mitarbeiterVorname
            FROM Mitarbeiter
            WHERE (mitarbeiterName, mitarbeiterVorname)=ANY
            (SELECT kundenName, kundenVorname
            FROM Kunden
            WHERE rabatt<10 AND
            kundenVorname LIKE 'G%')".


    TVL122 "SELECT mitarbeiterName, mitarbeiterVorname
            FROM Mitarbeiter
            WHERE mitarbeiterNr IN
            (SELECT bearbeiterNr
            FROM Auftrag
            WHERE datum BETWEEN DATE '2000-01-01'
            AND DATE '2000-12-31')".


    TVL123 "SELECT mitarbeiterName, mitarbeiterVorname
            FROM Mitarbeiter
            WHERE EXISTS
            (SELECT *
            FROM Auftrag
            WHERE datum BETWEEN DATE '2000-01-01'
            AND DATE '2000-12-31'
            AND mitarbeiterNr=bearbeiterNr)".

    TVL124 "SELECT produktTyp, AVG(nettoPreis)
            AS nettoPreisÜberAlleLager
            FROM (SELECT produktTyp, produktLagerNr,
            AVG(nettoPreis) AS nettoPreis
            FROM ProduktLagertIn NATURAL JOIN Produkt
            WHERE lagerbestand>0
            GROUP BY produktTyp, produktLagerNr)
            GROUP BY produktTyp".


    TVL125_FAIL "
            (SELECT produktTyp, produktLagerNr, AVG(nettoPreis)
            AS nettoPreis
            FROM ProduktLagertIn NATURAL JOIN Produkt
            WHERE lagerbestand>0
            GROUP BY produktTyp, produktLagerNr) AS
            ProdukttypLager" FAIL_PARSING.

      TVL125 "SELECT * FROM (SELECT produktTyp, produktLagerNr, AVG(nettoPreis) AS nettoPreis
                FROM ProduktLagertIn NATURAL JOIN Produkt
                WHERE lagerbestand>0
                GROUP BY produktTyp, produktLagerNr) AS
                ProdukttypLager".


    TVL126 "SELECT produktTyp, AVG(nettoPreis) AS nettoPreisÜberAlleLager
            FROM   ProdukttypLager GROUP BY produktTyp".

    TVL127 "SELECT *
            FROM Kunden AS K1
            WHERE rabatt > (SELECT AVG(rabatt)
            FROM Kunden AS K2
            WHERE K1
            .ort=K2
            .ort AND
            K1
            .kundenNr <> K2.kundenNr)".

    TVL128 "SELECT kundenName AS name, plz, ort, strasse, hausNr
            FROM Kunden
            UNION
            SELECT zuliefererName AS name, plz, ort, strasse,
            hausNr
            FROM Zulieferer
            UNION
            SELECT mitarbeiterName AS name, plz, ort, strasse, hausNr
            FROM Mitarbeiter".


    TVL129 "SELECT kundenNr AS identifikationsNr, kundenName AS
            name, kundenVorname AS vorname
            FROM Kunden
            INTERSECT
            SELECT mitarbeiterNr AS identifikationsNr, mitarbeiterName
            AS name, mitarbeiterVorname AS vorname
            FROM Mitarbeiter".

    TVL130 "SELECT kundenNr, kundenName, kundenVorname
            FROM Kunden
            WHERE EXISTS
            (SELECT *
            FROM Mitarbeiter
            WHERE kundenNr=mitarbeiterNr)".

    TVL131 "SELECT kundenNr, kundenName, kundenVorname
            FROM Kunden
            WHERE kundenNr = SOME
            (SELECT kundenNr AS identifikationsNr
            FROM Kunden
            INTERSECT
            SELECT mitarbeiterNr AS identifikationsNr
            FROM Mitarbeiter)".

    TVL132 "SELECT kundenNr, kundenName, kundenVorname
            FROM Kunden
            WHERE kundenNr= SOME
            (SELECT mitarbeiterNr
            FROM Mitarbeiter)".

    TVL133 "SELECT kundenNr AS identifikationsNr, kundenName AS
                   name, kundenVorname AS vorname
            FROM Kunden
            EXCEPT
            SELECT mitarbeiterNr AS identifikationsNr, mitarbeiterName
            AS name, mitarbeiterVorname AS vorname
            FROM Mitarbeiter".

    TVL134 "SELECT kundenNr, kundenName, kundenVorname
            FROM Kunden
            WHERE NOT EXISTS (SELECT *
            FROM Mitarbeiter
            WHERE kundenNr=mitarbeiterNr)".

    TVL135 "SELECT kundenNr, kundenName, kundenVorname
            FROM Kunden
            WHERE kundenNr = SOME (SELECT kundenNr AS
            identifikationsNr
            FROM Kunden
            EXCEPT
            SELECT mitarbeiterNr AS
            identifikationsNr
            FROM Mitarbeiter)".

    TVL136 "SELECT kundenNr, kundenName, kundenVorname
            FROM Kunden
            WHERE kundenNr != ALL (SELECT mitarbeiterNr
            FROM Mitarbeiter)".

    TVL137 "SELECT mitarbeiterName, mitarbeiterVorname
            FROM Mitarbeiter
            ORDER BY mitarbeiterName, mitarbeiterVorname".

    TVL138 "SELECT datum, bearbeiterNr, auftragsNr,
            auftragsPos, menge
            FROM Auftrag NATURAL JOIN Auftragsposition
            WHERE datum BETWEEN DATE '2000-01-01' AND
            DATE '2000-12-31'
            ORDER BY datum, bearbeiterNr".



    TVL_FINAL_1 "SELECT produktTyp, produktLagerBez, AVG(nettoPreis) AS nettoPreis,
                 COUNT(produktNr) AS anzahlUnterschiedlicherProdukte

                 FROM Produkt NATURAL JOIN ProduktLagertIn NATURAL JOIN ProduktLager
                 ".

    TVL_FINAL_2_3 "SELECT *
                        FROM A
                        WHERE produktTyp='Geschirrspüler'
                         AND produktBez LIKE 'GS%'
                         AND produktBez LIKE 'WM%' ".

    TVL_FINAL_2_2 "SELECT *
                    FROM A
                    WHERE produktTyp='Geschirrspüler'
                     AND produktBez LIKE 'GS%'
            OR produktTyp='Waschmaschine'
                     AND produktBez LIKE 'WM%' ".



    TVL_FINAL_2_1 "SELECT *
                  FROM A
                  WHERE (produktTyp='Geschirrspüler' AND produktBez LIKE 'GS%' OR produktTyp='Waschmaschine'
                                     AND produktBez LIKE 'WM%')".

    TVL_FINAL_2 "SELECT *
                  FROM A
                  WHERE (produktTyp='Geschirrspüler' AND produktBez LIKE 'GS%' OR produktTyp='Waschmaschine'
                                     AND produktBez LIKE 'WM%') AND
                                     produktLagerBez NOT IN
                                     ('Essen', 'München')
                     ".


    TVL_FINALBOSS "SELECT produktTyp, produktLagerBez, AVG(nettoPreis) AS nettoPreis,
                          COUNT(produktNr) AS anzahlUnterschiedlicherProdukte

                   FROM Produkt NATURAL JOIN ProduktLagertIn NATURAL JOIN ProduktLager

                   WHERE (produktTyp='Geschirrspüler' AND produktBez LIKE 'GS%' OR produktTyp='Waschmaschine'
                   AND produktBez LIKE 'WM%') AND
                   pro - duktLagerBez NOT IN
                   ('Essen', 'München')
                   GROUP BY produktTyp, produktLagerBez
                   HAVING COUNT(produktNr) BETWEEN 5 AND 20
                   ORDER BY nettoPreis, produktTyp DESC".




    TDings "SELECT " FAIL_PARSING.


    TComment "SELECT /* asldkjfs///dklfj */ lol FROM ding".
    T0 "SELECT 1dfk  FROM a" . /* (?) <--- Syntaktisch korrekte Kacke! :D */


    T123 "SELECT dings FROM ((SELECT dings FROM dangs))".

    T123 "SELECT dings FROM ((SELECT dings FROM dangs) AS da)" FAIL_PARSING.




    T999 "SELECT lol FROM ((SELECT hi FROM lal))".
    T55 "SELECT hi FROM ding".

    TJOIN_USING "SELECT hi FROM ding JOIN bing USING(a,b,c)".

    TWHERE_1 "SELECT hi FROM ding WHERE a = 1".

    TWHERE_LIKE "SELECT hi FROM ding WHERE a LIKE 'dings'".

    TWHERE_IN "SELECT hi FROM ding WHERE lol IN ( (SELECT hi FROM dings),(SELECT ding FROM Dingding) )".
    TWHERE_IN2 "SELECT hi FROM ding WHERE a IN (SELECT hi FROM (lol NATURAL JOIN beb) JOIN ding ON true WHERE lol = 1)".

    TWHERE_QUANT_ALL "SELECT hi FROM ding WHERE lol = ALL(SELECT dingsbums FROM lolol)". /* (?) ALL mit eigener Liste möglich? */
    TWHERE_QUANT_LIKE_ANY "SELECT lol FROM lala WHERE lol LIKE ANY (SELECT hi FROM (ding JOIN LAL ON (true-true)) WHERE (xD - (2 + 1) = 100) AND 1 = 1)".

    TWHERE_EXISTS "SELECT lolwiegehts FROM ding WHERE EXISTS (SELECT dingdong FROM hahaha)".

    THeftigeKlammern "SELECT lolwiegehts FROM (ding JOIN bla ON ((NOT (bla) AND ble) OR blo IS NOT NULL))".

    T_Using "SELECT lol FROM lol JOIN a USING(a,b,c)".

    T1 "§" FAIL_PARSING FAIL_SCANNING.
    T2 "SELECT hi FROM asd".
    T12 "SELECT ? FROM lal" FAIL_PARSING FAIL_SCANNING.
    T55 "SELECT" FAIL_PARSING.
    T10 "SELECT 10000 + hopplaboppla - dingsbums+ -10505 + lol - -hi FROM wiegehtsdirso".
    T3 "SELECT * FROM LAL".
    T4 "SELECT hi, di, bye FROM lel".
    T5 "SELECT hi+54 FROM dai".
    T6 "SELECT hi+0 FROM dings".
    T7 "SELECT hi-100, hi-1043 FROM dingdong".
    T8 "SELECT 10+10, 10, hi-555 FROM tralala".
    T9 "SELECT 10-10+2130-tralala+dingdong - -5 FROM dingsbums".
    T11 "SELECT FF FROM wiegehtsdirso".
    T13 "SELECT ?, hi, ?, 5+dings FROM dong" FAIL_PARSING FAIL_SCANNING.
    T14 "SELECT 'lal' FROM lol".

    T26 "SELECT '0110' FROM yey".
    T27 "SELECT 'FFA' FROM yup".
    T28 "SELECT hi.* FROM dings".
    T29 "SELECT hi.*, '0110', dindgong FROM dings".

    T33 "SELECT asd    FROM lalas".
    T34 "SELECT lol.hi FROM lelel".
    T35 "SELECT COUNT(*) FROM lal".

    T37 "SELECT AVG ( dongs ) FROM lel".

    T38 "SELECT AVG('WTF') FROM lal".


    T39_1 "SELECT AVG('WTF'), MAX(B '1101') FROM dings".
    T39 "SELECT AVG('WTF'), MAX(f+a+N'bob'), MIN(dingsbums-dingdong*wiegehts), SUM(B'1001' * hi) FROM dings".
    T40 "SELECT 'AVG' FROM dings".
    T41 "SELECT dingdong AS bingbong, '1001' AS dingsbums FROM dingdong".
    T42 "SELECT (SELECT dings FROM dings) FROM dings".
    T43 "SELECT NULLIF(1, 2) FROM dings".
    T44 "SELECT COALESCE('hi','bye', dong) FROM dingdong".

    T45 "SELECT firstjoin FROM a CROSS JOIN b".

    T46 "SELECT secondjoin FROM a JOIN b ON c JOIN b ON f".


    T47 "SELECT thirdjoin FROM abc NATURAL JOIN abc NATURAL LEFT JOIN baba".
    T48 "SELECT thirdjoin FROM (SELECT dings FROM dangs) bum NATURAL JOIN baba".
    T49 "SELECT a FROM b JOIN c ON a".
    T50 "SELECT a FROM b NATURAL JOIN c".
    T51 "SELECT dingdong FROM (a NATURAL JOIN b) AS c(a,b,c)".


    T52 "SELECT dingdong FROM (a NATURAL JOIN b) AS a NATURAL JOIN c".





    T53 "SELECT dongdong FROM (a JOIN b ON c) AS b NATURAL JOIN (dingdongs NATURAL JOIN b) AS c".
    T54 "SELECT a FROM b JOIN c ON z = z".


    T55 "SELECT a FROM b NATURAL JOIN (c NATURAL JOIN b) AS bum".

    T56 "SELECT a FROM b NATURAL JOIN (SELECT a FROM b) AS c".

    T57 "SELECT (SELECT (SELECT a FROM bbb) FROM aaa) AS lal FROM dingdong NATURAL JOIN dingdong".
    T58 "SELECT dingsbums FROM (b FULL OUTER JOIN a ON b) JOIN a ON c".
    T59 "SELECT lal FROM ((a JOIN b ON c IN (a, b, c)) AS c NATURAL JOIN b) JOIN (SELECT hi FROM ding) AS lol ON bla.a = ble.a".

    T60 "SELECT hi FROM a WHERE a = b".
    T61 "SELECT hi FROM a WHERE a = b AND a = c".
    T62 "SELECT hi FROM a WHERE A = 5 AND a+b=5".
    T63 "SELECT hi FROM a WHERE (a=5 OR a-5=5) AND NOT a =5".
    T64 "SELECT hi FROM a WHERE 'hi' = my-5".
    T65 "SELECT hi FROM b WHERE a IS NOT NULL".
    T66 "SELECT byebye FROM b WHERE (a IS NOT NULL OR b=a) AND (b-10 IN (SELECT hi FROM b NATURAL JOIN c))".
    T67 "SELECT dingdong FROM b WHERE (a = 'bingbong') AND dingdong=5".
    T68 "SELECT bongdong FROM bangbang WHERE a =1 GROUP BY bingbong HAVING bingbong > COUNT(dings)".
    T69 "SELECT dings FROM dongs GROUP BY a,b,c".
    T70 "SELECT lal FROM dingsbums ORDER BY lal ASC, dings DESC, lol".
    T1 "SELECT B '0110' FROM lol".
    T100 "SELECT hi FROM dings WHERE bom > bim".




''');
exampleStrings.add('''


load library util;
load library htmlConstruction;



var dffText;



var genericTreeGrammar;



var htmlString;

var htmlTree;
var nodesToHTML;



var slopeTokenString;



var currentNames;

var maxJoinCount;
var currentJoinCount;


var renameLimit;
var currentRenameCount;



var mapPrettyStr;



var currentAmountOfSubqueries;



countColumns node {
    var obj = newHashMap(["count", 0]);
    ->(countColumns)([node], rootResultNode, [obj]);
    return obj g "count";
}

addColumnCount obj count {
    obj p ("count",
           obj g"count" plus count);
}



nameColumns node {
    var obj = newHashMap(["columns", []]);
    ->(nameColumns)([node], rootResultNode, [obj]);
    return obj g "columns";
}



originColumns sqlNode {
    var searchedOriginColumn = findFirstColName(sqlNode);
    var originColumn = #"";
    var map = newHashMap(["columnName", searchedOriginColumn]);
    ->(originColumns)([sqlNode], originColumn, [map]);
    return map g "originTableName" concatAll [".", map g "columnName"];
}


findFirstColName sqlNode {
    return ->(findFirstColName)([sqlNode], rootResultNode);
}



originTable sqlNode {
    var searchedOriginColumn = findFirstColName(sqlNode);
    var originColumn = #"";
    var map = newHashMap(["columnName", searchedOriginColumn]);
    ->(originTable)([sqlNode], originColumn, [map]);
    return map g "originTableName";
}



addClass resultNode className {
    setNodeName(resultNode,
                    concatAll(slotNodeName(resultNode),
                              [" ", className]
                             )
               );
}

constructDFFText aNode {
    dffText = "";
    ->(constructDFFTextTree)([aNode], thisResultNode, ["ROOT"]);
     return dffText;
}



initConstructFromTreeString {
    genericTreeGrammar = parseSlopeGrammar("

        Generic
        Rules: treeNode

        treeNode => ~"#~" value ~"\\(~" treeNode* ~"\\)~".

        value => (identifier | number | boolean).

        identifier => IDENTIFIER.
        number => NUMBER.
        boolean => ~"true|false~".

        Scanner:

        NUMBER => ~"[1-9][0-9]*|0~".
        whitespace => SEPARATOR ~"\\n|\\r| ~" BOTTOM.
        IDENTIFIER => ~"([a-z]|[A-Z]|_)([a-z]|[A-Z]|[0-9]|_)*~" BOTTOM.

    ");
}

testConstructFromTreeString {
    var tree = constructFromTreeString("#SELECT()");
}

constructFromTreeString treeString {


    if(equals(genericTreeGrammar, null)) {
        initConstructFromTreeString();
    };

    if(equals(treeString, null)) {
        return null;
    };
    var grammarTree = parseSlopeWordFromGrammar(treeString, genericTreeGrammar);
    var lolTree = ->(t_constructFromTreeString)([grammarTree], rootResultNode);
    return lolTree;
}





constructHTMLString aNode aNodeList {
    htmlString = "";
    ->(_constructHTMLString)([aNode], rootResultNode, [aNodeList]);
    return htmlString;
}

startOpenTag aNode aNodeList {

    htmlString = concatAll(htmlString, ["<",
                                      slotNodeName(aNode),
                                      " ",
                                      html_generateIDString(aNode, aNodeList)
                                      ]
                 );

}

startDivTagClass aNode aNodeList {
    htmlString = concatAll(htmlString, ["<div class='",
                                      slotNodeName(aNode),
                                      "' ",
                                      html_generateIDString(aNode, aNodeList)
                                      ]
                 );

}



addAttributesFromAttNode attNode {
    forEach(child in children(attNode)) {
        if(hasChildren(child)) {
            addAttribute(slotNodeName(child), slotNodeName(child.#1));
        }
        else {
            addNoValueAttribute(slotNodeName(child));
        };

    };
}



addNoValueAttribute attName {
    htmlString = concatAll(htmlString, [" ", attName]);
}

addAttribute attName attValue {
    htmlString = concatAll(htmlString, [
                                " ",
                                attName,
                                "='",
                                attValue,
                                "'"
                           ]
                 );
}




endOpenTag {
    htmlString = concat(htmlString, ">");
}


closeTag aNode {
    htmlString = concat(htmlString, html_createCloseTagFromNode(aNode));
}

closeTagString aString {
    htmlString = concatAll(htmlString, ["</", aString, "> "]);
}

nodeListToString nodeList {
    var theString = "";
    forEach(child in children(nodeList)) {
        theString = concatAll(theString, ["#", slotNodeName(child)]);
    };
    return theString;
}




doHTMLFromTagNode node aNodeList {
    if(hasChildren(node)) {
        if(slotNodeNameEquals(node.#1, "attr")) {
            htmlFromTagNodeWithAttr(node, aNodeList);
            return null;
        };
    };
    htmlFromTagNode(thisGrammarNode, aNodeList);

}

htmlFromTagNode node aNodeList{
    startOpenTag(node, aNodeList);
    endOpenTag();

    var i = 1;
    while(smallerEqualsThan(i, length(children(node)))) {
        ->(_constructHTMLString)([childAt(node, i)], rootResultNode, [aNodeList]);
        i = plus(i, 1);
    };

    closeTagString(slotNodeName(node));
}

htmlFromTagNodeWithAttr node aNodeList {
    startOpenTag(node, aNodeList);
    addAttributesFromAttNode(node.#1);
    endOpenTag();

    var i = 2;
    while(smallerEqualsThan(i, length(children(node)))) {
        ->(_constructHTMLString)([childAt(node, i)], rootResultNode, [aNodeList]);
        i = plus(i, 1);
    };

    closeTagString(slotNodeName(node));
}

htmlFromClassNode node aNodeList {
    startDivTagClass(node, aNodeList);
    endOpenTag();

    var i = 1;
    while(smallerEqualsThan(i, length(children(node)))) {
        ->(_constructHTMLString)([childAt(node, i)], rootResultNode, [aNodeList]);
        i = plus(i, 1);
    };

    closeTagString("DIV");
}

htmlFromClassNodeWithAttr node aNodeList {

    startDivTagClass(node, aNodeList);
    addAttributesFromAttNode(node.#1);
    endOpenTag();

    var i = 2;
    while(smallerEqualsThan(i, length(children(node)))) {
        ->(_constructHTMLString)([childAt(node, i)], rootResultNode, [aNodeList]);
        i = plus(i, 1);
    };

    closeTagString("DIV");
}



constructRuleString aNode {
    var ruleTree = #"ROOT";
    ->(constructRuleTree)([aNode], ruleTree);
    return constructDFFText(ruleTree);
}



constructTreeString node {
    if(equals(node, null)) {
        return "null";
    };
    var stringTree = #"";
    ->(t_constructTreeString)([node], stringTree, [0]);
    return constructDFFText(stringTree);
}



equalTrees tree1 tree2 {
if(or(and(equals(tree1, null),
              not(equals(tree2, null))),
          and(equals(tree2, null),
              not(equals(tree1, null))))) {

        return false;

  };

  if(and(equals(tree1, null),
        equals(tree2, null))) {

        return true;
        };

    return ->(t_equalTrees)([tree1, tree2], rootResultNode);
}


testEqualTrees {

    var a = #"HI";
    addSlotNode(a, #"HI3");

    var b = #"HI";
    addSlotNode(b, #"HI3");

    printString(toString(equalTrees(a, b)));

    var hi = #"HI";
    addSlotNode(hi, #true);

    var hi2 = #"HI";
    addSlotNode(hi2, #true);

    printString(toString(equalTrees(hi, hi2)));
}



pair parentName childName {
    return #parentName [+#childName;];
}


constructSQLHTMLTreeFromGrammar slopeNode {
    var root = #"SQL";
    var env = newHashMap();
    ->(constructSQLHTMLTreeFromGrammar | sqlHTMLDoChildren)
      ([slopeNode], root, [env]);
    return root;
}





nearestChild childNode parentName childName {
    if(slotNodeNameEquals(childNode, parentName)) {
        var currentNode = parent(childNode);
        while(not(equals(currentNode, null))) {
            if(slotNodeNameEquals(
                currentNode, childName)) {
                return false;
            };
            if(slotNodeNameEquals(
                currentNode, parentName)) {
                return true;
            };
            currentNode = parent(parentNode);
        };

        return false;

    } else {
        return false;
    };
}



createJoinBlock joinSymbol leftTableRef joinCondition rightTableRef{

    return #"joinCollectorGroup" [
        +#"collectedNames";
        +#"collectionTarget" {
            +#"join" {
                +#"tableTop" {
                    +#"placeholder";
                    +#"joinDescription" {
                        +#"joinSymbol" {
                            +#"joinSymbolText" {
                                +#"TEXT" {
                                    +#joinSymbol;
                                };
                            };
                        };
                        if(not(equals(joinCondition, null))) {
                            +joinCondition;
                        };
                    };
                    +#"placeholder";

                };
                +#"tableBottom" {
                    +#"leftCell" {
                        +leftTableRef;
                    };
                    +#"placeholder";
                    +#"rightCell" {
                        +rightTableRef;
                    };
                };
            };
        };
    ];
}

allWorkaround {
    return true;
}




currentColumnName node {
    return ->(currentColumnName)([node], rootResultNode);
}




getQualifiedName columnNode {
        var map = newHashMap();
        ->(getQualifiedName)([columnNode], thisResultNode, [map]);
        return map;
}




getTokenString node {
    return ->(getTokenString_t)([node], rootResultNode);
}

getTokenStringWithDefault node default{
    var tokenString = ->(getTokenString_t)([node], rootResultNode);
    if(equals(tokenString, "NULL")) {
        return default;
    } else {
        return tokenString;
    };
}



constructGenericHTMLTree node rootName {
    return #rootName [
      ->(constructGenericHTMLTree)([node], thisResultNode);
    ];
}



main {
    initNodes();
    return doBunchOfVisTests();
}

beispiel lol {
    return lol;
}

hallo {
    print("LOLZ");
}

initNodes {
    nodesToHTML = [];
}

createSQLParseTree inputText {
    return parseSlopeWord(inputText);
}

createSQLVisualization slopeNode {
    var tree = constructSQLHTMLTreeFromGrammar(slopeNode);
    return constructHTMLString(tree, nodesToHTML);
}

createPrettyPrintedString slopeNode {
    return constructStringFromParseTree(slopeNode);
}



doInputVisTests {
    htmlTree = visualizeTests([newHashMap(["text", "SELECT hi FROM ding",
                                            "node", rootGrammarNode])]);

    return redraw();
}


runPrettyPrintTests {
    var tests = ["SELECT hi FROM ding",

                "SELECT hi FROM ding JOIN C ON f",

                 "SELECT hi
                  FROM ( ding JOIN c ON f) b"];

    var bla = 1;
    while(smallerEqualsThan(bla, length(tests))) {
        var node = parseSlopeWord(elementAt(tests, bla));
        print(constructStringFromParseTree(node));
        print("");
        bla = plus(bla, 1);
    };
}




visualize {
    nodesToHTML = [];

    htmlTree = constructSQLHTMLTreeFromGrammar(rootGrammarNode);
    htmlString = constructHTMLString(htmlTree, nodesToHTML);

    return redraw();
}


redraw {
    htmlString = constructHTMLString(htmlTree, nodesToHTML);
    return htmlString;
}

constructConcatString node {
     var concatRoot = #"";
     ->(constructConcatString)([node], concatRoot);
     return constructDFFText(concatRoot);
}

constructSpacedString node {
    var spacedRoot = #"";
    ->(constructConcatString | constructSpaces)([node], spacedRoot);
    return constructDFFText(spacedRoot);
}



constructStringFromParseTree node {
    var stringRoot = #"";
    ->(constructStringFromParseTree | preSpace)([node], stringRoot, [createPrettyPrintEnvironment(0)]);
    return constructDFFText(stringRoot);
}


createPrettyPrintEnvironment spaceNumber {
    return newHashMap(["spaceNumber", spaceNumber]);
}

concatTimes string times {
    var newStr = "";
    var count = 0;
    while(smallerThan(count, times)) {
        newStr = concat(newStr, string);
        count = plus(count, 1);
    };
    return newStr;
}

addedSpaceNumber map1 spaceNumber {
    return addedMap(map1, newHashMap(["spaceNumber", spaceNumber]));
}

addedMap pmap1 map2 {
    var map1 = clone(pmap1);
    forEach(pair in mapPairs(map2)) {

        var key = elementAt(pair, 1);
        mapPut(map1,
               key,
               plus(mapGet(map1, key),
                    mapGet(map2, key))
        );

    };
    return map1;
}

getSpaces environment {
    return concatTimes(" ", mapGet(environment, "spaceNumber"));
}


isAnyOf node name1 name2 {
    if(or(slotNodeNameEquals(node, name1),
          slotNodeNameEquals(node, name2)
          )
    ) {
        return true;
    }
    else {
        return false;
    };
}



getMaxCharSpace node {
    return ->(getMaxCharSpace)([node], rootResultNode);
}




getRecMatches node nameList {
    return getRecMatchesRec(node, nameList, 1, []);
}

getRecMatchesRec node nameList currentIndex matchList {
    if(notEquals(type(slotNodeName(node)), "SLOTString")) {
        return matchList;
    };
    if(isNamedAnd(node)) {
        return matchList;
    };


    if(slotNodeNameEquals(node, elementAt(nameList, currentIndex))) {


        currentIndex = plus(currentIndex, 1);
        if(equals(currentIndex, plus(length(nameList),1))) {
            addElement(matchList, node);
            return matchList;
        };
    };
    forEach(child in children(node)) {
        getRecMatchesRec(child, nameList, currentIndex, matchList);
    };
    return matchList;
}







createNewTable tableName columnsUnknown columns {


    if(length(columns) equals 0) {
        throw("wtf");
    };

    return newHashMap(["tableName",      tableName,
                       "columnsUnknown", columnsUnknown,
                       "columns",        columns]);


}

createNewColumn columnName originTableName {
    return newHashMap(["columnName", columnName,
                       "originTableName", originTableName]);
}

createRecursionStatMap node {
    var map = newHashMap();

    var currentNode = node;
    while(notEquals(currentNode, null)) {
        if(isNamedAnd(currentNode)) {
            /*Add to map */
            var value = mapGet(map, slotNodeName(currentNode));
            if(notEquals(value, null)) {
                mapPut(map, slotNodeName(currentNode), plus(value,1));
            } else {
                mapPut(map, slotNodeName(currentNode), 1);
            };
        };
        currentNode = parent(currentNode);
    };

    return map;
}



generateRandomTreeNode {
    return generateRandomTree(createTemplate(rootGrammarNode));
}

generateRandomTreeNodeParam map {
    if(map g "renameLimit" notEquals null) {
        renameLimit = map g "renameLimit";
    }
    else {
        renameLimit = 0 minus 1;
    };

    return generateRandomTree(createTemplate(rootGrammarNode));
}

generateRandomTrees number {
    var trees = [];
    var i = 0;
    while(i smallerThan number) {
        var template = createTemplate(rootGrammarNode);
        trees addElement generateRandomTree(template);
        i = i plus 1;
    };
    return trees;
}





generateRandomTreesTags number tags {
    var trees = [];
    var i = 0;
    while(i smallerThan number) {
        var template = createTemplate(rootGrammarNode);
        var clonedTags = clone(tags);
        trees addElement generateRandomTreeTags(template, clonedTags);
        i = i plus 1;
    };
    return trees;
}

generateRandomTemplateTags tags {
    return generateRandomTreeTags(createTemplate(rootGrammarNode),
                                    tags);
}

generateRandomTemplate {
    return generateRandomTreeTags(createTemplate(rootGrammarNode),
                                        m());
}


generateRandomTreeTags node tags {
    currentNames = [];

    maxJoinCount = randomIntFromRange(25, 25);
    currentJoinCount = 0;



    if(tags g "length" equals null) {
        tags p ("length", randomIntFromRange(40, 300));
    };
    currentRenameCount = 0;

    var environment = newHashMap();

    ->(generateRandomTree | generateRandomChancesSQL | generateRandomChances)
    ([node], thisResultNode, [environment, tags]);

    return node;

}

generateRandomTree node {
    var tags = newHashMap(["orAbleTermOR", true,
                           "andAbleTermAND", true,
                           "exactDepth", 6,
                           "wordCount", 100]);

    return generateRandomTreeTags(node, tags);
}

printStatementStats node {
        print(constructStringFromParseTree(node));
        print("Column-Rename-Count: " concat toString(ruleNameAmount(node, "selectColumnRename")));
        print("     MaxQuery-Depth: " concat toString(maxSubqueryDepth(node)));
        print("         Join-Count: " concat toString(ruleNameAmount(node, "join")));
        print("     Subquery-Count: " concat toString(amountOfSubqueries(node)));
        print("        Table-Count: " concat toString(amountOfTables(node)));
        print("       Stuff-Amount: " concat toString(amountOfStuff(node)));
        print("    Renames on Path: " concat toString(pathRenameCount(node)));
        print("       Result-Depth: " concat toString(length(getFirstSelectPath(node))));
        print("");
        print("");
}

countRenames node {
    return ruleNameAmount(node, "selectColumnRename");
}

hasAllCapsScannerRule slopeNode {
    var name = scannerRuleName(slopeNode);
    if(equals(name, null)) {
        return false;
    } else {
        if(isAllCaps(name)) {
            return true;
        } else {
            return false;
        };
    };
}

generateRandomColumnName environment {
    var tables = mapValues(environment);

    var columnList = [];
    forEach(table in tables) {
        forEach(column in table g "columns") {
            columnList
                addElement
              b(column g "columnName");
        };
    };

    return generateRandomIdentifierExcept(columnList);
}





getRandomElementFromList list {
    var randomIndex = randomIntFromRange(1, length(list));
    return elementAt(list, randomIndex);
}


isNamedAndChild slopeNode namedANDName childName {
    if(slotNodeNameEquals(slopeNode, childName)) {
        var currentNode = parent(slopeNode);
        while(not(equals(currentNode, null))) {
            if(isNamedAnd(currentNode)) {
                if(slotNodeNameEquals(currentNode, namedANDName)) {
                    return true;
                }
                else {
                    return false;
                };
            };
            currentNode = parent(currentNode);
        };
        return false;
    } else {
        return false;
    };
}


clearNF1 nfold1Node {
    if(isNFOLD_0(nfold1Node)) {
        clearNFOLD_0(nfold1Node);
    } else {
        while(not(equals(length(children(nfold1Node)), 1))) {
            removeNFOLDChildAt(nfold1Node, 2);
        };
    };
}

getNamedAnd slopeNode {
    if(isNamedAnd(slopeNode)) {
        return slopeNode;
    }
    else {
        var currentNode = parent(slopeNode);
        while(not(equals(currentNode, null))) {
            if(isNamedAnd(currentNode)) {
                return currentNode;
            };
            currentNode = parent(currentNode);
        };
        return null;
    };
}

getNamedAndWithName slopeNode nodeName {
    if(and(isNamedAnd(slopeNode),
           slotNodeNameEquals(slopeNode, nodeName)
          )
      ) {
        return slopeNode;
    }
    else {
        var currentNode = parent(slopeNode);
        while(not(equals(currentNode, null))) {
            if(and(isNamedAnd(currentNode),
                   slotNodeNameEquals(currentNode, nodeName))) {
                return currentNode;
            };
            currentNode = parent(currentNode);
        };
        return null;
    };

}



generateRandomChances nodeList resultNode extraList {
    ->(generateRandomChancesSQL | generateRandomChances)(nodeList, resultNode, extraList);
}


generateNFOLDChildrenTF nodeList resultNode extraList {
    var chances = generateRandomChances(nodeList, resultNode, extraList);
    return generateNFOLDChildren(nodeList elementAt 1,
                                 chances  elementAt 1,
                                 chances  elementAt 2);
}

generateNFOLDChildren nfoldNode value1 value2{
    var randInt = randomIntFromRange(value1, value2);

    clearNF1(nfoldNode);

    var counter = 0;

    if(isNFOLD_1(nfoldNode)) {
       counter = 1;
    };

    while(smallerThan(counter, randInt)) {
        addNFOLDChildTemplate(nfoldNode);
        counter = plus(counter, 1);
    };

    return children(nfoldNode);

}

switchToMaxDepth tags {
    var exactDepth = tags g "exactDepth";
    tags mapRemove "exactDepth";
    tags p ("maxDepth", exactDepth);
}

reachedDepthLimit node tags {
    var stats = createRecursionStatMap(node);
    var sfwDepth = stats g "sfwBlock";



    if(tags g "exactDepth" notEquals null) {
        var exactDepth = tags g "exactDepth";
        if(sfwDepth largerEqualsThan exactDepth) {
            return true;
        }
        else {
            return false;
        };
    }
    else {
        if(tags g "maxDepth" notEquals null) {
            var maxDepth = tags g "maxDepth";
            if(sfwDepth largerEqualsThan maxDepth) {
                return true;
            }
            else {
                return false;
            };
        };

        return false;
    };
}

concatList list1 list2{
    var ret = [];
    forEach(element in list1) {
        ret addElement element;
    };

    forEach(element in list2) {
        ret addElement element;
    };

    return ret;
}


createIndexList list {

    var indexList = [];
    var c = 1;
    forEach(child in list) {
        indexList addElement c;
        c = c plus 1;
    };
    return indexList;
}

takeRandomElements list count {
    var randList = [];

    var i = 0;
    while(i smallerThan count) {
        var randIndex = randomIntFromRange(1, length(list));
        var randElement = list removeElementAt randIndex;
        randList addElement randElement;
        i = i plus 1;
    };

    return randList;
}


takeRandomElementsUpTo list count {
    var randList = [];

    var i = 0;
    while(i smallerThan count
    and b(length(list) largerThan 0)  ) {
        var randIndex = randomIntFromRange(1, length(list));
        var randElement = list removeElementAt randIndex;
        randList addElement randElement;
        i = i plus 1;
    };

    return randList;
}

takeRandomElement list {

    var randIndex = randomIntFromRange(1, length(list));
    var randElement = list removeElementAt randIndex;
    return randElement;
}

max n1 n2 {
    if(n1 largerThan n2) {
        return n1;
    }
    else {
        return n2;
    };
}

min n1 n2 {
    if(n1 smallerThan n2) {
        return n1;
    }
    else {
        return n2;
    };
}

listFilled length fill{
    var list = [];
    var i = 1;
    while(i smallerEqualsThan length) {
        list addElement fill;
        i = i plus 1;
    };
    return list;

}

subtractFromNumberUpTo number sub {
    return max(number minus randomIntFromRange(1, sub), 0);
}

listMinus list1 list2 {
    var ret = [];
    var i = 1;
    while(i smallerEqualsThan length(list1)) {
        var first = list1 elementAt i;
        var second = list2 elementAt i;
        ret addElement b(first minus second);
        i = i plus 1;
    };
    return ret;
}

listPlus list1 list2 {
    var ret = [];
    var i = 1;
    while(i smallerEqualsThan length(list1)) {
        var first = list1 elementAt i;
        var second = list2 elementAt i;
        ret addElement b(first plus second);
        i = i plus 1;
    };
    return ret;
}








renameRandomColumns columnList limit {

    /*var randCols = clone(columnList);
    */
    var randCols = takeRandomElements(columnList, limit);

    forEach(column in randCols) {
        print(prettyPrint(column));
    };

}





randomIndexFromWeightedList distrList {
    var summedDistrList = summedList(distrList);

    var randInt = randomInt(lastElement(summedDistrList));


    var i = 1;
    forEach(element in summedDistrList) {
        if(smallerThan(randInt, element)) {
            return i;
        };
        i = plus(i, 1);
    };
    throw("WTF randomIndexFromList");
}

summedList list {
    clonedList = clone(list);
    var i = 2;
    while(smallerEqualsThan(i, length(clonedList))) {
        var previousElement = elementAt(clonedList, minus(i,1));
        var currentElement = elementAt(clonedList, i);
        var summedElement = plus(currentElement, previousElement);
        setElementAt(clonedList, summedElement, i);
        i = plus(i, 1);
    };
    return clonedList;
}


/* OR: (foo | bar) -> [1,1] (Both equally likely)
*/
orEqualDistribution orNode {
    var distrList = [];


    var i = 1;
    while(smallerEqualsThan(i, numAlternatives(orNode))) {
        addElement(distrList, 1);
        i = plus(i, 1);
    };
    return distrList;

}

getColumnsFromSubquery node {
    var columnList = [];
    ->(getColumnsFromSubquery)([node], rootResultNode, [columnList]);

    return columnList;
}



generateRandomIdentifier {
    return getRandomElementFromList(englishWords());
}

generateRandomIdentifierExcept list {
    var identifier = generateRandomIdentifier();
    while(list listContains identifier) {
        identifier = generateRandomIdentifier();
    };
    return identifier;
}

generateLowercaseLetters n {
    var generatedString = "";
    var count = 0;
    while(smallerThan(count, n)) {
        generatedString = concat(generatedString, generateRandomLowercaseLetter());
        count = plus(count, 1);
    };
    return generatedString;
}


generateRandomLowercaseLetter {
    var intCode = randomIntFromRange(97,122);
    return intToCharString(intCode);
}




objToPrettyStringRec obj offset{

    if(type(obj) equals "SLOTHashMap") {

            mapToPrettyStringRec(obj, offset);
    }
    else {
       if(type(obj) equals "SLOTList") {
            listToPrettyStringRec(obj, offset);
       }
       else {
            addPrettyStr(toString(obj) concat "\n");
       };
    };

}


addPrettyStr str {
    mapPrettyStr = mapPrettyStr concat str;
}

mapToPrettyString map {
    mapPrettyStr = "";
    mapToPrettyStringRec(map, "");
    return mapPrettyStr;
}


mapToPrettyStringRec map previousOffset {
    addPrettyStr("{\n");

    var offset = concat(previousOffset, "  ");
    forEach(key in mapKeys(map)) {
        addPrettyStr(offset concatAll [key, " : "]);

        var value = map g key;

        var nextOffsetCount = stringLength(offset) plus stringLength(key) plus 5;
        var nextOffset = concatTimes(" ", nextOffsetCount);

        objToPrettyStringRec(value, nextOffset);
        addPrettyStr("\n");
    };

    addPrettyStr(previousOffset concat "}\n");

}


listToPrettyStringRec list offset {
    addPrettyStr("[\n" concat offset);

    forEach(element in list) {
        objToPrettyStringRec(element, offset concat "  ");
        if(indexOf(list, element) notEquals length(list)) {
            addPrettyStr(",\n");
        } else {
            addPrettyStr("\n");
        };
    };

    addPrettyStr(offset concat "]\n");
}


allColumnsEndWell node {
    return ->(allColumnsEndWell)([node], rootResultNode);
}



createSelectColumnMap node {
    return ->(createSelectColumnMap)([node], rootResultNode);
}


getFinalColumnName selectColumnMap {
    if(selectColumnMap g "renamedAs" notEquals null) {
        return selectColumnMap g "renamedAs";
    }
    else {
        return selectColumnMap g "originColumn" g "columnName";
    };
}




getFirstSelectPath node {
    return ->(getFirstSelectPath)([node], rootResultNode);
}



getSelectPath node {
    return ->(getSelectPath)([node], rootResultNode);
}





pathDepth node {
    var res = #0;
    ->(pathDepth)([node], res);
    return slotNodeName(res);
}




pathRenameCount node {
    var res = #0;
    ->(pathRenameCount)([node], res);
    return slotNodeName(res);
}



setColumnMaps node {
    ->(setColumnMaps)([node], rootResultNode);
}




setFromExtras node {
    ->(setFromExtras)([node], rootResultNode);
}

parentByName node name {
    var theParent = parent(node);
    while(theParent notEquals null) {
        if(theParent slotNodeNameEquals name) {
            return theParent;
        };
    };
    return null;
}

getSelectedBy node {
    var selectedBy = node getExtra "selectedBy";
    if(selectedBy equals null) {
        selectedBy = [];
        node putExtra ("selectedBy", selectedBy);
        return selectedBy;
    }
    else {
        return selectedBy;
    };
}

addSelectedBy node selectingNode{
    var selectedBy = getSelectedBy(node);
    selectedBy addElement selectingNode;
}



setNodeMaps node {
    ->(setNodeMaps)([node], rootResultNode);
}





createNewTableWithRandomColumns tableName {
    var columnList = [];
    var columnNameList = [];
    var randomNumberOfAttributes = randomIntFromRange(8, 8);

    var counter = 0;
    while(smallerThan(counter, randomNumberOfAttributes)) {
        var randomColumnName = generateRandomIdentifierExcept(columnNameList);
        columnNameList addElement randomColumnName;
        columnList addElement createNewColumn(randomColumnName, tableName);
        counter = plus(counter, 1);
    };


    return newHashMap(["tableName", tableName,
                      "columnsUnknown", false,
                      "columns", columnList]);



}


generateNewTableName environment {
    return generateRandomIdentifierExcept(mapKeys(environment));
}




getRandomColumnFromEnvironment environment {
    var randomTable = getRandomTableFromEnvironment(environment);
    return getRandomColumnFromTable(randomTable);
}

getAllColumnsFromEnvironment environment {
    var columnList = [];



    forEach(tableName in mapKeys(environment)) {
        var currentTable = environment g tableName;
        var currentColumns = currentTable g "columns";
        forEach(column in currentColumns) {
            columnList addElement column;
        };


        /*
        if(currentTable g "type" equals "tableLiteral") {
            columnList addElement b(currentTable g "columns" elementAt 1);
        }
        else {
            if(currentTable g "type" equals "tableSubquery") {
                var currentColumns = currentTable g "columns";
                forEach(column in currentColumns) {
                    columnList addElement column;
                };
            }
            else {
                throw("WTF");
            };
        };
        */



    };
    return columnList;
}

getAllColumnsFromEnvironmentAmountPerTable environment amount {
    var columnList = [];

    forEach(tableName in mapKeys(environment)) {
        var currentTable = environment g tableName;
        var currentColumns = currentTable g "columns";

        if(currentTable g "type" equals "tableLiteral") {
            var kuku = 1;
            while(kuku smallerEqualsThan amount) {
                columnList addElement b(currentTable g "columns" elementAt kuku);
                kuku = kuku plus 1;
            };
        }
        else {
            if(currentTable g "type" equals "tableSubquery") {
                var currentColumns = currentTable g "columns";
                forEach(column in currentColumns) {
                    columnList addElement column;
                };
            }
            else {
                throw("WTF");
            };
        };




    };
    return columnList;
}






getRandomColumnFromTable table {
    return getRandomElementFromList(mapGet(table, "columns"));
}


getRandomTableFromEnvironment environment {
    return getRandomElementFromList(mapValues(environment));
}


amountOfStuff node {
    var stuff = #0;
    ->(amountOfStuff)([node], stuff);
    return slotNodeName(stuff);
}

addOneToResultNode resultNode {
    resultNode setNodeName
            b(slotNodeName(resultNode)
                    plus 1);
}



amountOfSubqueries node {
    currentAmountOfSubqueries = 0;
    ->(amountOfSubqueries)([node], rootResultNode);
    return currentAmountOfSubqueries;
}



amountOfTables node {
    return ruleNameAmount(node, "tableLiteral");
}



maxSubqueryDepth node {
    var resultMaxDepth = #1;
    ->(maxSubqueryDepth)([node], resultMaxDepth);
    return slotNodeName(resultMaxDepth) minus 1;
}



namedAndListWithName node name {
    var listResult = #[];
    ->(namedAndListWithName)([node], listResult, [name]);
    return slotNodeName(listResultNode);
}



ruleNameAmount node ruleName {
    var result = #0;
    ->(ruleNameAmount)([node], result, [ruleName]);
    return slotNodeName(result);
}



scannerRuleNameAmount node ruleName {
    var result = #0;
    ->(scannerRuleNameAmount)([node], result, [ruleName]);
    return slotNodeName(result);
}



setPathDepth node depth {

}




between number left right {
    return number largerEqualsThan left
     and b(number smallerEqualsThan right);
}

runRandomGeneration {

    var count = 100;

    print("/*");
    print("*/");
    var i = 0;
    while(i smallerThan count) {
        var node = generateRandomTemplateTags(m(
            "length", 100
        ));
        setColumnMaps(node);
        setFromExtras(node);
        printStatementStats(node);
        i = i plus 1;
    };

}




















chicken {
    var lol = null;


    var num = randomIntFromRange(2, 10);
    var splitLength = randomIntFromRange(1, num);



    var list = splitNumberRandomlyExceptOne(num, splitLength);

    print(num);
    print(splitLength);
    print(list);
    forEach(element in list) {
        expect(element equals 2, false);
    };


    return true;
}

fromExtras {
    var node = parseSlopeWord("SELECT ding.hi
                               FROM (
                                    SELECT bla.hi
                                    FROM bla
                               ) ding");
    setColumnMaps(node);
    setFromExtras(node);

    ->(_fromExtras)([node], rootResultNode);

}


prettyPrint node {
    if(node equals null) {
        return "null";
    }
    else {
        return constructStringFromParseTree(node);
    };
}



createPrettyPrintedTests testStrings {
    var tests = [];
    forEach(testString in testStrings) {
        var node = parseSlopeWord(testString);
        var map = newHashMap(["text", constructStringFromParseTree(node)
                             ,"node", node]);
        print(map mGet "text");
        tests addElement map;
    };
    return tests;
}

visualizeTests testList {
    return #"tests" [
        forEach(testCase in testList) {
            print("Visualizing " concatAll [testCase g "text"]);
            +#"testCase" {
                +#"testBlock textual" {
                    +#"PRE" {
                        +#"CODE" {
                            +#"TEXT" {
                                +#mapGet(testCase, "text");
                            };
                        };
                    };
                };
                +#"testBlock visual" {
                    +constructSQLHTMLTreeFromGrammar(mapGet(testCase, "node"));
                };
            };
        };
    ];
}


doBunchOfVisTests {
    htmlTree = visualizeTests(createPrettyPrintedTests([
        "
         SELECT excavation.safety
         FROM excavation
        ",


        "
        SELECT tb_1.sp2
        FROM (SELECT tb_2.sp2
                FROM (SELECT tb2.sp2
                        FROM tb2
                     ) tb_2
             ) tb_1
        ",

        "
        SELECT tb_1.sp2
        FROM (SELECT tb_2.sp2
                FROM tb_2
             ) tb_1
        ",


        "SELECT medicine.impact
          FROM            (SELECT reflection.professional,
                                 reflection.impact
                            FROM reflection
                          ) discourage
              CROSS JOIN (SELECT privacy.impact
                            FROM privacy
                         ) medicine

        ",
        "
         SELECT excavate.embark
         FROM  (SELECT calf.account,
                       calf.embark
         	  FROM (SELECT pocket.demonstrate AS graze,
         				   pocket.embark
         				   FROM pocket
         	        ) calf
         	  ) excavate
       ",
       "SELECT excavate.embark
        FROM           (SELECT calf.mild AS embark
                        FROM calf
        			   ) excavate
       ",
       "SELECT discourage.content
          FROM            trick
               CROSS JOIN (SELECT reflection.professional,
                                  reflection.content
                             FROM reflection
                          ) discourage",

       "SELECT  discourage.professional, trick.blade, discourage.content
           FROM            trick
                CROSS JOIN (SELECT reflection.professional,
                                   reflection.content
                                FROM reflection
                           ) discourage
       ",


       "
         SELECT intensify.mixture, established.watch extort, intensify.damage
         FROM intensify CROSS JOIN established
       "
     ]));

    return redraw();

}

doBunchOfVisTests10 {
    htmlTree = visualizeTests(createPrettyPrintedTests([
        "SELECT intensify.mixture, established.watch extort, intensify.damage
         FROM intensify CROSS JOIN established",


        "SELECT bottle.glove, presence.stable instrument, bottle.heaven
           FROM bottle CROSS JOIN presence"

    ]));

    return redraw();
}


doBunchOfVisTests2 {
    htmlTree = visualizeTests(createPrettyPrintedTests([
     "
         SELECT tabelle1.spalte1,
                tabelle1.spalte2 AS sp2,
                tb2.spalte3,
                tb2.sp4
         FROM            tabelle1
              CROSS JOIN (SELECT tabelle2.spalte3,
                                 tabelle2.spalte4 AS sp4
                            FROM tabelle2
                         ) tb2
     ",
     "
     SELECT cookie.cable,
            formal.examination,
            unlike.automatic,
            laugh.shareholder,
            cookie.march
     FROM           formal
         CROSS JOIN (SELECT standard.painting automatic
                     FROM standard
                    ) unlike
         CROSS JOIN (SELECT cab.offender elbow,
                            cab.fist shareholder,
                            cab.yes usual
                     FROM (SELECT chance.fist,
                                  chance.offender,
                                  chance.yes
                           FROM (SELECT frontier.yes,
                                        frontier.look offender,
                                        frontier.grin fist
                                 FROM frontier
                                ) chance
                          ) cab
                    ) laugh
         CROSS JOIN (SELECT regain.dismiss cable,
     	 		            regain.chocolate march
                     FROM (SELECT regulator.reach dismiss,
                                  regulator.chocolate
                           FROM regulator
                          ) regain
                    ) cookie
     ",
     "SELECT cookie.cable
      FROM           formal
          CROSS JOIN (SELECT standard.painting automatic
                      FROM standard
                     ) unlike
          CROSS JOIN (SELECT cab.offender elbow,
                             cab.fist shareholder,
                             cab.yes usual
                      FROM (SELECT chance.fist,
                                   chance.offender,
                                   chance.yes
                            FROM (SELECT frontier.yes,
                                         frontier.look offender,
                                         frontier.grin fist
                                  FROM frontier
                                 ) chance
                           ) cab
                     ) laugh
          CROSS JOIN (SELECT regain.dismiss cable,
      			regain.chocolate march
                      FROM (SELECT regulator.reach dismiss,
                                   regulator.chocolate
                            FROM regulator
                           ) regain
                     ) cookie

            ",

    "SELECT tabelle1.spalte1,
            tabelle1.spalte2,
            tb2.spalte3,
            tb2.spalte4
     FROM            tabelle1
          CROSS JOIN (SELECT tabelle2.spalte3,
                             tabelle2.spalte4
                        FROM tabelle2
                     ) tb2",
    "SELECT tabelle1.spalte1
    FROM           tabelle1
        CROSS JOIN (SELECT tabelle2.spalte2,
                           tabelle3.spalte3
                    FROM           tabelle2
                        CROSS JOIN tabelle3
                    ) tb2
        CROSS JOIN tabelle4",

    "SELECT tb1.spalte1
     FROM (SELECT tabelle1.spalte1
           FROM tabelle1
          ) tb1",
    "SELECT tabelle3.spalte3,
            tabelle2.spalte2,
            tabelle1.spalte1
     FROM           tabelle1
         CROSS JOIN tabelle2
         CROSS JOIN tabelle3",

    "SELECT tabelle1.spalte1,
            tabelle2.spalte2,
            tabelle3.spalte3,
            tabelle4.spalte4,
            tabelle5.spalte5,
            tabelle6.spalte6,
            tabelle7.spalte7,
            tabelle8.spalte8,
            tabelle9.spalte9,
            tabelle10.spalte10,
            tabelle11.spalte11
    FROM           tabelle1
        CROSS JOIN tabelle2
        CROSS JOIN tabelle3
        CROSS JOIN tabelle4
        CROSS JOIN tabelle5
        CROSS JOIN tabelle6
        CROSS JOIN tabelle7
        CROSS JOIN tabelle8
        CROSS JOIN tabelle9
        CROSS JOIN tabelle10
        CROSS JOIN tabelle11
        ",

    "SELECT tabelle1.spalte1,
            tabelle2.spalte2,
            tabelle3.spalte3,
            tabelle4.spalte4,
            tabelle5.spalte5,
            tabelle6.spalte6,
            tabelle7.spalte7,
            tabelle8.spalte8,
            tabelle9.spalte9,
            tabelle10.spalte10
    FROM           tabelle1
        CROSS JOIN tabelle2
        CROSS JOIN tabelle3
        CROSS JOIN tabelle4
        CROSS JOIN tabelle5
        CROSS JOIN tabelle6
        CROSS JOIN tabelle7
        CROSS JOIN tabelle8
        CROSS JOIN tabelle9
        CROSS JOIN tabelle10
        ",



    "SELECT tabelle1.spalte1, tabelle2.spalte2, tabelle3.spalte3
         FROM   tabelle1 CROSS JOIN (tabelle2 CROSS JOIN tabelle3)",
    "SELECT tabelle1.spalte1, tb2.spalte2
     FROM            tabelle1
         CROSS JOIN (SELECT tabelle2.spalte2
                     FROM tabelle2
                    ) tb2",
    "SELECT tabelle1.spalte1
     FROM tabelle1",
    "SELECT tabelle1.spalte1, tabelle2.spalte2
     FROM tabelle1 CROSS JOIN tabelle2",
    "SELECT tabelle1.spalte1, tabelle2.spalte2, tabelle3.spalte3
     FROM   tabelle1 CROSS JOIN tabelle2 CROSS JOIN tabelle3"
    ]));

    return redraw();
}

doBunchOfVisTests2 {

    htmlTree = visualizeTests(createPrettyPrintedTests([
    "SELECT cookie.cable
     FROM           formal
         CROSS JOIN (SELECT standard.painting automatic
                     FROM standard
                    ) unlike
         CROSS JOIN (SELECT cab.offender elbow,
                            cab.fist shareholder,
                            cab.yes usual
                     FROM (SELECT chance.fist,
                              chance.offender,
                              chance.yes
                           FROM (SELECT frontier.yes,
                                        frontier.look offender,
                                        frontier.grin fist
                                 FROM frontier
                                ) chance
                          ) cab
                    ) laugh
         CROSS JOIN (SELECT regain.dismiss cable,
     			regain.chocolate march
                     FROM (SELECT regulator.reach dismiss,
                                  regulator.chocolate
                           FROM regulator
                          ) regain
                    ) cookie
    ",
    "SELECT valid.pork patient
     FROM          challenge
        CROSS JOIN (SELECT assessment.put,
                           assessment.put exercise,
                           senior.stuff conclude
                    FROM          (SELECT property.language,
                                          property.language,
                                          property.council AS recruit,
                                          property.couch put,
                                          property.couch starter,
                                          property.couch selection
                                   FROM property
                                  ) AS assessment
                       CROSS JOIN senior
                   ) clearly
        CROSS JOIN (SELECT summit.currency AS pork,
                           summit.slope,
                           summit.ideal,
                           summit.slope
                    FROM (SELECT aluminum.slope,
                                 aluminum.orange,
                                 aluminum.ideal AS currency,
                                 aluminum.engagement,
                                 aluminum.ideal
                          FROM aluminum
                         ) AS summit
                   ) valid
        CROSS JOIN only
    ",
    "SELECT suddenly.rePile AS boom
         FROM          relief
            CROSS JOIN worth
            CROSS JOIN (SELECT derive.pile AS rePile,
                               derive.silk,
                               derive.brother
                        FROM derive
                       ) suddenly
            ",


    "SELECT suddenly.pile AS boom
         FROM          relief
            CROSS JOIN worth
            CROSS JOIN (SELECT derive.pile,
                               derive.silk,
                               derive.brother
                        FROM derive
                       ) suddenly
            ",
    "SELECT suddenly.pile
         FROM          relief
            CROSS JOIN worth
            CROSS JOIN (SELECT derive.pile,
                               derive.silk,
                               derive.brother
                        FROM derive
                       ) suddenly
            ",

  "SELECT sqScience. transportation
 FROM (SELECT science. transportation,
              science. illustrate
        FROM  science
      ) sqScience",

    "SELECT tb1.c1, tb1.c2
     FROM tb1
     WHERE tb1.c1 = c2 AND tb1.c2 != c3
     OR   tb1.c2 = c3 AND tb1.c3 = 100",

      "SELECT tb1.c1, tb1.c2
     FROM tb1
     WHERE  (tb1.c1 = c2 OR tb1.c2 != c3)
      AND   (tb1.c2 = c3 OR tb1.c3 = 100)",


    "SELECT a. b
     FROM a
     WHERE b = 2
        OR c = 3",
    "
    SELECT tb1. c1
    FROM tb1
    WHERE c1 = 2",
    "SELECT tb1. c1
       FROM tb1
      WHERE c1 = 2 AND c2 = 4",

    "SELECT ding.hi FROM ding",
   "SELECT doong.loool FROM doong, bong",
   "SELECT doong.loool FROM doong NATURAL JOIN bong",
   "SELECT doong.loool FROM doong NATURAL LEFT OUTER JOIN bong",
   "SELECT doong.loool FROM doong NATURAL LEFT JOIN bong",
   "SELECT doong.loool FROM doong NATURAL RIGHT JOIN bong",
   "SELECT doong.loool FROM doong NATURAL FULL JOIN bong",
   "SELECT Sadowski.Alexander,
           Dings.Alex,
           Sadowski.Erdbeere,
           Dings.Baer
    FROM Sadowski , Dings"

    ]));

    return redraw();

}

/*
WTF thing:


   "SELECT  wk. ix
    FROM             sg wk
        NATURAL JOIN (SELECT uv.hh,
                             hq.bw,
                             hq.cq
                      FROM             uv
                          NATURAL JOIN bf AS hq
                     ) AS hi"

*/


createColumnMapFromNode columnNode {
    var map = newHashMap();
    ->(createColumnMapFromNode)([columnNode], rootResultNode, [map]);

    return map;
}




createTableMapFromNode pTableLiteral {
    var map = newHashMap();
    ->(createTableMapFromNode)([pTableLiteral], rootResultNode, [map]);

    return map;
}




listContains list obj {
    forEach(element in list) {
        if(equals(element, obj)) {
            return true;
        };
    };
    return false;
}


TreeFunction countColumns

setOpableQuery.AND obj {
    return thisTreeFunction([thisGrammarNode.#1], thisResultNode, [obj]);
}

intersectableQuery.AND obj {
    return thisTreeFunction([thisGrammarNode.#1], thisResultNode, [obj]);
}


selectList.AND obj {

    addColumnCount(obj, 1);
    addColumnCount(obj, length(children(thisGrammarNode.#2)));

    return true;
}

sfwBlock.AND obj {
    var found = thisTreeFunction([thisGrammarNode.#1], thisResultNode, [obj]);

    if(not(found)) {
        return thisTreeFunction([thisGrammarNode.#2], thisResultNode, [obj]);
    }
    else {
        return true;
    };

}


ALL obj {
    var ret = false;
    forEach(child in children(thisGrammarNode)) {
        var current = thisTreeFunction([child],
                                       thisResultNode,
                                       [obj]);

        if(current) {
            ret = true;
        };
    };
    return ret;
}

TreeFunction nameColumns

setOpableQuery.AND obj {
    return thisTreeFunction([thisGrammarNode.#1], thisResultNode, [obj]);
}

intersectableQuery.AND obj {
    return thisTreeFunction([thisGrammarNode.#1], thisResultNode, [obj]);
}

selectList.AND obj {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode, [obj]);
    thisTreeFunction([thisGrammarNode.#2], thisResultNode, [obj]);

    return true;
}

selectColumn.AND obj {
    var singleMap = newHashMap(["addColumnName", true,
                                "columnName", null]);

    thisTreeFunction([thisGrammarNode.#1], thisResultNode, [singleMap]);

    g(obj, "columns") addElement g(singleMap, "columnName");

}

column.AND singleMap {
    var col = createColumnMapFromNode(thisGrammarNode);
    singleMap p ("columnName" ,g(col, "columnName"));
}



sfwBlock.AND obj {
    var found = thisTreeFunction([thisGrammarNode.#1], thisResultNode, [obj]);

    if(not(found)) {
        return thisTreeFunction([thisGrammarNode.#2], thisResultNode, [obj]);
    }
    else {
        return true;
    };

}


ALL obj {
    var ret = false;
    forEach(child in children(thisGrammarNode)) {
        var current = thisTreeFunction([child],
                                       thisResultNode,
                                       [obj]);

        if(current equals true) {
            ret = true;
        };
    };
    return ret;
}

TreeFunction originColumns

sfwBlock.AND map {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode, [map]);
    return thisTreeFunction([thisGrammarNode.#2], thisResultNode, [map]);
}

selectColumn.AND map
{
    var ret = false;
    if(hasOPTChild(thisGrammarNode.#2))
    {
        ret = thisTreeFunction([thisGrammarNode.#2], thisResultNode, [map]);
        if(ret equals true)
        {
            map p ("columnName", null);
            ret = thisTreeFunction([thisGrammarNode.#1], thisResultNode, [map]);
        };
    }
    else
    {
        ret = thisTreeFunction([thisGrammarNode.#1], thisResultNode, [map]);
    };
    return ret;
}

fromclause.AND map {
    if(thisTreeFunction([thisGrammarNode.#2], thisResultNode, [map])) {
        return true;
    }else {
        return thisTreeFunction([thisGrammarNode.#3], thisResultNode, [map]);
    };
}

tableLiteral.AND map {
    if(hasOPTChild(thisGrammarNode.#2)) {
        return thisTreeFunction([thisGramamrNode.#2], thisResultNode, [map]);
    }
    else {
        var name = getTokenString(thisGrammarNode.#1);
        if(map g "originTableName" equals name) {
            return true;
        }else {
            return false;
        };
    };
}



isNamedAndChain("tableLiteral" "tableAsRename").AND map {
    var name = getTokenString(thisGrammarNode.#2);
    if(map g "originTableName" equals name) {
        return true;
    }else {
        return false;
    };
}


tableSubquery.AND map {
    var ret = thisTreeFunction([thisGrammarNode.#2], thisResultNode, [map]);
    if(ret equals true) {
        thisTreeFunction([thisGrammarNode.#1], thisResultNode, [map]);
        return true;
    }
    else {
        return false;
    };
}

isNamedAndChain("tableSubquery" "tableAsRename").AND map {
    var name = getTokenString(thisGrammarNode.#2);
    if(map g "originTableName" equals name) {
        return true;
    }else {
        return false;
    };
}

selectColumnRename.AND map{
    var name = getTokenString(thisGrammarNode.#2);
    if(name equals b(map g "columnName")) {
        return true;
    }
    else {
        return false;
    };
}

usingSpecification map {
    return false;
}

onSpecification map {
    return false;
}

column.AND map {
    var columnMap = createColumnMapFromNode(thisGrammarNode);
    if(map g "columnName" equals b(columnMap g "columnName")) {

      map p ("originTableName", columnMap g "originTableName");
      return true;
    }
    else {
        if(map g "columnName" equals null) {
          map p ("originTableName", columnMap g "originTableName");
          map p ("columnName", columnMap g "columnName");
          return true;
        } else {
            return false;
        };
    };
}

ALL map{
    var ret = false;
    forEach(child in children(thisGrammarNode)) {
        var current = thisTreeFunction([child], thisResultNode, [map]);
        if(current equals true) {
            ret = true;
        };
    };
    return ret;
}


TreeFunction findFirstColName

selectColumn.AND {
    if(hasOPTChild(thisGrammarNode.#2)) {
        thisTreeFunction([thisGrammarNode.#2], thisResultNode);
    }
    else {
        return thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    };
}

selectColumnRename.AND {
    return getTokenString(thisGrammarNode.#2);
}

column.AND {
    if(hasOPTChild(thisGrammarNode.#2)) {
        return thisTreeFunction([thisGrammarNode.#2], thisResultNode);
    }
    else {
        return getTokenString(thisGrammarNode.#1);
    };
}

columnTail.AND {
    return getTokenString(thisGrammarNode.#2);
}

sfwBlock.AND {
    return thisTreeFunction([thisGrammarNode.#1], thisResultNode);
}


ALL {
    var ret = null;
    forEach(child in children(thisGrammarNode)) {
        var current = thisTreeFunction([child], thisResultNode);
        if(current notEquals null) {
            ret = current;
        };
    };
    return ret;
}


TreeFunction originTable

sfwBlock.AND map {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode, [map]);
    return thisTreeFunction([thisGrammarNode.#2], thisResultNode, [map]);
}

selectColumn.AND map
{
    var ret = false;
    if(hasOPTChild(thisGrammarNode.#2))
    {
        ret = thisTreeFunction([thisGrammarNode.#2], thisResultNode, [map]);
        if(ret equals true)
        {
            map p ("columnName", null);
            ret = thisTreeFunction([thisGrammarNode.#1], thisResultNode, [map]);
        };
    }
    else
    {
        ret = thisTreeFunction([thisGrammarNode.#1], thisResultNode, [map]);
    };
    return ret;
}

fromclause.AND map {
    if(thisTreeFunction([thisGrammarNode.#2], thisResultNode, [map])) {
        return true;
    }else {
        return thisTreeFunction([thisGrammarNode.#3], thisResultNode, [map]);
    };
}

tableLiteral.AND map {
    if(hasOPTChild(thisGrammarNode.#2)) {
        return thisTreeFunction([thisGramamrNode.#2], thisResultNode, [map]);
    }
    else {
        var name = getTokenString(thisGrammarNode.#1);
        if(map g "originTableName" equals name) {
            return true;
        }else {
            return false;
        };
    };
}



isNamedAndChain("tableLiteral" "tableAsRename").AND map {
    var name = getTokenString(thisGrammarNode.#2);
    if(map g "originTableName" equals name) {
        return true;
    }else {
        return false;
    };
}


tableSubquery.AND map {
    var ret = thisTreeFunction([thisGrammarNode.#2], thisResultNode, [map]);
    if(ret equals true) {
        thisTreeFunction([thisGrammarNode.#1], thisResultNode, [map]);
        return true;
    }
    else {
        return false;
    };
}

isNamedAndChain("tableSubquery" "tableAsRename").AND map {
    var name = getTokenString(thisGrammarNode.#2);
    if(map g "originTableName" equals name) {
        return true;
    }else {
        return false;
    };
}

selectColumnRename.AND map{
    var name = getTokenString(thisGrammarNode.#2);
    if(name equals b(map g "columnName")) {
        return true;
    }
    else {
        return false;
    };
}

usingSpecification map {
    return false;
}

onSpecification map {
    return false;
}

column.AND map {
    var columnMap = createColumnMapFromNode(thisGrammarNode);
    if(map g "columnName" equals b(columnMap g "columnName")) {

      map p ("originTableName", columnMap g "originTableName");
      return true;
    }
    else {
        if(map g "columnName" equals null) {
          map p ("originTableName", columnMap g "originTableName");
          map p ("columnName", columnMap g "columnName");
          return true;
        } else {
            return false;
        };
    };
}

ALL map{
    var ret = false;
    forEach(child in children(thisGrammarNode)) {
        var current = thisTreeFunction([child], thisResultNode, [map]);
        if(current equals true) {
            ret = true;
        };
    };
    return ret;
}


TreeFunction findFirstColName

selectColumn.AND {
    if(hasOPTChild(thisGrammarNode.#2)) {
        thisTreeFunction([thisGrammarNode.#2], thisResultNode);
    }
    else {
        return thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    };
}

selectColumnRename.AND {
    return getTokenString(thisGrammarNode.#2);
}

column.AND {
    if(hasOPTChild(thisGrammarNode.#2)) {
        return thisTreeFunction([thisGrammarNode.#2], thisResultNode);
    }
    else {
        return getTokenString(thisGrammarNode.#1);
    };
}

columnTail.AND {
    return getTokenString(thisGrammarNode.#2);
}

sfwBlock.AND {
    return thisTreeFunction([thisGrammarNode.#1], thisResultNode);
}


ALL {
    var ret = null;
    forEach(child in children(thisGrammarNode)) {
        var current = thisTreeFunction([child], thisResultNode);
        if(current notEquals null) {
            ret = current;
        };
    };
    return ret;
}


TreeFunction constructDFFTextTree

ALL exceptString {
   var stringToAppend = "";
   if(not(equals(slotNodeName(thisGrammarNode), exceptString))) {
       stringToAppend = toString(slotNodeName(thisGrammarNode));
   };
   dffText = concat(dffText, stringToAppend);


   forEach(aChild in children(thisGrammarNode)) {
        thisTreeFunction([aChild], thisResultNode, [exceptString]);
   };
}

TreeFunction t_constructFromTreeString

treeNode.AND {
    var node = thisTreeFunction([thisGrammarNode.#2], thisResultNode);

    forEach(child in children(thisGrammarNode.#4)) {
        addSlotNode(node, thisTreeFunction([child], thisResultNode));
    };
    return node;
}

identifier.AND {

    return #thisTreeFunction([thisGrammarNode.#1], thisResultNode);
}

number.AND {

    return #stringToInt(
            thisTreeFunction([thisGrammarNode.#1], thisResultNode)
           );
}

boolean.AND {

    return #stringToBool(
            thisTreeFunction([thisGrammarNode.#1], thisResultNode)
           );
}

value.AND {
    return thisTreeFunction([thisGrammarNode.#1], thisResultNode);
}

SLOPETOKEN {
    return slotNodeName(thisGrammarNode.#1);
}

GROUP {
    return slotNodeName(thisGrammarNode.#1.#1);
}

ALL {
    if(hasChildren(thisGrammarNode)) {
        return thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    };
}


TreeFunction _constructHTMLString

TEXT aNodeList {


    var i = 0;
    forEach(child in children(thisGrammarNode)) {
        if(largerThan(i, 0)) {
            htmlString =
                    concat(htmlString, " ");
        };
        htmlString =
                concat(htmlString, slotNodeName(child));
        i = plus(i, 1);
    };


}

ROOT aNodeList {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode, [aNodeList]);
    };
}

att {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode);
    };
}

att.ALL {
    /*htmlString = concatAll(htmlString, [" ", slotNodeName(thisGrammarNode),
                                        "='",
                                        slotNodeName(thisGrammarNode.#1),
                                        "'"
                                     ]
                 );*/


    var attributeValue = "";
    var addSpace = false;
    forEach(child in children(thisGrammarNode)) {
        if(addSpace) {
            attributeValue = concat(attributeValue, " ");
        };
        attributeValue = concat(attributeValue, slotNodeName(child));
        addSpace = true;
    };
    addAttribute(slotNodeName(thisGrammarNode), attributeValue);
}


DIV aNodeList {

    if(and(
        largerThan(length(children(thisGrammarNode)), 0),
        equals(slotNodeName(thisGrammarNode.#1), "att")
       )
    ) {
        printString(concat("In ", slotNodeName(thisGrammarNode)));
        startOpenTag(thisGrammarNode, aNodeList);
            thisTreeFunction([thisGrammarNode.#1], thisResultNode);
        endOpenTag();

        printString(htmlString);

        var i = 2;
        while(smallerEqualsThan(i, length(children(thisGrammarNode)))) {
            thisTreeFunction([childAt(thisGrammarNode, i)], thisResultNode, [aNodeList]);
            i = plus(i, 1);
        };

        closeTag(thisGrammarNode);

    };



}

BR aNodeList {

    if(and(
        largerThan(length(children(thisGrammarNode)), 0),
        equals(slotNodeName(thisGrammarNode.#1), "att")
       )
    ) {
        printString(concat("In ", slotNodeName(thisGrammarNode)));
        startOpenTag(thisGrammarNode, aNodeList);
            thisTreeFunction([thisGrammarNode.#1], thisResultNode);
        endOpenTag();

        printString(htmlString);

        var i = 2;
        while(smallerEqualsThan(i, length(children(thisGrammarNode)))) {
            thisTreeFunction([childAt(thisGrammarNode, i)], thisResultNode, [aNodeList]);
            i = plus(i, 1);
        };

        closeTag(thisGrammarNode);

    };



}

SPAN aNodeList {

    if(and(
        largerThan(length(children(thisGrammarNode)), 0),
        equals(slotNodeName(thisGrammarNode.#1), "att")
       )
    ) {
        printString(concat("In ", slotNodeName(thisGrammarNode)));
        startOpenTag(thisGrammarNode, aNodeList);
            thisTreeFunction([thisGrammarNode.#1], thisResultNode);
        endOpenTag();

        printString(htmlString);

        var i = 2;
        while(smallerEqualsThan(i, length(children(thisGrammarNode)))) {
            thisTreeFunction([childAt(thisGrammarNode, i)], thisResultNode, [aNodeList]);
            i = plus(i, 1);
        };

        closeTag(thisGrammarNode);

    };



}

PRE aNodeList{
    doHTMLFromTagNode(thisGrammarNode, aNodeList);
}

CODE aNodeList {
    doHTMLFromTagNode(thisGrammarNode, aNodeList);
}

ALL aNodeList {

            if(hasChildren(thisGrammarNode)) {
                if(slotNodeNameEquals(thisGrammarNode.#1, "attr")) {
                    htmlFromClassNodeWithAttr(thisGrammarNode, aNodeList);
                    return null;
                };
            };

            htmlFromClassNode(thisGrammarNode, aNodeList);



}




TreeFunction constructRuleTree

OR {
    var numAlts = numAlternatives(thisGrammarNode);


    thisTreeFunction([createORChildTemplateAt(thisGrammarNode, 0)], thisResultNode);


    var counter = 1;

    while(smallerThan(counter, numAlts)) {

        +#" | ";
        thisTreeFunction([createORChildTemplateAt(thisGrammarNode, counter)], thisResultNode);
        counter = plus(counter, 1);
    };

}

OPT {
    var childTemplate = createSingleChildTemplate(thisGrammarNode);
    thisTreeFunction([childTemplate], thisResultNode);
    +#"?";
}

BRACKET {

    +#"(";
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    +#")";
}

isNFOLD_0() {
    var childTemplate = createNFOLDChildTemplate(thisGrammarNode);
    thisTreeFunction([childTemplate], thisResultNode);
    +#"*";
}

isNFOLD_1() {
    var childTemplate = createNFOLDChildTemplate(thisGrammarNode);
    thisTreeFunction([childTemplate], thisResultNode);
    +#"+";
}

AND {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);

    var counter = 2;
    var childrenLength = length(children(thisGrammarNode));
    while(or(smallerThan(counter, childrenLength),
             equals(counter,childrenLength))) {
        +#" ";
        thisTreeFunction([childAt(thisGrammarNode, counter)], thisResultNode);

        counter = plus(counter, 1);
    };
}

NULL {
    +#"NULL";
}

SLOPETOKEN {
    +#"~"";
    +#slotNodeName(thisGrammarNode.#1) ;
    +#"~"";
}

ALL {
    +#slotNodeName(thisGrammarNode);
}

TreeFunction t_constructTreeString

ALL tabNumber {

    var i = 0;
    while(smallerThan(i, tabNumber)) {
        +#"    ";
        i = plus(i,1);
    };

    +#"#";
    +#slotNodeName(thisGrammarNode);
    +#"(";

    if(hasChildren(thisGrammarNode)) {
        +#"\n";

        forEach(child in children(thisGrammarNode)) {
            thisTreeFunction([child], thisResultNode, [plus(tabNumber, 1)]);
            +#"\n";
        };

        i = 0;
        while(smallerThan(i, tabNumber)) {
                +#"    ";
                i = plus(i,1);
        };
    };

    +#")";
}

TreeFunction t_equalTrees

ALL secondGrammarNode:ALL {
    if(or(
            not(
                equals(slotNodeName(thisGrammarNode),
                      slotNodeName(secondGrammarNode)
                )
            ),
            not(
                equals(length(children(thisGrammarNode)),
                       length(children(secondGrammarNode))
                )
            )
        )
    ) {
        printString("Nodes NOT EQUAL");
        printString("LEFT:");
        printString(runtimeType(slotNodeName(thisGrammarNode)));
        printString(constructTreeString(thisGrammarNode));
        printString("RIGHT:");
        printString(runtimeType(slotNodeName(secondGrammarNode)));
        printString(constructTreeString(secondGrammarNode));
        return false;
    }
    else {
        var i = 1;
        while(smallerEqualsThan(i, length(children(thisGrammarNode)))) {
            if(not(
                thisTreeFunction([childAt(thisGrammarNode, i),
                                  childAt(secondGrammarNode, i)],
                                 rootResultNode)
            )) {
                return false;
            };

            i = plus(i,1);
        };
        return true;
    };
}

TreeFunction constructSQLHTMLTreeFromGrammar

queryStatement.AND env {
    +#"hidePins" {
        continue([thisGrammarNode], thisResultNode, [env]);
    };
}

setOpableQuery.AND env {
    /*
        TODO: Implement queryExceptOrUnion
    */
    thisTreeFunction([thisGrammarNode.#1], thisResultNode, [env]);
}


intersectableQuery.AND env {
    /*
        TODO: Implement queryIntersect
    */
    thisTreeFunction([thisGrammarNode.#1], thisResultNode, [env]);
}



sfwBlock.AND env {
    +#"sfwBlock" {
        thisTreeFunction([thisGrammarNode.#1], thisResultNode, [env]);
        thisTreeFunction([thisGrammarNode.#3], thisResultNode, [env]);
        thisTreeFunction([thisGrammarNode.#2], thisResultNode, [env]);
        /*TODO: Implement Groupby, Having, Orderby
        */
    };
}


/*
    TODO: DISTINCT / ALL Visualisieurng fehlt
*/
selectclause.AND env {
    +#"sfwBlockRow" {
        +#"selectClause arrowable" {
            thisTreeFunction([thisGrammarNode.#3],
                              thisResultNode, [env]);
            if(env g "currentTableName" notEquals null) {
                +#"subqueryName" {
                    +#"TEXT" {
                        +#env g "currentTableName";
                    };
                };
            };
            +#"collectedNames";
        };
        +#"subqueryCellPlaceholder";
    };
}

selectList.AND env {
    +#"selectedAttributes" {
        +#"attr" {
            +#"data-trio-arrow";
        };
        continue([thisGrammarNode], thisResultNode, [env]);
    };


}


selectColumn.AND env {
    +#"selectColumn" {
        var clonedEnv = clone(env);

        +#"attr" {
            +pair("data-name", currentColumnName(thisGrammarNode));
        };

        if(hasOPTChild(thisGrammarNode.#2)) {
            addClass(thisResultNode, "renamed");
            thisTreeFunction([thisGrammarNode.#2], thisResultNode, [clonedEnv]);
        }
        else {
            addClass(thisResultNode, "original");
        };

        clonedEnv p ("hideTableNames", true);

        thisTreeFunction([thisGrammarNode.#1], thisResultNode, [clonedEnv]);


    };
}

selectColumnRename.AND env {
    env p ("currentColumnName", getTokenString(thisGrammarNode.#2));
    +#"as" {
        +#"TEXT" {
            +#getTokenString(thisGrammarNode.#2);
        };
    };
}

column.AND env {

    var columnMap = createColumnMapFromNode(thisGrammarNode);

    var originTableName = mapGet(columnMap, "originTableName");
    var columnName = mapGet(columnMap, "columnName");
    var createdNode =
            #"column" [
                +#"attr" {
                    +#"data-origin-table-name" {
                        if(equals(originTableName,null)) {
                            +#"NULL";
                        } else {
                            +#originTableName;
                        };
                    };
                    +#"data-name" {
                        +#columnName;
                    };
                };

                +#"pin top";
                +#"pin bottom";
                +#"originalName" {
                    +#"TEXT" {
                        if(or(env g "hideTableNames" equals true,
                              originTableName equals null
                                )) {
                            +#columnName;
                        }
                        else {
                            +#originTableName concatAll [".", columnName];
                        };
                    };
                };
            ];

    +createdNode;
}


number.AND env {
    +#"TEXT" {
        +#getTokenString(thisGrammarNode.#1);
    };
}

allColumns.AND env {
    addClass(thisResultNode, "all");
    +#"selectedAttributes" {
        +#"selectedAttribute" {
            +#"attr" {
                +pair("data-origin-table-name", "*");
                +pair("data-name", "*");
            };
            +#"pin top";
            +#"pin bottom";
            +#"originalName" {
                +#"TEXT" {
                    +#"*";
                };
            };
        };
    };
}

whereclause.AND env {
    +#"sfwBlockRow" {
        +#"whereClause" {
            +#"expression" {
                thisTreeFunction([thisGrammarNode.#2], thisResultNode, [env]);
            };
        };
        +#"subqueryCellPlaceholder";
    };
}


orAbleTerm.AND env {
    if(hasNoChildren(thisGrammarNode.#2)) {
        thisTreeFunction([thisGrammarNode.#1], thisResultNode, [env]);
    }
    else {
        +#"box" {
            +#"boxDescriptor" {
                +#"boxDescriptorText" {
                    +#"TEXT" {
                        +#"OR";
                    };
                };
                +#"boxContent" {
                    +#"boxSection" {
                        thisTreeFunction([thisGrammarNode.#1], thisResultNode, [env]);
                    };
                    forEach(child in children(thisGrammarNode.#2)) {
                        +#"boxSection" {
                            thisTreeFunction([child], thisResultNode, [env]);
                        };
                    };
                };
            };
        };
    };
}


predicate.AND env {
    var temp_termexpr = #"";
    thisTreeFunction([thisGrammarNode.#1], temp_termexpr, [env]);
    thisTreeFunction([thisGrammarNode.#2], temp_termexpr, [env]);

    +removeNode(temp_termexpr.#1);

}

comparison.AND env {
    var leftExpr = removeNode(thisResultNode.#1);

    +#"comparison" {
        +leftExpr;
        +#"TEXT" {
            +#getTokenString(thisGrammarNode.#1);
        };
        +#"TEXT" {
            +#" ";
        };
        thisTreeFunction([thisGrammarNode.#2], thisResultNode, [env]);
    };
}

fromclause.AND env {
    +#"sfwBlockRow" {
        +#"fromClause" {
            continue([thisGrammarNode], thisResultNode, [env]);
        };
        +#"subqueryCellPlaceholder";
    };
}

tablereference.AND env {
    var temp_result = #"";
    thisTreeFunction([thisGrammarNode.#1], temp_result, [env]);
    thisTreeFunction([thisGrammarNode.#2], temp_result, [env]);
    +removeNode(temp_result.#1);
}


tablereferenceComma.AND env {
    var leftTableRef = removeNode(thisResultNode.#1);

    var temp_result = #"";
    thisTreeFunction([thisGrammarNode.#2], temp_result, [env]);

    var rightTableRef = removeNode(temp_result.#1);

    var joinBlock = createJoinBlock("X", leftTableRef, null, rightTableRef);
    +joinBlock;
}

tablereference0.AND env {

    var temp_result = #"";
    thisTreeFunction([thisGrammarNode.#1], temp_result, [env]);
    thisTreeFunction([thisGrammarNode.#2], temp_result, [env]);

    +removeNode(temp_result.#1);
}

crossJoin.AND env {
    var leftTableRef = removeNode(thisResultNode.#1);

    var temp_result = #"";
    thisTreeFunction([thisGrammarNode.#3], temp_result, [env]);

    var rightTableRef = removeNode(temp_result.#1);
    var joinBlock = createJoinBlock("X", leftTableRef, null, rightTableRef);
    +joinBlock;
}

naturalJoin.AND env {
    var leftTableRef = removeNode(thisResultNode.#1);

    var temp_result = #"";
    thisTreeFunction([thisGrammarNode.#4], temp_result, [env]);

    var rightTableRef = removeNode(temp_result.#1);

    var token = ->(getJoinCharacter)([thisGrammarNode.#2], thisResultNode);


    var joinBlock = createJoinBlock(token, leftTableRef, null, rightTableRef);
    +joinBlock;
}

tableSubquery.AND env {
    var clonedEnv = clone(env);
    clonedEnv p ("currentTableName", null);
    thisTreeFunction([thisGrammarNode.#2], thisResultNode, [clonedEnv]);

    +#"tableSubquery" {
        +#"attr" {
            +pair("data-name", b(clonedEnv g "currentTableName"));
        };
        thisTreeFunction([thisGrammarNode.#1], thisResultNode, [clonedEnv]);
    };
}

tableAsRename.AND env {
    env p ("currentTableName", getTokenString(thisGrammarNode.#2));
}


tableLiteral.AND env {

    var tableMap = createTableMapFromNode(thisGrammarNode);

    var tableName = mapGet(tableMap, "tableName");


    +#"tableLiteral" {
        +#"attr" {
            +pair("data-name", tableName);
        };

        +#"pin top";
        +#"pin left";
        +#"pin right";
        +#"pin bottom";

        +#"TEXT" {
            +#tableName;
        };
    };
}

isNamedAnd().AND env {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode, [env]);
    };
}

OR.INDEX env {
}




ALL environment {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode, [environment]);
    };
}



TreeFunction sqlHTMLDoChildren

ALL environment{
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode, [environment]);
    };
}

TreeFunction getJoinCharacter

joinSetSpecification{
    return thisTreeFunction([thisGrammarNode.#1], thisResultNode);
}

OPT.NULL {
    return "⨝";
    /* &#10781;
    */
}

INNER {
    return "⨝";
    /* &#10781;
        */
}

LEFT {
    return "⟕";
    /* 	&#10197;
    */
}

RIGHT {
    return "⟖";
    /* 	&#10198;
    */
}

FULL {
    return "⟗";
    /* &#10199;
    */
}

ALL {
    return thisTreeFunction([thisGrammarNode.#1], thisResultNode);
}



TreeFunction currentColumnName

selectColumn.AND {
    if(hasOPTChild(thisGrammarNode.#2)) {
        return thisTreeFunction([thisGrammarNode.#2], thisResultNode);
    }
    else {
        return thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    };
}

selectColumnRename.AND {
    return getTokenString(thisGrammarNode.#2);
}

column.AND {
    var columnMap = createColumnMapFromNode(thisGrammarNode);
    return columnMap g "columnName";
}

ALL {
    var hel = null;
    forEach(child in children(thisGrammarNode)) {
        var current = thisTreeFunction([child], thisResultNode);
        if(current notEquals null) {
            return current;
        };
    };
    return null;
}

TreeFunction getQualifiedName

column.AND map{
    thisTreeFunction([thisGrammarNode.#1], thisResultNode, [map]);
    thisTreeFunction([thisGrammarNode.#2], thisResultNode, [map]);


}



ALL map{
   if(hasNoChildren(thisGrammarNode)) {
    return null;
   } else {
    return thisTreeFunction([thisGrammarNode.#1], thisResultNode, [map]);
   };
}



TreeFunction getTokenString_t



queryExcept {
    return "EXCEPT";
}

queryIntersect {
    return "INTERSECT";
}

queryUnion {
    return "UNION";
}

some {
    return "ANY";
}

sign {
    var hi = thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    if(equals(hi, "+")) {
        return "POSITIVE";
    }
    else {
        return "NEGATIVE";
    };
}

notequalsoperator {
    return "!=";
}

SLOPETOKEN {
    return slotNodeName(thisGrammarNode.#1);
}

NULL {
    return "NULL";
}

ALL {
    if(hasChildren(thisGrammarNode)) {
        return thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    };
}

TreeFunction constructGenericHTMLTree


isNamedAnd() {
    +#slotNodeName(thisGrammarNode) {
        forEach(child in children(thisGrammarNode)) {
            thisTreeFunction([thisGrammarNode], thisResultNode);
        };
    };
}


ALL {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([thisGrammarNode], thisResultNode);
    };
}

TreeFunction constructConcatString

OR.INDEX {
}

SLOPETOKEN {
    continue([thisGrammarNode], thisResultNode);
    +#slotNodeName(thisGrammarNode.#1);
}

ALL {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode);
    };
}



TreeFunction constructSpaces

SLOPETOKEN {
    if(hasChildren(thisResultNode)) {
        +#" ";
    };
}

TreeFunction constructStringFromParseTree

OR.INDEX environment{
}

SLOPETOKEN."," environment {
    +#",";
}


selectclause.AND environment {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode, [clone(environment)]);
    thisTreeFunction([thisGrammarNode.#2], thisResultNode, [clone(environment)]);

    thisTreeFunction([thisGrammarNode.#3], thisResultNode, [addedSpaceNumber(environment, 6)]);
}


columnTail.AND environment {
    +#".";
    +#getTokenString(thisGrammarNode.#2);
}

tableBracketReference.AND environment {
    +#" (";
    thisTreeFunction([thisGrammarNode.#2], thisResultNode,
                        [addedSpaceNumber(environment, 2)]);

    +#"\n";
    +#getSpaces(environment);
    +#" )";
}

bracketQuery.AND environment {
    +#" (";
    thisTreeFunction([thisGrammarNode.#2], thisResultNode,
                        [addedSpaceNumber(environment, 2)]);

    +#"\n";
    +#getSpaces(environment);
    +#" )";
}

whereclause.AND environment {
    +#"\n";
    +#getSpaces(environment);
    thisTreeFunction([thisGrammarNode.#1], thisResultNode, [clone(environment)]);

    thisTreeFunction([thisGrammarNode.#2], thisResultNode, [addedSpaceNumber(environment, 5)]);
}


fromclause.AND environment {
    +#"\n";
    +#getSpaces(environment);
    thisTreeFunction([thisGrammarNode.#1], thisResultNode, [clone(environment)]);

    thisTreeFunction([thisGrammarNode.#2], thisResultNode, [addedSpaceNumber(environment, 4)]);
    thisTreeFunction([thisGrammarNode.#3], thisResultNode, [addedSpaceNumber(environment, 4)]);
}

isAnyOf("tablereference0"
        "tablereference0InBrackets").AND environment {
    if(hasChildren(thisGrammarNode.#2)) {
        var maxCharSpace = getMaxCharSpace(thisGrammarNode.#2);
        +#concatTimes(" ", maxCharSpace);
        thisTreeFunction([thisGrammarNode.#1], thisResultNode, [addedSpaceNumber(environment, maxCharSpace)]);
        thisTreeFunction([thisGrammarNode.#2], thisResultNode, [addedSpaceNumber(environment, maxCharSpace)]);
    }
    else {
        thisTreeFunction([thisGrammarNode.#1], thisResultNode, [clone(environment)]);
    };
}

crossJoin.AND environment {
    var joinSpaces = minus(mapGet(environment,
                                 "spaceNumber"
                                ),
                          10
                         );

    if(smallerThan(joinSpaces, 0)) {
        joinSpaces = 0;
    };

    +#"\n";
    +#concatTimes(" ", joinSpaces);
    +#"CROSS JOIN";

    thisTreeFunction([thisGrammarNode.#3], thisResultNode, [clone(environment)]);
}

naturalJoin.AND environment {



    var joinOffsetString = "NATURAL";
    var setToken = constructSpacedString(thisGrammarNode.#2);
    if(notEquals(setToken, "")) {
        joinOffsetString = concat(joinOffsetString, " ");
        joinOffsetString = concat(joinOffsetString, setToken);
    };

    joinOffsetString = concat(joinOffsetString, " JOIN");

    var joinSpaces = minus(mapGet(environment,
                                 "spaceNumber"
                                ),
                          stringLength(joinOffsetString)
                         );

    if(smallerThan(joinSpaces, 0)) {
        joinSpaces = 0;
    };

    +#"\n";
    +#concatTimes(" ", joinSpaces);
    +#joinOffsetString;


    thisTreeFunction([thisGrammarNode.#4], thisResultNode, [clone(environment)]);


}

conditionalJoin.AND environment {

    var joinOffsetString = "";
    var setToken = constructSpacedString(thisGrammarNode.#1);
    if(notEquals(setToken, "")) {
        joinOffsetString = concat(joinOffsetString, " ");
        joinOffsetString = concat(joinOffsetString, setToken);
    };

    joinOffsetString = concat(joinOffsetString, " JOIN");

    var joinSpaces = minus(mapGet(environment,
                                 "spaceNumber"
                                ),
                          stringLength(joinOffsetString)
                         );

    if(smallerThan(joinSpaces, 0)) {
        joinSpaces = 0;
    };

    +#"\n";
    +#concatTimes(" ", joinSpaces);
    +#joinOffsetString;


    thisTreeFunction([thisGrammarNode.#3], thisResultNode, [clone(environment)]);

    thisTreeFunction([thisGrammarNode.#4], thisResultNode, [clone(environment)]);


}

onSpecification.AND environment{
    +#"\n";
    var spaceNum = minus(mapGet(environment, "spaceNumber"),
                       2);
    +#concatTimes(" ", spaceNum);
    +#"ON";
    thisTreeFunction([thisGrammarNode.#2], thisResultNode, [addedSpaceNumber(environment, spaceNum)]);
}






selectListTail.AND environment {
    +#",";
    +#"\n";
    +#getSpaces(environment);

    thisTreeFunction([thisGrammarNode.#2], thisResultNode, [environment]);
}

SLOPETOKEN.SELECT environment {
    +#"SELECT";
}

SLOPETOKEN.FROM environment{
    +#"FROM";
}

SLOPETOKEN.WHERE environment {
    +#"WHERE";
}


orAbleTerm.AND environment {
    var cloned = null;
    if(hasChildren(thisGrammarNode.#2)) {
        cloned = addedSpaceNumber(environment, 3);
        +#"   ";
    }
    else {
        cloned = clone(environment);
    };
    thisTreeFunction([thisGrammarNode.#1], thisResultNode, [cloned]);


    thisTreeFunction([thisGrammarNode.#2], thisResultNode, [clone(environment)]);
}

orTerm.AND environment {
    +#"\n";
    +#concatTimes(" ", mapGet(environment, "spaceNumber"));
    +#" OR";

   var added = addedSpaceNumber(environment, 3);
   thisTreeFunction([thisGrammarNode.#2], thisResultNode, [added]);
}

andAbleTerm.AND environment {

    var cloned = null;
    if(hasChildren(thisGrammarNode.#2)) {
        cloned = addedSpaceNumber(environment, 4);
        +#"    ";
    }
    else {
        cloned = clone(environment);
    };
    thisTreeFunction([thisGrammarNode.#1], thisResultNode, [cloned]);


    thisTreeFunction([thisGrammarNode.#2], thisResultNode, [clone(environment)]);

}

andTerm.AND environment {
    +#"\n";
    +#concatTimes(" ", mapGet(environment, "spaceNumber"));
    +#" AND";

    var added = addedSpaceNumber(environment, 4);
    thisTreeFunction([thisGrammarNode.#2], thisResultNode, [added]);
}

SLOPETOKEN."." environment{
    +#".";
}






SLOPETOKEN.ALL environment{
    +#" ";
    +#slotNodeName(thisGrammarNode);
}

ALL environment{
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode, [clone(environment)]);
    };
}



TreeFunction preSpace

ALL environment{
    var counter = 0;
    while(smallerThan(counter, mapGet(environment, "spaceNumber"))) {
        +#" ";
    };
}

TreeFunction getMaxCharSpace

OR.INDEX {
    return 0;
}

crossJoin.AND {
    return 9;
}

naturalJoin.AND {
    var str = "NATURAL";
    var setStr = constructSpacedString(thisGrammarNode.#2);
    if(notEquals(setStr, "")) {
        str = concat(str, " ");
        str = concat(str, setStr);
    };
    str = concat(str, " JOIN");
    return stringLength(str);

}

conditionalJoin.AND {
    var str = "";
    var setStr = constructSpacedString(thisGrammarNode.#1);
    if(notEquals(setStr, "")) {
        str = concat(str, " ");
        str = concat(str, setStr);
    };
    str = concat(str, " JOIN");
    return stringLength(str);
}

ALL {
    var max = 0;
    forEach(child in children(thisGrammarNode)) {
        var current = thisTreeFunction([child], thisResultNode);
        if(largerThan(current, max)) {
            max = current;
        };
    };
    return max;
}

TreeFunction generateRandomTree

OPT environment tags {
    var dist = generateRandomChances([thisGrammarNode], thisResultNode, [environment, tags]);
    var doOPT = randomBoolTrueProb(dist);

    if(doOPT) {
        insertOPTChildTemplate(thisGrammarNode);
        thisTreeFunction([thisGrammarNode.#1], thisResultNode, [environment, tags]);
    }
    else {
        removeOPTChild(thisGrammarNode);
    };
}

column.AND environment tags {
    var columnNameNode = null;
    var originTableNameNode = null;

    var temp_columnTail = #"" [
            thisTreeFunction([thisGrammarNode.#2], thisResultNode, [environment, tags]);
    ];

    /*var randCol = getRandomColumnFromEnvironment(environment);
*/
    var randCol = takeRandomElement(tags g "selectableColumns");


    if(hasNoChildren(temp_columnTail)) {
        columnNameNode = thisGrammarNode.#1.#1;
        originTableNameNode = null;

        setNodeName(columnNameNode,
            mapGet(randCol, "columnName"));
    } else {
        columnNameNode = slotNodeName(temp_columnTail.#1);
        originTableNameNode = thisGrammarNode.#1.#1;

        setNodeName(columnNameNode,
                mapGet(randCol, "columnName"));
        setNodeName(originTableNameNode,
                mapGet(randCol, "originTableName"));
    };



    thisGrammarNode putExtra ("columnMap", createColumnMapFromNode(thisGrammarNode));

}


allColumns.AND environment tags {
    thisGrammarNode putExtra ("environment", clone(environment));
}

isNamedAndChain("columnTail" "identifier") environment tags {
    +#thisGrammarNode.#1;
}

OR environment tags{
    var distribution = generateRandomChances([thisGrammarNode], thisResultNode, [environment, tags]);
    var randIndex = randomIndexFromWeightedList(distribution);

    insertORChildTemplateAt(thisGrammarNode, randIndex);
    thisTreeFunction([thisGrammarNode.#1], thisResultNode, [environment, tags]);
}


isNamedAndChain("tableLiteral" "identifier") environment tags {
    var tableName = generateRandomIdentifierExcept(mapKeys(environment));
    var tb = createNewTableWithRandomColumns(tableName);
    tb p ("type", "tableLiteral");
    environment p(tableName, tb);
    thisGrammarNode.#1 setNodeName tableName;
}

isNamedAndChain("selectColumnRename" "identifier") environment tags {
    var randColName = generateRandomColumnName(environment);
    thisGrammarNode.#1 setNodeName randColName;
}

isNamedAndChain("tableLiteral" "tableAsRename" "identifier") environment tags {
    var originalName =
        slotNodeName(getNamedAndWithName(thisGrammarNode, "tableLiteral").#1.#1.#1);

    var tbObject = environment g originalName;

    var newName = generateNewTableName(environment);

    tbObject p ("tableName", newName);
    environment mapRemove originalName;
    environment p (newName, tbObject);

    setNodeName(thisGrammarNode.#1, newName);
}

tableSubquery.AND environment tags {
    var map = newHashMap();

    var sfwBlockTags = clone(tags);
    sfwBlockTags p ("length", tags g "length" minus 1);
    var renameTags = clone(tags);
    renameTags p ("length", 1);

    thisTreeFunction([thisGrammarNode.#1], thisResultNode, [newHashMap(), sfwBlockTags ]);
    thisTreeFunction([thisGrammarNode.#2], thisResultNode, [environment, renameTags]);

}

isNamedAndChain("tableSubquery" "tableAsRename" "identifier") environment tags {
    var tableName = generateNewTableName(environment);

    var parent = getNamedAndWithName(thisGrammarNode, "tableSubquery").#1;

    var columnNameList = getColumnsFromSubquery(parent.#1);

    var columns = [];
    forEach(colName in columnNameList) {
        columns addElement createNewColumn(colName, tableName);
    };

    var tbObject = createNewTable(tableName, false, columns);
    tbObject p ("type", "tableSubquery");

    environment p (tableName, tbObject);

    setNodeName(thisGrammarNode.#1, tableName);
}

isScannerRule("identifier") environment tags {
    setNodeName(thisGrammarNode.#1, generateRandomLowercaseLetter());
}

isScannerRule("UnsignedNumber") environment tags {
    var randInt = randomInt(10);
    setNodeName(thisGrammarNode.#1, intToString(randInt));
}

hasAllCapsScannerRule() environment tags {
    setNodeName(thisGrammarNode.#1, scannerRuleName(thisGrammarNode));
}


tablereference0.AND environment tags {


    var nfoldChildren = generateNFOLDChildrenTF([thisGrammarNode.#2], thisResultNode,
            [environment, tags]);

    var allChildren = [thisGrammarNode.#1] concatList nfoldChildren;

    var length = tags g "length";
    length =  length minus length(allChildren);
    var distr = listFilled(length(allChildren), 1);




    var randSplit = splitNumberRandomlyExceptOne(length, length(allChildren));


    distr = distr listPlus randSplit;

/*
    var i = 1;
    while(i smallerEqualsThan length(randSplit)) {
        randSplit setElementAt(i, randSplit elementAt i plus 1);
        i = i plus 1;
    };
*/

   var i = 1;
   while(i smallerEqualsThan length(allChildren)) {
     var clonedTags = clone(tags);

     clonedTags p ("length", distr elementAt i);

     thisTreeFunction([allChildren elementAt i], thisResultNode, [environment, clonedTags]);
     i = i plus 1;
   };
}

NFOLD environment tags {
   var dist = generateRandomChances([thisGrammarNode], thisResultNode, [environment, tags]);

   var value1 = elementAt(dist, 1);
   var value2 = elementAt(dist, 2);

   generateNFOLDChildren(thisGrammarNode, value1, value2);

   forEach(child in children(thisGrammarNode)) {
       thisTreeFunction([child], thisResultNode, [environment, tags]);
   };
}


selectList.AND environment tags {
    var clonedTags = clone(tags);
    var selectableColumns = null;

    /*= getAllColumnsFromEnvironment(environment);
    */

    var i = 1;
    while(i smallerEqualsThan 8) {
        selectableColumns = getAllColumnsFromEnvironmentAmountPerTable(environment, i);
        if(length(selectableColumns) largerEqualsThan 8) {
            i = 9;
        }
        else {
            i = i plus 1;
        };
    };

    /*clonedTags p ("selectableColumns", takeRandomElementsUpTo(selectableColumns, 8));
    */


    clonedTags p ("selectableColumns", takeRandomElementsUpTo(selectableColumns, tags g "length"));






    var nfoldChildren = generateNFOLDChildrenTF([thisGrammarNode.#2], thisResultNode, [environment, clonedTags]);

    var allChildren = [thisGrammarNode.#1] concatList nfoldChildren;
    var clonedEnv = clone(environment);


    forEach(child in allChildren) {
        thisTreeFunction([child], thisResultNode, [clonedEnv, clonedTags]);
    };
}


selectColumn.AND environment tags {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode, [environment, tags]);
    };

    thisGrammarNode putExtra ("selectColumnMap", createSelectColumnMap(thisGrammarNode));

}

sfwBlock.AND environment tags {
    var clonedWHERETags = clone(tags);
    var clonedFROMTags = clone(tags);
    var clonedSELECTTags = clone(tags);

    var length = tags g "length";


    var stats = createRecursionStatMap(thisGrammarNode);
    if(stats g "sfwBlock" equals 1) {
        clonedSELECTTags p ("tags", 1);
    };

    if(length equals 2) {
        clonedSELECTTags p("length", 1);
        clonedFROMTags p("length", 1);
    }
    else {

        length = length minus 2;

        var takeForSelect = min(randomIntFromRange(0, 7), length);
        var takeForFrom = length minus takeForSelect;


        clonedSELECTTags p ("length", takeForSelect plus 1);
        clonedFROMTags p ("length", takeForFrom plus 1);


    };








    thisTreeFunction([thisGrammarNode.#2], thisResultNode, [environment, clonedFROMTags]);
    thisTreeFunction([thisGrammarNode.#3], thisResultNode, [environment, clonedWHERETags]);

    thisTreeFunction([thisGrammarNode.#4], thisResultNode, [environment, tags]);
    thisTreeFunction([thisGrammarNode.#5], thisResultNode, [environment, tags]);
    thisTreeFunction([thisGrammarNode.#6], thisResultNode, [environment, tags]);

    thisTreeFunction([thisGrammarNode.#1], thisResultNode, [environment, clonedSELECTTags]);
}

ALL environment tags{
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode, [environment, tags]);
    };
}

TreeFunction generateRandomChancesSQL

isNamedAndChild("selectColumn" "OPT") environment tags {
    /* selectColumnRename?*/

    return d(0,5);

}

isNamedAndChild("selectColumnRename" "OPT") environment tags {
    /* AS? (keyword) */
    return d(0,0);
}


ALL environment tags{
    return continue([thisGrammarNode], thisResultNode, [environment, tags]);
}




TreeFunction generateRandomChances

sfwBlock.AND.#2 environment tags{
    /*fromClause?
    */
    return d(1,0);
}


isNamedAndChild("tablereference" "NFOLD") environment tags {
    /*tablereferenceComma* */
    return [0, 0];
}

isNamedAndChild("tablereference0" "NFOLD") environment tags {
    /*  join*   */




   var currentMax = maxJoinCount minus currentJoinCount;
   var rand = min(4,min(randomIntFromRange(0, currentMax), tags g "length" minus 1));


   currentJoinCount = currentJoinCount plus rand;


   return [rand, rand];

}



isNamedAndChild("tablereference1" "OR") environment tags {

    var onlyBrackets = [0, /* tableLiteral */
                        1]; /* tableBracketExpression */

    var okBrackets = [1, /* tableLiteral */
                      1]; /* tableBracketExpression */

    var tooManyBrackets = [1, /* tableLiteral */
                          0]; /* tableBracketExpression */


    if(tags g "length" equals 1) {
        return tooManyBrackets;
    }
    else {
        return onlyBrackets;
    };

}

sfwBlock.AND.#3 environment tags{
    /*whereClause?
    */
    return d(0,0);
}

/*
never generate other clauses
*/
isNamedAndChild("sfwBlock" "OPT") environment tags{
    return d(0,0);
}

isNamedAndChild("allOrList" "OR") environment tags {
    return [0,   /* allColumns*/
            1];  /* selectList */
}



isNamedAndChild("selectList" "NFOLD") environment tags {

    var stats = createRecursionStatMap(thisGrammarNode);
    if(stats g "sfwBlock" equals 1) {
        return [0,0];
    }
    else {
        var selCol = tags g "length" minus 1;
        return [selCol, selCol];
    };


}



isNamedAndChild("selectElement" "OR") environment  tags {
    return [0, /*tableAll*/
            1  /*selectColumn*/];
}


isNamedAndChild("valueexpr" "OR") environment  tags {
    return [0, /* functionspec */
            0, /* dateliteral */
            0, /* timeliteral */
            0, /* timestampliteral */
            0, /* intervalliteral */
            0, /* identifier stringLiteralQuote */
            0, /* stringLiteralQuote */
            0, /* NULL */
            0, /* TRUE*/
            0, /* FALSE */
            0, /* UNKNOWN */
            1, /* column */
            0, /* number */
            0, /* casespecification */
            0, /* castspecification */
            0, /* extractspec */
            0, /* "\(" setOpableQuery "\)" */
            0, /* "\(" orAbleTerm "\)" */
            0];/* rowvalueconstructor */
}

isNamedAndChild("valueexpr" "OPT") environment tags {
    return d(0,0); /*sign?*/
}


isNamedAndChild("column" "OPT") environment tags {
    /* columnTail? */
    return d(1,0);
}

isNamedAndChild("booleanfactor" "OPT") environment tags {
    /* NOT? */
    return d(0,0);
}

isNamedAndChild("booleantest" "OPT") environment tags {
    return d(0,0); /* booleantestOp */
}

isNamedAndChild("booleanprimary" "OR") environment tags {
    return [0,  /* exists    */
            1]; /* predicate */
}

isNamedAndChild("predicate" "OPT") environment tags {
    return d(0,0); /* all the different predicate parts */
}

isNamedAndChild("predicate" "OR") environment tags {

    return [1, /*comparison*/
            0, /*between*/
            0, /*in*/
            0, /*like*/
            0, /*nullcheck*/
            0,  /*match*/
            0, /*overlaps*/
            0, /*quantifiedcomparison*/
            0];/*quantifiedlike*/
}


/* ...OR...*/
isNamedAndChild("orAbleTerm" "NFOLD") environment tags {
    return [0, 0];
}



/*...AND...*/
isNamedAndChild("andAbleTerm" "NFOLD")  environment tags {
    return [0, 0];
}

isNamedAndChild("selectclause" "OPT") environment tags {
    return d(0,0);
}

isNamedAndChild("bracketableQuery" "OR") environment tags {
    return [0, /* "\(" setOpableQuery "\)" */
            1]; /* sfwBlock */
}

isNamedAndChild("setOpableQuery" "NFOLD") environment tags {
    return [0, 0];
}


isNamedAndChild("join" "OR") environment tags {
    return [1, /* crossJoin */
            0, /* naturalJoin */
            0]; /* conditionalJoin */
}

isNamedAndChild("tableSubquery" "OPT") environment tags {
                      /* tableAsRename? */
     return d(1,0);
}

isNamedAndChild("tableLiteral" "OPT") environment tags {
                      /* tableAsRename? */
     return d(0,0);
}


tableAsRename.AND.#1 environment tags {
    /* AS? (keyword) */
    return d(0,0);
}

tableAsRename.AND.#3 environment tags {
    return d(0,0);
}




isNamedAndChild("tableBracketExpression" "OR") environment tags {
    return [1, /* tableLiteral */
            0 /* tableBracketExpression */
           ];
}

OR environment tags {
   return orEqualDistribution(thisGrammarNode);
}

OPT environment tags {
    return d(0,5);
}

isNFOLD_0() environment tags {
    return [0, 0];
}

isNFOLD_1() environment tags {
    return [1, 1];
}



TreeFunction getColumnsFromSubquery

sfwBlock.AND columnList {
    var hasAllColumns = thisTreeFunction([thisGrammarNode.#1], thisResultNode, [columnList]);

    if(equals(hasAllColumns,
              true)) {
        thisTreeFunction([thisGrammarNode.#2], thisResultNode, [columnList]);
        return false;
    };

}

allColumns.AND columnList {
    /*print("all columns from subquery");
    print(mapToPrettyString(thisGrammarNode getExtra "environment"));
    */

    forEach(table in mapValues(thisGrammarNode getExtra "environment")) {
        forEach(column in table g "columns") {
            columnList addElement b(column g "columnName");
        };
    };
    return true;
}

selectColumn.AND columnList {
    if(hasOPTChild(thisGrammarNode.#2)) {
        return thisTreeFunction([thisGrammarNode.#2], thisResultNode, [columnList]);
    }
    else {
        return thisTreeFunction([thisGrammarNode.#1], thisResultNode, [columnList]);
    };
}

selectColumnRename.AND columnList {
    columnList addElement getTokenString(thisGrammarNode.#2);
    return false;
}

column.AND columnList {
    if(not(hasOPTChild(thisGrammarNode.#2))) {
        columnList addElement getTokenString(thisGrammarNode.#1);
    }
    else {
        thisTreeFunction([thisGrammarNode.#2], thisResultNode, [columnList]);
    };
    return false;
}

columnTail.AND columnList {
        columnList addElement getTokenString(thisGrammarNode.#2);
        return false;
}

ALL columnList {
    var ret = false;
    forEach(childNode in children(thisGrammarNode)) {
        var current = thisTreeFunction([childNode], thisResultNode, [columnList]);
        if(current equals true) {
            ret = true;
        };
    };
    return ret;
}

TreeFunction allColumnsEndWell

selectColumn.AND {
    var path = getSelectPath(thisGrammarNode);
    if(lastElement(path) getExtra "fromTableLiteral" equals null) {
        print(prettyPrint(lastElement(path)));
        return false;
    }
    else {
        return true;
    };
}

ALL {
    var ret = true;
    forEach(child in children(thisGrammarNode)) {
        var current = thisTreeFunction([child], thisResultNode);
        if(current equals false) {
            ret = false;
        };
    };
    return ret;
}

TreeFunction createSelectColumnMap

sfwBlock.AND {
    return thisTreeFunction([thisGrammarNode.#1], thisResultNode);
}

selectColumn.AND {
    var originColumn = createColumnMapFromNode(thisGrammarNode.#1);

    var renamedAs = thisTreeFunction([thisGrammarNode.#2], thisResultNode);

    return m("originColumn", originColumn,
                "renamedAs", renamedAs,
                     "node", thisGrammarNode);

}

selectColumnRename.AND {
    return getTokenString(thisGrammarNode.#2);
}

ALL {
    var ret = null;
    forEach(child in children(thisGrammarNode)) {
    var current = thisTreeFunction([child], thisResultNode);
        if(current notEquals null) {
            ret = current;
        };
    };
    return ret;
}

TreeFunction getFirstSelectPath

sfwBlock.AND {
    return thisTreeFunction([thisGrammarNode.#1], thisResultNode);
}

selectColumn.AND {
    return getSelectPath(thisGrammarNode);
}

ALL {
    var ret = null;
    forEach(child in children(thisGrammarNode)) {
        var current = thisTreeFunction([child], thisResultNode);
        if(current notEquals null) {
            return current;
        };
    };
    return null;
}

TreeFunction getSelectPath

sfwBlock.AND {
    return thisTreeFunction([thisGrammarNode.#1], thisResultNode);
}

selectColumn.AND {
    var path =[];

    var current = thisGrammarNode;
    while(current notEquals null) {
        path addElement current;
        current = current getExtra "from";
    };

    return path;
}

ALL {
    var ret = null;
    forEach(child in children(thisGrammarNode)) {
        var current = thisTreeFunction([child], thisResultNode);
        if(current notEquals null) {
            ret = current;
        };
    };
    return ret;
}

TreeFunction pathDepth


sfwBlock.AND {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
}

selectColumn.AND {
    var currentCol = thisGrammarNode;
    while(currentCol getExtra "from" notEquals null) {
        currentCol = currentCol getExtra "from";
    };

    var stats = createRecursionStatMap(currentCol);
    thisResultNode setNodeName b(stats g "sfwBlock" minus 1);

}

ALL {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode);
    };
}

TreeFunction pathRenameCount


sfwBlock.AND {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
}

selectColumn.AND {
    var currentNode = thisGrammarNode;
    while(currentNode notEquals null) {
        if(hasOPTChild(currentNode.#2)) {
            thisResultNode setNodeName
                b(slotNodeName(thisResultNode) plus 1);
        };
        currentNode = currentNode getExtra "from";
    };
}

ALL {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode);
    };
}


TreeFunction setColumnMaps

selectColumn.AND {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode);
    };

    thisGrammarNode putExtra ("selectColumnMap", createSelectColumnMap(thisGrammarNode));

}

column.AND {
    thisGrammarNode putExtra ("columnMap", createColumnMapFromNode(thisGrammarNode));
}

ALL {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode);
    };
}

TreeFunction setFromExtras

sfwBlock.AND {
        ->(setFromExtras_sfwBlock)([thisGrammarNode.#1, thisGrammarNode], thisResultNode);
        thisTreeFunction([thisGrammarNode.#2], thisResultNode);
}

ALL {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode);
    };
}

TreeFunction setFromExtras_sfwBlock

selectColumn.AND sfw:sfwBlock.AND {
    var selectColumnMap = createSelectColumnMap(thisGrammarNode);
    ->(setFromExtras_lookForSelectedColumn)([sfw.#2], thisResultNode, [selectColumnMap]);
}

ALL sfw:ALL {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child, sfw], thisResultNode);
    };
}



TreeFunction setFromExtras_lookForSelectedColumn

tableSubquery.AND selectColumnMap {
    var originTableName = selectColumnMap g "originColumn" g "originTableName";

    var resCurrentName = #"";
    thisTreeFunction([thisGrammarNode.#2], resCurrentName, [selectColumnMap]);
    var currentName = slotNodeName(resCurrentName);


    if(originTableName equals currentName) {
        thisTreeFunction([thisGrammarNode.#1], thisResultNode, [selectColumnMap]);
    };
}

tableAsRename.AND selectColumnMap {
    thisResultNode setNodeName getTokenString(thisGrammarNode.#2);
}


sfwBlock.AND selectColumnMap {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode, [selectColumnMap]);
}

selectColumn.AND selectColumnMap {
    var targetSelectColumnMap = thisGrammarNode getExtra "selectColumnMap";
    var originName = selectColumnMap g "originColumn" g "columnName";
    var targetName = getFinalColumnName(targetSelectColumnMap);
    if(originName equals targetName) {
        var selectingNode = selectColumnMap g "node";
        selectingNode putExtra ("from", thisGrammarNode);
        thisGrammarNode addSelectedBy selectingNode;
    };
}

tableLiteral.AND selectColumnMap {
    var originTableName = selectColumnMap g "originColumn" g "originTableName";
    var targetTableName = getTokenString(thisGrammarNode.#1);
   if(originTableName equals targetTableName) {
        var selectingNode = selectColumnMap g "node";
        selectingNode putExtra ("fromTableLiteral", thisGrammarNode);
        thisGrammarNode addSelectedBy selectingNode;
   };

}

ALL selectColumnMap {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode, [selectColumnMap]);
    };
}




TreeFunction setNodeMaps

selectColumn.AND {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode);
    };

    thisGrammarNode putExtra ("selectColumnMap", createSelectColumnMap(thisGrammarNode));

}

column.AND {
    thisGrammarNode putExtra ("columnMap", createColumnMapFromNode(thisGrammarNode));
}

ALL {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode);
    };
}

TreeFunction amountOfStuff

selectColumn.AND {
    addOneToResultNode(thisResultNode);
}

tableLiteral.AND {
    addOneToResultNode(thisResultNode);
}

tableAsRename.AND {
    addOneToResultNode(thisResultNode);
}

ALL {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode);
    };
}



TreeFunction amountOfSubqueries


sfwBlock {
    currentAmountOfSubqueries =
    currentAmountOfSubqueries plus 1;
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode);
    };
}

ALL {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode);
    };
}

TreeFunction maxSubqueryDepth

sfwBlock.AND {
    var stats = createRecursionStatMap(thisGrammarNode);
    var currentMax = slotNodeName(thisResultNode);

    if( stats g "sfwBlock" largerThan currentMax) {
        thisResultNode setNodeName b(stats g "sfwBlock");
    };

    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode);
    };
}

ALL {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode);
    };
}

TreeFunction namedAndListWithName

ALL name{
    if(thisGrammarNode slotNodeNameEquals name) {
        slotNodeName(thisResultNode) addElement thisGrammarNode;
    }
    else {
        forEach(child in children(thisGrammarNode)) {
            thisTreeFunction([child], thisResultNode, [name]);
        };
    };
}

TreeFunction ruleNameAmount

ALL name{
    if(slotNodeName(thisGrammarNode) equals name) {
        thisResultNode setNodeName
        b(slotNodeName(thisResultNode) plus 1);
    };
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode, [name]);
    };
}



TreeFunction scannerRuleNameAmount

ALL name {
    if(isScannerRule(thisGrammarNode, name)) {
        thisResultNode setNodeName
        b(slotNodeName(thisResultNode) plus 1);
    };
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode, [name]);
    };
}



TreeFunction setPathDepth

tableSubquery.AND depth {
    var stats = createRecursionStatMap(thisGrammarNode);
    var currentDepth = stats g "sfwBlock";
    if(currentDepth equals depth) {

    };
}

ALL depth {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode, [depth]);
    };
}

TreeFunction _fromExtras

selectColumn.AND {
    var from = thisGrammarNode getExtra "from";
    var selectedBy = thisGrammarNode getExtra "selectedBy";
    print(prettyPrint(thisGrammarNode));
    print("from:" concat prettyPrint(from));
    print("selectedBy:" concat toString(selectedBy));
}




ALL {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode);
    };
}

TreeFunction createColumnMapFromNode

column.AND map {
    if(hasOPTChild(thisGrammarNode.#2)) {
        mapPut(map, "originTableName",
                constructSpacedString(thisGrammarNode.#1));
        thisTreeFunction([thisGrammarNode.#2], thisResultNode, [map]);
    } else {
        mapPut(map, "columnName",
        constructSpacedString(thisGrammarNode.#1));
    };

    map p ("node", thisGrammarNode);

}

columnTail.AND map {
    mapPut(map, "columnName",
           constructSpacedString(thisGrammarNode.#2));
}

ALL map{
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode, [map]);
    };
}

TreeFunction createTableMapFromNode

tableLiteral.AND map {
    if(hasOPTChild(thisGrammarNode.#2)) {
        thisTreeFunction([thisGrammarNode.#2], thisResultNode, [map]);
    }
    else {
        mapPut(map,
               "tableName",
                constructSpacedString(thisGrammarNode.#1));
    };
}


tableAsRename.AND {
        mapPut(map,
               "tableName",
                constructSpacedString(thisGrammarNode.#2));
}

ALL map{
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode, [map]);
    };
}




TreeFunction getSubqueryColumnNames

sfwBlock.AND {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
}

ALL {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode);
    };
}''');
exampleStrings.add('''SELECT hi''');
  return exampleStrings;
}
}
