import '../../../wtf_tols/TOLSFunctionFamily.dart';
import '../../../wtf_tols/wtf.dart';
import '../../../../lib/slot/runtime/objects/SLOTTreeNode.dart';
import '../../../../lib/lib/parser/ResultObject.dart';


nAND(String name) => n(name).n("AND");

main() {
  ResultFunctionFamily family = new ResultFunctionFamily({
    nAND("queryStatement") : (TOLSResultFrame<SQL> me) {
      ResultObject<SelectStatement> selectStatement = new ResultObject(null);
      callThisOnChildren(me, selectStatement);
      me.setResult(new SQL()..statement = selectStatement.getResult());
    },

    n("queryExceptOrUnion") : (TOLSResultFrame me) {
    },

    n("queryIntersect") : (TOLSResultFrame me) {
    },

    n("sfwBlock").AND : (TOLSResultFrame me) {
      ResultObject<FromClause> fromClause;
      me.treeFunction(me.grammarNode.c(1), me.resultObject);
      me.treeFunction(me.grammarNode.c(0), me.resultObject);
    },

    ALL : (TOLSResultFrame me) {
      callThisOnChildren(me, me.resultObject);
    }




  });

 // family.runSingle(grammarNode, resultObject)
}

class FromClause {
}




callThisOnChildren(TOLSResultFrame me, ResultObject resultObject) {
  for(SLOTTreeNode child in me.grammarNode.children.elements) {
    me.treeFunction(child, resultObject);
  }
}



class SQL {
  SelectStatement statement;
}






class SelectStatement {

}