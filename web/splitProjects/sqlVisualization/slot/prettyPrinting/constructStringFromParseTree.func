### VARDEF

var slopeTokenString;

### FUNCTION

constructStringFromParseTree node {
    var stringRoot = #"";
    ->(constructStringFromParseTree | preSpace)([node], stringRoot, [createPrettyPrintEnvironment(0)]);
    return constructDFFText(stringRoot);
}


createPrettyPrintEnvironment spaceNumber {
    return newHashMap(["spaceNumber", spaceNumber]);
}

concatTimes string times {
    var newStr = "";
    var count = 0;
    while(smallerThan(count, times)) {
        newStr = concat(newStr, string);
        count = plus(count, 1);
    };
    return newStr;
}

addedSpaceNumber map1 spaceNumber {
    return addedMap(map1, newHashMap(["spaceNumber", spaceNumber]));
}


/*
Add all values from map2 to all values of map1 of the same key-value-pair
and return the new map as a copy.
*/

addedMap pmap1 map2 {
    var map1 = clone(pmap1);
    forEach(pair in mapPairs(map2)) {

        var key = elementAt(pair, 1);
        mapPut(map1,
               key,
               plus(mapGet(map1, key),
                    mapGet(map2, key))
        );

    };
    return map1;
}

getSpaces environment {
    return concatTimes(" ", mapGet(environment, "spaceNumber"));
}


isAnyOf node name1 name2 {
    if(or(slotNodeNameEquals(node, name1),
          slotNodeNameEquals(node, name2)
          )
    ) {
        return true;
    }
    else {
        return false;
    };
}

### TREE

TreeFunction constructStringFromParseTree

OR.INDEX environment{
}

SLOPETOKEN."," environment {
    +#",";
}


selectclause.AND environment {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode, [clone(environment)]);
    thisTreeFunction([thisGrammarNode.#2], thisResultNode, [clone(environment)]);

    thisTreeFunction([thisGrammarNode.#3], thisResultNode, [addedSpaceNumber(environment, 6)]);
}


columnTail.AND environment {
    +#".";
    +#getTokenString(thisGrammarNode.#2);
}

tableBracketReference.AND environment {
    +#" (";
    thisTreeFunction([thisGrammarNode.#2], thisResultNode,
                        [addedSpaceNumber(environment, 2)]);

    +#"\n";
    +#getSpaces(environment);
    +#" )";
}

bracketQuery.AND environment {
    +#" (";
    thisTreeFunction([thisGrammarNode.#2], thisResultNode,
                        [addedSpaceNumber(environment, 2)]);

    +#"\n";
    +#getSpaces(environment);
    +#" )";
}

whereclause.AND environment {
    +#"\n";
    +#getSpaces(environment);
    thisTreeFunction([thisGrammarNode.#1], thisResultNode, [clone(environment)]);

    thisTreeFunction([thisGrammarNode.#2], thisResultNode, [addedSpaceNumber(environment, 5)]);
}


fromclause.AND environment {
    +#"\n";
    +#getSpaces(environment);
    thisTreeFunction([thisGrammarNode.#1], thisResultNode, [clone(environment)]);

    thisTreeFunction([thisGrammarNode.#2], thisResultNode, [addedSpaceNumber(environment, 4)]);
    thisTreeFunction([thisGrammarNode.#3], thisResultNode, [addedSpaceNumber(environment, 4)]);
}

isAnyOf("tablereference0"
        "tablereference0InBrackets").AND environment {
    if(hasChildren(thisGrammarNode.#2)) {
        var maxCharSpace = getMaxCharSpace(thisGrammarNode.#2);
        +#concatTimes(" ", maxCharSpace);
        thisTreeFunction([thisGrammarNode.#1], thisResultNode, [addedSpaceNumber(environment, maxCharSpace)]);
        thisTreeFunction([thisGrammarNode.#2], thisResultNode, [addedSpaceNumber(environment, maxCharSpace)]);
    }
    else {
        thisTreeFunction([thisGrammarNode.#1], thisResultNode, [clone(environment)]);
    };
}

crossJoin.AND environment {
    var joinSpaces = minus(mapGet(environment,
                                 "spaceNumber"
                                ),
                          10
                         );

    if(smallerThan(joinSpaces, 0)) {
        joinSpaces = 0;
    };

    +#"\n";
    +#concatTimes(" ", joinSpaces);
    +#"CROSS JOIN";

    thisTreeFunction([thisGrammarNode.#3], thisResultNode, [clone(environment)]);
}

naturalJoin.AND environment {



    var joinOffsetString = "NATURAL";
    var setToken = constructSpacedString(thisGrammarNode.#2);
    if(notEquals(setToken, "")) {
        joinOffsetString = concat(joinOffsetString, " ");
        joinOffsetString = concat(joinOffsetString, setToken);
    };

    joinOffsetString = concat(joinOffsetString, " JOIN");

    var joinSpaces = minus(mapGet(environment,
                                 "spaceNumber"
                                ),
                          stringLength(joinOffsetString)
                         );

    if(smallerThan(joinSpaces, 0)) {
        joinSpaces = 0;
    };

    +#"\n";
    +#concatTimes(" ", joinSpaces);
    +#joinOffsetString;


    thisTreeFunction([thisGrammarNode.#4], thisResultNode, [clone(environment)]);


}

conditionalJoin.AND environment {

    var joinOffsetString = "";
    var setToken = constructSpacedString(thisGrammarNode.#1);
    if(notEquals(setToken, "")) {
        joinOffsetString = concat(joinOffsetString, " ");
        joinOffsetString = concat(joinOffsetString, setToken);
    };

    joinOffsetString = concat(joinOffsetString, " JOIN");

    var joinSpaces = minus(mapGet(environment,
                                 "spaceNumber"
                                ),
                          stringLength(joinOffsetString)
                         );

    if(smallerThan(joinSpaces, 0)) {
        joinSpaces = 0;
    };

    +#"\n";
    +#concatTimes(" ", joinSpaces);
    +#joinOffsetString;


    thisTreeFunction([thisGrammarNode.#3], thisResultNode, [clone(environment)]);

    thisTreeFunction([thisGrammarNode.#4], thisResultNode, [clone(environment)]);


}

onSpecification.AND environment{
    +#"\n";
    var spaceNum = minus(mapGet(environment, "spaceNumber"),
                       2);
    +#concatTimes(" ", spaceNum);
    +#"ON";
    thisTreeFunction([thisGrammarNode.#2], thisResultNode, [addedSpaceNumber(environment, spaceNum)]);
}






selectListTail.AND environment {
    +#",";
    +#"\n";
    +#getSpaces(environment);

    thisTreeFunction([thisGrammarNode.#2], thisResultNode, [environment]);
}

SLOPETOKEN.SELECT environment {
    +#"SELECT";
}

SLOPETOKEN.FROM environment{
    +#"FROM";
}

SLOPETOKEN.WHERE environment {
    +#"WHERE";
}


orAbleTerm.AND environment {
    var cloned = null;
    if(hasChildren(thisGrammarNode.#2)) {
        cloned = addedSpaceNumber(environment, 3);
        +#"   ";
    }
    else {
        cloned = clone(environment);
    };
    thisTreeFunction([thisGrammarNode.#1], thisResultNode, [cloned]);


    thisTreeFunction([thisGrammarNode.#2], thisResultNode, [clone(environment)]);
}

orTerm.AND environment {
    +#"\n";
    +#concatTimes(" ", mapGet(environment, "spaceNumber"));
    +#" OR";

   var added = addedSpaceNumber(environment, 3);
   thisTreeFunction([thisGrammarNode.#2], thisResultNode, [added]);
}

andAbleTerm.AND environment {

    var cloned = null;
    if(hasChildren(thisGrammarNode.#2)) {
        cloned = addedSpaceNumber(environment, 4);
        +#"    ";
    }
    else {
        cloned = clone(environment);
    };
    thisTreeFunction([thisGrammarNode.#1], thisResultNode, [cloned]);


    thisTreeFunction([thisGrammarNode.#2], thisResultNode, [clone(environment)]);

}

andTerm.AND environment {
    +#"\n";
    +#concatTimes(" ", mapGet(environment, "spaceNumber"));
    +#" AND";

    var added = addedSpaceNumber(environment, 4);
    thisTreeFunction([thisGrammarNode.#2], thisResultNode, [added]);
}

SLOPETOKEN."." environment{
    +#".";
}






SLOPETOKEN.ALL environment{
    +#" ";
    +#slotNodeName(thisGrammarNode);
}

ALL environment{
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode, [clone(environment)]);
    };
}



TreeFunction preSpace

ALL environment{
    var counter = 0;
    while(smallerThan(counter, mapGet(environment, "spaceNumber"))) {
        +#" ";
    };
}