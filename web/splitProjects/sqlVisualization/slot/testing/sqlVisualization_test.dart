import 'dart:io';
import 'package:path/path.dart';
import 'package:test/test.dart';

import '../../../../../lib/slot/runtime/objects/SLOTNull.dart';
import '../../../../../lib/slot/runtime/builtInFunction/ANY/equals.dart';
import '../../../../../lib/slot/runtime/SLOTObject.dart';
import '../../../../../lib/slot/runtime/frames/SLOTStackFrame.dart';
import '../../dartExperiment/projectStuff/projectStuff.dart';


main() {
  initProject();
  project.slotProgram.runtime.builtInFunctions["expect"] = (SLOTStackFrame frame, [List<SLOTObject> params]) {
    expect(equalsSlot(params[0], params[1]), true, reason: "${params[0].valueToString()} != ${params[1].valueToString()}");
    frame.pushData(SLOTNull.instance);
  };
  new Directory(Directory.current.path + "\\web\\splitProjects\\sqlVisualization")
      .listSync(recursive: true, followLinks: false)
      .where((FileSystemEntity entity) {
        return entity is File && basename(entity.path).endsWith(".test");
      })
      .forEach((FileSystemEntity entity) {
        var fileName = basename(entity.path);
        var testName = fileName.substring(0, fileName.length-5);
        test(fileName, () {
          project.runFunctionUnwrap(testName);
        });
      });

  var hi = {
    "Hi" : '''
    SELECT hi
    FROM
    '''

  };
}