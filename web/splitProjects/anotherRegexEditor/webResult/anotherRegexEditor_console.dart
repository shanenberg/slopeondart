import '../../../html/examples/SLOTHTMLToConsoleConverter.dart';
import '../../../../lib/slot/runtime/SLOTProject.dart';
import 'anotherRegexEditor.slp.dart';

main() {
  SLOTProject project =
  new SLOTHTMLToConsoleConverter().constructProject(new anotherRegexEditor());
  var slotResult = project.runProject();
}
