import '../../../../lib/slot/runtime/SLOTHTMLProject.dart';
import '../../../html/examples/SLOTProjectConstructor.dart';
import 'anotherRegexEditor.slp.dart';

main() {

  SLOTHTMLProject project =
  new SLOTProjectConstructor().constructHTMLProject(new anotherRegexEditor());
  var slotResult = project.runProject();

}
