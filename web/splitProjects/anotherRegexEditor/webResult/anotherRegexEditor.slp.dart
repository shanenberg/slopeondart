/***********************************************************
I am purely generated!!!! Don't touch me, touch the slp files 
***************************************************************/
      import '../../../html/examples/HTMLExample.dart';
class anotherRegexEditor extends HTMLExample {
   List<String> getExampleProjectStrings() {
  List<String> exampleStrings = new List<String>();
exampleStrings.add('''


Dings
Rules: start



/*start => "A"*.
*/

/*start => ("1" "[2-7]"+ "A")*.
*/

/*start => ("1"* "[2-7]"* "A")*.
*/

/*start => "1" "2"+.
*/
/*start => ("[A-Z]"| "[a-z]")*.
*/




/*start => "[A-Z]"*  "[0-9]"*.
*/



start => (  "J" "H" ("A"|"B")+   )+.




/*start => "Bla+".
*/

/*start => "([A-Z]|[a-z]|_)([A-Z]|[a-z]|_|[0-9])*".
*/





/*start => ("E"|"D") "S" "[0-9]" "[0-9]" "[0-9]" "[0-9]"
                       "[0-9]" "[0-9]" "[0-9]".
*/


       /* ("A" "B")**/


/*"A" "B"? "C"* "D"+  "[1-9]" ("F" "^Z"  "^Z"+ "[H-I]"+ ("J"|"K")+)?.
*/




/*("F" "^Z"+ "[H-I]"+ ("J"|"K")+)? .*/


/*start => (("0"|"1") "A")+.
*/


/*start => ("[A-Z]"|"[a-z]"|"_")("[a-z]"|"[A-Z]"|"_"|"[0-9]")*.
*/

/*
start => "A"*.
*/

/*start => "A"  ("1"|"2")?  (("1"|"2") "3")?     "[0-9]" ("J" ("A"|"B")* "C")? "^KLJ"+ .
*/

/*
start => "A"*.
*/




/*
start => "A"   ("[a-z]"|"[A-Z]"|"_")*    "[0-9]"?.
*/


/*
start => ("[A-Z]"*).
*/






''');
exampleStrings.add('''


load library util;
load library htmlConstruction;



var dffText;



var genericTreeGrammar;


/*
var genericTreeGrammar;
*/


var foundCurrentEditableNode;



var markedEditableList;





var htmlString;
var nodesToHTML;



var cursor;

var data;
var htmlTree;



var cursorIndexPath;
var cursor;



var currentRootGrammarNode;



var stateList;
var redoList;
var undoList;





var markedTextString;



addClass resultNode className {
    setNodeName(resultNode,
                    concatAll(slotNodeName(resultNode),
                              [" ", className]
                             )
               );
}

constructDFFText aNode {
    dffText = "";
    ->(constructDFFTextTree)([aNode], thisResultNode, ["ROOT"]);
     return dffText;
}



initConstructFromTreeString {
    genericTreeGrammar = parseSlopeGrammar("

        Generic
        Rules: treeNode

        treeNode => ~"#~" value ~"\\(~" treeNode* ~"\\)~".

        value => (identifier | number | boolean).

        identifier => IDENTIFIER.
        number => NUMBER.
        boolean => ~"true|false~".

        Scanner:

        NUMBER => ~"[1-9][0-9]*|0~".
        whitespace => SEPARATOR ~"\\n|\\r| ~" BOTTOM.
        IDENTIFIER => ~"([a-z]|[A-Z]|_)([a-z]|[A-Z]|[0-9]|_)*~" BOTTOM.

    ");
}

testConstructFromTreeString {
    var tree = constructFromTreeString("#SELECT()");
}

constructFromTreeString treeString {
    if(equals(treeString, null)) {
        return null;
    };
    var grammarTree = parseSlopeWordFromGrammar(treeString, genericTreeGrammar);
    var lolTree = ->(t_constructFromTreeString)([grammarTree], rootResultNode);
    return lolTree;
}




/*
initConstructFromTreeString {
    genericTreeGrammar = parseSlopeGrammar("

        Generic
        Rules: treeNode

        treeNode => ~"#~" value ~"\\(~" treeNode* ~"\\)~".

        value => (identifier | number | boolean).

        identifier => IDENTIFIER.
        number => NUMBER.
        boolean => ~"true|false~".

        Scanner:


        NUMBER => ~"[1-9][0-9]*~".

        whitespace => SEPARATOR ~"\\n|\\r| ~" BOTTOM.
        IDENTIFIER => ~"([a-z]|[A-Z]|_)([a-z]|[A-Z]|[0-9]|_)*~" BOTTOM.

    ");
}

testConstructFromTreeString {
    var tree = constructFromTreeString("#SELECT()");
    printString(constructTreeString(tree));
}

constructFromTreeString treeString {
    var grammarTree = parseSlopeWordFromGrammar(treeString, genericTreeGrammar);
    printString(constructTreeString(grammarTree));
    var root = #"";
    ->(constructFromTreeString | t_constructFromTreeString)([grammarTree], root);
    return root.#1;
}

*/



constructHTMLString aNode aNodeList {
    htmlString = "";
    ->(_constructHTMLString)([aNode], rootResultNode, [aNodeList]);
    return htmlString;
}

startOpenTag aNode aNodeList {

    htmlString = concatAll(htmlString, ["<",
                                      slotNodeName(aNode),
                                      " ",
                                      html_generateIDString(aNode, aNodeList)
                                      ]
                 );

}

startDivTagClass aNode aNodeList {
    htmlString = concatAll(htmlString, ["<div class='",
                                      slotNodeName(aNode),
                                      "' ",
                                      html_generateIDString(aNode, aNodeList)
                                      ]
                 );

}

addAttribute attName attValue {
    htmlString = concatAll(htmlString, [
                                " ",
                                attName,
                                "='",
                                attValue,
                                "'"
                           ]
                 );
}

endOpenTag {
    htmlString = concat(htmlString, ">");
}


closeTag aNode {
    htmlString = concat(htmlString, html_createCloseTagFromNode(aNode));
}

closeTagString aString {
    htmlString = concatAll(htmlString, ["</", aString, "> "]);
}

nodeListToString nodeList {
    var theString = "";
    forEach(child in children(nodeList)) {
        theString = concatAll(theString, ["#", slotNodeName(child)]);
    };
    return theString;
}




constructRuleString aNode {
    var ruleTree = #"ROOT";
    ->(constructRuleTree)([aNode], ruleTree);
    return constructDFFText(ruleTree);
}



constructTreeString node {
    if(equals(node, null)) {
        return "null";
    };
    var stringTree = #"";
    ->(t_constructTreeString)([node], stringTree, [0]);
    return constructDFFText(stringTree);
}



equalTrees tree1 tree2 {
if(or(and(equals(tree1, null),
              not(equals(tree2, null))),
          and(equals(tree2, null),
              not(equals(tree1, null))))) {

        return false;

  };

  if(and(equals(tree1, null),
        equals(tree2, null))) {

        return true;
        };

    return ->(t_equalTrees)([tree1, tree2], rootResultNode);
}


testEqualTrees {

    var a = #"HI";
    addSlotNode(a, #"HI3");

    var b = #"HI";
    addSlotNode(b, #"HI3");

    printString(toString(equalTrees(a, b)));

    var hi = #"HI";
    addSlotNode(hi, #true);

    var hi2 = #"HI";
    addSlotNode(hi2, #true);

    printString(toString(equalTrees(hi, hi2)));
}



canMoveCursorLeft {
    return not(equals(findLeftNextCursorPos(), null));
}

canMoveCursorRight {
    return not(equals(findRightNextCursorPos(), null));
}

findDeepestFirstEditable node {
    return ->(findDeepestFirstEditable)([node], rootResultNode);
}




findFirstEditable dataNode{
    return ->(_findFirstEditable)([dataNode], rootResultNode);
}



findLastEditable dataNode{
    return ->(findLastEditable)([dataNode], rootResultNode);
}



findLastNotMarkedCursorPos dataNode {
    if(not(isEmpty(markedEditableList))) {
        var editableLeftC = elementAt(markedEditableList, 1).#1;

        return findLeftNextCursorPosParam(editableLeftC);
    } else {
        return null;
    };
}

findLeftNextCursorPos {
       return ->(findLeftNextCursorPos)([cursor, parent(cursor)], rootResultNode);
}

findLeftNextCursorPosParam cursorPos {
   return ->(findLeftNextCursorPos)([cursorPos, parent(cursorPos)], rootResultNode);

}

findLeftFromLeft curNode{
        forEach(prevNode in beforeReverse(curNode)) {
            var lastEditable = findLastEditable(prevNode);
            if(not(equals(lastEditable, null))) {
                return lastEditable.#3;
            };
        };
        var theParent = parent(curNode);
        if(equals(theParent, null)) {
            return null;
        };

        if(slotNodeNameEquals(theParent, "EDITABLE")) {
            return theParent.#1;
        } else {
            return findLeftFromLeft(theParent);
        };
}



findNextEditable dataNode editableNode {
    printString("finding next editable...");
    foundCurrentEditableNode = false;
    return ->(_findEditable | _findNextEditable)([dataNode], rootResultNode, [editableNode]);

}



findParentEditable editableNode {
    return ->(findParentEditable)([parent(editableNode)], rootResultNode);
}



findPreviousEditable dataNode editableNode {
    foundCurrentEditableNode = false;
 /*return ->(_findEditable | _findPreviousEditable)([dataNode], rootResultNode, [editableNode]);
   */
    return ->(_findPreviousEditable)([dataNode], rootResultNode, [editableNode]);

}



findRightNextCursorPos {
    ->(findRightNextCursorPos)([cursor, parent(cursor)], rootResultNode);
}

findRightFromRight curNode {

    forEach(nextNode in after(curNode)) {
        var firstEditable = findFirstEditable(nextNode);
        if(not(equals(firstEditable, null))) {
            return firstEditable.#1;
        };
    };
    var theParent = parent(curNode);
    if(equals(theParent, null)) {
        return null;
    };

    if(slotNodeNameEquals(theParent, "EDITABLE")) {
        return theParent.#3;
    }
    else {
        return findRightFromRight(theParent);
    };
}



moveCursorLeftForceStay {
    var nextCursor = findLeftNextCursorPos();
       if(not(equals(nextCursor, null))) {
            setCursorTo(nextCursor);
       };


}

moveCursorRightForceStay {
    var nextCursor = findRightNextCursorPos();
       if(not(equals(nextCursor, null))) {
            setCursorTo(nextCursor);
       };


}

generateNodePath pointer {
    var path = [];
    var current = pointer;
    while(not(equals(parent(current), null))) {
        insertElementAt(path, indexInParent(current),1);
        current = parent(current);
    };
    return path;
}

hasEditable dataNode {
    var firstEditable = findFirstEditable(dataNode);
    if(not(equals(firstEditable, null))) {
        return true;
    } else {
        return false;
    };

}

moveCursorLeft {
       var nextCursor = findLeftNextCursorPos();
       if(not(equals(nextCursor, null))) {
            setCursorTo(nextCursor);
       };

       var canStay = ->(cursorCanStayRight)([parent(cursor).#2], rootResultNode);
       if(and( slotNodeNameEquals(cursor, "RIGHTCURSOR"),
               not(canStay))) {
           moveCursorLeft();
       };
}



moveCursorLeftIfCantStay {
    var canStay = ->(cursorCanStayRight)([parent(cursor).#2], rootResultNode);
    if(and( slotNodeNameEquals(cursor, "RIGHTCURSOR"),
            not(canStay))) {
        moveCursorRight();
    };
}

moveCursorRight {
    var nextCursor = findRightNextCursorPos();

    if(not(equals(nextCursor, null))) {
        setCursorTo(nextCursor);

        var canStay = ->(cursorCanStayRight)([parent(cursor).#2], rootResultNode);
        if(and( slotNodeNameEquals(cursor, "RIGHTCURSOR"),
                not(canStay))) {
            moveCursorRight();
        };
    };




}




moveCursorRightIfCantStay {
    var canStay = ->(cursorCanStayRight)([parent(cursor).#2], rootResultNode);
    if(and( slotNodeNameEquals(cursor, "RIGHTCURSOR"),
            not(canStay))) {
        moveCursorRight();
    };
}

setCursorPathFromPointer {
    cursorIndexPath = generateNodePath(cursor);
}

setCursorPointerFromPath {
    cursor = data;
    forEach(value in cursorIndexPath) {
        cursor = childAt(cursor, value);
    };
}

setCursorTo node {
    cursor = node;
    setCursorPathFromPointer();

}

handleKeyboardEvent anEvent {

    var eventNode = #"KEYBOARDEVENT" [
                            +#htmlAtt(anEvent, "key") {
                                if(htmlAtt(anEvent, "shiftKey")) {
                                    if(htmlAtt(anEvent, "ctrlKey")) {
                                        +#"CTRLShift";
                                    } else {
                                        +#"Shift";
                                    };

                                    /*printString("shift key held");
                                    */
                                }
                                else {
                                    if(htmlAtt(anEvent, "ctrlKey")) {
                                        +#"CTRL";
                                        /*printString("Ctrl Key Held");
                                        */
                                    } else {
                                        +#"RAW";
                                    };

                                };

                            };
                    ];

    ->(doKeyboardHandleEvent)([eventNode], rootResultNode);
    redrawAll();

}



onPaste content {

    var fromState = constructStateCopy();

    var list = stringToCharList(content);

    forEach(char in list) {
        printString(concat("executing paste ", char));
        var succeeded=executeCharCommandFromCursor(char);
        if(not(succeeded)) {
            revertToState(fromState);
        };
    };
    var toState = constructStateCopy();
    addToUndoList(fromState, toState);
    clearRedo();

    redrawAll();


}

main {



    initVariables();
    return initEditor();



   /*initConstructFromTreeString();
   */


   /*testUndo();
   */

    /*testDeleteMarkedGroup();*/

/*  testConstructFromTreeString();
*/

/*  testCharCommands();
*/

/*  testDeleteCommands();
*/

/*  testBeforeAfter();
*/



}


initVariables {
        markedEditableList = [];
        undoList = [];
        redoList = [];
        stateList = [];
}

initEditor {
    currentRootGrammarNode = rootGrammarNode;

    nodesToHTML = [];



    initDataTree();

    redrawDataTree();

    return redraw();
}



/*

Constructs Data Tree from currentGrammarNode
Sets data to new data tree

Sets cursor to first editable's left side in new data tree

*/
initDataTree {
    data = #"ROOT" [
                +constructDataTree(currentRootGrammarNode);
            ];
    var firstEditable = findFirstEditable(data);
    if(not(equals(firstEditable, null))) {
        cursor = firstEditable.#1;
    };


    if(not(equals(cursor, null))) {
        setCursorPathFromPointer();
    };


}

redrawCursorInDataTree {
    if(not(equals(cursor, null))) {
            setCursorPathFromPointer();
            setCursorPointerFromPath();

    };
}


redrawDataTree {
    nodesToHTML = [];
    /*printString(constructTreeString(cursor));
    */

    if(not(equals(cursor, null))) {
        setCursorPathFromPointer();
    };

    /*printString(listToString(cursorIndexPath));
    */

    /*printString(constructTreeString(cursor));
    */

    data = #"ROOT" [
                +constructDataTree(currentRootGrammarNode);
            ];

    markedEditableList = [];

    if(not(equals(cursor, null))) {
        setCursorPointerFromPath();
    };

}

redrawHTMLTree {
    /*htmlTree = constructHTMLTree(data);
    */
    htmlTree = constructSMALLHTMLTree(data);


}


redrawHTMLString {
    htmlString = constructHTMLString(htmlTree, nodesToHTML);
}

redraw {

    htmlString = "";



    redrawHTMLTree();

    redrawHTMLString();



/*
    testFindEditable();
*/
/*
    testCursorMovement();
*/

  /*  testCursorCharCommands();
*/
    return htmlString;
}



hashMapWtf {
        var hashMap = {
                        hi : "bye",
                        dings : "lol"
                        };

        printString(mapKeyValue(hashMap, "hi"));
        printString("hiss");

          var hashMap2 = {
                            hi : "bye",
                            dings : "lol"
                            };
        printString(mapKeyValue(hashMap, "dings"));
        printString("hiss");

          var hashMap3 = {
                            hi : "bye",
                            dings : "lol"
                            };
        printString(mapKeyValue(hashMap, "dings"));
        printString("hiss");

          var hashMap4 = {
                            hi : "bye",
                            dings : "lol"
                            };
        printString(mapKeyValue(hashMap, "hi"));

        printString("hiss");
}


constructStateCopy {
    /*var logEntry = {
           slopeTree         : clone(currentRootGrammarNode),
           currentCursorPath : clone(cursorIndexPath)
    };*/


    var stateCopy = #"STATE" [
                        +#"slopeTree" {
                            +#clone(currentRootGrammarNode);
                        };
                        +#"currentCursorPath" {
                            +#clone(cursorIndexPath);
                        };

                    ];

/*
    var stateCopyMap = {
            slopeTree : clone(currentRootGrammarNode),
            currentCursorPath : clone(cursorIndexPath)
    };


    printString(constructTreeString(mapKeyValue(stateCopyMap, "slopeTree")));
    printString(listToString(mapKeyValue(stateCopyMap, "currentCursorPath")));
*/

    return stateCopy;
}

addToUndoList fromState toState {
    addElement(undoList, constructActionCommand(fromState, toState));
}

undo {
    if(canUndo()) {
        var actionCommand = removeElementFromList(lastElement(undoList), undoList);

        revertToState(childByNameChild(actionCommand, "from"));

        insertElementAt(redoList, actionCommand,
                          1);
    };
}


canUndo {
    /*return largerThan(length(stateList), 1);
    */
    return isNotEmpty(undoList);
}

clearUndo {
    undoList = [];
}

canRedo {
    return isNotEmpty(redoList);
}

redo {
    if(canRedo()) {

        var actionCommand = removeElementFromList(firstElement(redoList), redoList);

        revertToState(childByNameChild(actionCommand, "to"));

        addElement(undoList, actionCommand);

    };
}

clearRedo {
    redoList = [];
}




constructActionCommand fromState toState {
    var revertCommand = #"ACTION" [
                            +#"from" {
                                +fromState;
                            };
                            +#"to" {
                                +toState;
                            };
                        ];
}




revertToState state {

    currentRootGrammarNode =
                       clone(
                            childByNameValue(state, "slopeTree")
                       );


    cursorIndexPath =
                        clone(
                                childByNameValue(state, "currentCursorPath")
                         );

    printString("reverting to state");
    printString(constructTreeString(currentRootGrammarNode));
    printString(listToString(cursorIndexPath));

    cursor = null;
    redrawDataTree();
    setCursorPointerFromPath();

}




testCharCommands {

    testCharCommand("~"A~"", "A", "#start(
                                       #AND(
                                           #SLOPETOKEN(
                                               #A()
                                               #A()
                                               #STOP()
                                           )
                                       )
                                   )");
    testCharCommand("~"A~" ~"B~"", "B", null);


    testCharCommand("~"A~" ~"B~"", "A", "#start(
                                             #AND(
                                                 #SLOPETOKEN(
                                                     #A()
                                                     #A()
                                                     #STOP()
                                                 )
                                                 #SLOPETOKEN(
                                                     #B()
                                                     #B()
                                                     #STOP()
                                                 )
                                             )
                                         )");


    testCharCommand(" ~"[A-Z]~" ", "Z", "#start(
                                             #AND(
                                                 #GROUP(
                                                     #TOKEN(
                                                         #Z()
                                                     )
                                                     #A()
                                                     #Z()
                                                 )
                                             )
                                         )");

    testCharCommand(" ~"[A-B]~" ", "Z", null);


    testCharCommand(" ~"A~"  ~"[A-Z]~" ", "A", "#start(
                                                    #AND(
                                                        #SLOPETOKEN(
                                                            #A()
                                                            #A()
                                                            #STOP()
                                                        )
                                                        #GROUP(
                                                            #NULL()
                                                            #A()
                                                            #Z()
                                                        )
                                                    )
                                                )");


    testCharCommand(" ~"[A-Z]~"  ~"A~" ", "A", "#start(
                                                    #AND(
                                                        #GROUP(
                                                            #TOKEN(
                                                                #A()
                                                            )
                                                            #A()
                                                            #Z()
                                                        )
                                                        #SLOPETOKEN(
                                                            #A()
                                                            #A()
                                                            #STOP()
                                                        )
                                                    )
                                                )");


    testCharCommand(" ~"^ABC~" ", "D", "#start(
                                            #AND(
                                                #ANY_EXCEPT(
                                                    #TOKEN(
                                                        #D()
                                                    )
                                                    #EXCEPT(
                                                        #A()
                                                        #B()
                                                        #C()
                                                    )
                                                )
                                            )
                                        )");

    testCharCommand(" ~"^ABC~" ", "A", null);


    testCharCommand(" ~"A~"? ", "A", "#start(
                                         #AND(
                                             #OPT(
                                                 #SLOPETOKEN(
                                                     #A()
                                                     #A()
                                                     #STOP()
                                                 )
                                             )
                                         )
                                     )");

    testCharCommand(" ~"A~"? ", "B", null);


    testCharCommand(" ~"[A-Z]~"? ", "B", "#start(
                                              #AND(
                                                  #OPT(
                                                      #GROUP(
                                                          #TOKEN(
                                                              #B()
                                                          )
                                                          #A()
                                                          #Z()
                                                      )
                                                  )
                                              )
                                          )");


    testCharCommand(" (~"[A-Z]~" ~"H~")? ", "B", "#start(
                                                      #AND(
                                                          #OPT(
                                                              #AND(
                                                                  #GROUP(
                                                                      #TOKEN(
                                                                          #B()
                                                                      )
                                                                      #A()
                                                                      #Z()
                                                                  )
                                                                  #SLOPETOKEN(
                                                                      #H()
                                                                      #H()
                                                                      #STOP()
                                                                  )
                                                              )
                                                          )
                                                      )
                                                  )");


    testCharCommand(" (~"[A-Z]~" ~"H~")? ", "/", null);
    testCharCommand(" ~"A~"|~"B~" ", "A", "#start(
                                               #OR(
                                                   #AND(
                                                       #SLOPETOKEN(
                                                           #A()
                                                           #A()
                                                           #STOP()
                                                       )
                                                   )
                                                   #INDEX(
                                                       #1()
                                                   )
                                               )
                                           )");

    testCharCommand(" ~"G~" ~"A~"|~"B~" ", "A", null);

    testCharCommand(" ~"A~" ~"G~"|~"B~" ", "A", "#start(
                                                     #OR(
                                                         #AND(
                                                             #SLOPETOKEN(
                                                                 #A()
                                                                 #A()
                                                                 #STOP()
                                                             )
                                                             #SLOPETOKEN(
                                                                 #G()
                                                                 #G()
                                                                 #STOP()
                                                             )
                                                         )
                                                         #INDEX(
                                                             #1()
                                                         )
                                                     )
                                                 )");

    testCharCommand(" ~"A~" ~"G~"|~"B~" ", "B", "#start(
                                                     #OR(
                                                         #AND(
                                                             #SLOPETOKEN(
                                                                 #B()
                                                                 #B()
                                                                 #STOP()
                                                             )
                                                         )
                                                         #INDEX(
                                                             #2()
                                                         )
                                                     )
                                                 )");


    testCharCommand(" (~"F~"|~"Z~") ~"A~" ", "Z", "#start(
                                                       #AND(
                                                           #OR(
                                                               #AND(
                                                                   #SLOPETOKEN(
                                                                       #Z()
                                                                       #Z()
                                                                       #STOP()
                                                                   )
                                                               )
                                                               #INDEX(
                                                                   #2()
                                                               )
                                                           )
                                                           #SLOPETOKEN(
                                                               #A()
                                                               #A()
                                                               #STOP()
                                                           )
                                                       )
                                                   )");


    testCharCommand(" ~"A~" (~"F~"|~"Z~")  ", "Z", null);
    testCharCommand(" (~"A~" (~"F~"|~"Z~"))+|~"B~"  ", "Z", null);

    testCharCommand(" (~"A~" (~"F~"|~"Z~"))+|~"B~"  ", "A", "#start(
                                                                 #OR(
                                                                     #AND(
                                                                         #NFOLD(
                                                                             #AND(
                                                                                 #SLOPETOKEN(
                                                                                     #A()
                                                                                     #A()
                                                                                     #STOP()
                                                                                 )
                                                                                 #OR(
                                                                                     #NULL()
                                                                                     #INDEX(
                                                                                         #0()
                                                                                     )
                                                                                 )
                                                                             )
                                                                         )
                                                                     )
                                                                     #INDEX(
                                                                         #1()
                                                                     )
                                                                 )
                                                             )");
    testCharCommand(" (~"A~" (~"F~"|~"Z~"))+|~"B~"  ", "B", "#start(
                                                                 #OR(
                                                                     #AND(
                                                                         #SLOPETOKEN(
                                                                             #B()
                                                                             #B()
                                                                             #STOP()
                                                                         )
                                                                     )
                                                                     #INDEX(
                                                                         #2()
                                                                     )
                                                                 )
                                                             )");

    testCharCommand(" (~"A~" (~"F~"|~"Z~"))*|~"B~"  ", "Z", null);
    testCharCommand(" (~"A~" (~"F~"|~"Z~"))*|~"B~"  ", "A", "#start(
                                                                 #OR(
                                                                     #AND(
                                                                         #NFOLD(
                                                                             #AND(
                                                                                 #SLOPETOKEN(
                                                                                     #A()
                                                                                     #A()
                                                                                     #STOP()
                                                                                 )
                                                                                 #OR(
                                                                                     #NULL()
                                                                                     #INDEX(
                                                                                         #0()
                                                                                     )
                                                                                 )
                                                                             )
                                                                         )
                                                                     )
                                                                     #INDEX(
                                                                         #1()
                                                                     )
                                                                 )
                                                             )");


    testCharCommand(" (~"A~" (~"F~"|~"Z~"))*|~"B~"  ", "B", "#start(
                                                                 #OR(
                                                                     #AND(
                                                                         #SLOPETOKEN(
                                                                             #B()
                                                                             #B()
                                                                             #STOP()
                                                                         )
                                                                     )
                                                                     #INDEX(
                                                                         #2()
                                                                     )
                                                                 )
                                                             )");



}

testCharCommand regexString char pResultString{



    printString("Testing Char Command");
    printString(concat("Regex: ", regexString));



    var grammar = parseSlopeGrammar(concatAll("
                   Dings
                   Rules: start
                   start => ", [regexString, "."]));



    /*var grammarNode = parseSlopeWordFromGrammar(word, grammar);
    */
    var grammarNode = createTemplateFromGrammar(grammar);

    printString(constructTreeString(grammarNode));

    printString(concat("Char: ", char));


    /*printString(constructTreeString(grammarNode));
    */
    
    var resultTree = executeCharCommandRecursive(grammarNode, char);
    
    printString("Result:");
    printString( constructTreeString(
                    resultTree
                )
    );

    var expectedResultTree = constructFromTreeString(pResultString);


    if(equalTrees(expectedResultTree, resultTree)) {
        printString("TEST PASSED");
    } else {
        printString("TEST FAILED, Expected:");
        printString(constructTreeString(expectedResultTree));
    };
    printString("\n");
}

testCursorCharCommands {

    testCursorCharCommand(" ~"A~"? ", ["A", "Backspace"]);
    testCursorCharCommand(" (~"A~" ~"B~")? ", ["A",
                                                "ArrowRight",
                                                "ArrowRight",
                                                "ArrowRight",
                                                "ArrowRight",
                                                "Backspace"]);

    testCursorCharCommand(" ~"A~"*  ", ["A", "A"]);

    testCursorCharCommand(" (~"[A-Z]~"|~"[a-z]~"|~"_~")(~"[A-Z]~"|~"[a-z]~"|~"[0-9]~"|~"_~")*  ", ["A", "l", "e", "x"]);

    testCursorCharCommand(" ~"A~"?   ", ["A", "C"]);
    testCursorCharCommand(" ~"A~"? ~"C~"?  ", ["A", "C"]);
}


testCursorCharCommand regexString charCommandStringList {

    printString(concat("REGEX: ", regexString));

    var grammar = parseSlopeGrammar(concatAll("Ding
                                               Rules: start
                                               start => ", [regexString, "."]));

    currentRootGrammarNode = createTemplateFromGrammar(grammar);
    cursor = null;
    redrawDataTree();
    cursor = findFirstEditable(data).#1;
    setCursorPathFromPointer();

    printCurrentTreeSituation();

    forEach(charCommandString in charCommandStringList) {
        printString(concat("Char Command: ", charCommandString));
        ->(doCursorEvent)([#charCommandString, cursor], rootResultNode);
        /*executeCharCommandFromCursor(charCommandString);*/
        printCurrentTreeSituation();
    };

    printString("\n\n");


}





testCursorMovement {

    printString("running cursor tests");

    printCursorLocation();


    testCanMoveCursorRight();
    testMoveCursorRight();
    testCanMoveCursorRight();
    testMoveCursorRight();
    testCanMoveCursorRight();
    testMoveCursorRight();
    testCanMoveCursorRight();
    testMoveCursorRight();

    testMoveCursorLeft();
    testCanMoveCursorLeft();
    testMoveCursorLeft();
    testCanMoveCursorLeft();
    testMoveCursorLeft();
    testCanMoveCursorLeft();
    testMoveCursorLeft();



}



testSetCursorPath {
    printString("Test: cursor movement");
    printString("set cursor first");
    cursor = first.#1;
    printString("set cursor path");
    setCursorPathFromPointer();

    printString(constructTreeString(cursor));
    printString(constructTreeString(parent(cursor)));

    printString(listToString(cursorIndexPath));


}

testMoveCursorRight {

    printString("move cursor right");
    moveCursorRight();
    printCursorLocation();
}

testCanMoveCursorLeft {
    printString("can move cursor left?");
    if(canMoveCursorLeft()) {
        printString("yes");
    }
    else {
        printString("no");
    };
}

testCanMoveCursorRight {
    printString("can move cursor right?");
    if(canMoveCursorRight()) {
        printString("yes");
    }
    else {
        printString("no");
    };
}

testMoveCursorLeft {
    printString("move cursor left");
    moveCursorLeft();
    printCursorLocation();
}



printCursorLocation {
    printString(constructTreeString(cursor));
    printString(constructTreeString(parent(cursor)));
    printString(listToString(cursorIndexPath));
    printString("");
}

testDeleteCommands {

    var grammar = parseRegexGrammar("~"A~"*");
    var node = parseSlopeWordFromGrammar("A", grammar);
    printString(constructTreeString(node));
    currentRootGrammarNode = node;
    initDataTree();
    redrawDataTree();

    printCurrentTreeSituation();



    printString("\nMove Cursor Right");
    moveCursorRight();
    printCurrentTreeSituation();

    printString("\nMove Cursor Right");
    moveCursorRight();
    printCurrentTreeSituation();





    printString("\nDelete");
    executeBackspaceDeleteCommandFromCursor();
    printCurrentTreeSituation();
}


testDeleteMarkedGroup {

    generateDataTreeFromRegex(parseRegexGrammar("~"A~"*"), "A");

    executeMarkRightFromCursor();
    executeMarkRightFromCursor();

    printCurrentTreeSituation();

    executeBackspaceDeleteMarkedEditables();

    printCurrentTreeSituation();

}

generateDataTreeFromRegex regexGrammar regexWord {
    var grammar = regexGrammar;
    var node = parseSlopeWordFromGrammar(regexWord, grammar);

    currentRootGrammarNode = node;
    cursor = null;

    initDataTree();
    redrawDataTree();


}


testFindEditable {
    printString("Test: findFirstEditable");

    var first = findFirstEditable(data);
    printString(constructTreeString(first));


    printString("Test: findNextEditable");
    var next = findNextEditable(data, first);
    printString(constructTreeString(next));
    printString("found next");
    printString("");



    printString("Test: findFirstEditable");
    var first2 = findFirstEditable(data, next.#2);
    printString(constructTreeString(first2));
    printString("found first2");
    printString("");

    printString("Test: findNextEditable");
    var firstNext = findNextEditable(data, first2);
    printString(constructTreeString(firstNext));
    printString("found firstNext");
    printString("");





    printString("Test: findNextEditable");
    var next2 = findNextEditable(data, next);
    printString(constructTreeString(next2));
    printString("found next2");
    printString("");

    printString("Test: findNextEditable");
    var next3 = findNextEditable(data, next2);
    printString(constructTreeString(next3));
    printString("found next3");
    printString("");




    printString("Test: findPreviousEditable");

    var previous = findPreviousEditable(data, next);
    printString(constructTreeString(previous));


    printString("");
}


testUndo {

    testCommandNodeList(
        "~"A~"*", [
                    #"A"[+#"RAW";],
                    #"A"[+#"RAW";],
                   #"z"[+#"CTRL";],
                   #"z"[+#"CTRL";]
        ]
    );
}

testCommandList regexString charCommandStringList {

    printString(concat("REGEX: ", regexString));

    var grammar = parseSlopeGrammar(concatAll("Ding
                                               Rules: start
                                               start => ", [regexString, "."]));

    currentRootGrammarNode = createTemplateFromGrammar(grammar);
    cursor = null;
    redrawDataTree();
    cursor = findFirstEditable(data).#1;
    setCursorPathFromPointer();

    printCurrentTreeSituation();

    forEach(charCommandString in charCommandStringList) {
        printString(concat("Char Command: ", charCommandString));
        ->(doKeyboardHandleEvent)([
            #"KEYBOARDEVENT" [
                +#charCommandString {
                    +#"RAW";
                };
            ]], rootResultNode);
        /*executeCharCommandFromCursor(charCommandString);*/
        printCurrentTreeSituation();
    };

    printString("\n\n");


}




testCommandNodeList  regexString charCommandNodeList{

        printString(concat("REGEX: ", regexString));

        var grammar = parseSlopeGrammar(concatAll("Ding
                                                   Rules: start
                                                   start => ",
                                                   [regexString, "."]));

        currentRootGrammarNode = createTemplateFromGrammar(grammar);
        cursor = null;
        initDataTree();
        redrawDataTree();
        /*cursor = findFirstEditable(data).#1;
        setCursorPathFromPointer();*/

        printCurrentTreeSituation();

        forEach(charCommandNode in charCommandNodeList) {
            printString(concat("Char Command: ", slotNodeName(charCommandNode)));
            ->(doKeyboardHandleEvent)([
                #"KEYBOARDEVENT" [
                    +charCommandNode;
                ]], rootResultNode);
            /*executeCharCommandFromCursor(charCommandString);*/
            printCurrentTreeSituation();
        };

        printString("\n\n");


}

addCursorIfNeeded editable htmlNode {

    if(equals(cursor, editable.#1)) {
        var leftCursor = #"cursor left" [
                            +#"TEXT" {
                                +#"|";
                            };
                        ];
        addSlotNode(htmlNode, leftCursor);
        addClass(thisResultNode, "focused");
    };

    if(equals(cursor, editable.#3)) {
        var rightCursor = #"cursor right" [
                            +#"TEXT" {
                                +#"|";
                            };
                        ];
        addSlotNode(htmlNode, rightCursor);
        addClass(thisResultNode, "focused");
    };

}





addCursorIfNeededWithFocus editable htmlNode showFocus {

    if(equals(cursor, editable.#1)) {
        var leftCursor = #"cursor left" [
                            +#"TEXT" {
                                +#"|";
                            };
                        ];
        addSlotNode(htmlNode, leftCursor);
        if(showFocus) {
            addClass(thisResultNode, "focused");
        };
    };

    if(equals(cursor, editable.#3)) {
        var rightCursor = #"cursor right" [
                            +#"TEXT" {
                                +#"|";
                            };
                        ];
        addSlotNode(htmlNode, rightCursor);
        if(showFocus) {
            addClass(thisResultNode, "focused");
        };
    };

}





checkNeedFocus slopeNode {
    return ->(checkNeedFocus)([slopeNode], rootResultNode);
}




constructDataTree slopeTree {
    var root = #"ROOT";
    ->(_constructDataTree)([slopeTree], root, [true]);
    if(not(equals(length(children(root)), 1))) {
        /*printString("wtf");*/
        throw("root is shitty");
    };
    return root.#1;
}


generateEditableIfPossible inputNode currentResultNode editable{
    if(editable) {
        var editableNode = #"EDITABLE" [
                                +#"LEFTCURSOR";
                                +inputNode;
                                +#"RIGHTCURSOR";
                                +#"MARKED" {
                                    +#"FALSE";
                                };
                            ];
        addSlotNode(currentResultNode, editableNode);
    }
    else {
        addSlotNode(currentResultNode, inputNode);
    };
}






constructHTMLTree dataTree {
    var root = #"ROOT";
    ->(_constructHTMLTree | doThisOnChildren1)([dataTree], root);
    return root;
}





constructRegexSlopeTree bla {

}

constructSMALLHTMLTree dataTree {
    var root = #"ROOT";
    ->(_constructSMALLHTMLTree | doThisOnChildren1)([dataTree], root);
    return root;
}





constructCharCommand char cursorPosNode {
    return #"COMMAND" [
            +#"CHAR" [
                +#char;
                +#generateNodePath(cursorPosNode);
            ];
           ];
}

dataExecuteCharCommandRecursive editableNode char {
    return ->(dataExecuteCharCommandRecursive)([dataNode], rootResultNode, [char]);
}



executeCharCommandFromCursor char {

    ->(executeCharCommandFromCursor)([cursor, parent(cursor).#2, slotOriginNode(cursor)],
                                    rootResultNode, [char]);

}





executeCharCommandRecursive slopeNode char {
    return ->(executeCharCommandRecursive)([slopeNode], rootResultNode, [char]);
}



constructBackspaceDeleteCommand cursorPointer {
    return  #"COMMAND" [
                +#"BACKSPACE" [
                    +#generateNodePath(cursorPointer);
                ];
            ];
}

deleteEditable editable {
    ->(deleteEditable)([editable.#2], rootResultNode);
}



executeBackspaceDeleteCommandFromCursor {
    return ->(executeBackspaceDeleteCommandFromCursor)([cursor, parent(cursor).#2, slotOriginNode(cursor)], rootResultNode);
}



executeDelDeleteCommandFromCursor {

    return ->(executeDelDeleteCommandFromCursor)([cursor, parent(cursor).#2, slotOriginNode(cursor)], rootResultNode);
}



executeCommandNode commandNode {
    ->(executeCommandNode)([commandNode], rootResultNode);
}



deleteMarkedEditables {

    forEach(editable in markedEditableList) {
        if(canDeleteEditable(editable)) {
            deleteEditable(editable);
        };
    };


}



executeBackspaceDeleteMarkedEditables {
    var lastNotMarkedPos = findLastNotMarkedCursorPos(data);
    if(not(equals(lastNotMarkedPos , null))) {
        setCursorTo(lastNotMarkedPos);
        deleteMarkedEditables();
        printString("deleted");
        redrawDataTree();
    }
    else {
        deleteMarkedEditables();
        setCursorTo(findFirstEditable(data).#1);
        redrawDataTree();
    };

    return null;



}


constructMarkedTextString {
    markedTextString = "";
    ->(constructMarkedTextString)([data], rootResultNode);
    return markedTextString;
}



executeMarkLeftFromCursor {
    if(canMoveCursorLeft()) {
        moveCursorLeft();
        var editable = parent(cursor);

        if(and( not(hasEditable(parent(cursor).#2)),
                slotNodeNameEquals(cursor, "LEFTCURSOR")
              )
        ) {
            if(isMarked(editable)) {
                unMarkEditable(editable);
            } else {
                markEditable(editable);
                insertElementAt(markedEditableList, editable, 1);
            };
        };
    };
}

executeMarkRightFromCursor {

    if(canMoveCursorRight()) {
        moveCursorRightForceStay();

        var editable = parent(cursor);

        if(and( not(hasEditable(parent(cursor).#2)),
                slotNodeNameEquals(cursor, "RIGHTCURSOR")
              )
        ) {
            if(isMarked(editable)) {
                unMarkEditable(editable);

            } else {
                markEditable(editable);
                addElement(markedEditableList, editable);

            };
        };

            moveCursorRightIfCantStay();

    };

}

isMarked editable {
    if(slotNodeNameEquals(editable.#4.#1, "TRUE")) {
        return true;
    } else {
        return false;
    };
}

markEditable editable {
    setChildAt(editable.#4, #"TRUE", 1);
}

unMarkEditable editable {
    setChildAt(editable.#4, #"FALSE", 1);
    removeElementFromList(editable, markedEditableList);
}

dataCanSetCharAnyExcept dataNode {
    return canSetCharAnyExcept(slotOriginNode(dataNode));
}

dataCanSetCharGroup dataNode {
    return canSetCharGroup(slotOriginNode(dataNode));
}

/*
                NFOLD(slope) {
                    ...
                }
                   <-
    starNode = #"STAR" {
                        ...
                }

*/
dataClearStar starNode {
    clearNFOLD_0(slotOriginNode(starNode));
    removeAllChildren(starNode);
}

/*

*/

dataCreateNFOLDChildTemplate dataNode {

    var nfoldChildTemplate = createNFOLDChildTemplate(slotSourceNode(dataNode));
    var miniTree = #"NFOLDCHILD" [
                        +constructDataTree(nfoldChildTemplate);
                    ];
    setSourceNode(miniTree, slotSourceNode(dataNode));
    return nfoldChildTemplate;
}

dataCreateORChildTemplateAt dataOr at {
    var orChildTemplate = constructDataTree(
                            createORChildTemplateAt(slotOriginNode(dataOr), at)
                            );
    return orChildTemplate;
}

dataCreateSingleChildTemplate dataNode {
    var template = constructDataTree(
                    createSingleChildTemplate(slotOriginNode(dataNode))
                    );
    return template;
}

dataCreateTemplate dataNode {
    return constructDataTree(
                createTemplate(
                    slotSourceNode(dataNode)
                )
           );
}

dataDeleteChar dataNode {
    deleteChar(slotSourceNode(dataNode));
    setChildAt(dataNode, #"NULL", 1);
}

dataDeleteNode dataNode {
    deleteNode(slotSourceNode(dataNode));
    setChildAt(dataNode, #"NULL", 1);
}

dataHasOPTChild dataNode {
    return hasOPTChild(slotSourceNode(dataNode));
}

dataInsertNFOLDChildAt dataNFOLD nfoldChild at{
    var slopeNFOLD = slotOriginNode(dataNFOLD);

    printString(slotNodeName("dataInsertNFOLDChildAt"));
    printString(slotNodeName(slopeNFOLD));
    printString(slotNodeName(slotOriginNode(nfoldChild)));

    insertNFOLDChildAt(slopeNFOLD, slotOriginNode(nfoldChild.#1), at);
    insertChildAt(dataNFOLD, nfoldChild, 1);
}

dataInsertNFOLDChildTemplateAt dataNode at{
    var dataTemplate = dataCreateNFOLDChildTemplate(dataNode);
    dataInsertNFOLDChildAt(dataNode, dataTemplate, at);

}

dataInsertOPTChild dataOPT dataChild {
    var slopeOptChild = slotOriginNode(dataChild);
    var slopeOpt = slotOriginNode(dataOPT);
    insertOPTChild(slopeOpt, slopeOptChild);
    setChildAt(dataOPT, dataChild, 1);

}

dataInsertORChild dataOR dataChild {
    var slopeOrChild = slotOriginNode(dataChild);
    var slopeOr = slotOriginNode(dataOR);
    insertORChild(slopeOr, slopeOrChild);
    setChildAt(dataOR, dataChild, 1);
}

dataNumAlternatives dataOr {
    return numAlternatives(slotOriginNode(dataOr));
}

dataRemoveCharAnyExcept dataAnyExcept {
    removeCharAnyExcept(slotOriginNode(dataAnyExcept));
    setChildAt(dataAnyExcept, #"NULL", 1);
}

dataRemoveCharGroup dataGroup {
    removeCharGroup(slotOriginNode(dataGroup));
    setChildAt(dataGroup, #"NULL", 1);
}

dataRemoveNFOLDChild nfoldChild {
    removeNFOLDChild(slotOriginNode(nfoldChild.#1));
    removeNode(nfoldChild);
}

dataRemoveNFOLDChildAt dataNFOLD at{
    removeNFOLDChildAt(slotOriginNode(dataNFOLD), at);
    removeChildAt(dataNFOLD, at);
}

dataRemoveOPTChild dataOPT {
    removeOPTChild(slotOriginNode(dataOPT));
    setChildAt(dataOPT, #"NULL", 1);
}

dataSetCharAnyExcept dataAnyExcept char {
    setCharAnyExcept(slotOriginNode(dataAnyExcept), char);
    setChildAt(dataAnyExcept, #"Letter" [+#char;]  , 1);

}

dataSetCharGroup dataGroup char {
    setCharGroup(slotOriginNode(dataGroup), char);
    setChildAt(dataGroup, #"Letter" [+#char;]  , 1);
}

isDataNFOLD dataNode {
    if(or(slotNodeNameEquals(dataNode, "STAR"),
          slotNodeNameEquals(dataNode, "PLUS"))
       ) {
      return true;
    }
    else {
      return false;
    };
}

isDataNFOLDFilled dataNode {
    return or(
                slotNodeNameEquals(dataNode, "STAR"),
                slotNodeNameEquals(dataNode, "PLUS")
                );
}

isDataStarEmpty dataNode {
    if(and( slotNodeNameEquals(dataNode, "STAR"),
            hasNoChildren(dataNode))
       ) {
         return true;
    } else {
         return false;
    };
}

after node {
    var afterList = [];
    var theParent = parent(node);
    if(equals(theParent, null)) {
        return afterList;
    };

    var counter = plus(indexInParent(node), 1);
    var childrenLength = length(children(theParent));
    while(smallerEqualsThan(counter, childrenLength)) {
        var child = childAt(theParent, counter);
        addElement(afterList, child);
        counter = plus(counter, 1);
    };

    return afterList;
}

afterReverse node {
    var afterList = [];
    var theParent = parent(node);
    if(equals(theParent, null)) {
        return afterList;
    };

    var counter = length(children(theParent));
    while(largerThan(counter, 0)) {
        var child = childAt(theParent, counter);
        if(equals(child, node)) {
            return afterList;
        } else {
            addElement(afterList, child);
        };
        counter = minus(counter, 1);
    };

    /*printString("WTF after");*/
    return afterList;
}

before node {
    var beforeList = [];
    var theParent = parent(node);
    if(equals(theParent, null)) {
        return beforeList;
    };

    forEach(child in children(theParent)) {
        if(equals(child, node)) {
            return beforeList;
        } else {
            addElement(beforeList, child);
        };
    };

    printString("WTF before");
    return beforeList;
}

beforeReverse node {
    var beforeList = [];
    var theParent = parent(node);
    if(equals(theParent, null)) {
        return beforeList;
    };

    var index = minus(indexInParent(node), 1);
    while(largerThan(index, 0)) {
        var child = childAt(theParent, index);
        addElement(beforeList, child);
        index = minus(index, 1);
    };

    return beforeList;
}

canDeleteEditable editable{
    if(not(slotNodeNameEquals(editable.#2, "Letter"))) {
        return true;
    } else {
        return false;
    };
}

hasNextNode node {
    var index = indexInParent(node);
    if(equals(index, length(children(node)))) {
        return false;
    } else {
        return true;
    };
}

hasPreviousNode node {
    var index = indexInParent(node);
    if(smallerEqualsThan(index, 1)) {
        return false;
    } else {
        return true;
    };
}

isFirstChild node {
    if(equals(parent(node), null)) {
        return false;
    };

    if(equals(indexInParent(node), 1)) {
        return true;
    } else {
        return false;
    };
}

isLastChild node {

    if(equals(parent(node), null)) {
        return false;
    };

    var theParent = parent(node);

    if(equals(indexInParent(node), length(children(theParent)))) {
       return true;
    } else {
        return false;
    };
}

nextNode node {
    if(isLastChild(node)) {
        return null;
    };

    if(equals(parent(node), null)) {
        return null;
    };

    var theParent = parent(node);
    var nextIndex = plus(indexInParent(node), 1);
    return childAt(theParent, nextIndex);
}

previousNode node {
    if(isFirstChild(node)) {
        return null;
    };

    if(equals(parent(node), null)) {
        return null;
    };

    var theParent = parent(node);
    var previousIndex = minus(indexInParent(node), 1);
    return childAt(theParent, previousIndex);
}

printCurrentTreeSituation {
    printString("");
    printString("current tree");
    printString(constructTreeString(currentRootGrammarNode));
    printString(constructTreeString(data));
    printString("cursor on:");
    printString(constructTreeString(cursor));
    printString(constructTreeString(parent(cursor)));
    printString("");
    printString("");
    printString("");

}

parseRegexGrammar regexString {
    return parseSlopeGrammar(concatAll("Ding
                                               Rules: start
                                               start => ", [regexString, "."]));
}

testBeforeAfter {
    printString("testBeforeAfter");
    var tree = #"HI" [
                    +#"Child1";
                    +#"Child2";
                    +#"Child3";
                    +#"Child4";
                    +#"Child5";
                    +#"Child6";
                ];
    printString(constructTreeString(tree));

    printString("");


    var child = childByName(tree, "Child4");

    printString("testing before(child4)");
    forEach(previousChild in before(child)) {
        printString(slotNodeName(previousChild));
    };
    printString("");

    printString("testing beforeReverse(child4)");
    forEach(previousChild in beforeReverse(child)) {
        printString(slotNodeName(previousChild));
    };
    printString("");



    printString("testing after(child4)");
    forEach(nextChild in after(child)) {
        printString(slotNodeName(nextChild));
    };
    printString("");


    printString("testing afterReverse(child4)");
    forEach(nextChild in afterReverse(child)) {
        printString(slotNodeName(nextChild));
    };
    printString("");




}

TreeFunction constructDFFTextTree

ALL exceptString {
   var stringToAppend = "";
   if(not(equals(slotNodeName(thisGrammarNode), exceptString))) {
       stringToAppend = toString(slotNodeName(thisGrammarNode));
   };
   dffText = concat(dffText, stringToAppend);


   forEach(aChild in children(thisGrammarNode)) {
        thisTreeFunction([aChild], thisResultNode, [exceptString]);
   };
}

TreeFunction t_constructFromTreeString

treeNode.AND {
    var node = thisTreeFunction([thisGrammarNode.#2], thisResultNode);

    forEach(child in children(thisGrammarNode.#4)) {
        addSlotNode(node, thisTreeFunction([child], thisResultNode));
    };
    return node;
}

identifier.AND {

    return #thisTreeFunction([thisGrammarNode.#1], thisResultNode);
}

number.AND {

    return #stringToInt(
            thisTreeFunction([thisGrammarNode.#1], thisResultNode)
           );
}

boolean.AND {

    return #stringToBool(
            thisTreeFunction([thisGrammarNode.#1], thisResultNode)
           );
}

value.AND {
    return thisTreeFunction([thisGrammarNode.#1], thisResultNode);
}

SLOPETOKEN {
    return slotNodeName(thisGrammarNode.#1);
}

GROUP {
    return slotNodeName(thisGrammarNode.#1.#1);
}

ALL {
    if(hasChildren(thisGrammarNode)) {
        return thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    };
}

/*
TreeFunction constructFromTreeString

isNamedSLOTSLOPETreeNode() {
    return continue([thisGrammarNode, thisGrammarNode.#1], thisResultNode);
}

SLOPETOKEN {
    return slotNodeName(thisGrammarNode.#1);
}

ALL {
    if(hasChildren(thisGrammarNode)) {
        return thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    };
}


TreeFunction t_constructFromTreeString

treeNode slopeAND:AND {
    var node = thisTreeFunction([slopeAND.#2], thisResultNode);

    forEach(child in children(slopeAND.#4)) {
        addSlotNode(node, thisTreeFunction([child], thisResultNode));
    };
    return node;
}

identifier slopeAND:AND {
    return #thisTreeFunction([slopeAND.#1], thisResultNode);
}

number slopeAND:AND {
    return #stringToInt(
            thisTreeFunction([slopeAND.#1], thisResultNode)
           );
}

boolean slopeAND:AND {
    return #stringToBool(
            thisTreeFunction([slopeAND.#1], thisResultNode)
           );
}

ALL slopeAND:AND {
    return thisTreeFunction([slopeAND.#1], thisResultNode);
}

*/

TreeFunction _constructHTMLString

TEXT aNodeList {


    var i = 0;
    forEach(child in children(thisGrammarNode)) {
        if(largerThan(i, 0)) {
            htmlString =
                    concat(htmlString, " ");
        };
        htmlString =
                concat(htmlString, slotNodeName(child));
        i = plus(i, 1);
    };


}

ROOT aNodeList {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode, [aNodeList]);
    };
}

att {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode);
    };
}

att.ALL {
    /*htmlString = concatAll(htmlString, [" ", slotNodeName(thisGrammarNode),
                                        "='",
                                        slotNodeName(thisGrammarNode.#1),
                                        "'"
                                     ]
                 );*/


    var attributeValue = "";
    var addSpace = false;
    forEach(child in children(thisGrammarNode)) {
        if(addSpace) {
            attributeValue = concat(attributeValue, " ");
        };
        attributeValue = concat(attributeValue, slotNodeName(child));
        addSpace = true;
    };
    addAttribute(slotNodeName(thisGrammarNode), attributeValue);
}


DIV aNodeList {

    if(and(
        largerThan(length(children(thisGrammarNode)), 0),
        equals(slotNodeName(thisGrammarNode.#1), "att")
       )
    ) {
        printString(concat("In ", slotNodeName(thisGrammarNode)));
        startOpenTag(thisGrammarNode, aNodeList);
            thisTreeFunction([thisGrammarNode.#1], thisResultNode);
        endOpenTag();

        printString(htmlString);

        var i = 2;
        while(smallerEqualsThan(i, length(children(thisGrammarNode)))) {
            thisTreeFunction([childAt(thisGrammarNode, i)], thisResultNode, [aNodeList]);
            i = plus(i, 1);
        };

        closeTag(thisGrammarNode);

    };



}

BR aNodeList {

    if(and(
        largerThan(length(children(thisGrammarNode)), 0),
        equals(slotNodeName(thisGrammarNode.#1), "att")
       )
    ) {
        printString(concat("In ", slotNodeName(thisGrammarNode)));
        startOpenTag(thisGrammarNode, aNodeList);
            thisTreeFunction([thisGrammarNode.#1], thisResultNode);
        endOpenTag();

        printString(htmlString);

        var i = 2;
        while(smallerEqualsThan(i, length(children(thisGrammarNode)))) {
            thisTreeFunction([childAt(thisGrammarNode, i)], thisResultNode, [aNodeList]);
            i = plus(i, 1);
        };

        closeTag(thisGrammarNode);

    };



}

SPAN aNodeList {

    if(and(
        largerThan(length(children(thisGrammarNode)), 0),
        equals(slotNodeName(thisGrammarNode.#1), "att")
       )
    ) {
        printString(concat("In ", slotNodeName(thisGrammarNode)));
        startOpenTag(thisGrammarNode, aNodeList);
            thisTreeFunction([thisGrammarNode.#1], thisResultNode);
        endOpenTag();

        printString(htmlString);

        var i = 2;
        while(smallerEqualsThan(i, length(children(thisGrammarNode)))) {
            thisTreeFunction([childAt(thisGrammarNode, i)], thisResultNode, [aNodeList]);
            i = plus(i, 1);
        };

        closeTag(thisGrammarNode);

    };



}

ALL aNodeList {

           /* printString(concat("In ", slotNodeName(thisGrammarNode)));*/
            startDivTagClass(thisGrammarNode, aNodeList);
            endOpenTag();

   /*         printString(htmlString);*/

            var i = 1;
            while(smallerEqualsThan(i, length(children(thisGrammarNode)))) {
                thisTreeFunction([childAt(thisGrammarNode, i)], thisResultNode, [aNodeList]);
                i = plus(i, 1);
            };

            closeTagString("DIV");


            /*

        if(and(
                    largerThan(length(children(thisGrammarNode)), 0),
                    equals(slotNodeName(thisGrammarNode.#1), "att")
                   )
                ) {
                    printString(concat("In ", slotNodeName(thisGrammarNode)));
                    startDivTagClass(thisGrammarNode, aNodeList);


                    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
                    endOpenTag();

                    printString(htmlString);

                    var i = 2;
                    while(smallerEqualsThan(i, length(children(thisGrammarNode)))) {
                        thisTreeFunction([childAt(thisGrammarNode, i)], thisResultNode, [aNodeList]);
                        i = plus(i, 1);
                    };

                    closeTagString("DIV");

                } else {

                ........
        };*/
}



TreeFunction constructRuleTree

OR {
    var numAlts = numAlternatives(thisGrammarNode);


    thisTreeFunction([createORChildTemplateAt(thisGrammarNode, 0)], thisResultNode);


    var counter = 1;

    while(smallerThan(counter, numAlts)) {

        +#" | ";
        thisTreeFunction([createORChildTemplateAt(thisGrammarNode, counter)], thisResultNode);
        counter = plus(counter, 1);
    };

}

OPT {
    var childTemplate = createSingleChildTemplate(thisGrammarNode);
    thisTreeFunction([childTemplate], thisResultNode);
    +#"?";
}

BRACKET {

    +#"(";
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    +#")";
}

isNFOLD_0() {
    var childTemplate = createNFOLDChildTemplate(thisGrammarNode);
    thisTreeFunction([childTemplate], thisResultNode);
    +#"*";
}

isNFOLD_1() {
    var childTemplate = createNFOLDChildTemplate(thisGrammarNode);
    thisTreeFunction([childTemplate], thisResultNode);
    +#"+";
}

AND {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);

    var counter = 2;
    var childrenLength = length(children(thisGrammarNode));
    while(or(smallerThan(counter, childrenLength),
             equals(counter,childrenLength))) {
        +#" ";
        thisTreeFunction([childAt(thisGrammarNode, counter)], thisResultNode);

        counter = plus(counter, 1);
    };
}

NULL {
    +#"NULL";
}

SLOPETOKEN {
    +#"~"";
    +#slotNodeName(thisGrammarNode.#1) ;
    +#"~"";
}

ALL {
    +#slotNodeName(thisGrammarNode);
}

TreeFunction t_constructTreeString

ALL tabNumber {

    var i = 0;
    while(smallerThan(i, tabNumber)) {
        +#"    ";
        i = plus(i,1);
    };

    +#"#";
    +#slotNodeName(thisGrammarNode);
    +#"(";

    if(hasChildren(thisGrammarNode)) {
        +#"\n";

        forEach(child in children(thisGrammarNode)) {
            thisTreeFunction([child], thisResultNode, [plus(tabNumber, 1)]);
            +#"\n";
        };

        i = 0;
        while(smallerThan(i, tabNumber)) {
                +#"    ";
                i = plus(i,1);
        };
    };

    +#")";
}

TreeFunction t_equalTrees

ALL secondGrammarNode:ALL {
    if(or(
            not(
                equals(slotNodeName(thisGrammarNode),
                      slotNodeName(secondGrammarNode)
                )
            ),
            not(
                equals(length(children(thisGrammarNode)),
                       length(children(secondGrammarNode))
                )
            )
        )
    ) {
        printString("Nodes NOT EQUAL");
        printString("LEFT:");
        printString(runtimeType(slotNodeName(thisGrammarNode)));
        printString(constructTreeString(thisGrammarNode));
        printString("RIGHT:");
        printString(runtimeType(slotNodeName(secondGrammarNode)));
        printString(constructTreeString(secondGrammarNode));
        return false;
    }
    else {
        var i = 1;
        while(smallerEqualsThan(i, length(children(thisGrammarNode)))) {
            if(not(
                thisTreeFunction([childAt(thisGrammarNode, i),
                                  childAt(secondGrammarNode, i)],
                                 rootResultNode)
            )) {
                return false;
            };

            i = plus(i,1);
        };
        return true;
    };
}

TreeFunction findDeepestFirstEditable

EDITABLE {
    forEach(child in children(thisGrammarNode)) {
        var editable = thisTreeFunction([child], thisResultNode);
        if(not(equals(editable, null))) {
            return editable;
        };
    };
    return thisGrammarNode;
}

ALL {
    forEach(child in children(thisGrammarNode)) {
            var editable = thisTreeFunction([child], thisResultNode);
            if(not(equals(editable, null))) {
                return editable;
            };
        };
}

TreeFunction _findFirstEditable

EDITABLE {
    return thisGrammarNode;
}

ALL {
    forEach(child in children(thisGrammarNode)) {
        var editable = thisTreeFunction([child], thisResultNode);
        if(not(equals(editable, null))) {
            return editable;
        };
    };
}

TreeFunction findLastEditable

EDITABLE {
    return thisGrammarNode;
}

ALL {
    var count = length(children(thisGrammarNode));
    while(largerThan(count, 0)) {
        var editable = thisTreeFunction([childAt(thisGrammarNode, count)], thisResultNode);
        if(not(equals(editable, null))) {
            return editable;
        };
        count = minus(count, 1);
    };
}

TreeFunction findLeftNextCursorPos

/*Left from Right*/
RIGHTCURSOR editable:EDITABLE {
    var lastEditable = findLastEditable(editable.#2);
    if(not(equals(lastEditable,null))) {
       return lastEditable.#3;
    }
    else {
       return editable.#1;
    };
}

LEFTCURSOR editable:EDITABLE {
    return findLeftFromLeft(editable);
}

ALL editable:ALL {
    throw("moveCursorLeft wtf");
}


TreeFunction _findNextEditable

ALL currentEditableNode {
    forEach(child in children(thisGrammarNode)) {
        var aNode = thisTreeFunction([child], thisResultNode, [currentEditableNode]);

        if(not(equals(aNode, null))) {
            return aNode;
        };
    };

    return null;

}

TreeFunction findParentEditable

EDITABLE {
    return thisGrammarNode;
}

ALL {
    if(not(equals(parent(thisGrammarNode), null))) {
        return thisTreeFunction([parent(thisGrammarNode)], thisResultNode);
    } else {
        return null;
    };
}

TreeFunction _findPreviousEditable

ALL currentEditableNode {
    var counter = length(children(thisGrammarNode));
    while(largerThan(counter, 0)) {
        var aNode = thisTreeFunction([childAt(thisGrammarNode, counter)], thisResultNode, [currentEditableNode]);
        if(not(equals(aNode, null))) {
            return aNode;
        };
        counter = minus(counter, 1);
    };
}

TreeFunction findRightNextCursorPos

/* Right From Left */
LEFTCURSOR editable:EDITABLE {
    var firstEditable = findFirstEditable(editable.#2);
    if(not(equals(firstEditable, null))) {
       return firstEditable.#1;
    } else {
        return editable.#3;
    };
}


/* Right From Right*/
RIGHTCURSOR editable:EDITABLE {
    return findRightFromRight(editable);
}

ALL editable:ALL {
    throw("WTF moveCursorRight");
}

TreeFunction cursorCanStayLeft

NFOLDCHILD {
    if(not(equals(indexInParent(parent(thisGrammarNode)),
                  1)
                 )) {

       return false;

    } else {
        return true;
    };
}

ALL {
    return true;
}



TreeFunction cursorCanStayRight

NFOLDCHILD {
    if(not(equals(indexInParent(parent(thisGrammarNode)),
                  length(children(parent(parent(thisGrammarNode))))
                 ))) {

       return false;

    } else {
        return true;
    };
}

ALL {
    return true;
}



TreeFunction _findEditable

EDITABLE currentEditableNode {
    if(equals(thisGrammarNode, currentEditableNode)) {
        foundCurrentEditableNode = true;
        return null;
    } else {
       if(foundCurrentEditableNode) {
          return thisGrammarNode;
       } else {
          return null;
       };
    };
}

ALL currentEditableNode {
    return continue([thisGrammarNode], thisResultNode,
    [currentEditableNode]);
}


TreeFunction doKeyboardHandleEvent

KEYBOARDEVENT {
    /*printString(constructTreeString(thisGrammarNode));
    */
    /*printString(slotNodeName(thisGrammarNode.#1));
    */
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
}


c.CTRL {

    writeToClipboard(constructMarkedTextString());



}




ArrowLeft {
    if(canMoveCursorLeft()) {
        thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    };
}

Shift.ALL{
}

Control.ALL {
}

z.CTRL {
    if(canUndo()) {
        undo();
    };
}


y.CTRL {
    if(canRedo()) {
        redo();
    };
}

ArrowLeft.Shift {
    executeMarkLeftFromCursor();
    redrawCursorInDataTree();
}

ArrowLeft.RAW /* extra parameter hilfreich? */{

    moveCursorLeft();
    redrawDataTree();
}

ArrowRight {
    if(canMoveCursorRight()) {
        thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    };
}

ArrowRight.Shift {
    executeMarkRightFromCursor();
    redrawCursorInDataTree();
}

ArrowRight.RAW   /*Zweiter Parameter hilfreich? */{
    moveCursorRight();
    redrawDataTree();
}


">".RAW {
    forEach(child in markedEditableList) {
        printString(constructTreeString(child));
    };
}

"<".RAW {
    printString(constructTreeString(rootGrammarNode));
    printString(constructTreeString(data));
    printString("Cursor On:");
    printString(constructTreeString(cursor));
    printString(constructTreeString(parent(cursor)));
    printString("");
}

Backspace.RAW {

    printString(toString(isNotEmpty(markedEditableList)));

    var stateFrom = constructStateCopy();

    var succeeded = false;
    if(isNotEmpty(markedEditableList)) {
        succeeded = executeBackspaceDeleteMarkedEditables();
    } else {
        succeeded = executeBackspaceDeleteCommandFromCursor();
    };

    if(succeeded) {
        var stateTo = constructStateCopy();
        addToUndoList(stateFrom, stateTo);
            clearRedo();

    };
}

Delete.RAW {

    var stateFrom = constructStateCopy();

    var succeeded = executeDelDeleteCommandFromCursor();

    if(succeeded) {
        var stateTo = constructStateCopy();
        addToUndoList(stateFrom, stateTo);
            clearRedo();

    };

}


KEYBOARDEVENT.ALL.ALL {

    var fromState = constructStateCopy();

    var executed = executeCharCommandFromCursor(slotNodeName(parent(thisGrammarNode)));



    if(executed) {

        printString("executed");
        var toState = constructStateCopy();

        addToUndoList(fromState, toState);
                clearRedo();


    } else {
        printString("not executed");
    };


}


ALL {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode);
    };
}



TreeFunction checkNeedFocus

INDEX {
    return false;
}
AND {
    if(largerThan(1, length(children(thisGrammarNode)))) {
        return true;
    } else {
        return thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    };
}

NFOLD {
    return true;
}




ALL {
    forEach(child in children(thisGrammarNode)) {
        var needFocus = thisTreeFunction([child], thisResultNode);
        if(needFocus) {
            return true;
        };
    };
    return false;
}

TreeFunction _constructDataTree

REGEX editable {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode, [editable]);
}

OR child:OR.NULL editable{

   var orNull = #"OR" [
                   +#"NULL";
                ];


   generateEditableIfPossible(orNull, thisResultNode, editable);
}

OR child:OR.ALL editable {
    var or = #"OR" [
      thisTreeFunction([child], thisResultNode, [false]);
    ];

    generateEditableIfPossible(or, thisResultNode, editable);

}

OR editable{
    thisTreeFunction([thisGrammarNode, thisGrammarNode.#1], thisResultNode, [editable]);
}



OPT child:OPT.NULL editable{
    var optnull =  #"OPT" [
                     +#"NULL";
                    ];

  generateEditableIfPossible(optnull, thisResultNode, editable);

}

OPT child:OPT.ALL editable{
    var opt =  #"OPT" [
                    thisTreeFunction([child], thisResultNode, [false]);

                 ];

          generateEditableIfPossible(opt, thisResultNode, editable);

}

OPT editable{
    thisTreeFunction([thisGrammarNode, thisGrammarNode.#1], thisResultNode, [editable]);
}




isNFOLD_1() editable {
        +#"PLUS" {
            forEach(child in children(thisGrammarNode)) {
                +#"EDITABLE" {
                    +#"LEFTCURSOR";
                    +#"NFOLDCHILD" {
                        thisTreeFunction([child], thisResultNode, [false]);
                    };
                    +#"RIGHTCURSOR";
                    +#"MARKED" {
                        +#"FALSE";
                    };
                };
            };
        };
}

isNFOLD_0() editable{
    thisTreeFunction([thisGrammarNode, thisGrammarNode], thisResultNode, [editable]);
}

isNFOLD_0() nfold:hasNoChildren() editable{
   var starnull = #"STAR" [
                    ];
   generateEditableIfPossible(starnull, thisResultNode, editable);

}
isNFOLD_0() nfold:ALL editable {

    var star = #"STAR" [
            forEach(child in children(thisGrammarNode)) {
                +#"EDITABLE" {
                    +#"LEFTCURSOR";
                    +#"NFOLDCHILD" {
                        thisTreeFunction([child], thisResultNode, [false]);
                    };
                    +#"RIGHTCURSOR";
                    +#"MARKED" {
                        +#"FALSE";
                    };
                };
            };

    ];

    /*+star;*/
    generateEditableIfPossible(star, thisResultNode, editable);

}

GROUP editable{
    thisTreeFunction([thisGrammarNode, thisGrammarNode.#1], thisResultNode, [editable]);
}

GROUP child:GROUP.NULL editable{
    var groupnull = #"GROUP" [
                        +#"NULL";
                    ];

  generateEditableIfPossible(groupnull, thisResultNode, editable);

}

GROUP child:GROUP.ALL editable {
    var group = #"GROUP" [
                    +#"Letter" {
                        +#slotNodeName(child.#1);
                    };
                ];

         generateEditableIfPossible(group, thisResultNode, editable);


}



ANY_EXCEPT child:ANY_EXCEPT.NULL editable {
    var any_exceptnull = #"ANY_EXCEPT" [
                            +#"NULL";
                        ];

      generateEditableIfPossible(any_exceptnull, thisResultNode, editable);

}

ANY_EXCEPT child:ANY_EXCEPT.TOKEN editable {
    var any_except = #"ANY_EXCEPT" [
                        +#"Letter" {
                            +#slotNodeName(child.#1);
                        };
                    ];

  generateEditableIfPossible(any_except, thisResultNode, editable);

}

ANY_EXCEPT editable {
    thisTreeFunction([thisGrammarNode, thisGrammarNode.#1], thisResultNode, [editable]);
}

SLOPETOKEN editable{
    var letter = #"Letter" [
                    +#slotNodeName(thisGrammarNode.#1);
                 ];

    generateEditableIfPossible(letter, thisResultNode, editable);
}

AND editable{
    var trueEditable = editable;
    if(largerThan(length(children(thisGrammarNode)), 1)) {
        trueEditable = true;
        +#"AND" {
            forEach(child in children(thisGrammarNode)) {
                thisTreeFunction([child], thisResultNode, [trueEditable]);
            };
        };
    }
    else {
        forEach(child in children(thisGrammarNode)) {
            thisTreeFunction([child], thisResultNode, [trueEditable]);
        };
    };
}

ALL editable {
    forEach(child in children(thisGrammarNode)) {
       thisTreeFunction([child], thisResultNode, [editable]);
    };
}

TreeFunction _constructHTMLTree

EDITABLE {
    +#"editableThing" {
        addCursorIfNeeded(thisGrammarNode, thisResultNode);

        if(isMarked(thisGrammarNode)) {
            addClass(thisResultNode, "marked");
        };

        thisTreeFunction([thisGrammarNode, thisGrammarNode.#2], thisResultNode);
    };
}


EDITABLE thing:EDITABLE.STARNULL {

    addClass(thisResultNode, "NULL opt");
    +#"TEXT" {
            +#"&nbsp;";
        };

}



EDITABLE thing:EDITABLE.Letter {
    addClass(thisResultNode, "unbordered");
    thisTreeFunction([thing], thisResultNode);
}


EDITABLE anything:EDITABLE.ALL {
    thisTreeFunction([anything], thisResultNode);
}


OR.NULL {
    addClass(thisResultNode, "NULL");
    +#"TEXT" {
        +#"&nbsp;";
    };
}

OPT.NULL {
    addClass(thisResultNode, "NULL opt");
    +#"TEXT" {
        +#"&nbsp;";
    };
}

GROUP.NULL {
    addClass(thisResultNode, "NULL");
    +#"TEXT" {
        +#"&nbsp;";
    };
}

ANY_EXCEPT.NULL {
    addClass(thisResultNode, "NULL");
    +#"TEXT" {
        +#"&nbsp;";
    };
}

OR {
    continue([thisGrammarNode], thisResultNode);
}

OPT {
        continue([thisGrammarNode], thisResultNode);
}


PLUS {
    +#"NFOLD" {
        +#"boxDescriptor" {
            +#"boxDescriptorText" {
                +#"TEXT" {
                    +#"1-n";
                };
            };
        };
        forEach(child in children(thisGrammarNode)) {
                thisTreeFunction([child], thisResultNode);
        };
    };
}



isDataStarEmpty() {
    addClass(thisResultNode, "NULL opt");
    +#"TEXT" {
        +#"&nbsp;";
    };
}

STAR {
    +#"NFOLD" {
        +#"boxDescriptor" {
            +#"boxDescriptorText" {
                +#"TEXT" {
                    +#"0-n";
                };
            };
        };
        forEach(child in children(thisGrammarNode)) {
                thisTreeFunction([child], thisResultNode);
        };
    };
}

GROUP {
        continue([thisGrammarNode], thisResultNode);
}

ANY_EXCEPT {
    continue([thisGrammarNode], thisResultNode);
}

Letter {
    +#"letter" {
        +#"TEXT" {
            +#slotNodeName(thisGrammarNode.#1);
        };
    };
}


ALL {/*
    printString("Whats missing?");
    printString(slotNodeName(thisGrammarNode));*/
    continue([thisGrammarNode], thisResultNode);
}


TreeFunction _constructSMALLHTMLTree

EDITABLE {
    +#"editableThing unbordered" {
        var needFocus = false;

        /*
            cuz source of nfoldchild is nfold...
        */
        if(slotNodeNameEquals(thisGrammarNode.#2, "NFOLDCHILD")) {
            /*printString("check need focus..");
            */
            needFocus = checkNeedFocus(slotOriginNode(thisGrammarNode.#2.#1));
        } else {
            needFocus = checkNeedFocus(slotOriginNode(thisGrammarNode));
        };



        addCursorIfNeededWithFocus(thisGrammarNode, thisResultNode, needFocus);

        if(isMarked(thisGrammarNode)) {
            addClass(thisResultNode, "marked");
        };


        if(not(needFocus)) {
            addClass(thisResultNode, "unbordered");
        };

        thisTreeFunction([thisGrammarNode, thisGrammarNode.#2], thisResultNode);
    };
}

isDataStarEmpty() {
    addClass(thisResultNode, "NULL opt");
    +#"TEXT" {
        +#"&nbsp;";
    };
}

EDITABLE thing:EDITABLE.Letter {
    addClass(thisResultNode, "unbordered");
    thisTreeFunction([thing], thisResultNode);
}


EDITABLE anything:EDITABLE.ALL {
    thisTreeFunction([anything], thisResultNode);
}

OR.NULL {
    addClass(thisResultNode, "NULL");
    +#"TEXT" {
        +#"&nbsp;";
    };
}



OR {
    continue([thisGrammarNode], thisResultNode);
}






OPT {
        continue([thisGrammarNode], thisResultNode);
}


PLUS {
    +#"NFOLD unbordered" {
        +#"boxDescriptor" {
            +#"boxDescriptorText" {
                +#"TEXT" {
                    +#"1-n";
                };
            };
        };
        forEach(child in children(thisGrammarNode)) {
                thisTreeFunction([child], thisResultNode);
        };
    };
}


STAR {
    +#"NFOLD unbordered" {
        +#"boxDescriptor" {
            +#"boxDescriptorText" {
                +#"TEXT" {
                    +#"0-n";
                };
            };
        };
        forEach(child in children(thisGrammarNode)) {
                thisTreeFunction([child], thisResultNode);
        };
    };
}





GROUP {
        continue([thisGrammarNode], thisResultNode);
}

GROUP.NULL {
    addClass(thisResultNode, "NULL opt");
    +#"TEXT" {
        +#"&nbsp;";
    };
}



ANY_EXCEPT {
    continue([thisGrammarNode], thisResultNode);
}

Letter {
    +#"letter" {
        +#"TEXT" {
            +#slotNodeName(thisGrammarNode.#1);
        };
    };
}

LEFTCURSOR {
    if(equals(thisGrammarNode, cursor)) {
        +#"cursor left" {
            +#"TEXT" {
                +#"|";
            };
        };
    };
}

RIGHTCURSOR {
    if(equals(thisGrammarNode, cursor)) {
        +#"cursor right" {
            +#"TEXT" {
                +#"|";
            };
        };
    };
}

CHILDREN {
    continue([thisGrammarNode], thisResultNode);
}

ALL {/*
    printString("Whats missing?");
    printString(slotNodeName(thisGrammarNode));*/
    continue([thisGrammarNode], thisResultNode);
}



TreeFunction executeCharCommandRecursive

AND char {
    if(not(equals(thisTreeFunction([thisGrammarNode.#1], thisResultNode, [char]),
                  null))) {
        return thisGrammarNode;
    } else {
        return null;
    };
}

SLOPETOKEN char {
    if(equals(slotNodeName(thisGrammarNode.#1),
              char)) {
        return thisGrammarNode;
    } else {
        return null;
    };
}

GROUP char {
    if(canSetCharGroup(thisGrammarNode, char)) {
        setCharGroup(thisGrammarNode, char);
        return thisGrammarNode;
    } else {
        return null;
    };
}

ANY_EXCEPT char {
    if(canSetCharAnyExcept(thisGrammarNode, char)) {
        setCharAnyExcept(thisGrammarNode, char);
        return thisGrammarNode;
    } else {
        return null;
    };
}

OPT char {
    return thisTreeFunction([thisGrammarNode, thisGrammarNode.#1], thisResultNode, [char]);
}

OPT child:NULL char{
    var temp = thisTreeFunction([createSingleChildTemplate(thisGrammarNode)],
                                thisResultNode,
                                [char]);
    if(not(equals(temp, null))) {
        insertOPTChild(thisGrammarNode, temp);
        return thisGrammarNode;
    } else {
        return null;
    };
}

OPT child:ALL char{
    return null;
}

BRACKET char {
    var child = thisTreeFunction([thisGrammarNode.#1], thisResultNode, [char]);
    if(not(equals(child, null))) {
        return thisGrammarNode;
    } else {
        return null;
    };
}


OR char {
    return thisTreeFunction([thisGrammarNode, thisGrammarNode.#1], thisResultNode, [char]);
}


OR orChild:NULL char {

    var alt = 1;

    while(smallerEqualsThan(alt, numAlternatives(thisGrammarNode))) {
        var temp = thisTreeFunction([createORChildTemplateAt(thisGrammarNode, alt)],
                                    thisResultNode,
                                    [char]);
        if(not(equals(temp, null))) {
            insertORChild(thisGrammarNode, temp, alt);
            return thisGrammarNode;
        };

        alt = plus(alt, 1);
    };

    return null;
}


OR orChild:ALL char {
    return null;
}


isNFOLD_1() char {
    var child = thisTreeFunction([thisGrammarNode.#1], thisResultNode, [char]);
    if(not(equals(child, null))) {
        return thisGrammarNode;
    } else {
        return null;
    };
}

isNFOLD_0() char {
    var temp = ->(executeCharCommandRecursive)([createNFOLDChildTemplate(thisGrammarNode)],
                                              rootResultNode,
                                              [char]);

    if(not(equals(temp, null))) {
        insertNFOLDChildAt(thisGrammarNode, temp, 1);
        return thisGrammarNode;
    } else {
        return null;
    };
}



ALL char {
    if(not(equals(thisTreeFunction([thisGrammarNode.#1], thisResultNode, [char]),
                  null))) {
        return thisGrammarNode;
    } else {
        return null;
    };
}

TreeFunction executeCharCommandFromCursor
/*
ALL dataNode:ALL slope:ALL char {
    printString("Hi:)");
    printString(constructTreeString(thisGrammarNode));
    printString(constructTreeString(dataNode));
    printString(constructTreeString(slope));
    printString(char);
}*/

LEFTCURSOR dataNode:isDataNFOLD() slope:NFOLD char {
    /*printString("Hallo");
    */
    var temp = executeCharCommandRecursive(createNFOLDChildTemplate(slope), char);

    if(not(equals(temp, null))) {
        insertNFOLDChildAt(slope, temp, 1);

        redrawDataTree();


        var redrawnNFOLD = parent(cursor).#2;
        var addedChildEditable = redrawnNFOLD.#1;
        var firstEditable = findDeepestFirstEditable(addedChildEditable.#2);
        if(not(equals(firstEditable, null))) {
            setCursorTo(firstEditable.#3);
        }
        else {
            setCursorTo(addedChildEditable.#3);
            moveCursorRightIfCantStay();
        };




        return true;
    };

    return false;
}


LEFTCURSOR dataNode:NFOLDCHILD slope:NFOLD char {


    var editableIndex = indexInParent(parent(dataNode));
        var slopeChild = childAt(slope, editableIndex);
    if(->(isFirstNull)([slopeChild], thisResultNode)) {
        var changedSlope = executeCharCommandRecursive(slope.#1, char);

        if(not(equals(changedSlope, null))) {
                redrawDataTree();
                var redrawnEditable = parent(cursor);

                setCursorTo(findDeepestFirstEditable(redrawnEditable).#3);
                moveCursorRightIfCantStay();

                return true;
        } else {
                return false;
        };

    };

    var temp = executeCharCommandRecursive(createNFOLDChildTemplate(slope), char);

    if(not(equals(temp, null))) {
        insertNFOLDChildAt(slope, temp, indexInParent(parent(dataNode)));
        redrawDataTree();

        /*
            After Redraw, cursor is automatically on right cursor of the INSERTED editable,
            because the path has not been changed,
            but that path is now occupied by the inserted editable
        */
        var insertedEditable = parent(cursor);
        setCursorTo(findDeepestFirstEditable(insertedEditable).#3);
        return true;
    };

    return false;
}

RIGHTCURSOR dataNode:NFOLDCHILD slope:NFOLD char {
    var temp = executeCharCommandRecursive(createNFOLDChildTemplate(slope), char);
    if(not(equals(temp, null))) {
        insertNFOLDChildAt(slope, temp, plus(indexInParent(parent(dataNode)),1));
        redrawDataTree();

        /*
            Here, the new editable is added AFTER the current position,
            so after redraw we need to go one node further to retrieve it.
        */
        var addedEditable = nextNode(parent(cursor));
        setCursorTo(findDeepestFirstEditable(addedEditable).#3);
        return true;
    } else {
        printString("HERE");

        var nextCursorPos = findRightNextCursorPos();

        if(not(equals(nextCursorPos, null))) {
            printString("found next cursorPos");
            setCursorTo(nextCursorPos);
            var succeeded = executeCharCommandFromCursor(char);
            printString("executed Char Command");
            if(equals(succeeded, true)) {
                    return true;
            } else {
                    setCursorTo(thisGrammarNode);
                    return false;
            };



        } else {
            return false;
        };

    };

}

RIGHTCURSOR dataNode:ALL slope:ALL char {
    if(canMoveCursorRight()) {
        moveCursorRight();
        return thisTreeFunction([cursor, parent(cursor).#2, slotOriginNode(cursor)], rootResultNode, [char]);
    };

    return false;
}

LEFTCURSOR dataNode:ALL slope:ALL char{
    var temp = executeCharCommandRecursive(slope, char);

    if(not(equals(temp, null))) {
        redrawDataTree();

        while(not(slotNodeNameEquals(cursor, "RIGHTCURSOR"))) {
            moveCursorRight();
        };

        return true;
    };

    return false;
}



ALL slope:ALL char {
    printString("lol wtf executeCharCommandFromCursor");
    return null;
}


TreeFunction isFirstNull

OR.NULL {
    return true;
}

GROUP.NULL {
    return true;
}

ANY_EXCEPT.NULL {
    return true;
}

ALL {
    if(hasChildren(thisGrammarNode)) {
        return thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    } else {
        return false;
    };
}


TreeFunction executeCharCommandRecursive

AND char {
    if(not(equals(thisTreeFunction([thisGrammarNode.#1], thisResultNode, [char]),
                  null))) {
        return thisGrammarNode;
    } else {
        return null;
    };
}

SLOPETOKEN char {
    if(equals(slotNodeName(thisGrammarNode.#1),
              char)) {
        return thisGrammarNode;
    } else {
        return null;
    };
}

GROUP char {
    if(canSetCharGroup(thisGrammarNode, char)) {
        setCharGroup(thisGrammarNode, char);
        return thisGrammarNode;
    } else {
        return null;
    };
}

ANY_EXCEPT char {
    if(canSetCharAnyExcept(thisGrammarNode, char)) {
        setCharAnyExcept(thisGrammarNode, char);
        return thisGrammarNode;
    } else {
        return null;
    };
}

OPT char {
    return thisTreeFunction([thisGrammarNode, thisGrammarNode.#1], thisResultNode, [char]);
}

OPT child:NULL char{
    var temp = thisTreeFunction([createSingleChildTemplate(thisGrammarNode)],
                                thisResultNode,
                                [char]);
    if(not(equals(temp, null))) {
        insertOPTChild(thisGrammarNode, temp);
        return thisGrammarNode;
    } else {
        return null;
    };
}

OPT child:ALL char{
    return null;
}

BRACKET char {
    var child = thisTreeFunction([thisGrammarNode.#1], thisResultNode, [char]);
    if(not(equals(child, null))) {
        return thisGrammarNode;
    } else {
        return null;
    };
}


OR char {
    return thisTreeFunction([thisGrammarNode, thisGrammarNode.#1], thisResultNode, [char]);
}


OR orChild:NULL char {

    var alt = 1;

    while(smallerEqualsThan(alt, numAlternatives(thisGrammarNode))) {
        var temp = thisTreeFunction([createORChildTemplateAt(thisGrammarNode, alt)],
                                    thisResultNode,
                                    [char]);
        if(not(equals(temp, null))) {
            insertORChild(thisGrammarNode, temp, alt);
            return thisGrammarNode;
        };

        alt = plus(alt, 1);
    };

    return null;
}


OR orChild:ALL char {
    return null;
}


isNFOLD_1() char {
    var child = thisTreeFunction([thisGrammarNode.#1], thisResultNode, [char]);
    if(not(equals(child, null))) {
        return thisGrammarNode;
    } else {
        return null;
    };
}

isNFOLD_0() char {
    var temp = ->(executeCharCommandRecursive)([createNFOLDChildTemplate(thisGrammarNode)],
                                              rootResultNode,
                                              [char]);

    if(not(equals(temp, null))) {
        insertNFOLDChildAt(thisGrammarNode, temp, 1);
        return thisGrammarNode;
    } else {
        return null;
    };
}



ALL char {
    if(not(equals(thisTreeFunction([thisGrammarNode.#1], thisResultNode, [char]),
                  null))) {
        return thisGrammarNode;
    } else {
        return null;
    };
}

TreeFunction deleteEditable

STAR.EDITABLE.NFOLDCHILD {
    removeNFOLDChildAt(slotOriginNode(thisGrammarNode), indexInParent(parent(thisGrammarNode)));

    removeNode(parent(thisGrammarNode));
}

PLUS.EDITABLE.NFOLDCHILD {
    if(not(equals(1, length(children(parent(parent(thisGrammarNode))))))) {
        removeNFOLDChildAt(slotOriginNode(thisGrammarNode), indexInParent(parent(thisGrammarNode)));
        removeNode(parent(thisGrammarNode));
    };
}

ALL {
    ->(executeDeleteCommandRecursive)([slotOriginNode(thisGrammarNode)], thisResultNode);
}

TreeFunction executeBackspaceDeleteCommandFromCursor

RIGHTCURSOR dataNode:isDataNFOLDFilled() slope:NFOLD char {
    clearNFOLD_0(slope);
    redrawDataTree();
    moveCursorLeft();
    return true;
}

RIGHTCURSOR dataNode:NFOLDCHILD slopeNode:ALL {
    if(and(isNFOLD_1(slopeNode),
           equals(length(children(slopeNode)), 1)))
    {
        return false;
    };


    if(equals(length(children(slopeNode)), 1)) {
        setCursorTo(findParentEditable(parent(parent(cursor))).#1);
        printString(constructTreeString(currentRootGrammarNode));

    } else {

        var editable = parent(cursor);
        if(equals(1, indexInParent(editable))) {
            setCursorTo(editable.#1);
        } else {
            setCursorTo(previousNode(editable).#3);
        };


    };

    removeNFOLDChildAt(slopeNode, indexInParent(parent(dataNode)));

    redrawDataTree();

    moveCursorRightIfCantStay();

    return true;
}



RIGHTCURSOR dataNode:ALL slopeNode:ALL {
    var deleted = ->(executeDeleteCommandRecursive)([slopeNode], rootResultNode);

    redrawDataTree();

    moveCursorLeft();
    if(canMoveCursorLeft()) {
        moveCursorLeft();
    };

    redrawDataTree();


    return not(equals(deleted, null));
}

LEFTCURSOR dataNode:ALL slopeNode:ALL {
    if(canMoveCursorLeft()) {
        moveCursorLeftForceStay();
        return executeBackspaceDeleteCommandFromCursor();
    } else {
        return false;
    };


}



TreeFunction executeDelDeleteCommandFromCursor

LEFTCURSOR dataNode:isDataNFOLDFilled() slope:NFOLD char {
    clearNFOLD_0(slope);
    redrawDataTree();
    return true;
}
LEFTCURSOR dataNode:NFOLDCHILD slopeNode:ALL {
    if(and(isNFOLD_1(slopeNode),
           equals(length(children(slopeNode)), 1)))
    {
        return false;
    };


    if(equals(length(children(slopeNode)), 1)) {
        setCursorTo(findParentEditable(parent(parent(cursor))).#1);
        removeNFOLDChildAt(slopeNode, indexInParent(parent(dataNode)));

    } else {
        var index = indexInParent(parent(dataNode));
        if(equals(index, length(children(slopeNode)))) {
            moveCursorLeftForceStay();
        };
        removeNFOLDChildAt(slopeNode, indexInParent(parent(dataNode)));

    };


    redrawDataTree();

    return true;
}


LEFTCURSOR dataNode:ALL slopeNode:ALL {
    var deleted = ->(executeDeleteCommandRecursive)([slopeNode], rootResultNode);
    redrawDataTree();

    return not(equals(deleted, null));
}

RIGHTCURSOR dataNode:ALL slopeNode:ALL {
    if(canMoveCursorRight()) {
        moveCursorRight();
        return executeDelDeleteCommandFromCursor();
    };


}



TreeFunction executeDeleteCommandRecursive

OR {
    return thisTreeFunction([thisGrammarNode, thisGrammarNode.#1], thisResultNode);
}

OR child:OR.NULL {
    return null;
}

OR child:OR.ALL {
    deleteNode(thisGrammarNode);
    return thisGrammarNode;
}

OPT {
    return thisTreeFunction([thisGrammarNode, thisGrammarNode.#1], thisResultNode);
}

OPT child:OPT.ALL {
    deleteNode(thisGrammarNode);
    return thisGrammarNode;
}

OPT child:OPT.NULL {
    return null;
}

AND {
    return null;
}

GROUP {
    deleteChar(thisGrammarNode);
    return thisGrammarNode;
}

ANY_EXCEPT {
    deleteChar(thisGrammarNode);
    return thisGrammarNode;
}

NFOLD {
    return null;
}

BRACKET {
    return null;
}

SLOPETOKEN {
    return null;
}

TreeFunction executeCommandNode

COMMAND {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
}

CHAR {
    var char = slotNodeName(thisGrammarNode.#1);
    cursorIndexPath = slotNodeName(thisGrammarNode.#2);
    setCursorPointerFromPath();
    executeCharCommandFromCursor(char);
}

BACKSPACE {
    cursorIndexPath = slotNodeName(thisGrammarNode.#1);
    setCursorPointerFromPath();
    executeBackspaceDeleteCommandFromCursor();
}



TreeFunction constructMarkedTextString

EDITABLE {
    if(isMarked(thisGrammarNode)) {
        ->(appendTextContent)([thisGrammarNode.#2], thisResultNode);
    } else {
        forEach(child in children(thisGrammarNode)) {
            thisTreeFunction([child], thisResultNode);
        };
    };
}

ALL {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode);
    };
}


TreeFunction appendTextContent

Letter{
    markedTextString = concat(markedTextString, slotNodeName(thisGrammarNode.#1));
}

ALL {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode);
    };
}

''');
exampleStrings.add('''JHA''');
  return exampleStrings;
}
}
