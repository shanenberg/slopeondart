/***********************************************************
I am purely generated!!!! Don't touch me, touch the slp files 
***************************************************************/
      import '../../../html/examples/HTMLExample.dart';
class miniHtml extends HTMLExample {
   List<String> getExampleProjectStrings() {
  List<String> exampleStrings = new List<String>();
exampleStrings.add('''


gutantag

Rules: hi

hi => "HELLO"+.

Scanner:

whitespace => SEPARATOR "\\r|\\n| " BOTTOM.



''');
exampleStrings.add('''


load library util;
load library htmlConstruction;



var htmlString;
var nodesToHTML;



startOpenTag aNode aNodeList {

    htmlString = concatAll(htmlString, ["<",
                                      slotNodeName(aNode),
                                      " ",
                                      html_generateIDString(aNode, aNodeList)
                                      ]
                        );

}

addAttribute attName attValue {
    htmlString = concatAll(htmlString, [
                                " ",
                                attName,
                                "='",
                                attValue,
                                "'"
                           ]
                 );
}

endOpenTag {
    htmlString = concat(htmlString, ">");
}


closeTag aNode {
    htmlString = concat(htmlString, html_createCloseTagFromNode(aNode));
}



main {
    redraw();
}



handleEvent anEventTreeNode anID anHTMLObject {
    if (isNotNull(anID)) {
        var slotTreeNode = elementAt(nodesToHTML, anID);
        ->(doMouseHandleEvent)([slotTreeNode, anEventTreeNode], rootResultNode, [anHTMLObject]);
    } else {
        ->(doKeyboardHandleEvent)([anEventTreeNode], rootResultNode, [anHTMLObject]);
    };
}



redraw {
        htmlString = "";
        nodesToHTML = [];

        var node
           =
        #"DIV" [
            +#"att" {
                +pairNode("class", "hallo");
            };
            +#"H1" {
                +#"att" {
                    +pairNode("class", "hallo");
                };
                +#"text" [
                    +#"Guten Morgähn";
                ];
            };

        ];

        ->(constructHTMLString )([node], rootResultNode, [nodesToHTML]);
       printString(htmlString);
        return htmlString;
}

pairNode parentValue childValue {
    var parentNode = #parentValue;
    addSlotNode(parentNode, #childValue);
    return parentNode;
}

TreeFunction constructHTMLString

text aNodeList {
    htmlString =
        concat(htmlString, slotNodeName(thisGrammarNode.#1));
}

ROOT aNodeList {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode, [aNodeList]);
}

att.ALL {
    /*htmlString = concatAll(htmlString, [" ", slotNodeName(thisGrammarNode),
                                        "='",
                                        slotNodeName(thisGrammarNode.#1),
                                        "'"
                                     ]
                 );*/
    addAttribute(slotNodeName(thisGrammarNode), slotNodeName(thisGrammarNode.#1));
}


ALL aNodeList {

    if(and(
        largerThan(length(children(thisGrammarNode)), 0),
        equals(slotNodeName(thisGrammarNode.#1), "att")
       )
    ) {
        printString(concat("In ", slotNodeName(thisGrammarNode)));
        startOpenTag(thisGrammarNode, aNodeList);
            thisTreeFunction([thisGrammarNode.#1], thisResultNode);
        endOpenTag();

        printString(htmlString);

        var i = 2;
        while(smallerEqualsThan(i, length(children(thisGrammarNode)))) {
            thisTreeFunction([childAt(thisGrammarNode, i)], thisResultNode, [aNodeList]);
            i = plus(i, 1);
        };

        closeTag(thisGrammarNode);

    };



}

''');
exampleStrings.add('''HELLO''');
  return exampleStrings;
}
}
