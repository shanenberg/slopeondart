import '../../../../lib/slot/runtime/SLOTHTMLProject.dart';
import '../../../html/examples/SLOTProjectConstructor.dart';
import 'miniHtml.slp.dart';

main() {

  SLOTHTMLProject project =
  new SLOTProjectConstructor().constructHTMLProject(new miniHtml());
  var slotResult = project.runProject();

}
