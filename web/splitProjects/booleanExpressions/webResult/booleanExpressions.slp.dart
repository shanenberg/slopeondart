/***********************************************************
I am purely generated!!!! Don't touch me, touch the slp files 
***************************************************************/
      import '../../../html/examples/HTMLExample.dart';
class booleanExpressions extends HTMLExample {
   List<String> getExampleProjectStrings() {
  List<String> exampleStrings = new List<String>();
exampleStrings.add('''


Boolean
Rules: orAble

orAble => andAble orPart*.
orPart => "OR" andAble.

andAble => bracketAble andPart*.
andPart => "AND" bracketAble.

bracketAble => "\\(" orAble "\\)" | exp.

exp => "NOT"? literal expRight?.
expRight => ("=" | "!=" | ">" | "<") literal.


literal => identifier | number.


Scanner:

identifier => "([A-Z]|[a-z]|_)([A-Z]|[a-z]|[0-9]|_)*" BOTTOM.
number => "[1-9][0-9]*" BOTTOM.
whitespace => SEPARATOR " |\\n|\\r|\\t" BOTTOM.

Tests:

T1 "A = B".
T1 "A != B".
T1 "A = B OR B = C AND (F = J OR I)".





''');
exampleStrings.add('''


load library util;
load library htmlConstruction;



var dffText;



var genericTreeGrammar;


/*
var genericTreeGrammar;
*/


var htmlString;
var nodesToHTML;

var htmlTree;


var dataTree;





var answerGrammar;



addClass resultNode className {
    setNodeName(resultNode,
                    concatAll(slotNodeName(resultNode),
                              [" ", className]
                             )
               );
}

constructDFFText aNode {
    dffText = "";
    ->(constructDFFTextTree)([aNode], thisResultNode, ["ROOT"]);
     return dffText;
}



initConstructFromTreeString {
    genericTreeGrammar = parseSlopeGrammar("

        Generic
        Rules: treeNode

        treeNode => ~"#~" value ~"\\(~" treeNode* ~"\\)~".

        value => (identifier | number | boolean).

        identifier => IDENTIFIER.
        number => NUMBER.
        boolean => ~"true|false~".

        Scanner:

        NUMBER => ~"[1-9][0-9]*|0~".
        whitespace => SEPARATOR ~"\\n|\\r| ~" BOTTOM.
        IDENTIFIER => ~"([a-z]|[A-Z]|_)([a-z]|[A-Z]|[0-9]|_)*~" BOTTOM.

    ");
}

testConstructFromTreeString {
    var tree = constructFromTreeString("#SELECT()");
}

constructFromTreeString treeString {
    if(equals(treeString, null)) {
        return null;
    };
    var grammarTree = parseSlopeWordFromGrammar(treeString, genericTreeGrammar);
    var lolTree = ->(t_constructFromTreeString)([grammarTree], rootResultNode);
    return lolTree;
}




/*
initConstructFromTreeString {
    genericTreeGrammar = parseSlopeGrammar("

        Generic
        Rules: treeNode

        treeNode => ~"#~" value ~"\\(~" treeNode* ~"\\)~".

        value => (identifier | number | boolean).

        identifier => IDENTIFIER.
        number => NUMBER.
        boolean => ~"true|false~".

        Scanner:


        NUMBER => ~"[1-9][0-9]*~".

        whitespace => SEPARATOR ~"\\n|\\r| ~" BOTTOM.
        IDENTIFIER => ~"([a-z]|[A-Z]|_)([a-z]|[A-Z]|[0-9]|_)*~" BOTTOM.

    ");
}

testConstructFromTreeString {
    var tree = constructFromTreeString("#SELECT()");
    printString(constructTreeString(tree));
}

constructFromTreeString treeString {
    var grammarTree = parseSlopeWordFromGrammar(treeString, genericTreeGrammar);
    printString(constructTreeString(grammarTree));
    var root = #"";
    ->(constructFromTreeString | t_constructFromTreeString)([grammarTree], root);
    return root.#1;
}

*/



constructHTMLString aNode aNodeList {
    htmlString = "";
    ->(_constructHTMLString)([aNode], rootResultNode, [aNodeList]);
    return htmlString;
}

startOpenTag aNode aNodeList {

    htmlString = concatAll(htmlString, ["<",
                                      slotNodeName(aNode),
                                      " ",
                                      html_generateIDString(aNode, aNodeList)
                                      ]
                 );

}

startDivTagClass aNode aNodeList {
    htmlString = concatAll(htmlString, ["<div class='",
                                      slotNodeName(aNode),
                                      "' ",
                                      html_generateIDString(aNode, aNodeList)
                                      ]
                 );

}

addAttribute attName attValue {
    htmlString = concatAll(htmlString, [
                                " ",
                                attName,
                                "='",
                                attValue,
                                "'"
                           ]
                 );
}

endOpenTag {
    htmlString = concat(htmlString, ">");
}


closeTag aNode {
    htmlString = concat(htmlString, html_createCloseTagFromNode(aNode));
}

closeTagString aString {
    htmlString = concatAll(htmlString, ["</", aString, "> "]);
}

nodeListToString nodeList {
    var theString = "";
    forEach(child in children(nodeList)) {
        theString = concatAll(theString, ["#", slotNodeName(child)]);
    };
    return theString;
}




constructRuleString aNode {
    var ruleTree = #"ROOT";
    ->(constructRuleTree)([aNode], ruleTree);
    return constructDFFText(ruleTree);
}



constructTreeString node {
    if(equals(node, null)) {
        return "null";
    };
    var stringTree = #"";
    ->(t_constructTreeString)([node], stringTree, [0]);
    return constructDFFText(stringTree);
}



equalTrees tree1 tree2 {
if(or(and(equals(tree1, null),
              not(equals(tree2, null))),
          and(equals(tree2, null),
              not(equals(tree1, null))))) {

        return false;

  };

  if(and(equals(tree1, null),
        equals(tree2, null))) {

        return true;
        };

    return ->(t_equalTrees)([tree1, tree2], rootResultNode);
}


testEqualTrees {

    var a = #"HI";
    addSlotNode(a, #"HI3");

    var b = #"HI";
    addSlotNode(b, #"HI3");

    printString(toString(equalTrees(a, b)));

    var hi = #"HI";
    addSlotNode(hi, #true);

    var hi2 = #"HI";
    addSlotNode(hi2, #true);

    printString(toString(equalTrees(hi, hi2)));
}



getTokenString node {
    return ->(getTokenString)([node], rootResultNode);
}



constructDataTree node {
    var expression =
    #"TASK" [
       +#"EXPRESSION" {
            ->(constructDataTree)([node], thisResultNode);
       };
       +#"INPUT" {
       };
    ];
    return expression;
}



constructHTMLTree node {
    var root = #"expression";
    ->(constructHTMLTree)([node], root);
    return root;
}






main {
        nodesToHTML = [];
        return redraw();
}

redraw {
    printString(constructTreeString(rootGrammarNode));

    dataTree = constructDataTree(rootGrammarNode);
    printString(constructTreeString(dataTree));

    htmlTree = constructHTMLTree(dataTree);
    printString(constructTreeString(htmlTree));

    htmlString = constructHTMLString(htmlTree, nodesToHTML);

    return htmlString;
}


initAnswerGrammar {
    answerGrammar = parseSlopeGrammar("

        Answer
        Rules: start

        start => (var | notVar)+.
        var => IDENTIFIER.
        notVar => ~"NOT~" IDENTIFIER.


        Scanner:

        whitespace => SEPARATOR ~" |\n|\r~" BOTTOM.
        IDENTIFIER => ~"([A-Z]|[a-z]|_)([0-9]|[A-Z]|[a-z]|_)*~" BOTTOM.


    ");



}


parseAnswer answerText {
    var slopeTree = parseSlopeWordFromGrammar(answerText, answerGrammar);

    return slopeTree;
}

TreeFunction constructDFFTextTree

ALL exceptString {
   var stringToAppend = "";
   if(not(equals(slotNodeName(thisGrammarNode), exceptString))) {
       stringToAppend = toString(slotNodeName(thisGrammarNode));
   };
   dffText = concat(dffText, stringToAppend);


   forEach(aChild in children(thisGrammarNode)) {
        thisTreeFunction([aChild], thisResultNode, [exceptString]);
   };
}

TreeFunction t_constructFromTreeString

treeNode.AND {
    var node = thisTreeFunction([thisGrammarNode.#2], thisResultNode);

    forEach(child in children(thisGrammarNode.#4)) {
        addSlotNode(node, thisTreeFunction([child], thisResultNode));
    };
    return node;
}

identifier.AND {

    return #thisTreeFunction([thisGrammarNode.#1], thisResultNode);
}

number.AND {

    return #stringToInt(
            thisTreeFunction([thisGrammarNode.#1], thisResultNode)
           );
}

boolean.AND {

    return #stringToBool(
            thisTreeFunction([thisGrammarNode.#1], thisResultNode)
           );
}

value.AND {
    return thisTreeFunction([thisGrammarNode.#1], thisResultNode);
}

SLOPETOKEN {
    return slotNodeName(thisGrammarNode.#1);
}

GROUP {
    return slotNodeName(thisGrammarNode.#1.#1);
}

ALL {
    if(hasChildren(thisGrammarNode)) {
        return thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    };
}

/*
TreeFunction constructFromTreeString

isNamedSLOTSLOPETreeNode() {
    return continue([thisGrammarNode, thisGrammarNode.#1], thisResultNode);
}

SLOPETOKEN {
    return slotNodeName(thisGrammarNode.#1);
}

ALL {
    if(hasChildren(thisGrammarNode)) {
        return thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    };
}


TreeFunction t_constructFromTreeString

treeNode slopeAND:AND {
    var node = thisTreeFunction([slopeAND.#2], thisResultNode);

    forEach(child in children(slopeAND.#4)) {
        addSlotNode(node, thisTreeFunction([child], thisResultNode));
    };
    return node;
}

identifier slopeAND:AND {
    return #thisTreeFunction([slopeAND.#1], thisResultNode);
}

number slopeAND:AND {
    return #stringToInt(
            thisTreeFunction([slopeAND.#1], thisResultNode)
           );
}

boolean slopeAND:AND {
    return #stringToBool(
            thisTreeFunction([slopeAND.#1], thisResultNode)
           );
}

ALL slopeAND:AND {
    return thisTreeFunction([slopeAND.#1], thisResultNode);
}

*/

TreeFunction _constructHTMLString

TEXT aNodeList {


    var i = 0;
    forEach(child in children(thisGrammarNode)) {
        if(largerThan(i, 0)) {
            htmlString =
                    concat(htmlString, " ");
        };
        htmlString =
                concat(htmlString, slotNodeName(child));
        i = plus(i, 1);
    };


}

ROOT aNodeList {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode, [aNodeList]);
    };
}

att {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode);
    };
}

att.ALL {
    /*htmlString = concatAll(htmlString, [" ", slotNodeName(thisGrammarNode),
                                        "='",
                                        slotNodeName(thisGrammarNode.#1),
                                        "'"
                                     ]
                 );*/


    var attributeValue = "";
    var addSpace = false;
    forEach(child in children(thisGrammarNode)) {
        if(addSpace) {
            attributeValue = concat(attributeValue, " ");
        };
        attributeValue = concat(attributeValue, slotNodeName(child));
        addSpace = true;
    };
    addAttribute(slotNodeName(thisGrammarNode), attributeValue);
}


DIV aNodeList {

    if(and(
        largerThan(length(children(thisGrammarNode)), 0),
        equals(slotNodeName(thisGrammarNode.#1), "att")
       )
    ) {
        printString(concat("In ", slotNodeName(thisGrammarNode)));
        startOpenTag(thisGrammarNode, aNodeList);
            thisTreeFunction([thisGrammarNode.#1], thisResultNode);
        endOpenTag();

        printString(htmlString);

        var i = 2;
        while(smallerEqualsThan(i, length(children(thisGrammarNode)))) {
            thisTreeFunction([childAt(thisGrammarNode, i)], thisResultNode, [aNodeList]);
            i = plus(i, 1);
        };

        closeTag(thisGrammarNode);

    };



}

TEXTAREA aNodeList {

    if(and(
        largerThan(length(children(thisGrammarNode)), 0),
        equals(slotNodeName(thisGrammarNode.#1), "att")
       )
    ) {
        printString(concat("In ", slotNodeName(thisGrammarNode)));
        startOpenTag(thisGrammarNode, aNodeList);
            thisTreeFunction([thisGrammarNode.#1], thisResultNode);
        endOpenTag();

        printString(htmlString);

        var i = 2;
        while(smallerEqualsThan(i, length(children(thisGrammarNode)))) {
            thisTreeFunction([childAt(thisGrammarNode, i)], thisResultNode, [aNodeList]);
            i = plus(i, 1);
        };

        closeTag(thisGrammarNode);

    };



}

BR aNodeList {

    if(and(
        largerThan(length(children(thisGrammarNode)), 0),
        equals(slotNodeName(thisGrammarNode.#1), "att")
       )
    ) {
        printString(concat("In ", slotNodeName(thisGrammarNode)));
        startOpenTag(thisGrammarNode, aNodeList);
            thisTreeFunction([thisGrammarNode.#1], thisResultNode);
        endOpenTag();

        printString(htmlString);

        var i = 2;
        while(smallerEqualsThan(i, length(children(thisGrammarNode)))) {
            thisTreeFunction([childAt(thisGrammarNode, i)], thisResultNode, [aNodeList]);
            i = plus(i, 1);
        };

        closeTag(thisGrammarNode);

    };



}

SPAN aNodeList {

    if(and(
        largerThan(length(children(thisGrammarNode)), 0),
        equals(slotNodeName(thisGrammarNode.#1), "att")
       )
    ) {
        printString(concat("In ", slotNodeName(thisGrammarNode)));
        startOpenTag(thisGrammarNode, aNodeList);
            thisTreeFunction([thisGrammarNode.#1], thisResultNode);
        endOpenTag();

        printString(htmlString);

        var i = 2;
        while(smallerEqualsThan(i, length(children(thisGrammarNode)))) {
            thisTreeFunction([childAt(thisGrammarNode, i)], thisResultNode, [aNodeList]);
            i = plus(i, 1);
        };

        closeTag(thisGrammarNode);

    };



}

ALL aNodeList {

           /* printString(concat("In ", slotNodeName(thisGrammarNode)));*/
            startDivTagClass(thisGrammarNode, aNodeList);
            endOpenTag();

   /*         printString(htmlString);*/

            var i = 1;
            while(smallerEqualsThan(i, length(children(thisGrammarNode)))) {
                thisTreeFunction([childAt(thisGrammarNode, i)], thisResultNode, [aNodeList]);
                i = plus(i, 1);
            };

            closeTagString("DIV");


            /*

        if(and(
                    largerThan(length(children(thisGrammarNode)), 0),
                    equals(slotNodeName(thisGrammarNode.#1), "att")
                   )
                ) {
                    printString(concat("In ", slotNodeName(thisGrammarNode)));
                    startDivTagClass(thisGrammarNode, aNodeList);


                    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
                    endOpenTag();

                    printString(htmlString);

                    var i = 2;
                    while(smallerEqualsThan(i, length(children(thisGrammarNode)))) {
                        thisTreeFunction([childAt(thisGrammarNode, i)], thisResultNode, [aNodeList]);
                        i = plus(i, 1);
                    };

                    closeTagString("DIV");

                } else {

                ........
        };*/
}



TreeFunction constructRuleTree

OR {
    var numAlts = numAlternatives(thisGrammarNode);


    thisTreeFunction([createORChildTemplateAt(thisGrammarNode, 0)], thisResultNode);


    var counter = 1;

    while(smallerThan(counter, numAlts)) {

        +#" | ";
        thisTreeFunction([createORChildTemplateAt(thisGrammarNode, counter)], thisResultNode);
        counter = plus(counter, 1);
    };

}

OPT {
    var childTemplate = createSingleChildTemplate(thisGrammarNode);
    thisTreeFunction([childTemplate], thisResultNode);
    +#"?";
}

BRACKET {

    +#"(";
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    +#")";
}

isNFOLD_0() {
    var childTemplate = createNFOLDChildTemplate(thisGrammarNode);
    thisTreeFunction([childTemplate], thisResultNode);
    +#"*";
}

isNFOLD_1() {
    var childTemplate = createNFOLDChildTemplate(thisGrammarNode);
    thisTreeFunction([childTemplate], thisResultNode);
    +#"+";
}

AND {
    thisTreeFunction([thisGrammarNode.#1], thisResultNode);

    var counter = 2;
    var childrenLength = length(children(thisGrammarNode));
    while(or(smallerThan(counter, childrenLength),
             equals(counter,childrenLength))) {
        +#" ";
        thisTreeFunction([childAt(thisGrammarNode, counter)], thisResultNode);

        counter = plus(counter, 1);
    };
}

NULL {
    +#"NULL";
}

SLOPETOKEN {
    +#"~"";
    +#slotNodeName(thisGrammarNode.#1) ;
    +#"~"";
}

ALL {
    +#slotNodeName(thisGrammarNode);
}

TreeFunction t_constructTreeString

ALL tabNumber {

    var i = 0;
    while(smallerThan(i, tabNumber)) {
        +#"    ";
        i = plus(i,1);
    };

    +#"#";
    +#slotNodeName(thisGrammarNode);
    +#"(";

    if(hasChildren(thisGrammarNode)) {
        +#"\n";

        forEach(child in children(thisGrammarNode)) {
            thisTreeFunction([child], thisResultNode, [plus(tabNumber, 1)]);
            +#"\n";
        };

        i = 0;
        while(smallerThan(i, tabNumber)) {
                +#"    ";
                i = plus(i,1);
        };
    };

    +#")";
}

TreeFunction t_equalTrees

ALL secondGrammarNode:ALL {
    if(or(
            not(
                equals(slotNodeName(thisGrammarNode),
                      slotNodeName(secondGrammarNode)
                )
            ),
            not(
                equals(length(children(thisGrammarNode)),
                       length(children(secondGrammarNode))
                )
            )
        )
    ) {
        printString("Nodes NOT EQUAL");
        printString("LEFT:");
        printString(runtimeType(slotNodeName(thisGrammarNode)));
        printString(constructTreeString(thisGrammarNode));
        printString("RIGHT:");
        printString(runtimeType(slotNodeName(secondGrammarNode)));
        printString(constructTreeString(secondGrammarNode));
        return false;
    }
    else {
        var i = 1;
        while(smallerEqualsThan(i, length(children(thisGrammarNode)))) {
            if(not(
                thisTreeFunction([childAt(thisGrammarNode, i),
                                  childAt(secondGrammarNode, i)],
                                 rootResultNode)
            )) {
                return false;
            };

            i = plus(i,1);
        };
        return true;
    };
}

TreeFunction getTokenString

SLOPETOKEN {
    return slotNodeName(thisGrammarNode.#1);
}

TOKEN {
    return slotNodeName(thisGrammarNode.#1);
}

ALL {
    if(hasChildren(thisGrammarNode)) {
        return thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    } else {
        return slotNodeName(thisGrammarNode);
    };
}

TreeFunction constructDataTree

orAble.AND {
    if(hasChildren(thisGrammarNode.#2)) {
        +#"OR" {
            forEach(child in children(thisGrammarNode)) {
                thisTreeFunction([child], thisResultNode);
            };
        };
    } else {
        thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    };
}

andAble.AND {
    if(hasChildren(thisGrammarNode.#2)) {
        +#"AND" {
            forEach(child in children(thisGrammarNode)) {
                thisTreeFunction([child], thisResultNode);
            };
        };
    } else {
        thisTreeFunction([thisGrammarNode.#1], thisResultNode);
    };
}

exp.AND {

    var literal = #"LITERAL" [
        +#getTokenString(thisGrammarNode.#2);
        thisTreeFunction([thisGrammarNode.#3], thisResultNode);
    ];

    if(hasOPTChild(thisGrammarNode.#1)) {
        +#"NOT" {
            +literal;
        };
    } else {
        +literal;
    };
}

expRight.AND {
    +#getTokenString(thisGrammarNode.#1);
    +#getTokenString(thisGrammarNode.#2);
}

literal.ALL {
    +#getTokenString(thisGrammarNode);
}

INDEX {
}

ALL {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode);
    };
}



TreeFunction constructTextField



TreeFunction constructHTMLTree

OR {
    +#"box" {
        +#"boxDescriptor" {
            +#"boxDescriptorText" {
                +#"TEXT" {
                    +#"OR";
                };
            };
        };
        +#"boxContent" {
            forEach(child in children(thisGrammarNode)) {
                +#"boxSection" {
                    thisTreeFunction([child], thisResultNode);
                };
            };
        };
    };
}

/*OR {
    +#"bracketedBox" {
        +#"box" {
            +#"boxDescriptor" {
                +#"boxDescriptorText" {
                    +#"TEXT" {
                        +#"OR";
                    };
                };
            };
            +#"boxContent" {

                forEach(child in children(thisGrammarNode)) {
                    +#"boxSection" {
                        thisTreeFunction([child], thisResultNode);
                    };
                };
            };
        };
    };
}*/

NOT {
        +#"box red" {
            +#"boxDescriptor" {
                +#"boxDescriptorText" {
                    +#"TEXT" {
                        +#"NOT";
                    };
                };
            };
            +#"boxContent" {
                forEach(child in children(thisGrammarNode)) {
                    +#"boxSection" {
                        thisTreeFunction([child], thisResultNode);
                    };
                };
            };
        };
}

LITERAL {
    +#"literal" {
        +#"TEXT" {
           forEach(child in children(thisGrammarNode)) {
                +#slotNodeName(child);
           };
        };
    };
}



INPUT {
    +#"BR" [+#"att";];
    +#"TEXTAREA" {
        +#"att" {
        };
    };
}





ALL {
    forEach(child in children(thisGrammarNode)) {
        thisTreeFunction([child], thisResultNode);
    };
}

''');
exampleStrings.add('''        GfAND (            (          NOT Au                     OR      Xy                         AND Hu                     OR      Eq                         AND Bw                         AND Qd                     OR      Lh                         AND (    Gb                              OR      Tu                                  AND Cj                              OR      Dx                                  AND Nw                                  AND (      NOT Au                                           OR     Kr                                               AND Up                                           OR     Bm                                               AND Rh                                               AND Ty                                      )                             )                         AND Nt                     OR  Ro                  )              AND Ho              AND (        NOT Xt                       OR      H                           AND Ri                       OR      Zk                           AND Ia                           AND Cr                       OR      Ts                           AND (    K                                OR      Hu                                    AND Jd                                OR      Uz                                    AND Ir                                    AND (      NOT Vv                                             OR     Jm                                                 AND Pg                                             OR     El                                                 AND Po                                                 AND Oj                                        )                               )                           AND Ei                       OR  Eb                  )         OR      Si             AND Qm             AND (        NOT Ow                      OR      Kl                          AND Un                      OR      Ym                          AND Ui                          AND Vp                      OR      Al                          AND (    Pz                               OR      Nd                                   AND Qr                               OR      Fi                                   AND Ks                                   AND (      NOT Sn                                            OR     Ga                                                AND Ev                                            OR     Do                                                AND Yz                                                AND Lz                                       )                              )                          AND Ix                      OR  Qx                 )        )    AND U''');
  return exampleStrings;
}
}
