import '../../../html/examples/SLOTHTMLToConsoleConverter.dart';
import '../../../../lib/slot/runtime/SLOTProject.dart';
import 'booleanExpressions.slp.dart';

main() {
  SLOTProject project =
  new SLOTHTMLToConsoleConverter().constructProject(new booleanExpressions());
  var slotResult = project.runProject();
}
