import '../../../../lib/slot/runtime/SLOTHTMLProject.dart';
import '../../../html/examples/SLOTProjectConstructor.dart';
import 'booleanExpressions.slp.dart';

main() {

  SLOTHTMLProject project =
  new SLOTProjectConstructor().constructHTMLProject(new booleanExpressions());
  var slotResult = project.runProject();

}
