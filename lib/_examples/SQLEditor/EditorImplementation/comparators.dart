import 'FunctionRuleList.dart';
import 'editorScope.dart';

class ComparatorFunctions
{
  static bool equalUnify(Expression e1, Expression e2, Scope s)
  {
    if(e1 is Variable && e2 is Variable)
      return equalUnifyVariables(e1,e2,s);
    else if(e1 is Variable && e2 is EditorNode)
      return equalUnifyVarNode(e1, e2, s);
    else if(e1 is EditorNode && e2 is Variable)
      return equalUnifyVarNode(e2, e1, s);
    else if(e1 is Variable && e2 is StringLiteral)
      return equalUnifyVarString(e1, e2, s);
    else if(e1 is StringLiteral && e2 is Variable)
      return equalUnifyVarString(e2, e1, s);
    else if(e1 is Variable && e2 is Number)
      return equalUnifyVarNumber(e1, e2, s);
    else if(e1 is Number && e2 is Variable)
      return equalUnifyVarNumber(e2, e1, s);
    else if(e1 is StringLiteral && e2 is StringLiteral)
      return equalUnifyStrStr(e1, e2, s);
    else if(e1 is Number && e2 is Number )
      return equalUnifyNumNum(e1, e2, s);
    else if(e1 is EditorNode && e2 is EditorNode)
      return equalUnifyNodeNode(e1, e2, s);
  }


  static bool tryUnify(Expression e1, Expression e2, Scope s)
  {
    if(e1 is Variable && e2 is Variable)
      return equalUnifyVariables(e1,e2,s);
    else if(e1 is Variable && e2 is EditorNode)
      return equalUnifyVarNode(e1, e2, s);
    else if(e1 is EditorNode && e2 is Variable)
      return equalUnifyVarNode(e2, e1, s);
    else if(e1 is Variable && e2 is StringLiteral)
      return equalUnifyVarString(e1, e2, s);
    else if(e1 is StringLiteral && e2 is Variable)
      return equalUnifyVarString(e2, e1, s);
    else if(e1 is Variable && e2 is Number)
      return equalUnifyVarNumber(e1, e2, s);
    else if(e1 is Number && e2 is Variable)
      return equalUnifyVarNumber(e2, e1, s);
    else if(e1 is StringLiteral && e2 is StringLiteral)
      return equalUnifyStrStr(e1, e2, s);
    else if(e1 is Number && e2 is Number )
      return equalUnifyNumNum(e1, e2, s);
    else if(e1 is EditorNode && e2 is EditorNode)
      return equalUnifyNodeNode(e1, e2, s);
  }


  static bool equalUnifyNodeNode(EditorNode node1, EditorNode node2, Scope s)
  {
    if(node1.name != node2.name
    || node1.children.length != node2.children.length) return false;

    for(int i = 0; i<node1.children.length; i++)
    {
      if(!equalUnify(node1.children[i], node2.children[i], s))
        return false;
    }
  }

  static bool equalUnifyNumNum(Number num1, Number num2, Scope s)
  {
    return num1.number == num2.number;
  }

  static bool equalUnifyStrStr(StringLiteral str1, StringLiteral str2, Scope s)
  {
    return str1.literal == str2.literal;
  }

  static bool equalUnifyVarNumber(Variable v, Number num, Scope s)
  {
    if(s.containsVariable(v.identifier))
    {
      return equalUnify(s.getValue(v.identifier), num, s);
    }
    else
    {
      s.addVariable(v.identifier, num);
      return true;
    }
  }

  static bool equalUnifyVarString(Variable v, StringLiteral str, Scope s)
  {
    if(s.containsVariable(v.identifier))
    {
      return equalUnify(s.getValue(v.identifier), str, s);
    }
    else
    {
      s.addVariable(v.identifier, str);
      return true;
    }
  }



  static bool equalUnifyVarNode(Variable v, EditorNode n, Scope s)
  {
    if(s.containsVariable(v.identifier))
      return equalUnify(s.getValue(v.identifier), n, s);
    else {
      s.addVariable(v.identifier, n);
      return true;
    }
  }

  static bool equalUnifyVariables(Variable v1, Variable v2, Scope s)
  {
    if(s.containsVariable(v1.identifier) && !s.containsVariable(v2.identifier))
    {
      s.addVariable(v2.identifier, s.getValue(v1.identifier));
      return true;
    }
    else if(!s.containsVariable(v1.identifier) && s.containsVariable(v2.identifier))
    {
      s.addVariable(v1.identifier, s.getValue(v2.identifier));
      return true;
    }
    else if(!s.containsVariable(v1.identifier) && !s.containsVariable(v2.identifier))
    {
      //String alias = s.alias(v1.identifier, v2.identifier);
      Variable p = new Variable("§${s.generateAlias()}");
      s.addVariable(v1.identifier, p);
      s.addVariable(v2.identifier, p);
      return true;
    }
    else if(s.containsVariable(v1.identifier) && s.containsVariable(v2.identifier))
    {
      return equalUnify(s.getValue(v1.identifier), s.getValue(v2.identifier), s);
    }
  }
}