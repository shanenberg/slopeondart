import 'comparators.dart';
import 'editorScope.dart';


abstract class Expression
{
  bool equals(Expression e)
  {
    return false;
  }

  bool unifyEquals(Expression e, Scope s)
  {
    return false;
  }

  bool isReducible(Scope s);

  Expression reduce(Scope s);

  Expression clone();
}

class Number extends Expression
{
  int number;
  Number(this.number);

  String toString()
  {
    return "$number";
  }

  bool isReducible(Scope s) => false;
  @override
  Expression reduce(Scope s) {
    throw "Can't Reduce Number";
    // TODO: implement reduce
  }

  Expression clone() => new Number(number);
}

class Placeholder extends Expression
{
  int aliasnum;

  Placeholder([this.aliasnum]);

  bool equals(Expression e)
  {
    return this == e;
  }

  bool unifyEquals(Expression e, Scope s)
  {
    return this == e;
  }

  bool isReducible(Scope s) => false;
  @override
  Expression reduce(Scope s) {
    // TODO: implement reduce
  }

  Expression clone()
  {}
}

class Variable extends Expression
{
  String identifier;

  Variable([this.identifier]);

  bool equals(Expression e) => e is Variable && identifier == e.identifier;

  String toString() => identifier;



  @override
  bool isReducible(Scope s) {
    // TODO: implement isReducible
    if(s.containsVariable(identifier))
      return true;
    else
      return false;
  }

  @override
  Expression reduce(Scope s) {
    // TODO: implement reduce
    return s.getValue(identifier);
  }

  Expression clone()
  {
    return new Variable(identifier);
  }
}

class EditorNode extends Expression
{
  String name;
  String id;
  List<Expression> children;

  EditorNode([this.name, this.children]);

  String toString()
  {
    return "#${name}$children";
  }
  @override
  bool isReducible(Scope s) {
    // TODO: implement isReducible
    for(Expression e in children)
    {
      if(e.isReducible(s)) return true;
    }
    return false;

  }

  @override
  Expression reduce(Scope s) {
    // TODO: implement reduce
    List<Expression> redexChildren = new List<Expression>(children.length);

    for(int i = 0; i< redexChildren.length; i++)
    {
      if(children[i].isReducible(s))
        redexChildren[i] = children[i].reduce(s);
      else
        redexChildren[i] = children[i].clone();
    }

    EditorNode node = new EditorNode(name, redexChildren);
    return node;
  }

  Expression clone()
  {
    List<Expression>  clonedChildren = new List<Expression>(children.length);
    for(int i = 0; i<clonedChildren.length;i++)
      clonedChildren[i] = children[i].clone();

    return new EditorNode(name, clonedChildren);
  }
}

class NULL extends Expression
{

  @override
  Expression clone() {
    // TODO: implement clone
    return this;
  }

  @override
  bool isReducible(Scope s) {
    // TODO: implement isReducible
    return false;
  }

  @override
  Expression reduce(Scope s) {
    // TODO: implement reduce
    throw "Can't Reduce NULL";
  }
}


class StringLiteral extends Expression
{
  String literal;
  StringLiteral([this.literal]);
  bool equals(Expression e) => e is StringLiteral && e.literal == literal;

  String toString() => "'$literal'";
  @override
  Expression clone() {
    return new StringLiteral(literal);
    // TODO: implement clone
  }

  @override
  bool isReducible(Scope s) {
    // TODO: implement isReducible
    return false;
  }

  @override
  Expression reduce(Scope s) {
    // TODO: implement reduce
    throw "Can't Reduce StringLiteral";
  }
}

class FunctionCall extends Expression
{
  String name;
  List<Expression> paramList;
  Expression result = null;

  FunctionCall([this.name, this.paramList]);

  String toString() => "$name$paramList";

  call(Scope s)
  {
    for(FunctionDefinition fDef in s.functionDefs)
    {
      if(bindParameters(s, fDef))
      {
        result = fDef.returnExpression;

        while(result.isReducible(s))
          result = result.reduce(s);

        return;
      }

    }
  }

  bool bindParameters(Scope s, FunctionDefinition fDef)
  {
    if(fDef.name == name && fDef.paramList.length == paramList.length)
    {
      for(int i = 0; i < paramList.length; i++)
      {
        if(!ComparatorFunctions.equalUnify(paramList[i], fDef.paramList[i], s))
          return false;

      }
      return true;
    }
    else return false;
  }
  @override
  Expression clone() {
    // TODO: implement clone
    List<Expression>  clonedParams = new List<Expression>(paramList.length);
    for(int i = 0; i<clonedParams.length; i++)
      clonedParams[i] = paramList[i].clone();

    return new FunctionCall(name, clonedParams);
  }

  @override
  bool isReducible(Scope s) {
    // TODO: implement isReducible
    return true;
  }

  @override
  Expression reduce(Scope s) {
    // TODO: implement reduce
    call(s);
    return result;
  }

}


class FunctionDefinition extends Expression
{
  String name;
  List<Expression> preconditions;
  List<Expression> paramList;
  Expression returnExpression;
  FunctionDefinition([this.name, this.paramList, this.returnExpression ,this.preconditions]);

  @override
  Expression clone() {
    // TODO: implement clone
    return null;
  }

  @override
  bool isReducible(Scope s) {
    // TODO: implement isReducible
    return false;
  }

  @override
  Expression reduce(Scope s) {
    // TODO: implement reduce
    throw "wtf are you doing man";
  }

  String toString()
  {
    return "$name$paramList";
  }
}


void main()
{
  FunctionDefinition fd = new FunctionDefinition(
      "LOL",
      [new Variable("X")],
      new EditorNode(
        "Blabla",
        [
          new EditorNode("hihi", [
            new StringLiteral("bla")
            ]),
          new Variable("X")
        ]
      ),
      []
  );

  Scope s = new Scope([fd], new Map<String, Expression>(), null);

  FunctionCall c = new FunctionCall("LOL", [new EditorNode("Dingsbums", [])]);
  c.call(s);


  print(fd.toString());
  print(c.result.toString());

}

