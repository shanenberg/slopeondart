import 'FunctionRuleList.dart';

class Scope
{
  Scope previousScope;
  List<FunctionDefinition> functionDefs;
  Map<String, Expression> variables;

  int aliasNum;

  Scope([this.functionDefs, this.variables, this.previousScope])
  {
    if(previousScope != null)
      aliasNum = previousScope.aliasNum;
    else
      aliasNum = 1;
  }


  String generateAlias()
  {
    return "§${aliasNum++}";
  }

  Expression getValue(String v)
  {
    if(containsVariable(v))
    {
      Expression e = variables[v];
      if(e is Variable)
        return getValue(e.identifier);
      else
        return e;
    }
    else if(previousScope != null)
      return previousScope.getValue(v);
    else
      return null;
  }

  bool containsVariable(String v)
  {
    return variables.containsKey(v)
        || previousScope != null && previousScope.containsVariable(v);
  }

  void addVariable(String v, Expression e)
  {
    variables[v] = e;
  }

  bool setIfExistsRecursive(String v, Expression e)
  {
    if(previousScope != null
        && setIfExistsRecursive(v, e))
      return true;
    else if(containsVariable(v))
    {
      variables[v] = e;
      return true;
    }
    else return false;
  }



}
