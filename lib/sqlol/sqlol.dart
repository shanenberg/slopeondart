class SQLStatement {
  SQLStatement(this.sfwBlock);
  SFWBlock sfwBlock;
}

class SFWBlock {

  SELECTClause selectClause;
  FROMClause fromClause;

  SFWBlock(this.selectClause, this.fromClause);

}

class SELECTClause {

  SelectList selectList;

  SELECTClause(this.selectList);

}

class SelectList {
  List<SelectColumn> selectColumnList;

}

class SelectColumn{
  Column column;

  bool isRenamed;
  String renamedName;
}

class Column {
  String name;
  String originTableName;

  TargetExpression target;


}

class TargetExpression {

}

class TargetSelectColumn extends TargetExpression {
  SelectColumn column;
}

class TargetTableLiteral extends TargetExpression{
  String columnName;
  TableLiteral literal;
}

class TableLiteral {
}


class FROMClause {
}



class HALLO {



}