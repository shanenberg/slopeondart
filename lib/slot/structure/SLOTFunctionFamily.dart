import '../runtime/frames/SLOTStackFrame.dart';
import '../runtime/objects/SLOTTreeNode.dart';
import 'SLOTParserElement.dart';
import 'SLOTSingleTreeFunction.dart';

class SLOTFunctionFamily extends SLOTParserElement {
  String name;

  SLOTFunctionFamily(this.name);

  List<SLOTSingleTreeFunction> functions =
    new List<SLOTSingleTreeFunction>();

  SLOTSingleTreeFunction findMatchingFunction(SLOTStackFrame frame, List<SLOTTreeNode> nodes) {
    for(SLOTSingleTreeFunction aFunction in functions) {
      if (aFunction.matchesSLOTNodes(frame, nodes))
        return aFunction;
    }
    return null;
  }

  bool hasMatchingFunction(SLOTStackFrame frame, List<SLOTTreeNode> nodes) {
    for(SLOTSingleTreeFunction aFunction in functions) {
      if (aFunction.matchesSLOTNodes(frame, nodes))
        return true;
    }
    return false;
  }

}
