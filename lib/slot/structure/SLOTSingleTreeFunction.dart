import '../path/SLOTPathExpression.dart';
import '../runtime/frames/SLOTStackFrame.dart';
import '../runtime/objects/SLOTTreeNode.dart';
import 'SLOTAbstractFunction.dart';
import 'SLOTBlock.dart';
import 'SLOTFunctionFamily.dart';

class SLOTSingleTreeFunction extends SLOTAbstractFunction{
  SLOTFunctionFamily family;
  List<SLOTPathExpression> dispatchArgumentTypes = new List<SLOTPathExpression>();
  List<String> treeparameterNames = new List<String>();
  List<String> parameterNames = new List<String>();

  SLOTSingleTreeFunction(
      SLOTBlock body,
      this.dispatchArgumentTypes) : super(body);

  bool matchesSLOTNodes(SLOTStackFrame frame, List<SLOTTreeNode> nodes) {
    if (this.dispatchArgumentTypes.length!=nodes.length)
      return false;

    for(int i=0;i<nodes.length; i++) {
      if(!dispatchArgumentTypes[i].matchesSlotTreeNode(frame, nodes[i]))
        return false;

    }
    return true;
  }


}