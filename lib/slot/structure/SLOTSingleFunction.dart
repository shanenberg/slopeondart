import '../runtime/SLOTObject.dart';
import '../runtime/SLOTRuntime.dart';
import '../runtime/frames/SLOTSingleFunctionStackFrame.dart';
import '../runtime/frames/SLOTStackFrame.dart';
import 'SLOTAbstractFunction.dart';

class SLOTSingleFunction extends SLOTAbstractFunction{
  String name;

  List<String> parameterNames = new List<String>();

  void pushOnStack(SLOTStackFrame frame, List<SLOTObject> parameterObjects) {
    SLOTRuntime runtime = frame.getRuntime();
    SLOTSingleFunctionStackFrame newFunctionFrame =
      new SLOTSingleFunctionStackFrame(runtime, this);

    newFunctionFrame.currentSourceSLOTNodes = frame.currentSourceSLOTNodes;
    newFunctionFrame.currentResultSLOTNode = frame.currentResultSLOTNode;

    // Set parameters for function
    for(int i=0; i<this.parameterNames.length;i++) {
      try {
        newFunctionFrame.variables[this.parameterNames[i]]=parameterObjects[i];
      } catch (e) {
        print(e);
        throw e;
      }
    }

    runtime.stack.add(newFunctionFrame);
    runtime.stack.last.pushExpression(this.body.shallowCopy());
  }

  SLOTSingleFunction(this.name, this.parameterNames, body): super(body);

}