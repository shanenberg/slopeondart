import '../../runtime/SLOTObject.dart';
import '../../runtime/frames/SLOTStackFrame.dart';
import '../../runtime/objects/SLOTFalse.dart';
import '../../runtime/objects/SLOTTrue.dart';
import '../SLOTBlock.dart';
import 'SLOTExpression.dart';

enum WhileState {notYetStarted, conditionPushed, blockPushed, done}

class SLOTStatement_While extends SLOTExpression {

  WhileState state = WhileState.notYetStarted;

  SLOTExpression shallowCopy() {
    SLOTStatement_While ret = new SLOTStatement_While();
    ret.body = this.body;
    ret.condition = this.condition;
    return ret;
  }


  SLOTExpression condition;
  bool conditionWasEvaluated=false;
  bool bodyWasEvaluated=false;
  SLOTBlock body;

  @override
  void evaluate(SLOTStackFrame frame) {

    switch (state) {
      case WhileState.notYetStarted:
        frame.pushExpression(condition.shallowCopy());
        state = WhileState.conditionPushed;
        return;

      case WhileState.conditionPushed:
        SLOTObject needsToExecuteLoop = frame.popData();
        if (needsToExecuteLoop == SLOTTrue.instance) {
          body.shallowCopy().addToStack(frame);
        } else if (needsToExecuteLoop == SLOTFalse.instance) {
          state = WhileState.done;
          return;
        } else {
          throw "Condition in while loop must be true or false";
        }
        state = WhileState.blockPushed;
        return;

      case WhileState.blockPushed:
        frame.popData(); // throw away last expression
        state = WhileState.notYetStarted;
        return;

      case WhileState.done:
        frame.popExpression(); // remove me from stack
        return;

    }

  }

  @override
  void nextStep(SLOTStackFrame frame) {
    evaluate(frame);
  }

  String toStackString() {
    return "While";}


}