import '../../runtime/SLOTObject.dart';
import '../../runtime/SLOTRuntime.dart';
import '../../runtime/frames/SLOTStackFrame.dart';
import '../../runtime/objects/SLOTTreeNode.dart';
import '../SLOTBlock.dart';
import 'SLOTExpression.dart';

enum AddNodeState {uninitialized, nodeEvaluated, blockEvaluated}

class SLOTExpression_AddNode extends SLOTExpression {

  AddNodeState state = AddNodeState.uninitialized;

  SLOTExpression shallowCopy() {
    SLOTExpression_AddNode ret = new SLOTExpression_AddNode(nodeExpression);
    ret.body = this.body;
    return ret;
  }

  SLOTExpression nodeExpression;
  bool expressionWasEvaluated = false;

  SLOTBlock body = null;
  bool hasEvaluatedBody = false;
  SLOTTreeNode newNode;

  SLOTExpression_AddNode(this.nodeExpression);

  @override
  void nextStep(SLOTStackFrame frame) {

    switch (state) {

      case AddNodeState.uninitialized:
        state = AddNodeState.nodeEvaluated;
        frame.pushExpression(nodeExpression.shallowCopy());
        return;

      case AddNodeState.nodeEvaluated:

        if(frame.getRuntime().stack.last.dataLength()<1)
          throw "BIG PROBLEM!!";

        state = AddNodeState.blockEvaluated;

        var val = frame.popData();
        if (!(val is SLOTTreeNode)) {
          throw "A node must be a treenode!";
        }
        newNode = (val as SLOTTreeNode);

        addToResultNode(frame, newNode);

        if (hasBody()) {
          SLOTBlock newBody = body.shallowCopy();
          newBody.addToStack(frame);
//print("currentResult " + newNode.debug_SLOTTreeString());
//print("currentResult " + frame.currentSourceSLOTNodes.toString());
          frame.getRuntime().stack.last.currentResultSLOTNode = newNode;
          frame.getRuntime().stack.last.currentSourceSLOTNodes = frame.currentSourceSLOTNodes;
        }
        return;

      case AddNodeState.blockEvaluated:
        evaluate(frame);


        frame.popExpression();
        frame.getRuntime().stack.last.pushData(newNode);

        return;
    }
  }

  SLOTTreeNode addToResultNode(SLOTStackFrame frame, SLOTTreeNode treeNode) {
    return frame.addToResultNode(treeNode);
  }

  bool hasBody() {
    return body != null;
  }

  String toStackString() {
    return "AddNode: " + state.toString();}



  @override
  void evaluate(SLOTStackFrame frame) {
    // TODO: implement evaluate
  }
}