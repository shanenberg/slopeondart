import '../../runtime/SLOTObject.dart';
import '../../runtime/SLOTRuntime.dart';
import '../../runtime/frames/SLOTStackFrame.dart';
import 'SLOTExpression.dart';

class SLOTStatement_Return extends SLOTExpression {


  SLOTExpression shallowCopy() {
    SLOTStatement_Return ret = new SLOTStatement_Return(rightTerm);
    return ret;
  }

  SLOTExpression rightTerm;
  bool rightTermEvaluated = false;

  SLOTStatement_Return(this.rightTerm);

  void nextStep(SLOTStackFrame frame) {
    if (!rightTermEvaluated) {
      frame.pushExpression(rightTerm.shallowCopy());
      rightTermEvaluated = true;
    } else {
      evaluate(frame);
    }
  }


  /**
   * Remove all Frames wich are not functionFrames (such as inner
   * frames from if-statements).
   *
   *
   * Put the returnValue to the data
   * of the frame that's left
   */
  @override
  void evaluate(SLOTStackFrame frame) {
    SLOTRuntime runtime = frame.getRuntime();
    SLOTObject returnValue = frame.popData();

    while(!runtime.stack.last.isFunctionFrame()) {
      runtime.stack.removeLast();
    }

    runtime.stack.removeLast(); // now, the function is no longer on the stack
    runtime.stack.last.pushData(returnValue); // the lower frame gets the return value
    rightTermEvaluated = false;
  }

  String toStackString() {
    return "Return";}

}