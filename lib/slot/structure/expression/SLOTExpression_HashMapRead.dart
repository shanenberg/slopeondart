import '../../runtime/frames/SLOTStackFrame.dart';
import '../../runtime/objects/SLOTHashMap.dart';
import 'SLOTExpression.dart';

enum HashMapReadState {notYetStarted, targetExpressionPassed}
class SLOTExpression_HashMapRead extends SLOTExpression {

  HashMapReadState state = HashMapReadState.notYetStarted;

  SLOTExpression hashMapObject;
  String fieldName;

  SLOTExpression shallowCopy() {
    SLOTExpression_HashMapRead ret = new SLOTExpression_HashMapRead(hashMapObject, fieldName);
    return ret;
  }

  SLOTExpression_HashMapRead(this.hashMapObject, this.fieldName);

  @override
  void nextStep(SLOTStackFrame frame) {
    switch(state) {

      case HashMapReadState.notYetStarted:
        state = HashMapReadState.targetExpressionPassed;
        frame.pushExpression(hashMapObject.shallowCopy());
        return;

      case HashMapReadState.targetExpressionPassed:
        SLOTHashMap o = frame.popData() as SLOTHashMap;
        frame.pushData(o);
        frame.popExpression();
        return;
    }
    evaluate(frame);
    frame.popExpression();
  }

  @override
  void evaluate(SLOTStackFrame frame) {
    frame.pushData(frame.getVariable(fieldName));
  }

  String toStackString() {
    return "VarReade: " + this.fieldName;}

}