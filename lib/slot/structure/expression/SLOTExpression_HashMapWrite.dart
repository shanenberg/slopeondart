import '../../runtime/SLOTObject.dart';
import '../../runtime/frames/SLOTStackFrame.dart';
import '../../runtime/objects/SLOTHashMap.dart';
import 'SLOTExpression.dart';

enum HashMapReadState {notYetStarted, expressionsPassed}
class SLOTExpression_HashMapRead extends SLOTExpression {

  HashMapReadState state = HashMapReadState.notYetStarted;

  SLOTExpression hashMapObject;
  SLOTExpression assignexExpression;
  String fieldName;

  SLOTExpression shallowCopy() {
    SLOTExpression_HashMapRead ret = new SLOTExpression_HashMapRead(hashMapObject, fieldName);
    return ret;
  }

  SLOTExpression_HashMapRead(this.hashMapObject, this.fieldName);

  @override
  void nextStep(SLOTStackFrame frame) {
    switch(state) {

      case HashMapReadState.notYetStarted:
        state = HashMapReadState.expressionsPassed;
        frame.pushExpression(assignexExpression);
        frame.pushExpression(hashMapObject.shallowCopy());
        return;

      case HashMapReadState.expressionsPassed:
        SLOTObject assignedValue = frame.popData();
        SLOTHashMap hashMap = frame.popData() as SLOTHashMap;
        hashMap.map[fieldName] = assignedValue;
        frame.pushData(assignedValue);
        frame.popExpression();
        return;
    }
    evaluate(frame);
    frame.popExpression();
  }

  @override
  void evaluate(SLOTStackFrame frame) {
    frame.pushData(frame.getVariable(fieldName));
  }

  String toStackString() {
    return "VarReade: " + this.fieldName;}

}