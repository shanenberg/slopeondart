import '../../runtime/frames/SLOTStackFrame.dart';
import '../SLOTBlock.dart';
import 'SLOTStatement.dart';

abstract class SLOTExpression extends SLOTStatement{

  void evaluate(SLOTStackFrame frame);
  void nextStep(SLOTStackFrame frame);

  SLOTBlock putIntoBlock() {
    SLOTBlock ret = new SLOTBlock();
    ret.statements.add(this.shallowCopy());
    return ret;
  }

  String toStackString();

  SLOTExpression shallowCopy();
}