import '../../../runtime/SLOTObject.dart';
import '../../../runtime/frames/SLOTStackFrame.dart';
import '../../../runtime/frames/SLOTTreeFunctionStackFrame.dart';
import '../../../runtime/objects/SLOTList.dart';
import '../../../runtime/objects/SLOTTreeNode.dart';
import '../SLOTExpression.dart';
import 'SLOTExpression_AbstractFunctionCall.dart';

abstract class SLOTExpression_AbstractFamilyFunctionCall extends SLOTExpression_AbstractFunctionCall {

  @override
  void evaluate(SLOTStackFrame frame) {

    // erzeuge neuen Frame für Family
    SLOTTreeFunctionStackFrame newFrame =
      createFamilyFunctionFrame(frame, parameterObjects);

    if (!(parameterObjects[0] is SLOTList)) {
      throw "FamilyCalls require a list as a first parameter";
    }

    if (parameterObjects.length==3) {
        if (!(parameterObjects[2] is SLOTList)) {
          throw "If you pass three parameters to a familyFunction call, the third one needs to be a list";
        }
    }

    newFrame.currentSourceSLOTNodes = (parameterObjects[0] as SLOTList).asSLOTTreeNodeList();
    newFrame.currentResultSLOTNode = parameterObjects[1] as SLOTTreeNode;

    newFrame.setBlockIndex(frame);
    frame.getRuntime().stack.add(newFrame);

    for(int i=1;i<(parameterObjects[0] as SLOTList).elements.length;i++) {
      newFrame.variables[
          newFrame.currentSingleTreeFunction
            .treeparameterNames[i - 1]] =
              (parameterObjects[0] as SLOTList).elements[i];
    }

    if (parameterObjects.length==3) {
      for(int i=0;i<(parameterObjects[2] as SLOTList).elements.length;i++) {
        try {
          newFrame.variables[
          newFrame.currentSingleTreeFunction
              .parameterNames[i]] =
          (parameterObjects[2] as SLOTList).elements[i];
        } catch(e) {
          frame.printStackFrame();
          throw(e);
        }
      }
    }



      frame.getRuntime().stack.last.pushExpression(newFrame.currentSingleTreeFunction.body.shallowCopy());
  }

  void nextStep(SLOTStackFrame frame) {
    super.nextStep(frame);
  }

    SLOTTreeFunctionStackFrame createFamilyFunctionFrame(SLOTStackFrame frame, List<SLOTObject> parameterObjects);

  SLOTExpression_AbstractFamilyFunctionCall(
      List<SLOTExpression> parameterExpressions):
        super(parameterExpressions);

  String toStackString() {
    return "FamFunction";}


}