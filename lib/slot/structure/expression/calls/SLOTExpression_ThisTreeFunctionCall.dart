import '../../../runtime/SLOTObject.dart';
import '../../../runtime/SLOTRuntime.dart';
import '../../../runtime/frames/SLOTStackFrame.dart';
import '../../../runtime/frames/SLOTTreeFunctionStackFrame.dart';
import '../../SLOTFunctionFamily.dart';
import 'SLOTExpression_AbstractFamilyFunctionCall.dart';
import 'SLOTExpression_AbstractFunctionCall.dart';

class SLOTExpression_ThisTreeFunctionCall extends SLOTExpression_AbstractFamilyFunctionCall {

  SLOTExpression_ThisTreeFunctionCall shallowCopy() {
    SLOTExpression_ThisTreeFunctionCall ret = new SLOTExpression_ThisTreeFunctionCall(parameterExpressions);
    return ret;
  }

  SLOTExpression_ThisTreeFunctionCall(parameterExpressions):
        super(parameterExpressions);

  void nextStep(SLOTStackFrame frame) {
    super.nextStep(frame);
  }

  void evaluate(SLOTStackFrame frame) {
      var stack = frame.getRuntime().stack;

//print("Called this with: " + this.parameterObjects[0].valueToString());

      super.evaluate(frame);
  }

  String toStackString() {
    return "Call thisTreeFunction" + state.toString();}

  SLOTTreeFunctionStackFrame createFamilyFunctionFrame(SLOTStackFrame frame, List<SLOTObject> parameterObjects) {
    SLOTRuntime runtime = frame.getRuntime();
    return new SLOTTreeFunctionStackFrame(runtime, runtime.stack.last.currentFunctionList);
  }
}