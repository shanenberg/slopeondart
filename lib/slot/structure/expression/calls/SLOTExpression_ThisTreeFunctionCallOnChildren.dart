import '../../../runtime/SLOTObject.dart';
import '../../../runtime/SLOTRuntime.dart';
import '../../../runtime/frames/SLOTStackFrame.dart';
import '../../../runtime/frames/SLOTTreeFunctionStackFrame.dart';
import '../../SLOTFunctionFamily.dart';
import 'SLOTExpression_AbstractFamilyFunctionCall.dart';
import 'SLOTExpression_AbstractFunctionCall.dart';
import '../../../runtime/objects/SLOTSLOPETreeNode.dart';

class SLOTExpression_ThisTreeFunctionCallOnChildren extends SLOTExpression_AbstractFamilyFunctionCall {

  FunctionCallState state = FunctionCallState.notYetStarted;
  int numChildrenToCall;
  int alreadyCalledChildren = 0;

  SLOTExpression_ThisTreeFunctionCallOnChildren shallowCopy() {
    SLOTExpression_ThisTreeFunctionCallOnChildren ret = new SLOTExpression_ThisTreeFunctionCallOnChildren(parameterExpressions);
    return ret;
  }

  SLOTExpression_ThisTreeFunctionCallOnChildren(parameterExpressions):
        super(parameterExpressions);

  void nextStep(SLOTStackFrame frame) {
    super.nextStep(frame);
  }

  void evaluate(SLOTStackFrame frame) {
      var tfStackFrame = frame as SLOTTreeFunctionStackFrame;
      SLOTSLOPETreeNode thisNode = frame.currentSourceSLOTNodes[0];
      SLOTSLOPETreeNode andNode = thisNode.children.elements[0] as SLOTSLOPETreeNode;

      numChildrenToCall = andNode.children.elements.length;


      for(SLOTSLOPETreeNode aChild in andNode.children.elements) {

      }
      print("dummy");

//print("Called this with: " + this.parameterObjects[0].valueToString());
      throw "TODO!!!!!!!!!!";
    numChildrenToCall = null;
        super.evaluate(frame);
  }

  String toStackString() {
    return "Call thisTreeFunction" + state.toString();}

  SLOTTreeFunctionStackFrame createFamilyFunctionFrame(SLOTStackFrame frame, List<SLOTObject> parameterObjects) {
    SLOTRuntime runtime = frame.getRuntime();
    return new SLOTTreeFunctionStackFrame(runtime, runtime.stack.last.currentFunctionList);
  }
}