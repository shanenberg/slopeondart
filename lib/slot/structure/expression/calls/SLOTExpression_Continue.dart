import '../../../runtime/SLOTObject.dart';
import '../../../runtime/SLOTRuntime.dart';
import '../../../runtime/frames/SLOTStackFrame.dart';
import '../../../runtime/frames/SLOTTreeFunctionStackFrame.dart';
import '../../../runtime/objects/SLOTList.dart';
import '../../../runtime/objects/SLOTNull.dart';
import '../../../runtime/objects/SLOTTreeNode.dart';
import 'SLOTExpression_AbstractFamilyFunctionCall.dart';

class SLOTExpression_Continue extends SLOTExpression_AbstractFamilyFunctionCall {

  SLOTExpression_Continue shallowCopy() {
    SLOTExpression_Continue ret = new SLOTExpression_Continue(parameterExpressions);
    return ret;
  }

  @override
  void evaluate(SLOTStackFrame frame) {

    SLOTRuntime runtime = frame.getRuntime();

    // erzeuge neuen Frame für Family
    SLOTTreeFunctionStackFrame newFrame =
    createFamilyFunctionFrame(frame, parameterObjects);
    newFrame.currentSourceSLOTNodes = (parameterObjects[0] as SLOTList).asSLOTTreeNodeList();
    newFrame.currentResultSLOTNode = parameterObjects[1] as SLOTTreeNode;

    newFrame.setContinueBlockIndex(frame);

    if (newFrame.currentFamilyIndex==-1) {
      frame.pushData(SLOTNull.instance);
      return;
    }

    frame.getRuntime().stack.add(newFrame);

    for(int i=1;i<(parameterObjects[0] as SLOTList).elements.length;i++) {
      newFrame.variables[
      newFrame.currentSingleTreeFunction
          .treeparameterNames[i - 1]] =
      (parameterObjects[0] as SLOTList).elements[i];
    }


    if (parameterObjects.length==3) {
      for(int i=0;i<(parameterObjects[2] as SLOTList).elements.length;i++) {
        try {
          newFrame.variables[
          newFrame.currentSingleTreeFunction
              .parameterNames[i]] =
          (parameterObjects[2] as SLOTList).elements[i];
        }
        catch(e) {
          frame.printStackFrame();
          throw(e);
        }
      }
    }


    frame.getRuntime().stack.last.pushExpression(newFrame.currentSingleTreeFunction.body.shallowCopy());

//    frame.expressions.la
  }

  SLOTExpression_Continue(parameterExpressions):
        super(parameterExpressions);

  String toStackString() {
    return "Call thisTreeFunction";}

  SLOTTreeFunctionStackFrame createFamilyFunctionFrame(SLOTStackFrame f, List<SLOTObject> parameterObjects) {
    SLOTRuntime runtime = f.getRuntime();
    SLOTTreeFunctionStackFrame newFrame = new SLOTTreeFunctionStackFrame(runtime, runtime.stack.last.currentFunctionList);
    newFrame.currentFamilyIndex = f.currentFamilyIndex;
    newFrame.currentFunctionFamilyIndex = f.currentFunctionFamilyIndex;
    newFrame.currentFunctionList = f.currentFunctionList;

    return newFrame;
  }

}