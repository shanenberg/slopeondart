import '../../../runtime/SLOTObject.dart';
import '../../../runtime/SLOTRuntime.dart';
import '../../../runtime/frames/SLOTStackFrame.dart';
import '../SLOTExpression.dart';

enum FunctionCallState {notYetStarted, parametersPassed, done}

abstract class SLOTExpression_AbstractFunctionCall extends SLOTExpression {
  List<SLOTExpression> parameterExpressions;
  List<SLOTObject> parameterObjects = new List<SLOTObject>();

  FunctionCallState state = FunctionCallState.notYetStarted;
  int pushedNumParameters = 0;
  bool functionCalled = false;

  SLOTExpression_AbstractFunctionCall(List<SLOTExpression> e) {
    if (e==null)
      parameterExpressions = new List<SLOTExpression>();
    else
      parameterExpressions = e;
  }

  void nextStep(SLOTStackFrame frame) {

    switch (state) {
      case FunctionCallState.notYetStarted:
        for (int i = parameterExpressions.length - 1; i >= 0; i--) {
//print("pushed....." + parameterExpressions[i].toString());
          frame.pushExpression(parameterExpressions[i].shallowCopy());
        }
        state = FunctionCallState.parametersPassed;
        return;

      case FunctionCallState.parametersPassed:
        state = FunctionCallState.done;

        var runtime = frame.getRuntime();
        if(frame.dataLength()<parameterExpressions.length) {
          throw "blblb";
        }

        parameterObjects = new List<SLOTObject>(parameterExpressions.length);
//print("********+ BEFORE **********");
//frame.printStackFrame();
        for (int i = 0; i < parameterExpressions.length; i++) {
          parameterObjects[parameterExpressions.length-i-1]=
                frame.popData();
//print("popped" + i.toString() + " " + parameterObjects[parameterExpressions.length-i-1].valueToString());
        }
        this.evaluate(frame);
        return;


      case FunctionCallState.done:
        // The last data remains on the data stack, because it is the
        // function's return
        if (frame.dataLength()==0) {
          throw "something is really shitty here";
        }
        frame.popExpression();
        return;
    }
  }
}