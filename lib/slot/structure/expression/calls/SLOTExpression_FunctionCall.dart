import '../../../runtime/SLOTRuntime.dart';
import '../../../runtime/frames/SLOTStackFrame.dart';
import 'SLOTExpression_AbstractFunctionCall.dart';

class SLOTExpression_FunctionCall extends SLOTExpression_AbstractFunctionCall {
  String functionName;

  static int counter = 0;

  SLOTExpression_FunctionCall shallowCopy() {
    SLOTExpression_FunctionCall ret = new SLOTExpression_FunctionCall(functionName,parameterExpressions);
    return ret;
  }

  @override
  void evaluate(SLOTStackFrame frame) {
    SLOTRuntime runtime = frame.getRuntime();
    runtime.pushFunction(frame, functionName, parameterObjects);
  }

  SLOTExpression_FunctionCall(this.functionName, parameterExpressions):
        super(parameterExpressions);

  String toStackString() {
    return "Call" + functionName;}

}