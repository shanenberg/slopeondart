import '../../../runtime/SLOTObject.dart';
import '../../../runtime/SLOTRuntime.dart';
import '../../../runtime/frames/SLOTStackFrame.dart';
import '../../../runtime/frames/SLOTTreeFunctionStackFrame.dart';
import '../../SLOTFunctionFamily.dart';
import '../SLOTExpression.dart';
import 'SLOTExpression_AbstractFamilyFunctionCall.dart';

class SLOTExpression_FamilyFunctionCall extends SLOTExpression_AbstractFamilyFunctionCall {

  SLOTExpression_FamilyFunctionCall shallowCopy() {
    SLOTExpression_FamilyFunctionCall ret =
      new SLOTExpression_FamilyFunctionCall(functionNames, parameterExpressions);
    return ret;
  }

  List<String> functionNames = new List<String>();

  SLOTTreeFunctionStackFrame createFamilyFunctionFrame(SLOTStackFrame frame, List<SLOTObject> parameterObjects) {
    SLOTRuntime runtime = frame.getRuntime();
    List<SLOTFunctionFamily> functionFamily
    = runtime.createChainedFunction(functionNames);
    return new SLOTTreeFunctionStackFrame(runtime, functionFamily);
  }

  SLOTExpression_FamilyFunctionCall(
      this.functionNames,
      List<SLOTExpression> parameterExpressions):
        super(parameterExpressions);

  String toStackString() {
    return "FamFunction";}


}