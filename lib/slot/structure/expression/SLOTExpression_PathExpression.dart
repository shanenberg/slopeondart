import '../../runtime/frames/SLOTStackFrame.dart';
import '../../runtime/objects/SLOTTreeNode.dart';
import 'SLOTExpression.dart';

class SLOTExpression_PathExpression extends SLOTExpression {

  @override
  SLOTExpression shallowCopy() {
    SLOTExpression_PathExpression ret = new SLOTExpression_PathExpression(targetTerm);
    ret.targetNumbers = this.targetNumbers;

    return ret;
  }

  List<int> targetNumbers = new List<int>();
  SLOTExpression targetTerm;
  bool targetTermWasEvaluated = false;

  void nextStep(SLOTStackFrame frame) {
    if (!targetTermWasEvaluated) {
      frame.pushExpression(targetTerm.shallowCopy());
      targetTermWasEvaluated = true;
    } else {
      evaluate(frame);
      targetTermWasEvaluated = false;
      frame.popExpression();
//      frame.getRuntime().stack.last.popData(); // remove the result
    }
  }

  /**
   * 1. Removes last element from data stack
   * 2. Stores element into variables
   * 3. Re
   */
  void evaluate(SLOTStackFrame frame) {
    SLOTTreeNode valueOfRightTerm = frame.popData() as SLOTTreeNode;
    SLOTTreeNode nextNode = valueOfRightTerm;
    for(int i=0;i<targetNumbers.length;i++) {
//      if (nextNode.children.elements.isEmpty) {
//        print("STOP");
//      }
/*
      int targetNumber = targetNumbers[i]-1;
      if(targetNumber < 0
      || targetNumber >= nextNode.children.elements.length) {
        print("was geht");
      }
*/
      try {
        nextNode = nextNode.children.elements[targetNumbers[i] - 1];
      }
      catch(e) {
        frame.printStackFrame();
        throw(e);
      }
    }
    frame.pushData(nextNode);
//print("pushed pathExpression" + nextNode.debug_SLOTTreeString());
//print("withRightTerm" + valueOfRightTerm.debug_SLOTTreeString());
//print("");
  }

  SLOTExpression_PathExpression(this.targetTerm);

  String toStackString() {
    String numString = "";
    for(int i=0;i<targetNumbers.length;i++) {
      numString = numString + " " + targetNumbers[i].toString();
    }
    return "PathExpression " + numString;
  }


}