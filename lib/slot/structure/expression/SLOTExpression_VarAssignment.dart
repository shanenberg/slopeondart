import '../../runtime/SLOTObject.dart';
import '../../runtime/frames/SLOTStackFrame.dart';
import 'SLOTExpression.dart';

class SLOTExpression_VarAssignment extends SLOTExpression {

  @override
  SLOTExpression shallowCopy() {
    SLOTExpression_VarAssignment ret = new SLOTExpression_VarAssignment(targetVariable);
    ret.rightTerm = this.rightTerm;
    return ret;
  }
  bool isDeclaration=false;
  String targetVariable;
  SLOTExpression rightTerm;
  bool rightTermWasEvaluated = false;

  void nextStep(SLOTStackFrame frame) {
    if (!rightTermWasEvaluated) {
      frame.pushExpression(rightTerm.shallowCopy());
      rightTermWasEvaluated = true;
    } else {
      evaluate(frame);
      rightTermWasEvaluated = false;
      frame.popExpression();
    }
  }

  /**
   * 1. Removes last element from data stack
   * 2. Stores element into variables
   * 3. Re
   */
  void evaluate(SLOTStackFrame frame) {

//    print("VARABLE: " + this.targetVariable.toString());

    SLOTObject valueOfRightTerm = frame.peekData();
    if (this.isDeclaration) {
      frame.defineLocalVariable(targetVariable, valueOfRightTerm);
    } else {
      frame.setVariable(targetVariable, valueOfRightTerm);
    }
//    print("");
    // don't remove data ....the assigned data is the result of this expression
  }

  SLOTExpression_VarAssignment(this.targetVariable);

  String toStackString() {
    return "VarAssign" + targetVariable;}


}