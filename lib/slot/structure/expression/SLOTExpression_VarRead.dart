import '../../runtime/frames/SLOTStackFrame.dart';
import 'SLOTExpression.dart';

class SLOTExpression_VarRead extends SLOTExpression {

  SLOTExpression shallowCopy() {
    SLOTExpression_VarRead ret = new SLOTExpression_VarRead(varName);
    return ret;
  }


  String varName;

  SLOTExpression_VarRead(this.varName);

  @override
  void nextStep(SLOTStackFrame frame) {
    evaluate(frame);
    frame.popExpression();
  }

  @override
  void evaluate(SLOTStackFrame frame) {
    frame.pushData(frame.getVariable(varName));
  }

  String toStackString() {
    return "VarReade: " + this.varName;}

}