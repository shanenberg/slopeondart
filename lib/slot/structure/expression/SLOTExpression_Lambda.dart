import '../../runtime/frames/SLOTStackFrame.dart';
import 'SLOTExpression.dart';
import '../../../slot/structure/SLOTBlock.dart';
import '../../../slot/runtime/objects/SLOTLambdaFunction.dart';

class SLOTExpression_Lambda extends SLOTExpression {

  List<String> parameterNames = new List<String>();
  SLOTBlock body;

  SLOTExpression shallowCopy() {
    SLOTExpression_Lambda ret = new SLOTExpression_Lambda(body);
    ret.parameterNames.addAll(parameterNames);
    return ret;
  }

  SLOTExpression_Lambda(this.body);

  @override
  void nextStep(SLOTStackFrame frame) {
    evaluate(frame);
    frame.popExpression();
  }

  @override
  void evaluate(SLOTStackFrame frame) {
    List<String> l = new List<String>();
    l.addAll(parameterNames);
    frame.pushData(new SLOTLambdaFunction(frame, l, this.body));
  }

  String toStackString() {
    return "LambdaExpression: ";}

}