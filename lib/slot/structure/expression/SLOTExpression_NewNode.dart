import '../../runtime/SLOTObject.dart';
import '../../runtime/SLOTRuntime.dart';
import '../../runtime/frames/SLOTStackFrame.dart';
import '../../runtime/objects/SLOTNull.dart';
import '../../runtime/objects/SLOTTreeNode.dart';
import '../SLOTBlock.dart';
import 'SLOTExpression.dart';
import 'SLOTExpression_AddNode.dart';

enum NewNodeState {uninitialized, nodeEvaluated, blockEvaluated}

class SLOTExpression_NewNode extends SLOTExpression {

  NewNodeState state = NewNodeState.uninitialized;
  SLOTExpression nodeExpression;
  SLOTBlock body = null;

  SLOTExpression_NewNode shallowCopy() {
    SLOTExpression_NewNode ret = new SLOTExpression_NewNode(nodeExpression);
    ret.body = this.body;
    return ret;
  }

  SLOTTreeNode newNode;
  SLOTExpression_NewNode(SLOTExpression this.nodeExpression);

  SLOTTreeNode createNewNode(SLOTStackFrame frame, SLOTObject content) {
    SLOTTreeNode ret = new SLOTTreeNode();
    ret.value = content;
    ret.origin = frame.currentSourceSLOTNodes[0];
    return ret;
  }

  String toStackString() {
    return "New Node";}


  void nextStep(SLOTStackFrame frame) {
    var runtime = frame.getRuntime();
    switch (state) {
      case NewNodeState.uninitialized:
        state = NewNodeState.nodeEvaluated;
        frame.pushExpression(nodeExpression.shallowCopy());
        return;

      case NewNodeState.nodeEvaluated:
          state = NewNodeState.blockEvaluated;

          SLOTObject content = frame.popData();
          newNode = createNewNode(frame, content);

          if (hasBody()) {
            SLOTBlock newBody = body.shallowCopy();
            newBody.addToStack(frame);
            frame.getRuntime().stack.last.currentResultSLOTNode = newNode;
          } else {

          }
          return;

      case NewNodeState.blockEvaluated:
        SLOTRuntime runtime = frame.getRuntime();
        if (hasBody()) {
          frame.popData(); // remove either the value or the NULL object
//          if (frame.getRuntime().stack.last.dataLength()!=0) {
//            throw "new node should leave one data" + frame.dataLength().toString();
//          }
        }

        frame.popExpression();
        frame.getRuntime().stack.last.pushData(newNode);

//        if (frame.getRuntime().stack.last.dataLength()!=1) {
//          print(newNode.valueToString());
//          throw "new node should leave one data" + frame.dataLength().toString();
//        }
        return;
    }

//    if(!expressionWasEvaluated) {
//      expressionWasEvaluated = true;
//    } else {
//
//      if(hasBody()) {
//
//
//        if(hasEvaluatedBody) {
//          frame.popData();
//          frame.pushData(newSlotNode);
//          frame.popExpression();
//          expressionWasEvaluated = true;
//        } else {
//          SLOTObject content = frame.popData();
//          newSlotNode = createNewNode(frame, content);
//          expressionWasEvaluated = false;
////          SLOTTreeFunctionStackFrame
////          frame.getRuntime().stack.add(new SLOTTreeFunctionStackFrame(frame.getRuntime(), );
////          frame.getRuntime().stack.last.pushExpression(this);
//
//          SLOTBlock newBody = body.shallowCopy();
//          newBody.addToStack(frame);
//          frame.getRuntime().stack.last.currentResultSLOTNode = newSlotNode;
////          newBody
////          newBody.
////          frame.getRuntime().stack.last.currentResultSLOTNode = newParent;
//          hasEvaluatedBody=true;
//          expressionWasEvaluated=false;
//        }
//      } else {
//        evaluate(frame);
//      }
//    }
  }

    @override
  void evaluate(SLOTStackFrame frame) {
//    SLOTObject nodeString = frame.peekData();
//    expressionWasEvaluated = false;
//
//    SLOTRuntime runtime = frame.getRuntime();
//    newSlotNode = createNewNode(frame, nodeString);
//    newSlotNode.origin = frame.currentSourceSLOTNodes[0];
//
//    hasEvaluatedBody=false;
//    expressionWasEvaluated=false;
//    expressionWasEvaluated = true;
////    frame.popExpression(); // push me from stack
//    frame.pushData(newSlotNode);
  }

  bool hasBody() {
    return body != null;
  }




}