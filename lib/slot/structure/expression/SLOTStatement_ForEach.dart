import '../../runtime/SLOTObject.dart';
import '../../runtime/frames/SLOTStackFrame.dart';
import '../../runtime/objects/SLOTList.dart';
import '../SLOTBlock.dart';
import 'SLOTExpression.dart';

enum ForEachState {uninitialized, listEvaluated, cancel, iteration}

class SLOTStatement_ForEach extends SLOTExpression {

  ForEachState evalState = ForEachState.uninitialized;

  SLOTExpression listExpression;
  SLOTBlock body;
  String identifierVariable;

  List<SLOTObject> storedListObject;
  bool listIsEvaluated = false;
  int currentlyIteratedElement = 0;

  SLOTExpression shallowCopy() {
    SLOTStatement_ForEach ret = new SLOTStatement_ForEach();
    ret.listExpression = this.listExpression;
    ret.identifierVariable = this.identifierVariable;
    ret.body = this.body;
    return ret;
  }

  void evaluate(SLOTStackFrame frame) {
    switch(evalState) {

      case ForEachState.uninitialized:
          frame.pushExpression(listExpression.shallowCopy());
          evalState = ForEachState.listEvaluated;
          return;

      case ForEachState.listEvaluated:
        storedListObject = (frame.popData() as SLOTList).elements;
        this.evalState=ForEachState.iteration;
        return;

      case ForEachState.iteration:
        if (storedListObject.length==0 || currentlyIteratedElement>=storedListObject.length) {
          this.evalState=ForEachState.cancel;
          return;
        }

        body.shallowCopy().addToStack(frame);
        frame.getRuntime().stack.last.variables[identifierVariable] = storedListObject[currentlyIteratedElement];
        currentlyIteratedElement++;
        return;

      case ForEachState.cancel:
        frame.popExpression(); // remove me from stack

//        if (frame.getRuntime().stack.last.dataLength()==0) {
//          throw "not even foreach should stop with 0 data!!";
//        }

        return;

    }
//
//      frame.pushExpression(condition);
//      conditionWasEvaluated = true;

//    else {
//      if (!bodyWasEvaluated) {
//        SLOTObject needsToExecuteLoop = frame.popData();
//        if (needsToExecuteLoop == SLOTTrue.instance) {
//          body.shallowCopy().addToStack(frame);
//
//          conditionWasEvaluated = false;
//        } else if (needsToExecuteLoop == SLOTFalse.instance) {
//          frame.popExpression(); // remove me from stack
//          conditionWasEvaluated = false;
//        } else {
//          throw "While condition must be true or false";
//        }
//      }
//    }
  }

  @override
  void nextStep(SLOTStackFrame frame) {
    evaluate(frame);
  }

  String toStackString() {
    return "ForEach";}

}