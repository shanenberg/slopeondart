import '../../runtime/SLOTObject.dart';
import '../../runtime/frames/SLOTStackFrame.dart';
import '../../runtime/objects/SLOTFalse.dart';
import '../../runtime/objects/SLOTTrue.dart';
import '../SLOTBlock.dart';
import 'SLOTExpression.dart';

enum IFState {notYetStarted, conditionPushed, blockPushed, done}

class SLOTStatement_IF extends SLOTExpression {

  IFState state = IFState.notYetStarted;

  SLOTExpression shallowCopy() {
    SLOTStatement_IF ret = new SLOTStatement_IF(condition, thenBlock, elseBlock);
    return ret;
  }

  SLOTExpression condition;
  SLOTBlock thenBlock;
  SLOTBlock elseBlock;

  SLOTStatement_IF(this.condition, this.thenBlock, this.elseBlock);

  @override
  void evaluate(SLOTStackFrame frame) {
    // TODO: implement evaluate
  }

  @override
  void nextStep(SLOTStackFrame frame) {

    switch (state) {

      case IFState.notYetStarted:
        frame.pushExpression(condition.shallowCopy());
        state = IFState.conditionPushed;
        return;

      case IFState.conditionPushed:

        SLOTObject conditionValue = frame.popData();
        if (conditionValue == SLOTTrue.instance) {
          thenBlock.shallowClone().addToStack(frame);
        } else if (conditionValue == SLOTFalse.instance && elseBlock!=null) {
          elseBlock.shallowClone().addToStack(frame);
        } else if (conditionValue == SLOTFalse.instance && elseBlock==null) {
            // do nothing....
        } else {
          throw "Conditions must be true or false";
        }
        state = IFState.blockPushed;
        return;

      case IFState.blockPushed:
        frame.getRuntime().stack.last.popExpression(); // remove me from stack
        state = IFState.done;
        return;

      case IFState.done:
        frame.getRuntime().stack.last.popExpression(); // remove me from stack
        return;
    }

  }

  String toStackString() {
    return "If";}

}