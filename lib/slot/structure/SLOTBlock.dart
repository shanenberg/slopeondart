import '../runtime/SLOTObject.dart';
import '../runtime/SLOTRuntime.dart';
import '../runtime/frames/SLOTChainedStackFrame.dart';
import '../runtime/frames/SLOTStackFrame.dart';
import '../runtime/objects/SLOTNull.dart';
import 'expression/SLOTExpression.dart';

class SLOTBlock extends SLOTExpression{

  SLOTBlock shallowCopy() {
    SLOTBlock ret = new SLOTBlock();
    ret.statements = this.statements;
    return ret;
  }

  List statements = new List();
  int statementCounter=0;


  @override
  void evaluate(SLOTStackFrame frame) {
    // TODO: implement evaluate
  }

  @override
  void nextStep(SLOTStackFrame frame) {
    if(statements.length == statementCounter) {
      removeFromStack(frame);
    } else {
      frame.pushExpression(
          statements[statementCounter].shallowCopy());
      statementCounter++;
      frame.dataClear(); // when I go to the next statement, data gets lost
    }
  }

  SLOTStackFrame createStackFrameFromRuntime(SLOTRuntime runtime) {
    return new SLOTChainedStackFrame(runtime.stack.last);
  }

  SLOTStackFrame createStackFrameFromStackFrame(SLOTStackFrame aFrame) {
    return new SLOTChainedStackFrame(aFrame);
  }

  void addToStack(SLOTStackFrame frame){
    frame.getRuntime().stack.add(this.createStackFrameFromStackFrame(frame));
    frame.getRuntime().stack.last.pushExpression(this);

    frame.getRuntime().stack.last.currentResultSLOTNode = frame.currentResultSLOTNode;
    frame.getRuntime().stack.last.currentSourceSLOTNodes = frame.currentSourceSLOTNodes;
  }

  void addToRuntime(SLOTRuntime runtime){
    runtime.stack.add(this.createStackFrameFromRuntime(runtime));
    runtime.stack.last.pushExpression(this);
  }

  void removeFromStack(SLOTStackFrame frame) {

    SLOTRuntime runtime = frame.getRuntime();

    if (frame.dataLength()==0) {
//      if (statements.length!=0) {
        frame.pushData(SLOTNull.instance);
//      }
    }

    SLOTObject returnValue = frame.peekData();
    frame.popExpression(); // remove me from stack
    runtime.stack.removeLast();
    runtime.stack.last.pushData(returnValue); // put my value to the data from the outer frame

    if (frame.getRuntime().stack.last.dataLength()==0) {
      throw "BLOCK WITHOUT DATA?!?!?";
    }
  }

  String toStackString() {return "Block: statementcounter=" + statementCounter.toString();}

  SLOTBlock shallowClone() {
    SLOTBlock ret = new SLOTBlock();
    ret.statements = this.statements;
    return ret;
  }

}