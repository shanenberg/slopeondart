import '../../lib/parser/tree/TreeNode.dart';
import '../runtime/SLOTRuntime.dart';
import '../runtime/SLOTRuntimeResult.dart';
import '../runtime/objects/SLOTNull.dart';
import 'SLOTFunctionFamily.dart';
import 'SLOTSingleFunction.dart';
import '../runtime/SLOTObject.dart';

class SLOTProgram {

  SLOTProgram();

  SLOTRuntime runtime;

  String libraryName;
  List<String> requiredLibraries = new List<String>();

  List<String> declaredVariables
    = new List<String>();

  List<SLOTFunctionFamily> functionFamilies
    = new List<SLOTFunctionFamily>();

  List<SLOTSingleFunction> singleFunctions
  = new List<SLOTSingleFunction>();


//  SLOTRuntimeResult runMain() {
//
//    loadLibraries();
//    runtime.slopeTreeNodes.add(slopeTreeNode.toSLOTTreeNode());
//
//    return runFunction("main", slopeTreeNode);
//  }


  initStuff(TreeNode slopeTreeNode) {
    loadLibraries();
    runtime.slopeTreeNodes.add(slopeTreeNode.toSLOTTreeNode());
    initSLOTProgram();
  }

  /*
    Run initStuff once before doing runFunction...
   */
  SLOTRuntimeResult runFunction(String functionName, [List<SLOTObject> parameters]) {

    return runtime.runFunction(functionName, parameters);
  }

  SLOTRuntimeResult runProgram(TreeNode slopeTreeNode) {
    loadLibraries();
    runtime.slopeTreeNodes.add(slopeTreeNode.toSLOTTreeNode());

    initSLOTProgram();
    return runtime.runFunction("main");
  }

  SLOTRuntimeResult doRedrawInProgram() {
    return runtime.runFunction("redraw");
  }

  /*
    Runs in the program a function called "functionName"
    on the treeNode "slopeTreeNode" -- the last parameter
    is the input node from the grammar.
   */
  SLOTRuntimeResult initSLOTProgram() {
    for (SLOTSingleFunction f in singleFunctions) {
      runtime.addFunction(f);
    }

    for (SLOTFunctionFamily f in functionFamilies) {
      runtime.addTreeFunction(f);
    }

    for (SLOTFunctionFamily f in functionFamilies) {
      runtime.addTreeFunction(f);
    }

    for (String varName in declaredVariables) {
      runtime.stack.first.variables[varName] = SLOTNull.instance;
    }
  }

//
//    SLOTRuntimeResult ret = runtime.runFunction(functionName);
//    return ret;
//  }

  void loadLibraries() {
    for(String aLibName in this.requiredLibraries) {
      runtime.libraryLoader.loadAndInitLibraryNamed(aLibName, runtime);
    }
  }

//
//  SLOTLibraryLoader libraryLoader() {
//    return new
//  }

}
