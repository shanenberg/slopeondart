import '../../runtime/frames/SLOTStackFrame.dart';
import '../expression/SLOTExpression.dart';

abstract class SLOTExpression_Literal extends SLOTExpression {

  @override
  void nextStep(SLOTStackFrame frame) {
    evaluate(frame);
    frame.popExpression();
  }



}