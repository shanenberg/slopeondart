import '../../runtime/SLOTObject.dart';
import '../../runtime/frames/SLOTStackFrame.dart';
import '../../runtime/objects/SLOTNull.dart';
import 'SLOTExpression_Literal.dart';

class SLOTExpression_Null extends SLOTExpression_Literal {

  @override
  void evaluate(SLOTStackFrame frame) {
    SLOTObject tn = SLOTNull.instance;
    frame.pushData(tn);
  }

  String toStackString() {return "null";}

  SLOTExpression_Null shallowCopy() {
    return this;
  }

}