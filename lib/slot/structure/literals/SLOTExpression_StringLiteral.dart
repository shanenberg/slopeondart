import '../../runtime/frames/SLOTStackFrame.dart';
import '../../runtime/objects/SLOTString.dart';
import 'SLOTExpression_Literal.dart';

class SLOTExpression_StringLiteral extends SLOTExpression_Literal {

  String value;

  @override
  void evaluate(SLOTStackFrame frame) {
    frame.pushData(new SLOTString(value));
  }

  SLOTExpression_StringLiteral(this.value);

  String toStackString() {return "String: " + this.value;}


  SLOTExpression_StringLiteral shallowCopy() {
    return this;
  }

}