import '../../runtime/frames/SLOTStackFrame.dart';
import '../../runtime/objects/SLOTSLOPETreeNode.dart';
import 'SLOTExpression_Literal.dart';

class SLOTExpression_RootGrammarNode extends SLOTExpression_Literal {

  @override
  void evaluate(SLOTStackFrame frame) {
    SLOTSLOPETreeNode tn = frame.getRuntime().slopeTreeNodes[0];
    frame.pushData(tn);
  }

  String toStackString() {return "Literal: RootNode";}

  SLOTExpression_RootGrammarNode shallowCopy() {
    return this;
  }

}