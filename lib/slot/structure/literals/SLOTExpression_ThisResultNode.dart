import '../../runtime/frames/SLOTStackFrame.dart';
import 'SLOTExpression_Literal.dart';

class SLOTExpression_ThisResultNode extends SLOTExpression_Literal {

  @override
  void evaluate(SLOTStackFrame frame) {
    frame.pushData(
        frame.currentResultSLOTNode);
  }
  String toStackString() {return "ThisResultNode:";}


  SLOTExpression_ThisResultNode shallowCopy() {
    return this;
  }
}