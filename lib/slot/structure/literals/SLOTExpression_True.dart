import '../../runtime/frames/SLOTStackFrame.dart';
import '../../runtime/objects/SLOTTrue.dart';
import 'SLOTExpression_Literal.dart';

class SLOTExpression_True extends SLOTExpression_Literal {

  @override
  void evaluate(SLOTStackFrame frame) {
    frame.pushData(SLOTTrue.instance);
  }

  String toStackString() {return "Literal: True";}

  SLOTExpression_True shallowCopy() {
    return this;
  }


}