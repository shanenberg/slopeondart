import '../../runtime/frames/SLOTStackFrame.dart';
import '../../runtime/objects/SLOTFalse.dart';
import 'SLOTExpression_Literal.dart';

class SLOTExpression_False extends SLOTExpression_Literal {

  @override
  void evaluate(SLOTStackFrame frame) {
    frame.pushData(SLOTFalse.instance);
  }

  String toStackString() {
    return "Literal: false";}

  @override
  SLOTExpression_False shallowCopy() {
    return this;
  }

}