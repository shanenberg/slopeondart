import '../../runtime/frames/SLOTStackFrame.dart';
import '../../runtime/objects/SLOTInteger.dart';
import 'SLOTExpression_Literal.dart';

class SLOTExpression_IntegerLiteral extends SLOTExpression_Literal {

  int value;

  @override
  void evaluate(SLOTStackFrame frame) {
    frame.pushData(new SLOTInteger(value));
  }

  SLOTExpression_IntegerLiteral(this.value);

  String toStackString() {return "Integer: " + this.value.toString();}

  @override
  SLOTExpression_IntegerLiteral shallowCopy() {
    return this;
  }
}