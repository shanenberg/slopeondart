import '../../runtime/frames/SLOTStackFrame.dart';
import 'SLOTExpression_Literal.dart';

class SLOTExpression_ThisGrammarNode extends SLOTExpression_Literal {

  @override
  void evaluate(SLOTStackFrame frame) {

    if (frame.currentSourceSLOTNodes.length==0) {
      throw "something strange here";
    }

    frame.pushData(frame.currentSourceSLOTNodes[0]);
  }

  String toStackString() {return "Literal: ThisGrammarNode";}

  SLOTExpression_ThisGrammarNode shallowCopy() {
    return this;
  }

}