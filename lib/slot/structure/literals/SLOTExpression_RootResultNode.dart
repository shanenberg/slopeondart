import '../../runtime/frames/SLOTStackFrame.dart';
import 'SLOTExpression_Literal.dart';

class SLOTExpression_RootResultNode extends SLOTExpression_Literal {

  @override
  void evaluate(SLOTStackFrame frame) {
    frame.pushData(frame.getRuntime().getSLOTRootResultNode());
  }

  String toStackString() {return "Literal: RootResultNode";}

  SLOTExpression_RootResultNode shallowCopy() {
    return this;
  }

}