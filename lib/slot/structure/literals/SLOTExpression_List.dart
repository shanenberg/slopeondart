import '../../runtime/SLOTObject.dart';
import '../../runtime/SLOTRuntime.dart';
import '../../runtime/frames/SLOTStackFrame.dart';
import '../../runtime/objects/SLOTList.dart';
import '../expression/SLOTExpression.dart';
import 'SLOTExpression_Literal.dart';

enum ListState {notYetStarted, parametersEvaluated, done}

class SLOTExpression_List extends SLOTExpression_Literal {

  ListState state = ListState.notYetStarted;

  List<SLOTExpression> expressionElements = new List<SLOTExpression>();
  List<SLOTObject> elementObjects = new List<SLOTObject>();

  int pushedNumExpressions = 0;
  bool listEvaluated = false;

  void evaluate(SLOTStackFrame frame) {
  }

  @override
  void nextStep(SLOTStackFrame frame) {

    switch (state) {
      case ListState.notYetStarted:
        for (int i = expressionElements.length - 1; i >= 0; i--) {
          frame.pushExpression(expressionElements[i].shallowCopy());
        }
        state = ListState.parametersEvaluated;
        return;

      case ListState.parametersEvaluated:
        elementObjects = new List<SLOTObject>();
        elementObjects.addAll(new List<SLOTObject>(expressionElements.length));

        for (int i = 0; i < expressionElements.length; i++) {
          elementObjects[expressionElements.length-i-1]=
              frame.popData();
//print("List popped " + elementObjects[expressionElements.length-i-1].toString())    ;
        }
        state = ListState.done;
        this.evaluate(frame);
        return;
      case ListState.done:
        frame.popExpression();
        frame.pushData(new SLOTList(elementObjects));
        return;
    }
  }

  String toStackString() {return "List literal: ";}

  SLOTExpression_List shallowCopy() {
    SLOTExpression_List ret = new SLOTExpression_List();
    ret.expressionElements = this.expressionElements;
    ret.pushedNumExpressions = 0;
    return ret;
  }


}