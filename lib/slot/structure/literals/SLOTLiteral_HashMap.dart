import '../../../lib/util/Pair.dart';
import '../../runtime/SLOTObject.dart';
import '../../runtime/frames/SLOTStackFrame.dart';
import '../../runtime/objects/SLOTHashMap.dart';
import '../expression/SLOTExpression.dart';
import 'SLOTExpression_Literal.dart';
import 'dart:collection';

enum HashMapState {notYetInitialized, parametersPopped, done}
class SLOTLiteral_HashMap extends SLOTExpression_Literal {

  HashMapState state = HashMapState.notYetInitialized;
  List<Pair<String, SLOTExpression>> elementExpressions = new List<Pair<String, SLOTExpression>>();
  HashMap<String, SLOTObject> elements = new HashMap<String, SLOTObject>();

  void nextStep(SLOTStackFrame frame) {

    switch (state) {
      case HashMapState.notYetInitialized:
        state = HashMapState.parametersPopped;
        for (Pair<String, SLOTExpression> anElement in elementExpressions.reversed) {
          frame.pushExpression(anElement.right.shallowCopy());
        }
        return;
      case HashMapState.parametersPopped:
        state = HashMapState.done;
        for (int i = 0; i<elementExpressions.length; i++) {
          elements[elementExpressions[i].left] = frame.popData();
        }

        frame.pushData(new SLOTHashMap(elements));
        return;

      case HashMapState.done:
        frame.popExpression();
        return;
    }

    return;
  }

  String toStackString() {return "null";}

  SLOTLiteral_HashMap shallowCopy() {
    SLOTLiteral_HashMap ret = new SLOTLiteral_HashMap();
    ret.elementExpressions = this.elementExpressions;
    return ret;

  }

  @override
  void evaluate(SLOTStackFrame frame) {
    // TODO: implement evaluate
  }
}