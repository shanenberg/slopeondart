import '../runtime/objects/SLOTString.dart';
import '../runtime/objects/SLOTTreeNode.dart';

class SLOTROOTNode extends SLOTTreeNode {

  SLOTROOTNode(): super() {
    this.value = new SLOTString("ROOT");
    this.origin = null;
  }

  String valueToString() {
    return "ROOT";
  }

  bool isRoot() {return true;}
    

}
