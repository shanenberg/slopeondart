import 'SLOTNode.dart';

class SLOTValueNode extends SLOTNode {
  SLOTValueNode(parent, this.value, typeName): super(parent, typeName);
  Object value;


  String toTreeNodeString() {
    if (typename == "STRING")
      return '"' + value.toString() +'"';
    else if (typename == "NUMBER")
      return this.typename.toString() + '#"' + value.toString() +'"';
    else
      throw "Unknown SLOT Type: " + typename.toString();
  }
}
