import '../../runtime/SLOTObject.dart';
import '../../runtime/objects/SLOTString.dart';
import 'package:test/test.dart';

import '../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../lib/parser/features/and/AND_TreeNode.dart';
import '../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../runtime/objects/SLOTTreeNode.dart';

main() {







  test("ToSlotNode - AND(AND)", () {
    var slopeNode = new AND_TreeNode();
    slopeNode.addTreeNode(new AND_TreeNode());
    SLOTTreeNode slotNode = slopeNode.toSLOTTreeNode();
    expect(slotNode.debug_SLOTTreeString() , "AND(AND())");
  });



  test("ToSlotNode - AND(NULL)", () {
    var slopeNode = new AND_TreeNode();
    slopeNode.addTreeNode(new NULL_TreeNode());
    SLOTTreeNode slotNode = slopeNode.toSLOTTreeNode();
    expect(slotNode.debug_SLOTTreeString(), "AND(NULL())");
    expect((slotNode.children.elements[0] as SLOTTreeNode).debug_SLOTTreeString(), "NULL()");
  });

  test("ToSlotNode - OR(AND)", () {
    var slopeNode = new OR_TreeNode(0, new AND_TreeNode());
    slopeNode.child.addTreeNode(new NULL_TreeNode());
    SLOTTreeNode slotNode = slopeNode.toSLOTTreeNode();
    expect(slotNode.debug_SLOTTreeString(), "OR(AND(NULL())INDEX(1()))");
    expect((slotNode.value as SLOTString).debug_SLOTTreeString(), "OR");
    expect((slotNode.children.elements[0] as SLOTTreeNode).debug_SLOTTreeString(), "AND(NULL())");
  });




}