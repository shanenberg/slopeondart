import '../../../runtime/SLOTObject.dart';
import '../../../runtime/objects/SLOTString.dart';
import '../../../runtime/objects/SLOTTreeNode.dart';
import '../../../runtime/objects/SLOTSLOPETreeNode.dart';


import '../../../../lib/parser/features/optional/OPTIONAL_TreeNode.dart';
import '../../../../lib/parser/features/bracket/BRACKET_TreeNode.dart';
import '../../../../lib/parser/features/scannerelement/ScannerElement_TreeNode.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import 'package:test/test.dart';

import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/and/AND_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';


main() {
  test("ToSlotNode - AND() -> AND()", () {
    SLOTSLOPETreeNode slotNode;
    slotNode = new AND_TreeNode().toSLOTTreeNode();
    expect(slotNode.debug_SLOTTreeString(), "AND()");
  });

  test("ToSlotNode - Rule [Named AND]", () {
    SLOTSLOPETreeNode slotNode;
    slotNode = (new AND_TreeNode()
                ..nodeName = "nameBla")
                .toSLOTTreeNode();
    expect(slotNode.debug_SLOTTreeString(), "nameBla()");
  });

  test("ToSlotNode - OR(NULL()) -> OR(NULL())", () {
    SLOTSLOPETreeNode slotNode;
    slotNode = new OR_TreeNode(-1, new NULL_TreeNode()).toSLOTTreeNode();
    expect(slotNode.debug_SLOTTreeString(), "OR(NULL()INDEX(0()))");
  });

  test("ToSlotNode - OPTIONAL() -> OPT(NULL())", () {
    SLOTSLOPETreeNode slotNode;
    slotNode = new OPTIONAL_TreeNode(null).toSLOTTreeNode();
    expect(slotNode.debug_SLOTTreeString(), "OPT(NULL())");
  });

  test("ToSlotNode - BRACKET(AND()) -> BRACKET(AND())", () {
    SLOTSLOPETreeNode slotNode;
    slotNode = new BRACKET_TreeNode(new AND_TreeNode()).toSLOTTreeNode();
    expect(slotNode.debug_SLOTTreeString(), "BRACKET(AND())");
  });

  test("ToSlotNode - NFOLD -> NFOLD", () {
    SLOTSLOPETreeNode slotNode;
    slotNode = new NFOLD_TreeNode().toSLOTTreeNode();
    expect(slotNode.debug_SLOTTreeString(), "NFOLD()");
  });


  test("ToSlotNode - NULL -> NULL", () {
    var slopeNode = new NULL_TreeNode();
    SLOTSLOPETreeNode slotNode = slopeNode.toSLOTTreeNode();
    expect(slotNode.debug_SLOTTreeString(), "NULL()");
  });

  test("ToSlotNode - SCANNER -> SLOPETOKEN", () {
    var slopeNode = new ScannerElement_TreeNode("keyWord", "scannedWord");
    SLOTSLOPETreeNode slotNode = slopeNode.toSLOTTreeNode();

    String slotNodeName = slotNode.value.valueToString();

    String slotChildNodeName = (slotNode.children.elements[0] as SLOTTreeNode).value.valueToString();

    expect(slotNodeName, "SLOPETOKEN");
    expect(slotChildNodeName, "scannedWord");

  });
}