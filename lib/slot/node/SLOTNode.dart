import 'SLOTNodeAssociation.dart';

class SLOTNode {
  String typename;
  SLOTNode parent;

  SLOTNode(this.parent, this.typename);

  List<SLOTNodeAssociation>children = new List<SLOTNodeAssociation>();

  String toTreeNodeString() {
    String ret = this.typename + "(";

    for(SLOTNodeAssociation child in children) {
      ret = ret + child.toTreeNodeString();
    }

    ret = ret + ")";
    return ret;
  }

  addAssociation(SLOTNodeAssociation association) {
    association.parent = this;
    children.add(association);
  }

  addChild(String associationName, SLOTNode node) {
    SLOTNodeAssociation association = new SLOTNodeAssociation(this, associationName, node);
    children.add(association);
  }

  SLOTNode getChild(int i) {
    return children[i].value;
  }

  bool isRoot() {return false;}

  int refNumberOfChild(SLOTNode aChildObject) {
    for(int i=0;i<children.length;i++) {
      SLOTNodeAssociation a = children[i];
      if (a.value==aChildObject) {
        return i;
      }
    }
    throw "Not contained";
  }

  SLOTNode getNextDFSPreorderNode() {
    if (this.children.length>0) {
      return this.children[0].value;
    } else {
      if (this.hasParent())  {
        return parent.parent.getNextDFSPreorderNodeFromChild(this);
      } else {
        return null;
      }
    }
  }

  bool hasParent() {
    return parent!=null;
  }

  SLOTNode getNextDFSPreorderNodeFromChild(SLOTNode slotNode) {
    int childNo = this.refNumberOfChild(slotNode);
    if (childNo+1<children.length) {
      return this.children[childNo+1].value;
    } else {
      if (this.hasParent()) {
        return parent.parent.getNextDFSPreorderNodeFromChild(this);
      } else {
        return null;
      }
    }
  }

  int getIndexInParent() {
    if (!this.hasParent())
      return -1;
    return parent.parent.refNumberOfChild(this);
  }

}
