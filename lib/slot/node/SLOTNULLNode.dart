import 'SLOTNode.dart';

class SLOTNULLNode extends SLOTNode {
  SLOTNULLNode(parent): super(parent, "NULL");


  String toTreeNodeString() {
    return "{NULL}";
  }
}
