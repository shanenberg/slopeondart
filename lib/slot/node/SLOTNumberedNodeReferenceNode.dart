import 'SLOTNode.dart';

// Objects of this class represent the number in a path expression, such as
//
class SLOTNumberedNodeRefererenceNode extends SLOTNode {

  SLOTNumberedNodeRefererenceNode(parent, this.value, typeName): super(parent, typeName);
  Object value;


  String toTreeNodeString() {
    if (typename == "STRING")
      return '"' + value.toString() +'"';
    else if (typename == "NUMBER")
      return this.typename.toString() + '#"' + value.toString() +'"';
    else
      throw "Unknown SLOT Type: " + typename.toString();
  }
}
