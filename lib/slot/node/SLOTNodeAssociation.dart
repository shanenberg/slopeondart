import 'SLOTNode.dart';

/**
 * NOTE: typeName is in fact the NAME this association!!!!!
 */
class SLOTNodeAssociation extends SLOTNode {
  SLOTNode value;

  // First param = parent, second = name of association, last one is the value
  SLOTNodeAssociation(parent, associationName, this.value): super(parent, associationName) {
    this.value.parent = this;
  }

  String toTreeNodeString() {
    if (!associationIsUnnamed()) {
      return typename + ":" + value.toTreeNodeString();
    } else {
      return value.toTreeNodeString();
    }
  }

  bool associationIsUnnamed() {
    return typename == null;
  }

}