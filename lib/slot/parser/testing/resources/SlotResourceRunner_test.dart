import '../../../../lib/parser/ResultObject.dart';
import '../../../structure/SLOTParserElement.dart';
import '../../../structure/SLOTProgram.dart';
import '../../SLOTParser.dart';
import 'dart:io';
import 'package:test/test.dart';

/* Creates grammar objects from all .slogr-files in
   resources directory
   and runs all tests defined in the grammar.
 */


/*
  Creates from the passed object a grammar object and runs the tests
 */
void testGrammar(String grammarString) {
  ResultObject<SLOTProgram> result = new ResultObject<SLOTProgram>();
  var g = new SLOTParser();
  g.parseString(grammarString, result);
}


/*
  Returns all .slogr-files
 */
List<File> allSLOGRFiles() {
  Directory currentDir =
  new Directory(
      Directory.current.path + "\\lib\\slot\\parser\\testing\\resources");

  List<FileSystemEntity> l = currentDir.listSync(recursive: true, followLinks: false);

  // Get all .slogr files
  return
    l
        .map((FileSystemEntity e) {return new File(e.path);}).toList()
        .where((File file) {
          return file.existsSync() && file.path.endsWith(".slot");}).toList();
}

void main() {
    for (File f in allSLOGRFiles()) {
      test("All slot-parsing - " + f.path.split("\\").last,
      () {
        testGrammar(f.readAsStringSync());
      });
    };
}

