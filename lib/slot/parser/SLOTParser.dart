import '../../lib/automata/regex/RegEx.dart';
import '../../lib/grammarparser/Separator.dart';
import '../../lib/grammarparser/SlopeGrammar.dart';
import '../../lib/grammarparser/Tokens.dart';
import '../../lib/parser/ResultObject.dart';
import '../../lib/parser/parserconstruction/Rule.dart';
import '../../lib/util/Pair.dart';
import '../path/SLOTANYPathExpression.dart';
import '../path/SLOTPathExpression.dart';
import '../path/SLOTPathExpressionFunctionCall.dart';
import '../path/SLOTRefPathExpression.dart';
import '../structure/SLOTBlock.dart';
import '../structure/SLOTFunctionFamily.dart';
import '../structure/SLOTParserElement.dart';
import '../structure/SLOTProgram.dart';
import '../structure/SLOTSingleFunction.dart';
import '../structure/SLOTSingleTreeFunction.dart';
import '../structure/expression/SLOTExpression.dart';
import '../structure/expression/SLOTExpression_AddNode.dart';
import '../structure/expression/SLOTExpression_PathExpression.dart';
import '../structure/expression/SLOTExpression_VarAssignment.dart';
import '../structure/expression/SLOTExpression_VarRead.dart';
import '../structure/expression/SLOTStatement.dart';
import '../structure/expression/SLOTStatement_ForEach.dart';
import '../structure/expression/SLOTStatement_IF.dart';
import '../structure/expression/SLOTStatement_Return.dart';
import '../structure/expression/SLOTStatement_While.dart';
import '../structure/expression/calls/SLOTExpression_Continue.dart';
import '../structure/expression/calls/SLOTExpression_FamilyFunctionCall.dart';
import '../structure/expression/calls/SLOTExpression_FunctionCall.dart';
import '../structure/expression/SLOTExpression_NewNode.dart';
import '../structure/expression/calls/SLOTExpression_ThisTreeFunctionCall.dart';
import '../structure/expression/calls/SLOTExpression_ThisTreeFunctionCallOnChildren.dart';
import '../structure/literals/SLOTExpression_False.dart';
import '../structure/literals/SLOTExpression_IntegerLiteral.dart';
import '../structure/literals/SLOTExpression_List.dart';
import '../structure/literals/SLOTExpression_Literal.dart';
import '../structure/literals/SLOTExpression_Null.dart';
import '../structure/literals/SLOTExpression_RootGrammarNode.dart';
import '../structure/literals/SLOTExpression_RootResultNode.dart';
import '../structure/literals/SLOTExpression_StringLiteral.dart';
import '../structure/literals/SLOTExpression_ThisGrammarNode.dart';
import '../structure/literals/SLOTExpression_ThisResultNode.dart';
import '../structure/literals/SLOTExpression_True.dart';
import '../structure/literals/SLOTLiteral_HashMap.dart';
import '../structure/expression/SLOTExpression_Lambda.dart';

SEPARATOR(var element) {
  return new Separator(element);
}

class SLOTParser extends SlopeGrammar {
  SLOTParser() : super(null);

  RegEx identifier = Tokens.fromREGEX("([a-z]|[A-Z]|_)([a-z]|[A-Z]|_|[0-9])*");
  RegEx number = Tokens.fromREGEX("[0-9]|[1-9][0-9]*");
  RegEx stringLiteral = Tokens.fromREGEX('"((^"~)|~~|~"|~n)*"');

  tokens() => [
    "TreeFunction", "Function", "ALL", "#", "\\->", "\\|",
    "\\+", ",", "=", "in", "var",
    "thisGrammarNode", "thisResultNode", "var",
    "rootGrammarNode", "rootResultNode",
    "load", "library",
    "forEach", "continue", "thisTreeFunctionOnChildren", "thisTreeFunction", ":",
    "{", "}", ";", "function", "if", "else", "return", "true", "false", "while", "null",
    "\\(", "\\)", "\\[", "\\]",
    "\\?", "=>", "\\+", "\\*", "\\(", "\\)", "\\|", "\\.",
    identifier, stringLiteral, number,
    SEPARATOR('/\\*(^\\*|\\*(^/))+\\*/'),
    SEPARATOR(" "), SEPARATOR("\n"), SEPARATOR("\r"), SEPARATOR("\t")];

  String unescapeStringLiteral(String s) {
//    print("*********** UNECAPE" + "String: " + s);
    StringBuffer ret = new StringBuffer();
    int i=1;
    for (;i<s.length-2;i++) {
      if (s.codeUnitAt(i).compareTo('~'.codeUnitAt(0))==0) {
//print("RET" + s.substring(i, i + 2));

        if (s.substring(i, i + 2).compareTo("~\"") == 0) {
//print("RET" + ret.toString()  + " " + i.toString());
          ret.write("\"");
          i++;
        } else if (s.substring(i, i + 2).compareTo("~~") == 0) {
//print("RET" + ret.toString()+ " " + i.toString());
          ret.write("~");
          i++;
        } else if (s.substring(i, i + 2).compareTo("~n") == 0) {
//print("RET" + ret.toString()+ " " + i.toString());
          ret.write("\n");
          i++;
        } else {
          throw "invalid unescaped char";
        }
      } else {
//print("RET" + ret.toString()+ " " + i.toString());
        ret.write(new String.fromCharCode(s.codeUnitAt(i)));
      }
    }
    if (i<s.length-1) {
//print("RET" + ret.toString());
//print(i);
//print(s.length);
      ret.write(new String.fromCharCode(s.codeUnitAt(i)));
    }

//    print("*********** UNESCAPED" + ret.toString());
    return ret.toString();
  }



  Rule Start(ResultObject<SLOTProgram> result) => BODY(() {
    SLOTProgram ret = new SLOTProgram();
    ResultObject<SLOTSingleFunction> singleFunction =
      new ResultObject<SLOTSingleFunction>();
    ResultObject<SLOTFunctionFamily> treeFamilty =
      new ResultObject<SLOTFunctionFamily>();
    ResultObject<String> aString =
      new ResultObject<String>();

    return
      AND([

        OPTIONAL(
          AND([
            TOKEN("library"),
            REGEXObj(identifier, aString),
            TOKEN(";"),
            DO((){
              ret.libraryName = (aString.getResult());})
            ])
        ),
        NFOLD(
            AND([
              TOKEN("load"),
              TOKEN("library"),
              REGEXObj(identifier, aString),
              TOKEN(";"),
              DO((){
                ret.requiredLibraries.add(aString.getResult());
              })
            ])
        ),
        NFOLD(
            AND([
              TOKEN("var"),
              REGEXObj(identifier, aString),
              TOKEN(";"),
              DO((){
                ret.declaredVariables.add(aString.getResult());})
            ])
        ),
        NFOLD(
          AND([
            SingleFunction(singleFunction),
            DO((){
              ret.singleFunctions.add(singleFunction.getResult());})
          ])
        ),
        NFOLD(
          OR([
            AND([
              TreeFamily(treeFamilty),
              DO((){
                ret.functionFamilies.add(treeFamilty.getResult());
              })
            ]),
          ])
        ),
        DO(()=>result.setResult(ret))
      ]);
  });


  Rule SingleFunction(ResultObject<SLOTSingleFunction> result) => BODY(() {
    SLOTSingleFunction ret = new SLOTSingleFunction(null, new List<String>(), null);
    ResultObject<String> name = new ResultObject<String>();
    ResultObject<String> paramName = new ResultObject<String>();
    ResultObject<SLOTBlock> block = new ResultObject<SLOTBlock>();
    return AND([
      REGEXObj(identifier, name), DO(()=>ret.name = name.getResult()),
      NFOLD(
        AND([
          REGEXObj(identifier, paramName),
          DO(()=>ret.parameterNames.add(paramName.getResult()))
        ])
      ),
      AND([
        Body(block),
        DO(()=>ret.body = block.getResult())
      ]),
      DO(() {
        result.setResult(ret);
      })
    ]);
  });

  Rule TreeFamily(ResultObject<SLOTParserElement> result) => BODY(() {
    SLOTFunctionFamily ret = new SLOTFunctionFamily(null);
    ResultObject<String> name = new ResultObject<String>();
    ResultObject<SLOTSingleTreeFunction> tf =
      new ResultObject<SLOTSingleTreeFunction>();

    return
      AND([
        TOKEN("TreeFunction"),
        REGEXObj(identifier, name),
        DO(()=>ret.name=name.getResult()),
        NFOLD(
          AND([
            TreeFunction(tf),
            DO(() {
              ret.functions.add(tf.getResult());
            })
          ])
        ),
        DO((){
          result.setResult(ret);
        })
      ]);

  });

  Rule TreeFunction(ResultObject<SLOTSingleTreeFunction> result) => BODY(() {
    SLOTSingleTreeFunction ret = new SLOTSingleTreeFunction(null, new List());
    ResultObject<SLOTPathExpression> p = new ResultObject<SLOTPathExpression>();
    ResultObject<SLOTBlock> b = new ResultObject<SLOTBlock>();
    ResultObject<String> paramName = new ResultObject<String>();
    return
      AND([
        PathExpression(p),
        DO((){
          ret.dispatchArgumentTypes.add(p.getResult());
        }),
        NFOLD(
          AND([
            REGEXObj(identifier, paramName),
            TOKEN(":"),
            PathExpression(p),
            DO((){
              ret.dispatchArgumentTypes.add(p.getResult());
              ret.treeparameterNames.add(paramName.getResult());
            }),
          ])
        ),
        NFOLD(
          AND([
            REGEXObj(identifier, paramName),
            DO(()=>ret.parameterNames.add(paramName.getResult()))
          ])
        ),
        Body(b),
        DO((){
          ret.body=b.getResult();
        }),
        DO(()=>result.setResult(ret))
      ]);
  });

  Rule NewNodeBody(ResultObject<SLOTBlock> result) => BODY(() {
    SLOTBlock b = new SLOTBlock();
    ResultObject<SLOTStatement> s = new ResultObject<SLOTStatement>();
    return
      AND([
        TOKEN("["),
        NFOLD(
            AND([
              Statement(s),
              DO((){
                b.statements.add(s.getResult());
              })
            ])
        ),
        TOKEN("]"),
        DO((){
          result.setResult(b);
        })
      ])
    ;});

  Rule Body(ResultObject<SLOTBlock> result) => BODY(() {
    SLOTBlock b = new SLOTBlock();
    ResultObject<SLOTStatement> s = new ResultObject<SLOTStatement>();
    return
      AND([
        TOKEN("{"),
        NFOLD(
          AND([
            Statement(s),
            DO((){
              b.statements.add(s.getResult());
            })
          ])
        ),
        TOKEN("}"),
        DO((){
          result.setResult(b);
        })
      ])
  ;});

  Rule Statement(ResultObject<SLOTStatement> result) => BODY(() {
    ResultObject<SLOTExpression_VarAssignment> a = new ResultObject<SLOTExpression_VarAssignment>();
    ResultObject<SLOTExpression> e = new ResultObject<SLOTExpression>();
    ResultObject<SLOTStatement_Return> r = new ResultObject<SLOTStatement_Return>();
    ResultObject<SLOTExpression_AddNode> addN = new ResultObject<SLOTExpression_AddNode>();
    ResultObject<SLOTStatement_IF> i = new ResultObject<SLOTStatement_IF>();
    ResultObject<SLOTStatement_While> w = new ResultObject<SLOTStatement_While>();
    ResultObject<SLOTStatement_ForEach> fe = new ResultObject<SLOTStatement_ForEach>();
    return
      AND([
        OR([
          AND([AssignStatement(a), DO(()=>result.setResult(a.getResult()))]),
          AND([Expression(e), DO(()=>result.setResult(e.getResult()))]),
          AND([ReturnStatement(r), DO(()=>result.setResult(r.getResult()))]),
          AND([IFStatement(i), DO(()=>result.setResult(i.getResult()))]),
          AND([AddNodeStatement(addN), DO(()=>result.setResult(addN.getResult()))]),
          AND([WhileStatement(w), DO(()=>result.setResult(w.getResult()))]),
          AND([ForEachStatement(fe), DO(()=>result.setResult(fe.getResult()))]),
        ]),
        TOKEN(";")
      ]);
  });

  Rule AddNodeStatement(ResultObject<SLOTExpression_AddNode> result) => BODY(() {
    SLOTExpression_AddNode ret = new SLOTExpression_AddNode(null);
    ResultObject<SLOTExpression> exp = new ResultObject<SLOTExpression>();
    ResultObject<SLOTBlock> b = new ResultObject<SLOTBlock>();
    return
      AND([
        TOKEN("+"),
        Expression(exp),
        DO(()=>ret.nodeExpression=exp.getResult()),
        OPTIONAL(
          AND([
            Body(b),
            DO((){ret.body=b.getResult();})
          ])
        ),
        DO(()=>result.setResult(ret))
      ]);
  });


  Rule WhileStatement(ResultObject<SLOTStatement_While> result) => BODY(() {
    SLOTStatement_While ret = new SLOTStatement_While();
    ResultObject<SLOTExpression> exp = new ResultObject<SLOTExpression>();
    ResultObject<SLOTBlock> b = new ResultObject<SLOTBlock>();
    return
      AND([
        TOKEN("while"),
        TOKEN("("),
        Expression(exp),
        DO(()=>ret.condition=exp.getResult()),
        TOKEN(")"),
        Body(b),
        DO(()=>ret.body=b.getResult()),
        DO(()=>result.setResult(ret))
      ]);
  });


  Rule ForEachStatement(ResultObject<SLOTStatement_ForEach> result) => BODY(() {
    SLOTStatement_ForEach ret = new SLOTStatement_ForEach();
    ResultObject<String> v = new ResultObject<String>();
    ResultObject<SLOTExpression> exp = new ResultObject<SLOTExpression>();
    ResultObject<SLOTBlock> b = new ResultObject<SLOTBlock>();

    return
      AND([
        TOKEN("forEach"),
        TOKEN("("),
        REGEXObj(identifier, v), DO(()=>ret.identifierVariable=v.getResult()),
        TOKEN("in"),
        Expression(exp), DO(()=>ret.listExpression=exp.getResult()),
        TOKEN(")"),
        Body(b), DO(()=>ret.body=b.getResult()),
        DO(()=>result.setResult(ret))
      ]);
  });

  Rule AssignStatement(ResultObject<SLOTExpression_VarAssignment> result) => BODY(() {
    SLOTExpression_VarAssignment ret = new SLOTExpression_VarAssignment(null);
    ResultObject<String> v = new ResultObject<String>();
    ResultObject<SLOTExpression> exp = new ResultObject<SLOTExpression>();
    return
      AND([
        OPTIONAL(
            AND([
              TOKEN("var"),
              DO(()=>ret.isDeclaration)
            ])
        ),
        REGEXObj(identifier, v), DO(()=>ret.targetVariable=v.getResult()),
        TOKEN("="),
        Expression(exp), DO(()=>ret.rightTerm=exp.getResult()),
        DO(()=>result.setResult(ret))
      ]);
  });

  Rule ReturnStatement(ResultObject<SLOTStatement_Return> result) => BODY(() {
    SLOTStatement_Return ret = new SLOTStatement_Return(null);
    ResultObject<SLOTExpression> exp = new ResultObject<SLOTExpression>();
    return
      AND([
        TOKEN("return"),
        OPTIONAL(
          AND([
            Expression(exp),
            DO(()=>ret.rightTerm = exp.getResult())
          ])
        ),
        DO(()=>result.setResult(ret))
      ]);
  });

  Rule IFStatement(ResultObject<SLOTStatement_IF> result) => BODY(() {
    SLOTStatement_IF ret = new SLOTStatement_IF(null, null, null);
    ResultObject<SLOTExpression> exp = new ResultObject<SLOTExpression>();
    ResultObject<SLOTBlock> b = new ResultObject<SLOTBlock>();
    return
      AND([
        TOKEN("if"),
        TOKEN("("),
        Expression(exp), DO(()=>ret.condition = exp.getResult()),
        TOKEN(")"),
        Body(b), DO(()=>ret.thenBlock = b.getResult()),
        OPTIONAL(
          AND([
            TOKEN("else"),
            Body(b), DO(()=>ret.elseBlock = b.getResult())
          ])
        ),
        DO((){
          result.setResult(ret);
        })
      ]);
  });

  Rule Expression(ResultObject<SLOTExpression> result) => BODY(() {

    ResultObject<String> functionName = new ResultObject<String>();
    ResultObject<SLOTExpression> singleParam = new ResultObject<SLOTExpression>();
    ResultObject<List<SLOTExpression>> multiParams = new ResultObject<List<SLOTExpression>>();



    return
        AND([
          Expression0(result),

          NFOLD(
            AND([
                  REGEXObj(identifier, functionName)
                 ,OR([
                    AND([Expression0(singleParam)
                         ,DO(() => result.setResult(new SLOTExpression_FunctionCall(functionName.getResult(),
                                                                                [result.getResult(), singleParam.getResult()])))
                    ])
                   ,AND([ParameterListInBrackets(multiParams)
                        ,DO(() => result.setResult(new SLOTExpression_FunctionCall(functionName.getResult()
                                                          , multiParams.getResult()..insert(0, result.getResult()))
                                                  ))
                        ])
                  ])

            ])
          )
        ]);

  });

  Rule Lambda(ResultObject<SLOTExpression_Lambda> result) => BODY(() {
    SLOTExpression_Lambda ret = new SLOTExpression_Lambda(null);
    ResultObject<SLOTExpression> exp = new ResultObject<SLOTExpression>();
    ResultObject<SLOTBlock> b = new ResultObject<SLOTBlock>();

    ResultObject<String> paramName = new ResultObject<String>();
    ResultObject<SLOTBlock> block = new ResultObject<SLOTBlock>();

    return AND([
      TOKEN("function"),
      NFOLD(
          AND([
            REGEXObj(identifier, paramName),
            DO(()=>ret.parameterNames.add(paramName.getResult()))
          ])
      ),
      AND([
        Body(block),
        DO(()=>ret.body = block.getResult())
      ]),
      DO(() {
        result.setResult(ret);
      })
    ]);
  });

  Rule Expression0(ResultObject<SLOTExpression> result) => BODY(() {
    ResultObject<SLOTExpression_ThisGrammarNode> thisNode = new ResultObject<SLOTExpression_ThisGrammarNode>();
    ResultObject<SLOTExpression_True> t = new ResultObject<SLOTExpression_True>();
    ResultObject<SLOTExpression_False> f = new ResultObject<SLOTExpression_False>();
    ResultObject<SLOTExpression_Literal> num = new ResultObject<SLOTExpression_Literal>();
    ResultObject<SLOTExpression_Literal> string = new ResultObject<SLOTExpression_Literal>();
    ResultObject<SLOTExpression_ThisResultNode> resultNode = new ResultObject<SLOTExpression_ThisResultNode>();
    ResultObject<SLOTExpression_FunctionCall> fc = new ResultObject<SLOTExpression_FunctionCall>();
    ResultObject<SLOTExpression_FamilyFunctionCall> ffc = new ResultObject<SLOTExpression_FamilyFunctionCall>();
    ResultObject<SLOTExpression_VarRead> read = new ResultObject<SLOTExpression_VarRead>();
    ResultObject<SLOTExpression_RootGrammarNode> rootGrammarNode = new ResultObject<SLOTExpression_RootGrammarNode>();
    ResultObject<SLOTExpression_RootResultNode> rootResultNode = new ResultObject<SLOTExpression_RootResultNode>();
    ResultObject<SLOTExpression_List> list = new ResultObject<SLOTExpression_List>();
    ResultObject<SLOTLiteral_HashMap> hashmapLiteral = new ResultObject<SLOTLiteral_HashMap>();
    ResultObject<SLOTExpression_Literal> aNull = new ResultObject<SLOTExpression_Literal>();
    ResultObject<SLOTExpression_Continue> aContinue = new ResultObject<SLOTExpression_Continue>();
    ResultObject<SLOTExpression_ThisTreeFunctionCall> tfc = new ResultObject<SLOTExpression_ThisTreeFunctionCall>();
    ResultObject<SLOTExpression_ThisTreeFunctionCallOnChildren> tfcoc = new ResultObject<SLOTExpression_ThisTreeFunctionCallOnChildren>();
    ResultObject<SLOTExpression_Lambda> lambda = new ResultObject<SLOTExpression_Lambda>();
    ResultObject<SLOTExpression_PathExpression> pexp = new ResultObject<SLOTExpression_PathExpression>();
    ResultObject<SLOTExpression_NewNode> nnexp = new ResultObject<SLOTExpression_NewNode>();
    ResultObject<String> aString = new ResultObject<String>();
    ResultObject<SLOTExpression> rightAssignmentExpression = new ResultObject<SLOTExpression>();

    return
      AND([
        OR([
          AND([SLOTList(list), DO(()=>result.setResult(list.getResult()))]),
          AND([HashMapLiteral(hashmapLiteral), DO(()=>result.setResult(hashmapLiteral.getResult()))]),
          AND([NewNode(nnexp), DO(()=>result.setResult(nnexp.getResult()))]),
          AND([NumLiteral(num), DO(()=>result.setResult(num.getResult()))]),
          AND([FamilyFunctionCall(ffc), DO(()=>result.setResult(ffc.getResult()))]),
          AND([StringLiteral(string), DO(()=>result.setResult(string.getResult()))]),
          AND([TrueLiteral(t), DO(()=>result.setResult(t.getResult()))]),
          AND([FalseLiteral(f), DO(()=>result.setResult(f.getResult()))]),
          AND([Null(aNull), DO(()=>result.setResult(aNull.getResult()))]),
          AND([FunctionCall(fc), DO(()=>result.setResult(fc.getResult()))]),
          AND([Lambda(lambda), DO(()=>result.setResult(lambda.getResult()))]),
          AND([ThisTreeFunctionCallOnChildren(tfcoc), DO(()=>result.setResult(tfcoc.getResult()))]),
          AND([ThisTreeFunctionCall(tfc), DO(()=>result.setResult(tfc.getResult()))]),
          AND([VarRead(read), DO(()=>result.setResult(read.getResult()))]),
          AND([RootGrammarNode(rootGrammarNode), DO(()=>result.setResult(rootGrammarNode.getResult()))]),
          AND([RootResultNode(rootResultNode), DO(()=>result.setResult(rootResultNode.getResult()))]),
          AND([ThisNodeLiteral(thisNode), DO(()=>result.setResult(thisNode.getResult()))]),
          AND([ResultNodeLiteral(resultNode), DO(()=>result.setResult(resultNode.getResult()))]),
          AND([Continue(aContinue), DO(()=>result.setResult(aContinue.getResult()))])
        ]),
        OPTIONAL(
          OR([
            AND([
              PathExpressionExpressionTail(pexp),
              DO(() {
                SLOTExpression pre = result.getResult();
                SLOTExpression_PathExpression pathExpression = pexp.getResult();
                pathExpression.targetTerm = pre;
                result.setResult(pathExpression);
              })
            ]),
            AND([
              TOKEN(":"),
              REGEXObj(identifier, aString),
              DO(() {
                SLOTExpression pre = result.getResult();
                SLOTExpression_PathExpression pathExpression = pexp.getResult();
                pathExpression.targetTerm = pre;
                result.setResult(pathExpression);
              }),
              OPTIONAL(
                AND([
                  TOKEN("="),
                  Expression(rightAssignmentExpression),
                  DO(() {
                    SLOTExpression pre = result.getResult();
                    SLOTExpression_PathExpression pathExpression = pexp.getResult();
                    pathExpression.targetTerm = pre;
                    result.setResult(pathExpression);
                  }),
                ])
              )
            ])
          ])
        )
      ]);
  });

  Rule NumLiteral(ResultObject<SLOTExpression_Literal> result) => BODY(() {
    ResultObject<String> aString = new ResultObject<String>();
    return AND([
      REGEXObj(number, aString),
      DO(()=>result.setResult(new SLOTExpression_IntegerLiteral(int.parse(aString.getResult()))))
    ]);
  });

  Rule StringLiteral(ResultObject<SLOTExpression_Literal> result) => BODY(() {
    ResultObject<String> aString = new ResultObject<String>();
    return AND([
      REGEXObj(stringLiteral, aString),
      DO(()=>result.setResult(new SLOTExpression_StringLiteral(
        unescapeStringLiteral(aString.getResult()))))
    ]);
  });

  Rule RootResultNode(ResultObject<SLOTExpression_RootResultNode> result) => BODY(() {
    return AND([
      TOKEN("rootResultNode"),
      DO(()=>result.setResult(new SLOTExpression_RootResultNode()))
    ]);
  });

  Rule Null(ResultObject<SLOTExpression_Literal> result) => BODY(() {
    return AND([
      TOKEN("null"),
      DO(()=>result.setResult(new SLOTExpression_Null()))
    ]);
  });

  Rule NewNode(ResultObject<SLOTExpression_NewNode> result) => BODY(() {
    SLOTExpression_NewNode newNode = new SLOTExpression_NewNode(null);
    ResultObject<SLOTExpression> paramObj = new ResultObject<SLOTExpression>();
    ResultObject<SLOTBlock> body = new ResultObject<SLOTBlock>();
    return AND([
      TOKEN("#"),
      Expression(paramObj),
      DO(() {
        newNode.nodeExpression = paramObj.getResult();
      }),
      OPTIONAL(
        AND([
          NewNodeBody(body),
          DO(() {
            newNode.body = body.getResult();
          }),
        ])
      ),
      DO(() {
        result.setResult(newNode);
      })
    ]);
  });

  Rule RootGrammarNode(ResultObject<SLOTExpression_RootGrammarNode> result) => BODY(() {
    return AND([
      TOKEN("rootGrammarNode"),
      DO(()=>result.setResult(new SLOTExpression_RootGrammarNode()))
    ]);
  });

  Rule ThisNodeLiteral(ResultObject<SLOTExpression_ThisGrammarNode> result) => BODY(() {
    return AND([
      TOKEN("thisGrammarNode"),
      DO(()=>result.setResult(new SLOTExpression_ThisGrammarNode()))

    ]);
  });

  Rule PathExpressionExpressionTail(ResultObject<SLOTExpression_PathExpression> result) => BODY(() {
    SLOTExpression_PathExpression ret = new SLOTExpression_PathExpression(null);
    ResultObject<String> aNumberString = new ResultObject<String>();
    return AND([
      NFOLD1(
        AND([
          TOKEN("."),
          TOKEN("#"),
          REGEXObj(number, aNumberString),
          DO(() {
            int anInt = int.parse(aNumberString.getResult());
            ret.targetNumbers.add(anInt);
          })
        ])
      ),
      DO(() {
        result.setResult(ret);
      })      
    ]);
  });



  Rule ResultNodeLiteral(ResultObject<SLOTExpression_ThisResultNode> result) => BODY(() {
    return AND([
      TOKEN("thisResultNode"),
      DO(()=>result.setResult(new SLOTExpression_ThisResultNode()))

    ]);
  });

  Rule TrueLiteral(ResultObject<SLOTExpression_True> result) => BODY(() {
    return AND([
      TOKEN("true"),
      DO(()=>result.setResult(new SLOTExpression_True()))
    ]);
  });



  Rule HashMapLiteral(ResultObject<SLOTLiteral_HashMap> result) => BODY(() {
    SLOTLiteral_HashMap ret = new SLOTLiteral_HashMap();
    ResultObject<String> aString = new ResultObject<String>();
    ResultObject<SLOTExpression> anExpression = new ResultObject<SLOTExpression>();
    return AND([
      TOKEN("{"),

      OPTIONAL(
        AND([
          REGEXObj(identifier, aString),
          TOKEN(":"),
          Expression(anExpression),
          DO(()=>ret.elementExpressions.add(new Pair(aString.getResult(), anExpression.getResult()))),


      NFOLD(
          AND([
            TOKEN(","),
            REGEXObj(identifier, aString),
            TOKEN(":"),
            Expression(anExpression),
            DO(()=>ret.elementExpressions.add(new Pair(aString.getResult(), anExpression.getResult()))),
          ])
      )])
    ),
      TOKEN("}"),
      DO(()=>result.setResult(ret)),
      DO((){
        print("did hashmap");
      })
    ]);
  });


  Rule SLOTList(ResultObject<SLOTExpression_List> result) => BODY(() {
    SLOTExpression_List ret = new SLOTExpression_List();
    ResultObject<SLOTExpression> exp = new ResultObject<SLOTExpression>();
    return AND([
      TOKEN("["),
      OPTIONAL(
        AND([
          Expression(exp),
          DO(()=>ret.expressionElements.add(exp.getResult())),
          NFOLD(
            AND([
              TOKEN(","),
              Expression(exp),
              DO(() {
                ret.expressionElements.add(exp.getResult());
              })
            ])
          ),
        ])
      ),
      TOKEN("]"),
      DO(()=>result.setResult(ret))
    ]);
  });

  Rule FalseLiteral(ResultObject<SLOTExpression_False> result) => BODY(() {
    return AND([
      TOKEN("false"),
      DO(()=>result.setResult(new SLOTExpression_False()))
    ]);
  });

  Rule VarRead(ResultObject<SLOTExpression_VarRead> result) => BODY(() {
    ResultObject<String> aString = new ResultObject<String>();
    return AND([
        REGEXObj(identifier, aString),
        DO(()=>result.setResult(new SLOTExpression_VarRead(aString.getResult())))
    ]);
  });

  Rule Continue(ResultObject<SLOTExpression_Continue> result) => BODY(() {
    SLOTExpression_Continue ret = new SLOTExpression_Continue(null);
    ResultObject<List<SLOTExpression>> params = new ResultObject<List<SLOTExpression>>();
    return
      AND([
        TOKEN("continue"),
        OPTIONAL(
          AND([
            ParameterListInBrackets(params), DO(()=>ret.parameterExpressions=params.getResult()),
            DO(()=>result.setResult(ret))
          ])
        ),
        DO(()=>result.setResult(ret))
      ])
    ;
  });

  Rule ThisTreeFunctionCallOnChildren(ResultObject<SLOTExpression_ThisTreeFunctionCallOnChildren> result) => BODY(() {
    SLOTExpression_ThisTreeFunctionCallOnChildren ret = new SLOTExpression_ThisTreeFunctionCallOnChildren(null);
    ResultObject<String> aString = new ResultObject<String>();
    ResultObject<List<SLOTExpression>> params = new ResultObject<List<SLOTExpression>>();
    return
      AND([
        TOKEN("thisTreeFunctionOnChildren"),
        AND([
          ParameterListInBrackets(params), DO(()=>ret.parameterExpressions=params.getResult())
        ]),
        DO(()=>result.setResult(ret))
      ])
    ;
  });


  Rule ThisTreeFunctionCall(ResultObject<SLOTExpression_ThisTreeFunctionCall> result) => BODY(() {
    SLOTExpression_ThisTreeFunctionCall ret = new SLOTExpression_ThisTreeFunctionCall(null);
    ResultObject<String> aString = new ResultObject<String>();
    ResultObject<List<SLOTExpression>> params = new ResultObject<List<SLOTExpression>>();
    return
      AND([
        TOKEN("thisTreeFunction"),
        AND([
          ParameterListInBrackets(params), DO(()=>ret.parameterExpressions=params.getResult())
        ]),
        DO(()=>result.setResult(ret))
      ])
    ;
  });


  Rule FunctionCall(ResultObject<SLOTExpression_FunctionCall> result) => BODY(() {
    SLOTExpression_FunctionCall ret = new SLOTExpression_FunctionCall(null, null);
    ResultObject<String> aString = new ResultObject<String>();
    ResultObject<List<SLOTExpression>> params = new ResultObject<List<SLOTExpression>>();
    return
      AND([
        REGEXObj(identifier, aString), DO(()=>ret.functionName=aString.getResult()),
        ParameterListInBrackets(params), DO(()=>ret.parameterExpressions=params.getResult()),
        DO(()=>result.setResult(ret))
      ])
    ;
  });

  Rule FamilyFunctionCall(ResultObject<SLOTExpression_FamilyFunctionCall> result) => BODY(() {
    SLOTExpression_FamilyFunctionCall ret = new SLOTExpression_FamilyFunctionCall(new List<String>(), null);
    ResultObject<String> aString = new ResultObject<String>();
    ResultObject<List<SLOTExpression>> params = new ResultObject<List<SLOTExpression>>();
    return
      AND([
        TOKEN("->"),
        TOKEN("("),
        AND([
          REGEXObj(identifier, aString), DO(()=>ret.functionNames.add(aString.getResult())),
          NFOLD(
            AND([
              TOKEN("|"),
              REGEXObj(identifier, aString), DO(()=>ret.functionNames.add(aString.getResult())),
            ])
          )

        ]),
        TOKEN(")"),
        ParameterListInBrackets(params), DO(()=>ret.parameterExpressions=params.getResult()),
        DO(()=>result.setResult(ret))
      ])
    ;
  });

/*
            TOKEN("("),
          OPTIONAL(
            AND([
              ParameterList(params), DO(()=>ret.parameterExpressions=params.getResult())
            ]),
          ),
          TOKEN(")")
   */
  Rule ParameterListInBrackets(ResultObject<List<SLOTExpression>> result) => BODY(() {
    ResultObject<List<SLOTExpression>> params = new ResultObject<List<SLOTExpression>>();
    return
      AND([
      TOKEN("("),
        OPTIONAL(
          AND([
            ParameterList(params)
          ]),
        ),
      TOKEN(")"),
      DO(() {
        result.setResult(params.getResult());
      })
    ]);
  });

  Rule ParameterList(ResultObject<List<SLOTExpression>> result) => BODY(() {
    List<SLOTExpression> ret = new List<SLOTExpression>();
    ResultObject<SLOTExpression> exp = new ResultObject<SLOTExpression>();


    return
      AND([
        Expression(exp), DO(()=>ret.add(exp.getResult())),
        NFOLD(
           AND([
             TOKEN(","),
             Expression(exp),
             DO(()=>ret.add(exp.getResult())),
           ])
        ),
        DO(() {
          result.setResult(ret);
        })
    ]);
  });

  Rule PathExpression(ResultObject<SLOTPathExpression> result) => BODY(() {
    ResultObject<SLOTPathExpression> ret =
      new ResultObject<SLOTPathExpression>();
    ResultObject<SLOTPathExpression> next =
      new ResultObject<SLOTPathExpression>();
    SLOTPathExpression last;
    return
        AND([
          PathElement(ret), DO(()=>last=ret.getResult()),
          NFOLD(
            AND([
              TOKEN("."),
              PathElement(next),
              DO(() {
                next.getResult().parent = last;
                last = next.getResult();
                }),
            ])
          ),
          DO((){
            result.setResult(last);
          })
        ]);
  });

  Rule PathElement(ResultObject<SLOTPathExpression> result) => BODY(() {
    ResultObject<SLOTPathExpression> ret =
      new ResultObject<SLOTPathExpression>();
    return
      OR([
        AND([NamedPathElement(ret), DO(()=>result.setResult(ret.getResult()))]),
        AND([StringLiteralPathElement(ret), DO(()=>result.setResult(ret.getResult()))]),
        AND([ALLPathElement(ret), DO(()=>result.setResult(ret.getResult()))]),
        AND([RefPathElement(ret), DO(()=>result.setResult(ret.getResult()))])
      ]);
  });


  Rule NamedPathElement(ResultObject<SLOTPathExpression> result) => BODY(() {
    SLOTPathExpression ret;
    ResultObject<String> aString = new ResultObject<String>();
    List<SLOTExpression_Literal> literals= new List<SLOTExpression_Literal>();
    ResultObject<SLOTExpression_Literal> aLiteral= new ResultObject<SLOTExpression_Literal>();
    return
      AND([
        REGEXObj(identifier, aString),
        DO((){
                    ret = new SLOTPathExpression(null, aString.getResult());
        }),
        OPTIONAL(
          AND([
            TOKEN("("),
            NFOLD(
              AND([
                OR([
                  NumLiteral(aLiteral),
                  StringLiteral(aLiteral),
                  Null(aLiteral)
                ]),
              DO(() {
                literals.add(aLiteral.getResult());
              })
              ])
            ),
            TOKEN(")"),
            DO(() {
              ret =
                new SLOTPathExpressionFunctionCall(literals, null, aString.getResult());
            })
          ])
        ),
        DO(()=> result.setResult(ret))
      ]);
  });

  Rule StringLiteralPathElement(ResultObject<SLOTPathExpression> result) => BODY(() {
    ResultObject<String> aString = new ResultObject<String>();
    return AND([
      REGEXObj(stringLiteral, aString),
      DO(()=>
          result.setResult(new SLOTPathExpression(
              null,
              unescapeStringLiteral(aString.getResult()))))
    ]);
  });

  Rule ALLPathElement(ResultObject<SLOTPathExpression> result) => BODY(() {
    return AND([
      TOKEN("ALL"), DO(()=>result.setResult(new SLOTANYPathExpression(null)))
    ]);
  });

  Rule RefPathElement(ResultObject<SLOTPathExpression> result) => BODY(() {
    SLOTRefPathExpression ret = new SLOTRefPathExpression(null, null);
    ResultObject<String> aString = new ResultObject<String>();
    return
      AND([
        TOKEN("#"),
        REGEXObj(number, aString),
          DO((){
            int val = int.parse(aString.getResult());
            ret.refNumber = val;
            result.setResult(ret);
          }),
      ])
  ;});


}