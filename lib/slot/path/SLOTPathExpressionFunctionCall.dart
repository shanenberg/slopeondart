import '../runtime/SLOTObject.dart';
import '../runtime/frames/SLOTStackFrame.dart';
import '../runtime/objects/SLOTBoolean.dart';
import '../runtime/objects/SLOTString.dart';
import '../runtime/objects/SLOTTreeNode.dart';
import '../structure/literals/SLOTExpression_Literal.dart';
import 'SLOTPathExpression.dart';

class SLOTPathExpressionFunctionCall extends SLOTPathExpression {

  List<SLOTExpression_Literal> literals = new List<SLOTExpression_Literal>();

  SLOTPathExpressionFunctionCall(this.literals, parent, value): super(parent, value);

  bool isReference() { return false;}
  bool isANYNode() { return false;}
  bool isFunctionCallNode() { return true;}

  bool matchesSlotTreeNode(SLOTStackFrame frame, SLOTTreeNode n) {

    List<SLOTObject> parameters = new List<SLOTObject>();
    parameters.add(n);

    for(SLOTExpression_Literal l in literals) {
      l.evaluate(frame);
      parameters.add(frame.popData());
    }

    var x = frame.getRuntime().runPathExpressionFunctionCallFunction(value, parameters);

    bool functionResult = (x.result as SLOTBoolean).boolValue();

    if (!functionResult)
      return false;
    else {
      if(this.parent==null)
        return true;
      else
        if (n.parent != null)
          return this.parent.matchesSlotTreeNode(frame, n.parent);
        else
          return false;
    }
  }

  String debug_String() {
    String ret = "";
    if (parent!=null) {
      ret = parent.debug_String() + ".";
      ret = ret + debug_this_StringValue();
      return ret;
    } else {
      return debug_this_StringValue();
    }
  }

  String debug_this_StringValue() {
    return value;
  }

}