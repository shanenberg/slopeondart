import '../runtime/frames/SLOTStackFrame.dart';
import '../runtime/objects/SLOTString.dart';
import '../runtime/objects/SLOTTreeNode.dart';
import '../runtime/objects/SLOTInteger.dart';

class SLOTPathExpression {
  SLOTPathExpression parent;
  String value;

  SLOTPathExpression(this.parent, this.value);

  bool isReference() { return false;}
  bool isANYNode() { return false;}

  bool matchesSlotTreeNode(SLOTStackFrame frame, SLOTTreeNode n) {
    bool ret;
    String otherValue;
    try {
      if (n.value is SLOTInteger) {
        otherValue = (n.value as SLOTInteger).val.toString();
      } else {
        otherValue = (n.value as SLOTString).value;
      }
      ret = otherValue == this.value;
    }
    catch(e) {
      frame.printStackFrame();
      throw(e);
    }
    if (!ret) return false;
    if(this.parent==null) return true;
    return this.parent.matchesSlotTreeNode(frame, n.parent);
  }

  String debug_String() {
    String ret = "";
    if (parent!=null) {
      ret = parent.debug_String() + ".";
      ret = ret + debug_this_StringValue();
      return ret;
    } else {
      return debug_this_StringValue();
    }
  }

  String debug_this_StringValue() {
    return value;
  }

}