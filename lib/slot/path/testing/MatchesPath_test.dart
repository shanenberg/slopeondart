import 'package:test/test.dart';

import '../../../lib/parser/features/and/AND_TreeNode.dart';
import '../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../runtime/objects/SLOTTreeNode.dart';
import '../SLOTPathExpression.dart';
import '../SLOTRefPathExpression.dart';

pe(String value) {

  List<String> elements = value.split(".");
  SLOTPathExpression parent = null;
  for(String e in elements) {
    if(e.startsWith("#")) {
      parent = new SLOTRefPathExpression(parent, int.parse(e.substring(1)));
    } else {
      parent = new SLOTPathExpression(parent, e);
    }
  }

  return parent;
}

main() {

    test("ToSlotTree: ->AND<-(AND) matches AND", () {
      AND_TreeNode node;
      SLOTPathExpression path;

      node = new AND_TreeNode();
      node.addTreeNode(new AND_TreeNode());

      path = pe("AND");
      expect(path.matchesSlotTreeNode(null, node.toSLOTTreeNode()), true);
    });

    test("ToSlotTree: AND(>-AND<-) matches AND", () {
      AND_TreeNode node;
      SLOTTreeNode slotNode;
      var path;
      node = new AND_TreeNode();
      node.addTreeNode(new AND_TreeNode());
      slotNode = node.toSLOTTreeNode();
      slotNode = slotNode.children.elements[0];

      path = pe("AND");
      expect(path.matchesSlotTreeNode(null, node.toSLOTTreeNode()), true);
    });

    test("ToSlotTree: OR(AND) matches AND", () {
      var node, inner;
      var path;
      SLOTTreeNode slotNode;
      node = new OR_TreeNode(0, new AND_TreeNode());
      slotNode = node.toSLOTTreeNode();
      slotNode = slotNode.children.elements[0];

      path = pe("AND");
      expect(path.matchesSlotTreeNode(null, slotNode), true);
    });


    test("ToSlotTree: OR(AND) matches AND", () {
      var node, inner;
      var path;
      SLOTTreeNode slotNode;
      node = new OR_TreeNode(0, new AND_TreeNode());
      slotNode = node.toSLOTTreeNode();
      slotNode = slotNode.children.elements[0];

      path = pe("OR.AND");
      expect(path.matchesSlotTreeNode(null, slotNode), true);
    });

    test("ToSlotTree: OR(AND) matches AND", () {
      var node, inner;
      var path;
      SLOTTreeNode slotNode;
      node = new OR_TreeNode(0, new AND_TreeNode());
      slotNode = node.toSLOTTreeNode();
      slotNode = slotNode.children.elements[0];

      path = pe('OR.#1');
      expect(path.matchesSlotTreeNode(null, slotNode), true);
    });

}