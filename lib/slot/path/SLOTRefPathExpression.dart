import '../runtime/frames/SLOTStackFrame.dart';
import '../runtime/objects/SLOTTreeNode.dart';
import 'SLOTPathExpression.dart';

class SLOTRefPathExpression extends SLOTPathExpression {
  SLOTRefPathExpression(
      SLOTPathExpression parent, this.refNumber) : super(parent, null);

  int refNumber;

  bool isReference() { return true;}

  bool matchesSlotTreeNode(SLOTStackFrame frame, SLOTTreeNode n) {
//print("matches " + refNumber.toString());
    if(this.parent==null)
      throw "refPathExpressions require parent object";
    if(n.parent==null) return false;
    return 
      (n.parent.refNumberOfChild(n) + 1)==refNumber && this.parent.matchesSlotTreeNode(frame, n.parent);
  }

  String debug_this_StringValue() {
    return "#" + refNumber.toString();
  }
}