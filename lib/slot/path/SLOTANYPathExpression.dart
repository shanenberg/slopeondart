import '../runtime/frames/SLOTStackFrame.dart';
import '../runtime/objects/SLOTTreeNode.dart';
import 'SLOTPathExpression.dart';

class SLOTANYPathExpression extends SLOTPathExpression {
  SLOTANYPathExpression(SLOTPathExpression parent)
      : super(parent, null);

  bool isANYNode() {
    return true;
  }

  bool matchesSlotTreeNode(SLOTStackFrame frame, SLOTTreeNode n) {
    if(this.parent==null) return true;
    if (n.parent!=null)
      return this.parent.matchesSlotTreeNode(frame, n.parent);
    return false;
  }

  String debug_this_StringValue() {
    return "ALL";
  }

}