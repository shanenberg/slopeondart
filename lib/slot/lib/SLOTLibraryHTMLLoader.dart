import '../../lib/parser/ResultObject.dart';
import '../parser/SLOTParser.dart';
import '../runtime/SLOTRuntime.dart';
import '../structure/SLOTProgram.dart';
import 'SLOTLibraryLoader.dart';
import 'dart:io';
import 'generatedHTMLLib/SLOTGeneratedLib.dart';

/*
  This class will somewhere in the near future .....
 */

class SLOTLibraryHTMLLoader extends SLOTLibraryLoader {

  /**
   * Quite brute-force.....better solution required here. Well, we can live with
   * it, until libraries become too large....
   */
  @override

  void loadAllLibraries() {
    loadLibraryProgramObjects();
  }

  loadAndInitLibraryNamed(String libName, SLOTRuntime runtime) {

    if (!loadedLibraries.containsKey(libName)){
      throw "Unknown library named: " + libName;
    }

    if (aboutToLoadAndInitLibaries.contains(libName)) {
      throw "Circular libraries loaded";
    }


    // everything done....
    if (initializedLibraries.containsKey(libName))
      return;
    else {
      aboutToLoadAndInitLibaries.add(libName);

      for(String aLibraryName in loadedLibraries[libName].requiredLibraries) {
        loadAndInitLibraryNamed(aLibraryName, runtime);
      }

      loadedLibraries[libName].runtime = runtime;
      SLOTProgram p = loadedLibraries[libName];
      p.initSLOTProgram();

      aboutToLoadAndInitLibaries.remove(libName);

    }

  }

  void loadLibraryProgramObjects() {

    for(String aString in allLibraryFilesAsStrings()) {
      ResultObject<SLOTProgram> result2 = new ResultObject<SLOTProgram>();
      var slotParser = new SLOTParser();
      try {
        slotParser.parseString(aString, result2);
      } catch (ex) {
        throw "error while parsing: " + aString + " " + ex;
      }
      var slotProgram = result2.getResult();
      loadedLibraries[slotProgram.libraryName] = slotProgram;
    }

  }

  List<String> allLibraryFilesAsStrings() {
    return loadPreStoredLibraryStrings();
  }



}