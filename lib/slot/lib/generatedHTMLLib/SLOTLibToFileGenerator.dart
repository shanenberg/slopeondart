import 'dart:io';

/*
  This class will somewhere in the near future .....
 */
main() {
  File f = new File(Directory.current.path + "\\lib\\slot\\lib\\generatedHTMLLib\\SLOTGeneratedLib.dart");

  if(f.existsSync()) { f.delete(); }
  var s = f.openWrite();
  s.writeln(
'''/***********************************************************
I am purely generated!!!! Don't touch me, touch the slot files 
***************************************************************/'''
  );

  s.writeln("loadPreStoredLibraryStrings(){");
  s.writeln("List<String> htmlPreGeneratedLibFileStrings = new List<String>();");

  for(String aString in allLibraryFilesAsStrings()) {
    s.writeln("htmlPreGeneratedLibFileStrings.add('''");
    s.writeln(aString);
    s.writeln("''');");
  }
  s.writeln("return htmlPreGeneratedLibFileStrings;");
  s.writeln("}");




  s.close();



}

List<String> allLibraryFilesAsStrings() {
  List<String> ret = new List<String>();
  for(File aFile in allLibraryFiles()) {
    ret.add(aFile.readAsStringSync());
  }
  return ret;
}

List<File> allLibraryFiles() {
    Directory currentDir =
    new Directory(
        Directory.current.path + "\\lib\\slot\\lib\\slotFiles");

    List<FileSystemEntity> l = currentDir.listSync(recursive: true, followLinks: false);

    return
      l
          .map((FileSystemEntity e) {return new File(e.path);}).toList()
          .where((File file) {
        return file.existsSync() && file.path.endsWith(".slot");}).toList();
}