/***********************************************************
I am purely generated!!!! Don't touch me, touch the slot files 
***************************************************************/
loadPreStoredLibraryStrings(){
List<String> htmlPreGeneratedLibFileStrings = new List<String>();
htmlPreGeneratedLibFileStrings.add('''
library htmlConstruction;

html_createCloseTag tagName {
  var ret = concat("</", tagName);
  ret = concat(ret, ">");
  return ret;
}

html_createCloseTagFromNode aNode {
    return html_createCloseTag(slotNodeName(aNode));
}

html_createOpenTag tagName {
  var ret = concat("<", tagName);
  ret = concat(ret, ">");
  return ret;
}

html_createOpenTagFromNode aNode aNodeList {
    return html_createOpenTag(
        concat(
            slotNodeName(aNode),
            html_generateIDString(aNode,aNodeList)
        )
    );
}


html_createOpenTagWithAdditional tagName additionalString {
  var ret = concat("<", tagName);
  ret = concat(ret, additionalString);
  ret = concat(ret, ">");
  return ret;
}

html_createOpenTagFromNodeWithAdditional aNode aNodeList aString {
    return html_createOpenTag(
        concatAll(
            slotNodeName(aNode),
            [html_generateIDString (aNode, aNodeList), " ", aString]
        )
    );
}

html_createButtonWithLabelAndAction labelString actionString {
    return
        #"Button"[
            +#"Label"[+#labelString;];
            +#"ActionName"[+#actionString;];
        ];
}

html_generateIDString aNode aNodeList {
    addElement(aNodeList, aNode);
    var nodeID = length(nodesToHTML);
    return concatAll(" id=~"", [intToString(nodeID), "~""]);
}


''');
htmlPreGeneratedLibFileStrings.add('''
/*
  Currently, this stuff is not used.
  in the near future, this is how we will represent libraries
*/

library util;

concatAll aString aList {
    var resultString=aString;
    forEach(s in aList) {
        resultString = concat(resultString, s);
    };
    return resultString;
}

printTree aTreeNode {
    printTreeWithOffset(aTreeNode, "");
}

printTreeWithOffset aTreeNode aString {
    printString(concat(aString, slotNodeName(aTreeNode)));
    forEach(aChild in children(aTreeNode)) {
        printTreeWithOffset(aChild, concat(aString, "  "));
    };
}

newNode aString {
    return #aString;
}

nodeNameEquals aNode aString {
    if (isNull(aNode)) {
        return false;
    };
    return equalsString(slotNodeName(aNode), aString);
}


nextMatchingNodeNot aNode aNodeName {
    if (not(equalsString(slotNodeName(aNode), aNodeName))) {
        return aNode;
    };

    forEach(aChild in children(aNode)) {
        var nextNode = nextMatchingNodeNot(aChild, aNodeName);

        if(isNotNull(nextNode)) {
            return nextNode;
        };
    };
    return null;
}


TreeFunction doThisOnChildren1
ALL {
    forEach(aChild in children(thisGrammarNode)) {
        thisTreeFunction([aChild], thisResultNode);
    };
}

TreeFunction doThisOnChildren1_OneParam
ALL aParam {
    forEach(aChild in children(thisGrammarNode)) {
        thisTreeFunction([aChild], thisResultNode, [aParam]);
    };
}

''');
return htmlPreGeneratedLibFileStrings;
}
