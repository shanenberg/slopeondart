import '../runtime/SLOTRuntime.dart';
import '../runtime/objects/SLOTTreeNode.dart';
import '../structure/SLOTProgram.dart';
import 'dart:collection';

/*
  This class will somewhere in the near future .....
 */

abstract class SLOTLibraryLoader {

  List<String> aboutToLoadAndInitLibaries = new List<String>();

  List<String> libraryNames = new List<String>();

  HashMap<String, SLOTProgram> loadedLibraries
    = new HashMap<String, SLOTProgram>();

  HashMap<String, SLOTRuntime> initializedLibraries
  = new HashMap<String, SLOTRuntime>();

  loadLibraries(List<String> libNames, SLOTTreeNode inputTree) {
    this.libraryNames = libNames;
  }

  loadAndInitLibraryNamed(String libName, SLOTRuntime runtime);

  void loadAllLibraries();

}