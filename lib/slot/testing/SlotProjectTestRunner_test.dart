import '../runtime/SLOTRuntime.dart';
import 'dart:io';

import 'package:test/test.dart';

import '../../lib/dynamicParser/DynamicParser.dart';
import '../../lib/grammarparser/GrammarParser.dart';
import '../../lib/grammarparser/tree/GrammarObject.dart';
import '../../lib/parser/ResultObject.dart';
import '../../lib/parser/tree/TreeNode.dart';
import '../parser/SLOTParser.dart';
import '../runtime/SLOTRuntimeResult.dart';
import '../structure/SLOTProgram.dart';

void testSlotProject(String fileName, String slotProjectString) {

  SLProject project = SLProject.fromString(slotProjectString);
//  print(fileName);
  project.testProject(fileName);
}

void main() {
//  print("############### TEST");
  for (File f in allSLPFiles()) {
      testSlotProject(f.path.split("\\").last, f.readAsStringSync());
  };
//  print("############### DONE");
}

class SLProject {
  String slopeString;
  String slotString;
  List<SLPTest> tests;

  SLOTProgram slotProgram;
  SLOPEGrammarObject slopeGrammarObject;

  static SLProject fromString(String inputString) {
    SLProject project = new SLProject();
    List<String> tests = inputString.split("###");

    project.slopeString = tests[1].replaceFirst(" SLOPE", "");
    project.slotString = tests[2].replaceFirst(" SLOT", "");
    String testsString = tests[3].replaceFirst(" TESTS", "");
    project.tests = SLPTest.fromLines(testsString);
    return project;
  }

  void testProject(String fileName) {
//  print(fileName);

    ResultObject<SLOPEGrammarObject> result1 = new ResultObject<SLOPEGrammarObject>();
    var slopeParser = new GrammarParser();
    slopeParser.parseString(this.slopeString, result1);
    slopeGrammarObject = result1.getResult();

    ResultObject<SLOTProgram> result2 = new ResultObject<SLOTProgram>();
    var slotParser = new SLOTParser();
    slotParser.parseString(this.slotString, result2);
    slotProgram = result2.getResult();


    for(SLPTest aSLPTest in this.tests) {
      aSLPTest.runTests(fileName, this);
    }

  }


}

class SLPTest {
  String testname;
  String input;
  String result;
  String resultTree;

  static List<SLPTest> fromLines(String tests) {

    List<SLPTest> ret = new List<SLPTest>();

    List<String> list = tests.split("Test: ");
    list.removeAt(0);

    for(String listString in list) {
      List<String> lines = listString.split('\n');
      SLPTest t = new SLPTest();
      t.testname = lines[0].replaceAll("Test: ", "");
      t.input = lines[1].replaceAll("-Input: ", "");
      t.input = t.input.substring(0,t.input.length-1); // remove last linebreak

      t.result = lines[2].replaceAll("-Result: ", "");
      t.result = t.result.substring(0,t.result.length-1); // remove last linebreak

      t.resultTree = lines[3].replaceAll("-ResultTree: ", "");
      t.resultTree = t.resultTree.substring(0,t.resultTree.length-1); // remove last linebreak

      ret.add(t);
    }
    return ret;
  }

  void runTests(String fileName, SLProject project) {
    test("SLOT-Project - Tests - " + fileName, () {
      TreeNode node = this.createParseNodeFromInput(fileName, project);
      project.slotProgram.runtime = new SLOTRuntime();

      SLOTRuntimeResult slotResult =
        project.slotProgram.runProgram(node);
      expect(slotResult.result.valueToString(), this.result);
      expect(slotResult.resultTree.debug_SLOTTreeString(), this.resultTree);
    });
  }

  TreeNode createParseNodeFromInput(String fileName, SLProject project) {

      DynamicParser dynamicParser = new DynamicParser(project.slopeGrammarObject);
//      SlopeGrammar dynamicGrammarParser = new DynamicParser(grammar);
      TreeNode sourceTreeNode = dynamicParser.parseString(this.input, null);
      return sourceTreeNode;
//      dynamicParser.Start(new ResultObject());

//      dynamicParser.tokenStream = dynamicParser.scanString(this.input);

//      Rule r = project.slopeGrammarObject.getStartRule().body.createParserConstructor(project.slopeGrammarObject);
//      r.parserName = project.slopeGrammarObject.startRuleName;
//      return dynamicParser.doParsing(r);

  }

}

/*
  Returns all .slp-files
 */
List<File> allSLPFiles() {
  Directory currentDir =
  new Directory(
      Directory.current.path + "\\lib\\slot\\testing\\resources");

  List<FileSystemEntity> l = currentDir.listSync(recursive: true, followLinks: false);

  // Get all .slogr files
  return
    l
        .map((FileSystemEntity e) {return new File(e.path);}).toList()
        .where((File file) {
          return file.existsSync() && file.path.endsWith(".slp");}).toList();
}
