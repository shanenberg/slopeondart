import 'SLOTObject.dart';
import 'objects/SLOTSLOPETreeNode.dart';
import 'objects/SLOTTreeNode.dart';

class SLOTRuntimeResult {

  SLOTObject result;
  SLOTTreeNode resultTree;
  SLOTSLOPETreeNode slopeTree;

}
