import '../../lib/grammarparser/tree/GrammarObject.dart';
import '../structure/SLOTProgram.dart';
import 'SLOTRuntime.dart';
import 'SLOTRuntimeResult.dart';
import 'objects/SLOTSLOPETreeNode.dart';
import 'objects/SLOTString.dart';
import 'objects/SLOTTreeNode.dart';
import 'SLOTObject.dart';
import 'objects/slotUnwrapping.dart';

class SLOTProject {
  SLOTProgram slotProgram;
  SLOPEGrammarObject slopeGrammarObject;
  String inputString;

  SLOTProject(this.slotProgram, this.slopeGrammarObject, this.inputString);

  initProject() {
    slotProgram.runtime = new SLOTRuntime();
    slotProgram.runtime.project = this;
    slotProgram.initStuff(slopeGrammarObject.parseString(inputString));
    //slotProgram.runtime.slopeTreeNodes.add(slopeTreeNode.toSLOTTreeNode());
  }

  runFunctionUnwrap(String functionName, [List<SLOTObject> parameters]) {
    SLOTRuntimeResult slotResult = slotProgram.runFunction(functionName, parameters);
    return unwrapSLOTObject(slotResult.result);
  }

  runFunctionWrapUnwrap(String functionName, List<Object> parameters) {
    return runFunctionUnwrap(functionName,
        new List.generate(parameters.length,
                          (int index) => wrapSLOTObject(parameters[index])
                        )
    );
  }

  SLOTRuntimeResult runProject() {
    slotProgram.runtime = new SLOTRuntime();
    slotProgram.runtime.project = this;
    SLOTRuntimeResult slotResult = slotProgram.runProgram(slopeGrammarObject.parseString(inputString));
    return slotResult;
  }

  SLOTRuntimeResult doRedrawInProject() {
    SLOTSLOPETreeNode tn = slotProgram.runtime.slopeTreeNodes[0] as SLOTSLOPETreeNode;
    slotProgram.runtime.slopeTreeNodes = new List<SLOTTreeNode>();
    slotProgram.runtime.slopeTreeNodes.add(tn);
    print("RERUN");
    SLOTRuntimeResult slotResult = slotProgram.doRedrawInProgram();
    return slotResult;
  }

}
