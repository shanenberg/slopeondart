import '../../structure/SLOTBlock.dart';
import '../../structure/SLOTFunctionFamily.dart';
import '../../structure/expression/calls/SLOTExpression_FamilyFunctionCall.dart';
import '../SLOTRuntime.dart';
import '../objects/SLOTTreeNode.dart';
import 'SLOTChainedStackFrame.dart';
import 'SLOTStackFrame.dart';
import '../objects/SLOTString.dart';

class SLOTTreeFunctionStackFrame extends SLOTChainedStackFrame {

  bool isFunctionFrame() {
    return true;
  }

  SLOTTreeFunctionStackFrame(
      SLOTRuntime runtime,
      newFunctionList) : super(runtime.stack.first) {
    this.currentFunctionList = newFunctionList;
  }

  void initializeFamilyIndex(List<SLOTTreeNode> treeNodes) {
    //print("initFamilyIndex");
   // try {
      for (int i = 0; i < currentFunctionList.length; i++) {
        SLOTFunctionFamily family = currentFunctionList[i];
        //print(family.name);
        var fct = family.findMatchingFunction(this, treeNodes);
        if (fct != null) {
          currentFamilyIndex = i;
          currentSingleTreeFunction = fct;
          return;
        }
        i++;
      }
   // } catch(e) {

  //    this.printStackFrame();
 //     throw(e);
  //  }
      for(SLOTTreeNode node in treeNodes) {
        print((node.value as SLOTString).value);
      }
      throw "No matching function for slotNode: ";
  }


  void setContinueNewFamily(List<SLOTTreeNode> treeNodes) {
    for(int i=currentFamilyIndex; i<currentFunctionList.length; i++) {
      SLOTFunctionFamily family = currentFunctionList[i];
      var fct = family.findMatchingFunction(this, treeNodes);
      if (fct!=null) {
        currentFamilyIndex = i;
        currentSingleTreeFunction = fct;
        return;
      }
      i++;
    }
    throw "No matching function for slotNode: " ;
  }


  void pushOnRuntime(SLOTStackFrame stackFrame, SLOTExpression_FamilyFunctionCall call) {
//    this.setCurrentFamilyIndex(stackFrame.getCurrentSourceSLOTNode());

    stackFrame.getRuntime().stack.add(this);

    SLOTBlock block = new SLOTBlock();

  }

  SLOTTreeFunctionStackFrame getTreeFunctionFrame() {
    return this;
  }

  bool hasNextFamily() {
    return this.currentFamilyIndex +1 < this.currentFunctionList.length;
  }

  SLOTTreeFunctionStackFrame createNextTreeFunctionStackFrame() {
//    SLOTTreeFunctionStackFrame newFrame =
//        new SLOTTreeFunctionStackFrame(this.getRuntime(), this.thisFunctionList);
//    newFrame.currentFamilyIndex = this.currentFamilyIndex+1;
    throw "not implemented";
  }

  void setBlockIndex(SLOTStackFrame frame) {
//    this.setCurrentFamilyIndex(thisSourceNode);
    this.initializeFamilyIndex(currentSourceSLOTNodes);
  }

  void setContinueBlockIndex(SLOTStackFrame frame) {
    currentFamilyIndex = currentFamilyIndex + 1;
    try {
      this.setContinueNewFamily(currentSourceSLOTNodes);
    } catch (exception) {
      currentFamilyIndex = -1;
    }
  }

}