import '../../structure/SLOTSingleFunction.dart';
import '../SLOTObject.dart';
import '../SLOTRuntime.dart';
import 'SLOTRootStackFrame.dart';
import 'SLOTStackFrame.dart';
import '../../runtime/objects/SLOTLambdaFunction.dart';

class SLOTLambdaStackFrame extends SLOTRootStackFrame {

  SLOTLambdaFunction thisFunction;

  bool isFunctionFrame() {
    return true;
  }

  SLOTLambdaStackFrame(SLOTRuntime runtime, this.thisFunction) : super(runtime);

  // It is possible that a varName is a global variable
  SLOTObject getVariable(String varName) {

    if (this.variables.containsKey(varName)) {
      return this.variables[varName];
    }

    return thisFunction.context.getVariable(varName);
  }

  SLOTObject setVariable(String varName, SLOTObject value) {

    if (this.variables.containsKey(varName))
      return this.variables[varName]=value;

    if (thisFunction.context.hasVariable(varName)) {
      return thisFunction.context.setVariable(varName, value);
    }

    // unknown variable....so I define it here in my local context
    return this.variables[varName]=value;

  }

  bool hasVariable(String varName) {
    if (this.variables.containsKey(varName))
      return true;
    return context.hasVariable(varName);
  }
  

}