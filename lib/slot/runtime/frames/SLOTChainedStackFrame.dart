import '../SLOTObject.dart';
import '../SLOTRuntime.dart';
import 'SLOTRootStackFrame.dart';
import 'SLOTStackFrame.dart';
import 'SLOTTreeFunctionStackFrame.dart';

class SLOTChainedStackFrame extends SLOTStackFrame {
  SLOTStackFrame outerFrame;

  SLOTChainedStackFrame(this.outerFrame):super() {
    this.currentSourceSLOTNodes = outerFrame.currentSourceSLOTNodes;
    this.currentResultSLOTNode = outerFrame.currentResultSLOTNode;
    this.currentFunctionList = outerFrame.currentFunctionList;
    this.currentFamilyIndex = outerFrame.currentFamilyIndex;
    this.currentFunctionFamilyIndex = outerFrame.currentFunctionFamilyIndex;
  }

  bool hasVariable(String varName) {
    if (this.variables.containsKey(varName))
      return true;
    else
      return outerFrame.hasVariable(varName);
  }

  SLOTObject getVariable(String varName) {
    if (this.variables.containsKey(varName))
      return this.variables[varName];
    else {
      try {
        return outerFrame.getVariable(varName);
      } catch(e) {
        printStackFrame();
        throw e;
      }
    }
  }

  SLOTObject setVariable(String varName, SLOTObject value) {
    if (variables.containsKey(varName)) {
      this.variables[varName] = value;
      return value;
    } else if (outerFrame.hasVariable(varName)) {
      return outerFrame.setVariable(varName, value);
    } else {
      this.variables[varName] = value;
      return value;
    }
  }


  @override
  SLOTRuntime getRuntime() {
    SLOTStackFrame next = outerFrame;
    while(!(next is SLOTRootStackFrame)) {
      next = (next as SLOTChainedStackFrame).outerFrame;
    }
    return (next as SLOTRootStackFrame).getRuntime();
  }
  @override
  SLOTTreeFunctionStackFrame getCurrentFunctionFrame() {
    SLOTStackFrame f = outerFrame;
    while (f!=null) {
      if(f is SLOTChainedStackFrame) f = (f as SLOTChainedStackFrame).outerFrame;
      else f=null;
    }
    return f;
  }

}