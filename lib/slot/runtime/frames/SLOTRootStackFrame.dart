import '../SLOTObject.dart';
import '../SLOTRuntime.dart';
import 'SLOTStackFrame.dart';
import 'SLOTTreeFunctionStackFrame.dart';

class SLOTRootStackFrame extends SLOTStackFrame {
  SLOTRuntime runtime;

  SLOTRootStackFrame(this.runtime) {
    this.currentSourceSLOTNodes = runtime.slopeTreeNodes;
    this.currentResultSLOTNode= runtime.slotResultTree;
  }

  nextStep() {
    peekExpression().nextStep(this);
  }

  SLOTObject getVariable(String varName) {
    if (this.variables.containsKey(varName))
      return this.variables[varName];
    printStackFrame();
    throw "Unknown variable " + varName;
  }

  SLOTObject setVariable(String varName, SLOTObject value) {

    if (this.variables.containsKey(varName))
      return this.variables[varName]=value;

    printStackFrame();
    throw "Unknown variable " + varName;
  }

  bool hasVariable(String varName) {
    if (this.variables.containsKey(varName))
      return true;
    return false;
  }


  @override
  SLOTRuntime getRuntime() {
    return runtime;
  }
  @override
  SLOTTreeFunctionStackFrame getTreeFunctionFrame() {
    return null;
  }
}