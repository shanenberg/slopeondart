import '../../structure/expression/SLOTExpression.dart';
import '../SLOTObject.dart';
import '../SLOTRuntime.dart';
import 'SLOTChainedStackFrame.dart';
import 'SLOTRootStackFrame.dart';
import 'SLOTStackFrame.dart';
import 'SLOTTreeFunctionStackFrame.dart';

class SLOTPathExpressionExecutionStackFrame extends SLOTChainedStackFrame {

  SLOTPathExpressionExecutionStackFrame(SLOTStackFrame outerFrame) : super(outerFrame);

  nextStep() {
    peekExpression().nextStep(this);
  }

}