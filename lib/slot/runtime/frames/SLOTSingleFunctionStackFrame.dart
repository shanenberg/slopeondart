import '../../structure/SLOTSingleFunction.dart';
import '../SLOTObject.dart';
import '../SLOTRuntime.dart';
import 'SLOTRootStackFrame.dart';

class SLOTSingleFunctionStackFrame extends SLOTRootStackFrame {

  SLOTSingleFunction thisFunction;

  bool isFunctionFrame() {
    return true;
  }

  SLOTSingleFunctionStackFrame(SLOTRuntime runtime, this.thisFunction) : super(runtime);

  // It is possible that a varName is a global variable
  SLOTObject getVariable(String varName) {
    if (this.variables.containsKey(varName)) {
      return this.variables[varName];
    }

    if (!runtime.stack.first.variables.containsKey(varName)) {
      throw "Unknown variable: " + varName;
    } else {
      return runtime.stack.first.variables[varName];
    }
  }

  SLOTObject setVariable(String varName, SLOTObject value) {

    if (this.variables.containsKey(varName))
      return this.variables[varName]=value;

    if (runtime.stack.first.variables.containsKey(varName)) {
      return runtime.stack.first.variables[varName]=value;
    }

    return this.variables[varName]=value;

  }



  bool hasVariable(String varName) {
    if (this.variables.containsKey(varName))
      return true;
    return runtime.stack.first.variables.containsKey(varName);
  }
  
  

}