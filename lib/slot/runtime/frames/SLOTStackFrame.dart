import 'dart:collection';

import '../../structure/SLOTFunctionFamily.dart';
import '../../structure/SLOTSingleTreeFunction.dart';
import '../../structure/expression/SLOTExpression.dart';
import '../SLOTObject.dart';
import '../SLOTRuntime.dart';
import '../objects/SLOTTreeNode.dart';
import 'SLOTTreeFunctionStackFrame.dart';

/**
 * A SLOTStackFrame is pushed on the stack and
 * ....
 */
abstract class SLOTStackFrame {

  // The variables that are accessible within this stack frame
  HashMap<String, SLOTObject> variables = new HashMap<String, SLOTObject>();

  List<SLOTExpression> _expressions = new List<SLOTExpression>();
  List<SLOTObject> _data = new List<SLOTObject>();




  void pushExpression(SLOTExpression ex) {
    _expressions.add(ex);
  }

  void expressionsClear() {
    _expressions.clear();
  }

  SLOTExpression popExpression() {
    return _expressions.removeLast();
  }

  SLOTExpression peekExpression() {
    return _expressions.last;
  }

  expressionLength() {
    return _expressions.length;
  }

  expressionAt(int index) {
    return _expressions[index];
  }


  void pushData(SLOTObject o) {
    if (o==null) {
      throw "HEILIGE SCHEISSE!!!....hier versucht jemand, den leeren Stack zu reduzieren!!!";
    }

    _data.add(o);
  }

  int dataLength() {
    return _data.length;
  }

  SLOTObject dataAt(int index) {
    return _data[index];
  }

  SLOTObject peekData() {
    return _data.last;
  }

  SLOTObject popData() {
    if(_data.length==0) {
      throw "you try to pop data from the data stack....but there is no data";
    }


    return _data.removeLast();
  }

  void dataClear() {
    _data.clear();
  }


  List<SLOTTreeNode> currentSourceSLOTNodes;
  SLOTTreeNode currentResultSLOTNode;

  List<SLOTFunctionFamily> currentFunctionList;
  int currentFamilyIndex = -1;
  int currentFunctionFamilyIndex = -1;
  SLOTSingleTreeFunction currentSingleTreeFunction;


  nextStep() {
    peekExpression().nextStep(this);
  }

  SLOTRuntime getRuntime();

  // The distinction between functionFrame and nonFunctionFrame
  // becomes important when speaking about returns
  bool isFunctionFrame() {
    return false;
  }

  SLOTStackFrame();

  SLOTTreeNode addToResultNode(SLOTTreeNode aTreeNode) {
    aTreeNode.parent = this.currentResultSLOTNode;
    currentResultSLOTNode.children.elements.add(aTreeNode);
    return aTreeNode;
  }

  SLOTTreeFunctionStackFrame getSLOTFunctionFamilyFrame() {
    return null;
  }

  String _debug_variablesAsString() {
    String ds=" {variables: ";
    for(String variableName in variables.keys) {
      ds = ds + " " + variableName + "="+ variables[variableName].valueToString() + " ";
    }
    return ds + "}";
  }

  String _debug_dataAsString() {

    String ds= "{data length: " + this._data.length.toString() + " data: ";
    for(SLOTObject d in _data) {
      ds = ds + " " + d.valueToString() + " ";
    }
    return ds + "}";
  }

  void printStackFrame() {

    print(this.toString() + _debug_variablesAsString() + " " + _debug_dataAsString());


    for(SLOTExpression ex  in this._expressions) {
      print(" " + ex.toStackString());
    }
  }

  bool hasVariable(String varName);

  SLOTObject setVariable(String varName, SLOTObject val);

  SLOTObject defineLocalVariable(String varName, SLOTObject val) {
    this.variables[varName] = val;
  }

  SLOTObject getVariable(String varName);

/*
  void printStuff() {
    this.cu
  }*/
}