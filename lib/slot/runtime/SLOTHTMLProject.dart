import '../lib/SLOTLibraryHTMLLoader.dart';
import 'SLOTObject.dart';
import 'SLOTProject.dart';
import 'SLOTRuntime.dart';
import 'SLOTRuntimeResult.dart';
import 'dart:html';
import 'objects/SLOTHTMLElement.dart';
import 'objects/SLOTInteger.dart';
import 'objects/SLOTNull.dart';
import 'objects/SLOTSLOPETreeNode.dart';
import 'objects/SLOTString.dart';
import 'objects/SLOTTreeNode.dart';
import 'objects/SLOTHTMLKeyboardEvent.dart';

import 'builtInFunction/htmlBuiltInFunction.dart';
import 'package:clippy/browser.dart';
import '../../lib/parser/tree/TreeNode.dart';
import 'objects/slotUnwrapping.dart';


/**
 * TERRIBLE CODE.....BUT NEEDED: see https://stackoverflow.com/questions/31264654/appendhtml-doesnt-append-the-full-html-dart/31266045#31266045
 */
class MySanitizer implements NodeTreeSanitizer {
  void sanitizeTree(Node node) {}
}

class SLOTHTMLProject extends SLOTProject {
  Element htmlElement;
  SLOTHTMLProject(slotProgram, slopeGrammarObject, inputString, [this.htmlElement = null]):super(slotProgram, slopeGrammarObject, inputString);



  initProject() {
    slotProgram.runtime = new SLOTRuntime(new SLOTLibraryHTMLLoader());
    loadHTMLBuiltInFunctions(slotProgram.runtime);
    slotProgram.runtime.project = this;
    slotProgram.loadLibraries();
    slotProgram.initStuff(slopeGrammarObject.parseString(inputString));
    //slotProgram.runtime.slopeTreeNodes.add(slopeTreeNode.toSLOTTreeNode());
  }




  SLOTRuntimeResult runProgramFunction(String functionName, [List<SLOTObject> parameters]) {

    SLOTRuntimeResult slotResult = slotProgram.runFunction(functionName, parameters);

    return slotResult;
  }

  SLOTRuntimeResult runProjectWithParam(String input) {
    slotProgram.runtime = new SLOTRuntime(new SLOTLibraryHTMLLoader());
    loadHTMLBuiltInFunctions(slotProgram.runtime);
    slotProgram.runtime.project = this;

    SLOTRuntimeResult slotResult = slotProgram.runProgram(slopeGrammarObject.parseString(input));

    document.body.setInnerHtml(slotResult.result.valueToString(), treeSanitizer:new MySanitizer()); // = slotResult.result.valueToString();

    document.onClick.listen(
            (MouseEvent event) {
          if ((event.target as Element).id!="") {
            SLOTTreeNode n = this.mouseEventToSLOTTreeNode(event);
            SLOTInteger id = new SLOTInteger(int.parse((event.target as Element).id.toString()));
            SLOTHTMLElement htmlObject = new SLOTHTMLElement(event.target as Element);
            this.slotProgram.runtime.runFunction("handleEvent", [n, id, htmlObject]);
          }

        }
    );

    onPaste.listen((String content) {

      this.slotProgram.runtime.runFunction("onPaste", [new SLOTString(content)]);

    });
/*
    document.onKeyPress.listen(
            (KeyboardEvent event) {

                SLOTTreeNode n = this.keyboardEventToSLOTTreeNode(event);
                SLOTObject id = SLOTNull.instance;
                SLOTHTMLElement htmlObject = new SLOTHTMLElement(event.target as Element);
                this.slotProgram.runtime.runFunction("handleEvent", [n, id, htmlObject]);
        }
    );*/

    document.onKeyDown.listen(
            (KeyboardEvent event) {
          /*SLOTTreeNode n = this.keyboardEventToSLOTTreeNode(event);*/
          SLOTHTMLKeyboardEvent n = new SLOTHTMLKeyboardEvent(event);
          this.slotProgram.runtime.runFunction("handleKeyboardEvent", [n]);
        }
    );
/*
    document.onKeyDown.listen(
            (KeyboardEvent event) {
          /*SLOTTreeNode n = this.keyboardEventToSLOTTreeNode(event);*/
          SLOTHTMLKeyboardEvent n = new SLOTHTMLKeyboardEvent(event);
          this.slotProgram.runtime.runFunction("handleKeyboardEvent", [n]);
        }
    );*/


    return slotResult;
  }


  SLOTRuntimeResult runProject() {
    slotProgram.runtime = new SLOTRuntime(new SLOTLibraryHTMLLoader());
    loadHTMLBuiltInFunctions(slotProgram.runtime);
    slotProgram.runtime.project = this;

    SLOTRuntimeResult slotResult = slotProgram.runProgram(slopeGrammarObject.parseString(inputString));

    document.body.setInnerHtml(slotResult.result.valueToString(), treeSanitizer:new MySanitizer()); // = slotResult.result.valueToString();

    document.onClick.listen(
            (MouseEvent event) {
          if ((event.target as Element).id!="") {
          SLOTTreeNode n = this.mouseEventToSLOTTreeNode(event);
          SLOTInteger id = new SLOTInteger(int.parse((event.target as Element).id.toString()));
          SLOTHTMLElement htmlObject = new SLOTHTMLElement(event.target as Element);
          this.slotProgram.runtime.runFunction("handleEvent", [n, id, htmlObject]);
          }

        }
    );

    onPaste.listen((String content) {

        this.slotProgram.runtime.runFunction("onPaste", [new SLOTString(content)]);

    });
/*
    document.onKeyPress.listen(
            (KeyboardEvent event) {

                SLOTTreeNode n = this.keyboardEventToSLOTTreeNode(event);
                SLOTObject id = SLOTNull.instance;
                SLOTHTMLElement htmlObject = new SLOTHTMLElement(event.target as Element);
                this.slotProgram.runtime.runFunction("handleEvent", [n, id, htmlObject]);
        }
    );*/

    document.onKeyDown.listen(
            (KeyboardEvent event) {
          /*SLOTTreeNode n = this.keyboardEventToSLOTTreeNode(event);*/
          SLOTHTMLKeyboardEvent n = new SLOTHTMLKeyboardEvent(event);
          this.slotProgram.runtime.runFunction("handleKeyboardEvent", [n]);
        }
    );
/*
    document.onKeyDown.listen(
            (KeyboardEvent event) {
          /*SLOTTreeNode n = this.keyboardEventToSLOTTreeNode(event);*/
          SLOTHTMLKeyboardEvent n = new SLOTHTMLKeyboardEvent(event);
          this.slotProgram.runtime.runFunction("handleKeyboardEvent", [n]);
        }
    );*/


    return slotResult;
  }

  SLOTRuntimeResult doRedrawInProject() {
    SLOTSLOPETreeNode tn = slotProgram.runtime.slopeTreeNodes[0] as SLOTSLOPETreeNode;
    slotProgram.runtime.slopeTreeNodes = new List<SLOTTreeNode>();
    slotProgram.runtime.slopeTreeNodes.add(tn);

    SLOTRuntimeResult slotResult = slotProgram.doRedrawInProgram();
    if(htmlElement == null)
    document.body.setInnerHtml(slotResult.result.valueToString(), treeSanitizer:new MySanitizer()); // = slotResult.result.valueToString();
    else
      htmlElement.setInnerHtml(slotResult.result.valueToString(), treeSanitizer: new MySanitizer());

    return slotResult;
  }


  SLOTTreeNode mouseEventToSLOTTreeNode(MouseEvent m) {
    SLOTTreeNode ret = new SLOTTreeNode();
    ret.value = new SLOTString("MOUSEEVENT");

    return ret;
  }


  SLOTTreeNode keyboardEventToSLOTTreeNode(KeyboardEvent m) {
    SLOTTreeNode ret = new SLOTTreeNode();
    ret.value = new SLOTString("KEYBOARDEVENT");

    SLOTTreeNode next = new SLOTTreeNode();
    next.value = new SLOTString("KEY");
    ret.addChild(next);

    SLOTTreeNode next2 = new SLOTTreeNode();
    next2.value = new SLOTString(m.key);

    next.addChild(next2);


    return ret;
  }

}
