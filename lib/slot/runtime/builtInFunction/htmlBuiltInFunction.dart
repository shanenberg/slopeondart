import '../SLOTRuntime.dart';
import 'html/HtmlAtt.dart';
import 'html/WriteToClipboard.dart';
import 'package:english_words/english_words.dart';
import '../objects/SLOTString.dart';
import 'package:slopeondart/slot/runtime/builtInFunction/ANY/EnglishWords.dart';
import 'package:slopeondart/slot/runtime/builtInFunction/ANY/EnglishAdjectives.dart';
import 'package:slopeondart/slot/runtime/builtInFunction/ANY/EnglishNouns.dart';


loadHTMLBuiltInFunctions(SLOTRuntime runtime) {
  runtime.builtInFunctions["htmlAtt"] = htmlAttFct;
  runtime.builtInFunctions["writeToClipboard"] = writeToClipboardFct;

}