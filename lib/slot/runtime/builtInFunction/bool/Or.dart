import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTBoolean.dart';
import '../BuiltInFunction.dart';

BuiltInFunction orFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  /*frame.pushData((parameters[0] as SLOTBoolean).or(parameters[1] as SLOTBoolean));*/

  for(SLOTBoolean param in parameters) {
    if(param.boolValue()) {
      frame.pushData(SLOTBoolean.fromBool(true));
      return;
    }
  }

  frame.pushData(SLOTBoolean.fromBool(false));


};

