import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTBoolean.dart';
import '../BuiltInFunction.dart';

BuiltInFunction notFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  frame.pushData((parameters[0] as SLOTBoolean).not());
};

