import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTBoolean.dart';
import '../BuiltInFunction.dart';

BuiltInFunction andFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  /*frame.pushData((parameters[0] as SLOTBoolean).and(parameters[1] as SLOTBoolean));*/

  for(SLOTBoolean param in parameters) {
    if(!param.boolValue()) {
      frame.pushData(SLOTBoolean.fromBool(false));
      return;
    }
  }
  frame.pushData(SLOTBoolean.fromBool(true));
};

