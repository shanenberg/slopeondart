import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTBoolean.dart';
import '../../objects/SLOTList.dart';
import '../../../../lib/parser/features/scannerelement/ScannerElement_TreeNode.dart';
import '../../../../lib/parser/features/optional/OPTIONAL_TreeNode.dart';


/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction insertOPTChildFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  return insertOPTChildFct_DEBUG(frame, parameters);
};

insertOPTChildFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  SLOTSLOPETreeNode treeNode = parameters[0];
  OPTIONAL_TreeNode slopeNode = treeNode.slopeTreeNode;

  SLOTSLOPETreeNode child = parameters[1];
  TreeNode slopeChild = child.slopeTreeNode;

  slopeNode.child = slopeChild;

  /*if(treeNode.children.elements.length == 0) {
    treeNode.addChild(child);
  } else {
    treeNode.setChildAt(0, child);
  }*/
  treeNode.setChildAt(0, child);

  frame.pushData(SLOTNull.instance);

}