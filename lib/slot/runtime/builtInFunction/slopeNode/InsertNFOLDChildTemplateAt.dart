import '../../../../lib/parser/tree/TreeNode.dart';

import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../../../slot/runtime/objects/SLOTInteger.dart';
import '../../../../slot/runtime/objects/SLOTNull.dart';
import '../../../../lib/parser/features/bracket/BRACKET_TreeNode.dart';



BuiltInFunction insertNFOLDChildTemplateAtFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  return insertNFOLDChildTemplateAtFct_DEBUG(frame, parameters);
};

insertNFOLDChildTemplateAtFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters]) {


  SLOTSLOPETreeNode current = parameters[0];

  if (current.slopeTreeNode is BRACKET_TreeNode) {
//    (current.slopeTreeNode as BRACKET_TreeNode)
  }


    NFOLD_TreeNode currentSlope = current.slopeTreeNode;



  int at = (parameters[1] as SLOTInteger).val-1;


  currentSlope.treeNodes.insert(at, currentSlope.createChildTemplate());

  frame.getRuntime().slopeTreeNodes[0] =
      (frame.getRuntime().slopeTreeNodes[0] as SLOTSLOPETreeNode).slopeTreeNode.toSLOTTreeNode();


  frame.pushData(SLOTNull.instance);

}