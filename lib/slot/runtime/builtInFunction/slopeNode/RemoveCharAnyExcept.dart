import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTBoolean.dart';
import '../../objects/SLOTList.dart';
import '../../../../lib/parser/features/scannerelement/ScannerElement_TreeNode.dart';

/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction removeCharAnyExceptFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  return removeCharAnyExceptFct_DEBUG(frame, parameters);
};

removeCharAnyExceptFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  SLOTSLOPETreeNode anyExcept = parameters[0];
  ScannerElement_TreeNode slope = anyExcept.slopeTreeNode;
  slope.scannedWord = "#";

  frame.getRuntime().slopeTreeNodes[0] =
      (frame.getRuntime().slopeTreeNodes[0] as SLOTSLOPETreeNode).slopeTreeNode.toSLOTTreeNode();

  frame.pushData(SLOTNull.instance);
}