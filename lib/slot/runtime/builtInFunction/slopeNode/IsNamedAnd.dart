import '../../../../lib/parser/features/or/OR_ParserConstructor.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/features/and/AND_TreeNode.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTBoolean.dart';

BuiltInFunction isNamedAndFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  return isNamedAndFct_DEBUG(frame, parameters);
};

isNamedAndFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters]) {

  if(!(parameters[0] is SLOTSLOPETreeNode)) {
    frame.pushData(SLOTBoolean.fromBool(false));
    return;
  }

  SLOTSLOPETreeNode current = parameters[0];

  TreeNode treeNode = current.slopeTreeNode;
  bool ret = treeNode is AND_TreeNode && treeNode.nodeName != null;


  frame.pushData(SLOTBoolean.fromBool(ret));

}