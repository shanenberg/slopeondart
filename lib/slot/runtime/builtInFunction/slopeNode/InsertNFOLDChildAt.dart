import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTBoolean.dart';
import '../../objects/SLOTList.dart';
import '../../../../lib/parser/features/scannerelement/ScannerElement_TreeNode.dart';


/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction insertNFOLDChildAtFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  return insertNFOLDChildAtFct_DEBUG(frame, parameters);
};

insertNFOLDChildAtFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  SLOTSLOPETreeNode current = parameters[0];



  NFOLD_TreeNode currentSlope = current.slopeTreeNode;


  SLOTSLOPETreeNode child = parameters[1];
  TreeNode childSlope = child.slopeTreeNode;

  int at = (parameters[2] as SLOTInteger).val-1;


  //currentSlope.treeNodes.insert(at, currentSlope.createChildTemplate());

  currentSlope.treeNodes.insert(at, childSlope);

  current.insertChildAt(at, child);
  /*
  frame.getRuntime().slopeTreeNodes[0] =
      (frame.getRuntime().slopeTreeNodes[0] as SLOTSLOPETreeNode).slopeTreeNode.toSLOTTreeNode();
*/

  frame.pushData(SLOTNull.instance);
}