import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTBoolean.dart';
import '../../../../lib/parser/features/optional/OPTIONAL_TreeNode.dart';

BuiltInFunction hasOPTChildFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  return hasOPTChildFct_DEBUG(frame, parameters);
};

hasOPTChildFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters]) {

  SLOTSLOPETreeNode current = parameters[0];

  TreeNode treeNode = current.slopeTreeNode;
  bool ret = treeNode is OPTIONAL_TreeNode
          && treeNode.child != null;


  frame.pushData(SLOTBoolean.fromBool(ret));

}