import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_ParserConstructor.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../../../lib/parser/features/scannerelement/ScannerElement_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../../../slot/runtime/objects/SLOTInteger.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';

/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction createSingleChildTemplateFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  return createSingleChildTemplateFct_DEBUG(frame, parameters);
};

createSingleChildTemplateFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters]) {

  SLOTSLOPETreeNode current = parameters[0];
  SingleChildTreeNode currentSlope = current.slopeTreeNode;


  TreeNode templateSlope = currentSlope.createChildTemplate();

  SLOTSLOPETreeNode template = templateSlope.toSLOTTreeNode();
  //  frame.getRuntime().slopeTreeNodes[0] =
  //  (frame.getRuntime().slopeTreeNodes[0] as SLOTSLOPETreeNode).slopeTreeNode.toSLOTTreeNode();

  frame.pushData(template);

}
