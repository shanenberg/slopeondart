import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/features/optional/OPTIONAL_TreeNode.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';

/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction deleteNodeFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  return deleteNodeFct_DEBUG(frame, parameters);
};

deleteNodeFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters]) {

  SLOTSLOPETreeNode current = parameters[0];

  if(current.slopeTreeNode is OR_TreeNode) {
    OR_TreeNode orTreeNode = current.slopeTreeNode;
    orTreeNode.child = new NULL_TreeNode();
    orTreeNode.orIndex = -1;

    (current.children.elements[1] as SLOTTreeNode)
      .setChildAt(0, new SLOTTreeNode()..value=new SLOTInteger(0));

    current.setChildAt(0, orTreeNode.child.toSLOTTreeNode());

  } else if (current.slopeTreeNode is OPTIONAL_TreeNode){
    OPTIONAL_TreeNode opt = current.slopeTreeNode;
    opt.child = null;

    current.setChildAt(0, new SLOTTreeNode()..value = new SLOTString("NULL"));
  }
 /* frame.getRuntime().slopeTreeNodes[0] =
      (frame.getRuntime().slopeTreeNodes[0] as SLOTSLOPETreeNode).slopeTreeNode.toSLOTTreeNode();
*/
  frame.pushData(SLOTNull.instance);

}