import '../../../../lib/parser/features/or/OR_ParserConstructor.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';

BuiltInFunction switchToOrFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  return switchToOrFct_DEBUG(frame, parameters);
};

switchToOrFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters]) {

  SLOTSLOPETreeNode current = parameters[0];
  OR_TreeNode orTreeNode = current.slopeTreeNode;

  SLOTInteger b = parameters[1];
  int paramIndex = b.val;

  /*
  int currentIndex = orTreeNode.orIndex;

  if (currentIndex+1<orTreeNode.numberOfAlternatives()) {
    currentIndex++;
  } else {
    currentIndex = 0;
  }
*/
  orTreeNode.child =
      (orTreeNode.unRuledParserConstructor() as OR_ParserConstructor).createTemplateAt(paramIndex);
  orTreeNode.orIndex = paramIndex;

  frame.getRuntime().slopeTreeNodes[0] =
      (frame.getRuntime().slopeTreeNodes[0] as SLOTSLOPETreeNode).slopeTreeNode.toSLOTTreeNode();

  frame.pushData(SLOTNull.instance);

}