import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTBoolean.dart';
import '../../objects/SLOTList.dart';
import '../../../../lib/parser/features/scannerelement/ScannerElement_TreeNode.dart';
import '../../objects/SLOTFalse.dart';
import '../../objects/SLOTTrue.dart';


/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction canSetCharAnyExceptFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  return canSetCharAnyExceptFct_DEBUG(frame, parameters);
};

canSetCharAnyExceptFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  SLOTSLOPETreeNode treeNode = parameters[0];
  ScannerElement_TreeNode slopeTreeNode = treeNode.slopeTreeNode;

  String inputValue = (parameters[1] as SLOTString).value;

  for(SLOTTreeNode child in (treeNode.children.elements[1] as SLOTTreeNode).children.elements) {
    String value = (child.value as SLOTString).value;
    if(value == inputValue || inputValue.length>1) {
      //throw "$value == $inputValue AnyExcept Problem";
      frame.pushData(SLOTFalse.instance);
      return;
    }
  }

  frame.pushData(SLOTTrue.instance);
}