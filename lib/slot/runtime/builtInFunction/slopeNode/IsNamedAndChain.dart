import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTBoolean.dart';
import '../../objects/SLOTList.dart';
import '../../../../lib/parser/features/scannerelement/ScannerElement_TreeNode.dart';
import '../../../../lib/parser/features/and/AND_TreeNode.dart';


/**
 * param: treeNode, name, [name, ...]
 *
 */

BuiltInFunction isNamedAndChainFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  return isNamedAndChainFct_DEBUG(frame, parameters);
};

isNamedAndChainFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  if(!(parameters[0] is SLOTSLOPETreeNode)) {
    frame.pushData(SLOTBoolean.fromBool(false));
    return;
  }



  SLOTSLOPETreeNode deepestChild = parameters[0];
  String deepestChildName = (parameters[parameters.length-1] as SLOTString).value;

  if(deepestChild.slopeTreeNode.nodeName != deepestChildName) {
    frame.pushData(SLOTBoolean.fromBool(false));
    return;
  }

  int currentIndex = parameters.length-2;




  for(SLOTSLOPETreeNode currentNode = deepestChild.parent;
      currentNode != null && currentIndex > 0;
      currentNode = currentNode.parent)
  {
        if((  currentNode.slopeTreeNode is AND_TreeNode
            || currentNode.slopeTreeNode is ScannerElement_TreeNode
           )
        && currentNode.slopeTreeNode.nodeName != null) {
              String name = (parameters[currentIndex] as SLOTString).value;
              if(currentNode.slopeTreeNode.nodeName != name) {
                frame.pushData(SLOTBoolean.fromBool(false));
                return;
              }
              currentIndex--;
        }
  }
  if(currentIndex > 0) {
    frame.pushData(SLOTBoolean.fromBool(false));
    return;
  }
  else {
    frame.pushData(SLOTBoolean.fromBool(true));
    return;
  }
}