import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTBoolean.dart';
import '../../objects/SLOTList.dart';
import '../../../../lib/parser/features/scannerelement/ScannerElement_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_ParserConstructor.dart';


/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction insertORChildTemplateAtFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  return insertORChildTemplateAtFct_DEBUG(frame, parameters);
};

insertORChildTemplateAtFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  SLOTSLOPETreeNode treeNode = parameters[0];
  OR_TreeNode slopeNode = treeNode.slopeTreeNode;



  int at = (parameters[1] as SLOTInteger).val-1;

  TreeNode slopeChild = (slopeNode.unRuledParserConstructor() as OR_ParserConstructor).createTemplateAt(at);
  SLOTSLOPETreeNode child = slopeChild.toSLOTTreeNode();


  slopeNode.child = slopeChild;
  slopeNode.orIndex = at;

  /*if(treeNode.children.elements.length == 0) {
      treeNode.addChild(child);
    } else {
      treeNode.setChildAt(0, child);
    }*/

  treeNode.setChildAt(0, child);
  treeNode.setChildAt(1, new SLOTTreeNode()..value=new SLOTString("INDEX")
    ..addChild(new SLOTTreeNode()..value=new SLOTInteger(at+1)));

  frame.pushData(SLOTNull.instance);
}