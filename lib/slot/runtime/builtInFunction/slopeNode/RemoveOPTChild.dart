import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../objects/SLOTInteger.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';
import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/optional/OPTIONAL_TreeNode.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTTreeNode.dart';

/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction removeOPTChildFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  return removeOPTChildFct_DEBUG(frame, parameters);
};

removeOPTChildFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters]) {

  SLOTSLOPETreeNode current = (parameters[0] as SLOTSLOPETreeNode);

  OPTIONAL_TreeNode currentSlope = current.slopeTreeNode;

  currentSlope.child = null;


  current.setChildAt(0, new SLOTTreeNode()..value=new SLOTString("NULL"));

  //currentSlope.child = new NULL_TreeNode();

  /*frame.getRuntime().slopeTreeNodes[0] =
      (frame.getRuntime().slopeTreeNodes[0] as SLOTSLOPETreeNode).slopeTreeNode.toSLOTTreeNode();
*/
  frame.pushData(SLOTNull.instance);

}