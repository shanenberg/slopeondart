import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../objects/SLOTInteger.dart';

/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction removeNFOLDChildAtFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  return removeNFOLDChildAtFct_DEBUG(frame, parameters);
};

removeNFOLDChildAtFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters]) {

  SLOTSLOPETreeNode current = (parameters[0] as SLOTSLOPETreeNode);
  int index = (parameters[1] as SLOTInteger).val-1;


  NFOLD_TreeNode currentSlope = current.slopeTreeNode;

  currentSlope.treeNodes.removeAt(index);
  current.removeChildAt(index);
/*
  frame.getRuntime().slopeTreeNodes[0] =
      (frame.getRuntime().slopeTreeNodes[0] as SLOTSLOPETreeNode).slopeTreeNode.toSLOTTreeNode();
*/

  frame.pushData(SLOTNull.instance);

}