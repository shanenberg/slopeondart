import '../../../../lib/parser/features/or/OR_ParserConstructor.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';

BuiltInFunction switchToPreviousNodeFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  return switchToPreviousNodeFct_DEBUG(frame, parameters);
};

switchToPreviousNodeFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters]) {

  SLOTSLOPETreeNode current = parameters[0];
  OR_TreeNode orTreeNode = current.slopeTreeNode;

  int currentIndex = orTreeNode.orIndex;

  if (currentIndex-1>=0) {
    currentIndex--;
  } else {
    currentIndex = orTreeNode.numberOfAlternatives()-1;
  }

  orTreeNode.child =
      (orTreeNode.unRuledParserConstructor() as OR_ParserConstructor).createTemplateAt(currentIndex);
  orTreeNode.orIndex = currentIndex;

  frame.getRuntime().slopeTreeNodes[0] =
      (frame.getRuntime().slopeTreeNodes[0] as SLOTSLOPETreeNode).slopeTreeNode.toSLOTTreeNode();

  frame.pushData(SLOTNull.instance);

}