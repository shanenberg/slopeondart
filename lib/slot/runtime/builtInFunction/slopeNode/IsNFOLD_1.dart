import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTBoolean.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';

BuiltInFunction isNFOLD_1Fct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  return isNFOLD_1Fct_DEBUG(frame, parameters);
};

isNFOLD_1Fct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters]) {


  SLOTObject param = parameters[0];
  if(!(param is SLOTSLOPETreeNode)) {
    frame.pushData(SLOTBoolean.fromBool(false));
    return;
  }

  SLOTSLOPETreeNode current = parameters[0];

  TreeNode treeNode = current.slopeTreeNode;
  bool ret = treeNode is NFOLD_TreeNode
         && (treeNode.createTemplate() as NFOLD_TreeNode).treeNodes.length == 1;


  frame.pushData(SLOTBoolean.fromBool(ret));

}