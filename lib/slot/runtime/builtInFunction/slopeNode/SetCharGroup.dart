import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTBoolean.dart';
import '../../objects/SLOTList.dart';
import '../../../../lib/parser/features/scannerelement/ScannerElement_TreeNode.dart';


/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction setCharGroupFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  return setCharGroupFct_DEBUG(frame, parameters);
};

setCharGroupFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  SLOTSLOPETreeNode treeNode = parameters[0];
  ScannerElement_TreeNode slopeTreeNode = treeNode.slopeTreeNode;

  String inputValue = (parameters[1] as SLOTString).value;

  int unit = inputValue.codeUnitAt(0);

  int rangeBegin = ((treeNode.children.elements[1] as SLOTTreeNode)
                    .value as SLOTString)
                        .value
                        .codeUnitAt(0);

  int rangeEnd = ((treeNode.children.elements[2] as SLOTTreeNode)
                    .value as SLOTString)
                        .value
                        .codeUnitAt(0);

  if(inputValue.length > 1 ||
      unit < rangeBegin || unit > rangeEnd) {
    //throw "${new String.fromCharCode(rangeBegin)}  --- ${new String.fromCharCode(unit)} --- ${new String.fromCharCode(rangeEnd)}";
    frame.pushData(SLOTNull.instance);
    return;
  }

  slopeTreeNode.scannedWord = inputValue;

   treeNode.setChildAt(0, new SLOTTreeNode()
    ..value = new SLOTString("TOKEN")
    ..addChild(new SLOTTreeNode()
      ..value = new SLOTString(inputValue)));

/*
  frame.getRuntime().slopeTreeNodes[0] =
      (frame.getRuntime().slopeTreeNodes[0] as SLOTSLOPETreeNode).slopeTreeNode.toSLOTTreeNode();
*/
  frame.pushData(SLOTNull.instance);
}