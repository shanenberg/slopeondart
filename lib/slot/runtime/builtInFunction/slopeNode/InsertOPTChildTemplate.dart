import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_ParserConstructor.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../../../lib/parser/features/scannerelement/ScannerElement_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../../../slot/runtime/objects/SLOTInteger.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';

/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction insertOPTChildTemplateFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  return insertOPTChildTemplateFct_DEBUG(frame, parameters);
};

insertOPTChildTemplateFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters]) {

  SLOTSLOPETreeNode current = parameters[0];
  SingleChildTreeNode currentSlope = current.slopeTreeNode;


  TreeNode templateSlope = currentSlope.createChildTemplate();

  currentSlope.child =templateSlope;

  current.setChildAt(0, templateSlope.toSLOTTreeNode());

  /*
  currentSlope.child = templateSlope;

    frame.getRuntime().slopeTreeNodes[0] =
    (frame.getRuntime().slopeTreeNodes[0] as SLOTSLOPETreeNode).slopeTreeNode.toSLOTTreeNode();
*/
  frame.pushData(SLOTNull.instance);

}
