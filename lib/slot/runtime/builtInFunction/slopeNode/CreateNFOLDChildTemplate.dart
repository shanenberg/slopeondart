import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_ParserConstructor.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../../../lib/parser/features/scannerelement/ScannerElement_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';


BuiltInFunction createNFOLDChildTemplateFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  return createNFOLDChildTemplateFct_DEBUG(frame, parameters);
};

createNFOLDChildTemplateFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters]) {

  SLOTSLOPETreeNode current = parameters[0];
  TreeNode currentSlope = current.slopeTreeNode;

  TreeNode templateSlope = (currentSlope as NFOLD_TreeNode).createChildTemplate();

  SLOTSLOPETreeNode template = templateSlope.toSLOTTreeNode();
  //  frame.getRuntime().slopeTreeNodes[0] =
  //  (frame.getRuntime().slopeTreeNodes[0] as SLOTSLOPETreeNode).slopeTreeNode.toSLOTTreeNode();

  frame.pushData(template);

}
