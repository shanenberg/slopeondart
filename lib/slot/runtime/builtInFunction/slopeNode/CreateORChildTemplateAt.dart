import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_ParserConstructor.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../../../lib/parser/features/scannerelement/ScannerElement_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../../../slot/runtime/objects/SLOTInteger.dart';

/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction createORChildTemplateAtFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  return createORChildTemplateAtFct_DEBUG(frame, parameters);
};

createORChildTemplateAtFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters]) {

  SLOTSLOPETreeNode current = parameters[0];
  OR_TreeNode currentSlope = current.slopeTreeNode;

  int at = (parameters[1] as SLOTInteger).val-1;





  TreeNode templateSlope = (currentSlope.unRuledParserConstructor() as OR_ParserConstructor).createTemplateAt(at);

  SLOTSLOPETreeNode template = templateSlope.toSLOTTreeNode();
  //  frame.getRuntime().slopeTreeNodes[0] =
      //  (frame.getRuntime().slopeTreeNodes[0] as SLOTSLOPETreeNode).slopeTreeNode.toSLOTTreeNode();

  frame.pushData(template);

}
