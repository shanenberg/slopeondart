import '../../../../lib/parser/features/or/OR_ParserConstructor.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../../../lib/parser/features/scannerelement/ScannerElement_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';

BuiltInFunction numAlternativesFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  return numAlternativesFct_DEBUG(frame, parameters);
};

numAlternativesFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters]) {

  SLOTSLOPETreeNode current = parameters[0];
  OR_TreeNode orTreeNode = current.slopeTreeNode;
  frame.pushData(new SLOTInteger(orTreeNode.numberOfAlternatives()));

}