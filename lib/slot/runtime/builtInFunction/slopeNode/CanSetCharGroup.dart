import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTBoolean.dart';
import '../../objects/SLOTList.dart';
import '../../../../lib/parser/features/scannerelement/ScannerElement_TreeNode.dart';
import '../../objects/SLOTFalse.dart';
import '../../objects/SLOTTrue.dart';


/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction canSetCharGroupFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  return canSetCharGroupFct_DEBUG(frame, parameters);
};

canSetCharGroupFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  SLOTSLOPETreeNode treeNode = parameters[0];
  String inputValue = (parameters[1] as SLOTString).value;

  int unit = inputValue.codeUnitAt(0);

  int rangeBegin = ((treeNode.children.elements[1] as SLOTTreeNode)
      .value as SLOTString)
      .value
      .codeUnitAt(0);

  int rangeEnd = ((treeNode.children.elements[2] as SLOTTreeNode)
      .value as SLOTString)
      .value
      .codeUnitAt(0);

  if(unit < rangeBegin || unit > rangeEnd || inputValue.length > 1) {
    frame.pushData(SLOTFalse.instance);
    return;
  } else {
    frame.pushData(SLOTTrue.instance);
    return;
  }
}