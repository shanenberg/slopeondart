import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTBoolean.dart';
import '../../objects/SLOTList.dart';
import '../../../../lib/parser/features/scannerelement/ScannerElement_TreeNode.dart';


/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction clearNFOLD_0Fct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  return clearNFOLD_0Fct_DEBUG(frame, parameters);
};

clearNFOLD_0Fct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  SLOTSLOPETreeNode slotNFOLD = parameters[0];

  NFOLD_TreeNode slopeNFOLD = slotNFOLD.slopeTreeNode;
  if(slopeNFOLD.isN0()) {
    slopeNFOLD.treeNodes = [];
    slotNFOLD.children.elements = [];
  } else {
    throw "...";
  }

  frame.pushData(SLOTNull.instance);
}