import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTBoolean.dart';
import '../../objects/SLOTList.dart';
import '../../../../lib/parser/features/scannerelement/ScannerElement_TreeNode.dart';
import '../../../../lib/grammarparser/ScannerRuleObject.dart';



/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction isScannerRuleFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  return isScannerRuleFct_DEBUG(frame, parameters);
};

isScannerRuleFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  bool ret;



  if(!(parameters[0] is SLOTSLOPETreeNode)) {
    ret = false;
    frame.pushData(SLOTBoolean.fromBool(ret));
    return;
  }

  SLOTSLOPETreeNode slotParseNode = (parameters[0] as SLOTSLOPETreeNode);

  if(!(slotParseNode.slopeTreeNode is ScannerElement_TreeNode)) {
    ret = false;
    frame.pushData(SLOTBoolean.fromBool(ret));
    return;
  }


  ScannerElement_TreeNode parseNode = slotParseNode.slopeTreeNode;

  String ruleName = (parameters[1] as SLOTString).value;

  ScannerRuleObject scannerRuleObject = frame.getRuntime().project.slopeGrammarObject.getScannerRule(ruleName);

  ret =
  (ruleName
      == parseNode.nodeName);


  frame.pushData(SLOTBoolean.fromBool(ret));

}