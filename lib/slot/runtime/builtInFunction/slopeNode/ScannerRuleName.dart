import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTBoolean.dart';
import '../../objects/SLOTList.dart';
import '../../../../lib/parser/features/scannerelement/ScannerElement_TreeNode.dart';
import '../../../../lib/grammarparser/ScannerRuleObject.dart';


/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction scannerRuleNameFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  return scannerRuleNameFct_DEBUG(frame, parameters);
};

scannerRuleNameFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  if( !(parameters[0] is SLOTSLOPETreeNode)
  || !(  (parameters[0] as SLOTSLOPETreeNode).slopeTreeNode
           is ScannerElement_TreeNode
      )
  ) {
    frame.pushData(SLOTNull.instance);
    return;
  }
  ScannerElement_TreeNode node = (parameters[0] as SLOTSLOPETreeNode).slopeTreeNode;

  var grammar = frame.getRuntime().project.slopeGrammarObject;
  List<ScannerRuleObject> top = grammar.topScanners;
  List<ScannerRuleObject> bottom = grammar.bottomScanners;


  bool found = false;
  _traverseLists([top, bottom], (ScannerRuleObject obj) {
    if (obj.scannerExpression == node.keyword) {
      frame.pushData(new SLOTString(obj.name));
      found = true;
    }
  });

  if(!found)
  frame.pushData(SLOTNull.instance);

}

_traverseLists<T>(List<List<T>> list, Function(T) traverse) {
  for(List<T> elList in list) {
    elList.forEach(traverse);
  }
}