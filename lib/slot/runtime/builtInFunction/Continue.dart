import '../SLOTObject.dart';
import '../frames/SLOTStackFrame.dart';
import '../frames/SLOTTreeFunctionStackFrame.dart';
import '../objects/SLOTNull.dart';
import 'BuiltInFunction.dart';

BuiltInFunction continueFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
//  frame.expressions.l
  SLOTTreeFunctionStackFrame treeFunctionFrame =
    frame.getSLOTFunctionFamilyFrame();

  if(!treeFunctionFrame.hasNextFamily())
    return SLOTNull.instance;

  SLOTTreeFunctionStackFrame newTreeFuctionFrame =
    treeFunctionFrame.createNextTreeFunctionStackFrame();

  newTreeFuctionFrame.pushOnRuntime(frame, null);




};

