import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTString.dart';
import '../BuiltInFunction.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../../../../lib/util/GrammarUtils.dart';
import '../../objects/SLOTSLOPEGrammar.dart';
import '../../objects/SLOTNull.dart';


BuiltInFunction parseSlopeWordFromGrammarFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters]) {
      parseSlopeWordFromGrammarFct_DEBUG(frame, parameters);
};

parseSlopeWordFromGrammarFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters]) {

  //String grammar = slopeWordGrammars[(parameters[0] as SLOTString).value];
  SLOTString word = parameters[0];
  SLOTSLOPEGrammar grammar = parameters[1];



  try {
    SLOTSLOPETreeNode node = grammar.parseWord(word);
    frame.pushData(node);
  }
  catch(e) {
    print(e);
    frame.pushData(SLOTNull.instance);
  }



}

