import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTBoolean.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';
import '../BuiltInFunction.dart';

BuiltInFunction equalsStringFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  frame.pushData(
      SLOTBoolean.fromBool((parameters[0] as SLOTString).equals(parameters[1] as SLOTString)));
};

