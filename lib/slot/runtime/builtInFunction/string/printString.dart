import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';
import '../BuiltInFunction.dart';
import '../../objects/SLOTNull.dart';

BuiltInFunction printStringFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {

  if(parameters[0] is SLOTString)
  print(

      (parameters[0] as SLOTString).value);

  else if(parameters[0] is SLOTNull)
      print("SLOTNull.instance");
  else {
    throw "damn";
  }
  frame.pushData(
      SLOTNull.instance);
};

