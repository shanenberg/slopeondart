import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../../objects/SLOTString.dart';
import '../BuiltInFunction.dart';


BuiltInFunction parseSlopeWordFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  parseSlopeWordFct_DEBUG(frame, parameters);
};

parseSlopeWordFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  String word = (parameters[0] as SLOTString).value;
  SLOTSLOPETreeNode node = frame
                            .getRuntime()
                            .project
                            .slopeGrammarObject
                            .parseString(word)
                            .toSLOTTreeNode();

  frame.pushData(node);
}

