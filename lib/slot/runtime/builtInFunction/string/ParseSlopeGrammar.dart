import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTString.dart';
import '../../../../lib/util/GrammarUtils.dart';
import '../../../../lib/grammarparser/tree/GrammarObject.dart';
import '../../objects/SLOTSLOPEGrammar.dart';

/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction parseSlopeGrammarFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  return parseSlopeGrammarFct_DEBUG(frame, parameters);
};

parseSlopeGrammarFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  String grammarString = (parameters[0] as SLOTString).value;

  SLOPEGrammarObject grammarObject = parseGrammar(grammarString);

  frame.pushData(new SLOTSLOPEGrammar(grammarObject));
}