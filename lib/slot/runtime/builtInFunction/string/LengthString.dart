import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';
import '../BuiltInFunction.dart';

BuiltInFunction lengthStringFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
//  print((parameters[0] as SLOTString).value);
  frame.pushData(
      new SLOTInteger(
          (parameters[0] as SLOTString).value.length));
};

