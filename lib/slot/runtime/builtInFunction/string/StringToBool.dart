import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTBoolean.dart';

/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction stringToBoolFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  return stringToBoolFct_DEBUG(frame, parameters);
};

stringToBoolFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  String parameter = (parameters[0] as SLOTString).value;

  frame.pushData(SLOTBoolean.fromBool(parameter == "true" ? true
                                                  : false));

}