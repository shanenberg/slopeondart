import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTString.dart';
import '../BuiltInFunction.dart';

BuiltInFunction concatFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {

  concatFctDebug(frame, parameters);
};

concatFctDebug(SLOTStackFrame frame, [List<SLOTObject> parameters]) {

  if(!(parameters[1] is SLOTString)) {
    throw "Your passed parameter is not a string. " + parameters[1].valueToString();
  }
  if(!(parameters[0] is SLOTString)) {
    throw "Your passed parameter is not a string. " + parameters[0].valueToString();
  }

  frame.pushData(
      new SLOTString(
          (parameters[0] as SLOTString).value + (parameters[1] as SLOTString).value));
}