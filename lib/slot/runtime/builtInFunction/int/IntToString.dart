import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';
import '../BuiltInFunction.dart';

BuiltInFunction intToStringFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  frame.pushData(
      new SLOTString((parameters[0] as SLOTInteger).val.toString()));
};

