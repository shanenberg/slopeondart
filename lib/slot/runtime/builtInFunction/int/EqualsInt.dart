import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTInteger.dart';
import '../BuiltInFunction.dart';

BuiltInFunction equalsIntFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  frame.pushData(
      (parameters[0] as SLOTInteger).equals(parameters[1] as SLOTInteger));
};

