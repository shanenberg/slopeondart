import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTBoolean.dart';
import '../../objects/SLOTList.dart';
import '../../../../lib/parser/features/scannerelement/ScannerElement_TreeNode.dart';
import '../../objects/SLOTDouble.dart';


/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction dFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  return dFct_DEBUG(frame, parameters);
};

dFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  int preComma = (parameters[0] as SLOTInteger).val;
  int postComma = (parameters[1] as SLOTInteger).val;

  double d = double.parse("$preComma.$postComma");

  frame.pushData(new SLOTDouble(d));
}