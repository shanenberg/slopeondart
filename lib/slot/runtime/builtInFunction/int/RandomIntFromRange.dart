import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTBoolean.dart';
import '../../objects/SLOTList.dart';
import '../../../../lib/parser/features/scannerelement/ScannerElement_TreeNode.dart';
import 'dart:math';


/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction randomIntFromRangeFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  return randomIntFromRangeFct_DEBUG(frame, parameters);
};

randomIntFromRangeFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters])
{


  int leftIncl = (parameters[0] as SLOTInteger).val;
  int rightIncl = (parameters[1] as SLOTInteger).val;

  Random r = new Random();
  var ret;
  try {
    ret = new SLOTInteger(leftIncl + r.nextInt(rightIncl - leftIncl + 1));
  }
  catch(e) {
    frame.printStackFrame();
    throw e;
  }
    frame.pushData(ret);
}