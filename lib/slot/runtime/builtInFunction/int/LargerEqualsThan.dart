import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTInteger.dart';

/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction largerEqualsThanFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  return largerEqualsThanFct_DEBUG(frame, parameters);
};

largerEqualsThanFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  frame.pushData((parameters[0] as SLOTInteger)
      .largerEqualsThan(parameters[1]));
}