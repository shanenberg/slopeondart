import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTInteger.dart';
import '../BuiltInFunction.dart';

BuiltInFunction smallerThanFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  frame.pushData(
      (parameters[0] as SLOTInteger).smallerThan(parameters[1] as SLOTInteger));
};

