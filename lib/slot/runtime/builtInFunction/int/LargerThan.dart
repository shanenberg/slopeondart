import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTInteger.dart';
import '../BuiltInFunction.dart';

BuiltInFunction largerThanFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  frame.pushData(
      (parameters[0] as SLOTInteger).largerThan(parameters[1] as SLOTInteger));
};

