import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTInteger.dart';
import '../BuiltInFunction.dart';

BuiltInFunction printIntFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  print(parameters[0].valueToString());
  frame.pushData(
      (parameters[0] as SLOTInteger));
};

