import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTBoolean.dart';
import '../../objects/SLOTList.dart';
import '../../../../lib/parser/features/scannerelement/ScannerElement_TreeNode.dart';
import '../../objects/SLOTNumber.dart';


/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction divFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  return divFct_DEBUG(frame, parameters);
};

divFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  num value1 = (parameters[0] as SLOTNumber).value;
  num value2 = (parameters[1] as SLOTNumber).value;

  frame.pushData(SLOTNumber.fromValue(value1 / value2));
}