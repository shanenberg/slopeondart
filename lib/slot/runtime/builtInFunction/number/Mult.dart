import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTInteger.dart';
import '../BuiltInFunction.dart';
import '../../objects/SLOTNumber.dart';

BuiltInFunction multFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  /*frame.pushData(
      new SLOTInteger((parameters[0] as SLOTInteger).val * (parameters[1] as SLOTInteger).val));
  */

  num value1 = (parameters[0] as SLOTNumber).value;
  num value2 = (parameters[1] as SLOTNumber).value;

  frame.pushData(SLOTNumber.fromValue(value1 * value2));
};

