import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTBoolean.dart';
import '../../objects/SLOTList.dart';
import '../../../../lib/parser/features/scannerelement/ScannerElement_TreeNode.dart';
import '../../objects/SLOTHashMap.dart';
import 'dart:math';


/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction splitNumberRandomlyFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  return splitNumberRandomlyFct_DEBUG(frame, parameters);
};

splitNumberRandomlyFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  int number = (parameters[0] as SLOTInteger).val;
  int partCount = (parameters[1] as SLOTInteger).val;

  List<SLOTInteger> list = new List.generate(partCount, (index) => new SLOTInteger(0));

  for(int num = 0; num < number; num++) {
    int randomPartIndex = new Random().nextInt(partCount);
    int value = list[randomPartIndex].val;
    list[randomPartIndex].val = value+1;
  }

  frame.pushData(new SLOTList(list));
}

BuiltInFunction splitNumberRandomlyExceptOneFct =(SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  int number = (parameters[0] as SLOTInteger).val;
  int partCount = (parameters[1] as SLOTInteger).val;

  List<SLOTInteger> list = new List.generate(partCount, (index) => new SLOTInteger(0));

  for(int num = 0; num < number; num++) {
    int randomPartIndex = new Random().nextInt(partCount);
    int value = list[randomPartIndex].val;
    list[randomPartIndex].val = value+1;
  }

  for(int i = 0; i<list.length; i++) {
    if(list[i] == 1) {
      List<SLOTInteger> notZeroes = list.where((SLOTInteger sInt) => sInt.val != 0 && list.indexOf(sInt) != i).toList();
      int randIndex = _randFromRange(0, notZeroes.length-1);
      notZeroes[randIndex].val++;
      list[i].val--;
    }
  }

  frame.pushData(new SLOTList(list));
};

_randFromRange(int min, int max) => min + new Random().nextInt(max-min+1);