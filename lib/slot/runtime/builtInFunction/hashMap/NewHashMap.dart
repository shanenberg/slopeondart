import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTBoolean.dart';
import '../../objects/SLOTList.dart';
import '../../../../lib/parser/features/scannerelement/ScannerElement_TreeNode.dart';
import '../../objects/SLOTHashMap.dart';





/**
  creates new hash map.
    you can also make a hashmap like this:
    var map = newHashMap(["key1", val1,
                          "key2", val2,
                          ...   , ...]);
 */
BuiltInFunction newHashMapFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  return newHashMapFct_DEBUG(frame, parameters);
};

newHashMapFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  SLOTHashMap map = new SLOTHashMap({});
  if(parameters.length == 1) {
    List<SLOTObject> elements = (parameters[0] as SLOTList).elements;
    String currentKey = null;
    for(int i = 0; i < elements.length; i++) {
      if(i %2 == 0) {
        currentKey = (elements[i] as SLOTString).value;
      }
      else {
        map.map[currentKey] = elements[i];
      }
    }
  }

  frame.pushData(map);
}