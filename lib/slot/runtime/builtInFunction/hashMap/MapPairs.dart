import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTBoolean.dart';
import '../../objects/SLOTList.dart';
import '../../../../lib/parser/features/scannerelement/ScannerElement_TreeNode.dart';
import '../../objects/SLOTHashMap.dart';


/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction mapPairsFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  return mapPairsFct_DEBUG(frame, parameters);
};

mapPairsFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  var map = (parameters[0] as SLOTHashMap).map;
  List<SLOTList> pairList = [];

  map.forEach((String key, SLOTObject value)   {
    List<SLOTObject> pair = [new SLOTString(key), value];
    pairList.add(new SLOTList(pair));
  });

  frame.pushData(new SLOTList(pairList));
}