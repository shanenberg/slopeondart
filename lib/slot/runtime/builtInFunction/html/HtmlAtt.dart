import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTBoolean.dart';
import '../../objects/SLOTHTMLKeyboardEvent.dart';

/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction htmlAttFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  return htmlAttFct_DEBUG(frame, parameters);
};

htmlAttFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  if(parameters[0] is SLOTHTMLKeyboardEvent) {
    SLOTHTMLKeyboardEvent ev = parameters[0];
    var attribute = ev.accessAttribute((parameters[1] as SLOTString).value);
    frame.pushData(_convertLiteral(attribute));

  }
  else
    throw "whats this";
}

_convertLiteral(Object o) {
  if(o is String) {
    return new SLOTString(o);
  }
  else if(o is int) {
    return new SLOTInteger(o);
  }
  else if(o is bool) {
    return SLOTBoolean.fromBool(o);
  }
}