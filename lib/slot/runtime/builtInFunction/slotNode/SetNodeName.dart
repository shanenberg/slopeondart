import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTBoolean.dart';

/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction setNodeNameFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  return setNodeNameFct_DEBUG(frame, parameters);
};

setNodeNameFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  SLOTTreeNode treeNode = parameters[0];
  SLOTObject name = parameters[1];
  treeNode.value = name;

  frame.pushData(SLOTNull.instance);
}