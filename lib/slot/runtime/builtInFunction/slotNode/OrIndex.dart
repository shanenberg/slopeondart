import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';

BuiltInFunction OrIndexFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {

  SLOTSLOPETreeNode anOrNode = (parameters[0] as SLOTSLOPETreeNode);
  OR_TreeNode n = (anOrNode.slopeTreeNode as OR_TreeNode);
  frame.pushData(new SLOTInteger(n.orIndex + 1));
};

