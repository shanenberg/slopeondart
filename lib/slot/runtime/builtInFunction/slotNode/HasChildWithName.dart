import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTBoolean.dart';
import '../../objects/SLOTTrue.dart';
import '../../objects/SLOTFalse.dart';

/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction hasChildWithNameFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  return hasChildWithNameFct_DEBUG(frame, parameters);
};

hasChildWithNameFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  SLOTTreeNode node = parameters[0];
  SLOTString name = parameters[1];

  for(SLOTTreeNode child in node.children.elements) {
    if( child.value is SLOTString
    && (child.value as SLOTString).value == name.value
    ) {
      frame.pushData(SLOTTrue.instance);
      return;
    }
  }
  frame.pushData(SLOTFalse.instance);
}