import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTBoolean.dart';
import '../../objects/SLOTList.dart';
import '../../../../lib/parser/features/scannerelement/ScannerElement_TreeNode.dart';


/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction childByNameChildFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  return childByNameChildFct_DEBUG(frame, parameters);
};

childByNameChildFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  SLOTTreeNode treeNode = parameters[0];
  String name = (parameters[1] as SLOTString).value;
  for(SLOTTreeNode child in treeNode.children.elements) {
    if((child.value as SLOTString).value == name) {
      frame.pushData(child.children.elements[0] as SLOTTreeNode);
      return;
    }
  }
  throw("wtf");
}