import '../../../../lib/parser/features/or/OR_ParserConstructor.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../objects/SLOTTrue.dart';
import '../../objects/SLOTFalse.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../objects/SLOTString.dart';
import '../../../runtime/objects/SLOTTreeNode.dart';

BuiltInFunction unsignedFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  return unsignedFct_DEBUG(frame, parameters);
};

unsignedFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters]) {

  SLOTTreeNode node = parameters[0];

  SLOTString str = parameters[1];
  if(node.value is SLOTString
  && (node.value as SLOTString).value.toLowerCase()
  == str.value.toLowerCase()
  ) {
    frame.pushData(SLOTTrue.instance);
  }
  else {
    frame.pushData(SLOTFalse.instance);
  }

}