import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTBoolean.dart';
import '../../objects/SLOTList.dart';
import '../../../../lib/parser/features/scannerelement/ScannerElement_TreeNode.dart';


/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction indexInParentFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  return indexInParentFct_DEBUG(frame, parameters);
};

indexInParentFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  SLOTTreeNode node = parameters[0];

  if(node.parent == null) {
    frame.pushData(new SLOTInteger(0));
    return;
  } else {
    frame.pushData(new SLOTInteger(node.parent.children.elements.indexOf(node)
                                    + 1));
    return;
  }
}