import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTTreeNode.dart';
import '../BuiltInFunction.dart';
import '../../objects/SLOTFalse.dart';
import '../../objects/SLOTTrue.dart';

BuiltInFunction hasChildrenFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  hasChildrenFctDebug(frame, parameters);
};

hasChildrenFctDebug(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  SLOTTreeNode ret = parameters[0] as SLOTTreeNode;
  if(ret.children.elements.length>0)
    frame.pushData(SLOTTrue.instance);
  else
    frame.pushData(SLOTFalse.instance);
}