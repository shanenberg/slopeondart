import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTTreeNode.dart';
import '../BuiltInFunction.dart';
import '../../objects/SLOTFalse.dart';
import '../../objects/SLOTTrue.dart';

BuiltInFunction hasNoChildrenFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  hasNoChildrenFctDebug(frame, parameters);
};

hasNoChildrenFctDebug(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  SLOTTreeNode ret = parameters[0] as SLOTTreeNode;
  if(ret.children.elements.length>0)
    frame.pushData(SLOTFalse.instance);
  else
    frame.pushData(SLOTTrue.instance);
}