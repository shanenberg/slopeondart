import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTList.dart';
import '../../objects/SLOTTreeNode.dart';
import '../BuiltInFunction.dart';

BuiltInFunction childAtFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  childrenAtFctDebug(frame, parameters);
};

childrenAtFctDebug(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  SLOTTreeNode parentNode = (parameters[0] as SLOTTreeNode);
  SLOTInteger index = (parameters[1] as SLOTInteger);
  try {
    frame.pushData(parentNode.children.elements[index.val - 1]);
  } catch(e) {
    print("hi");
  }
}