import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTTreeNode.dart';
import '../BuiltInFunction.dart';

BuiltInFunction slotNodeNameFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {

  slotNodeNameFctBody(frame, parameters);
};

slotNodeNameFctBody(SLOTStackFrame frame, [List<SLOTObject> parameters]) {

//  print("*******************");
//  print("*******************");
//  print("*******************");
//  print("*******************");
//  print(parameters[0]);
//  print((parameters[0] as SLOTSLOPETreeNode).value);
//  print(parameters[0].valueToString());

//if ((parameters[0] as SLOTTreeNode).value == null) {
//  throw "a treeNode should never have null as value";
//}

frame.pushData(
(parameters[0] as SLOTTreeNode).value);

//if (frame.dataLength()!=1)
//  throw "something is wrong here: " + frame.dataLength().toString();

}