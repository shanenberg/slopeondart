import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTTreeNode.dart';
import '../BuiltInFunction.dart';
import '../../objects/SLOTNull.dart';

BuiltInFunction parentFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  parentFctDebug(frame, parameters);
};

parentFctDebug(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  SLOTTreeNode parentNode;

  try {
    parentNode = (parameters[0] as SLOTTreeNode).parent;

  }
  catch(e) {
    print("hi");
  }

    if(parentNode == null)
    frame.pushData(SLOTNull.instance);
  else
    frame.pushData(parentNode);
}