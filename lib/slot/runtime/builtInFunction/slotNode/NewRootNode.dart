import '../../../node/SLOTROOTNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTSLOTNode.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTTreeNode.dart';
import '../BuiltInFunction.dart';

BuiltInFunction newRootNodeFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {

  SLOTROOTNode parentNode = new SLOTROOTNode();

  frame.pushData(parentNode);
};

