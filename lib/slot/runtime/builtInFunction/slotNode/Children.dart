import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTList.dart';
import '../../objects/SLOTTreeNode.dart';
import '../BuiltInFunction.dart';

BuiltInFunction childrenFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
/*
  SLOTList ret = new SLOTList(new List());

  SLOTTreeNode parentNode = (parameters[0] as SLOTTreeNode);
  for(SLOTObject o in parentNode.children.elements) {
    ret.elements.add(o);
  }

  frame.pushData(ret);
*/
  childrenFctDebug(frame, parameters);
};

childrenFctDebug(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  SLOTList ret = new SLOTList(new List());
  SLOTTreeNode parentNode;
  try {
    parentNode = (parameters[0] as SLOTTreeNode);

  } catch (e) {
    print(e);
  }


  for(SLOTObject o in parentNode.children.elements) {
    ret.elements.add(o);
  }

  frame.pushData(ret);

}