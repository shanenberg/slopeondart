import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTString.dart';
import '../BuiltInFunction.dart';

BuiltInFunction slotNodeFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  frame.pushData(
      new SLOTString(parameters[0].valueToString()));
};

