import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTTreeNode.dart';
import '../BuiltInFunction.dart';

BuiltInFunction newSlotNodeFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {

  SLOTString nodeName = (parameters[0] as SLOTString);
  SLOTTreeNode tn = new SLOTTreeNode();
  tn.origin = frame.currentSourceSLOTNodes[0];
  tn.value = nodeName;

  frame.pushData(tn);
};

