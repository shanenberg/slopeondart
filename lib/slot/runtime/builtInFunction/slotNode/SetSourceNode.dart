import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTTreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../slot/runtime/objects/SLOTNull.dart';

BuiltInFunction setSourceNodeFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  return setSourceNodeFct_DEBUG(frame, parameters);
};

setSourceNodeFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters]) {

  SLOTTreeNode current = parameters[0];
  SLOTTreeNode otherSource = parameters[1];
  current.origin = otherSource;
  frame.pushData(SLOTNull.instance);
}