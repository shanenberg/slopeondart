import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTTreeNode.dart';
import '../BuiltInFunction.dart';

BuiltInFunction addSlotNodeFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters]) {

  SLOTTreeNode parentNode = (parameters[0] as SLOTTreeNode);
  SLOTTreeNode childNode = (parameters[1] as SLOTTreeNode);

  parentNode.children.elements.add(childNode);
  childNode.parent = parentNode;

  frame.pushData(childNode);
};