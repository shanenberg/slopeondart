import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTList.dart';
import '../../objects/SLOTTreeNode.dart';
import '../BuiltInFunction.dart';

BuiltInFunction removeAllChildrenFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {

  SLOTTreeNode aTreeNode = (parameters[0] as SLOTTreeNode);
  aTreeNode.children = new SLOTList(new List());

  frame.pushData(aTreeNode);
};

