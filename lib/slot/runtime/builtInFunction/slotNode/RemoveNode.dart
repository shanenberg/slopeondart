import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTTreeNode.dart';

/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction removeNodeFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  return removeNodeFct_DEBUG(frame, parameters);
};

removeNodeFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters]) {

  SLOTTreeNode current = (parameters[0] as SLOTTreeNode);
  SLOTTreeNode parent = current.parent;

  parent.children.elements.remove(current);
  current.parent = null;
/*
  frame.getRuntime().slopeTreeNodes[0] =
      (frame.getRuntime().slopeTreeNodes[0] as SLOTSLOPETreeNode).slopeTreeNode.toSLOTTreeNode();
*/
  frame.pushData(current);

}