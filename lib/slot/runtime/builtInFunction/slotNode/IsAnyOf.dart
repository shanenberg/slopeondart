import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTBoolean.dart';
import '../../objects/SLOTList.dart';
import '../../../../lib/parser/features/scannerelement/ScannerElement_TreeNode.dart';


/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction isAnyOfFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  return isAnyOfFctFct_DEBUG(frame, parameters);
};

isAnyOfFctFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  SLOTTreeNode node = parameters[0];
  String nodeName = (node.value as SLOTString).value;

  bool ret = false;
  for(int i = 1; i <parameters.length; i++) {
      String name = (parameters[i] as SLOTString).value;
      if(name == nodeName) {
        ret = true;
        break;
      }
  }

  frame.pushData(SLOTBoolean.fromBool(ret));
}