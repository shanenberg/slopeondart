import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTTreeNode.dart';
import '../BuiltInFunction.dart';

BuiltInFunction slotOriginNodeFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  return slotOriginNodeFct_DEBUG(frame, parameters);
};

slotOriginNodeFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters]) {

  SLOTTreeNode current = parameters[0];
  while(!(current is SLOTSLOPETreeNode)) {
    current = current.origin;
  }
  frame.pushData(current);
}