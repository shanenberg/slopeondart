import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTTreeNode.dart';
import '../BuiltInFunction.dart';

BuiltInFunction slotSourceNodeFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  return slotSourceNodeFct_DEBUG(frame, parameters);
};

slotSourceNodeFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters]) {

  SLOTTreeNode current = parameters[0];
  if (current.origin==null) {
    frame.pushData(SLOTNull.instance);
  } else {
    frame.pushData(current.origin);
  }
}