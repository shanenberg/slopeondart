import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTTreeNode.dart';
import '../BuiltInFunction.dart';

BuiltInFunction addAlternativeNodeFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {

  SLOTTreeNode parentNode = (parameters[0] as SLOTTreeNode);
  SLOTTreeNode childNode = (parameters[1] as SLOTTreeNode);

  frame.pushData(SLOTNull.instance);
};

