import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTBoolean.dart';
import '../../objects/SLOTNull.dart';
import '../BuiltInFunction.dart';

BuiltInFunction notNullFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  frame.pushData(SLOTBoolean.fromBool(!(parameters[0] is SLOTNull)));
};

