import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTBoolean.dart';
import '../BuiltInFunction.dart';
import '../../objects/SLOTTreeNode.dart';

BuiltInFunction isNodeFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  frame.pushData(SLOTBoolean.fromBool((parameters[0] is SLOTTreeNode)));
};

