import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTBoolean.dart';
import '../BuiltInFunction.dart';
import '../../../../slot/runtime/objects/SLOTSLOPETreeNode.dart';

BuiltInFunction isNamedSLOTSLOPETreeNodeFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters]) {
      isNamedSLOTSLOPETreeNode_DEBUG(frame, parameters);
};

isNamedSLOTSLOPETreeNode_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  var ret = false;
  if (parameters[0] is SLOTSLOPETreeNode) {
    if ((parameters[0] as SLOTSLOPETreeNode).slopeTreeNode.nodeName!=null)
    ret = true;
  }

  frame.pushData(SLOTBoolean.fromBool(ret));
}