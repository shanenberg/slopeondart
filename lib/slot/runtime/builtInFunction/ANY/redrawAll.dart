import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTBoolean.dart';
import '../../objects/SLOTNull.dart';
import '../BuiltInFunction.dart';

BuiltInFunction redrawAllFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
    frame.getRuntime().emptyStack();
    frame.getRuntime().redrawAll();
};

