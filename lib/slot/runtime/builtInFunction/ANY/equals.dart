import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTBoolean.dart';
import '../BuiltInFunction.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTHashMap.dart';
import '../../objects/SLOTList.dart';


BuiltInFunction equalsFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters]) {
    frame.pushData(SLOTBoolean.fromBool(equalsSlot(parameters[0], parameters[1])));

};

BuiltInFunction notEqualsFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters]) {
      frame.pushData(SLOTBoolean.fromBool(!equalsSlot(parameters[0], parameters[1])));
};

/* dart returning function, used by the other builtinfunctions literally or negated... */
equalsSlot(SLOTObject par1, SLOTObject par2) {
  bool ret;

  if(par1.runtimeType == par2.runtimeType) {
    if (par1 is SLOTInteger) {
      ret = (par1 as SLOTInteger).val
              ==
            (par2 as SLOTInteger).val;
    }

    else if (par1 is SLOTBoolean) {
      ret = (par1 as SLOTBoolean).boolValue()
             ==
            (par2 as SLOTBoolean).boolValue();
    }

    else if (par1 is SLOTString) {
      ret = (par1 as SLOTString).value
              ==
            (par2 as SLOTString).value;
    }

    else if (par1 is SLOTTreeNode) {
      ret = (par1 == par2);
    }

    else if (par1 is SLOTNull) {
      ret = (par1 == par2);
    }
    else if (par1 is SLOTHashMap) {
      ret = (par1 == par2);
    }
    else if(par1 is SLOTList) {
      ret = (par1 == par2);
    }
    else
      throw "builtInfunction equals not defined for ${par1.runtimeType.toString()}";

  }
  else {
    ret = false;
  }

  return ret;
}