import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../BuiltInFunction.dart';
import '../../objects/SLOTList.dart';


/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction englishWordsFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  return englishWordsFct_DEBUG(frame, parameters);
};

englishWordsFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters])
{

  frame.pushData(new SLOTList(slotWords));
}






