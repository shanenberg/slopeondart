import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTBoolean.dart';
import '../BuiltInFunction.dart';

BuiltInFunction isBooleanFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  frame.pushData(SLOTBoolean.fromBool((parameters[0] is SLOTBoolean)));
};

