import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTBoolean.dart';
import '../BuiltInFunction.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTList.dart';
import '../../objects/SLOTHashMap.dart';
import '../../objects/SLOTNumber.dart';

BuiltInFunction toStringFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  frame.pushData(toStringSlot(parameters[0]));
};

SLOTString toStringSlot(SLOTObject param) {
  SLOTString ret = new SLOTString("");

  if(param is SLOTNumber) {
    ret.value = param.value.toString();
  }
  else if(param is SLOTBoolean) {
    ret.value = param.boolValue().toString();
  }
  else if(param is SLOTString) {
    ret.value = param.value;
  }
  else if(param is SLOTTreeNode) {
    ret.value = "#"+param.valueToString();
  }
  else if(param is SLOTNull) {
    ret.value = "null";
  }
  else if(param is SLOTList) {
    ret.value = param.valueToString();
  }
  else if(param is SLOTHashMap) {
    ret.value = "{";
    param.map.forEach((String key, SLOTObject value) {
      ret.value += "\n$key : ${toStringSlot(value).value}\n";
    });
    ret.value += "}";
  }
  else {
    throw "?:)? ${param.runtimeType}";
  }


  return ret;
}


