import '../SLOTObject.dart';
import '../SLOTRuntime.dart';
import '../frames/SLOTStackFrame.dart';
import 'ANY/isNull.dart';
import 'ANY/isNotNull.dart';
import 'ANY/redrawAll.dart';
import 'ANY/ToString.dart';
import 'bool/And.dart';
import 'bool/Not.dart';
import 'bool/Or.dart';
import 'int/EqualsInt.dart';
import 'int/IntToString.dart';
import 'int/LargerThan.dart';
import 'int/RandomInt.dart';
import 'number/Minus.dart';
import 'number/Mult.dart';
import 'number/Plus.dart';
import 'int/SmallerThan.dart';
import 'int/printInt.dart';
import 'list/AddElement.dart';
import 'list/ElementAt.dart';
import 'list/IndexOf.dart';
import 'list/IsEmpty.dart';
import 'list/IsNotEmpty.dart';
import 'list/Length.dart';
import 'list/SetElementAt.dart';
import 'bool/EqualsBool.dart';
import 'ANY/isBoolean.dart';
import 'ANY/isInteger.dart';
import 'ANY/isNode.dart';
import 'ANY/isString.dart';
import 'ANY/equals.dart';
import 'int/LargerEqualsThan.dart';
import 'int/SmallerEqualsThan.dart';
import 'ANY/isNamedSLOTSLOPETreeNode.dart';
import 'list/RemoveElementAt.dart';
import 'list/ClearElements.dart';
import 'list/ListToString.dart';
import 'list/InsertElementAt.dart';
import 'ANY/RuntimeType.dart';
import 'list/RemoveElementFromList.dart';
import 'ANY/Clone.dart';
import 'hashMap/MapGet.dart';
import 'hashMap/MapPut.dart';
import 'hashMap/NewHashMap.dart';
import 'list/FirstElement.dart';
import 'list/LastElement.dart';
import 'int/D.dart';
import 'number/Div.dart';
import 'int/RandomIntFromRange.dart';
import 'bool/RandomBool.dart';
import 'bool/RandomBoolTrueProb.dart';
import 'ANY/Print.dart';
import 'int/IntToCharString.dart';
import 'ANY/Type.dart';
import 'ANY/TypeEquals.dart';
import 'hashMap/MapValues.dart';
import 'hashMap/MapKeys.dart';
import 'hashMap/MapRemove.dart';
import 'hashMap/MapEquals.dart';
import 'hashMap/MapPairs.dart';
import 'ANY/B.dart';
import 'package:english_words/english_words.dart';
import 'ANY/EnglishAdjectives.dart';
import 'ANY/EnglishNouns.dart';
import 'ANY/EnglishWords.dart';
import '../objects/SLOTString.dart';
import 'number/SplitNumberRandomly.dart';
import '../../../lib/grammarparser/ScannerRuleObject.dart';
import '../../runtime/objects/SLOTHashMap.dart';
import '../objects/SLOTInteger.dart';
import 'list/SubList.dart';
import 'slotNode/IsAnyOf.dart';
import 'slotNode/Unsigned.dart';
import 'slotNode/GetExtra.dart';
import 'slotNode/PutExtra.dart';
import 'string/StringLength.dart';
import 'slopeNode/IsNamedAndChain.dart';
import 'string/CharStringToInt.dart';
import 'slopeNode/AddNFOLDChildTemplate.dart';
import 'string/IsAllCaps.dart';
import 'slopeNode/ScannerRuleName.dart';
import 'slopeNode/IsScannerRule.dart';
import 'slopeNode/InsertORChildTemplateAt.dart';
import 'slopeNode/InsertOPTChildTemplate.dart';
import 'slopeNode/Keyword.dart';
import 'slotNode/LastChild.dart';
import 'slotNode/ChildByNameChild.dart';
import 'slotNode/ChildByNameValue.dart';
import 'slotNode/SetChildAt.dart';
import 'string/StringToCharList.dart';
import 'slopeNode/ClearNFOLD_0.dart';
import 'slotNode/SlotNodeNameEquals.dart';
import 'slopeNode/DeleteChar.dart';
import 'slopeNode/InsertNFOLDChildAt.dart';
import 'slopeNode/InsertOPTChild.dart';
import 'slopeNode/CanSetCharAnyExcept.dart';
import 'slopeNode/CanSetCharGroup.dart';
import 'slopeGrammar/CreateTemplateFromGrammar.dart';
import 'slopeNode/InsertORChild.dart';
import 'slotNode/IndexInParent.dart';
import 'slopeNode/RemoveCharAnyExcept.dart';
import 'slopeNode/RemoveCharGroup.dart';
import 'slopeNode/SetCharAnyExcept.dart';
import 'slopeNode/SetCharGroup.dart';
import 'slotNode/ChildByName.dart';
import 'slotNode/HasChildWithName.dart';
import 'slotNode/SetNodeName.dart';
import 'string/StringToBool.dart';
import 'string/StringToInt.dart';
import 'string/ThrowFct.dart';
import 'string/ParseSlopeWordFromGrammar.dart';
import 'string/ParseSlopeGrammar.dart';
import 'slotNode/Replace.dart';
import 'slopeNode/HasOPTChild.dart';
import 'slopeNode/CreateSingleChildTemplate.dart';
import 'slopeNode/RemoveOPTChild.dart';
import 'slotNode/RemoveChildAt.dart';
import 'slotNode/RemoveNode.dart';
import 'slopeNode/RemoveNFOLDChildAt.dart';
import 'slotNode/SetSourceNode.dart';
import 'slopeNode/RemoveNFOLDChild.dart';
import 'slopeNode/InsertNFOLDChildTemplateAt.dart';
import 'slopeNode/CreateNFOLDChildTemplate.dart';
import 'slopeNode/CreateTemplate.dart';
import 'slopeNode/CreateORChildTemplateAt.dart';
import 'slopeNode/IsNFOLD_0.dart';
import 'slopeNode/IsNFOLD_1.dart';
import 'slopeNode/IsNamedAnd.dart';
import 'slopeNode/SwitchTo.dart';
import 'slotNode/Parent.dart';
import 'slopeNode/DeleteNode.dart';
import 'slopeNode/NumAlternatives.dart';
import 'slopeNode/SwitchToNextNode.dart';
import 'slotNode/AddAlternativeNode.dart';
import 'slotNode/AddSlotNode.dart';
import 'slotNode/ChildAt.dart';
import 'slotNode/Children.dart';
import 'slotNode/HasChildren.dart';
import 'slotNode/HasNoChildren.dart';
import 'slotNode/NewRootNode.dart';
import 'slotNode/NewSlotNode.dart';
import 'slotNode/OrIndex.dart';
import 'slotNode/RemoveAllChildren.dart';
import 'slotNode/SlotNodeName.dart';
import 'slotNode/SlotOriginNode.dart';
import 'slotNode/SlotSourceNode.dart';
import 'string/Concat.dart';
import 'string/EqualsString.dart';
import 'string/LengthString.dart';
import 'string/NotEqualsString.dart';
import 'string/ParseSlopeWord.dart';
import 'string/printString.dart';
import 'string/subStringAt.dart';

typedef SLOTObject BuiltInFunction(SLOTStackFrame frame, [List<SLOTObject> parameters]);



List<SLOTString> slotNouns = [];
List<SLOTString> slotAdjectives = [];
List<SLOTString> slotWords = [];

List<String> sqlKeywords = [
  "union",
  "except",
  "intersect",
  "all",
  "select",
  "distinct",
  "as",
  "or",
  "and",
  "not",
  "is",
  "true",
  "false",
  "unknown",
  "between",
  "in",
  "like",
  "null",
  "match",
  "unique",
  "partial",
  "full",
  "overlaps",
  "some",
  "any",
  "exists",
  "cast",
  "date",
  "time",
  "timestamp",
  "interval",
  "to",
  "count",
  "avg",
  "max",
  "min",
  "sum",
  "from",
  "where",
  "group",
  "by",
  "having",
  "order",
  "cross",
  "natural",
  "left",
  "right",
  "outer",
  "inner",
  "join",
  "on",
  "using",
  "case",
  "when",
  "then",
  "else",
  "end",
  "collate",
  "asc",
  "desc",
  "extract"
];

loadBuiltInFunctions(SLOTRuntime runtime) {


  nouns.forEach(
  (String str) {
    if(!sqlKeywords.contains(str))
    slotNouns.add(new SLOTString(str));
  });
  all.forEach((String str) {
    if(!sqlKeywords.contains(str))
    slotWords.add(new SLOTString(str));
  });
  adjectives.forEach((String str) {
    if(!sqlKeywords.contains(str))
      slotAdjectives.add(new SLOTString(str));
  });

  // SLOPENodes
  runtime.builtInFunctions["switchToNextNode"] = switchToNextNodeFct;
  runtime.builtInFunctions["deleteNode"] = deleteNodeFct;
  runtime.builtInFunctions["numAlternatives"] = numAlternativesFct;
  runtime.builtInFunctions["switchTo"] = switchToOrFct;
  runtime.builtInFunctions["orIndex"] = OrIndexFct;
  runtime.builtInFunctions["isNamedAnd"] = isNamedAndFct;
  runtime.builtInFunctions["isNFOLD_0"] = isNFOLD_0Fct;
  runtime.builtInFunctions["isNFOLD_1"] = isNFOLD_1Fct;
  runtime.builtInFunctions["createORChildTemplateAt"] = createORChildTemplateAtFct;
  runtime.builtInFunctions["createTemplate"] = createTemplateFct;
  runtime.builtInFunctions["createNFOLDChildTemplate"] = createNFOLDChildTemplateFct;
  runtime.builtInFunctions["insertNFOLDChildAt"] = insertNFOLDChildAtFct;
  runtime.builtInFunctions["insertNFOLDChildTemplateAt"] = insertNFOLDChildTemplateAtFct;
  runtime.builtInFunctions["removeNFOLDChild"] = removeNFOLDChildFct;
  runtime.builtInFunctions["removeNFOLDChildAt"] = removeNFOLDChildAtFct;
  runtime.builtInFunctions["createSingleChildTemplate"] = createSingleChildTemplateFct;
  runtime.builtInFunctions["insertOPTChildTemplate"] = insertOPTChildTemplateFct;
  runtime.builtInFunctions["removeOPTChild"] = removeOPTChildFct;
  runtime.builtInFunctions["hasOPTChild"] = hasOPTChildFct;
  runtime.builtInFunctions["isNamedSLOTSLOPETreeNode"] = isNamedSLOTSLOPETreeNodeFct;
  runtime.builtInFunctions["setCharAnyExcept"] = setCharAnyExceptFct;
  runtime.builtInFunctions["canSetCharAnyExcept"] = canSetCharAnyExceptFct;
  runtime.builtInFunctions["removeCharAnyExcept"] = removeCharAnyExceptFct;
  runtime.builtInFunctions["removeCharGroup"] = removeCharGroupFct;
  runtime.builtInFunctions["setCharGroup"] = setCharGroupFct;
  runtime.builtInFunctions["canSetCharGroup"] = canSetCharGroupFct;
  runtime.builtInFunctions["insertORChild"] = insertORChildFct;
  runtime.builtInFunctions["insertORChildTemplateAt"] = insertORChildTemplateAtFct;
  runtime.builtInFunctions["insertOPTChild"] = insertOPTChildFct;
  runtime.builtInFunctions["deleteChar"] = deleteCharFct;
  runtime.builtInFunctions["keyword"] = keywordFct;
  runtime.builtInFunctions["isScannerRule"] = isScannerRuleFct;
  runtime.builtInFunctions["scannerRuleName"] = scannerRuleNameFct;
  runtime.builtInFunctions["addNFOLDChildTemplate"] = addNFOLDChildTemplateFct;
  runtime.builtInFunctions["isNamedAndChain"] = isNamedAndChainFct;
  runtime.builtInFunctions["englishWords"] = englishWordsFct;
  runtime.builtInFunctions["englishNouns"] = englishNounsFct;
  runtime.builtInFunctions["englishAdjectives"] = englishAdjectivesFct;


  //SLOPEGrammar

  runtime.builtInFunctions["createTemplateFromGrammar"] = createTemplateFromGrammarFct;

  // ANY
  runtime.builtInFunctions["isNotNull"] = notNullFct;
  runtime.builtInFunctions["isNull"] = isNullFct;
  runtime.builtInFunctions["redrawAll"] = redrawAllFct;
  runtime.builtInFunctions["isNode"] = isNodeFct;
  runtime.builtInFunctions["isBoolean"] = isBooleanFct;
  runtime.builtInFunctions["isInteger"] = isIntegerFct;
  runtime.builtInFunctions["isString"] = isStringFct;
  runtime.builtInFunctions["equals"] = equalsFct;
  runtime.builtInFunctions["notEquals"] = notEqualsFct;
  runtime.builtInFunctions["toString"] = toStringFct;
  runtime.builtInFunctions["runtimeType"] = runtimeTypeFct;
  runtime.builtInFunctions["clone"] = cloneFct;
  runtime.builtInFunctions["print"] = printFct;
  runtime.builtInFunctions["type"] = typeFct;
  runtime.builtInFunctions["typeEquals"] = typeEqualsFct;
  runtime.builtInFunctions["b"] = bFct;


  // Lists
  runtime.builtInFunctions["addElement"] = addElementFct;
  runtime.builtInFunctions["elementAt"] = elementAtFct;
  runtime.builtInFunctions["indexOf"] = indexOfFct;
  runtime.builtInFunctions["length"] = lengthFct;
  runtime.builtInFunctions["isEmpty"] = isEmptyFct;
  runtime.builtInFunctions["isNotEmpty"] = isNotEmptyFct;
  runtime.builtInFunctions["setElementAt"] = setElementAtFct;
  runtime.builtInFunctions["removeElementAt"] = removeElementAtFct;
  runtime.builtInFunctions["clearElements"] = clearElementsFct;
  runtime.builtInFunctions["listToString"] = listToStringFct;
  runtime.builtInFunctions["insertElementAt"] = insertElementAtFct;
  runtime.builtInFunctions["removeElementFromList"] = removeElementFromListFct;
  runtime.builtInFunctions["firstElement"] = firstElementFct;
  runtime.builtInFunctions["lastElement"] = lastElementFct;
  runtime.builtInFunctions["subList"] = subListFct;

  // SLOTNodes
  runtime.builtInFunctions["addAlternativeNode"] = addAlternativeNodeFct;
  runtime.builtInFunctions["addSlotNode"] = addSlotNodeFct;
  runtime.builtInFunctions["children"] = childrenFct;
  runtime.builtInFunctions["childAt"] = childAtFct;
  runtime.builtInFunctions["hasChildren"] = hasChildrenFct;
  runtime.builtInFunctions["hasNoChildren"] = hasNoChildrenFct;
  runtime.builtInFunctions["newSlotNode"] = newSlotNodeFct;
  runtime.builtInFunctions["slotNodeName"] = slotNodeNameFct;
  runtime.builtInFunctions["slotNodeNameEquals"] = slotNodeNameEqualsFct;
  runtime.builtInFunctions["newRootNode"] = newRootNodeFct;
  runtime.builtInFunctions["removeAllChildren"] = removeAllChildrenFct;
  runtime.builtInFunctions["slotOriginNode"] = slotOriginNodeFct;
  runtime.builtInFunctions["slotSourceNode"] = slotSourceNodeFct;
  runtime.builtInFunctions["parent"] = parentFct;
  runtime.builtInFunctions["removeNode"] = removeNodeFct;
  runtime.builtInFunctions["removeChildAt"] = removeChildAtFct;
  runtime.builtInFunctions["setSourceNode"] = setSourceNodeFct;
  runtime.builtInFunctions["replace"] = replaceFct;
  runtime.builtInFunctions["setNodeName"] = setNodeNameFct;
  runtime.builtInFunctions["hasChildWithName"] = hasChildWithNameFct ;
  runtime.builtInFunctions["childByName"] = childByNameFct;
  runtime.builtInFunctions["clearNFOLD_0"] = clearNFOLD_0Fct;
  runtime.builtInFunctions["setChildAt"] = setChildAtFct;
  runtime.builtInFunctions["childByNameValue"] = childByNameValueFct;
  runtime.builtInFunctions["childByNameChild"] = childByNameChildFct;
  runtime.builtInFunctions["lastChild"] = lastChildFct;
  runtime.builtInFunctions["putExtra"] = putExtraFct;
  runtime.builtInFunctions["getExtra"] = getExtraFct;
  runtime.builtInFunctions["unsigned"] = unsignedFct;
  runtime.builtInFunctions["isAnyOf"] = isAnyOfFct;


  // String
  runtime.builtInFunctions["equalsString"] = equalsStringFct;
  runtime.builtInFunctions["notEqualsString"] = notEqualsStringFct;
  runtime.builtInFunctions["printString"] = printStringFct;
  runtime.builtInFunctions["concat"] = concatFct;
  runtime.builtInFunctions["lengthString"] = lengthStringFct;
  runtime.builtInFunctions["subStringAt"] = subStringAtFct;
  runtime.builtInFunctions["parseSlopeWord"] = parseSlopeWordFct;
  runtime.builtInFunctions["parseSlopeGrammar"] = parseSlopeGrammarFct;
  runtime.builtInFunctions["parseSlopeWordFromGrammar"] = parseSlopeWordFromGrammarFct;
  runtime.builtInFunctions["throw"] = throwFct;
  runtime.builtInFunctions["stringToBool"] = stringToBoolFct;
  runtime.builtInFunctions["stringToInt"] = stringToIntFct;
  runtime.builtInFunctions["stringToCharList"] = stringToCharListFct;
  runtime.builtInFunctions["isAllCaps"] = isAllCapsFct;
  runtime.builtInFunctions["charStringToInt"] = charStringToIntFct;
  runtime.builtInFunctions["stringLength"] = stringLengthFct;


  // Int
  runtime.builtInFunctions["printInt"] = printIntFct;
  runtime.builtInFunctions["equalsInt"] = equalsIntFct;
  runtime.builtInFunctions["randomInt"] = randomIntFct;
  runtime.builtInFunctions["randomIntFromRange"] = randomIntFromRangeFct;
  runtime.builtInFunctions["largerThan"] = largerThanFct;
  runtime.builtInFunctions["smallerThan"] = smallerThanFct;
  runtime.builtInFunctions["smallerEqualsThan"] = smallerEqualsThanFct;
  runtime.builtInFunctions["largerEqualsThan"] = largerEqualsThanFct;
  runtime.builtInFunctions["intToString"] = intToStringFct;
  runtime.builtInFunctions["indexInParent"] = indexInParentFct;
  runtime.builtInFunctions["d"] = dFct;
  runtime.builtInFunctions["intToCharString"] = intToCharStringFct;


  // Number
  runtime.builtInFunctions["div"] = divFct;
  runtime.builtInFunctions["plus"] = plusFct;
  runtime.builtInFunctions["mult"] = multFct;
  runtime.builtInFunctions["minus"] = minusFct;
  runtime.builtInFunctions["splitNumberRandomly"] = splitNumberRandomlyFct;
  runtime.builtInFunctions["splitNumberRandomlyExceptOne"] = splitNumberRandomlyExceptOneFct;

  BuiltInFunction floorFct = (SLOTStackFrame frame, [List<SLOTObject> params]) {
    frame.pushData(new SLOTInteger((params[0] as SLOTInteger).val.floor()));
  };
  runtime.builtInFunctions["floor"] = floorFct;


  //SLOTHashMap
  runtime.builtInFunctions["m"] = mFct;
  runtime.builtInFunctions["mapGet"] = mapGetFct;
  runtime.builtInFunctions["g"] = mapGetFct;
  runtime.builtInFunctions["mGet"] = mapGetFct;
  runtime.builtInFunctions["mapPut"] = mapPutFct;
  runtime.builtInFunctions["p"] = mapPutFct;
  runtime.builtInFunctions["mPut"] = mapPutFct;
  runtime.builtInFunctions["mapValues"] = mapValuesFct;
  runtime.builtInFunctions["mapKeys"] = mapKeysFct;
  runtime.builtInFunctions["newHashMap"] = newHashMapFct;
  runtime.builtInFunctions["mapRemove"] = mapRemoveFct;
  runtime.builtInFunctions["mapEquals"] = mapEqualsFct;
  runtime.builtInFunctions["mapPairs"] = mapPairsFct;


  // Bool
  runtime.builtInFunctions["not"] = notFct;
  runtime.builtInFunctions["and"] = andFct;
  runtime.builtInFunctions["or"] = orFct;
  runtime.builtInFunctions["equalsBool"] = equalsBoolFct;
  runtime.builtInFunctions["randomBool"] = randomBoolFct;
  runtime.builtInFunctions["randomBoolTrueProb"] = randomBoolTrueProbFct;

}
BuiltInFunction mFct = (SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  SLOTHashMap map = new SLOTHashMap({});

  if(parameters == null || parameters.length == 0) {
    frame.pushData(map);
    return;
  }

  List<SLOTObject> elements = parameters;
  String currentKey = null;
  for(int i = 0; i < elements.length; i++) {
    if(i %2 == 0) {
      currentKey = (elements[i] as SLOTString).value;
    }
    else {
      map.map[currentKey] = elements[i];
    }
  }

  frame.pushData(map);
};
