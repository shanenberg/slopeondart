import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTBoolean.dart';
import '../../objects/SLOTList.dart';

/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction removeElementAtFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  return removeElementAtFct_DEBUG(frame, parameters);
};

removeElementAtFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  SLOTList list = parameters[0];
  int index = (parameters[1] as SLOTInteger).val - 1;

  SLOTObject element = list.elements[index];
  list.elements.removeAt(index);

  frame.pushData(element);
}