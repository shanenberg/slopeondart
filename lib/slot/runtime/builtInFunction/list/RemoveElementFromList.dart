import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTBoolean.dart';
import '../../objects/SLOTList.dart';
import '../../../../lib/parser/features/scannerelement/ScannerElement_TreeNode.dart';


/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction removeElementFromListFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  return removeElementFromListFct_DEBUG(frame, parameters);
};

removeElementFromListFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  SLOTObject element = parameters[0];
  SLOTList list = parameters[1];

  for(SLOTObject childElement in list.elements) {
    if(element == childElement) {
      list.elements.remove(element);
      frame.pushData(element);
      return;
    }
  }

  frame.pushData(element);
  return;
}