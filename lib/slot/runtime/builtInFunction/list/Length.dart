import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTBoolean.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTList.dart';
import '../../objects/SLOTString.dart';
import '../BuiltInFunction.dart';

BuiltInFunction lengthFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  try {
    frame.pushData(new SLOTInteger((parameters[0] as SLOTList).elements.length));
  } catch (e) {
    print(e);
    throw e;
  }
};

