import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTBoolean.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTList.dart';
import '../../objects/SLOTString.dart';
import '../BuiltInFunction.dart';

BuiltInFunction indexOfFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {

  var list = (parameters[0] as SLOTList).elements;
  var element = parameters[1];

  if (list.contains(element))
      frame.pushData(new SLOTInteger(list.indexOf(element)+1));
  else
    frame.pushData(new SLOTInteger(0));

};

