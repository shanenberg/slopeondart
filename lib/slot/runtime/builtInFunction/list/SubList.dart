import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTBoolean.dart';
import '../../objects/SLOTList.dart';
import '../../../../lib/parser/features/scannerelement/ScannerElement_TreeNode.dart';


/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction subListFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  return subListFct_DEBUG(frame, parameters);
};

subListFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  SLOTList list = parameters[0];
  SLOTInteger from = parameters[1];

  List<SLOTObject> subList = new List<SLOTObject>();
  for(int i = from.val; i<list.elements.length; i++) {
    subList.add(list.elements[i]);
  }

  SLOTList retSubList = new SLOTList(subList);
  frame.pushData(retSubList);
}