import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTBoolean.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTList.dart';
import '../../objects/SLOTString.dart';
import '../BuiltInFunction.dart';

BuiltInFunction addElementFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  try {
    (parameters[0] as SLOTList).elements.add(parameters[1] as SLOTObject);
  } catch (e) {
    frame.printStackFrame();
    print("hi");
    throw e;
  }

  frame.pushData(parameters[1]);
};

