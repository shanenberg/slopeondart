import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTBoolean.dart';
import '../../objects/SLOTList.dart';
import '../../../../lib/parser/features/scannerelement/ScannerElement_TreeNode.dart';


/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction listToStringFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  return listToStringFct_DEBUG(frame, parameters);
};

listToStringFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  SLOTList param = parameters[0];
  String theString = "[";
  for(int i = 0; i < param.elements.length; i++) {
    if(param.elements[i] is SLOTInteger) {
      if(i >= 1) {
        theString += ", ";
      }
      theString += (param.elements[i] as SLOTInteger).val.toString();
    }
    else {
      throw "BuiltInFunction listToString: ${param.elements[i].runtimeType} Not implemented yet";
    }
  }

  theString += "]";
  frame.pushData(new SLOTString(theString));

}