import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTFalse.dart';
import '../../objects/SLOTList.dart';
import '../../objects/SLOTTrue.dart';
import '../BuiltInFunction.dart';

BuiltInFunction isEmptyFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  isEmptyFctDebug(frame, parameters);
};

isEmptyFctDebug(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  SLOTList ret = parameters[0] as SLOTList;
  if(ret.elements.length>0)
    frame.pushData(SLOTFalse.instance);
  else
    frame.pushData(SLOTTrue.instance);
}