import '../../../../lib/parser/features/_null_/NULL_TreeNode.dart';
import '../../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTNull.dart';
import '../../objects/SLOTSLOPETreeNode.dart';
import '../BuiltInFunction.dart';
import '../../../../lib/parser/tree/SingleChildTreeNode.dart';
import '../../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTBoolean.dart';
import '../../objects/SLOTList.dart';

/**
 * DELETE can only be invoked below an OR or an NFOLD
 */

BuiltInFunction clearElementsFct =
    (SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  return clearElementsFct_DEBUG(frame, parameters);
};

clearElementsFct_DEBUG(SLOTStackFrame frame, [List<SLOTObject> parameters])
{
  SLOTList list = parameters[0];
  list.elements.clear();

  frame.pushData(SLOTNull.instance);
}