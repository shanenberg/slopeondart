import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTBoolean.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTList.dart';
import '../../objects/SLOTString.dart';
import '../BuiltInFunction.dart';

BuiltInFunction elementAtFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  frame.
    pushData((parameters[0] as SLOTList).elements[(parameters[1] as SLOTInteger).val-1]);
};

