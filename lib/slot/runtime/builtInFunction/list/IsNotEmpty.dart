import '../../SLOTObject.dart';
import '../../frames/SLOTStackFrame.dart';
import '../../objects/SLOTFalse.dart';
import '../../objects/SLOTInteger.dart';
import '../../objects/SLOTList.dart';
import '../../objects/SLOTSLOTNode.dart';
import '../../objects/SLOTString.dart';
import '../../objects/SLOTTreeNode.dart';
import '../../objects/SLOTTrue.dart';
import '../BuiltInFunction.dart';

BuiltInFunction isNotEmptyFct =
(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  isNotEmptyFctDebug(frame, parameters);
};

isNotEmptyFctDebug(SLOTStackFrame frame, [List<SLOTObject> parameters]) {
  SLOTList ret = parameters[0] as SLOTList;
  if(ret.elements.length>0)
    frame.pushData(SLOTTrue.instance);
  else
    frame.pushData(SLOTFalse.instance);
}