import '../lib/SLOTLibraryFileLoader.dart';
import '../lib/SLOTLibraryLoader.dart';
import 'SLOTProject.dart';
import 'dart:collection';

import '../node/SLOTROOTNode.dart';
import '../structure/SLOTFunctionFamily.dart';
import '../structure/SLOTSingleFunction.dart';
import 'SLOTObject.dart';
import 'SLOTRuntimeResult.dart';
import 'builtInFunction/BuiltInFunction.dart';
import 'frames/SLOTPathExpressionExecutionStackFrame.dart';
import 'frames/SLOTRootStackFrame.dart';
import 'frames/SLOTStackFrame.dart';
import 'objects/SLOTNull.dart';
import 'objects/SLOTTreeNode.dart';

import 'objects/SLOTLambdaFunction.dart';

class SLOTRuntime {

  SLOTProject project;
  SLOTLibraryLoader libraryLoader;


  int counter=0;



  void allSteps() {
    while(!this.hasFinished()) {
// If you dont know what happens....better print out the stack
//////   ...and set a breakpoint
//      print("**********");
//      this.printStack();
//this.stack.last.printStackFrame();
//print("DID SOMETHING");
      var stack = this.stack;
      this.nextStep();

//    if (this.stack.last.dataLength()==0 && maybeInteresting) {
//      print("HER");
//    }

    }
  }

  HashMap<String, BuiltInFunction> builtInFunctions = new HashMap<String, BuiltInFunction>();
  HashMap<String, SLOTFunctionFamily> availableFunctionFamilies = new HashMap<String, SLOTFunctionFamily>();
  HashMap<String, SLOTSingleFunction> availableFunctions = new   HashMap<String, SLOTSingleFunction>();

  SLOTRuntimeResult runFunction(String functionname, [List<SLOTObject> parameterObjects]) {
    SLOTRuntimeResult ret = new SLOTRuntimeResult();
    if (parameterObjects==null) parameterObjects = new List<SLOTObject>();

    this.pushFunction(this.stack.last, functionname, parameterObjects);

    this.allSteps();

    if (this.stack.last.dataLength()==0) {
      ret.result = SLOTNull.instance;
    } else {
      ret.result = this.stack.last.popData();
    }

    ret.resultTree = this.slotResultTree;
    ret.slopeTree = this.slopeTreeNodes[0];
    return ret;
  }

  SLOTRuntimeResult runPathExpressionFunctionCallFunction(String functionname, [List<SLOTObject> parameterObjects]) {
    SLOTRuntimeResult ret = new SLOTRuntimeResult();
    if (parameterObjects==null) parameterObjects = new List<SLOTObject>();

    int oldStackSize = this.stack.length;

    // Neuer StackFrame für PathExpressions
    var myFrame = new SLOTPathExpressionExecutionStackFrame(this.stack[0]);
    this.stack.add(myFrame);

    // Neuer StackFrame für PathExpressions
//    this.stack.add(new SLOTPathExpressionExecutionStackFrame(this.stack[0]));


    this.pushFunction(this.stack.last, functionname, parameterObjects);

    while(this.stack.last != myFrame) {
      stack.last.nextStep();
    }

    ret.result = this.stack.last.peekData();
    stack.removeLast();

    ret.resultTree = this.slotResultTree;
    ret.slopeTree = this.slopeTreeNodes[0];
    return ret;
  }

  addBuiltInFunction(String name, BuiltInFunction block) {
    builtInFunctions[name] = block;
  }

  List<SLOTStackFrame> stack = new List<SLOTStackFrame>();

  SLOTRuntime([SLOTLibraryLoader aLibraryLoader]) {
    stack.add(new SLOTRootStackFrame(this));
    if(aLibraryLoader==null) {
      this.libraryLoader = new SLOTLibraryFileLoader();
    } else {
      this.libraryLoader = aLibraryLoader;
    }


    loadBuiltInFunctions(this);
    loadLibraries();
  }

  loadLibraries() {
    this.libraryLoader.loadAllLibraries();
  }


  List<SLOTTreeNode> slopeTreeNodes = new List<SLOTTreeNode>(); // the real slope treeNode
  SLOTTreeNode slotResultTree = new SLOTROOTNode(); // the node that is created by the program

  SLOTObject result;

  void nextStep() {

    stack.last.nextStep();
  }


  /**
   * Creates from chain of functionFamily names a list of SLOTFunctionFamilies
   */
  List<SLOTFunctionFamily> createChainedFunction(List<String> treeFunctionNames) {
    List<SLOTFunctionFamily> functionList = new List<SLOTFunctionFamily>();
    for(String familyName in treeFunctionNames) {
      if(!availableFunctionFamilies.containsKey(familyName))
        throw "Unknown function: " + familyName;
      functionList.add(availableFunctionFamilies[familyName]);
    }
    return functionList;
  }

  bool hasFinished() {
    return stack.length==1;
  }

  void pushFunction(
      SLOTStackFrame frame,
      String functionName,
      List<SLOTObject> parameterObjects) {

      if (this.builtInFunctions.containsKey(functionName)) {
        // I just invoke a builtInFunction....
       // print(functionName);
        this.builtInFunctions[functionName](frame, parameterObjects);
        return;
      } else if(this.availableFunctions.containsKey(functionName)){
        // Push the function on the stack
        availableFunctions[functionName].pushOnStack(frame, parameterObjects);
        return;
      } else if (frame.hasVariable(functionName)) {
        SLOTObject aPotentialLambdaFunction = frame.getVariable(functionName);
        SLOTLambdaFunction f;
        if (aPotentialLambdaFunction is SLOTLambdaFunction) {
          (aPotentialLambdaFunction as SLOTLambdaFunction).pushOnStack(frame, parameterObjects);
          return;
        }
      }
      throw "no such function: " + functionName;
  }

//  SLOTTreeFunctionStackFrame createFamilyFunctionFrame(SLOTStackFrame frame, List<String> functionNames, List<SLOTObject> parameterObjects) {
//
//    SLOTRuntime runtime = frame.getRuntime();
//    List<SLOTFunctionFamily> functionFamily
//      = this.createChainedFunction(functionNames);
//
//    return new SLOTTreeFunctionStackFrame(runtime, functionFamily);
//  }


  void addFunction(SLOTSingleFunction function) {
    availableFunctions[function.name] = function;
  }

  void addTreeFunction(SLOTFunctionFamily functionFamily) {
    availableFunctionFamilies[functionFamily.name] = functionFamily;
  }

  void printStack() {
    for(SLOTStackFrame e in this.stack) {
      print("StackFrame");
      e.printStackFrame();
    }
  }

  SLOTObject getSLOTRootResultNode() {
    return slotResultTree;
  }

  void redrawAll() {
    if(project!=null)
      project.doRedrawInProject();
    else
      print("PROJECT==NULL");
  }

  void emptyStack() {
    var e = stack.first;
    if (e.expressionLength()>0) {
      e.expressionsClear();
      e.dataClear();
    }
    stack = new List<SLOTStackFrame>();
    stack.add(e);
  }

}