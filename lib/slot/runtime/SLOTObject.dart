abstract class SLOTObject {
//  String debug_SLOTTreeString();
  String valueToString();

  SLOTObject clone();
}