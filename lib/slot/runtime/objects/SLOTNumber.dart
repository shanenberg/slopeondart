import '../SLOTObject.dart';
import 'SLOTBoolean.dart';
import 'SLOTDouble.dart';
import 'SLOTInteger.dart';

abstract class SLOTNumber extends SLOTObject {
  num get value;

  static SLOTNumber fromValue(num val) {
    if(val is int) {
      return new SLOTInteger(val);
    }
    else if(val is double) {
      return new SLOTDouble(val);
    }
    else {
      throw "${val.runtimeType} has no corresponding SLOTNumber";
    }
  }


  SLOTBoolean largerThan(SLOTNumber i) {
    return SLOTBoolean.fromBool(value>i.value);
  }

  SLOTBoolean equals(SLOTNumber i) {
    return SLOTBoolean.fromBool(value==i.value);
  }

  SLOTBoolean smallerThan(SLOTNumber i) {
    return SLOTBoolean.fromBool(value<i.value);
  }

  SLOTBoolean smallerEqualsThan(SLOTNumber i) {
    return SLOTBoolean.fromBool(value<=i.value);
  }


  SLOTBoolean largerEqualsThan(SLOTNumber i) {
    return SLOTBoolean.fromBool(value>=i.value);
  }

}