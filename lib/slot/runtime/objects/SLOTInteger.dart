import '../SLOTObject.dart';
import 'SLOTBoolean.dart';
import 'SLOTNumber.dart';

class SLOTInteger extends SLOTNumber {
  int val;

  SLOTInteger(this.val);

  SLOTBoolean largerThan(SLOTInteger i) {
    return SLOTBoolean.fromBool(val>i.val);
  }

  SLOTObject equals(SLOTInteger i) {
    return SLOTBoolean.fromBool(val==i.val);
  }

  SLOTObject smallerThan(SLOTInteger i) {
    return SLOTBoolean.fromBool(val<i.val);
  }

  SLOTBoolean smallerEqualsThan(SLOTInteger i) {
    return SLOTBoolean.fromBool(val<=i.val);
  }


  SLOTBoolean largerEqualsThan(SLOTInteger i) {
    return SLOTBoolean.fromBool(val>=i.val);
  }

  @override
  String valueToString() {
    return val.toString();
  }

  SLOTInteger clone() {
    return new SLOTInteger(val);
  }

  @override
  Object get value => val;
}