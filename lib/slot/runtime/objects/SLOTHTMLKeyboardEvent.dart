import '../SLOTObject.dart';
import 'dart:html';

Map<String, Function> _keyboardEventAttributes = {
    "key" : _getKey,
    "ctrlKey" : _getCtrlKey,
    "altKey" : _getAltKey,
    "shiftKey": _getShiftKey
};
_getKey(KeyboardEvent event) => event.key;
_getCtrlKey(KeyboardEvent event) => event.ctrlKey;
_getAltKey(KeyboardEvent event) => event.altKey;
_getShiftKey(KeyboardEvent event) => event.shiftKey;

class SLOTHTMLKeyboardEvent extends SLOTObject {

  KeyboardEvent event;
  SLOTHTMLKeyboardEvent(this.event);

  accessAttribute(String attributeName) {
    return _keyboardEventAttributes[attributeName](event);
  }


  @override
  String valueToString() {
    throw "not yet implemented";
    // TODO: implement valueToString
  }
  @override
  SLOTObject clone() {
    throw "not yet implemented";
    // TODO: implement clone
  }
}