import '../SLOTObject.dart';

class SLOTString extends SLOTObject {

  String value;

  String debug_SLOTTreeString() {
    return value;
  }

  SLOTString(this.value) {
    if (value==null) {
      throw "SLOTSTring with value null passed";
    }
  }

  bool equals(SLOTObject o) {
    return value == (o as SLOTString).value;
  }
  @override
  String valueToString() {
    return value;
  }

  @override
  SLOTObject clone() {
    return new SLOTString(value);
    // TODO: implement clone
  }
}