import 'SLOTString.dart';
import 'SLOTTreeNode.dart';
import 'dart:html';

class SLOTHTMLElement extends SLOTTreeNode {
  Element htmlElement;

  SLOTHTMLElement(this.htmlElement):super() {
    if (this.htmlElement!=null)
      this.value = new SLOTString(htmlElement.toString());
  }

  @override
  String valueToString() {
    return htmlElement.toString();
  }
}