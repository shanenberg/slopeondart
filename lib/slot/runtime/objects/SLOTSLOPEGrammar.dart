import '../SLOTObject.dart';
import '../../../lib/grammarparser/tree/GrammarObject.dart';
import 'SLOTString.dart';
import 'SLOTTreeNode.dart';

class SLOTSLOPEGrammar extends SLOTObject {

  SLOPEGrammarObject grammarObject;
  SLOTSLOPEGrammar(this.grammarObject);

  SLOTTreeNode parseWord(SLOTString word) {
    return grammarObject.parseString(word.value).toSLOTTreeNode();
  }

  @override
  String valueToString() {
    // TODO: implement valueToString
  }

  SLOTSLOPEGrammar clone() {
    throw "Not yet implemented";
  }
}