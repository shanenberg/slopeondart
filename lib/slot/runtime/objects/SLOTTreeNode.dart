import '../SLOTObject.dart';
import 'SLOTList.dart';
import 'SLOTHashMap.dart';
import 'SLOTNull.dart';
import 'SLOTString.dart';

/**
 * Objects of this class are representatives for SLOPE TreeNode objects
 * in the runtime of SLOT
 */
class SLOTTreeNode extends SLOTObject{
  SLOTTreeNode parent;
  SLOTList children = new SLOTList(new List());
  SLOTObject value;

  SLOTTreeNode origin;


  SLOTHashMap extraData = new SLOTHashMap({});

  bool hasExtras() => extraData.map.isNotEmpty;

  SLOTObject getExtra(String name) {
    var extra = extraData.map[name];
    if(extra == null)
      return SLOTNull.instance;
    else
      return extra;
  }

  void putExtra(String name, SLOTObject extra) {
    extraData.map[name] = extra;
  }


  c(int index) => children.elements[0];

  SLOTTreeNode();

  SLOTTreeNode.fromName(String name) {
    value = new SLOTString(name);
  }

  String debug_SLOTTreeString() {

    String ret = this.valueToString() + "(";

    for(SLOTObject child in children.elements) {
      ret = ret + (child as SLOTTreeNode).debug_SLOTTreeString();
    }

    ret = ret + ")";
    return ret;
  }

  @override
  String valueToString() {
    return "${value.valueToString()}";
  }

  refNumberOfChild(SLOTTreeNode n) {
    for(int i=0; i<children.elements.length;i++) {
      if (n==children.elements[i]) {
        return i;
      }
    }
    throw "not a chile of this node..";
  }

  addChild(SLOTTreeNode n) {
    this.children.elements.add(n);
    n.parent=this;
  }

  setChildAt(int index, SLOTTreeNode n) {
    this.children.elements[index] = n;
    n.parent = this;
  }

  insertChildAt(int index, SLOTTreeNode n) {
    this.children.elements.insert(index, n);
    n.parent = this;
  }

  removeChildAt(int index) {
    this.children.elements.removeAt(index);
  }




  @override
  SLOTTreeNode clone() {
    return this.cloneWithParent(null);
  }

  SLOTTreeNode cloneWithParent(SLOTTreeNode clonedParent) {
    SLOTTreeNode cloned = new SLOTTreeNode();
    cloned.value = value.clone();
    cloned.parent = clonedParent;
    cloned.origin = this;
    for(SLOTTreeNode child in children.elements) {
      cloned.children.elements.add(child.cloneWithParent(cloned));
    }

    return cloned;
  }



}