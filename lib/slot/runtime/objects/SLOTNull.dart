import '../SLOTObject.dart';

class SLOTNull extends SLOTObject {
  static SLOTNull instance = new SLOTNull();
  @override

  @override
  String valueToString() {
    return "NULL";
  }


  @override
  SLOTObject clone() {
    // TODO: implement clone
    return this;
    //throw "can't clone null";
  }
}