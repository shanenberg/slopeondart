import '../SLOTObject.dart';
import 'SLOTBoolean.dart';
import 'SLOTFalse.dart';

class SLOTTrue extends SLOTBoolean {
  static SLOTTrue instance = new SLOTTrue();
  @override
  String debug_SLOTTreeString() {
    return "true";
  }
  @override
  SLOTBoolean not() {
    return SLOTFalse.instance;
  }
  @override
  SLOTObject and(SLOTBoolean slotBoolean) {
    return slotBoolean;
  }
  @override
  SLOTObject or(SLOTBoolean slotBoolean) {
    return this;
  }
  @override
  String valueToString() {
    return "true";
  }
  @override
  bool boolValue() {
    return true;
  }
  @override
  SLOTBoolean equals(SLOTBoolean slotBoolean) {
    return SLOTBoolean.fromBool(slotBoolean == instance);
  }

  SLOTBoolean clone() {
    return instance;
  }
}