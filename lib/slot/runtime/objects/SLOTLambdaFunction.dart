import '../../../slot/runtime/SLOTObject.dart';
import '../../../slot/runtime/SLOTRuntime.dart';
import '../../../slot/runtime/frames/SLOTSingleFunctionStackFrame.dart';
import '../../../slot/runtime/frames/SLOTStackFrame.dart';
import '../../../slot/structure/SLOTSingleFunction.dart';
import '../../../slot/structure/SLOTBlock.dart';
import '../../../slot/runtime/frames/SLOTLambdaStackFrame.dart';

class SLOTLambdaFunction extends SLOTSingleFunction with SLOTObject {

  SLOTStackFrame context;

  void pushOnStack(SLOTStackFrame frame, List<SLOTObject> parameterObjects) {
    SLOTRuntime runtime = frame.getRuntime();
    SLOTLambdaStackFrame newFunctionFrame =
      new SLOTLambdaStackFrame(runtime, this);

    newFunctionFrame.currentSourceSLOTNodes = frame.currentSourceSLOTNodes;
    newFunctionFrame.currentResultSLOTNode = frame.currentResultSLOTNode;

    // Set parameters for function
    for(int i=0; i<this.parameterNames.length;i++) {
      try {
        newFunctionFrame.variables[this.parameterNames[i]]=parameterObjects[i];
      } catch (e) {
        print(e);
        throw e;
      }
    }

    runtime.stack.add(newFunctionFrame);
    runtime.stack.last.pushExpression(this.body.shallowCopy());
  }

  SLOTLambdaFunction(this.context, List<String> parameterNames, SLOTBlock body):
    super(null, parameterNames, body);

  List<String> parameterNames = new List<String>();

  @override
  String valueToString() {
    String ret = "function [";

    ret = ret + "]";
    return ret;
  }

  SLOTLambda clone() {
    SLOTLambda lol = new SLOTLambda([]);
    for(String element in parameterNames) {
      lol.parameterNames.add(element);
    }

    return lol;

  }

}