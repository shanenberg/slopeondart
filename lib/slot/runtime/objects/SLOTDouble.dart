import '../SLOTObject.dart';
import 'SLOTBoolean.dart';
import 'SLOTNumber.dart';

class SLOTDouble extends SLOTNumber {
  double val;

  SLOTDouble(this.val);



  @override
  String valueToString() {
    return val.toString();
  }

  SLOTDouble clone() {
    return new SLOTDouble(val);
  }
  // TODO: implement value
  @override
  Object get value => val;
}