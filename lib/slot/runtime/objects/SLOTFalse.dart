import '../SLOTObject.dart';
import 'SLOTBoolean.dart';
import 'SLOTTrue.dart';

class SLOTFalse extends SLOTBoolean {
  static SLOTFalse instance = new SLOTFalse();

  @override
  String valueToString() {
    return "false";
  }

  @override
  SLOTBoolean not() {
    return SLOTTrue.instance;
  }
  @override
  SLOTObject and(SLOTBoolean slotBoolean) {
    return this;
  }

  SLOTObject or(SLOTBoolean slotBoolean) {
    return slotBoolean;
  }

  @override
  bool boolValue() {
    return false;
  }


  @override
  SLOTBoolean equals(SLOTBoolean slotBoolean) {
    return SLOTBoolean.fromBool(slotBoolean == instance);
  }

  SLOTBoolean clone() {
    return instance;
  }
}