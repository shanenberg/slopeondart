import '../../../lib/parser/features/scannerelement/ScannerElement_TreeNode.dart';
import 'SLOTSLOPETreeNode.dart';
import 'SLOTString.dart';
import 'SLOTTreeNode.dart';
import '../../../lib/parser/tree/TreeNode.dart';
import '../../../lib/util/RegexGrammar.dart';
import 'SLOTList.dart';

/**
 * A TreeNodeLeaf is a Terminal symbol
 */
class SLOTSLOPETreeNodeLeaf extends SLOTSLOPETreeNode {
  ScannerElement_TreeNode slopeTreeNode;

  SLOTSLOPETreeNodeLeaf(ScannerElement_TreeNode treeNode):super(null) {
    this.slopeTreeNode = treeNode;

    this.value = new SLOTString("SLOPETOKEN");
    this.children = new SLOTList([new SLOTTreeNode() ..value = new SLOTString(treeNode.scannedWord)
                                  ..parent = this]);

  }
/*
  SLOTSLOPETreeNodeLeaf(ScannerElement_TreeNode treeNode):super(null) {
    slopeTreeNode = treeNode;
    this.value = new SLOTString("REGEX");

    try {
      TreeNode regexParseTree = createRegexParseTree(treeNode);
      SLOTTreeNode tn3 = regexParseTree.toSLOTTreeNodeWithoutRegex();
      //tn3.value = new SLOTString(slopeTreeNode.keyword);
      this.children.elements.add(tn3);
      tn3.parent = this;

    } catch(e) {
      //throw(e);
      SLOTTreeNode tn3 = new SLOTTreeNode();
      tn3.value = new SLOTString("ERROR");
      this.children.elements.add(tn3);
      tn3.parent = this;

    }
  }
*/

/*
  @override
  String debug_SLOTTreeString()
  {
    String abcd =  value.valueToString() + "(";
    abcd += value.valueToString();
    abcd += ")";

  }
*/
  @override
  String valueToString() {
    return slopeTreeNode.slotTypeName();
  }
}

class SLOTSLOPETreeNodeLeafWithoutRegex extends SLOTSLOPETreeNode {
  ScannerElement_TreeNode slopeTreeNode;

  SLOTSLOPETreeNodeLeafWithoutRegex(ScannerElement_TreeNode treeNode):super(null) {

    slopeTreeNode = treeNode;
    Match match = new RegExp(r"\[(.)-(.)\]").firstMatch(slopeTreeNode.keyword);
    if(match != null) {

      this.value = new SLOTString("GROUP");

      if(treeNode.scannedWord != "#") {
        SLOTTreeNode valueParent = new SLOTTreeNode();
        valueParent.value = new SLOTString("TOKEN");
        this.addChild(valueParent);

        SLOTTreeNode chosenValue = new SLOTTreeNode();
        chosenValue.value = new SLOTString(treeNode.scannedWord);
        valueParent.addChild(chosenValue);
      } else {
        SLOTTreeNode nullValue = new SLOTTreeNode();
        nullValue.value = new SLOTString("NULL");
        this.addChild(nullValue);
      }

      SLOTTreeNode beginning = new SLOTTreeNode();
      beginning.value = new SLOTString(match.group(1).replaceAll("\\", ""));
      this.addChild(beginning);

      SLOTTreeNode end = new SLOTTreeNode();
      end.value = new SLOTString(match.group(2).replaceAll("\\", ""));
      this.addChild(end);




    }
    else if(slopeTreeNode.keyword.startsWith("(^")) {
      this.value = new SLOTString("ANY_EXCEPT");


      if(treeNode.scannedWord != "#") {
        SLOTTreeNode valueParent = new SLOTTreeNode();
        valueParent.value = new SLOTString("TOKEN");
        this.addChild(valueParent);

        SLOTTreeNode chosenValue = new SLOTTreeNode();
        chosenValue.value = new SLOTString(treeNode.scannedWord);
        valueParent.addChild(chosenValue);
      } else {
        SLOTTreeNode nullValue = new SLOTTreeNode();
        nullValue.value = new SLOTString("NULL");
        this.addChild(nullValue);
      }

      SLOTTreeNode exceptGroup = new SLOTTreeNode();
      exceptGroup.value = new SLOTString("EXCEPT");
      this.addChild(exceptGroup);

      for(int i = 2; i < slopeTreeNode.keyword.length-1; i++) {
        SLOTTreeNode except = new SLOTTreeNode();
        except.value = new SLOTString(slopeTreeNode.keyword.substring(i, i+1));
        exceptGroup.addChild(except);
      }

    }
    else {
      this.value = new SLOTString("SLOPETOKEN");

      SLOTTreeNode tn = new SLOTTreeNode();
      tn.value = new SLOTString(slopeTreeNode.scannedWord);
      this.children.elements.add(tn);
      tn.parent = this;

      SLOTTreeNode tn2 = new SLOTTreeNode();
      tn2.value = new SLOTString(slopeTreeNode.keyword);
      this.children.elements.add(tn2);
      tn2.parent = this;

      SLOTTreeNode tn3 = new SLOTTreeNode();
      tn3.value = new SLOTString("STOP");
      this.children.elements.add(tn3);
      tn3.parent = this;
    }

  }

/*
  @override
  String debug_SLOTTreeString()
  {
    String abcd =  value.valueToString() + "(";
    abcd += value.valueToString();
    abcd += ")";

  }
*/
  @override
  String valueToString() {
    return slopeTreeNode.slotTypeName();
  }
}