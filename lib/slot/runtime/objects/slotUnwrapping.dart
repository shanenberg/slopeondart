import '../SLOTObject.dart';
import 'SLOTBoolean.dart';
import 'SLOTNumber.dart';
import 'SLOTString.dart';
import 'SLOTNull.dart';
import 'SLOTList.dart';
import 'SLOTHashMap.dart';
import 'SLOTInteger.dart';
import 'SLOTDouble.dart';
import 'dart:collection';



unwrapSLOTObject(SLOTObject obj) {
  if(obj is SLOTBoolean) {
    return obj.boolValue();
  }
  else if(obj is SLOTNumber) {
    return obj.value;
  }
  else if(obj is SLOTString) {
    return obj.value;
  }
  else if(obj is SLOTNull) {
    return null;
  }
  else if(obj is SLOTList) {
    var list = [];
    for(var child in obj.elements) {
      list.add(unwrapSLOTObject(child));
    }
    return list;
  }
  else if(obj is SLOTHashMap) {
    var map = {};
    for(var key in obj.map.keys) {
      map[key] = unwrapSLOTObject(obj.map[key]);
    }
  }
  else {
    return obj;
  }
}

wrapSLOTObject(Object obj) {
  if(obj is bool) {
    return SLOTBoolean.fromBool(obj);
  }
  else if(obj is int) {
    return new SLOTInteger(obj);
  }
  else if(obj is double) {
    return new SLOTDouble(obj);
  }
  else if(obj is String) {
    return new SLOTString(obj);
  }
  else if(obj == null) {
    return SLOTNull.instance;
  }
  else if(obj is List) {
    return new SLOTList(
            new List.generate(obj.length, (int index) {
              return wrapSLOTObject(obj[index]);
            })
           );
  }
  else if(obj is Map<String, Type>) {
    var map = new HashMap<String, SLOTObject>();
    obj.forEach((String a, b) {
      map[a] = wrapSLOTObject(b);
    });
    return new SLOTHashMap(map);
  }
  else {
    return obj;
  }
}