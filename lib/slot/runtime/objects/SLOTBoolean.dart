import '../SLOTObject.dart';
import 'SLOTFalse.dart';
import 'SLOTTrue.dart';

abstract class SLOTBoolean extends SLOTObject {




  SLOTBoolean not();
  SLOTObject and(SLOTBoolean slotBoolean);
  SLOTObject or(SLOTBoolean slotBoolean);

  static SLOTBoolean fromBool(bool aBool) {
    if (aBool)
      return SLOTTrue.instance;
    else
      return SLOTFalse.instance;
  }

  SLOTBoolean equals(SLOTBoolean slotBoolean);

  bool boolValue();


}