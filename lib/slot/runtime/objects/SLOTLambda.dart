import '../SLOTObject.dart';
import 'SLOTBoolean.dart';
import 'SLOTFalse.dart';
import 'SLOTTreeNode.dart';

class SLOTLambda extends SLOTObject {

  List<String> parameterNames = new List<String>();

  @override
  String valueToString() {
    String ret = "function [";

    ret = ret + "]";
    return ret;
  }


  SLOTLambda(this.parameterNames);

  SLOTLambda clone() {
    SLOTLambda lol = new SLOTLambda([]);
    for(String element in parameterNames) {
      lol.parameterNames.add(element);
    }

    return lol;

  }

}