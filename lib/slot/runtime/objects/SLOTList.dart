import '../SLOTObject.dart';
import 'SLOTBoolean.dart';
import 'SLOTFalse.dart';
import 'SLOTTreeNode.dart';

class SLOTList extends SLOTObject{

  List<SLOTObject> elements = new List<SLOTObject>();

  @override
  String valueToString() {
    String ret = "[";

    if (elements.length>0) {
      ret = ret + elements[0].valueToString();
      for(int i =1; i<elements.length;i++) {
        ret = ret + "," + elements[i].valueToString();
      }
    }

    ret = ret + "]";
    return ret;
  }
  @override
  SLOTBoolean not() {
    return SLOTFalse.instance;
  }
  @override
  SLOTObject and(SLOTBoolean slotBoolean) {
    return slotBoolean;
  }
  @override
  SLOTObject or(SLOTBoolean slotBoolean) {
    return this;
  }

  SLOTList(this.elements);

  List<SLOTTreeNode> asSLOTTreeNodeList() {
    List ret = new List<SLOTTreeNode>();
    for(SLOTObject o in elements) {
      try {
        ret.add(o as SLOTTreeNode);
      } catch(e) {
        print("lal");
        throw(e);
      }
    }
    return ret;
  }


  SLOTList clone() {
    SLOTList lol = new SLOTList([]);
    for(SLOTObject element in elements) {
      lol.elements.add(element.clone());
    }

    return lol;

  }
}