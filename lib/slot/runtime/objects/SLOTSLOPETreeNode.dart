import '../../../lib/parser/tree/TreeNode.dart';
import 'SLOTString.dart';
import 'SLOTTreeNode.dart';
import '../../../lib/parser/features/nfold/NFOLD_TreeNode.dart';
import '../../../lib/parser/features/bracket/BRACKET_TreeNode.dart';
import '../../../lib/parser/tree/MultiChildrenNode.dart';
import 'SLOTHashMap.dart';

/**
 * Objects of this class are representatives for SLOPE TreeNode objects
 * in the runtime of SLOT
 */
class SLOTSLOPETreeNode extends SLOTTreeNode {
  TreeNode slopeTreeNode;


  SLOTSLOPETreeNode(this.slopeTreeNode):super() {
    if (this.slopeTreeNode!=null)
      this.value = new SLOTString(valueToString());
  }

  @override
  String valueToString() {
    return slopeTreeNode.slotTypeName();
  }

  SLOTSLOPETreeNode createTemplate() {
    TreeNode newNode = slopeTreeNode.createTemplate();
    return newNode.toSLOTTreeNode();
  }

  SLOTSLOPETreeNode createChildTemplate() {
    TreeNode newNode = (slopeTreeNode as NFOLD_TreeNode).createChildTemplate();
    return newNode.toSLOTTreeNode();
  }

  @override
  SLOTTreeNode clone() {
    return this.cloneWithParent(null);
  }

  SLOTTreeNode cloneWithParent(SLOTTreeNode clonedParent) {
    SLOTTreeNode cloned = new SLOTSLOPETreeNode(this.slopeTreeNode);
    cloned.value = value.clone();
    cloned.parent = clonedParent;
    cloned.origin = this;
    for(SLOTTreeNode child in children.elements) {
      cloned.children.elements.add(child.cloneWithParent(cloned));
    }

    return cloned;
  }

}