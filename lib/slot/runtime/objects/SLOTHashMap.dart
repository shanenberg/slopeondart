import '../SLOTObject.dart';
import 'dart:collection';

import '../builtInFunction/ANY/equals.dart';

import 'SLOTNull.dart';

class SLOTHashMap extends SLOTObject {
  HashMap<String, SLOTObject> map = new HashMap<String, SLOTObject>();

  SLOTHashMap(this.map);



  SLOTObject getValue(String key) {
    var value = map[key];
    if(value == null)
      return SLOTNull.instance;
    else
      return value;
  }

  void putValue(String key, SLOTObject value) {
    map[key] = value;
  }


  @override
  String valueToString() {

    String ret = "{";
    map.forEach((String key, SLOTObject value) {
      ret += "\n$key : ${value.valueToString()}\n";
    });
    ret += "}";
    return ret;
  }


  @override
  SLOTObject clone() {
    // TODO: implement clone
    HashMap<String, SLOTObject> clonedMap = new HashMap<String, SLOTObject>();

    map.forEach((String key, SLOTObject obj) {
      clonedMap[key] = obj.clone();
    });

    return new SLOTHashMap(clonedMap);
  }


  bool eq(SLOTObject par) {
    if(!(par is SLOTHashMap)) {
      return false;
    }
    SLOTHashMap map2 = par;
    var mapKeySet = map.keys.toSet();
    var mapKeySet2 = map2.map.keys.toSet();
    if(mapKeySet.containsAll(mapKeySet2)
      && mapKeySet2.containsAll(mapKeySet)) {
      bool failed = false;
      map.forEach((String key, SLOTObject obj) {
        var f2 = false;
        if(obj is SLOTHashMap) {
          f2 = obj.eq(map2.map[key]);
        }
        else {
          f2 = equalsSlot(obj, map2.map[key]);
        }


        if(!f2) failed = true;
      });

      if(failed) return false;
    }
    return true;
  }
}