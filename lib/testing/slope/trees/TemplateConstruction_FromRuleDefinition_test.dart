import '../../../lib/parser/features/and/AND_TreeNode.dart';
import 'package:test/test.dart';

import '../../../lib/grammarparser/GrammarParser.dart';
import '../../../lib/grammarparser/tree/GrammarObject.dart';
import '../../../lib/parser/ResultObject.dart';
import '../../../lib/parser/parserconstruction/Rule.dart';
import '../../../lib/parser/tree/TreeNode.dart';

TreeNode s(String grammarString, String templateString) {
  ResultObject<SLOPEGrammarObject> result = new ResultObject<SLOPEGrammarObject>();
  var g = new GrammarParser();
  g.parseString("AGr Rules: s " + grammarString, result);
  Rule pc = result.getResult().createParser()
      .Start(new ResultObject());

  TreeNode treeNode =     pc.createTemplate();
treeNode.createTemplate();
  expect(treeNode.treeLangNodeString(), templateString);

  // Now create template on the template itself -- nothing should change.
  AND_TreeNode n = treeNode.parserConstructor.createTemplate();
  expect(treeNode.parserConstructor.createTemplate().treeLangNodeString(), templateString);
}

main() {
  test("Template Construction", () {
    s('s => (A|"B")*. A => "A".', 'AND(-s-AND(NFOLD()))');
    s('s=> "A".', 'AND(-s-AND(A))');
    s('s=> "A" "B".', 'AND(-s-AND(AB))');
    s('s=> r. r=> "A".', 'AND(-s-AND(AND(-r-AND(A))))');
    s('s=> "A"?.', 'AND(-s-AND(OPT(***null***)))');
    s('s=> "A"*.', 'AND(-s-AND(NFOLD()))');
    s('s=> "A"+.', 'AND(-s-AND(NFOLD(A)))');
    s('s=> "A" | "B".', 'AND(-s-OR(#-1#NULL))');
    s('s=> "A" | "B" | "C".', 'AND(-s-OR(#-1#NULL))');
    s('s=> r1 | r2. r1=>"A". r2=>"B".', 'AND(-s-OR(#-1#NULL))');
    s('s=> r1 | r2. r1=>"A". r2=>"B".', 'AND(-s-OR(#-1#NULL))');
    s('s=> r1 (r1 | r2). r1=>"A". r2=>"B".', 'AND(-s-AND(AND(-r1-AND(A))OR(#-1#NULL)))');
    s('s=> r1 r1 (r1 | r2). r1=>"A". r2=>"B".',
        'AND(-s-AND(AND(-r1-AND(A))AND(-r1-AND(A))OR(#-1#NULL)))');
//    s('s=> r1 r2 (r1 | r2). r1=>"A". r2=>"B".', 'AND(-s-AND(-r1-A)AND(-r2-B)BRACKET(OR(#-1#NULL)))');
//    s('s=> ("A" | "B")+.', 'AND(-s-NFOLD(BRACKET(OR(#-1#NULL))))');
//    s('s=> (("A" | "B")+)+.', 'AND(-s-NFOLD(BRACKET(AND(NFOLD(BRACKET(OR(#-1#NULL)))))))');
//    s('s=> ("A"?)+.', 'AND(-s-NFOLD(BRACKET(AND(OPT(***null***)))))');



  });


  test("Template Construction after Template Construction", () {
    s('s=> "A".', 'AND(-s-AND(A))');
  });
}