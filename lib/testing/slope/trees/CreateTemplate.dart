import '../../../lib/parser/features/or/OR_TreeNode.dart';
import '../../../lib/parser/features/or/OR_ParserConstructor.dart';
import '../../../lib/parser/ResultObject.dart';
import 'package:test/test.dart';

import '../../../slot/runtime/objects/SLOTString.dart';
import '../../../slot/runtime/objects/SLOTTreeNode.dart';
import '../../../lib/dynamicParser/DynamicParser.dart';
import '../../../lib/grammarparser/GrammarParser.dart';
import '../../../lib/grammarparser/tree/GrammarObject.dart';
import '../../../lib/parser/tree/TreeNode.dart';
import '../../../slot/runtime/objects/SLOTSLOPETreeNode.dart';

createTree(grammarString, word) {
  var slopeParser = new GrammarParser();
  ResultObject<SLOPEGrammarObject> result = new ResultObject<SLOPEGrammarObject>();
  slopeParser.parseString(grammarString, result);
  SLOPEGrammarObject grammar = result.getResult();

  DynamicParser dynamicParser = new DynamicParser(grammar);

  TreeNode ret = dynamicParser.parseString(word, null);

  return ret;
}

main() {
  // (a | b) & c
  var slopeString = 'ABExample Rules: X X => (A|"B")*. A => "A".';
  var word = 'A';

  test("SLOPEParser - CreateTemplate - ", (){
    TreeNode sourceTreeNode = createTree(slopeString, word);

    SLOTSLOPETreeNode n = sourceTreeNode.toSLOTTreeNode();
    SLOTSLOPETreeNode nfold_n = (n.children.elements[0] as SLOTSLOPETreeNode).createChildTemplate();
    OR_TreeNode or = nfold_n.slopeTreeNode;


    expect(or.parserConstructor is OR_ParserConstructor, true);
    var left = (or.parserConstructor as OR_ParserConstructor).parserConstructors[0].createTemplate();
    var right = (or.parserConstructor as OR_ParserConstructor).parserConstructors[1].createTemplate();

    expect(left.treeLangNodeString(), 'AND(AND(-A-A))');
    expect(right.treeLangNodeString(), 'AND(B)');

  });


}