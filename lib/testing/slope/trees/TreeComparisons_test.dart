import 'package:test/test.dart';

import '../../../lib/treelang/TreeComparator.dart';

/*
  Checks whether the SLOPE parse trees look as expectedd
 */
T(String ruleStart, String word, String treeString) {
  String prefix = "ABExample Rules: s ";
  TreeComparator.doComparison(prefix+ruleStart, word, treeString);
}

main() {
  String prefix = "ABExample Rules: s ";

  test("TreeComparison ", () {

//  T('s=> r. r=>"A".', "A", "AND(RULE(-r-AND(-A))");
  T('s=> "A".', "A", "AND(-s-AND(A))");

  T('s=> ("A").', "A", "AND(-s-AND(BRACKET(AND(A))))");
  T('s=> "A" "B".', "AB", "AND(-s-AND(AB))");
  T('s=> "A" | "B".', "A", "AND(-s-OR(#0#AND(A)))");
  T('s=> ("A" | "B").', "A", "AND(-s-AND(BRACKET(OR(#0#AND(A)))))");
  T('s=> "A"*.', "A", "AND(-s-AND(NFOLD(A)))");
  T('s=> "A"*.', "AA", "AND(-s-AND(NFOLD(AA)))");

  T('s=> R. R=>"A".', "A", "AND(-s-AND(AND(-R-AND(A))))");
  T('s=> R. R=>"A".',"A", "AND(-s-AND(AND(-R-AND(A))))");
  T('s=> A|B. A=>"A".B=>"B".',"B", "AND(-s-OR(#1#AND(AND(-B-AND(B)))))");
  T('s=> A|B. A=>"A".B=>("B"|"C").',"B", "AND(-s-OR(#1#AND(AND(-B-AND(BRACKET(OR(#0#AND(B))))))))");
  T('s=> A?. A=>"A".',"A", "AND(-s-AND(OPT(AND(-A-AND(A)))))");
  T('s=> "A"?.',"A", "AND(-s-AND(OPT(A)))");
  T('s=> A+. A=>"A".',"A", "AND(-s-AND(NFOLD(AND(-A-AND(A)))))");
  T('s=> "A"|"B"|"C".' ,"C", "AND(-s-OR(#2#AND(C)))");

  });

}