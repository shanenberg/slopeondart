import 'dart:io';

import 'package:test/test.dart';

import '../../lib/grammarparser/GrammarParser.dart';
import '../../lib/grammarparser/tree/GrammarObject.dart';
import '../../lib/parser/ResultObject.dart';

/* Creates grammar objects from all .slogr-files in
   resources directory
   and runs all tests defined in the grammar.
 */


/*
  Creates from the passed object a grammar object and runs the tests
 */
void testGrammar(String grammarString) {

  ResultObject<SLOPEGrammarObject> result = new ResultObject<SLOPEGrammarObject>();
  var g = new GrammarParser();
  g.parseString(grammarString, result);
  result.getResult().doTests();
}


/*
  Returns all .slogr-files
 */
List<File> allSLOGRFiles() {
  Directory currentDir =
  new Directory(
      Directory.current.path + "\\lib\\testing\\slope\\resources");

  List<FileSystemEntity> l = currentDir.listSync(recursive: true, followLinks: false);

  // Get all .slogr files
  return
    l
        .map((FileSystemEntity e) {return new File(e.path);}).toList()
        .where((File file) {
          return file.existsSync() && file.path.endsWith(".slogr");}).toList();
}

void main() {

    for (File f in allSLOGRFiles()) {
      test("All slope-resources" + f.path.split("\\").last,
      () {
        testGrammar(f.readAsStringSync());
      });
    };
}

