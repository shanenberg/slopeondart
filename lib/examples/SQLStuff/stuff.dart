import 'dart:convert';
import 'dart:io';

import 'package:csv/csv.dart';
import 'package:slopeondart/lib/util/GrammarUtils.dart';
import 'package:slopeondart/lib/grammarparser/tree/GrammarObject.dart';

SLOPEGrammarObject sqlGrammar;
File passedTestFile;
File failedTestFile;
String file;

main2() {
  String sqlGrammarString = new File
    ("lib\\examples\\SQLStuff\\FINALSQL.slogr")
      .readAsStringSync();
}

main() {/*
  SLOPEGrammarObject grammar = parseGrammarFromPath("lib/examples/SQLStuff/SQL_BIG.slogr");
  grammar.doTests();
  print("hi");*/

  String sqlGrammarString = new File
    ("lib\\examples\\SQLStuff\\FINALSQL.slogr")
      .readAsStringSync();



  sqlGrammar = parseGrammar(sqlGrammarString);

  runGrammarTests();
  //runMiniTests();
  //runServerTests();

  //
  //print("select b.courseid from teaches b where year = 1998".toUpperCase());

}

runGrammarTests() {
  sqlGrammar.doTests();
 //sqlGrammar.doTestsFromName("T123");
}

runMiniTests() {
  sqlGrammar.parseString("( select b.courseid from teaches b where year = 1998 )");
  sqlGrammar.parseString("select b.courseid from teaches b where year = 1998");
  sqlGrammar.parseString("select b.courseid from teaches b where year = 1998");
}


runServerTests() {
  passedTestFile = new File
    ("lib\\examples\\SQLStuff\\FINALSQL_passedServerTests.txt");

  failedTestFile = new File
    ("lib\\examples\\SQLStuff\\FINALSQL_failedServerTests.txt");


  file = new File
    ("lib\\examples\\SQLStuff\\korrekte_Lösungen.csv")
      .readAsStringSync();


  List<List<dynamic>> rowsAsListOfValues = const CsvToListConverter().convert(file);
  int count = 1;
  for(List<dynamic> bla in rowsAsListOfValues) {
    String ble = (bla[0] as String);//.toUpperCase();

    if(!ble.startsWith("create")
        && !ble.startsWith("insert")
        && !ble.startsWith("delete")
        && !ble.startsWith("update")) {
      try {
        sqlGrammar.parseString(ble);
      //  print("PASSED: ");
      //  print('$count: $ble');
        passedTestFile.writeAsStringSync("$count: "+ble + "\n", mode: FileMode.APPEND);
      }
      catch(e) {
        print("FAILED: ");



        print('$count: $ble');
        failedTestFile.writeAsStringSync("$count: "+ble + "\n", mode: FileMode.APPEND);
      }

      count++;

    }
  }
}

/*

  new File
    ("lib\\examples\\SQLStuff\\FINALSQL_ServerTests.slogrr")
      .writeAsStringSync(blaas, mode: FileMode.APPEND);
*/
