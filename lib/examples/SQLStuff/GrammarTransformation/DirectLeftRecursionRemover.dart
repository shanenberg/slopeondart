import 'package:slopeondart/examples/SQLStuff/GrammarTransformation/Cloner.dart';
import 'package:slopeondart/examples/SQLStuff/GrammarTransformation/SlopeGrammarUser.dart';
import 'package:slopeondart/lib/grammarparser/GrammarRuleExpression.dart';
import 'package:slopeondart/lib/grammarparser/tree/GrammarObject.dart';
import 'package:slopeondart/lib/grammarparser/tree/GrammarRuleObject.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionAND.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionBRACKET.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionOR.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionRuleRefOrScannerRef.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleUnary.dart';

class DirectLeftRecursionRemover extends SlopeGrammarUser {

  GrammarRuleObject currentRule;

  @override
  use(SLOPEGrammarObject grammarObject)
  {
    this.grammarObject = grammarObject;
    //(reflect(DirectLeftRecursionRemover) as ClassMirror).newInstance(new Symbol(""), []);
/*
    runOnExpression(grammarObject
        .getRule("queryspecification")
        .body);*/

    for(GrammarRuleObject rule in grammarObject.rules) {
      currentRule = rule;
      runOnExpression(rule.body);
    }
  }

  @override
  grammarRuleExpressionOR(GrammarRuleExpressionOR expression)
  {
    if(_isDirectLeftRecursive(expression)) {
      //TODO _transformExpression(expression);
    }
  }

  bool _isDirectLeftRecursive(GrammarRuleExpressionOR expression) {
    for (GrammarRuleExpressionAND element in expression.elements) {
      if(   element.elements[0] is GrammarRuleExpressionRuleRefOrScannerRef
          && (element.elements[0] as GrammarRuleExpressionRuleRefOrScannerRef).token == currentRule.name)
      {
        return true;

      }
    }
    return false;
  }

  _transformExpression(GrammarRuleExpressionOR expression) {
    List<GrammarRuleExpressionAND> nonRecursives = [];
    List<GrammarRuleExpressionAND> recursives = [];

    for (GrammarRuleExpressionAND element in expression.elements) {
      if (element.elements[0] is GrammarRuleExpressionRuleRefOrScannerRef
          && (element.elements[0] as GrammarRuleExpressionRuleRefOrScannerRef).token == currentRule.name) {
        GrammarRuleExpressionAND clonedElement = cloneExpression(element);
        clonedElement.elements.removeAt(0);
        recursives.add(clonedElement);
      } else {
        GrammarRuleExpressionAND clonedElement = cloneExpression(element);
        nonRecursives.add(clonedElement);
      }
    }

    GrammarRuleExpressionBRACKET nonRecBracketAndOr = _createNonRecursiveExpressionOR(nonRecursives);
/*
   TODO GrammarRuleExpressionOR recOr = new GrammarRuleExpressionOR();
    nonRecOr.elements = recursives;
    GrammarRuleExpressionAND rececAndOr = new GrammarRuleExpressionAND();
    nonRecAndOr.elements = [nonRecOr];
    GrammarRuleExpressionBRACKET nonRecBracketAndOr = new GrammarRuleExpressionBRACKET(null);
    nonRecBracketAndOr.element = nonRecAndOr;*/


    GrammarRuleExpressionAND transformedExpression = new GrammarRuleExpressionAND();
    transformedExpression.elements = [];
  }

  _createNonRecursiveExpressionOR(List<GrammarRuleExpressionAND> nonRecursives) {
    GrammarRuleExpressionOR nonRecOr = new GrammarRuleExpressionOR();
    nonRecOr.elements = nonRecursives;
    GrammarRuleExpressionAND nonRecAndOr = new GrammarRuleExpressionAND();
    nonRecAndOr.elements = [nonRecOr];
    GrammarRuleExpressionBRACKET nonRecBracketAndOr = new GrammarRuleExpressionBRACKET(null);
    nonRecBracketAndOr.element = nonRecAndOr;
    return nonRecBracketAndOr;
  }
/*
  _createRecursiveExpressionSTAR(List<GrammarRuleExpressionAND> recursives) {
    GrammarRuleExpressionOR nonRecOr = new GrammarRuleExpressionOR();
    nonRecOr.elements = nonRecursives;
    GrammarRuleExpressionAND nonRecAndOr = new GrammarRuleExpressionAND();
    nonRecAndOr.elements = [nonRecOr];
    GrammarRuleExpressionBRACKET nonRecBracketAndOr = new GrammarRuleExpressionBRACKET(null);
    nonRecBracketAndOr.element = nonRecAndOr;
    return nonRecBracketAndOr;
  }*/


  @override
  grammarRuleExpressionNAry(GrammarRuleNAry expression)
  {
    for (GrammarRuleExpression element in expression.elements) {
      runOnExpression(element);
    }
  }

  @override
  grammarRuleExpressionRuleRefOrScannerRef(GrammarRuleExpressionRuleRefOrScannerRef expression)
  {

  }

  @override
  grammarRuleUnary(GrammarRuleUnary expression)
  {
    runOnExpression(expression.element);
  }


}