import 'package:slopeondart/examples/SQLStuff/GrammarTransformation/SlopeGrammarUser.dart';
import 'package:slopeondart/lib/grammarparser/GrammarRuleExpression.dart';
import 'package:slopeondart/lib/grammarparser/tree/GrammarObject.dart';
import 'package:slopeondart/lib/grammarparser/tree/GrammarRuleObject.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionAND.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionBRACKET.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionOPT.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionOR.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionPLUS.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionRegExString.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionRuleRefOrScannerRef.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionStar.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleUnary.dart';

SLOPEGrammarObject cloneGrammar(SLOPEGrammarObject grammarObject) {
  Cloner c = new Cloner();
  c.use(grammarObject);
  return c.clonedGrammarObject;
}

GrammarRuleObject cloneRule(GrammarRuleObject rule) {
  return Cloner.instance._cloneRule(rule);
}

GrammarRuleExpression cloneExpression(GrammarRuleExpression expression) {
  return Cloner.instance.runOnExpression(expression);
}

class Cloner extends SlopeGrammarUser {

  static Cloner instance = new Cloner();
  SLOPEGrammarObject clonedGrammarObject = new SLOPEGrammarObject();

  @override
  use(SLOPEGrammarObject grammarObject) {
    this.grammarObject = grammarObject;

    clonedGrammarObject.startRuleName = grammarObject.startRuleName;
    clonedGrammarObject.grammarName = grammarObject.grammarName;

    clonedGrammarObject.rules = [];

    for(GrammarRuleObject rule in grammarObject.rules) {
      clonedGrammarObject.rules.add(this._cloneRule(rule));
    }

  }


  _cloneRule(GrammarRuleObject rule) {
    GrammarRuleObject clonedRule = new GrammarRuleObject();
    clonedRule.name = rule.name;
    clonedRule.body = runOnExpression(rule.body);
    return clonedRule;
  }


  @override
  grammarRuleExpressionNAry(GrammarRuleNAry expression)
  {
    GrammarRuleNAry clonedExpression = _cloneFactory(expression);
    clonedExpression.elements = [];


    for(GrammarRuleExpression element in expression.elements) {
      clonedExpression.elements.add(runOnExpression(element));
    }
    return clonedExpression;
  }


  @override
  grammarRuleExpressionRuleRefOrScannerRef(GrammarRuleExpressionRuleRefOrScannerRef expression)
  {
    return _cloneFactory(expression);
  }

  @override
  grammarRuleExpressionRegExString(GrammarRuleExpressionRegExString expression)
  {
    return _cloneFactory(expression);
  }

  @override
  grammarRuleUnary(GrammarRuleUnary expression)
  {
    return _cloneFactory(expression);
  }

  GrammarRuleExpression _cloneFactory(GrammarRuleExpression expr) {
    GrammarRuleExpression clonedExpression;
    if(expr is GrammarRuleExpressionSTAR) {
      clonedExpression = new GrammarRuleExpressionSTAR(runOnExpression(expr.element));
    }
    else if(expr is GrammarRuleExpressionPLUS) {
      clonedExpression = new GrammarRuleExpressionPLUS(runOnExpression(expr.element));
    }
    else if(expr is GrammarRuleExpressionBRACKET) {
      clonedExpression = new GrammarRuleExpressionBRACKET(runOnExpression(expr.element));
    }
    else if(expr is GrammarRuleExpressionOPT) {
      clonedExpression = new GrammarRuleExpressionOPT(runOnExpression(expr.element));
    }
    else if(expr is GrammarRuleExpressionRuleRefOrScannerRef) {
      clonedExpression = new GrammarRuleExpressionRuleRefOrScannerRef(expr.token);
    }
    else if(expr is GrammarRuleExpressionRegExString) {
      clonedExpression = new GrammarRuleExpressionRegExString(expr.regexString);
    }
    else if(expr is GrammarRuleExpressionOR) {
      clonedExpression = new GrammarRuleExpressionOR();
    }
    else if(expr is GrammarRuleExpressionAND) {
      clonedExpression = new GrammarRuleExpressionAND();
    }

    _moveOver(expr, clonedExpression);

    return clonedExpression;

  }

  _moveOver(GrammarRuleExpression from, GrammarRuleExpression to) {
    to.ruleExpressionName = from.ruleExpressionName;
    to.grammarObject = from.grammarObject;
  }
}

