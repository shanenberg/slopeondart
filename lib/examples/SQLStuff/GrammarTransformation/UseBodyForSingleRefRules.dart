import 'package:slopeondart/examples/SQLStuff/GrammarTransformation/SlopeGrammarUser.dart';
import 'package:slopeondart/lib/grammarparser/GrammarRuleExpression.dart';
import 'package:slopeondart/lib/grammarparser/tree/GrammarObject.dart';
import 'package:slopeondart/lib/grammarparser/tree/GrammarRuleObject.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionAND.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionBRACKET.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionOPT.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionPLUS.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionRuleRefOrScannerRef.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionStar.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleUnary.dart';


class UseBodyForSingleRefRules extends SlopeGrammarUser {

  Map<String, String> singleRuleRules = {};

  @override
  use(SLOPEGrammarObject grammarObject) {
    this.grammarObject = grammarObject;

    for(GrammarRuleObject rule in grammarObject.rules) {
      GrammarRuleExpressionAND body = rule.body;

      if(body.elements.length == 1
      && body.elements[0] is GrammarRuleExpressionRuleRefOrScannerRef) {
        singleRuleRules[rule.name] = (body.elements[0] as GrammarRuleExpressionRuleRefOrScannerRef).token;
      }
    }

    runOnExpression(grammarObject.getRule("queryspecification").body);
  }

  @override
  grammarRuleUnary(GrammarRuleUnary expression)
  {
    var hi = runOnExpression(expression.element);
    if(hi != null) {
      expression.element = transformExpression(hi);
    }
  }

  @override
  grammarRuleExpressionRuleRefOrScannerRef(GrammarRuleExpressionRuleRefOrScannerRef expression)
  {
    if(singleRuleRules.containsKey(expression.token)) {
      return expression;
    } else {
      super.grammarRuleExpressionRuleRefOrScannerRef(expression);
    }
  }

  @override
  grammarRuleExpressionNAry(GrammarRuleNAry expression)
  {
    for(int i = 0; i < expression.elements.length; i++) {
      var hi = runOnExpression(expression.elements[i]);
      if(hi != null) {
        expression.elements[i] = transformExpression(hi);
      }

    }
  }


  transformExpression(GrammarRuleExpressionRuleRefOrScannerRef expr) {
    expr.token = singleRuleRules[expr.token];
    return expr;
  }

  grammarRuleExpressionSTAR(GrammarRuleExpressionSTAR expression) {
    return grammarRuleUnary(expression);
  }

  grammarRuleExpressionPLUS(GrammarRuleExpressionPLUS expression) {
    return grammarRuleUnary(expression);
  }

  grammarRuleExpressionBRACKET(GrammarRuleExpressionBRACKET expression) {
    return grammarRuleUnary(expression);
  }

  grammarRuleExpressionOPT(GrammarRuleExpressionOPT expression) {
    return grammarRuleUnary(expression);
  }

}