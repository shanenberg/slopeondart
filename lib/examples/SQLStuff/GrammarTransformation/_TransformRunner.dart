import 'package:slopeondart/examples/SQLStuff/GrammarTransformation/Cloner.dart';
import 'package:slopeondart/examples/SQLStuff/GrammarTransformation/FromStartRuleFilterer.dart';
import 'package:slopeondart/examples/SQLStuff/GrammarTransformation/PlusOpt_To_Star.dart';
import 'package:slopeondart/examples/SQLStuff/GrammarTransformation/SlopeGrammarUser.dart';
import 'package:slopeondart/examples/SQLStuff/GrammarTransformation/StackOverflowDetector.dart';
import 'package:slopeondart/examples/SQLStuff/GrammarTransformation/UseBodyForSingleRefRules.dart';
import 'package:slopeondart/lib/util/GrammarUtilsIO.dart';
import 'package:slopeondart/lib/grammarparser/tree/GrammarObject.dart';

main() {
  SLOPEGrammarObject sql = parseGrammarFromPath("lib\\examples\\SQLStuff\\SQL_BIG.slogr");

  //SLOPEGrammarObject sql2 = parseGrammarFromPath("lib\\examples\\SQLStuff\\SQL_SMALL_Queryspecification.slogr");


  transformWithUsers([
    new FromStartRuleFilterer("queryspecification"),
  ], sql);

  transformWithUsers([
    new PlusOpt_To_Star()],
      sql);

  transformWithUsers([
    new UseBodyForSingleRefRules()
  ], sql);

  transformWithUsers([
    new UseBodyForSingleRefRules()
  ], sql);

  transformWithUsers([
    new UseBodyForSingleRefRules()
  ], sql);

  transformWithUsers([
    new FromStartRuleFilterer("queryspecification"),
  ], sql);

  transformWithUsers([
    new StackOverflowDetector()
  ], sql);




}

