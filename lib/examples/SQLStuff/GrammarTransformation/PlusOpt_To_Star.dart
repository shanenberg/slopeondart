import 'package:slopeondart/examples/SQLStuff/GrammarTransformation/FromStartRuleFilterer.dart';
import 'package:slopeondart/examples/SQLStuff/GrammarTransformation/GrammarPrinter.dart';
import 'package:slopeondart/examples/SQLStuff/GrammarTransformation/SlopeGrammarUser.dart';
import 'package:slopeondart/lib/util/GrammarUtilsIO.dart';
import 'package:slopeondart/lib/grammarparser/GrammarRuleExpression.dart';
import 'package:slopeondart/lib/grammarparser/tree/GrammarObject.dart';
import 'package:slopeondart/lib/grammarparser/tree/GrammarRuleObject.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionAND.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionBRACKET.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionOPT.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionOR.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionPLUS.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionRegExString.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionRuleRefOrScannerRef.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionStar.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleUnary.dart';

main() {
  SLOPEGrammarObject sql = parseGrammarFromPath("lib\\examples\\SQLStuff\\SQL_BIG.slogr");

  transformWithUsers([
    new FromStartRuleFilterer("queryspecification"),
  ], sql);

  transformWithUsers([
    new PlusOpt_To_Star()],
      sql);

  transformWithUsers([
    new GrammarPrinter()
  ], sql);
}


class PlusOpt_To_Star extends SlopeGrammarUser {



  use(SLOPEGrammarObject grammarObject) {
    this.grammarObject = grammarObject;
    transform();
  }



  transform() {
    GrammarRuleObject rule = grammarObject.getRule("queryspecification");

    runOnExpression(rule.body);
  }


  @override
  grammarRuleExpressionNAry(GrammarRuleNAry expression)
  {
    for(int i = 0; i < expression.elements.length; i++) {
      var hi = runOnExpression(expression.elements[i]);
      if(hi != null) {
        expression.elements[i] = toStar(hi);
      }

    }
  }




  @override
  grammarRuleExpressionOPT(GrammarRuleExpressionOPT expression)
  {
    if(expression.element is GrammarRuleExpressionBRACKET) {
      GrammarRuleExpressionBRACKET expressionBRACKET = expression.element;
      if(expressionBRACKET.element is GrammarRuleExpressionAND) {
        GrammarRuleExpressionAND expressionAND = expressionBRACKET.element;
        if(expressionAND.elements.length == 1 && expressionAND.elements[0] is GrammarRuleExpressionPLUS) {
          return expressionAND.elements[0];
        }
      }

    }


    var hi = runOnExpression(expression.element);

  }

  @override
  grammarRuleExpressionRegExString(GrammarRuleExpressionRegExString expression) {

  }


  @override
  grammarRuleUnary(GrammarRuleUnary expression)
  {
    var hi = runOnExpression(expression.element);
    if(hi != null) {
      expression.element = toStar(hi);
    }
  }

  GrammarRuleExpressionSTAR toStar(GrammarRuleExpressionPLUS grammarRuleExpression) {

    GrammarRuleExpressionSTAR star = new GrammarRuleExpressionSTAR(grammarRuleExpression.element);
    return star;

  }
}