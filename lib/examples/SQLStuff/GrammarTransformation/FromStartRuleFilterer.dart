import 'package:slopeondart/examples/SQLStuff/GrammarTransformation/GrammarPrinter.dart';
import 'package:slopeondart/examples/SQLStuff/GrammarTransformation/SlopeGrammarUser.dart';
import 'package:slopeondart/lib/util/GrammarUtilsIO.dart';
import 'package:slopeondart/lib/grammarparser/GrammarRuleExpression.dart';
import 'package:slopeondart/lib/grammarparser/tree/GrammarObject.dart';
import 'package:slopeondart/lib/grammarparser/tree/GrammarRuleObject.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionAND.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionBRACKET.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionOPT.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionOR.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionPLUS.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionRegExString.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionRuleRefOrScannerRef.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionStar.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleUnary.dart';

main() {
  SLOPEGrammarObject sql = parseGrammarFromPath("lib\\examples\\SQLStuff\\SQL_BIG.slogr");

  transformWithUsers([
    new FromStartRuleFilterer("queryspecification"),
  ], sql);

  transformWithUsers([
    new GrammarPrinter()
  ], sql);
}


class FromStartRuleFilterer extends SlopeGrammarUser {

  String startRuleName;

  List<GrammarRuleObject> transformedRules = [];
  List<String> unusedNames = [];

  use(SLOPEGrammarObject grammarObject) {
    this.grammarObject = grammarObject;
    transform(grammarObject, startRuleName);
  }

  FromStartRuleFilterer(this.startRuleName);

  transform(SLOPEGrammarObject grammarObject, String startRuleName) {
    GrammarRuleObject rule = grammarObject.getRule(startRuleName);
    transformedRules.add(rule);
    runOnExpression(rule.body);

    grammarObject.rules = transformedRules;

  }


  @override
  grammarRuleExpressionNAry(GrammarRuleNAry expression)
  {
    for(GrammarRuleExpression expr in  expression.elements)
      runOnExpression(expr);
  }

  @override
  grammarRuleExpressionRegExString(GrammarRuleExpressionRegExString expression)
  {

  }

  @override
  grammarRuleExpressionRuleRefOrScannerRef(GrammarRuleExpressionRuleRefOrScannerRef expression)
  {
    GrammarRuleObject rule = grammarObject.getRule(expression.token);
    if(rule == null && !unusedNames.contains(expression.token)) {
      unusedNames.add(expression.token);
    }
    else
    if(rule != null && !transformedRules.contains(rule)) {
      transformedRules.add(rule);
      runOnExpression(rule.body);
    }
  }

  @override
  grammarRuleUnary(GrammarRuleUnary expression)
  {
    runOnExpression(expression.element);
  }
}