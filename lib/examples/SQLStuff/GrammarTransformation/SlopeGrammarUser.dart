import '../../../lib/util/GrammarUtils.dart';
import '../../../lib/grammarparser/GrammarRuleExpression.dart';
import '../../../lib/grammarparser/tree/GrammarObject.dart';
import '../../../lib/grammarparser/tree/GrammarRuleObject.dart';
import '../../../lib/grammarparser/tree/rule/GrammarRuleExpressionAND.dart';
import '../../../lib/grammarparser/tree/rule/GrammarRuleExpressionBRACKET.dart';
import '../../../lib/grammarparser/tree/rule/GrammarRuleExpressionOPT.dart';
import '../../../lib/grammarparser/tree/rule/GrammarRuleExpressionOR.dart';
import '../../../lib/grammarparser/tree/rule/GrammarRuleExpressionPLUS.dart';
import '../../../lib/grammarparser/tree/rule/GrammarRuleExpressionRegExString.dart';
import '../../../lib/grammarparser/tree/rule/GrammarRuleExpressionRuleRefOrScannerRef.dart';
import '../../../lib/grammarparser/tree/rule/GrammarRuleExpressionStar.dart';
import '../../../lib/grammarparser/tree/rule/GrammarRuleUnary.dart';




typedef FunctionGrammarRuleExpression(GrammarRuleExpression expr);


void transformWithUsers(List<SlopeGrammarUser> users, SLOPEGrammarObject grammarObject) {
  for(SlopeGrammarUser user in users)
    user.use(grammarObject);

}


abstract class SlopeGrammarUser {

  List<String> visited = [];
  SLOPEGrammarObject grammarObject;

  use(SLOPEGrammarObject grammarObject) {

  }

  runOnExpression(GrammarRuleExpression expr) {

    if(expr is GrammarRuleExpressionSTAR) {
      return grammarRuleExpressionSTAR(expr);
    }
    else if(expr is GrammarRuleExpressionPLUS) {
      return grammarRuleExpressionPLUS(expr);
    }
    else if(expr is GrammarRuleExpressionBRACKET) {
      return grammarRuleExpressionBRACKET(expr);
    }
    else if(expr is GrammarRuleExpressionRuleRefOrScannerRef) {
      return grammarRuleExpressionRuleRefOrScannerRef(expr);
    }
    else if(expr is GrammarRuleExpressionRegExString) {
      return grammarRuleExpressionRegExString(expr);
    }
    else if(expr is GrammarRuleExpressionOR) {
      return grammarRuleExpressionOR(expr);
    }
    else if(expr is GrammarRuleExpressionAND) {
      return grammarRuleExpressionAND(expr);
    }
    else if(expr is GrammarRuleExpressionOPT) {
      return grammarRuleExpressionOPT(expr);
    }
  }



  grammarRuleUnary(GrammarRuleUnary expression) {

  }

  grammarRuleExpressionSTAR(GrammarRuleExpressionSTAR expression) {
    return grammarRuleUnary(expression);
  }

  grammarRuleExpressionPLUS(GrammarRuleExpressionPLUS expression) {
    return grammarRuleUnary(expression);
  }

  grammarRuleExpressionBRACKET(GrammarRuleExpressionBRACKET expression) {
    return grammarRuleUnary(expression);
  }

  grammarRuleExpressionOPT(GrammarRuleExpressionOPT expression) {
    return grammarRuleUnary(expression);
  }

  grammarRuleExpressionRuleRefOrScannerRef(GrammarRuleExpressionRuleRefOrScannerRef expression) {
    if(!visited.contains(expression.token)) {
      visited.add(expression.token);
      GrammarRuleObject rule = grammarObject.getRule(expression.token);
      if(rule != null) {
        runOnExpression(rule.body);
      }
    }
  }

  grammarRuleExpressionRegExString(GrammarRuleExpressionRegExString expression) {

  }

  grammarRuleExpressionNAry(GrammarRuleNAry expression) {

  }

  grammarRuleExpressionOR(GrammarRuleExpressionOR expression) {
    return grammarRuleExpressionNAry(expression);

  }

  grammarRuleExpressionAND(GrammarRuleExpressionAND expression) {
    return grammarRuleExpressionNAry(expression);
  }


}
