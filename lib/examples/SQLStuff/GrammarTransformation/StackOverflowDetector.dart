import 'package:slopeondart/examples/SQLStuff/GrammarTransformation/FromStartRuleFilterer.dart';
import 'package:slopeondart/examples/SQLStuff/GrammarTransformation/GrammarPrinter.dart';
import 'package:slopeondart/examples/SQLStuff/GrammarTransformation/SlopeGrammarUser.dart';
import 'package:slopeondart/lib/util/GrammarUtils.dart';
import 'package:slopeondart/lib/grammarparser/GrammarRuleExpression.dart';
import 'package:slopeondart/lib/grammarparser/tree/GrammarObject.dart';
import 'package:slopeondart/lib/grammarparser/tree/GrammarRuleObject.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionAND.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionBRACKET.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionOPT.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionOR.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionPLUS.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionRegExString.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionRuleRefOrScannerRef.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionStar.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleUnary.dart';

class StackOverflowDetector extends SlopeGrammarUser {

  List<String> ruleStack = [];
  List<List<String>> mainStacks = [];
  List<List<String>> allStacks = [];

  String recursivePoint (List<String> stack) {
    List<String> miniStack = [];
    for(String s in stack) {
      if(miniStack.contains(s)) {
        return s;
      } else {
        miniStack.add(s);
      }
    }
    throw "WTF";
  }
  
  similarStack(List<String> pStack) {
    for(List<String> stack in mainStacks) {
      
      if(recursivePoint(stack) == recursivePoint(pStack)) {
        return stack;
      }
    }
    return null;
  }

  removeDuplicates(List<String> stack) {

  }

  similarStacksInAllStacks(List<String> pStack) {
    List<List<String>> similars = [];
    for (List<String> stack in allStacks) {
      if (recursivePoint(stack) == recursivePoint(pStack)) {
        similars.add(stack);
      }
    }
    return similars;
  }

  tryStackForSmallest(List<String> pStack) {
    List<String> stack = similarStack(pStack);
    if(stack == null) {
      mainStacks.add(new List.from(pStack));
    } else {
      if(pStack.length < stack.length) {
        mainStacks.remove(stack);
        mainStacks.add(new List.from(pStack));
      }
    }
  }

  @override
  use(SLOPEGrammarObject grammarObject) {
    this.grammarObject = grammarObject;

    for(GrammarRuleObject rule in grammarObject.rules) {
      runOnExpression(rule.body);
      visited.clear();
    }

    print("\nMain Stacks:\n");
    print(mainStacks);

    for(List<String> stack in mainStacks) {
      print(stack[0]+":");
      print("Smallest: "+stack.toString());
      print(similarStacksInAllStacks(stack));
      print("");
    }
  }

  @override
  grammarRuleExpressionAND(GrammarRuleExpressionAND expression)
  {
    for(GrammarRuleExpression expr in expression.elements) {

      var has = runOnExpression(expr);

      if(has == null) {
        print("lol");
        has = runOnExpression(expr);
      }

      if(has)
        return true;

      if(!(  expr is GrammarRuleExpressionSTAR
          || expr is GrammarRuleExpressionOPT
        )) {
        return false;
      }
    }
    return false;
  }

  @override
  grammarRuleExpressionOR(GrammarRuleExpressionOR expression)
  {
    for(GrammarRuleExpression element in expression.elements) {
      if(runOnExpression(element))
        return true;
    }
    return false;
  }

  @override
  grammarRuleExpressionRegExString(GrammarRuleExpressionRegExString expression)
  {
    return false;
  }

  @override
  grammarRuleExpressionRuleRefOrScannerRef(GrammarRuleExpressionRuleRefOrScannerRef expression)
  {
    if(ruleStack.contains(expression.token)) {
      ruleStack.add(expression.token);
      print("Found left recursion");
      print("Stack: "+ruleStack.toString());
      allStacks.add(new List.from(ruleStack));
      tryStackForSmallest(ruleStack);
      ruleStack.removeLast();
      return true;
    } else {



      GrammarRuleObject rule = grammarObject.getRule(expression.token);
      if(rule != null) {
        ruleStack.add(expression.token);
        bool has = runOnExpression(rule.body);
        ruleStack.removeLast();
        return has;
      //  runOnExpression(rule.body);?
      } else
        return false;

      /*
      if(!visited.contains(expression.token)) {
        visited.add(expression.token);
        GrammarRuleObject rule = grammarObject.getRule(expression.token);
        if(rule != null) {
          ruleStack.add(expression.token);
          bool has = runOnExpression(rule.body);
          ruleStack.removeLast();
          return has;
          runOnExpression(rule.body);
        } else
          return false;
      }
      return false;*/
      /*ruleStack.add(expression.token);
      bool has = super.grammarRuleExpressionRuleRefOrScannerRef(expression);
      ruleStack.removeLast();
      return has;*/
    }
  }

  @override
  grammarRuleUnary(GrammarRuleUnary expression)
  {
    return runOnExpression(expression.element);
  }
}