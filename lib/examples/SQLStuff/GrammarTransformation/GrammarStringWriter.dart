import 'SlopeGrammarUser.dart';
import '../../../lib/grammarparser/GrammarRuleExpression.dart';
import '../../../lib/grammarparser/tree/GrammarObject.dart';
import '../../../lib/grammarparser/tree/GrammarRuleObject.dart';
import '../../../lib/grammarparser/tree/rule/GrammarRuleExpressionAND.dart';
import '../../../lib/grammarparser/tree/rule/GrammarRuleExpressionBRACKET.dart';
import '../../../lib/grammarparser/tree/rule/GrammarRuleExpressionOPT.dart';
import '../../../lib/grammarparser/tree/rule/GrammarRuleExpressionOR.dart';
import '../../../lib/grammarparser/tree/rule/GrammarRuleExpressionPLUS.dart';
import '../../../lib/grammarparser/tree/rule/GrammarRuleExpressionRegExString.dart';
import '../../../lib/grammarparser/tree/rule/GrammarRuleExpressionRuleRefOrScannerRef.dart';
import '../../../lib/grammarparser/tree/rule/GrammarRuleExpressionStar.dart';
import '../../../lib/grammarparser/tree/rule/GrammarRuleUnary.dart';
import '../../../lib/grammarparser/ScannerRuleObject.dart';
import '../../../lib/util/GrammarUtils.dart';




main() {
    SLOPEGrammarObject obj = parseGrammar('''
      HI
      Rules: abc
      
      abc => "DINGDONG"*.
      dings => "Dingsbum" "s"+.
      literal => topps BLA.
      
      Scanner:
      
        
        BLA => "BLE+[h-z]".
        whitespace => SEPARATOR " |\\n|\\r".
        bot => "bottomDings" BOTTOM.
        topps => "topDings" TOP.
        lol => SEPARATOR "\\t" BOTTOM.        
      
    ''');


    GrammarStringWriter writer = new GrammarStringRegexWriter();
    writer.use(obj);
    print(writer.grammarString);
}

class GrammarStringRegexWriter extends GrammarStringWriter {

  int regexCounter = 1;
  Map<String, String> regexMap = {};

  @override
  writeGrammarString()
  {
    writeGrammarHeader();
    writeGrammarRules();

  }

  @override
  grammarRuleExpressionRegExString(GrammarRuleExpressionRegExString expression)
  {
    print("hi");

    String savedRegexName = regexMap[expression.regexString];

    if(savedRegexName != null) {
      grammarString += savedRegexName;
    } else {
      String regexName = "REGEX_${regexCounter++}";
    }
  }


}

class GrammarStringWriter extends SlopeGrammarUser {
  String grammarString = "";

  use(SLOPEGrammarObject obj) {
    grammarObject = obj;
    writeGrammarString();
  }

  writeGrammarString() {
    writeGrammarHeader();


    writeGrammarRules();
    writeScannerRules();
  }

  writeGrammarHeader() {
    grammarString += grammarObject.grammarName+"\n";

  }

  writeGrammarRules() {
    writeGrammarRulesHeader();

    for(GrammarRuleObject rule in grammarObject.rules) {
      writeRuleString(rule);
    }
  }

  writeGrammarRulesHeader() {
    grammarString += "Rules: "+grammarObject.startRuleName+ "\n";
  }


  writeRuleString(GrammarRuleObject rule) {
    grammarString += "${rule.name} => ";
    runOnExpression(rule.body);
    grammarString += ".\n";
  }



  writeScannerRules() {
    writeScannerRulesHeader();
    writeTopScannerRules();
    writeBottomScannerRules();
  }

  writeScannerRulesHeader() {
    grammarString += "Scanner: \n";

  }

  writeTopScannerRules() {
    for(ScannerRuleObject rule in grammarObject.topScanners) {
      writeScannerRuleString(rule);
    }
  }

  writeBottomScannerRules() {
    for(ScannerRuleObject rule in grammarObject.bottomScanners) {
      writeScannerRuleString(rule);
    }
  }

  writeScannerRuleString(ScannerRuleObject rule) {
    grammarString += "${rule.name} => ${rule.isSeparator ? "SEPARATOR" : ""} \"${rule.scannerExpression}\" ${rule.isTop ? "TOP" : "BOTTOM"}.\n";
  }



  @override
  grammarRuleExpressionNAry(GrammarRuleNAry expression) {
  }

  @override
  grammarRuleExpressionSTAR(GrammarRuleExpressionSTAR expression) {
    runOnExpression(expression.element);
    grammarString+= "*";
  }

  @override
  grammarRuleExpressionRegExString(GrammarRuleExpressionRegExString expression) {
    grammarString+="\"${expression.regexString}\"";
  }

  @override
  grammarRuleExpressionRuleRefOrScannerRef(GrammarRuleExpressionRuleRefOrScannerRef expression) {
    grammarString += expression.token;
  }

  @override
  grammarRuleUnary(GrammarRuleUnary expression) {
    // TODO: implement _grammarRuleUnary
  }

  @override
  grammarRuleExpressionPLUS(GrammarRuleExpressionPLUS expression) {
    runOnExpression(expression.element);
    grammarString+= "+";
  }

  @override
  grammarRuleExpressionBRACKET(GrammarRuleExpressionBRACKET expression) {
    grammarString += "(";
    runOnExpression(expression.element);
    grammarString += ")";
  }

  @override
  grammarRuleExpressionOPT(GrammarRuleExpressionOPT expression) {
    runOnExpression(expression.element);
    grammarString+= "?";
  }

  @override
  grammarRuleExpressionOR(GrammarRuleExpressionOR expression) {
    runOnExpression(expression.elements[0]);

    for(int i = 1; i < expression.elements.length ; i++) {
      grammarString+= " | ";
      runOnExpression(expression.elements[i]);
    }
  }

  @override
  grammarRuleExpressionAND(GrammarRuleExpressionAND expression) {
    runOnExpression(expression.elements[0]);

    for(int i = 1; i < expression.elements.length ; i++) {
      grammarString+= " ";
      runOnExpression(expression.elements[i]);
    }
  }
}