import 'dart:io';

import 'package:slopeondart/examples/SQLStuff/GrammarTransformation/SlopeGrammarUser.dart';
import 'package:slopeondart/examples/SQLStuff/GrammarTransformation/StackOverflowDetector.dart';
import 'package:slopeondart/lib/util/GrammarUtilsIO.dart';
import 'package:slopeondart/lib/grammarparser/tree/GrammarObject.dart';
import 'package:slopeondart/lib/parser/tree/TreeNode.dart';

main()
{
  String grammarString = new File("lib\\examples\\SQLStuff\\FINALSQL.slogr").readAsStringSync();
  //SLOPEGrammarObject sql = parseGrammarFromPath("lib\\examples\\SQLStuff\\SQL_2SMALL_Queryspecification.slogr");
  SLOPEGrammarObject sql = parseGrammarFromPath("lib\\examples\\SQLStuff\\FINALSQL.slogr");

  transformWithUsers([new StackOverflowDetector()], sql);

  //transformWithUsers([new DirectLeftRecursionRemover()], sql);

  sql.doTests();
  //TreeNode sqlNode = parseStringFromGrammar(
  //    "SELECT hi FROM (SELECT hi FROM (lol NATURAL JOIN beb) JOIN ding ON true WHERE lol = 1) abc", grammarString);
  //print("hi");
}