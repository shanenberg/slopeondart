import 'package:slopeondart/examples/SQLStuff/GrammarTransformation/SlopeGrammarUser.dart';
import 'package:slopeondart/lib/util/GrammarUtilsIO.dart';
import 'package:slopeondart/lib/grammarparser/GrammarRuleExpression.dart';
import 'package:slopeondart/lib/grammarparser/tree/GrammarObject.dart';
import 'package:slopeondart/lib/grammarparser/tree/GrammarRuleObject.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionAND.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionBRACKET.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionOPT.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionOR.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionPLUS.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionRegExString.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionRuleRefOrScannerRef.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionStar.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleUnary.dart';

main() {

  SLOPEGrammarObject sql = parseGrammarFromPath("lib\\examples\\SQLStuff\\SQL_BIG.slogr");

  GrammarPrinter h = new GrammarPrinter();
  h.printGrammar(sql);
}


class GrammarPrinter extends SlopeGrammarUser {
  String printout = '''SQL
  
  Rules: queryspecification
  
  
  
  ''';

  use(SLOPEGrammarObject obj) {
    printGrammar(obj);
  }

  printGrammar(SLOPEGrammarObject obj) {
    for(GrammarRuleObject rule in obj.rules) {
      printRule(rule);
    }

    print(printout);
  }


  printRule(GrammarRuleObject rule) {
    printout += "${rule.name} => ";
    runOnExpression(rule.body);
    printout += ".\n";
  }

  @override
  grammarRuleExpressionNAry(GrammarRuleNAry expression) {
  }

  @override
  grammarRuleExpressionSTAR(GrammarRuleExpressionSTAR expression) {
    runOnExpression(expression.element);
    printout+= "*";
  }

  @override
  grammarRuleExpressionRegExString(GrammarRuleExpressionRegExString expression) {
    printout+="\"${expression.regexString}\"";
  }

  @override
  grammarRuleExpressionRuleRefOrScannerRef(GrammarRuleExpressionRuleRefOrScannerRef expression) {
    printout += expression.token;
  }

  @override
  grammarRuleUnary(GrammarRuleUnary expression) {
    // TODO: implement _grammarRuleUnary
  }

  @override
  grammarRuleExpressionPLUS(GrammarRuleExpressionPLUS expression) {
    runOnExpression(expression.element);
    printout+= "+";
  }

  @override
  grammarRuleExpressionBRACKET(GrammarRuleExpressionBRACKET expression) {
    printout += "(";
    runOnExpression(expression.element);
    printout += ")";
  }

  @override
  grammarRuleExpressionOPT(GrammarRuleExpressionOPT expression) {
    runOnExpression(expression.element);
    printout+= "?";
  }

  @override
  grammarRuleExpressionOR(GrammarRuleExpressionOR expression) {
    runOnExpression(expression.elements[0]);

    for(int i = 1; i < expression.elements.length ; i++) {
      printout+= " | ";
      runOnExpression(expression.elements[i]);
    }
  }

  @override
  grammarRuleExpressionAND(GrammarRuleExpressionAND expression) {
    runOnExpression(expression.elements[0]);

    for(int i = 1; i < expression.elements.length ; i++) {
      printout+= " ";
      runOnExpression(expression.elements[i]);
    }
  }
}