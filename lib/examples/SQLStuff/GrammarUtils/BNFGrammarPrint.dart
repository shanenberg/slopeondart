import 'dart:io';

import 'package:slopeondart/lib/util/GrammarUtils.dart';
import 'package:slopeondart/lib/util/GrammarUtilsIO.dart';
import 'package:slopeondart/lib/dynamicParser/DynamicParser.dart';
import 'package:slopeondart/lib/grammarparser/GrammarParser.dart';
import 'package:slopeondart/lib/grammarparser/SlopeGrammar.dart';
import 'package:slopeondart/lib/grammarparser/tree/GrammarObject.dart';
import 'package:slopeondart/lib/parser/ResultObject.dart';
import 'package:slopeondart/lib/parser/features/and/AND_TreeNode.dart';
import 'package:slopeondart/lib/parser/features/bracket/BRACKET_TreeNode.dart';
import 'package:slopeondart/lib/parser/features/nfold/NFOLD_TreeNode.dart';
import 'package:slopeondart/lib/parser/features/optional/OPTIONAL_TreeNode.dart';
import 'package:slopeondart/lib/parser/features/scannerelement/ScannerElement_TreeNode.dart';
import 'package:slopeondart/lib/parser/tree/MultiChildrenNode.dart';
import 'package:slopeondart/lib/parser/tree/SingleChildTreeNode.dart';
import 'package:slopeondart/lib/parser/tree/TreeNode.dart';
import 'package:slopeondart/testing/slope/SlopeResourceRunner_test.dart';


typedef TreeFunctionAlternative(TreeNode, Map<String, Object> env, MiniTreeProgram);

SLOPEGrammarObject sqlGrammar;

main() {

  TreeNode treeNode = parseStringFromGrammarPath("lib\\examples\\SQLStuff\\sql-92.bnf",
     "lib\\examples\\SQLStuff\\BNFGrammar.slogr");

  MiniTreeProgram program = new MiniTreeProgram({

    "rules" : rules,
    "rule" : rule,
    "ruleref" : ruleref,
    "firstnumber" : firstnumber,
    "ruleExpr0Alt" : ruleExpr0Alt,
    "ruleExpr1Concat" : ruleExpr1Concat,
    "ruleExpr2NFold" : ruleExpr2NFold,
    "ruleExpr3Brackets" : ruleExpr3Brackets,
    "ruleExpr4Opt" : ruleExpr4Opt,
    "ruleExpr4Bracket" : ruleExpr4Bracket,
    "ruleValue" : ruleValue,
    "identifier" : identifier,
    "number" : number,
    "specialchar" : specialchar,
    "scannerIdentifier" : scannerIdentifier,
    "scannerNumber" : scannerNumber,
    "ALL" : ALL


  });

  program.start(treeNode);


}





rules(AND_TreeNode treeNode, Map<String, Object> env, MiniTreeProgram program) {
  env["printout"] = '''
  SQL
  
  Rules: queryspecification
  
  
  ''';

  env["counter"] = 0;


  for(TreeNode child in childrenOf(treeNode.treeNodes[0])) {
    program.runOnTreeNode(child, env);
    env["counter"]
      = (env["counter"] as int)+1;
  }




  env["printout"] =
      (env["printout"] as String) + '''
      
      Scanner: 
        blublu => "blublu".
      
      ''';

  print(env["printout"]);

  sqlGrammar = parseGrammar(env["printout"]);


}


rule(MultiChildrenNode treeNode, Map<String, Object> env, MiniTreeProgram program) {


  //if((env["counter"] as int) >= 554) {
    program.runOnTreeNode(treeNode.treeNodes[1], env);

    env["printout"] = (env["printout"] as String) + " => ";

    program.runOnTreeNode(treeNode.treeNodes[3], env);

    env["printout"] = (env["printout"] as String) + ".\n";
  //}
}


ruleref(AND_TreeNode treeNode, Map<String, Object> env, MiniTreeProgram program) {
  program.runOnTreeNode(treeNode.treeNodes[1], env);
  program.runOnTreeNode(treeNode.treeNodes[2], env);
}

firstnumber(AND_TreeNode treeNode, Map<String, Object> env, MiniTreeProgram program) {
  env["printout"] = (env["printout"] as String) + "_";

  _continueForChildren(treeNode, env, program);
}

ruleExpr0Alt(AND_TreeNode treeNode, Map<String, Object> env, MiniTreeProgram program) {
  _continueFor(treeNode.treeNodes[0], env, program);
  for(BRACKET_TreeNode childz in (treeNode.treeNodes[1] as NFOLD_TreeNode).treeNodes) {
    AND_TreeNode child = childz.child;
    env["printout"] = (env["printout"] as String) + " | ";
    _continueFor(child.treeNodes[1], env, program);
  }
}

ruleExpr1Concat(AND_TreeNode treeNode, Map<String, Object> env, MiniTreeProgram program) {
  _continueFor(treeNode.treeNodes[0], env, program);
  for(AND_TreeNode child in (treeNode.treeNodes[1] as NFOLD_TreeNode).treeNodes) {
    env["printout"] = (env["printout"] as String) + " ";
    _continueFor(child, env, program);
  }
}
ruleExpr2NFold(AND_TreeNode treeNode, Map<String, Object> env, MiniTreeProgram program) {
  _continueFor(treeNode.treeNodes[0], env, program);
  if((treeNode.treeNodes[1] as OPTIONAL_TreeNode).child != null)
    env["printout"] = (env["printout"] as String) + "+";
}
ruleExpr3Brackets(AND_TreeNode treeNode, Map<String, Object> env, MiniTreeProgram program) {
  _continueForChildren(treeNode, env, program);
}
ruleExpr4Opt(AND_TreeNode treeNode, Map<String, Object> env, MiniTreeProgram program) {
  env["printout"] = (env["printout"] as String) + "(";
  _continueFor(treeNode.treeNodes[1], env, program);
  env["printout"] = (env["printout"] as String) + ")?";

}
ruleExpr4Bracket(AND_TreeNode treeNode, Map<String, Object> env, MiniTreeProgram program) {
  env["printout"] = (env["printout"] as String) + "(";
  _continueFor(treeNode.treeNodes[1], env, program);
  env["printout"] = (env["printout"] as String) + ")";
}
ruleValue(AND_TreeNode treeNode, Map<String, Object> env, MiniTreeProgram program) {
  _continueForChildren(treeNode, env, program);
}


identifier(TreeNode treeNode, Map<String, Object> env, MiniTreeProgram program) {
  env["printout"]
      = (env["printout"] as String) + ((treeNode as MultiChildrenNode).treeNodes[0] as ScannerElement_TreeNode).scannedWord;
}

number(TreeNode treeNode, Map<String, Object> env, MiniTreeProgram program) {
  env["printout"]
  = (env["printout"] as String) + ((treeNode as MultiChildrenNode).treeNodes[0] as ScannerElement_TreeNode).scannedWord;

}

specialchar(TreeNode treeNode, Map<String, Object> env, MiniTreeProgram program) {
  env["printout"]
      = (env["printout"] as String) + "_";
}

scannerIdentifier(TreeNode treeNode, Map<String, Object> env, MiniTreeProgram program) {
  env["printout"]
  = (env["printout"] as String) + "\"";
  _continueForChildren(treeNode, env, program);
  env["printout"]
  = (env["printout"] as String) + "\"";
}

scannerNumber(TreeNode treeNode, Map<String, Object> env, MiniTreeProgram program) {
  env["printout"]
  = (env["printout"] as String) + "\"";
  _continueForChildren(treeNode, env, program);
  env["printout"]
  = (env["printout"] as String) + "\"";
}

ALL(TreeNode treeNode, Map<String, Object> env, MiniTreeProgram program) {
  _continueForChildren(treeNode, env, program);
}

_continueFor(TreeNode treeNode, Map<String, Object> env, MiniTreeProgram program) {
    program.runOnTreeNode(treeNode, env);
}

_continueForChildren(TreeNode treeNode, Map<String, Object> env, MiniTreeProgram program) {
  for(TreeNode child in childrenOf(treeNode)) {
    program.runOnTreeNode(child, env);
  }
}






class MiniTreeProgram {

  Map<String, TreeFunctionAlternative> map;

  MiniTreeProgram(this.map);

  start(TreeNode treeNode) {
    runOnTreeNode(treeNode, {});
  }

  runOnTreeNode(TreeNode treeNode, Map<String, Object> env) {
    TreeFunctionAlternative alternative;
    if(
    treeNode != null && (
             (alternative = map[treeNode.nodeName]) != null
         // || (alternative = )
          || (alternative = map[      "ALL"      ]) != null
       )
    ) {

      alternative(treeNode, env, this);

    }
  }


}



