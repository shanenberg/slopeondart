import 'package:slopeondart/examples/SQLStuff/GrammarUtils/BNFGrammarPrint.dart';
import 'package:slopeondart/lib/util/GrammarUtilsIO.dart';
import 'package:slopeondart/lib/grammarparser/GrammarRuleExpression.dart';
import 'package:slopeondart/lib/grammarparser/tree/GrammarObject.dart';
import 'package:slopeondart/lib/grammarparser/tree/GrammarRuleObject.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleExpressionRuleRefOrScannerRef.dart';
import 'package:slopeondart/lib/grammarparser/tree/rule/GrammarRuleUnary.dart';


main() {
  GrammarAnalyzer grammarAnalyzer = new GrammarAnalyzer("lib\\examples\\SQLStuff\\FINALSQL.slogr");
  grammarAnalyzer.fillUsed();
  print("Used:");

  grammarAnalyzer.printUsed();
  print("");
  print("Not Used:");
  grammarAnalyzer.printNotUsed();
}

class GrammarAnalyzer {

  SLOPEGrammarObject grammarObject;

  List<String> usedRules = new List<String>();

  GrammarAnalyzer(String path) {
    this.grammarObject = parseGrammarFromPath(path);
  }


  fillUsed() {
    recurseIfNotExists(grammarObject.startRuleName);
  }

  printUsed() {
    print(usedRules);
  }

  printNotUsed() {
    for(var rule in grammarObject.rules) {
      if(!usedRules.contains(rule.name))
      print(rule.name);
    }
  }



  analyseRule(GrammarRuleExpression rule) {
    if (rule is GrammarRuleUnary) {
      analyseRule(rule.element);
    }
    else if (rule is GrammarRuleNAry) {
      for(var expr in rule.elements) {
        analyseRule(expr);
      }
    }
    else if (rule is GrammarRuleExpressionRuleRefOrScannerRef) {
      recurseIfNotExists(rule.token);

    }
  }



  recurseIfNotExists(String token) {
    if(!usedRules.contains(token)) {
      usedRules.add(token);
      GrammarRuleObject rule = grammarObject.getRule(token);
      if(rule == null) {
        print(token+" is null");
      } else
      analyseRule(grammarObject.getRule(token).body);
    }
  }



}