import 'package:slopeondart/examples/SQLStuff/GrammarUtils/BNFGrammarPrint.dart';
import 'package:slopeondart/lib/util/GrammarUtilsIO.dart';
import 'package:slopeondart/lib/grammarparser/tree/GrammarObject.dart';
import 'package:slopeondart/lib/grammarparser/tree/GrammarRuleObject.dart';

main() {
  SLOPEGrammarObject grammarObject = parseGrammarFromPath("lib\\examples\\SQLStuff\\SQL_BIG.slogr");

  printz(grammarObject);
}




printz(SLOPEGrammarObject grammarObject) {
  print('''SQL
    Rules: queryspecification
    
  ''');

  for(GrammarRuleObject rule in grammarObject.rules) {
    print('''${rule.name} => 
    
    
    ''');

  }
}