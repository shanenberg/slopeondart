SQLSelectStatement

Rules: selectStatement

  selectStatement =>
        "SELECT" fieldIdentifierList
        ("FROM" fromClause)?
        ("WHERE" condition)?
        ("GROUP" "BY" groupBy
            ("HAVING" condition)?)?
        ("ORDER" "BY" simpleIdentifierList)?
        (";")?.

  fieldIdentifierList =>
        fieldIdentifier ("AS" fieldIdentifier)? ("," fieldIdentifier ("AS" fieldIdentifier)?)*.

  simpleIdentifierList => fieldIdentifier ("," fieldIdentifier)*.
  fieldIdentifier => identifier (dot identifier)?.
  groupBy => fieldIdentifierList.
  fromClause => tableName (naturalJoin | joinOn)*.
  naturalJoin => "NATURAL" "JOIN" identifier.
  joinOn => "JOIN" identifier "ON" condition.
  condition => fieldIdentifier ("=" | "LIKE") (fieldIdentifier | stringLiteral).
  tableName => identifier.

Scanner:
  separator => SEPARATOR " ".
  dot => "\.".
  identifier => "([a-z]|[A-Z])([a-z]|[A-Z]|[0-9])*" BOTTOM.
  stringLiteral => "~"((^~"~~)|~~~~|~~~")*~"" BOTTOM.

Tests:
    _ "SELECT abc". _ "SELECT abc.x". _ "SELECT abc.x AS newName".
    _ "SELECT x, y". _ "SELECT asda FROM a".
    _ "SELECT aasd FROM aff NATURAL JOIN eaab".
    _ "SELECT aasd FROM aff NATURAL JOIN eaab".
    _ "SELECT aasd FROM aff NATURAL JOIN eaab".
    _ "SELECT aasd FROM aff NATURAL JOIN eaab WHERE a.a=b.b".
    _ "SELECT aasd FROM x JOIN y ON x.a=y.b WHERE a.a=b.b".
    _ "SELECT aasd FROM x JOIN y ON x.a=y.b WHERE a.a=b.b GROUP BY a, b".
    _ "SELECT aasd FROM x JOIN y ON x.a=y.b WHERE a.a=b.b GROUP BY a, b".
    _ "SELECT aasd FROM x WHERE a.x=~"a.x~"".
