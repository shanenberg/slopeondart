import 'package:test/test.dart';

import '../../automata/alphabet/tests/CharAlphabet_test.dart';
import '../StringTokenStream.dart';

main() {

  test("Element from StringStream", (){
    StringTokenStream stream = new StringTokenStream("ABCDEF");
    expect(stream.position, -1);
    expect(stream.getNext(), char('A'));
    expect(stream.position, 0);
    expect(stream.getNext(), char('B'));
    expect(stream.position, 1);
    expect(stream.getNext(), char('C'));
    expect(stream.position, 2);
    expect(stream.getNext(), char('D'));
    expect(stream.position, 3);
    expect(stream.getNext(), char('E'));
    expect(stream.position, 4);
    expect(stream.getNext(), char('F'));
    expect(stream.list, "ABCDEF");
    expect(stream.hasNext(), false);

  });

  test("SUCK", (){
    expect(true, true);
  });

}