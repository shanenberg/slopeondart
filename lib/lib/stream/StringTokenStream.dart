import '../automata/alphabet/CharAlphabet.dart';
import 'TokenStream.dart';

class StringTokenStream extends TokenStream<int, CharAlphabet> {

  String list;

  StringTokenStream(this.list) : super(new CharAlphabet());

  int getNext() {
    position = position + 1;
    int r = list.codeUnitAt(position);
    return r;
  }

  bool hasNext() {
    return list.length> position + 1;
  }

}