import '../automata/alphabet/Alphabet.dart';
import '../automata/alphabet/AlphabeticElement.dart';

abstract class TokenStream<TokenType, AlphabetType extends Alphabet<TokenType>>
    extends AlphabeticElement<TokenType, AlphabetType> {

  TokenStream(Alphabet alphabet) : super(alphabet);

  int position = -1;

  TokenType getNext();
  bool hasNext();
}
