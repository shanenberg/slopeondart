///Stream of tokens put together to create a single element of multiple tokens

import '../automata/alphabet/Alphabet.dart';
import 'TokenStream.dart';

class ListTokenStream<TokenType, AlphabetType extends Alphabet<TokenType>>
    extends TokenStream<TokenType, AlphabetType> {
  List<TokenType> t;

  ListTokenStream(Alphabet alphabet) : super(alphabet);

  TokenType getNext() {
    TokenType r = t[position];
    position = position + 1;
    return r;
  }

  bool hasNext() {
    return t.length> position + 1;
  }
}

