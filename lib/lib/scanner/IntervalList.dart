import '../automata/NFA.dart';
import '../automata/alphabet/Alphabet.dart';
import '../automata/transition/RangeTransition.dart';
import '../automata/transition/SymbolTransition.dart';
import '../automata/transition/Transition.dart';
import 'IntermediateScanner.dart';
///Interval List

import 'IntervalNode.dart';
import 'ScannerStateSet.dart';
import 'state/FAStateFactory.dart';
///Interval List


class IntervalList<TokenType, AlphabetType extends Alphabet<TokenType>> {
  AlphabetType alphabet;

  IntervalList(this.alphabet);

  ///Tokens mapped to their respective IntervalNode
  ///(An IntervalNode may be both opening and closing)
  Map<TokenType, IntervalNode<TokenType>> intervalNodes =
  new Map<TokenType, IntervalNode<TokenType>>();

  ///Add a startnode associated with a specific transition
  void addStartNode(TokenType token, Transition t) {
    if (!(intervalNodes.containsKey(token))) {
      intervalNodes[token] = new IntervalNode<TokenType>(token);
    }
    intervalNodes[token].startIntervals.add(t);
  }

  ///Add a endnode associated with a specific transition
  void addEndNode(TokenType token, Transition t) {
    if (!(intervalNodes.containsKey(token))) {
      intervalNodes[token] = new IntervalNode<TokenType>(token);
    }
    intervalNodes[token].endIntervals.add(t);
  }

  printIntervalNodes() {
    Map<TokenType, IntervalNode<TokenType>> n = intervalNodes;
    var tokens = alphabet.sort(n.keys);
    print("***Intervals:");

    for(var t in tokens) {
      IntervalNode aNode = n[t];
      print("+++++++");
      print(t.toString());
      print("Start:" + aNode.startIntervals.toString());
      print("END:" + aNode.endIntervals.toString());
    }

    print("***");
  }

  Set<ScannerStateSet> addToDEAFromNEA(IntermediateScannerDFA<TokenType, Alphabet> dfa,
      NFA<ScannerState, TokenType, Alphabet> nfa) {

    Set<ScannerStateSet> ret = new Set<ScannerStateSet>(); // the newly introduced states

    List<TokenType> sortedKeys = alphabet.sort(intervalNodes.keys);
    if (sortedKeys.length == 0) return ret;

    Set<Transition> currentOpenTransitions = new Set<Transition>();
    TokenType currentToken = sortedKeys[0];
    TokenType lastToken = sortedKeys[0];

    while (currentToken!=null) {
//      printIntervalNodes();
//      print("////////currentToken " + currentToken.toString());
//      print("openTransitions " + currentOpenTransitions.toString());

      Set<Transition> newOpeningTransitions = intervalNodes[currentToken].startIntervals;
      Set<Transition> newClosingTransitions = intervalNodes[currentToken].endIntervals;
      Set<Transition> transitionsToCloseBeforeCurrent = currentOpenTransitions.difference(newOpeningTransitions);

      // Close all transitions before current token
      if (alphabet.hasTokenDirectlyBefore(currentToken) && newOpeningTransitions.length>0 && currentOpenTransitions.length>0) {
        TokenType directlyBeforeToken = alphabet.getTokenDirectlyBefore(currentToken);
        Set<ScannerState> followUpStates = getFollowUpStates(nfa, currentOpenTransitions);
        ScannerStateSet followUpStateSet = new ScannerStateSet(followUpStates);
        ScannerStateSet aSet = dfa.getStateLike(followUpStateSet);
        if (aSet!=null) {
          followUpStateSet = aSet;
        } else {
          ret.add(followUpStateSet);
//          print("==============================> new state: " + followUpStateSet.toString());
        }

//        print("add range transition: " + lastToken.toString() + " " + directlyBeforeToken.toString() + " " + currentOpenTransitions.length.toString());
        dfa.addTransition(
            dfa.currentState,
            new RangeTransition<ScannerStateSet, TokenType, AlphabetType>(
                alphabet, lastToken, directlyBeforeToken, followUpStateSet));

        lastToken = currentToken;
      }

      /* BIS HIER HIN
         Alle zu schließenden Transitionen sind geschlossen....aber was ist mit den neu zu öffnenden
         und neu zu schliessenden Transitionen?
      */

      currentOpenTransitions.addAll(newOpeningTransitions);

//      print("current open at " + currentToken.toString() + " "+ currentOpenTransitions.toString());
      Set<Transition> transitionsToCloseNow = currentOpenTransitions.intersection(newClosingTransitions);
//      print("to close at " + currentToken.toString() + " " + transitionsToCloseNow.toString());


      if (transitionsToCloseNow.length>0) {
        Set<ScannerState> followUpStates = getFollowUpStates(nfa, currentOpenTransitions.union(transitionsToCloseNow));
        ScannerStateSet followUpStateSet = new ScannerStateSet(followUpStates);
        ScannerStateSet aSet = dfa.getStateLike(followUpStateSet);
        if (aSet!=null) {
          followUpStateSet = aSet;
        } else {
          ret.add(followUpStateSet);
//          print("==============================> new state: " + followUpStateSet.toString());
        }

//        print("followUpStates " + followUpStates.length.toString());
        if (lastToken == currentToken) {
//print("===add single Transition: " + currentToken.toString() + " " + transitionsToCloseNow.length.toString());
//print("===add single Transition: " + followUpStateSet.toString() + " length: " + followUpStateSet.states.length.toString());
          dfa.addTransition(
              dfa.currentState,
              new SymbolTransition<ScannerStateSet, TokenType, AlphabetType>(
                  alphabet, currentToken, followUpStateSet));
        } else {
//          print("add range transition: " + lastToken.toString() + " " + currentToken.toString() + " " + currentOpenTransitions.length.toString());
          dfa.addTransition(
              dfa.currentState,
              new RangeTransition<ScannerStateSet, TokenType, AlphabetType>(
                  alphabet, lastToken, currentToken, followUpStateSet));
        }

        if (currentOpenTransitions.isNotEmpty) {
          lastToken = alphabet.getTokenDirectlyAfter(currentToken);
        } else {
          lastToken = currentToken;
        }
      }

      currentOpenTransitions.removeAll(transitionsToCloseNow);

//      print("open trans: " + currentOpenTransitions.length.toString());
//      print("###########");


      if (hasNextToken(currentToken)) {
        currentToken = nextTokenAfter(currentToken);
      } else {
        currentToken = null;
      }

      if (currentOpenTransitions.isEmpty) {
        lastToken = currentToken;
      }

    } // end while loop

    if (currentOpenTransitions.length>0)
      throw new Exception("There is something wrong...cannot have not closed transitions");

    return ret;
  }

  TokenType nextTokenAfter(TokenType currentToken) {
    List<TokenType> sortedKeys = alphabet.sort(intervalNodes.keys);
    int index = sortedKeys.indexOf(currentToken);
    return sortedKeys[index + 1];
  }

  Set<ScannerState> getFollowUpStates(
      NFA<ScannerState, TokenType, Alphabet> nfa,
      Set<Transition<ScannerState, TokenType, AlphabetType>> transitions) {

    Set<ScannerState> currentStates = nfa.currentStates;
    Set<ScannerState> resultSet = new Set<ScannerState>();
    for (Transition<ScannerState, TokenType, AlphabetType> aTransition
    in transitions) {
      resultSet.add(aTransition.follower);
    }

    nfa.currentStates = resultSet;
    nfa.doEpsilonTransitions();
    resultSet = nfa.currentStates;

    nfa.currentStates = currentStates;
    return resultSet;
  }

  /*
    Returns true if there are interval nodes registered after this one.
  */
  bool hasNextToken(TokenType currentToken) {
    List<TokenType> sortedKeys = alphabet.sort(intervalNodes.keys);
    int index = sortedKeys.indexOf(currentToken);
    return sortedKeys.length > index + 1;
  }

  String toString() {
    String ret = "IntervalList: \n";
    List<TokenType> sortedKeys = alphabet.sort(intervalNodes.keys);

    for(TokenType aKey in sortedKeys) {
      ret =
          ret + " key: " + aKey.toString() +
              " start[" + intervalNodes[aKey].startIntervals.length.toString() + "]" +
              " end["  + intervalNodes[aKey].endIntervals.length.toString() + "]\n";

    }
    return ret;
  }
}
