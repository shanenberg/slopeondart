import '../automata/alphabet/Alphabet.dart';
import 'state/FAStateFactory.dart';

class ScannerToken<TokenType, AlphabetType extends Alphabet<TokenType>> {
  ScannerState scannerState;
  List<TokenType> singleTokens = new List<TokenType>();

  String toString() {
    return new String.fromCharCodes(singleTokens as List);
  }
}
