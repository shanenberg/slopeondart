import 'ScannerStateSet.dart';

class ScannerStateSetSet {
  Set<ScannerStateSet> setOfSetsofStates = new Set<ScannerStateSet>();

  bool contains(ScannerStateSet states) {
    for (ScannerStateSet stateIDs in setOfSetsofStates) {
      if (stateIDs.equals(states)) return true;
    }
    return false;
  }

  void add(ScannerStateSet s) {
    if (!this.contains(s)) setOfSetsofStates.add(s);
  }

  void addAll(Set<ScannerStateSet> states) {
    for(ScannerStateSet aState in states) {
      if (!contains(aState))
        add(aState);
    }
  }
}
