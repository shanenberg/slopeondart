import '../automata/alphabet/Alphabet.dart';
import '../automata/regex/RegEx.dart';

///Wrapper of RegEx and a "separator-flag"
class RegExWithKind<TokenType, AlphabetType extends Alphabet<TokenType>> {
  RegEx<TokenType, AlphabetType> regex;
  bool isSeparator;
  RegExWithKind(this.regex, this.isSeparator);
}
