import '../automata/DFA.dart';
import '../automata/alphabet/Alphabet.dart';
import '../automata/transition/Transition.dart';
import 'ScannerStateSet.dart';

class IntermediateScannerDFA<TokenType,AlphabetType extends Alphabet<TokenType>>
    extends DFA<ScannerStateSet, TokenType, AlphabetType> {

  IntermediateScannerDFA(dynamic alphabet) : super(alphabet);

  ScannerStateSet getStateLike(ScannerStateSet inState) {
    for (ScannerStateSet aState in transitionTable.keys) {
      if (aState.equals(inState)) return aState;
    }
    return null;
  }

  void addState(ScannerStateSet aState) {
    ScannerStateSet s = this.getStateLike(aState);
    if (s == null) {
      this.transitionTable[aState] =
      new List<Transition<ScannerStateSet, TokenType, AlphabetType>>();
    }
  }

  void addTransition(ScannerStateSet fromState,
      Transition<ScannerStateSet, TokenType, AlphabetType> transition) {

    if (getStateLike(fromState)==null) {
//      print ("+++++++++++ new state");
      transitionTable[fromState] = new List<Transition<ScannerStateSet, TokenType, AlphabetType>>();
    }

    if (getStateLike(transition.follower)==null) {
      transitionTable[transition.follower] = new List<Transition<ScannerStateSet, TokenType, AlphabetType>>();
    }

    transitionTable[fromState].add(transition);
  }

  String toString() {
    String ret = "intermediate DFA: StartState{" + currentState.toString() + "}\n";
    for(ScannerStateSet aState in transitionTable.keys) {
      ret = ret + "\n" + aState.toString() + "\n";
      for (Transition aTransition in transitionTable[aState]) {
        ret = ret + "\n" + aTransition.toString();
      }
    }
    return ret;
  }

}