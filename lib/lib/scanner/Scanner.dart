import '../automata/NFA.dart';
import '../automata/alphabet/Alphabet.dart';
import '../stream/TokenStream.dart';
import 'IntermediateScanner.dart';
import 'RegExWithKind.dart';
import 'ScannerOutputStream.dart';
import 'ScannerStateSet.dart';
import 'ScannerStateSetSet.dart';
import 'ScannerToken.dart';
import 'state/FAStateFactory.dart';
import 'state/RegExState.dart';
import 'state/ScannerStateFactory.dart';
import 'state/SeparatorState.dart';

/// Scanner that can create NFAs and DFAs out of regular expressions on specific token-streams
class Scanner<TokenType, AlphabetType extends Alphabet<TokenType>> {
  Scanner(this.inputStream, this.tokenRegExs);

  TokenStream<TokenType, AlphabetType> inputStream;
  List<RegExWithKind<TokenType, AlphabetType>> tokenRegExs;

  ScannerOutputStream<ScannerToken<TokenType, AlphabetType>> doScanning() {
    IntermediateScannerDFA<TokenType, Alphabet> dfa = this.createIntermediateDFA();

    ScannerStateSet startState = dfa.currentState;
    ScannerStateSet lastState = null;
    ScannerStateSet currentState = dfa.currentState;
    bool hasStarted = false;

//    print(currentState.toString());
//    print(dfa.isInEndState());

    ScannerOutputStream<ScannerToken<TokenType, AlphabetType>> outputStream =
    new ScannerOutputStream<ScannerToken<TokenType, AlphabetType>>(inputStream.alphabet);

    List<TokenType> tokens = new List<TokenType>();

    while(inputStream.hasNext()) {
      hasStarted=true;
      ScannerStateSet currentState = dfa.currentState;
      TokenType element = inputStream.getNext();

//print("****************");
//print(element);
//print("accepts: " + dfa.acceptsInput(element).toString());
//print(dfa.toString());
      if (dfa.acceptsInput(element)) {
        dfa.goNext(element);
        tokens.add(element);
        lastState = dfa.currentState;
      } else {

//        print("++++++++++++++++++++++++++++++++++++");
//        print(dfa);

        if(lastState==null){
          throw new Exception("Scanner exception at " + inputStream.position.toString());
        }

        if(!lastState.isAcceptor()){
//          inputstream.printStream();
          throw new Exception("Scanner exception at " + inputStream.position.toString());
        }

        // now wrap all chars into a OutputTokenType and add to output
        // if it is a separator, just throw stuff away

        if (!lastState.isSeparator()) {
          ScannerToken acceptedToken = lastState.createAcceptedToken(tokens);
//print("added" + tokens.toString());
          outputStream.stream.add(acceptedToken);
//          print("add to output " + acceptedToken.toString());
        }
        tokens = new List<TokenType>();

        // let's check, whether the current input can still be handled
        dfa.currentState = startState;
        if (!dfa.acceptsInput(element)) {
          throw new Exception("Scanner exception at" + inputStream.position.toString());
        } else {
          tokens.add(element);
          dfa.goNext(element);
          lastState = dfa.currentState;
        }

      }
    }

    // In case the string was empty, it needs to be checked, whether
    // the startState was already an acceptor
    if (!hasStarted && startState.isAcceptor()) {
      return outputStream;
    }

    if((!lastState.isAcceptor())){
      throw new Exception("Scanner exception at " + inputStream.position.toString()); // + inputStream.list[inputStream.position]);
    }

    // handle last word if nothing follows
    if (!lastState.isSeparator()) {
      ScannerToken acceptedToken = lastState.createAcceptedToken(tokens);
      outputStream.stream.add(acceptedToken);
//      print("add to output " + acceptedToken.toString());
    }

//    print("******************");
    return outputStream;
  }

  NFA<RegExState, TokenType, Alphabet> createNFA() {
    NFA<ScannerState, TokenType, AlphabetType> nfa =
    new NFA<ScannerState, TokenType, AlphabetType>(inputStream.alphabet);

    ScannerStateFactory stateFactory = new ScannerStateFactory();
    ScannerState startState = new ScannerState(0);
    nfa.currentStates.add(startState);

    for (int i = 0; i < tokenRegExs.length; i++) {
      ScannerState endState = tokenRegExs[i].isSeparator
          ? new SeparatorState(i)
          : new ScannerState(i);
      endState.regex = tokenRegExs[i].regex;
      endState.regExString = endState.regex.toString();
      endState.isEndState = true;
      tokenRegExs[i].regex.addToNFA(nfa, startState, endState, stateFactory);
      nfa.endStates.add(endState);
    }
    nfa.doEpsilonTransitions();
    return nfa;
  }

  IntermediateScannerDFA<TokenType, Alphabet> createIntermediateDFA() {
    // 1. Create NFA
    NFA<ScannerState, TokenType, AlphabetType> nfa = this.createNFA();
    nfa.doEpsilonTransitions();

    IntermediateScannerDFA<TokenType, AlphabetType> dfa =
    new IntermediateScannerDFA<TokenType, AlphabetType>(nfa.alphabet);

    // 2. Give each state an ID
    int id = 0;
    for (ScannerState aState in nfa.transitionTable.keys) {
      aState.stateNumber = id++;
    }

    ScannerStateSet currentStateSet = new ScannerStateSet(nfa.currentStates);
    ScannerStateSet startState = currentStateSet;
    dfa.addState(currentStateSet);
    dfa.currentState = startState;

    // 3. Create state sets used for iteration
    ScannerStateSetSet unhandledStates = new ScannerStateSetSet();
    unhandledStates.add(currentStateSet);

    // 4. Create result DEA
    while (unhandledStates.setOfSetsofStates.isNotEmpty) {
      ScannerStateSet currentSet = unhandledStates.setOfSetsofStates.first;
      dfa.currentState = currentSet;
      currentSet.createIntervalList(nfa);
      Set<ScannerStateSet> newScannerStateSets =
      currentSet.intervalList.addToDEAFromNEA(dfa, nfa);
      unhandledStates.addAll(newScannerStateSets);
//      print("***************************+");
//      print("DEA: " + dfa.transitionTable.keys.length.toString());
      unhandledStates.setOfSetsofStates.remove(currentSet);

//      if (unhandledStates.setOfSetsofStates.isNotEmpty) {
//        print(currentSet.intervalList.toString());
////        printIntervalNodes();
//        throw "dummy";
//
//      }

    }



    dfa.currentState = startState;
    return dfa;
  }
}
