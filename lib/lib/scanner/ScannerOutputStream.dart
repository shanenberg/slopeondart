import '../automata/alphabet/Alphabet.dart';
import 'ScannerToken.dart';

class ScannerOutputStream<TokenType> {

  ScannerOutputStream(Alphabet this.alphabet);

  Alphabet alphabet;

  List<TokenType> stream = new List<TokenType>();

  int position = -1;

  TokenType getNext() {
    position = position + 1;
    TokenType r = stream[position];
    return r;
  }

  bool hasNext() {
    return stream.length> position + 1;
  }

  void printStream() {
    for(int i=0; i<stream.length;i++) {
      print("token: " + stream[i].toString() + " " + (stream[i] as ScannerToken).scannerState.regExString);
    }
  }

  TokenType elementAt(int i) {
    return stream[i];
  }

  TokenType currentElement() {
    return stream[position];
  }

}
