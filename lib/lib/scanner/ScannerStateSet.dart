import 'dart:collection';

import '../automata/NFA.dart';
import '../automata/alphabet/Alphabet.dart';
import '../automata/transition/Transition.dart';
import 'IntervalList.dart';
import 'ScannerToken.dart';
import 'state/FAStateFactory.dart';
import 'state/RegExState.dart';

class ScannerStateSet<TokenType, AlphabetType extends Alphabet<TokenType>> {
  HashSet<int> stateIDs = new HashSet<int>();
  Set<ScannerState> states = new Set<ScannerState>();
  IntervalList<TokenType, AlphabetType> intervalList;
  Map<Transition, ScannerStateSet> transitions =
  new Map<Transition, ScannerStateSet>();

  ScannerStateSet([Set<ScannerState> inStates]) {
    if (inStates != null) {
      for (ScannerState aState in inStates) {
        stateIDs.add(aState.stateNumber);
      }
      states.addAll(inStates);
    }
  }

  bool add(ScannerState inState) {
    stateIDs.add(inState.stateNumber);
  }

  bool addAll(Iterable<ScannerState> inStates) {
    for (ScannerState aState in inStates) {
      stateIDs.add(aState.stateNumber);
    }
  }

  bool equals(Object o) {
    if (!(o is ScannerStateSet)) return false;
    ScannerStateSet other = o;

    if (stateIDs.length != other.stateIDs.length) {
      return false;
    }

    for (int aStateNum in other.stateIDs) {
      if (!stateIDs.contains(aStateNum)) {
        return false;
      }
    }
    return true;
  }

  void createIntervalList(NFA<RegExState, TokenType, Alphabet> nfa) {
    intervalList = new IntervalList(nfa.alphabet);
    for (RegExState aState in states) {
      for (Transition aTransition in nfa.transitionTable[aState]) {
//print("add to intervalList: " + aTransition.toString());
        aTransition.addToIntervalList(intervalList);
      }
    }
//    print(intervalList);
  }

  String toString() {
    String ret = "ScannerStateSet: ";
    for (ScannerState aState in states) {
      ret = ret + " " + aState.stateNumber.toString();
    }
    return ret;
  }

  bool isAcceptor() {
    for(ScannerState aState in states) {
      if (aState.isEndState) return true;
    }
    return false;
  }

  ScannerState acceptingState() {
    ScannerState ret = null;
    for(ScannerState aState in states) {
      if (aState.isEndState) {
        if(ret==null) {
          ret = aState;
        } else {
          if (ret.priority>aState.priority) {
            ret = aState;
          }
        }
      }
    }
    return ret;
  }


  ScannerToken createAcceptedToken(List tokens) {
    ScannerToken ret = new ScannerToken();
    ret.singleTokens.addAll(tokens);
    ret.scannerState = this.acceptingState();
    return ret;
  }

  bool isSeparator() {
    return acceptingState().isSeparator();
  }
}
