import '../../automata/regex/RegEx.dart';
///Finite Automata State Factory
///Can create RegExStates

import 'RegExState.dart';
///Finite Automata State Factory
///Can create RegExStates


class FAStateFactory {
  RegExState createState() => new RegExState(null);
}

///Regular Expression that can be converted to an NFA


///States with priorities (?)
class ScannerState extends RegExState {
  int priority = -1;
  ScannerState(this.priority, [int aNumber]) : super(aNumber);
  RegEx regex;
  String regExString;
  bool isEndState = false;

  bool isSeparator() {
    return false;
  }

}
