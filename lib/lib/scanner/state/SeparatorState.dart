import 'FAStateFactory.dart';

///Separator State
class SeparatorState extends ScannerState {
  SeparatorState(int priority) : super(priority);

  bool isSeparator() {
    return true;
  }
}
