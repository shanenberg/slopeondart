import 'FAStateFactory.dart';

///Factory of scanner states
class ScannerStateFactory extends FAStateFactory {
  ScannerState createState() => new ScannerState(-1);
}
