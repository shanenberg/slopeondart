import '../automata/transition/Transition.dart';

class IntervalNode<TokenType> {
  IntervalNode(this.token);
  TokenType token;
  Set<Transition> startIntervals = new Set<Transition>();
  Set<Transition> endIntervals = new Set<Transition>();
}
