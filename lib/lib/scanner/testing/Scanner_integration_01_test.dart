import 'package:test/test.dart';

import '../../automata/alphabet/CharAlphabet.dart';
import '../../automata/regex/RegEx.dart';
import '../../stream/StringTokenStream.dart';
import '../RegExWithKind.dart';
import '../Scanner.dart';
import '../ScannerOutputStream.dart';

regExKeyword(String aKeyword, isSeparator) {
  CharAlphabet charAlphabet = new CharAlphabet();
  return new RegExWithKind( RegEx.fromKeyword(charAlphabet, aKeyword), isSeparator);
}

main() {
  test("Scanning - simple Keywords (one letter)", () {
    StringTokenStream stringStream = new StringTokenStream("AB");

    Scanner scanner = new Scanner(stringStream, [regExKeyword("A", false), regExKeyword("B", false)]);
    ScannerOutputStream result = scanner.doScanning();

    expect(result.stream.length, 2);
    expect(result.stream[0].toString(), "A");
    expect(result.stream[1].toString(), "B");
  });

  test("Scanning - simple Keywords (multiLetter)", () {
    StringTokenStream stringStream = new StringTokenStream("ABCD");
    Scanner scanner = new Scanner(stringStream, [regExKeyword("AB", false), regExKeyword("CD", false)]);

    ScannerOutputStream result = scanner.doScanning();

    expect(result.stream.length, 2);
    expect(result.stream[0].toString(), "AB");
    expect(result.stream[1].toString(), "CD");

  });

  test("Scanning - simple Keywords (multiLetter with Separator)", () {
    StringTokenStream stringStream = new StringTokenStream("EFCDAB");
    Scanner scanner = new Scanner(stringStream, [regExKeyword("AB", false), regExKeyword("CD", true), regExKeyword("EF", false)]);

    ScannerOutputStream result = scanner.doScanning();

    expect(result.stream.length, 2);
    expect(result.stream[0].toString(), "EF");
    expect(result.stream[1].toString(), "AB");
  });



}