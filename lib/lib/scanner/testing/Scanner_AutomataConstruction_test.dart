import 'package:test/test.dart';

import '../../automata/NFA.dart';
import '../../automata/alphabet/tests/CharAlphabet_test.dart';
import '../../automata/regex/tests/_utils.dart';
import '../../stream/StringTokenStream.dart';
import '../Scanner.dart';

main() {

  /*
      Simple NFA: 2 different inputs, hence just two end states and one start state.
      => 3 states
   */
  test("Scanner - createNEA(1)", () {
    StringTokenStream stringStream = new StringTokenStream("AB");

    Scanner scanner = new Scanner(stringStream, [regExKeyword("A", false), regExKeyword("B", false)]);
    NFA nfa = scanner.createNFA();

    expect(nfa.endStates.length, 2);
    expect(nfa.allStatesInFA().length, 3);
    expect(nfa.acceptsInput(char("A")), true);
    expect(nfa.acceptsInput(char("B")), true);
  });

  /*
      NFA: One overlapping token "A". On "A" has two followers, plus one "B"
      => 4 states
   */
  test("Scanner - createNEA(2)", () {
    StringTokenStream stringStream = new StringTokenStream("AB");

    Scanner scanner = new Scanner(stringStream, [regExKeyword("A", false), regExKeyword("AB", false)]);
    NFA nfa = scanner.createNFA();

    expect(nfa.endStates.length, 2);
    expect(nfa.allStatesInFA().length, 4);
    expect(nfa.acceptsInput(char("A")), true);
    expect(nfa.acceptsInput(char("B")), false);
  });

  /*
      NFA: One overlapping token "A". On "A" has two followers, plus one "B"
      => 4 states
   */
  test("Scanner - createNEA(3)", () {
    StringTokenStream stringStream = new StringTokenStream("AB");

    Scanner scanner = new Scanner(stringStream, [
      anyExceptRegEx([char("A"), char("B")], false),
      anyExceptRegEx([char("A"), char("C")], false)]);

    NFA nfa = scanner.createNFA();

    expect(nfa.endStates.length, 2);
    expect(nfa.allStatesInFA().length, 3);
    expect(nfa.acceptsInput(char("A")), false);
    expect(nfa.acceptsInput(char("B")), true);

  });

}