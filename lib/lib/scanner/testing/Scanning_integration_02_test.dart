import 'package:test/test.dart';

import '../../automata/alphabet/CharAlphabet.dart';
import '../../automata/alphabet/tests/CharAlphabet_test.dart';
import '../../automata/regex/RegExAnyExceptToken.dart';
import '../../automata/regex/RegExConcatenation.dart';
import '../../automata/regex/RegExSingleToken.dart';
import '../../stream/StringTokenStream.dart';
import '../RegExWithKind.dart';
import '../Scanner.dart';

alphabet() {return new CharAlphabet();}

Stream(aString) { return new StringTokenStream(aString); }

AND(r1, r2) {return new RegExConcatenation(alphabet(), r1, r2);}
TOKEN(aString) {return new RegExSingleToken(alphabet(), char(aString));}
KIND(r) {return new RegExWithKind(r, false);}
SEPARATOR(r) {return new RegExWithKind(r, true);}

ANYWITHOUT(chars) {return new RegExAnyExceptToken(alphabet(), chars);}
tokens(d) {
  var e = new Scanner(Stream(d[0]), d[1]).doScanning().stream;
  List ret = new List();
  for (int i = 0; i < e.length; i++) {
    ret.add(e[i].toString());
  }
  return ret;
}

 void main() {

  // Check whether Word FFa leads to tokens F and Fa
   test("Scanning FFa(Fa/F)", () {
     var d = ["FFa", [ KIND(AND(TOKEN("F"), TOKEN("a"))), KIND(TOKEN("F"))]];
     expect(tokens(d), ["F", "Fa"]);
   });

   test("Scanning FFa(Fa/F)", () {
     var d = ["FFa", [ KIND(TOKEN("F")), KIND(AND(TOKEN("F"), TOKEN("a")))]];
     expect(tokens(d), ["F", "Fa"]);
   });

   test("Scanning FFa(Fa/F)", () {
     var d = ["FFa", [ KIND(ANYWITHOUT([char("K")])), KIND(AND(TOKEN("F"), TOKEN("a")))]];
     expect(tokens(d), ["F", "Fa"]);
   });

   test("Scanning FFa(Fa/F)", () {
     var d = ["A   A A", [ KIND(TOKEN("A")), SEPARATOR(TOKEN(" "))]];
     expect(tokens(d), ["A", "A", "A"]);
   });

   test("Scanning FFa(Fa/F)", () {
     var d = ["Fa   F F",
        [ KIND(AND(TOKEN("F"), TOKEN("a"))), KIND(TOKEN("F")), SEPARATOR(TOKEN(" "))]];
     expect(tokens(d), ["Fa", "F", "F"]);
   });


//   test("Scanning FFa(Fa/F)", () {
//     var d = ["FFa",
//     KIND(TOKEN("F")), KIND(AND(TOKEN("F"), TOKEN("a")))]
//     ];
//     expect(tokens(d), ["F", "Fa"]);
//   });
//
//   test("Scanning FFa(F/Fa)", () {
//     var s = new Scanner(
//         Stream("FFa"),
//         [ KIND(TOKEN("F")), KIND(AND(TOKEN("F"), TOKEN("a")))])
//         .doScanning()
//         .stream;
//
//     expect(s.length, 2);
//     expect(s[0].toString() == "F", true);
//     expect(s[1].toString() == "Fa", true);
//   });
// }
//      //  test("Scanning 06 x1", () {
//
//    var tokenStream = new StringTokenStream("FFa");
//    var regExKinds = [
//      KIND(ANYWITHOUT(["K"])),
//      KIND(AND(TOKEN("F"), TOKEN("a")))
//
//    ];
//    Scanner scanner = new Scanner(tokenStream, regExKinds);
//
////    print(scanner.createIntermediateDFA().toString());
//    ScannerOutputStream<ScannerToken> outputStream = scanner.doScanning();
//
//    expect(outputStream.stream.length,2);
//    expect(outputStream.stream[0].toString()=="F", true);
//    expect(outputStream.stream[1].toString()=="Fa", true);
////    expect(outputStream.stream[0].scannerState.regExString == "F", true);
//  });
//
//  test("Scanning 05 x", () {
//
//    var tokenStream = new StringTokenStream("A A    A ");
//    var regExKinds = [
//      SEPARATOR(TOKEN(" ")),
//      KIND(TOKEN("A"))
//
//    ];
//    Scanner scanner = new Scanner(tokenStream, regExKinds);
//
//    ScannerOutputStream<ScannerToken> outputStream = scanner.doScanning();
//    expect(outputStream.stream[0].toString()=="A", true);
//    expect(outputStream.stream[1].toString()=="A", true);
//    expect(outputStream.stream[2].toString()=="A", true);
//  });
//
//  test("Scanning 05 x2", () {
//
//    var tokenStream = new StringTokenStream("A AZ    A ");
//    var regExKinds = [
//      SEPARATOR(TOKEN(" ")),
//      KIND(KLEENE(ANYWITHOUT(["Z", " "]))),
//      KIND(TOKEN("Z")),
//
//    ];
//
//    Scanner scanner = new Scanner(tokenStream, regExKinds);
//
//    ScannerOutputStream<ScannerToken> outputStream = scanner.doScanning();
//    expect(outputStream.stream.length, 4);
//    expect(outputStream.stream[0].toString(), "A");
//    expect(outputStream.stream[1].toString()=="A", true);
//    expect(outputStream.stream[2].toString(), "Z");
//  });
//
//  test("Scanning 05 x", () {
//
//    var tokenStream = new StringTokenStream("A A    A ");
//    var regExKinds = [
//      SEPARATOR(TOKEN(" ")),
//      KIND(TOKEN("A"))
//
//    ];
//    Scanner scanner = new Scanner(tokenStream, regExKinds);
//
//    ScannerOutputStream<ScannerToken> outputStream = scanner.doScanning();
//    expect(outputStream.stream[0].toString()=="A", true);
//    expect(outputStream.stream[1].toString()=="A", true);
//    expect(outputStream.stream[2].toString()=="A", true);
//  });
//
////  test("Scanning 05", () {
////
////    var tokenStream = new StringTokenStream("AAAX Z    AXZ");
////    var regExKinds = [
////      SEPARATOR(TOKEN(" ")),
////      KIND(TOKEN("Z")),
////      KIND(AND(KLEENE(ANYWITHOUT(["C", "D"])), TOKEN("X")))
////
////    ];
////    Scanner scanner = new Scanner(tokenStream, regExKinds);
////
////    ScannerOutputStream<ScannerToken> outputStream = scanner.doScanning();
////
////    expect(
////        outputStream.stream.length,4);
////  });
//
//  test("Scanning 03", () {
//
//    var tokenStream = new StringTokenStream("AAAXAXAX");
//    var regExKinds = [
//      KIND(AND(KLEENE(ANYWITHOUT(["C", "D", "X"])), TOKEN("X")))
//    ];
//    Scanner scanner = new Scanner(tokenStream, regExKinds);
//
//    ScannerOutputStream<ScannerToken> outputStream = scanner.doScanning();
//    expect(outputStream.stream.length, 3);
//    expect(
//        outputStream.stream[0].scannerState.regex.equals(AND(KLEENE(ANYWITHOUT(["C", "D", "X"])), TOKEN("X"))),
//        true);
//    expect(outputStream.stream[0].toString()=="AAAX",true);
//    expect(outputStream.stream[1].toString()=="AX",true);
//    expect(outputStream.stream[2].toString()=="AX",true);
//
//
//  });
//
//  test("Scanning 02", () {
//
//    var tokenStream = new StringTokenStream("AAAX");
//    var regExKinds = [
//      KIND((AND(KLEENE(ANYWITHOUT(["C", "D"])), TOKEN("X"))))
//    ];
//    Scanner scanner = new Scanner(tokenStream, regExKinds);
//
//    ScannerOutputStream outputStream = scanner.doScanning();
//    expect(outputStream.stream.length, 1);
//  });
//
//  test("Scanning 01", () {
//
//    var tokenStream = new StringTokenStream("A");
//    var regExKinds = [
//      KIND((OR(ANYWITHOUT(["C", "D"]), TOKEN("C"))))
//    ];
//    Scanner scanner = new Scanner(tokenStream, regExKinds);
//
//    ScannerOutputStream outputStream = scanner.doScanning();
//    expect(outputStream.stream.length, 1);
//  });
//
//  test("Scanner intervalList.addToDEAFromNEA ANYWithout 03", () {
//    Scanner scanner = new Scanner(new ListTokenStream(aCharAlphabet()), [
//      KIND((OR(ANYWITHOUT(["C", "D"]), TOKEN("C"))))
//    ]);
//
//    DFA dfa = scanner.createIntermediateDFA();
//
//    expect(dfa.transitionTable.keys.length, 3);
//
//    var startState = dfa.currentState;
//
//    accepts(dfa, "A");
//    accepts(dfa, "B");
//    accepts(dfa, "C");
//    acceptsNot(dfa, "D");
//    accepts(dfa, "E");
//    accepts(dfa, "Z");
//
//  });
//
//  test("Scanner intervalList.addToDEAFromNEA ANYWithout 01", () {
//    Scanner scanner = new Scanner(new ListTokenStream(aCharAlphabet()), [
//      KIND((ANYWITHOUT(["C"])))
//    ]);
//
//    DFA dfa = scanner.createIntermediateDFA();
//
//    expect(dfa.transitionTable.keys.length, 2);
//
//    var startState = dfa.currentState;
//
//    accepts(dfa, "A");
//    accepts(dfa, "B");
//    acceptsNot(dfa, "C");
//    accepts(dfa, "D");
//
//  });
//
//
//  test("Scanner intervalList.addToDEAFromNEA 08", () {
//    Scanner scanner = new Scanner(new ListTokenStream(aCharAlphabet()), [
//      KIND((OR(TOKEN("X"), TOKEN("A"))))
//    ]);
//
//    DFA dfa = scanner.createIntermediateDFA();
//
//    expect(dfa.transitionTable.keys.length, 3);
//
//    var aList = dfa.allTransitionsFromCurrent().toList();
//
//    var startState = dfa.currentState;
//
//    // DFA accepts A and X
//    expect(dfa.allTransitionsFromCurrent().length, 2);
//    accepts(dfa, "A");
//    accepts(dfa, "X");
//
//    // After X, it does not accept A or X any longer
//    dfa.goNext("X".codeUnits[0]);
//    expect(dfa.acceptsInput("A".codeUnits[0]), false);
//    expect(dfa.acceptsInput("X".codeUnits[0]), false);
//
//    // After A, it does not accept A or X any longer
//    dfa.currentState = startState;
//    expect(dfa.allTransitionsFromCurrent().length, 2);
//    expect(dfa.acceptsInput("A".codeUnits[0]), true);
//    expect(dfa.acceptsInput("X".codeUnits[0]), true);
//
//  });
//
//  test("Scanner intervalList.addToDEAFromNEA 07", () {
//    Scanner scanner = new Scanner(new ListTokenStream(aCharAlphabet()), [
//      KIND(KLEENE(AND(TOKEN("X"), TOKEN("A"))))
//    ]);
//
//    DFA dfa = scanner.createIntermediateDFA();
//    expect(dfa.allTransitionsFromCurrent().length, 1);
//    expect(dfa.acceptsInput("X".codeUnits[0]), true);
//    expect(dfa.acceptsInput("A".codeUnits[0]), false);
//
//    dfa.goNext("X".codeUnits[0]);
//    expect(dfa.acceptsInput("X".codeUnits[0]), false);
//    expect(dfa.acceptsInput("A".codeUnits[0]), true);
//
//    dfa.goNext("A".codeUnits[0]);
//
//    expect(dfa.transitionTable.keys.length, 3);
//
//  });
//
//  test("Scanner intervalList.addToDEAFromNEA 06 - RANGE 01", () {
//    Scanner scanner = new Scanner(new ListTokenStream(aCharAlphabet()), [
//      KIND(RANGE("A", "B"))
//    ]);
//
//    DFA dfa = scanner.createIntermediateDFA();
//
//    accepts(dfa, "A");
//    accepts(dfa, "B");
//
//  });
//
//  test("Scanner intervalList.addToDEAFromNEA 06", () {
//    Scanner scanner = new Scanner(new ListTokenStream(aCharAlphabet()), [
//      KIND(KLEENE(TOKEN("X")))
//    ]);
//
//    DFA dfa = scanner.createIntermediateDFA();
//    next(dfa, "X");
//    next(dfa, "X");
//    next(dfa, "X");
//    next(dfa, "X");
//
//    expect(dfa.transitionTable.keys.length, 2);
//
//  });
//
//  test("Scanner intervalList.addToDEAFromNEA 05", () {
//    Scanner scanner = new Scanner(new ListTokenStream(aCharAlphabet()), [
//      KIND(AND(TOKEN("X"), TOKEN("Z")))
//    ]);
//
//    DFA dfa = scanner.createIntermediateDFA();
//    accepts(dfa, "X");
//    acceptsNot(dfa, "Z");
//    next(dfa, "X");
//    acceptsNot(dfa, "X");
//    accepts(dfa, "Z");
//
//
//    expect(dfa.transitionTable.keys.length, 3);
//
//  });
//
//  test("Scanner intervalList.addToDEAFromNEA 03", () {
//    Scanner scanner = new Scanner(new ListTokenStream(aCharAlphabet()), [
//      KIND(RANGE("B", "G")),
//      KIND(RANGE("B", "H"))
//    ]);
//
//    DFA dfa = scanner.createIntermediateDFA();
//
//    expect(dfa.allTransitionsFromCurrent().length, 2);
//    acceptsNot(dfa, "A");
//    accepts(dfa, "B");
//
//
//  });
//
//
//  test("Scanner intervalList.addToDEAFromNEA 02", () {
//    Scanner scanner = new Scanner(new ListTokenStream(aCharAlphabet()), [
//      KIND(RANGE("C", "F")),
//      KIND(RANGE("A", "D"))
//    ]);
//
//    DFA dfa = scanner.createIntermediateDFA();
//    accepts(dfa, "A");
//    accepts(dfa, "B");
//    accepts(dfa, "C");
//    accepts(dfa, "D");
//    accepts(dfa, "E");
//    accepts(dfa, "F");
//    acceptsNot(dfa, "G");
//
//    var aList = dfa.allTransitionsFromCurrent().toList();
//    expect(dfa.allTransitionsFromCurrent().length, 3);
//
//  });
//
//  test("Scanner intervalList.addToDEAFromNEA 01", () {
//    Scanner scanner = new Scanner(new ListTokenStream(aCharAlphabet()), [
//      KIND(RANGE("B", "D"))
//    ]);
//
//    DFA dfa = scanner.createIntermediateDFA();
//
//    List<Transition<ScannerStateSet, int, CharAlphabet>> aList = dfa.allTransitionsFromCurrent().toList();
//
//    expect(dfa.allTransitionsFromCurrent().length, 1);
////    print(dfa.allTransitionsFromCurrent()[0]);
//    expect(dfa.allTransitionsFromCurrent()[0] is RangeTransition, true);
//    RangeTransition<ScannerStateSet, int, CharAlphabet> rt = dfa.allTransitionsFromCurrent()[0];
//    expect(rt.startToken == 'B'.codeUnitAt(0), true);
//    expect(rt.endToken == 'D'.codeUnitAt(0), true);
//
//  });
//
//
//  // A ((A|B)* C)
//  test("RegEx2NFA ANYExcept 01", () {
//    Scanner scanner = new Scanner(new ListTokenStream(aCharAlphabet()), [
//      KIND(
//          AND(
//              TOKEN("A"),
//              AND(
//                  KLEENE(OR(TOKEN("A"), TOKEN("B"))),
//                  TOKEN("C"))))
//    ]);
//
//    NFA nfa = scanner.createNFA();
//    expect(nfa.isInEndState(), false);
//    nfa.goNext(char("A"));
//    expect(nfa.isInEndState(), false);
//    nfa.goNext(char("A"));
//    expect(nfa.isInEndState(), false);
//    nfa.goNext(char("A"));
//    expect(nfa.isInEndState(), false);
//    nfa.goNext(char("B"));
//    expect(nfa.isInEndState(), false);
//    nfa.goNext(char("C"));
//    expect(nfa.isInEndState(), true);
//
//    try {
//      nfa.goNext(char("C"));
//      expect(true, false);
//    } catch (ex) {
//      expect(nfa.isInEndState(), true);
//    }
//  });
//
//  // AB[.\X]*
//  test("RegEx2NFA ANYExcept 01", () {
//    RegExSingleToken simple = new RegExSingleToken(aCharAlphabet(), char("X"));
//    Scanner scanner = new Scanner(new ListTokenStream(aCharAlphabet()), [
//      KIND(
//          AND(
//              TOKEN("A"),
//              AND(
//                  TOKEN("B"),
//                  KLEENE(new RegExAnyExceptToken(aCharAlphabet(), [char("X")])))))
//    ]);
//
//    NFA nfa = scanner.createNFA();
//    expect(nfa.isInEndState(), false);
//    nfa.goNext(char("A"));
//    expect(nfa.isInEndState(), false);
//    nfa.goNext(char("B"));
//    expect(nfa.isInEndState(), true);
//    nfa.goNext(char("C"));
//    expect(nfa.isInEndState(), true);
//    nfa.goNext(char("D"));
//    expect(nfa.isInEndState(), true);
//    nfa.goNext(char("E"));
//    expect(nfa.isInEndState(), true);
//    nfa.goNext(char("A"));
//    expect(nfa.isInEndState(), true);
//    nfa.goNext(char("B"));
//    expect(nfa.isInEndState(), true);
//    try {
//      nfa.goNext(char("X"));
//      expect(true, true);
//    } catch (ex) {
//      expect(nfa.isInEndState(), true);
//    }
//  });
//
//  test("RegEx2NFA 06", () {
//    RegExSingleToken simple = new RegExSingleToken(aCharAlphabet(), char("X"));
//    Scanner scanner = new Scanner(new ListTokenStream(aCharAlphabet()), [
//      KIND(
//          AND(
//              TOKEN("A"),
//              AND(
//                  TOKEN("B"),
//                  KLEENE(TOKEN("C")))))
//    ]);
//
//    NFA nfa = scanner.createNFA();
//    expect(nfa.isInEndState(), false);
//    nfa.goNext(char("A"));
//    expect(nfa.isInEndState(), false);
//    nfa.goNext(char("B"));
//    expect(nfa.isInEndState(), true);
//    nfa.goNext(char("C"));
//    expect(nfa.isInEndState(), true);
//    nfa.goNext(char("C"));
//    expect(nfa.isInEndState(), true);
//    nfa.goNext(char("C"));
//    expect(nfa.isInEndState(), true);
//  });
//
//  test("RegEx2NFA 05", () {
//    CharAlphabet alphabet = new CharAlphabet();
//
//    RegExSingleToken simple = new RegExSingleToken(alphabet, char("X"));
//    Scanner scanner = new Scanner(new ListTokenStream(aCharAlphabet()), [
//      KIND(
//          AND(TOKEN("A"),
//              KLEENE(TOKEN("B"))))
//    ]);
//
//    NFA nfa = scanner.createNFA();
//    expect(nfa.isInEndState(), false);
//    nfa.goNext(char("A"));
//    expect(nfa.isInEndState(), true);
//    nfa.goNext(char("B"));
//    expect(nfa.isInEndState(), true);
//    nfa.goNext(char("B"));
//    expect(nfa.isInEndState(), true);
//    nfa.goNext(char("B"));
//    expect(nfa.isInEndState(), true);
//    nfa.goNext(char("B"));
//    expect(nfa.isInEndState(), true);
//  });
//
//  test("RegEx2NFA 04", () {
//    RegExSingleToken simple = new RegExSingleToken(aCharAlphabet(), char("X"));
//    Scanner scanner = new Scanner(new ListTokenStream(aCharAlphabet()), [
//      KIND(KLEENE(TOKEN("B")))]);
//
//    NFA nfa = scanner.createNFA();
//    nfa.goNext(char("B"));
//    expect(nfa.isInEndState(), true);
//    nfa.goNext(char("B"));
//    expect(nfa.isInEndState(), true);
//    nfa.goNext(char("B"));
//    expect(nfa.isInEndState(), true);
//    nfa.goNext(char("B"));
//    expect(nfa.isInEndState(), true);
//  });
//
//  test("RegEx2NFA 03", () {
//    RegExSingleToken simple = new RegExSingleToken(aCharAlphabet(), char("X"));
//    Scanner scanner = new Scanner(new ListTokenStream(aCharAlphabet()), [
//      KIND(TOKEN("X")),
//      KIND(TOKEN("Y"))
//    ]);
//    NFA nfa = scanner.createNFA();
//
//    nfa.goNext(char("X"));
//    expect(nfa.currentStates.length, 1);
//    expect(nfa.currentStates.first.priority, 0);
//
//    nfa = scanner.createNFA();
//    nfa.goNext(char("Y"));
//    expect(nfa.currentStates.length, 1);
//    expect(nfa.currentStates.first.priority, 1);
//  });
//
//  test("RegEx2NFA 02", () {
//    RegExSingleToken simple = new RegExSingleToken(aCharAlphabet(), char("X"));
//    Scanner scanner = new Scanner(new ListTokenStream(aCharAlphabet()), [
//      KIND(TOKEN("X")),
//      KIND(TOKEN("Y"))
//    ]);
//    NFA nfa = scanner.createNFA();
//    nfa.goNext(char("X"));
//    expect(nfa.currentStates.length, 1);
//    expect(nfa.currentStates.first.priority, 0);
//
//    nfa = scanner.createNFA();
//    nfa.goNext(char("Y"));
//    expect(nfa.currentStates.length, 1);
//    expect(nfa.currentStates.first.priority, 1);
//  });
//
//  test("RegEx2NFA 01", () {
//    RegExSingleToken simple = new RegExSingleToken(aCharAlphabet(), char("X"));
//    Scanner scanner = new Scanner(
//        new ListTokenStream(aCharAlphabet()), [new RegExWithKind(simple, false)]);
//    NFA nfa = scanner.createNFA();
//    nfa.goNext(char("X"));
//
//    expect(nfa.currentStates.length, 1);
//    expect(nfa.currentStates.first.priority, 0);
//  });
//
//  test("DEA Range transition", () {
//    // A -t1-> B;
//    // B -t2-> C;
//
//    OrderedEnumAlphabet<t02T> alphabet =
//    new OrderedEnumAlphabet<t02T>(t02T.values);
//    DFA<t02S, t02T, OrderedEnumAlphabet<t02T>> dfa =
//    new DFA<t02S, t02T, OrderedEnumAlphabet<t02T>>(
//        new OrderedEnumAlphabet<t02T>(t02T.values));
//    dfa.currentState = t02S.A;
//    dfa.addTransition(t02S.A, new SymbolTransition(alphabet, t02T.t1, t02S.B));
//    dfa.addTransition(t02S.B, new SymbolTransition(alphabet, t02T.t2, t02S.C));
//    dfa.goNext(t02T.t1);
//    expect(dfa.currentState, t02S.B);
//    dfa.goNext(t02T.t2);
//    expect(dfa.currentState, t02S.C);
//  });
//
//  test("DEA Range transition", () {
//    // A -t1-> B;
//    // B -t2-> C;
//
//    var x = DFA;
//    OrderedEnumAlphabet<t01T> alphabet =
//    new OrderedEnumAlphabet<t01T>(t01T.values);
//
//    DFA<t01S, t01T, OrderedEnumAlphabet<t01T>> dfa =
//    new DFA<t01S, t01T, OrderedEnumAlphabet<t01T>>(
//        new OrderedEnumAlphabet<t01T>(t01T.values));
//    dfa.currentState = t01S.A;
//    dfa.addTransition(t01S.A, new SymbolTransition(alphabet, t01T.t1, t01S.B));
//    dfa.addTransition(t01S.B, new SymbolTransition(alphabet, t01T.t2, t01S.C));
//    dfa.goNext(t01T.t1);
//    expect(dfa.currentState, t01S.B);
//    dfa.goNext(t01T.t2);
//    expect(dfa.currentState, t01S.C);
//  });
//
//  test("NFA epsilon 01", () {
//    NFA<String, t01T, OrderedEnumAlphabet<t01T>> nfa = new NFA(aCharAlphabet());
//    nfa.currentStates = new Set()..addAll(["A"]);
//    nfa.addTransition("A", new SpontaneousTransition(aCharAlphabet(), "B"));
//    nfa.addTransition("B", new SpontaneousTransition(aCharAlphabet(), "C"));
//    nfa.addTransition("C", new SpontaneousTransition(aCharAlphabet(), "D"));
//    nfa.doEpsilonTransitions();
//    expect(nfa.currentStates.length, 4);
//  });
//
//  test("NFA epsion", () {
//    NFA nfa = new NFA(aCharAlphabet());
//    nfa.currentStates = new Set()..addAll([1, 2]);
//    nfa.addTransition(1, new SpontaneousTransition(aCharAlphabet(), 2));
//    nfa.addTransition(2, new SymbolTransition(aCharAlphabet(), char("Y"), 3));
//    nfa.goNext(char("Y"));
//    expect(nfa.currentStates.first, 3);
//  });
//
//  test("NFA allTransitions", () {
//    var nfa = new NFA(new CharAlphabet(Epsilon.instance));
//
//    nfa.currentStates = new Set()..addAll([1, 2]);
//    nfa.addTransition(1, symp("a", 2));
//    nfa.addTransition(2, symp("b", 1));
//    nfa.addTransition(1, symp("c", 3));
//    nfa.addTransition(2, symp("d", 3));
//    nfa.addTransition(3, symp("d", 4));
//
//    var ts = nfa.allTransitionsFromCurrent();
//    expect(ts.length, 4);
//    nfa.goNext(char("a"));
//    expect(nfa.currentStates.length, 1);
//    expect(nfa.currentStates.first, 2);
//  });
//    }
//  }
  }
