

typedef ATYPE Getter<ATYPE>();

bool SHALL_PRINT = false;
List<String> dingdonglist = new List();

//
//class T  {
//  static var HALLOX = TerminalLib.KEYWORD("hallox");
//  static var HALLO1 = TerminalLib.KEYWORD("hallo1");
//  static var HALLO2 = TerminalLib.KEYWORD("hallo2");
//  static var HALLO3 = TerminalLib.KEYWORD("hallo3");
//  static var H1 = TerminalLib.KEYWORD("h1");
//  static var H2 = TerminalLib.KEYWORD("h2");
//  static var H3 = TerminalLib.KEYWORD("h3");
//}
//
//class ParsingT01 extends Parsing {
//  ParsingT01(ScannerOutputStream tokenStream) : super(tokenStream);
//
//  Rule Rule01() => BODY(() =>
//  T.HALLOX
//  );
//
//  Rule Rule02And01() => BODY(() =>
//      AND([T.HALLO1, T.HALLO2])
//  );
//
//  Rule Rule02And02() => BODY(() =>
//      AND([AND([T.HALLO1, T.HALLO2]), T.H3])
//  );
//
//  Rule Rule03() => BODY(() =>
//      OR([T.HALLO1, T.HALLO2])
//  );
//
//  Rule Rule04AND01() => BODY(() =>
//      AND([T.H1, OPTIONAL(T.H2), T.H3])
//  );
//
//  Rule Rule05() => BODY(() =>
//      AND([NFOLD(T.H1), T.H2])
//  );
//}
//
//void main() {
//
//  var alphabet = new CharAlphabet();
//  var KEYWORD = (String keyword) {
//
//    RegEx ret = new RegExSingleToken(alphabet, keyword.codeUnitAt(0));
//
//    if (keyword.length==1) {
//      return ret;
//    }
//
//    for(int pos=1; pos<keyword.length; pos++) {
//      ret =
//      new RegExConcatenation(
//          alphabet, ret,
//          new RegExSingleToken(alphabet, keyword.codeUnitAt(pos)));
//
//    }
//
//    return ret;
//  };
//
//  test("NFOLD 01", () {
//    String aString = "h1h1h1h2";
//
//    StringTokenStream inputStream = new StringTokenStream(aString);
//    List<RegExWithKind<int, CharAlphabet>> tokenRegExs = new List<RegExWithKind<int, CharAlphabet>>();
//    CharScanner scanner = new CharScanner(inputStream, [
//      new RegExWithKind(KEYWORD("h1"), false),
//      new RegExWithKind(KEYWORD("h2"), false),
//      new RegExWithKind(KEYWORD("h3"), false)
//    ]);
//    ParsingT01 p = new ParsingT01(scanner.doScanning());
//
//    AND_TreeNode result = p.doParsing(p.Rule05());
//
//    expect(((result.treeNodes[0] as NFOLD_TreeNode).treeNodes[0] as ScannerElement_TreeNode).keyword, "h1");
//    expect(((result.treeNodes[0] as NFOLD_TreeNode).treeNodes[1] as ScannerElement_TreeNode).keyword, "h1");
//    expect(((result.treeNodes[0] as NFOLD_TreeNode).treeNodes[2] as ScannerElement_TreeNode).keyword, "h1");
//    expect((result.treeNodes[1] as ScannerElement_TreeNode).keyword, "h2");
//
//  });
//
//  test("OPTIONAL 01", () {
//    String aString = "h1h3";
//
//    StringTokenStream inputStream = new StringTokenStream(aString);
//    List<RegExWithKind<int, CharAlphabet>> tokenRegExs = new List<RegExWithKind<int, CharAlphabet>>();
//    CharScanner scanner = new CharScanner(inputStream, [
//      new RegExWithKind(KEYWORD("h1"), false),
//      new RegExWithKind(KEYWORD("h2"), false),
//      new RegExWithKind(KEYWORD("h3"), false)
//    ]);
//    ParsingT01 p = new ParsingT01(scanner.doScanning());
//
//    AND_TreeNode result = p.doParsing(p.Rule04AND01());
//    expect((result.treeNodes[0] as ScannerElement_TreeNode).keyword, "h1");
//    expect((result.treeNodes[1] as OPTIONAL_TreeNode).treeNode, null);
//    expect((result.treeNodes[2] as ScannerElement_TreeNode).keyword, "h3");
//
//  });
//
//  test("OR 02", () {
//    String aString = "hallo1";
//
//    StringTokenStream inputStream = new StringTokenStream(aString);
//    List<RegExWithKind<int, CharAlphabet>> tokenRegExs = new List<RegExWithKind<int, CharAlphabet>>();
//    CharScanner scanner = new CharScanner(inputStream, [
//      new RegExWithKind(KEYWORD("hallo2"), false),
//      new RegExWithKind(KEYWORD("hallo1"), false)]);
//    ParsingT01 p = new ParsingT01(scanner.doScanning());
//
//    OR_TreeNode result = p.doParsing(p.Rule03());
//    expect((result.treeNode as ScannerElement_TreeNode).keyword, "hallo1");
//
//  });
//
//  test("OR 01", () {
//    String aString = "hallo2";
//
//    StringTokenStream inputStream = new StringTokenStream(aString);
//    List<RegExWithKind<int, CharAlphabet>> tokenRegExs = new List<RegExWithKind<int, CharAlphabet>>();
//    CharScanner scanner = new CharScanner(inputStream, [
//      new RegExWithKind(KEYWORD("hallo1"), false),
//      new RegExWithKind(KEYWORD("hallo2"), false)]);
//    ParsingT01 p = new ParsingT01(scanner.doScanning());
//
//    OR_TreeNode result = p.doParsing(p.Rule03());
//    expect((result.treeNode as ScannerElement_TreeNode).keyword, "hallo2");
//
//  });
//
//  test("AND 01", () {
//    String aString = "hallo1hallo2";
//
//    StringTokenStream inputStream = new StringTokenStream(aString);
//    List<RegExWithKind<int, CharAlphabet>> tokenRegExs = new List<RegExWithKind<int, CharAlphabet>>();
//    CharScanner scanner = new CharScanner(inputStream, [
//      new RegExWithKind(KEYWORD("hallo1"), false), new RegExWithKind(KEYWORD("hallo2"), false)]);
//    ParsingT01 p = new ParsingT01(scanner.doScanning());
//
//    AND_TreeNode result = p.doParsing(p.Rule02And01());
//    expect((result.treeNodes[0] as ScannerElement_TreeNode).keyword, "hallo1");
//    expect((result.treeNodes[1] as ScannerElement_TreeNode).keyword, "hallo2");
//
//  });
//
//  test("AND 02", () {
//    String aString = "hallo1hallo2h3";
//
//    StringTokenStream inputStream = new StringTokenStream(aString);
//    List<RegExWithKind<int, CharAlphabet>> tokenRegExs = new List<RegExWithKind<int, CharAlphabet>>();
//    CharScanner scanner = new CharScanner(inputStream, [
//      new RegExWithKind(KEYWORD("hallo1"), false),
//      new RegExWithKind(KEYWORD("hallo2"), false),
//      new RegExWithKind(KEYWORD("h3"), false)]);
//    ParsingT01 p = new ParsingT01(scanner.doScanning());
//
//    AND_TreeNode result = p.doParsing(p.Rule02And02());
//    expect((result.treeNodes[0] as AND_TreeNode).treeNodes[0].keyword, "hallo1");
//    expect((result.treeNodes[0] as AND_TreeNode).treeNodes[1].keyword, "hallo2");
//    expect((result.treeNodes[1] as ScannerElement_TreeNode).keyword, "h3");
//
//  });
//
//  test("Parsing 01", () {
//    String aString = "hallox";
//
//    StringTokenStream inputStream = new StringTokenStream(aString);
//    List<RegExWithKind<int, CharAlphabet>> tokenRegExs = new List<RegExWithKind<int, CharAlphabet>>();
//    CharScanner scanner = new CharScanner(inputStream, [new RegExWithKind(KEYWORD("hallox"), false)]);
//    ParsingT01 p = new ParsingT01(scanner.doScanning());
//
//    TreeNode result = p.doParsing(p.Rule01());
//    expect(result is ScannerElement_TreeNode, true);
//    expect(result.keyword, "hallox");
//
//  });
//
//  test("KEYWORD 01", () {
//    RegEx regEx = KEYWORD("a");
//    expect(regEx is RegExSingleToken, true);
//
//    regEx = KEYWORD("ab");
//    expect(regEx is RegExConcatenation, true);
//    expect(regEx.left.token, "a".codeUnitAt(0));
//    expect(regEx.right.token, "b".codeUnitAt(0));
//
//    regEx = KEYWORD("abc");
//    expect(regEx is RegExConcatenation, true);
//    expect(regEx.left is RegExConcatenation, true);
//    expect(regEx.right.token, "c".codeUnitAt(0));
//    expect(regEx.left.left.token, "a".codeUnitAt(0));
//    expect(regEx.left.right.token, "b".codeUnitAt(0));
//
//  });
//}