typedef ATYPE Getter<ATYPE>();

bool SHALL_PRINT = false;
List<String> dingdonglist = new List();


class ResultObject<ResultType> {
  ResultType _result;
  ResultObject([this._result]);

  void setResult(ResultType newResult) {
    _result = newResult;
  }

  ResultType getResult() {
    return this._result;
  }

  Getter getter() {
    return getResult;
  }
}