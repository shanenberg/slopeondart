class ParsingException  {
  String message = "";
  ParsingException([String message]) {
    if (message!=null) {
      this.message = message;
    }
  }

  String toString() {
    return message;
  }
}
