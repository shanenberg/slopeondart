import '../../../scanner/ScannerOutputStream.dart';
import '../../Parser.dart';
import '../../ParsingException.dart';
import '../../parserconstruction/ParserConstructor.dart';
import '../../tree/TreeNode.dart';
import 'AND_TreeNode.dart';
import 'AND_ParserConstructor.dart';

class AND_Parser extends Parser {

//  AND_Parser(List<ParserConstructor> this.rules);
//
//  List<ParserConstructor> rules;

  AND_TreeNode parse(ScannerOutputStream scannerStream) {
//print("START");
    AND_TreeNode ret = new AND_TreeNode();

    ret.nodeName = this.parserName;
//print("set name: " + ret.nodeName.toString());
ret.parserConstructor = myParserConstructor;
//ret.parserConstructor.parserName = this.parserName;
    List<ParserConstructor> rules = (myParserConstructor as AND_ParserConstructor).parserConstructors;
    for (int i = 0; i < rules.length; i++) {
      Parser parser = rules[i].createParser();
      TreeNode node = parser.parse(scannerStream);
      if (node.parserConstructor == null)
        node.parserConstructor = rules[i];
//print("TRY: " + parser.toString());
//print("added: " + node.toString());
      ret.addTreeNode(node);
    }
    if (ret.treeNodes.length>0) {
//print("END");
      return ret;
    } else {
      throw new ParsingException("AND Exception");
    }
  }

}