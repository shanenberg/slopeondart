import '../../tree/MultiChildrenNode.dart';
import '../../tree/TreeNode.dart';

class AND_TreeNode extends MultiChildrenNode {

  void addTreeNode(TreeNode node) {
    treeNodes.add(node);
  }

  @override
  int length() {
    return treeNodes.length;
  }

  String treeLangNodeString() {
    String ret = "AND(";
    if (this.nodeName!=null) {
      ret = ret + "-" + nodeName + "-";
    }
    for(int i=0;i<this.treeNodes.length;i++) {
      ret = ret + this.treeNodes[i].treeLangNodeString();
    }
    return ret + ")";
  }

  String slotTypeName() {
    if (this.nodeName!=null)
        return nodeName;
    return "AND";
  }
}