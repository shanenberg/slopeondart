import '../../parserconstruction/ParserConstructor.dart';
import '../../tree/TreeNode.dart';
import 'AND_Parser.dart';
import 'AND_TreeNode.dart';

class AND_ParserConstructor extends ParserConstructor {

  AND_ParserConstructor(List<ParserConstructor> this.parserConstructors);

  List<ParserConstructor> parserConstructors;

  AND_Parser createParser() {
    AND_Parser ret = new AND_Parser();
//    ret.myParserConstructor = parserConstructors;
    ret.parserName = this.parserName;
    ret.myParserConstructor = this;
    return ret;
  }

  TreeNode createTemplate() {
    AND_TreeNode ret = new AND_TreeNode();
    ret.parserConstructor = this;
    ret.nodeName = this.parserName;

    for(ParserConstructor pc in parserConstructors) {
      ret.treeNodes.add(pc.createTemplate());
    }

    return ret;
  }
  bool isAND() { return true;}

//  ParserConstructor getCleanParserConstructor() {
//    if (this.parserConstructors.length==1)
//      return this.parserConstructors[0];
//    return this;
//  }

}