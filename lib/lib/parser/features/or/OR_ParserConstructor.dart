import '../../parserconstruction/ParserConstructor.dart';
import '../../tree/TreeNode.dart';
import '../_null_/NULL_TreeNode.dart';
import 'OR_Parser.dart';
import 'OR_TreeNode.dart';

class OR_ParserConstructor extends ParserConstructor {

  OR_ParserConstructor(List<ParserConstructor> this.parserConstructors);

  List<ParserConstructor> parserConstructors;

  OR_Parser createParser() {
    OR_Parser ret = new OR_Parser(parserConstructors);
    ret.myParserConstructor = this;
    return ret;
  }

  @override
  TreeNode createTemplate() {
    OR_TreeNode ret = new OR_TreeNode(-1, new NULL_TreeNode());
    ret.parserConstructor = this;
    return ret;
  }

  TreeNode createTemplateAt(int i) {
    return this.parserConstructors[i].createTemplate();
  }

}