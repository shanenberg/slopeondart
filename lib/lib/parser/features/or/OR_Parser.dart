import '../../../scanner/ScannerOutputStream.dart';
import '../../Parser.dart';
import '../../ParsingException.dart';
import '../../parserconstruction/ParserConstructor.dart';
import '../../tree/TreeNode.dart';
import 'OR_ParserConstructor.dart';
import 'OR_TreeNode.dart';

class OR_Parser extends Parser {

  OR_Parser(List<ParserConstructor> this.rules);
  List<ParserConstructor> rules;


  @override
  TreeNode parse(ScannerOutputStream scannerStream) {
    int pos = scannerStream.position;
    for (int i = 0; i < rules.length; i++) {
      scannerStream.position = pos;
      Parser parser = rules[i].createParser();
      try {
        TreeNode node = parser.parse(scannerStream);
        OR_TreeNode ret =  new OR_TreeNode(i, node);
        ret.parserConstructor = new OR_ParserConstructor(this.rules);
        return ret;
      } catch (exeption) {
        //scannerStream.position = pos;
      }
    }
    throw new ParsingException("OR Exception");
  }
}