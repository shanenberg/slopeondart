import '../../../../slot/runtime/objects/SLOTString.dart';
import '../../../../slot/runtime/objects/SLOTTreeNode.dart';
import '../../tree/SingleChildTreeNode.dart';
import 'OR_ParserConstructor.dart';
import '../../../../lib/grammarparser/tree/rule/GrammarRuleExpressionOR.dart';
import '../../../../lib/parser/parserconstruction/Rule.dart';
import '../../../../lib/parser/parserconstruction/ParserConstructor.dart';
import '../../../../lib/parser/tree/TreeNode.dart';
import '../../../../slot/runtime/objects/SLOTInteger.dart';

class OR_TreeNode extends SingleChildTreeNode {
  int orIndex = -1;

  OR_TreeNode(this.orIndex, treeNode): super(treeNode);
  @override
  int length() {
    return 1;
  }

  String treeLangNodeString() {
    String ret = "OR(";
    if (this.nodeName!=null) {
      ret = ret + "-" + nodeName + "-";
    }
    ret = ret + "#" + this.orIndex.toString() + "#";
    ret = ret + this.child.treeLangNodeString();
    return ret + ")";
  }

  int numberOfAlternatives() {
    return this._numberOfAlternatives(this.parserConstructor);
  }

  int _numberOfAlternatives(ParserConstructor pc) {
    if (pc is OR_ParserConstructor) {
      return (pc as OR_ParserConstructor).parserConstructors.length;
    }

    if (pc is GrammarRuleExpressionOR) {
      return (pc as GrammarRuleExpressionOR).parserConstructors.length;
    }

    if (pc is Rule) {
      ParserConstructor mypc = (pc as Rule).createParserConstructor();
      return _numberOfAlternatives(mypc);
    }

    throw "Problems with numAlternatives";


  }


  void addChildrenIfNecessary(SLOTTreeNode newNode) {
    super.addChildrenIfNecessary(newNode);
    SLOTTreeNode n = new SLOTTreeNode();
    n.value = new SLOTString("INDEX");

    SLOTTreeNode n2 = new SLOTTreeNode();
/*    n2.value = new SLOTString((this.orIndex + 1).toString());
  */
    n2.value = new SLOTInteger(this.orIndex + 1);
    n.addChild(n2);

    newNode.addChild(n);
  }

  void addChildrenIfNecessaryWithoutRegex(SLOTTreeNode newNode) {
    super.addChildrenIfNecessaryWithoutRegex(newNode);
    SLOTTreeNode n = new SLOTTreeNode();
    n.value = new SLOTString("INDEX");

    SLOTTreeNode n2 = new SLOTTreeNode();
    /*    n2.value = new SLOTString((this.orIndex + 1).toString());
  */
    n2.value = new SLOTInteger(this.orIndex + 1);
    n.addChild(n2);

    newNode.addChild(n);
  }

  @override
  String slotTypeName()=>"OR";


  TreeNode createChildTemplate() {
    throw "Which One Dude?!";
  }
}