import '../../../scanner/ScannerOutputStream.dart';
import '../../Parser.dart';
import '../../ResultObject.dart';
import '../../tree/Empty_TreeNode.dart';
import '../../tree/TreeNode.dart';

class SETValue_Parser<ATYPE>  extends Parser {

  ResultObject<ATYPE>  resultObject;
  Getter<ATYPE>  valueFunction;

  SETValue_Parser(this.resultObject, this.valueFunction);

  TreeNode parse(ScannerOutputStream scannerStream) {
    resultObject.setResult(valueFunction());
    return new Empty_TreeNode();
  }
}
