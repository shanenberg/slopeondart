import '../../ResultObject.dart';
import '../../parserconstruction/ParserConstructor.dart';
import '../../tree/TreeNode.dart';
import 'SETValue_Parser.dart';

class SETValue_ParserConstructor<ATYPE> extends ParserConstructor {

  ResultObject<ATYPE>  resultObject;
  Getter<ATYPE>  valueFunction;

  SETValue_ParserConstructor(this.resultObject, this.valueFunction);

  SETValue_Parser createParser() {
    return new SETValue_Parser(this.resultObject, this.valueFunction);
  }
  @override
  TreeNode createTemplate() {
    throw "not yet implemented";
  }
}