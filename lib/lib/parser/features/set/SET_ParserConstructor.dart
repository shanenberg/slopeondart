import '../../ResultObject.dart';
import '../../parserconstruction/ParserConstructor.dart';
import '../../tree/TreeNode.dart';
import 'SETParser.dart';

class SET_ParserConstructor<ATYPE>  extends ParserConstructor {

  ResultObject<ATYPE>  resultObject;
  ResultObject<ATYPE>  newValue;

  SET_ParserConstructor(this.resultObject, this.newValue);

  SET_Parser createParser() {
    return new SET_Parser(this.resultObject, this.newValue);
  }

  @override
  TreeNode createTemplate() {
    throw "not yet implemented";
  }
}