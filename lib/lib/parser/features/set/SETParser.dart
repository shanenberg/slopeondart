import '../../../scanner/ScannerOutputStream.dart';
import '../../Parser.dart';
import '../../ResultObject.dart';
import '../../tree/Empty_TreeNode.dart';
import '../../tree/TreeNode.dart';

class SET_Parser<ATYPE>  extends Parser {

  ResultObject<ATYPE>  resultObject;
  ResultObject<ATYPE>  newValue;

  SET_Parser(this.resultObject, this.newValue);

  TreeNode parse(ScannerOutputStream scannerStream) {
    resultObject.setResult(newValue.getResult());
//    print(newValue);
//    print("DID ASSIGNMENT" + newValue.toString());
    return new Empty_TreeNode();
  }
}