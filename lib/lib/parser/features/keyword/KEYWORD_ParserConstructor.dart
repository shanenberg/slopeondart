import '../../parserconstruction/ParserConstructor.dart';
import '../../ResultObject.dart';
import '../scannerelement/ScannerElement_TreeNode.dart';
import '../../tree/TreeNode.dart';
import 'KeywordParser.dart';

class KEYWORD_ParserConstructor extends ParserConstructor {

  String keyword;
  ResultObject<String> parsingResult = new ResultObject<String>();
  KEYWORD_ParserConstructor(this.keyword, [this.parsingResult]);

  KEYWORD_Parser createParser() {
    KEYWORD_Parser ret = new KEYWORD_Parser(keyword, parsingResult);
    ret.myParserConstructor = this;
    return ret;
  }

  @override
  TreeNode createTemplate() {
    /*if(this.keyword.contains(new RegExp(r'\+|\-|\['))) {

    }*/
    var templateString;
    if(keyword.length > 1)
      templateString = "#";
    else
      templateString = keyword;
//    TreeNode ret = new ScannerElement_TreeNode(this.keyword, templateString);
    TreeNode ret = new ScannerElement_TreeNode(this.keyword, this.keyword);

    ret.parserConstructor = this;
    return ret;
  }
}