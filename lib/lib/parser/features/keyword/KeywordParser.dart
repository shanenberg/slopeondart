import '../../../scanner/ScannerOutputStream.dart';
import '../../../scanner/ScannerToken.dart';
import '../../Parser.dart';
import '../../ParsingException.dart';
import '../../ResultObject.dart';
import '../scannerelement/ScannerElement_TreeNode.dart';

class KEYWORD_Parser extends Parser {

  String keyword;
  ResultObject<String> parsingResult;

  ScannerElement_TreeNode parse(ScannerOutputStream scannerStream) {
    ScannerToken token;
    try {
//print("*******a: " + scannerStream.position.toString());
      token= scannerStream.getNext();
//       print(token.scannerState.regExString);
//
//print("+++++++++++++b" + scannerStream.position.toString());
//      print("+++++++ scannerState: " + token.scannerState.regExString);
//      print("+++++++ token: ***" + token.toString() + "***");
//      print("++++++++ regex: ***" + token.scannerState.regExString + "***");
//      print("++++++++ keyword: ***" + keyword + "***");
//      print("+++ matches: " + (token.scannerState.regExString.compareTo(keyword) == 0).toString());
//print("+++++++ keyword:" + keyword);
//      print("+++++++ pos:" + scannerStream.position.toString());
//      print("+++++++ length " + scannerStream.stream.length.toString());
//      print("+++++++ hasNext" + scannerStream.hasNext().toString());

//      print("+++++++ " + token.toString());
      if (token.scannerState.regExString.compareTo(keyword) == 0) {
//        print("here: " + token.toString());
        if (parsingResult != null) {
//          print("set result: " + token.toString());
          parsingResult.setResult(token.toString());
          // print(token.toString() + " = "+keyword);
        }
        /*else
        {
          parsingResult = new ResultObject<String>();
          parsingResult.setResult(token.toString());
        }*/
//        print("hereXXX");
//        print("+++++++ DONE " + scannerStream.position.toString());
//        print("+++++++ DONE " + scannerStream.stream.length.toString());

        ScannerElement_TreeNode ret = new ScannerElement_TreeNode(keyword, token.toString());
        ret.nodeName = parserName;
        ret.parserConstructor = myParserConstructor;
//print(parserName);

        return ret;
      } else {
        throw new ParsingException("Invalid keyword: expected " + keyword.toString() + " but got " + parsingResult.toString());
      }
    } catch (exception) {
//      print(exception);
//      print(scannerStream.position);
//      try {
//        print(scannerStream.stream[0]);
//      } catch (ex) {
//
//      }
//      print("before exception: " + token.toString());
//      print("before exception: " + scannerStream.stream.length.toString());
//      print("before exception: " + scannerStream.position.toString());
//      print("before exception: " + scannerStream.stream[scannerStream.position-1]);
//      scannerStream.printStream();
      throw new ParsingException("Invalid keyword: expected " + keyword.toString() + " but got " + parsingResult.toString() + " at " + scannerStream.position.toString());
    }
  }

  KEYWORD_Parser(this.keyword, this.parsingResult);

}