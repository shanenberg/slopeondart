import '../../../scanner/ScannerOutputStream.dart';
import '../../Parser.dart';
import '../../parserconstruction/ParserConstructor.dart';
import '../../tree/TreeNode.dart';
import '../../ParsingException.dart';
import 'NFOLD_TreeNode.dart';

class NFOLD_Parser extends Parser {

  NFOLD_Parser(ParserConstructor this.childParserConstructor);
  ParserConstructor childParserConstructor;


  @override
  TreeNode parse(ScannerOutputStream scannerStream) {
    int pos = scannerStream.position;
    NFOLD_TreeNode ret = new NFOLD_TreeNode();

    while(true) {
      Parser parser = childParserConstructor.createParser();
      try {
        TreeNode node = parser.parse(scannerStream);
        ret.treeNodes.add(node);
        pos = scannerStream.position;
//print("......done " + node.toString())   ;
      } on ParsingException catch (exception) {
        scannerStream.position = pos;
//print("......ups")   ;
        return ret;
      }
    }
  }
}