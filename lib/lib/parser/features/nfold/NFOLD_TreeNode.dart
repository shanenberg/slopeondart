import '../../../treelang/TreeLangNode.dart';
import '../../tree/MultiChildrenNode.dart';
import '../../tree/TreeNode.dart';
import '../../parserconstruction/ParserConstructor.dart';
import '../../../../lib/parser/parserconstruction/Rule.dart';
import '../../../../lib/parser/features/nfold/NFOLD_ParserConstructor.dart';
import '../../../../lib/parser/features/nfold1/NFOLD1_ParserConstructor.dart';
import '../../../../lib/parser/features/bracket/BRACKET_ParserConstructor.dart';

class NFOLD_TreeNode extends MultiChildrenNode {
  NFOLD_TreeNode();

  @override
  int length() {
    return treeNodes.length;
  }

  String treeLangNodeString() {
    String ret = "NFOLD(";
    if (this.nodeName!=null) {
      ret = ret + "-" + nodeName + "-";
    }
    for(int i=0;i<this.treeNodes.length;i++) {
      ret = ret + this.treeNodes[i].treeLangNodeString();
    }
    return ret + ")";
  }

  bool isN0() {
    ParserConstructor pConstr = this.parserConstructor;
    while(pConstr is Rule) {
      pConstr = (pConstr as Rule).rule();
    }

    return pConstr is NFOLD_ParserConstructor;
  }

  bool isN1() {
    ParserConstructor pConstr = this.parserConstructor;
    while(pConstr is Rule) {
      pConstr = (pConstr as Rule).rule();
    }

    return pConstr is NFOLD1_ParserConstructor;
  }

  TreeNode createChildTemplate() {
    ParserConstructor pConstr =
      this.parserConstructor.getCleanParserConstructor();

    while(pConstr is Rule) {
      pConstr = (pConstr as Rule).rule();
    }
    TreeNode ret;
    if(pConstr is NFOLD_ParserConstructor) {
      var apc = (pConstr as NFOLD_ParserConstructor).parserConstructor;
      ret = apc
          .createTemplate();
    } else if(pConstr is NFOLD1_ParserConstructor)
      ret = (pConstr as NFOLD1_ParserConstructor).parserConstructor.createTemplate();
    else if(pConstr is BRACKET_ParserConstructor)
      ret = (pConstr as BRACKET_ParserConstructor).childParserConstructor.createTemplate();




    return ret;
  }

  String slotTypeName() => "NFOLD";


}