import '../../parserconstruction/ParserConstructor.dart';
import '../../tree/TreeNode.dart';
import 'NFOLD_Parser.dart';
import 'NFOLD_TreeNode.dart';

class NFOLD_ParserConstructor extends ParserConstructor {

  NFOLD_ParserConstructor(ParserConstructor this.parserConstructor);

  ParserConstructor parserConstructor;

  NFOLD_Parser createParser() {
    NFOLD_Parser ret = new NFOLD_Parser(parserConstructor);
    ret.myParserConstructor = this;
    return ret;
  }

  @override
  TreeNode createTemplate() {
    NFOLD_TreeNode ret = new NFOLD_TreeNode();
    ret.parserConstructor = this;

    return ret;
  }
}