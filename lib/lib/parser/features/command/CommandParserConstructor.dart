import '../../parserconstruction/ParserConstructor.dart';
import '../../tree/TreeNode.dart';
import 'CommandParser.dart';

class CommandParserConstructor<T> extends ParserConstructor {

  var aFunction;

  CommandParserConstructor(this.aFunction);

  CommandParser createParser() {
    return new CommandParser(aFunction);
  }

  @override
  TreeNode createTemplate() {
    // TODO: implement createTemplate
  }
}