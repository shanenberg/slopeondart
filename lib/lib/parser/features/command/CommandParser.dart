import '../../../scanner/ScannerOutputStream.dart';
import '../../Parser.dart';
import '../../tree/Empty_TreeNode.dart';
import '../../tree/TreeNode.dart';

class CommandParser extends Parser {

  var aFunction;
  CommandParser(this.aFunction);

  TreeNode parse(ScannerOutputStream scannerStream) {
//    print("position: " + scannerStream.position.toString());
    aFunction();
    return new Empty_TreeNode();
  }
}