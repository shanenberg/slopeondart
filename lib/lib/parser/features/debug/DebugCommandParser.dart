import '../../../scanner/ScannerOutputStream.dart';
import '../../tree/Empty_TreeNode.dart';
import '../../Parser.dart';
import '../../tree/TreeNode.dart';

class DebugCommandParser extends Parser {

  var aFunction;
  DebugCommandParser(this.aFunction);

  TreeNode parse(ScannerOutputStream scannerStream) {
//    print("position: " + scannerStream.position.toString());
    aFunction();
    print("@position: " + scannerStream.position.toString());
    print("@token: " + scannerStream.currentElement().toString());
    return new Empty_TreeNode();
  }
}