import '../../parserconstruction/ParserConstructor.dart';
import '../../tree/TreeNode.dart';
import 'DebugCommandParser.dart';

class DebugCommandParserConstructor<T> extends ParserConstructor {

  var aFunction;

  DebugCommandParserConstructor(this.aFunction);

  DebugCommandParser createParser() {
    return new DebugCommandParser(aFunction);
  }

  @override
  TreeNode createTemplate() {
    throw "no implemented";
  }
}