import '../../../scanner/ScannerOutputStream.dart';
import '../../Parser.dart';
import '../../parserconstruction/ParserConstructor.dart';
import '../../tree/TreeNode.dart';
import '../nfold/NFOLD_TreeNode.dart';
import '../../ParsingException.dart';

class NFOLD1_Parser extends Parser {

  NFOLD1_Parser(ParserConstructor this.childParserConstructor);
  ParserConstructor childParserConstructor;


  @override
  TreeNode parse(ScannerOutputStream scannerStream) {
    int pos = scannerStream.position;
    NFOLD_TreeNode ret = new NFOLD_TreeNode();
    bool hasError = false;
    while(!hasError) {
      Parser parser = childParserConstructor.createParser();
      pos = scannerStream.position;
      try {
        TreeNode node = parser.parse(scannerStream);
        ret.treeNodes.add(node);
//print("......done " + node.toString())   ;
      } on ParsingException catch (exception) {
        scannerStream.position = pos;
//print("......ups")   ;
        hasError = true;
      }
    }

    if (ret.treeNodes.length<1) {
//      print("HERE");
//      print("HERE" + ret.treeNodes.length.toString());
      throw new ParsingException("Exception in NFOLD1");
    } else {
      return ret;
    }

  }
}
