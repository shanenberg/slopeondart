import '../../parserconstruction/ParserConstructor.dart';
import '../../tree/TreeNode.dart';
import '../nfold/NFOLD_TreeNode.dart';
import 'NFOLD1_Parser.dart';

class NFOLD1_ParserConstructor extends ParserConstructor {

  NFOLD1_ParserConstructor(ParserConstructor this.parserConstructor);

  ParserConstructor parserConstructor;

  NFOLD1_Parser createParser() {
    NFOLD1_Parser ret = new NFOLD1_Parser(parserConstructor);
    ret.myParserConstructor = this;
    return ret;
  }

  @override
  TreeNode createTemplate() {
    NFOLD_TreeNode ret = new NFOLD_TreeNode();

    ret.treeNodes.add(this.parserConstructor.createTemplate());

    ret.parserConstructor = this;

    return ret;
  }
}