import '../../../../slot/runtime/objects/SLOTSLOPETreeNode.dart';
import '../../../../slot/runtime/objects/SLOTString.dart';
import '../../../../slot/runtime/objects/SLOTTreeNode.dart';
import '../../tree/TreeNode.dart';

class NULL_TreeNode extends TreeNode {

  @override
  int length() {
    return 0;
  }

  @override
  String treeLangNodeString() {
    return "NULL";
  }
  @override
  SLOTSLOPETreeNode toSLOTTreeNode([SLOTTreeNode parent]) {

    SLOTSLOPETreeNode ret = new SLOTSLOPETreeNode(this);

    ret.value = new SLOTString("NULL");
    ret.slopeTreeNode = this;

    if (parent != null) {
      ret.parent = parent;
      ret.parent.children.elements.add(ret);
    }

    addChildrenIfNecessary(ret);

    return ret;

  }

  @override
  SLOTTreeNode toSLOTTreeNodeWithoutRegex([SLOTTreeNode parent]) {
    SLOTSLOPETreeNode ret = new SLOTSLOPETreeNode(this);

    ret.value = new SLOTString("NULL");
    ret.slopeTreeNode = this;

    if (parent != null) {
      ret.parent = parent;
      ret.parent.children.elements.add(ret);
    }

    addChildrenIfNecessaryWithoutRegex(ret);

    return ret;
  }


  @override
  String slotTypeName() {
    return "NULL";
  }
}