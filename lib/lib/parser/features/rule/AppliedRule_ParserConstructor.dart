import '../../Parser.dart';
import '../../parserconstruction/ParserConstructor.dart';
import '../../Parsing.dart';
import '../../parserconstruction/Rule.dart';
import '../../tree/TreeNode.dart';
import 'AppliedRule_Parser.dart';

class AppliedRule_ParserConstructor extends Rule {

  ParserConstructor nextParserConstructor;

  AppliedRule_ParserConstructor(pc): super(()=>pc) {
    this.nextParserConstructor = pc;
  }

  @override
  Parser createParser() {
    Parser ret = new AppliedRule_Parser(nextParserConstructor.createParser());
    ret.parserName = this.parserName;
    return ret;
  }

  @override
  TreeNode createTemplate() {
    // TODO: implement createTemplate
  }
}