import '../../../scanner/ScannerOutputStream.dart';
import '../../Parser.dart';
import '../../tree/TreeNode.dart';
import 'AppliedRule_TreeNode.dart';

class AppliedRule_Parser extends Parser {

  Parser containedParser;

  AppliedRule_Parser(this.containedParser);

  @override
  TreeNode parse(ScannerOutputStream scannerStream) {
    TreeNode element = containedParser.parse(scannerStream);
    TreeNode n = new AppliedRule_TreeNode(element);
    n.nodeName = this.parserName;
    return n;
  }
}