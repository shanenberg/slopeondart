import '../../tree/SingleChildTreeNode.dart';

class AppliedRule_TreeNode extends SingleChildTreeNode {

  AppliedRule_TreeNode(child):super(child);

  @override
  int length() {
    return 0;
  }

  @override
  String treeLangNodeString() {
    return "RULE(" + "-" + this.nodeName.toString() + "-" + child.treeLangNodeString() + ")";
  }
  @override
  String slotTypeName() => "RULE";
}