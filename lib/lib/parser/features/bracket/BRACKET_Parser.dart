import '../../../scanner/ScannerOutputStream.dart';
import '../../Parser.dart';
import '../../parserconstruction/ParserConstructor.dart';
import '../../tree/TreeNode.dart';
import 'BRACKET_TreeNode.dart';

class BRACKET_Parser extends Parser {

  ParserConstructor childParserConstructor;

  BRACKET_Parser(ParserConstructor this.childParserConstructor);


  @override
  TreeNode parse(ScannerOutputStream scannerStream) {
    int pos = scannerStream.position;

    Parser parser = childParserConstructor.createParser();
    TreeNode node = parser.parse(scannerStream);
    BRACKET_TreeNode ret = new BRACKET_TreeNode(node);

    return ret;
  }
}