import '../../parserconstruction/ParserConstructor.dart';
import '../../tree/TreeNode.dart';
import 'BRACKET_Parser.dart';
import 'BRACKET_TreeNode.dart';

class BRACKET_ParserConstructor extends ParserConstructor {

  BRACKET_ParserConstructor(this.childParserConstructor);

  ParserConstructor childParserConstructor;

  ParserConstructor getCleanParserConstructor() {
    return childParserConstructor.getCleanParserConstructor();
  }

  BRACKET_Parser createParser() {
    BRACKET_Parser ret = new BRACKET_Parser(childParserConstructor);
    ret.myParserConstructor = this;
    return ret;
  }

  @override
  TreeNode createTemplate() {
    TreeNode ret = childParserConstructor.createTemplate();
    ret.parserConstructor = this.childParserConstructor;
    return ret;
  }

}