import '../../tree/SingleChildTreeNode.dart';
import '../../tree/TreeNode.dart';

class BRACKET_TreeNode extends SingleChildTreeNode {

  BRACKET_TreeNode(child): super(child);

  @override
  int length() {
    return 1;
  }

  String treeLangNodeString() {
    String ret = "BRACKET(" + child.treeLangNodeString() + ")";
    return ret;
  }

  String slotTypeName()=>"BRACKET";
}