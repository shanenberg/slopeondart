import '../../../../slot/node/SLOTNode.dart';
import '../../../../slot/runtime/objects/SLOTSLOPETreeNode.dart';
import '../../../../slot/runtime/objects/SLOTSLOPETreeNodeLeaf.dart';
import '../../../../slot/runtime/objects/SLOTString.dart';
import '../../../../slot/runtime/objects/SLOTTreeNode.dart';
import '../../../treelang/TreeLangNode.dart';
import '../../tree/TreeNode.dart';
import '../../../grammarparser/tree/GrammarObject.dart';
import '../../../util/GrammarUtils.dart';



class ScannerElement_TreeNode extends TreeNode {
  String keyword;
  String scannedWord;

  ScannerElement_TreeNode(this.keyword, this.scannedWord)
  {
    // print(this.scannedWord);
  }


  @override
  int length() { return 1; }


  @override
  bool equalsTreeLangNode(TreeLangNode n) {
    return (n.typeName==keyword);
  }
  @override
  String treeLangNodeString() {
    return keyword;
  }

  SLOTSLOPETreeNode toSLOTTreeNode([SLOTTreeNode parent=null]) {
    SLOTSLOPETreeNodeLeaf leaf =  new SLOTSLOPETreeNodeLeaf(this);
    if (parent!=null) {
      parent.children.elements.add(leaf);
      leaf.parent = parent;
    }

    return leaf;
  }


  SLOTSLOPETreeNode toSLOTTreeNodeWithoutRegex([SLOTTreeNode parent=null]) {
    SLOTSLOPETreeNodeLeafWithoutRegex leaf =  new SLOTSLOPETreeNodeLeafWithoutRegex(this);
    if (parent!=null) {
      parent.children.elements.add(leaf);
      leaf.parent = parent;
    }

    return leaf;
  }


  @override
  String slotTypeName() {
    return '"' + keyword + '"';
  }
}