import '../../../scanner/ScannerOutputStream.dart';
import '../../../scanner/ScannerToken.dart';
import '../../Parser.dart';
import '../../ParsingException.dart';
import '../../ResultObject.dart';
import '../scannerelement/ScannerElement_TreeNode.dart';

class VALUE_Parser extends Parser {

  String value;
  ResultObject<String> parsingResult;

  ScannerElement_TreeNode parse(ScannerOutputStream scannerStream) {
    ScannerToken token;
    try {
      token= scannerStream.getNext();
      // print(token.toString());
      if (token.toString().compareTo(value)==0) {

        if (parsingResult != null) {
          parsingResult.setResult(token.toString());
        }
        /*else
        {
          parsingResult = new ResultObject<String>();
          parsingResult.setResult(token.toString());
        }*/

        return new ScannerElement_TreeNode(value, token.toString());
      } else {
        throw new ParsingException("Invalid keyword: expected " + value.toString() + " but got " + parsingResult.toString());
      }
    } catch (exception) {
//      print(exception);
//      print(scannerStream.position);
//      try {
//        print(scannerStream.stream[0]);
//      } catch (ex) {
//
//      }
//      print("before exception: " + token.toString());
//      print("before exception: " + scannerStream.stream.length.toString());
//      print("before exception: " + scannerStream.position.toString());
//      print("before exception: " + scannerStream.stream[scannerStream.position-1]);
//      scannerStream.printStream();
      throw new ParsingException("Invalid keyword: expected " + value.toString() + " but got " + parsingResult.toString() + " at " + scannerStream.position.toString());
    }
  }

  VALUE_Parser(this.value, this.parsingResult);

}