import '../../parserconstruction/ParserConstructor.dart';
import '../../ResultObject.dart';
import '../../tree/TreeNode.dart';
import 'VALUE_Parser.dart';

class VALUE_ParserConstructor extends ParserConstructor {

  String keyword;
  ResultObject<String> parsingResult = new ResultObject<String>();
  VALUE_ParserConstructor(this.keyword, [this.parsingResult]);

  VALUE_Parser createParser() {
    return new VALUE_Parser(keyword, parsingResult);
  }

  @override
  TreeNode createTemplate() {
    // TODO: implement createTemplate
  }
}
