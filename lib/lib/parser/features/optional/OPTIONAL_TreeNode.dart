import '../../../../slot/node/SLOTNode.dart';
import '../../../../slot/runtime/objects/SLOTString.dart';
import '../../../../slot/runtime/objects/SLOTTreeNode.dart';
import '../../../treelang/TreeLangNode.dart';
import '../../tree/SingleChildTreeNode.dart';
import '../../tree/TreeNode.dart';
import 'OPTIONAL_ParserConstructor.dart';
import '../../../parser/features/_null_/NULL_TreeNode.dart';

class OPTIONAL_TreeNode extends SingleChildTreeNode {

  OPTIONAL_TreeNode(child):super(child);
  @override
  int length() {
    if (child==null) return 0;
    return 1;
  }

  bool isOptional() {
    return true;
  }

  @override
  String treeLangNodeString() {

    if (child==null) {
      return "OPT(" + "***null***" + ")";
    } else
      return "OPT(" + child.treeLangNodeString() + ")";
  }


  addChildrenIfNecessary(SLOTTreeNode newNode) {
    if(child != null)
      child.toSLOTTreeNode(newNode);
    else {
      new NULL_TreeNode().toSLOTTreeNode(newNode);
    }
  }

  addChildrenIfNecessaryWithoutRegex(SLOTTreeNode newNode) {
    if(child != null)
      child.toSLOTTreeNodeWithoutRegex(newNode);
    else {
      new NULL_TreeNode().toSLOTTreeNodeWithoutRegex(newNode);
    }
  }

/*
  @override
  SLOTTreeNode toSLOTTreeNode([SLOTTreeNode parent]) {
    SLOTSLOPETreeNode ret = new SLOTSLOPETreeNode();
    ret.origin = null;
    ret.parent = parent;
    if (parent!=null) {
      parent.children.elements.add(ret);
    }
    ret.value = new SLOTString("OPTIONAL");
    if (this.child!=null)
      this.addChildrenIfNecessary(ret);
    return ret;
  }*/

  @override
  String slotTypeName() => "OPT";

  TreeNode createChildTemplate() {
    return (unRuledParserConstructor() as OPTIONAL_ParserConstructor).parserConstructor.createTemplate();
  }
}

