import '../../tree/Empty_TreeNode.dart';
import '../../parserconstruction/ParserConstructor.dart';
import '../../tree/TreeNode.dart';
import '../_null_/NULL_TreeNode.dart';
import 'OPTIONAL_Parser.dart';
import 'OPTIONAL_TreeNode.dart';

class OPTIONAL_ParserConstructor extends ParserConstructor {

  OPTIONAL_ParserConstructor(ParserConstructor this.parserConstructor);

  ParserConstructor parserConstructor;

  OPTIONAL_Parser createParser() {
    OPTIONAL_Parser ret = new OPTIONAL_Parser(parserConstructor);
    ret.myParserConstructor = this;
    return ret;
  }

  @override
  TreeNode createTemplate() {
    //NULL_TreeNode e = new NULL_TreeNode();
    TreeNode ret = new OPTIONAL_TreeNode(null);

    ret.parserConstructor = this;

    return ret;
  }
}