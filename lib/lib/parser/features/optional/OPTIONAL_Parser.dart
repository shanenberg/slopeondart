import '../../../scanner/ScannerOutputStream.dart';
import '../../Parser.dart';
import '../../parserconstruction/ParserConstructor.dart';
import '../../ParsingException.dart';
import '../../tree/TreeNode.dart';
import 'OPTIONAL_TreeNode.dart';

class OPTIONAL_Parser extends Parser {

  OPTIONAL_Parser(ParserConstructor this.childParserConstructor);
  ParserConstructor childParserConstructor;


  @override
  TreeNode parse(ScannerOutputStream scannerStream) {
    int pos = scannerStream.position;
    Parser parser = childParserConstructor.createParser();
    try {
      TreeNode node = parser.parse(scannerStream);
      return new OPTIONAL_TreeNode(node);
    } catch (exception) {
      if (exception is ParsingException) {
        scannerStream.position = pos;
        return new OPTIONAL_TreeNode(null);
      } else {
        throw exception;
      }
    }
  }
}