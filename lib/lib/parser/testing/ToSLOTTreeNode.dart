import 'package:test/test.dart';

import '../../../slot/runtime/objects/SLOTString.dart';
import '../../../slot/runtime/objects/SLOTTreeNode.dart';
import '../../dynamicParser/DynamicParser.dart';
import '../../grammarparser/GrammarParser.dart';
import '../../grammarparser/tree/GrammarObject.dart';
import '../ResultObject.dart';
import '../tree/TreeNode.dart';

main() {

  var slopeString = 'ABExample Rules: start start => A. A => "A" "B" "C".';
  var toParse = 'ABC';

  test("SLOPEParser - toSLOTTreeNode- ", (){
    var slopeParser = new GrammarParser();
    ResultObject<SLOPEGrammarObject> result = new ResultObject<SLOPEGrammarObject>();
    slopeParser.parseString(slopeString, result);
    SLOPEGrammarObject program = result.getResult();

    DynamicParser dynamicParser = new DynamicParser(program);

    TreeNode sourceTreeNode = dynamicParser.parseString(toParse, null);

    SLOTTreeNode n = sourceTreeNode.toSLOTTreeNode();
    expect((n.value as SLOTString).value, "start");
    expect(((n.children.elements[0] as SLOTTreeNode).value as SLOTString).value, "A");
  });

}

