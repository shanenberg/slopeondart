import '../../scanner/ScannerOutputStream.dart';
import '../Parser.dart';
import '../Parsing.dart';
import '../features/rule/AppliedRule_ParserConstructor.dart';
import '../tree/TreeNode.dart';
import 'ParserConstructor.dart';
import '../features/and/AND_ParserConstructor.dart';
import '../features/or/OR_ParserConstructor.dart';
import '../features/bracket/BRACKET_ParserConstructor.dart';

/**
 * A Rule is a lazy evaluation of the grammar rules. When it creates
 * its parser, it just goes to the next level -- until all lazy
 * evaluated rules are executed.
 */
class Rule extends ParserConstructor{
  RULE_FUNCTION rule;
//  String ruleName; // this is probably useless.....I do not set it anymore...

  Rule(this.rule) {
//    ruleName = new Trace.current().frames[2].member.split(".")[1];
  }

  TreeNode doParsing(ScannerOutputStream stream) {
    Parser p = rule().createParser();
    p.parserName = this.parserName;
    TreeNode ret = p.parse(stream);
    ret.parserConstructor.parserName = this.parserName;
//    ret.parserConstructor = rule();
    return ret;
  }

  ParserConstructor createParserConstructor() {
    if (parserName!=null) {
      ParserConstructor ret = new AppliedRule_ParserConstructor(rule());
      throw "HELP";
    } else {
      ParserConstructor ret = rule();
      ret.parserName = parserName;
      return ret;
    }
  }

  bool isRule() { return true;}

  @override
  Parser createParser() {
    ParserConstructor parserConstructor = rule();
    Parser ret = parserConstructor.createParser();
    ret.parserName = parserConstructor.parserName;
    return ret;
  }

  @override
  TreeNode createTemplate() {
//    AND_TreeNode ret = new AND_TreeNode();
//    ret.nodeName = this.parserName;

    ParserConstructor next = getCleanParserConstructor();
    next.parserName = this.parserName;
    TreeNode ret = next.createTemplate();

//    if (next.isAND()) {
//      for (ParserConstructor aPC in (next as AND_ParserConstructor).parserConstructors) {
//        ret.addTreeNode(aPC.createTemplate());
//      }
//    } else {
//      ret.addTreeNode(next.createTemplate());
//    }

    ret.parserConstructor = this.getCleanParserConstructor();


    ret.nodeName = this.parserName;

    return ret;
  }

  ParserConstructor getCleanParserConstructor() {
    String parserName = this.parserName;
    ParserConstructor ret = this.rule();

    while(ret.isRule() || ret is BRACKET_ParserConstructor) {
      if (ret.isRule())
        ret = (ret as Rule).rule();
      else if (ret is BRACKET_ParserConstructor)
        ret = (ret as BRACKET_ParserConstructor).childParserConstructor;
    }

    ret = ret.getCleanParserConstructor();
    ret.parserName = parserName;
    return ret;
  }

}
