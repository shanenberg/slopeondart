import '../Parser.dart';
import '../tree/TreeNode.dart';

abstract class ParserConstructor {
  String parserName;
  Parser createParser();

  TreeNode createTemplate();

  bool isRule() { return false;}
  bool isAND() { return false;}

  ParserConstructor getCleanParserConstructor() {
    return this;
  }
}