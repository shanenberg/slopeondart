import '../scanner/ScannerOutputStream.dart';
import 'parserconstruction/ParserConstructor.dart';
import 'tree/TreeNode.dart';

abstract class Parser {
  String parserName;
  ParserConstructor myParserConstructor;
  TreeNode parse(ScannerOutputStream scannerStream);
}
