import '../automata/alphabet/CharAlphabet.dart';
import '../scanner/ScannerOutputStream.dart';
import '../scanner/ScannerToken.dart';
import 'Parsing.dart';

abstract class CharParsing extends Parsing<ScannerToken<int, CharAlphabet>> {

  CharParsing(ScannerOutputStream<ScannerToken<int, CharAlphabet>> tokenStream) : super(tokenStream);

  List<Object> tokens();

}