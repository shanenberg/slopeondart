import '../automata/regex/RegEx.dart';
import '../scanner/ScannerOutputStream.dart';
import 'ResultObject.dart';
import 'features/and/AND_ParserConstructor.dart';
import 'features/command/CommandParserConstructor.dart';
import 'features/debug/DebugCommandParserConstructor.dart';
import 'features/keyword/KEYWORD_ParserConstructor.dart';
import 'features/nfold/NFOLD_ParserConstructor.dart';
import 'features/nfold1/NFOLD1_ParserConstructor.dart';
import 'features/optional/OPTIONAL_ParserConstructor.dart';
import 'features/or/OR_ParserConstructor.dart';
import 'features/set/SETValue_ParserConstructor.dart';
import 'features/value/VALUE_ParserConstructor.dart';
import 'parserconstruction/ParserConstructor.dart';
import 'parserconstruction/Rule.dart';
import 'tree/TreeNode.dart';

typedef ParserConstructor AND_PC();
typedef ParserConstructor RULE_FUNCTION();


class Parsing<TokenType> {

  ScannerOutputStream<TokenType> tokenStream;
  Parsing(this.tokenStream);


  BODY(RULE_FUNCTION aRule) => new Rule(aRule);

  AND_ParserConstructor AND(List<ParserConstructor> parserConstructors) => new AND_ParserConstructor(parserConstructors);
  OR(List<ParserConstructor> parserConstructors) => new OR_ParserConstructor(parserConstructors);
  OPTIONAL(ParserConstructor parserConstructor) => new OPTIONAL_ParserConstructor(parserConstructor);
  NFOLD(ParserConstructor parserConstructor) => new NFOLD_ParserConstructor(parserConstructor);
  NFOLD1(ParserConstructor parserConstructor) => new NFOLD1_ParserConstructor(parserConstructor);
  VALUE(String aString, [ResultObject ro]) {
    return new VALUE_ParserConstructor(aString.toString(), ro);
  }

  Object SET<ATYPE>(ResultObject<ATYPE> resultObject, ResultObject<ATYPE> newValue) {
    return new SETValue_ParserConstructor(resultObject, ()=>newValue.getResult());
  }

  Object SETValue<ATYPE> (ResultObject<ATYPE>  resultObject, Getter<ATYPE>  value) {
    return new SETValue_ParserConstructor(resultObject, value);
  }

  Object PRINT<T>(ResultObject<T> result) => new CommandParserConstructor(()=>print(result.getResult));
  Object DO<T>(var aFunction) => new CommandParserConstructor(()=>aFunction());
  Object DEBUG<T>(var aFunction) => new DebugCommandParserConstructor(()=>aFunction());

  REGEXObj(RegEx aRegEx, [ResultObject<String> resultObject]) => new KEYWORD_ParserConstructor(aRegEx.toString(), resultObject);
  TOKEN(String aString, [ResultObject ro]) {
    return new KEYWORD_ParserConstructor(aString.toString(), ro);
  }

  TreeNode doParsing(Rule rule) {
    TreeNode ret = rule.doParsing(tokenStream);

//      print(rule.toString());
    if (tokenStream.hasNext()) {
      var s = "tokens not completed by parser.... nextToken: " + tokenStream.getNext().toString() + " " + (tokenStream.position.toString());
      /*print(s);
      return ret;*/
      throw s;
    }

    return ret;
  }

}
