import '../../../slot/runtime/objects/SLOTTreeNode.dart';
import 'TreeNode.dart';

/*
Empty tree nodes are used using parsing while executing
for example command objects. This is NOT intended to be
used for partical tree construction.
 */
class Empty_TreeNode extends TreeNode {

  @override
  int length() { return 0; }

  @override
  String treeLangNodeString() {
    throw "Empty hat einen anderen Zweck!!!! Sollte nie aufgerufenw erden";
  }

  @override
  SLOTTreeNode toSLOTTreeNode([SLOTTreeNode parent]) {
    throw "Never ask again!!!";
  }

  @override
  SLOTTreeNode toSLOTTreeNodeWithoutRegex([SLOTTreeNode parent]) {
    throw "Never ask again!!!";
  }

  @override
  String slotTypeName() {
    throw "not yet implemented";
  }
}