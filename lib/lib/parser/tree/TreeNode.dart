import '../../../slot/runtime/objects/SLOTSLOPETreeNode.dart';
import '../../../slot/runtime/objects/SLOTString.dart';
import '../../../slot/runtime/objects/SLOTTreeNode.dart';
import '../parserconstruction/ParserConstructor.dart';
import '../parserconstruction/Rule.dart';

abstract class TreeNode {
  String nodeName = null;
  ParserConstructor parserConstructor; // Used to generated default nodes

  int length();
  bool isOptional() {
    return false;
  }


  ParserConstructor unRuledParserConstructor() {
    ParserConstructor pConstr = parserConstructor;
    while(pConstr is Rule)
      pConstr = (pConstr as Rule).rule();
    return pConstr;
  }

  String treeLangNodeString();

  TreeNode createTemplate() {
    return parserConstructor.createTemplate();
  }


  SLOTSLOPETreeNode toSLOTTreeNode([SLOTTreeNode parent=null]) {
    SLOTSLOPETreeNode ret = new SLOTSLOPETreeNode(this);

    ret.value = new SLOTString(this.slotTypeName());
    ret.slopeTreeNode = this;

    if (parent != null) {
      ret.parent = parent;
      ret.parent.children.elements.add(ret);
    }

    addChildrenIfNecessary(ret);

    return ret;
  }
  void addChildrenIfNecessary(SLOTTreeNode newNode) {}


  SLOTSLOPETreeNode toSLOTTreeNodeWithoutRegex([SLOTTreeNode parent=null]) {
    SLOTSLOPETreeNode ret = new SLOTSLOPETreeNode(this);

    ret.value = new SLOTString(this.slotTypeName());
    ret.slopeTreeNode = this;

    if (parent != null) {
      ret.parent = parent;
      ret.parent.children.elements.add(ret);
    }

    addChildrenIfNecessaryWithoutRegex(ret);

    return ret;
  }
  void addChildrenIfNecessaryWithoutRegex(SLOTTreeNode ret) {}

  String slotTypeName();

}