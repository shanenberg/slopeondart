import '../../../slot/runtime/objects/SLOTTreeNode.dart';
import 'TreeNode.dart';

abstract class SingleChildTreeNode extends TreeNode {
  TreeNode child;

  SingleChildTreeNode(this.child);

  String slotTypeName();

  @override
  void addChildrenIfNecessary(SLOTTreeNode newNode) {
     child.toSLOTTreeNode(newNode);
  }


  @override
  void addChildrenIfNecessaryWithoutRegex(SLOTTreeNode newNode)
  {
    child.toSLOTTreeNodeWithoutRegex(newNode);
  }

  TreeNode createChildTemplate() {
    return child.createTemplate();
  }

}