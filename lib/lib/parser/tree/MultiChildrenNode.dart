import '../../../slot/runtime/objects/SLOTTreeNode.dart';
import 'TreeNode.dart';

abstract class MultiChildrenNode extends TreeNode {
  List<TreeNode> treeNodes = new List<TreeNode>();


  @override
  void addChildrenIfNecessary(SLOTTreeNode newNode) {
    for(int i=0;i<this.treeNodes.length;i++) {
      this.treeNodes[i].toSLOTTreeNode(newNode);
    }
  }


  @override
  void addChildrenIfNecessaryWithoutRegex(SLOTTreeNode newNode) {
    for(int i=0;i<this.treeNodes.length;i++) {
      this.treeNodes[i].toSLOTTreeNodeWithoutRegex(newNode);
    }
  }

  String slotTypeName();

}