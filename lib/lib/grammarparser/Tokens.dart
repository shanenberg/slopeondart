import '../automata/alphabet/CharAlphabet.dart';
import '../automata/regex/RegEx.dart';
import '../parser/ResultObject.dart';
import '../regex/RegExStringParser.dart';

class Tokens {


  static RegEx fromREGEX(String s) {
    ResultObject<RegEx<int, CharAlphabet>> result = new ResultObject<RegEx<int, CharAlphabet>>();
//      print("From RegEx: " + s);
    new RegExStringParser().parseString(s, result);
//    var o = reflect(result.getResult()).type;
    var r = result.getResult();

//      print("regexString: " + r.toString());

    return r;
  }
}