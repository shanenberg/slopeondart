import '../automata/regex/RegEx.dart';
import '../parser/ResultObject.dart';
import '../regex/RegExStringParser.dart';
import 'GrammarRuleExpression.dart';
import 'tree/GrammarObject.dart';
import 'tree/rule/GrammarRuleExpressionCommand.dart';
import 'tree/rule/GrammarRuleExpressionRegExString.dart';

/**
 * ??? Maybe this is not in use???
 * The ScannerTokenCollector collects all
 */

class ScannerTokenCollector extends  GrammarRuleExpressionTreeCommand {
  List<Object> collectedElements = new List<Object>();

  doCommand(GrammarRuleExpression expression, SLOPEGrammarObject grammar) {
    if (expression is GrammarRuleExpressionRegExString) {
      String aString =
          (expression as GrammarRuleExpressionRegExString).regexString;
      ResultObject<RegEx> result = new  ResultObject<RegEx>();
      new RegExStringParser().parseString(aString, result);

      if (!collectedElements.contains(result.getResult().toString())) {
//        print("added: " + result.getResult().toString());
//        print("added: " + aString);
//        collectedElements.add(result.getResult().toString());
        collectedElements.add(RegExStringParser.fromString(aString));
      }
    }
    return false;
  }

}