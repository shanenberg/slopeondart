import '../../parser/parserconstruction/Rule.dart';
import '../GrammarRuleExpression.dart';

class GrammarRuleObject {
  String name;
  GrammarRuleExpression body;
  Rule rule;
}