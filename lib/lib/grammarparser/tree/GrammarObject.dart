import '../../dynamicParser/DynamicParser.dart';
import '../../parser/ResultObject.dart';
import '../../parser/parserconstruction/Rule.dart';
import '../../parser/tree/TreeNode.dart';
import '../ScannerRuleObject.dart';
import '../ScannerTokenCollector.dart';
import '../Separator.dart';
import 'GrammarRuleObject.dart';
import 'GrammarTest.dart';

SEPARATOR(var element) {
  return new Separator(element);
}

class SLOPEGrammarObject {
  String grammarName;
  String startRuleName;
  List<GrammarTest> tests = new List<GrammarTest>();
  List<GrammarRuleObject> rules = new List<GrammarRuleObject>();
//  List<ScannerRuleObject> scanners = new List<ScannerRuleObject>();
  List<ScannerRuleObject> topScanners = new List<ScannerRuleObject>();
  List<ScannerRuleObject> bottomScanners = new List<ScannerRuleObject>();
  DynamicParser parser;

  TreeNode parseString(String inString) {
    var p = createParser();
    return p.parseString(inString, new ResultObject());
  }



  DynamicParser createParser() {
    parser = new DynamicParser(this);
    return parser;
  }

  DFSonRules() {
    for(GrammarRuleObject rule in rules) {

    }
  }

  GrammarRuleObject getStartRule() {
    GrammarRuleObject ret = getRule(this.startRuleName);
    ret.name = this.startRuleName;
    ret.body.ruleExpressionName = this.startRuleName;
//print(ret.body);
//print("start rule: " + this.startRuleName);
    return ret;
  }

  GrammarRuleObject getRule(String aRuleName) {
    for(GrammarRuleObject ruleObject in rules) {
      if (ruleObject.name==aRuleName) {
        return ruleObject;
      }
    }
    return null;
  }

  ScannerRuleObject getScannerRule(String aRuleName) {
    for(ScannerRuleObject ruleObject in topScanners) {
      if (ruleObject.name==aRuleName) {
        return ruleObject;
      }
    }
    for(ScannerRuleObject ruleObject in bottomScanners) {
      if (ruleObject.name==aRuleName) {
        return ruleObject;
      }
    }
    return null;
  }

  List<Object> getScannerElements() {
    ScannerTokenCollector collector = new ScannerTokenCollector();

    for(ScannerRuleObject aRule in topScanners) {
      Object toAdd = aRule.regexObject;
      if (aRule.isSeparator)
        toAdd = SEPARATOR(toAdd);
      collector.collectedElements.add(toAdd);
    }

    for(GrammarRuleObject aRule in rules) {
      aRule.body.preorderWalk(this, collector);
    }

    for(ScannerRuleObject aRule in bottomScanners) {
      Object toAdd = aRule.regexObject;
      if (aRule.isSeparator)
        toAdd = SEPARATOR(toAdd);
      collector.collectedElements.add(toAdd);
    }

    return collector.collectedElements;
  }

  //int testnum = 1;
  String currentTest;

  TreeNode doTestsFromName(String name) {
    bool nameFound = false;
    List<String> names = [];
    for(GrammarTest aTest in tests) {
      if(aTest.testname == name)
        nameFound = true;
      if(nameFound)
        names.add(aTest.testname);
    }
    doTests(names);
  }

  TreeNode doTests([List<String> testnames = null]) {
    parser = new DynamicParser(this);
    for (GrammarTest aTest in tests) {

      if(testnames != null && !(testnames.contains(aTest.testname)))
        continue;

      print('''Testing: 
      ${aTest.testname} ${aTest.teststring} ''');

      currentTest = aTest.testname;

      bool failed = false;

      try {
        parser.tokenStream = parser.scanString(aTest.teststring);
      } catch(e)
      {
        if(!aTest.fail_scanning) throw '''
        (ノಠ益ಠ)ノ ︵ ┻━┻
        SCANNING failed: 
          ${aTest.testname} "${aTest.teststring}"
   
          Exception:
          $e    
          ''';
        failed = true;
      }

      if(!failed && aTest.fail_scanning)
        throw '''
        (ノಠ益ಠ)ノ ︵ ┻━┻
        SCANNING should have failed:
        ${aTest.testname} "${aTest.teststring}"
      ''';


      failed = false;

      //      parser.tokenStream.printStream();
      Rule r = this.getStartRule().body.createParserConstructor(this);
      try {
        parser.doParsing(r);


      } catch(e)
      {
        if(!aTest.fail_parsing)
          throw '''
          (ノಠ益ಠ)ノ ︵ ┻━┻
          PARSING failed: 
          ${aTest.testname} "${aTest.teststring}"
   
          Exception:
          $e    
          ''';
        failed = true;
      }
      if(!failed && aTest.fail_parsing)
        throw '''
        (ノಠ益ಠ)ノ ︵ ┻━┻
        PARSING should have failed:
        ${aTest.testname} "${aTest.teststring}"
      ''';

      //testnum++;
    }
    print("¯\\_(ツ)_/¯");
    print("Everything fine, huh");

  }

  bool hasRuleNamed(String token) {
    return this.getRule(token) != null;
  }

  bool hasScannerRuleNamed(String token) {
    return this.getScannerRule(token) != null;
  }
}