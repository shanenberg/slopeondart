import '../../../parser/parserconstruction/ParserConstructor.dart';
import '../../../parser/parserconstruction/Rule.dart';
import '../../../parser/features/optional/OPTIONAL_ParserConstructor.dart';
import '../../tree/GrammarObject.dart';
import '../../GrammarRuleExpression.dart';
import 'GrammarRuleUnary.dart';

class GrammarRuleExpressionOPT extends GrammarRuleUnary {
  GrammarRuleExpressionOPT(GrammarRuleExpression element) : super(element);

  ParserConstructor createParserConstructor(SLOPEGrammarObject g) {
    this.grammarObject = g;
    return new Rule(() =>
      new OPTIONAL_ParserConstructor(this.element.createParserConstructor(grammarObject))
    );
  }
}