import '../../../parser/parserconstruction/ParserConstructor.dart';
import '../../../parser/parserconstruction/Rule.dart';
import '../../../parser/features/or/OR_ParserConstructor.dart';
import '../../tree/GrammarObject.dart';
import '../../GrammarRuleExpression.dart';

class GrammarRuleExpressionOR extends GrammarRuleNAry {
  ParserConstructor createParserConstructor(SLOPEGrammarObject g) {
    this.createParserConstructors(g);
    return new Rule(()=>
      new OR_ParserConstructor(this.parserConstructors)
    );
  }
}
