import '../../tree/GrammarObject.dart';
import '../../GrammarRuleExpression.dart';
import 'GrammarRuleExpressionCommand.dart';

abstract class GrammarRuleUnary extends GrammarRuleExpression {
  GrammarRuleExpression element;
  GrammarRuleUnary(this.element);

  @override
  void preorderWalk(SLOPEGrammarObject grammar, GrammarRuleExpressionTreeCommand command) {
    bool wantsToStop = command.doCommand(this, grammar);
    if (!wantsToStop) {
      element.preorderWalk(grammar, command);
    }
  }
}