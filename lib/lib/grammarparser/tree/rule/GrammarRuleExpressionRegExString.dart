import '../../../parser/parserconstruction/ParserConstructor.dart';
import '../../../parser/parserconstruction/Rule.dart';
import '../../../parser/features/keyword/KEYWORD_ParserConstructor.dart';
import '../../../regex/RegExStringParser.dart';
import '../../tree/GrammarObject.dart';
import '../../GrammarRuleExpression.dart';
import 'GrammarRuleExpressionCommand.dart';

class GrammarRuleExpressionRegExString extends GrammarRuleExpression {
  String regexString;

  GrammarRuleExpressionRegExString(this.regexString);
  @override
  void preorderWalk(SLOPEGrammarObject grammar, GrammarRuleExpressionTreeCommand command) {
    bool wantsToStop = command.doCommand(this, grammar);
  }

  ParserConstructor createParserConstructor(SLOPEGrammarObject g) {
//    print("************************** GrammarRuleExpressionRegExString $regexString");
    this.grammarObject = g;
    return new KEYWORD_ParserConstructor(RegExStringParser.fromString(regexString).toString());
  }

}