import '../../../parser/parserconstruction/ParserConstructor.dart';
import '../../../parser/parserconstruction/Rule.dart';
import '../../../parser/features/nfold/NFOLD_ParserConstructor.dart';
import '../../tree/GrammarObject.dart';
import '../../GrammarRuleExpression.dart';
import 'GrammarRuleUnary.dart';

class GrammarRuleExpressionSTAR extends GrammarRuleUnary {
  GrammarRuleExpressionSTAR(GrammarRuleExpression element) : super(element);

  ParserConstructor createParserConstructor(SLOPEGrammarObject g) {
//    print("**************************");
    this.grammarObject = g;
    return new Rule(()=>
      new NFOLD_ParserConstructor(this.element.createParserConstructor(grammarObject))
    );
  }
}