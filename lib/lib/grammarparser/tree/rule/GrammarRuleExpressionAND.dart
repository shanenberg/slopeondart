import '../../../parser/parserconstruction/ParserConstructor.dart';
import '../../../parser/parserconstruction/Rule.dart';
import '../../../parser/features/and/AND_ParserConstructor.dart';
import '../../tree/GrammarObject.dart';
import '../../GrammarRuleExpression.dart';

class GrammarRuleExpressionAND extends GrammarRuleNAry {
  Rule createParserConstructor(SLOPEGrammarObject g) {

    this.createParserConstructors(g);

    Rule r = new Rule(() {
      ParserConstructor ret =
      new Rule(() {
        AND_ParserConstructor pc = new AND_ParserConstructor(
            this.parserConstructors);
        pc.parserName = this.ruleExpressionName;
        return pc;
      });
      ret.parserName = this.ruleExpressionName;
      return ret;
    });
    r.parserName = this.ruleExpressionName;
    return r;
  }


  bool isAND() {return true;}
}
