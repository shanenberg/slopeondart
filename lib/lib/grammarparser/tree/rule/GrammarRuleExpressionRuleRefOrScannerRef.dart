import '../../../parser/parserconstruction/ParserConstructor.dart';
import '../../../parser/parserconstruction/Rule.dart';
import '../../../parser/features/keyword/KEYWORD_ParserConstructor.dart';
import '../../../parser/features/rule/AppliedRule_ParserConstructor.dart';
import '../../tree/GrammarObject.dart';
import '../../GrammarRuleExpression.dart';
import 'GrammarRuleExpressionCommand.dart';

class GrammarRuleExpressionRuleRefOrScannerRef extends GrammarRuleExpression {
  String token;

  GrammarRuleExpressionRuleRefOrScannerRef(this.token);
  @override
  void preorderWalk(SLOPEGrammarObject grammar, GrammarRuleExpressionTreeCommand command) {
    bool wantsToStop = command.doCommand(this, grammar);
  }

  ParserConstructor createParserConstructor(SLOPEGrammarObject g) {
//print("create rule: " + token);
    this.grammarObject = g;
    if (grammarObject.hasRuleNamed(token)) {
      GrammarRuleExpression body = grammarObject.getRule(token).body;
      body.ruleExpressionName = token.toString();
//print("LibGrammar: bodyName: " + token.toString());
//      ParserConstructor ret =  new AppliedRule_ParserConstructor(new Rule(() {
//        Rule pc = body.createParserConstructor(g);
//        pc.parserName = token.toString();
//        return pc;
//      }));

      ParserConstructor ret =  new Rule(()=>
          body.createParserConstructor(g)
      );
      ret.parserName = token.toString();
        return ret;

      ret.parserName = token.toString();
//print("LibGrammar: set ruleName: " + token.toString());
      return ret;
    }
    if (grammarObject.hasScannerRuleNamed(token)) {
//      Rule r = new Rule(()=> new KEYWORD_ParserConstructor(grammarObject.getScannerRule(token).regexObject.toString()));
      ParserConstructor ret = new Rule(()=>
        new KEYWORD_ParserConstructor(grammarObject.getScannerRule(token).regexObject.toString())
      );
      ret.parserName = token;
      return ret;
      Rule r;
//        grammarObject.getScannerRule(token). .body.createParserConstructor(g);
    }
  }

}