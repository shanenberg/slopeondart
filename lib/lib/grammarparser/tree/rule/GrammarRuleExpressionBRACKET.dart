import '../../../parser/parserconstruction/ParserConstructor.dart';
import '../../../parser/parserconstruction/Rule.dart';
import '../../../parser/features/bracket/BRACKET_ParserConstructor.dart';
import '../../tree/GrammarObject.dart';
import '../../GrammarRuleExpression.dart';
import 'GrammarRuleUnary.dart';

class GrammarRuleExpressionBRACKET extends GrammarRuleUnary {
  GrammarRuleExpressionBRACKET(GrammarRuleExpression element) : super(element);

  ParserConstructor createParserConstructor(SLOPEGrammarObject g) {
    this.grammarObject = g;
    return
      new Rule(()=>
      new BRACKET_ParserConstructor(
          this.element.createParserConstructor(grammarObject))
      );
    }
}