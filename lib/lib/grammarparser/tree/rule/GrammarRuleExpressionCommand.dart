import '../../tree/GrammarObject.dart';
import '../../GrammarRuleExpression.dart';

abstract class GrammarRuleExpressionTreeCommand {
  doCommand(GrammarRuleExpression expression, SLOPEGrammarObject grammar);
}
