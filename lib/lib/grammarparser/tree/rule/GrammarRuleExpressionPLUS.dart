import '../../../parser/parserconstruction/ParserConstructor.dart';
import '../../../parser/parserconstruction/Rule.dart';
import '../../../parser/features/nfold1/NFOLD1_ParserConstructor.dart';
import '../../tree/GrammarObject.dart';
import '../../GrammarRuleExpression.dart';
import 'GrammarRuleUnary.dart';

class GrammarRuleExpressionPLUS extends GrammarRuleUnary {
  GrammarRuleExpressionPLUS(GrammarRuleExpression element) : super(element);

  ParserConstructor createParserConstructor(SLOPEGrammarObject g) {
    this.grammarObject = g;
    return
      new Rule(()=>
      new NFOLD1_ParserConstructor(
          this.element.createParserConstructor(grammarObject))
      );
    }
}