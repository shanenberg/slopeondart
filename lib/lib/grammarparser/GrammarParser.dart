import '../automata/regex/RegEx.dart';
import '../parser/ResultObject.dart';
import '../parser/parserconstruction/Rule.dart';
import '../regex/RegExStringParser.dart';
import 'GrammarRuleExpression.dart';
import 'ScannerRuleObject.dart';
import 'SlopeGrammar.dart';
import 'Tokens.dart';
import 'tree/GrammarObject.dart';
import 'tree/GrammarRuleObject.dart';
import 'tree/GrammarTest.dart';
import 'tree/rule/GrammarRuleExpressionAND.dart';
import 'tree/rule/GrammarRuleExpressionBRACKET.dart';
import 'tree/rule/GrammarRuleExpressionOPT.dart';
import 'tree/rule/GrammarRuleExpressionOR.dart';
import 'tree/rule/GrammarRuleExpressionPLUS.dart';
import 'tree/rule/GrammarRuleExpressionRegExString.dart';
import 'tree/rule/GrammarRuleExpressionRuleRefOrScannerRef.dart';
import 'tree/rule/GrammarRuleExpressionStar.dart';

class GrammarParser extends SlopeGrammar {
  GrammarParser() : super(null);

  RegEx identifier = Tokens.fromREGEX("([a-z]|[A-Z]|_)([a-z]|[A-Z]|_|[0-9])*");

  RegEx stringLiteral = Tokens.fromREGEX('"((^"~)|~~|~")*"');


  String unescapeStringLiteral(String s) {
//    print("*********** UNECAPE" + "String: " + s);
    StringBuffer ret = new StringBuffer();
    int i=1;
    for (;i<s.length-2;i++) {
      if (s.codeUnitAt(i).compareTo('~'.codeUnitAt(0))==0) {
//print("RET" + s.substring(i, i + 2));

        if (s.substring(i, i + 2).compareTo("~\"") == 0) {
//print("RET" + ret.toString()  + " " + i.toString());
          ret.write("\"");
          i++;
        } else if (s.substring(i, i + 2).compareTo("~~") == 0) {
//print("RET" + ret.toString()+ " " + i.toString());
          ret.write("~");
          i++;
        } else {
          throw "invalid unescaped char";
        }
      } else {
//print("RET" + ret.toString()+ " " + i.toString());
        ret.write(new String.fromCharCode(s.codeUnitAt(i)));
      }
    }
    if (i<s.length-1) {
//print("RET" + ret.toString());
//print(i);
//print(s.length);
      ret.write(new String.fromCharCode(s.codeUnitAt(i)));
    }

//    print("*********** UNESCAPED" + ret.toString());
    return ret.toString();
  }


  tokens() => ["Tests:", "Scanner:", "SEPARATOR", "FAIL_PARSING", "FAIL_SCANNING", "TOP", "BOTTOM",
  "Rules:", identifier, stringLiteral,
  "\\?", "=>", "\\+", "\\*", "\\(", "\\)", "\\|", "\\.", SEPARATOR('/\\*(^\\*|\\*(^/))+\\*/'),

  SEPARATOR(" "), SEPARATOR("\n"), SEPARATOR("\r"), SEPARATOR("\t")];

  Rule Start(ResultObject<SLOPEGrammarObject> result) => BODY(() {

    SLOPEGrammarObject grammarObject = new SLOPEGrammarObject();
    ResultObject<String> grammarName = new ResultObject<String>();
    ResultObject<GrammarRuleObject> ruleObject = new ResultObject<GrammarRuleObject>();
    ResultObject<ScannerRuleObject> scannerRuleObject = new ResultObject<ScannerRuleObject>();
    ResultObject<String> testString = new ResultObject<String>();
    ResultObject<String> testName = new ResultObject<String>();
    ResultObject<String> fail_parsing = new ResultObject<String>();
    ResultObject<String> fail_scanning = new ResultObject<String>();

    return
      AND([
        REGEXObj(identifier, grammarName), DO(()=>grammarObject.grammarName=grammarName.getResult()),
        TOKEN("Rules:"),
        REGEXObj(identifier, grammarName), DO(()=>grammarObject.startRuleName=grammarName.getResult()),
        NFOLD(
            AND([
              GrammarRule(ruleObject),
//            DO(()=>ruleObject.getResult().name = "dummy"),
              DO(()=>grammarObject.rules.add(ruleObject.getResult()))
            ])
        ),
        OPTIONAL(
            AND([
              TOKEN("Scanner:"),
              NFOLD1(
                  AND([
                    ScannerRule(scannerRuleObject),
                    DO(()=>scannerRuleObject.getResult().addToGrammarObject(grammarObject))
                  ])
              )
            ])
        ),
        OPTIONAL(
            AND([
              TOKEN("Tests:"),
              NFOLD1(
                  AND([
                    REGEXObj(identifier, testName),
                    REGEXObj(stringLiteral, testString),
                    OPTIONAL(
                        AND([
                          TOKEN("FAIL_PARSING", fail_parsing),
                          OPTIONAL(
                              TOKEN("FAIL_SCANNING", fail_scanning)
                          )
                        ])
                    ),
                    TOKEN("."),
                    DO(() {
                      grammarObject.tests.add(new GrammarTest(testName.getResult(), unescapeStringLiteral(testString.getResult()),
                          fail_parsing.getResult() != null ? true : false,
                          fail_scanning.getResult() != null ? true : false));
                      fail_scanning.setResult(null);
                      fail_parsing.setResult(null);})
//                    DO(()=>print("unescaped testString: " + unescapeStringLiteral(testString.getResult())))
                  ])
              )
            ])
        ),
        DO(()=>result.setResult(grammarObject)),
      ]);
  });

  Rule ScannerRule(ResultObject<ScannerRuleObject> result) => BODY(() {
    ScannerRuleObject scannerRuleObject = new ScannerRuleObject();
    ResultObject<String> name = new ResultObject<String>();
    ResultObject<String> expression = new ResultObject<String>();
    ResultObject<GrammarRuleExpression> grammarExpression = new ResultObject<GrammarRuleExpression>();
    return
      AND([
        REGEXObj(identifier, name),
        DO(()=>scannerRuleObject.name = name.getResult()),
        TOKEN("=>"),
        OPTIONAL(
            AND([
              TOKEN("SEPARATOR"),
              DO(()=>scannerRuleObject.isSeparator = true)
            ])
        ),
        REGEXObj(stringLiteral, expression),
        OPTIONAL(OR([
          AND([
            TOKEN("TOP"), DO(()=>scannerRuleObject.isTop=true)
          ]),
          AND([
            TOKEN("BOTTOM"), DO(()=>scannerRuleObject.isBottom=true)
          ])
        ])),
        DO(() => scannerRuleObject.scannerExpression = unescapeStringLiteral(expression.getResult())),
        DO(() => scannerRuleObject.regexObject = RegExStringParser.fromString(unescapeStringLiteral(expression.getResult()))),
//        DO(() => print(scannerRuleObject.scannerExpression.toString())),
        TOKEN("."),
        DO(()=>result.setResult(scannerRuleObject))
      ]);
  });

  Rule GrammarRule(ResultObject<GrammarRuleObject> result) => BODY(() {
    GrammarRuleObject ruleObject = new GrammarRuleObject();
    ResultObject<String> name = new ResultObject<String>();
    ResultObject<GrammarRuleExpression> grammarExpression = new ResultObject<GrammarRuleExpression>();

    return
      AND([
        REGEXObj(identifier, name), DO(()=>ruleObject.name = name.getResult()),
        TOKEN("=>"),
        RuleExpression(grammarExpression),
        DO(() {
//          if (grammarExpression.getResult().isAND()) {
//            ruleObject.body = grammarExpression.getResult();
//          } else {
            GrammarRuleExpressionAND and = new GrammarRuleExpressionAND();
            and.elements.add(grammarExpression.getResult());
            ruleObject.body = and;
//          }
        }),
        TOKEN("."),
        DO(() {
          result.setResult(ruleObject);
        })
    ]);

  });

  Rule RuleExpression(ResultObject<Object> result) => BODY(() {
    ResultObject<GrammarRuleExpression> ret = new ResultObject<GrammarRuleExpression>();
    ResultObject<GrammarRuleExpression> ret2 = new ResultObject<GrammarRuleExpression>();
    GrammarRuleExpressionOR retOR = new GrammarRuleExpressionOR();
    return
      AND([
        ConcatenatedSimpleExpressions(ret),
        DO(()=>retOR.elements.add(ret.getResult())),
        NFOLD(
            AND([
              TOKEN("|"), ConcatenatedSimpleExpressions(ret2),
              DO(()=>retOR.elements.add(ret2.getResult())),
            ])
        ),
        DO(() {
          if (retOR.elements.length>1) {
//            GrammarRuleExpressionAND retAnd = new GrammarRuleExpressionAND();
//            retAnd.elements.add(retOR);
            result.setResult(retOR);
          } else
            result.setResult(ret.getResult());
        })
      ]);
  });

  Rule ConcatenatedSimpleExpressions(ResultObject<GrammarRuleExpression> result) => BODY(() {
    ResultObject<GrammarRuleExpression> ret = new ResultObject<GrammarRuleExpression>();
    GrammarRuleExpressionAND retAnd = new GrammarRuleExpressionAND();
    return
      AND([
        SimpleExpression(ret),
        DO(()=> retAnd.elements.add(ret.getResult())),
        NFOLD(
            AND([
              SimpleExpression(ret),
              DO(()=> retAnd.elements.add(ret.getResult())),
            ])
        ),
        DO(()=>result.setResult(retAnd))
      ]);
  });

  Rule SimpleExpression(ResultObject<GrammarRuleExpression> result) => BODY(() {
    ResultObject<GrammarRuleExpression> ret = new ResultObject<GrammarRuleExpression>();
    ResultObject<String> aString = new ResultObject<String>();
    return
      AND([
        OR([
          BracketExpression(ret),
          AND([REGEXObj(identifier, aString), DO(()=> ret.setResult(new GrammarRuleExpressionRuleRefOrScannerRef(aString.getResult())))]),
          AND([REGEXObj(stringLiteral, aString), DO(()=>ret.setResult(
              new GrammarRuleExpressionRegExString(unescapeStringLiteral(aString.getResult()))))])
        ]),
        OPTIONAL(
            OR([
              AND([TOKEN("+"), DO(()=>ret.setResult(new GrammarRuleExpressionPLUS(ret.getResult())))]),
              AND([TOKEN("?"), DO(()=>ret.setResult(new GrammarRuleExpressionOPT(ret.getResult())))]),
              AND([TOKEN("*"), DO(()=>ret.setResult(new GrammarRuleExpressionSTAR(ret.getResult())))])
            ])
        ),
        DO(()=>result.setResult(ret.getResult()))
      ]);
  });

  Rule BracketExpression(ResultObject<GrammarRuleExpression> result) => BODY(() {
    ResultObject<GrammarRuleExpression> ret = new ResultObject<GrammarRuleExpression>();

    return
      AND([
        TOKEN("("), RuleExpression(ret), TOKEN(")"),
        DO(() {
          result.setResult(new GrammarRuleExpressionBRACKET(ret.getResult()));

        })
      ]);
  });

}