import 'package:test/test.dart';

import '../../parser/ResultObject.dart';
import '../GrammarParser.dart';
import '../tree/GrammarObject.dart';

s (String s) {
  ResultObject<SLOPEGrammarObject> result = new ResultObject<SLOPEGrammarObject>();
  var g = new GrammarParser();
  g.parseString(s, result);
  return result.getResult();
}

main() {

  test("Grammar test - Checks whether grammar string is parsed", () {
    s('AGr Rules: start start=>"A".');
    s('AGr Rules: start start=>"A". Tests: aTest "A".');
    s('AGr Rules: start start=>"A". Tests: aTest "X" FAIL_PARSING.');
    s('AGr Rules: start start=>"A". Tests: aTest "X" FAIL_PARSING FAIL_SCANNING.');
    s('AGr Rules: start start=>"A". Scanner: anA => "A".');
    s('AGr Rules: start start=>"A". Scanner: anA => "A". nl => SEPARATOR "x". ');
    s('AAGr Rules: start start=>A. A=>"A".');
    s('AABMyGrammar Rules: start start=>A|B. A=>"A". B=>"B".');
  });
}