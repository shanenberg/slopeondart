import '../automata/regex/RegEx.dart';
import 'tree/GrammarObject.dart';

class ScannerRuleObject {
  String name;
  String scannerExpression;
  RegEx regexObject;

  bool isSeparator = false;
  bool isTop = false;
  bool isBottom = false;

  void addToGrammarObject(SLOPEGrammarObject grammarObject) {
    if (isTop) {
      grammarObject.topScanners.add(this);
    } else {
      grammarObject.bottomScanners.add(this);
    }
  }

}