import '../automata/alphabet/CharAlphabet.dart';
import '../automata/regex/RegEx.dart';
import '../parser/CharParsing.dart';
import '../parser/ResultObject.dart';
import '../parser/parserconstruction/Rule.dart';
import '../scanner/CharScanner.dart';
import '../scanner/RegExWithKind.dart';
import '../scanner/ScannerOutputStream.dart';
import '../scanner/ScannerToken.dart';
import '../stream/StringTokenStream.dart';
import 'Separator.dart';
import 'Tokens.dart';

abstract class SlopeGrammar extends CharParsing {

  SlopeGrammar(ScannerOutputStream<ScannerToken<int, CharAlphabet>> tokenStream) : super(tokenStream);

  List<Object> tokens();

  scanString(String toParse) {
    StringTokenStream stream = new StringTokenStream(toParse);
    List<RegExWithKind<int, CharAlphabet>> tokenRegExs = tokensToRegExWithKindList();
    CharScanner scanner = new CharScanner(stream, tokenRegExs);
//    print("SCAN: " + toParse);

    return scanner.doScanning();
  }

  parseString(String toParse, ResultObject result) {

//print(toParse);

    this.tokenStream = scanString(toParse);

    Rule r = this.Start(result);
//    this.tokenStream.printStream();
    var ret = this.doParsing(r);
//    ret.nodeName = r.parserName;
    return ret;
  }

  List<RegExWithKind<int, CharAlphabet>> tokensToRegExWithKindList() {
    List<RegExWithKind<int, CharAlphabet>> tokenRegExs = new List<RegExWithKind<int, CharAlphabet>>();
//print("tokensToRegExWithKindList");
    List<Object> regExList = tokens();
    for(int i=0; i< regExList.length;i++) {
//      print(regExList[i])     ;
      if (regExList[i] is RegEx<int, CharAlphabet>) {
        tokenRegExs.add(new RegExWithKind<int, CharAlphabet>(regExList[i], false));
//        print(regExList[i]);
      } else if (regExList[i] is String) {
        String e = regExList[i];
        tokenRegExs.add(new RegExWithKind<int, CharAlphabet>(
            Tokens.fromREGEX(e), false));
//        print(regExList[i]);
      } else if (regExList[i] is Separator && (regExList[i] as Separator).element is String) {
        String e = (regExList[i] as Separator).element;
        tokenRegExs.add(new RegExWithKind<int, CharAlphabet>(
            Tokens.fromREGEX(e), true));
      } else if (regExList[i] is Separator && (regExList[i] as Separator).element is RegEx) {
        tokenRegExs.add(new RegExWithKind<int, CharAlphabet>((regExList[i] as Separator).element, true));
      } else {
        throw "Something is wrong with the tokensRegEx list...wrong types?";
      }
    }
//    print("******************");
    return tokenRegExs;
  }

  Rule Start(ResultObject result);

}
