import '../parser/parserconstruction/ParserConstructor.dart';
import 'tree/GrammarObject.dart';
import 'tree/rule/GrammarRuleExpressionCommand.dart';

abstract class GrammarRuleExpression {
  String ruleExpressionName;
  SLOPEGrammarObject grammarObject;
  void preorderWalk(SLOPEGrammarObject grammar, GrammarRuleExpressionTreeCommand command);
  ParserConstructor createParserConstructor(SLOPEGrammarObject grammarObject);

  bool isAND() {return false;}
}

abstract class GrammarRuleNAry extends GrammarRuleExpression {
  List<GrammarRuleExpression> elements = new List<GrammarRuleExpression>();
  List<ParserConstructor> parserConstructors = new List<ParserConstructor>();

  GrammarRuleNAry();

  preorderWalk(SLOPEGrammarObject grammar, GrammarRuleExpressionTreeCommand command) {
    bool wantsToStop = command.doCommand(this, grammar);
    if (!wantsToStop) {
      for(GrammarRuleExpression element in elements) {
        element.preorderWalk(grammar, command);
      }
    }
  }

  void createParserConstructors(SLOPEGrammarObject g) {

    this.grammarObject = g;
    parserConstructors = new List<ParserConstructor>();
    for(GrammarRuleExpression element in elements) {
      ParserConstructor pc = element.createParserConstructor(g);
      parserConstructors.add(pc);
    }
  }

}