import '../automata/alphabet/CharAlphabet.dart';
import '../automata/regex/RegEx.dart';
import '../automata/regex/RegExAlternative.dart';
import '../automata/regex/RegExAnyExceptToken.dart';
import '../automata/regex/RegExConcatenation.dart';
import '../automata/regex/RegExKleene.dart';
import '../automata/regex/RegExOptional.dart';
import '../automata/regex/RegExPlus.dart';
import '../automata/regex/RegExRange.dart';
import '../automata/regex/RegExSingleToken.dart';
import '../automata/regex/RegExUnescapedToken.dart';
import '../parser/CharParsing.dart';
import '../parser/ResultObject.dart';
import '../parser/parserconstruction/Rule.dart';
import '../scanner/CharScanner.dart';
import '../scanner/RegExWithKind.dart';
import '../stream/StringTokenStream.dart';

CharAlphabet alphabet = new CharAlphabet();

List<int> toIntList(List<String> s) {
  List<int> ret = new List<int>();
  for(var aString in s) {
    ret.add(aString.codeUnitAt(0));
  }
  return ret;
}

class RegExStringParser extends CharParsing {


  static bool regExMatches(String regExString, String aStringToBeCompared) {
    ResultObject<RegEx<int, CharAlphabet>> result = new ResultObject<RegEx<int, CharAlphabet>>();
    new RegExStringParser().parseString(regExString, result);
    RegEx regex = result.getResult();

    StringTokenStream inputStream = new StringTokenStream(aStringToBeCompared);
    List<RegExWithKind<int, CharAlphabet>> tokenRegExs = new List<RegExWithKind<int, CharAlphabet>>();
    CharScanner scanner = new CharScanner(inputStream, [new RegExWithKind(regex, false)]);
    scanner.doScanning();
    return true;
  }

  static bool regExFail(String regExString, String aStringToBeCompared) {
    bool fail = true;
    try {
      regExMatches(regExString, aStringToBeCompared);
      fail = false;
    } catch (ex) {}
    if (!fail) throw "Test should have failed....but did not";
  }

  static RegEx fromString(String regExString) {
    ResultObject<RegEx<int, CharAlphabet>> result = new ResultObject<RegEx<int, CharAlphabet>>();
    new RegExStringParser().parseString(regExString, result);
    return result.getResult();
  }


  // singleToken : ^[]()-*+|.
  // regEx: (abc)*\[
  var simpleToken = new RegExAnyExceptToken(
      alphabet, toIntList([
    "[", "]", "(", ")", "-", "*", "+", "|", ".", "^", "?", "\\"
  ])
  );
  List<Object> tokens() => [simpleToken, "[", "]", "(", ")", "-", "*", "|", ".", "^", "+", "?", "\\"];

  RegExStringParser() : super(null);

  Rule RegularExpression(ResultObject<RegEx<int, CharAlphabet>> result) => BODY(() {
    // Tree done
    ResultObject<RegEx<int, CharAlphabet>> ret = new ResultObject<RegEx<int, CharAlphabet>>();
    ResultObject<RegEx<int, CharAlphabet>> rightTerm = new ResultObject<RegEx<int, CharAlphabet>>();

    return AND([
      SimpleConcatenations(ret),
      NFOLD(
          AND([
            TOKEN("|"),
            RegularExpression(rightTerm),
            SETValue(ret, ()=>new RegExAlternative(alphabet, ret.getResult(), rightTerm.getResult()))
          ])
      ),
      SET(result, ret),
    ]);
  });


  Rule SimpleConcatenations(ResultObject<RegEx<int, CharAlphabet>> result) => BODY(() {

    ResultObject<RegEx<int, CharAlphabet>> ro = new ResultObject<RegEx<int, CharAlphabet>>();
    ResultObject<RegEx<int, CharAlphabet>> inner = new ResultObject<RegEx<int, CharAlphabet>>();

    return

      AND([
        SimpleRegEx(ro),
        OPTIONAL(PostfixOperator(ro)),
        NFOLD(
          AND([
            SimpleRegEx(inner),
            OPTIONAL(PostfixOperator(inner)),
            SETValue(ro, ()=>new RegExConcatenation(alphabet, ro.getResult(), inner.getResult()))
          ]),
        ),
        SETValue(result, ()=>ro.getResult())
      ]);
  });

  Rule PostfixOperator(ResultObject<RegEx<int, CharAlphabet>> element) => BODY(() {
    return
      OR([
        AND([TOKEN("*"), SETValue(element, ()=> new RegExKleene(alphabet, element.getResult()))]),
        AND([TOKEN("+"), SETValue(element, ()=> new RegExPlus(alphabet, element.getResult()))]),
        AND([TOKEN("?"), SETValue(element, () => new RegExOptional(alphabet, element.getResult()))])
      ]);
  });

  Rule SimpleRegEx(ResultObject<RegEx<int, CharAlphabet>> result) => BODY(() =>
      OR([SingleToken(result), Except(result), BracketExpression(result), Group(result)])
  );


  Rule Group(ResultObject<RegEx<int, CharAlphabet>> result) => BODY(() {
    ResultObject<RegEx<int, CharAlphabet>> left = new ResultObject<RegEx<int, CharAlphabet>>();
    ResultObject<RegEx<int, CharAlphabet>> right = new ResultObject<RegEx<int, CharAlphabet>>();
    return
      AND([
        TOKEN("["), SingleToken(left), TOKEN("-"), SingleToken(right), TOKEN("]"),
        SETValue(result, () => new RegExRange(alphabet, (left.getResult() as RegExSingleToken).token, (right.getResult() as RegExSingleToken).token))
      ]);
  });

  Rule BracketExpression(ResultObject<RegEx<int, CharAlphabet>> result) => BODY(() {
    return
      AND([TOKEN("("), RegularExpression(result), TOKEN(")")]);
  });


  Rule SingleToken(ResultObject<RegEx<int, CharAlphabet>> result) => BODY(() {
    ResultObject<String> ret = new ResultObject<String>();
    return
      AND([
        OR([
          REGEXObj(simpleToken, ret),
          EscapedSingleTokens(ret),
        ]),
        SETValue(result, ()=> new RegExSingleToken(alphabet, ret.getResult().codeUnitAt(0))),
//        DO(()=>print("finish simpleToken" + ret.getResult().toString())),
      ]);
  });

  Rule EscapedSingleTokens(ResultObject<String> result) => BODY(() {
    ResultObject<String> res = new ResultObject<String>();
    return
      AND([
//        DO(()=>print("here")),
        TOKEN("\\"),
//        DEBUG(()=>print("in backslash")),
        OR([
          TOKEN("[", res), TOKEN("]", res), TOKEN(")", res), TOKEN("(", res), TOKEN(")", res), TOKEN("-", res), TOKEN(".", res),
          TOKEN("+", res), TOKEN("*", res), TOKEN("?", res), TOKEN("^", res), TOKEN("|", res), TOKEN("\\", res),
          AND([
            VALUE("n"),
//            DEBUG(()=>print("NEW LINE")),
            DO(()=>res.setResult("\n"))
          ]),
          AND([
            VALUE("r"),
//            DEBUG(()=>print("NEW LINE")),
            DO(()=>res.setResult("\r"))
          ]),
          AND([
            VALUE("t"),
//            DEBUG(()=>print("NEW LINE")),
            DO(()=>res.setResult("\t"))
          ])
        ]),
//        DEBUG(()=>print("finish backslash" + res.getResult())),
        DO(()=>result.setResult(res.getResult()))
      ]);
  });

  Rule Except(ResultObject<RegEx<int, CharAlphabet>> result) => BODY(() {

    ResultObject<RegExAnyExceptToken<int, CharAlphabet>> ro = new ResultObject<RegExAnyExceptToken>(new RegExAnyExceptToken(alphabet, new List()));
    ResultObject<RegEx<int, CharAlphabet>> singleToken = new ResultObject<RegEx<int, CharAlphabet>>();

    return
      AND([
        TOKEN("^"),
        NFOLD1(
            AND([
              SingleToken(singleToken),
              DO(()=>ro.getResult().exceptions.add((singleToken.getResult() as RegExSingleToken).token))
            ])),
        SET(result, ro)
      ]);
  });

  parseString(String toParse, ResultObject<RegEx<int, CharAlphabet>> result) {
    StringTokenStream stream = new StringTokenStream(toParse);

    List<RegExWithKind<int, CharAlphabet>> tokenRegExs = tokensToRegExWithKindList;
    CharScanner scanner = new CharScanner(stream, tokenRegExs);
//print("RegExParser: parse: ***" + toParse + "***");
    this.tokenStream = scanner.doScanning();
//this.tokenStream.printStream();
//print("Now run: ***" + this.RegularExpression(result).toString() + "***");
    return this.doParsing(this.RegularExpression(result));
  }

  List<RegExWithKind<int, CharAlphabet>> get tokensToRegExWithKindList {
    List<RegExWithKind<int, CharAlphabet>> tokenRegExs = new List<RegExWithKind<int, CharAlphabet>>();

    List<Object> regExList = tokens();
    for(int i=0; i< regExList.length;i++) {
      if (regExList[i] is RegEx<int, CharAlphabet>) {
        tokenRegExs.add(new RegExWithKind<int, CharAlphabet>(regExList[i], false));
      } else {
        if (regExList[i] is String) {
          String e = regExList[i];
          tokenRegExs.add(new RegExWithKind<int, CharAlphabet>(
              new RegExUnescapedSingleToken<int, CharAlphabet>(alphabet, e.codeUnitAt(0)), false));
        }
      }
    }
    return tokenRegExs;
  }

}
