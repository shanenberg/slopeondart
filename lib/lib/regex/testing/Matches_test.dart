import 'package:test/test.dart';

import '../RegExStringParser.dart';

m(regex, string) { RegExStringParser.regExMatches(regex, string); }
f(regex, string) { RegExStringParser.regExFail(regex, string); }

void main() {

  test("RegExParser matches - simple", () {
    m("a", "a");            f('a', 'b');          f('a', '');
    m("aa", "aa");          m("a*", "a");         m("a*", "aaaa");
    m("a*", "");            m("ab*", "a");        f('ab*', '');
    m("ab+", "abb");        m("(a|b)", "a");      m("(a|b)", "b");
    m("(a|b)*", "");        m("(a|b)*", "aaa");   m("(a|b)*", "aaabbbb");
    m("((a|b)(c|d))*", "acbc");
    m("ab?c", "abc");       m("ab?c", "ac");
    m("^a", "b");           m("^a", "c");         f("^a", "a");
  });

  test("RegExParser matches - Ranges", () {
    m("[c-f]", "c");      m("[g-k]", "i");    m("[m-p]", "o");
    m("[r-v]", "s");
  });

    // \\ => matches \, weil \ escaped ist
  test("RegExParser matches - complex", () {
    RegExStringParser.regExMatches("\"(^\")*\"", "\"asdasd\"");

    RegExStringParser.regExMatches('\\\\', '\\');

    RegExStringParser.regExMatches('"', '"'); // \\" matches
    RegExStringParser.regExMatches('\\\\"', '\\"'); // \\" matches
    RegExStringParser.regExMatches('"((^")|\\\\")*"', '""'); // \\" matches
    RegExStringParser.regExMatches('"((^")|\\\\")*"', '"\\\""'); // \\" matches

    // singular \ (printed twice, because it is escaped)
    RegExStringParser.regExMatches('\\\\', '\\\\');

    // singular \ (printed twice, because it is escaped)
    RegExStringParser.regExMatches('/\\*\\*/','/**/');

    // Java Comment
    RegExStringParser.regExMatches('/\\*(^\\*|\\*(^/))+\\*/', '/* abc* */');

    RegExStringParser.regExMatches(
        '((^"\\\\)|\\\\(^")|\\\\")+', 'asds\\r\\"'); // Java Comment

    RegExStringParser.regExMatches('\\.', '.'); // Java Comment
    RegExStringParser.regExMatches('\\n', '\n'); // Java Comment

    RegExStringParser.regExMatches(
        '"((^"\\\\)|(\\\\)+"|\\\\(^"))*"', '""'); // Java Comment
    RegExStringParser.regExFail('abc', ''); // Java Comment
    RegExStringParser.regExFail('abc', 'abcd'); // Java Comment
    RegExStringParser.regExFail('^x', ''); // Java Comment
    RegExStringParser.regExFail('(^a)', 'a'); // Java Comment
    RegExStringParser.regExFail('a*', 'b'); // Java Comment
    RegExStringParser.regExFail('^"', '"'); // Java Comment
    RegExStringParser.regExMatches(
        '((^"\\\\)|(\\\\)+"|\\\\(^"))*"', '"'); // Java Comment

  });
}