import 'package:test/test.dart';

import '../../automata/alphabet/CharAlphabet.dart';
import '../../automata/regex/RegEx.dart';
import '../../automata/regex/RegExAlternative.dart';
import '../../automata/regex/RegExAnyExceptToken.dart';
import '../../automata/regex/RegExConcatenation.dart';
import '../../automata/regex/RegExOptional.dart';
import '../../automata/regex/RegExSingleToken.dart';
import '../../parser/ResultObject.dart';
import '../RegExStringParser.dart';

/*
  \n
 */
CharAlphabet alphabet = new CharAlphabet(null,
    {
//      "\\":"\\\\",
//      "\n": "\\n",
//      "[": "\\[",
//      "]": "\\]",
//      "(": "\\(",
//      ")": "\\)",
//      "*": "\\*",
//      "+": "\\+",
//      ".": "\\.",
//      "-": "\\-",
//      "^": "\\^",
//      "|": "\\|"
    }
);


//
//class RET {
//}
//
//class RETUnaryOperator extends RET {
//  RET element;
//
//  RETUnaryOperator(this.element);
//}
//
//class RETOptional extends RETUnaryOperator {
//  RETOptional(RET element) : super(element);
//}
//
//class RETPlus extends RETUnaryOperator {
//  RETPlus(RET element) : super(element);
//}
//class RETKleene extends RETUnaryOperator {
//  RETKleene(RET element) : super(element);
//}
//
//class RETGroup extends RET {
//  RETSingleToken from, to;
//  RETGroup(this.from, this.to);
//}
//
//class RETSingleToken extends RET {
//  String value;
//  RETSingleToken(this.value);
//}
//
//class BinaryOperator extends RET{
//  RET left,right;
//
//  BinaryOperator(this.left, this.right);
//}
//
//class RETOR extends BinaryOperator {
//  RETOR(RET left, RET right) : super(left, right);
//}
//class RETAND extends BinaryOperator {
//  RETAND(RET left, RET right) : super(left, right);
//}
//
//class RETANYExcept extends RET {
//  List<RETSingleToken> exceptions = new List<RETSingleToken>();
//}


void main() {

  test("RegExParser matches", () {
    RegExStringParser.regExMatches("((a|b)(c|d))*", "acbc");
    RegExStringParser.regExMatches("\"(^\")*\"", "\"asdasd\"");
    RegExStringParser.regExMatches(
        '\\\\', '\\'); // \\ => matches \, weil \ escaped ist
    RegExStringParser.regExMatches('"', '"'); // \\" matches
    RegExStringParser.regExMatches('\\\\"', '\\"'); // \\" matches
    RegExStringParser.regExMatches('"((^")|\\\\")*"', '""'); // \\" matches
    RegExStringParser.regExMatches('"((^")|\\\\")*"', '"\\\""'); // \\" matches
    RegExStringParser.regExMatches(
        '\\\\', '\\\\'); // singular \ (printed twice, because it is escaped)
    RegExStringParser.regExMatches('/\\*\\*/',
        '/**/'); // singular \ (printed twice, because it is escaped)
    RegExStringParser.regExMatches(
        '/\\*(^\\*|\\*(^/))+\\*/', '/* abc* */'); // Java Comment
    RegExStringParser.regExMatches(
        '((^"\\\\)|\\\\(^")|\\\\")+', 'asds\\r\\"'); // Java Comment
    RegExStringParser.regExMatches('\\.', '.'); // Java Comment
    RegExStringParser.regExMatches('\\n', '\n'); // Java Comment

    RegExStringParser.regExMatches(
        '"((^"\\\\)|(\\\\)+"|\\\\(^"))*"', '""'); // Java Comment
    RegExStringParser.regExFail('abc', ''); // Java Comment
    RegExStringParser.regExFail('abc', 'abcd'); // Java Comment
    RegExStringParser.regExFail('^x', ''); // Java Comment
    RegExStringParser.regExFail('(^a)', 'a'); // Java Comment
    RegExStringParser.regExFail('a*', 'b'); // Java Comment
    RegExStringParser.regExFail('^"', '"'); // Java Comment
    RegExStringParser.regExMatches(
        '((^"\\\\)|(\\\\)+"|\\\\(^"))*"', '"'); // Java Comment
//    RegExStringParser.regExFail('(^")*"', '""');// Java Comment
//    RegExStringParser.regExFail('(^"\\\\)*"', '""');// Java Comment
  });

  test("RegExParser matches", () {

    var parseString = (String s) {
    ResultObject<RegEx<int, CharAlphabet>> result = new ResultObject<RegEx<int, CharAlphabet>>();
    new RegExStringParser().parseString(s, result);
    return result.getResult();
  };

  var parseStringFail = (String s) {
    bool error = false;
    try {
      ResultObject<RegEx<int, CharAlphabet>> result = new ResultObject<RegEx<int, CharAlphabet>>();
      parseString(s);
    } catch (exception) {
      error = true;
    }
    if (!error) {
      expect(true, false);
    }
  };

  tokenEquals(RegExSingleToken tokenInt, String s) {
    return tokenInt.token==s.codeUnitAt(0);
  }




    RegExSingleToken<int, CharAlphabet> r1 = parseString("a");
    expect(tokenEquals(r1, "a"), true);

    RegExAlternative<int, CharAlphabet> r2 = parseString("a|b");
    expect(tokenEquals(r2.left, "a"), true);
    expect(tokenEquals(r2.right, "b"), true);

    parseStringFail("|");

    RegExAnyExceptToken r3 = parseString("^a");
    expect(r3.exceptions.length==1, true);
    expect(new String.fromCharCode(r3.exceptions[0]) == "a", true);

    parseStringFail("^^a");

    var r4 = parseString("abc^a");
    var r5 = parseString("abc(^a)");
    var r6 = parseString("abc(^a)");
    var r7 = parseString("a(bc(^a))");

    RegExAlternative r8 = parseString("toBe|notToBe");


    var r9 = parseString("a[a-z]");

    var r10 = parseString("\\[");

    var r11 = parseStringFail("\\a");
    var r12 = parseString("bvc*");
    var r13 = parseString("bvc*a?[a-v]+sd?");
    var r14 = parseString("b?|vc*a?[a-v]+sd?");
    var r15 = parseStringFail("b|vc*a??[a-v]+sd");

    RegExConcatenation r16 = parseString("aa?");
    expect(r16.left is RegExSingleToken, true);
    expect(r16.right is RegExOptional, true);

    var r17 = parseString("([a-z]|[A-Z])([a-z]|[A-Z]|[0-9])*");
    var r18 = parseString("([a-z]|[A-Z]|_)([a-z]|[A-Z]|_)*");
    parseString("\\?");
    parseString("\\*");
    parseString("\\+");

  });

}