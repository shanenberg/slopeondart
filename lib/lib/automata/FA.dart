///Class representing a finite automaton (Also an alphabetic-element)
///

import 'dart:collection';

import 'alphabet/Alphabet.dart';
import 'alphabet/AlphabeticElement.dart';
import 'transition/Transition.dart';
///Class representing a finite automaton (Also an alphabetic-element)
///


abstract class FA<StateType, TokenType,AlphabetType extends Alphabet<TokenType>>

    extends AlphabeticElement<TokenType, AlphabetType> {

  ///Constructor defining alphabet of this automaton
  FA(Alphabet alphabet) : super(alphabet);

  ///Set of end-states
  Set<StateType> endStates = new Set<StateType>();

  ///Transition Table (Key = any SINGLE state, Value = List of Transitions)
  HashMap<StateType, List<Transition<StateType, TokenType, AlphabetType>>>
  transitionTable = new HashMap<StateType,
      List<Transition<StateType, TokenType, AlphabetType>>>();

  ///Method to add a transition to automaton.
  addTransition(StateType fromState,
      Transition<StateType, TokenType, AlphabetType> transition) {
    transitionTable.putIfAbsent(fromState,
            () => new List<Transition<StateType, TokenType, AlphabetType>>());
    transitionTable.putIfAbsent(transition.follower,
            () => new List<Transition<StateType, TokenType, AlphabetType>>());
    transitionTable[fromState].add(transition);
  }

  ///Execute next transition(s) matching the given token
  void goNext(TokenType input);

  ///Get a list of transitions from current
  List<Transition<StateType, TokenType, AlphabetType>>
  allTransitionsFromCurrent();

  ///Bool is in Endstate
  bool isInEndState();

  ///Get Number of States in automaton
  int numberOfState() {
    return transitionTable.keys.length;
  }

  bool acceptsInput(TokenType input) {
    List<Transition<StateType, TokenType, AlphabetType>> transitions = this.allTransitionsFromCurrent();
    for (Transition<StateType, TokenType, AlphabetType> aTransition in transitions) {
      if (aTransition.matches(alphabet, input)) return true;
    }
    return false;
  }

  Set<StateType> allStatesInFA() {
      Set<StateType> ret = new Set<StateType>();
      for(StateType aState in transitionTable.keys) {
        ret.add(aState);
        for(Transition<StateType, TokenType, AlphabetType> aTransition in transitionTable[aState]) {
          ret.add(aTransition.follower);
        }
      };
      return ret;
  }

}