import 'FA.dart';
import 'alphabet/Alphabet.dart';
import 'transition/Transition.dart';

/*
Make sure that in the inital state all epsilon states are reached
 */
///Nondeterministic Finite Automaton
class NFA<StateType, TokenType, AlphabetType extends Alphabet<TokenType>>
    extends FA<StateType, TokenType, AlphabetType> {
  NFA(Alphabet alphabet) : super(alphabet);

  StateType epsilon;
  Set<StateType> currentStates = new Set();

  /* Goes to the next states */
  void goNext(TokenType input) {
    Set<StateType> nextStates = new Set();
    bool aStateFound = false;
    for (var transition in allTransitionsFromCurrent()) {
      if (transition.matches(alphabet, input)) {
        nextStates.add(transition.follower);
        aStateFound = true;
      }
    }

    if (aStateFound == false) throw new Exception("No valid input found");
    currentStates = nextStates;
    doEpsilonTransitions();
  }

  /*  Starting from the current states, all epsilon-connected states are
      traversed   */
  void doEpsilonTransitions() {
    Set<StateType> unexploresStates = new Set<StateType>()
      ..addAll(currentStates);
    Set<StateType> nextStates = new Set<StateType>();

    while (unexploresStates.isNotEmpty) {
      StateType aState = unexploresStates.first;
      nextStates.add(aState);

      for (Transition<StateType, TokenType, AlphabetType> transition
      in transitionTable[aState]) {
        if (transition.isSpontaneous(alphabet)) {
          if (!nextStates.contains(transition.follower)) {
            unexploresStates.add(transition.follower);
          }
        }
      }
      unexploresStates.remove(aState);
    }

    currentStates = nextStates;
  }

  List<Transition<StateType, TokenType, AlphabetType>>
  allTransitionsFromCurrent() {
    List<Transition> ret = new List<Transition>();
    for (var aKey in transitionTable.keys) {
      if (currentStates.contains(aKey)) {
        ret.addAll(transitionTable[aKey]);
      }
    }
    return ret;
  }

  bool isInEndState() => endStates.intersection(currentStates).isNotEmpty;
}