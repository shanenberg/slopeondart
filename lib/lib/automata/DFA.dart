///Deterministic Finite Automaton

import 'dart:collection';

import 'FA.dart';
import 'alphabet/Alphabet.dart';
import 'transition/Transition.dart';
///Deterministic Finite Automaton


class DFA<StateType, InputType, AlphabetType extends Alphabet<InputType>>
    extends FA<StateType, InputType, AlphabetType> {
  DFA(AlphabetType alphabet) : super(alphabet);

  StateType currentState;

  HashMap<StateType, List<Transition<StateType, InputType, AlphabetType>>>
  transitionTable = new HashMap<StateType,
      List<Transition<StateType, InputType, AlphabetType>>>();

  void goNext(InputType input) {
    for (Transition aTransition in allTransitionsFromCurrent()) {
      if (aTransition.matches(alphabet, input)) {
        currentState = aTransition.follower;
        return;
      }
    }
    throw new Exception("Invalid input");
  }

  List<Transition<StateType, InputType, AlphabetType>>
  allTransitionsFromCurrent() {
    return transitionTable[currentState];
  }

  bool isInEndState() => endStates.contains(currentState);
}