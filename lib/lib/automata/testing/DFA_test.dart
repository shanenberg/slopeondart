import 'package:test/test.dart';

import '../DFA.dart';
import '../alphabet/CharAlphabet.dart';
import '../alphabet/tests/CharAlphabet_test.dart';
import '../transition/testing/_util.dart';

CharAlphabet alphabet = new CharAlphabet();

main() {
  /*
    Constructs a simple DFA
        42 -> "A" -> 1,
         1 -> "B" -> 2,
    checks whether
      - it accepts "A",
      - it switches its state on "A",
      - the end state 1 is reached.
   */

  test("DFA simple test - one transition", (){
    DFA<int, int, CharAlphabet> dfa = new DFA(alphabet);
    dfa.addTransition(42, symbolTransition("A", 1));

    dfa.currentState = 42;
    dfa.endStates.add(1);

    expect(dfa.acceptsInput(char("A")), true);
    dfa.goNext(char("A"));
    expect(dfa.currentState, 1);
    expect(dfa.isInEndState(), true);

  });


  /*
    Constructs a simple DFA
        42 -> "A" -> 1,
         1 -> "B" -> 2,
    checks whether
      - it accepts "A",
      - it switches its state on "A",
      - state 1 is reached.
      - "B" is accepted
      - switched to 2
      - reached an end state
   */

  test("DFA simple test - multiple transitions", (){
    DFA<int, int, CharAlphabet> dfa = new DFA(new CharAlphabet());
    dfa.addTransition(42, symbolTransition("A", 1));
    dfa.addTransition(1, symbolTransition("B", 2));

    dfa.currentState = 42;
    dfa.endStates.add(2);

    expect(dfa.acceptsInput(char("A")), true);
    dfa.goNext(char("A"));
    expect(dfa.currentState, 1);
    expect(dfa.isInEndState(), false);

    expect(dfa.acceptsInput(char("B")), true);
    dfa.goNext(char("B"));
    expect(dfa.currentState, 2);
    expect(dfa.isInEndState(), true);
  });
}