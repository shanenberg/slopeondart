import 'package:test/test.dart';

import '../NFA.dart';
import '../alphabet/CharAlphabet.dart';
import '../alphabet/tests/CharAlphabet_test.dart';
import '../transition/testing/_util.dart';

main() {


  /*
    Constructs a simple NFA
        42 -> "A" -> 1,
        42 -> "A" -> 2,
    checks whether
      - it accepts "A",
      - it switches its state on "A",
      - the next states 1,2 are reached.
   */

  test("NFA simple test - one transition", (){
    CharAlphabet alphabet = new CharAlphabet();
    NFA<int, int, CharAlphabet> nfa = new NFA(alphabet);
    nfa.addTransition(42, symbolTransition("A", 1));
    nfa.addTransition(42, symbolTransition("A", 2));

    nfa.currentStates = [42].toSet();
    nfa.endStates.add(1);

    expect(nfa.acceptsInput(char("A")), true);
    nfa.goNext(char("A"));
    expect(nfa.currentStates.contains(1), true);
    expect(nfa.currentStates.contains(2), true);
    expect(nfa.isInEndState(), true);

  });

  /*
    Tests Eps-Transition
        42 -> € -> 1,
        42 -> € -> 2
    doEpsilonTransitions
      => [42, 1, 2]
   */
  test("NFA simple test - do spontaneousTransition (1)", (){
    CharAlphabet alphabet = new CharAlphabet();
    NFA<int, int, CharAlphabet> nfa = new NFA(alphabet);
    nfa.currentStates = [42].toSet();
    nfa.addTransition(42, epsTransition(alphabet, 1));
    nfa.addTransition(42, epsTransition(alphabet, 2));

    nfa.doEpsilonTransitions();

    expect(nfa.currentStates, [42,1,2].toSet());

  });

  /*
    Tests Eps-Transition
        42 -> € -> 1,
        42 -> € -> 2,
        42 -> "A" -> 3
    doEpsilonTransitions
      => [42, 1, 2]
   */
  test("NFA simple test - do spontaneousTransition (2)", (){
    CharAlphabet alphabet = new CharAlphabet();
    NFA<int, int, CharAlphabet> nfa = new NFA(alphabet);
    nfa.currentStates = [42].toSet();
    nfa.addTransition(42, epsTransition(alphabet, 1));
    nfa.addTransition(42, epsTransition(alphabet, 2));
    nfa.addTransition(42, symbolTransition("A", 3));

    nfa.doEpsilonTransitions();

    expect(nfa.currentStates, [42,1,2].toSet());

  });


}