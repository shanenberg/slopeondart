import '../../scanner/state/FAStateFactory.dart';
import '../../scanner/state/RegExState.dart';
import '../NFA.dart';
import '../alphabet/Alphabet.dart';
import 'RegExConcatenation.dart';
import 'RegExSingleToken.dart';

abstract class RegEx<TokenType, AlphabetType extends Alphabet<TokenType>> {
  AlphabetType alphabet;

  RegEx(this.alphabet);

  void addToNFA(NFA nfa, RegExState startState, RegExState endState,
      FAStateFactory stateFactory);

  bool equals(Object other);

  String toString();

  static RegEx fromKeyword(Alphabet alphabet, String keyword) {

    RegEx ret = new RegExSingleToken(alphabet, keyword.codeUnitAt(0));

    if (keyword.length==1) {
      return ret;
    }

    for(int pos=1; pos<keyword.length; pos++) {
      ret =
      new RegExConcatenation(
          alphabet, ret,
          new RegExSingleToken(alphabet, keyword.codeUnitAt(pos)));

    }

    return ret;
  }
}