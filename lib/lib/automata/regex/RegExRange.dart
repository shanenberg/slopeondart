import '../../scanner/state/RegExState.dart';
import '../NFA.dart';
import '../alphabet/Alphabet.dart';
import '../transition/RangeTransition.dart';
import 'RegEx.dart';

///Regular Expression matching any expression except the tokens specified
class RegExRange<TokenType, AlphabetType extends Alphabet<TokenType>>
    extends RegEx<TokenType, AlphabetType> {
  RegExRange(AlphabetType alphabet, this.startToken, this.endToken)
      : super(alphabet);
  TokenType startToken;
  TokenType endToken;

  @override
  void addToNFA(
      NFA nfa, RegExState startState, RegExState endState, stateFactory) {
    nfa.addTransition(startState,
        new RangeTransition(alphabet, startToken, endToken, endState));
  }
  @override
  bool equals(Object other) {
    if (!(other is RegExRange)) return false;
    RegExRange that = other;
    return alphabet.compare(this.startToken, that.startToken) == 0 &&
        alphabet.compare(this.endToken, that.endToken) == 0;
  }

  String toString() {
    return "[" + alphabet.tokenToEscapedString(startToken) + "-" + alphabet.tokenToEscapedString(endToken) + "]";
  }
}
