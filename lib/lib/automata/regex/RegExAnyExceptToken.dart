import '../../scanner/state/RegExState.dart';
import '../NFA.dart';
import '../alphabet/Alphabet.dart';
import '../transition/AnyExceptSymbolTransition.dart';
import 'RegEx.dart';

///Regular Expression matching any expression except the tokens specified
class RegExAnyExceptToken<TokenType, AlphabetType extends Alphabet<TokenType>>
    extends RegEx<TokenType, AlphabetType> {
  RegExAnyExceptToken(AlphabetType alphabet, this.exceptions) : super(alphabet);
  List<TokenType> exceptions;

  @override
  void addToNFA(
      NFA nfa, RegExState startState, RegExState endState, stateFactory) {

    nfa.addTransition(startState,
        new AnyExceptSymbolTransition(alphabet, exceptions, endState));

    /**
     * I AM NOT SURE:::::THIS CODE LOOK WEIRD TO ME:::::
     */

  }

  @override
  bool equals(Object other) {
    if (!(other is RegExAnyExceptToken)) return false;
    RegExAnyExceptToken that = other;

    if (this.exceptions.length != that.exceptions.length) return false;

    for(var anException in this.exceptions) {
      if (!that.exceptions.contains(anException))
        return false;
    }
    return true;
  }

  String toString() {
    String ret = "(^";
    for(TokenType aToken in exceptions ) {
      ret = ret + alphabet.tokenToEscapedString(aToken);
    }
    return ret + ")";
  }
}
