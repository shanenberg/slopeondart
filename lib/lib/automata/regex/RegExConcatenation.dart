import '../../scanner/state/FAStateFactory.dart';
import '../../scanner/state/RegExState.dart';
import '../NFA.dart';
import '../alphabet/Alphabet.dart';
import 'RegEx.dart';

///Regular Expression matching a concatenation of two RegEx-Expressions
class RegExConcatenation<TokenType, AlphabetType extends Alphabet<TokenType>>
    extends RegEx<TokenType, AlphabetType> {
  RegExConcatenation(AlphabetType alphabet, this.left, this.right)
      : super(alphabet);

  RegEx<TokenType, AlphabetType> left;
  RegEx<TokenType, AlphabetType> right;

  @override
  void addToNFA(NFA nfa, RegExState startState, RegExState endState,
      FAStateFactory stateFactory) {
    RegExState intermediateState = stateFactory.createState();
    left.addToNFA(nfa, startState, intermediateState, stateFactory);
    right.addToNFA(nfa, intermediateState, endState, stateFactory);
  }
  @override
  bool equals(Object other) {
    if (!(other is RegExConcatenation)) return false;
    RegExConcatenation that = other;
    return this.left.equals(that.left) && this.right.equals(that.right);
  }

  String toString() {
    return left.toString() + right.toString();
  }

}
