import '../../NFA.dart';
import '../../alphabet/CharAlphabet.dart';
import '../../alphabet/tests/CharAlphabet_test.dart';
import '../RegEx.dart';
import '../RegExConcatenation.dart';
import '../RegExSingleToken.dart';
import 'package:test/test.dart';

CharAlphabet alphabet = new CharAlphabet();

main() {

  test("Concatenation - creation from String", () {
    RegExConcatenation regEx = RegEx.fromKeyword(charAlphabet(), "ABC");
    RegExConcatenation regEx2 = RegEx.fromKeyword(charAlphabet(), "ABC");
    expect(regEx.toString(), "ABC");
    expect(regEx.left.toString(), "AB");
    expect(regEx.right.toString(), "C");
    expect(regEx.equals(regEx2), true);
  });

  test("Concatenation - creation from String", () {
    NFA<int, int, CharAlphabet> nfa = new NFA<int, int, CharAlphabet>(alphabet);

  });




}