import 'package:test/test.dart';

import '../../alphabet/tests/CharAlphabet_test.dart';
import '../RegEx.dart';
import '../RegExSingleToken.dart';
import '_utils.dart';



main() {

  test("RegExSingleToken - creation from String / toString", () {
    RegExSingleToken regEx = RegEx.fromKeyword(charAlphabet(), "A");
    expect(regEx.toString(), "A");

  });

  test("RegExSingleToken - equals", () {
    RegExSingleToken regEx1 = RegEx.fromKeyword(charAlphabet(), "A");
    RegExSingleToken regEx2 = RegEx.fromKeyword(charAlphabet(), "A");
    expect(regEx1.equals(regEx2), true);
  });

  test("RegExSingleToken - toNFA - 1", () {

    RegExSingleToken regEx = RegEx.fromKeyword(charAlphabet(), "A");
    NFAWithStates nfa = createNFA([1, 2, 3], [0,1], [[regEx, 0, 2]]);

    expect(nfa.acceptsInput(char("A")), true);
    expect(nfa.acceptsInput(char("B")), false);
    nfa.goNext(char("A"));
    expect(nfa.currentStates, [nfa.allStates[2]].toSet());

  });

  test("RegExSingleToken - toNFA - 2", () {

    RegExSingleToken regEx = RegEx.fromKeyword(charAlphabet(), "A");
    RegExSingleToken regEx2 = RegEx.fromKeyword(charAlphabet(), "B");
    NFAWithStates nfa = createNFA([1, 2, 3, 4, 5], [0,1], [[regEx, 0, 1], [regEx2, 1, 4]]);

    nfa.goNext(char("A"));
    nfa.goNext(char("B"));
    expect(nfa.currentStates, [nfa.allStates[4]].toSet());

  });

}