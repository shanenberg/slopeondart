import 'package:test/test.dart';

import '../../alphabet/CharAlphabet.dart';
import '../../alphabet/tests/CharAlphabet_test.dart';
import '../RegExRange.dart';
import '_utils.dart';

main() {

  test("Range - toString, equals", () {
    RegExRange<int, CharAlphabet> regEx = new RegExRange<int, CharAlphabet>(charAlphabet(), char("D"), char("F"));
    RegExRange<int, CharAlphabet> regEx2 = new RegExRange<int, CharAlphabet>(charAlphabet(), char("D"), char("F"));
    expect(regEx.toString(), "[D-F]");
    expect(regEx.startToken, char("D"));
    expect(regEx.endToken, char("F"));
    expect(regEx.equals(regEx2), true);
  });


  test("Range - toNFA", () {
    RegExRange<int, CharAlphabet> regEx = new RegExRange<int, CharAlphabet>(charAlphabet(), char("D"), char("F"));

    NFAWithStates nfa = createNFA([1, 2, 3, 4, 5], [0,1], [[regEx, 0, 2]]);

    expect(nfa.acceptsInput(char("D")), true);
    expect(nfa.acceptsInput(char("E")), true);
    expect(nfa.acceptsInput(char("F")), true);
    nfa.goNext(char("D"));

    expect(nfa.currentStates, [nfa.allStates[2]].toSet());

  });



}