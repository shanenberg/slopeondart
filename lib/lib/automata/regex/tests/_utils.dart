import '../../../scanner/RegExWithKind.dart';
import '../../../scanner/state/FAStateFactory.dart';
import '../../../scanner/state/RegExState.dart';
import '../../NFA.dart';
import '../../alphabet/Alphabet.dart';
import '../../alphabet/CharAlphabet.dart';
import '../RegEx.dart';
import '../RegExAnyExceptToken.dart';

regExKeyword(String aKeyword, isSeparator) {
  CharAlphabet charAlphabet = new CharAlphabet();
  return new RegExWithKind( RegEx.fromKeyword(charAlphabet, aKeyword), isSeparator);
}

anyExceptRegEx(List exceptions, isSeparator) {
  CharAlphabet charAlphabet = new CharAlphabet();
  return new RegExWithKind( new RegExAnyExceptToken(charAlphabet, exceptions), isSeparator);
}

/*
    Function for easy creation of NFAs
    Called for example with
      NFAWithStates nfa = createNFA([1, 2, 3], [1,2], [[regEx, 1, 2]]);
    Where the NEA has three states 1-3, it startes in states 1,2 and it
    has one regular Expression that connects state 1 with 2.

 */
createNFA(List<int> nums, List<int> startStates, List regExStateArrays) {
  CharAlphabet alphabet = new CharAlphabet();
  NFA<RegExState, int, CharAlphabet> nfa = new NFAWithStates(alphabet);

  List<RegExState> states = new List<RegExState>();
  for (int i in nums) {
    states.add(new RegExState(i));
  }

  (nfa as NFAWithStates).allStates = states;

  List<RegExState> currentStates = new List<RegExState>();

  for(int aStartState in startStates) {
    currentStates.add(states[aStartState]);
  }

  nfa.currentStates = currentStates.toSet();


  for (var aRegExStateArray in regExStateArrays) {
    aRegExStateArray[0].addToNFA(nfa, states[aRegExStateArray[1]], states[aRegExStateArray[2]], new FAStateFactory());
  }

  return nfa;
}


class NFAWithStates extends NFA {
  var allStates;

  NFAWithStates(Alphabet alphabet) : super(alphabet);
}