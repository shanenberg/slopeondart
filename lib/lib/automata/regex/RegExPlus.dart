import '../../scanner/state/FAStateFactory.dart';
import '../../scanner/state/RegExState.dart';
import '../NFA.dart';
import '../alphabet/Alphabet.dart';
import '../transition/SpontaneousTransition.dart';
import 'RegEx.dart';

///Regular Expression matching 0-n iterations of another expression
class RegExPlus<TokenType, AlphabetType extends Alphabet<TokenType>>
    extends RegEx<TokenType, AlphabetType> {
  RegExPlus(AlphabetType alphabet, this.regex) : super(alphabet);

  RegEx<TokenType, AlphabetType> regex;

  @override
  void addToNFA(NFA nfa, RegExState startState, RegExState endState,
      FAStateFactory stateFactory) {
    RegExState innerStart = stateFactory.createState();
    RegExState innerEnd = stateFactory.createState();
    nfa.addTransition(
        startState, new SpontaneousTransition(alphabet, innerStart));
    nfa.addTransition(innerEnd, new SpontaneousTransition(alphabet, endState));

    regex.addToNFA(nfa, innerStart, innerEnd, stateFactory);

    nfa.addTransition(
        innerEnd, new SpontaneousTransition(alphabet, innerStart));
  }

  @override
  bool equals(Object other) {
    if (!(other is RegExPlus)) return false;
    RegExPlus that = other;
    return this.regex.equals(that.regex);
  }

  String toString() {
    return "(" + regex.toString() + ")+";
  }
}
