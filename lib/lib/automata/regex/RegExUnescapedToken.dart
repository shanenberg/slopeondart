///Regular Expression matching single token

import '../alphabet/Alphabet.dart';
import 'RegExSingleToken.dart';

class RegExUnescapedSingleToken<TokenType, AlphabetType extends Alphabet<TokenType>>
    extends RegExSingleToken<TokenType, AlphabetType> {
  RegExUnescapedSingleToken(AlphabetType alphabet, token) : super(alphabet, token);

  String toString() {
    return alphabet.tokenToUnEscapedString(token);
  }
}
