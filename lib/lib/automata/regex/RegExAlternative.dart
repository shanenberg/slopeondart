import '../../scanner/state/FAStateFactory.dart';
import '../../scanner/state/RegExState.dart';
import '../NFA.dart';
///Regular Expression matching either left or right expression

import '../alphabet/Alphabet.dart';
import '../transition/SpontaneousTransition.dart';
import 'RegEx.dart';
///Regular Expression matching either left or right expression


class RegExAlternative<TokenType, AlphabetType extends Alphabet<TokenType>>
    extends RegEx<TokenType, AlphabetType> {
  RegExAlternative(AlphabetType alphabet, this.left, this.right)
      : super(alphabet);

  RegEx<TokenType, AlphabetType> left;
  RegEx<TokenType, AlphabetType> right;

  @override
  void addToNFA(NFA nfa, RegExState startState, RegExState endState,
      FAStateFactory stateFactory) {
    RegExState alt1Start = stateFactory.createState();
    RegExState alt1End = stateFactory.createState();

    nfa.addTransition(
        startState, new SpontaneousTransition(alphabet, alt1Start));
    nfa.addTransition(alt1End, new SpontaneousTransition(alphabet, endState));
    left.addToNFA(nfa, alt1Start, alt1End, stateFactory);

    RegExState alt2Start = stateFactory.createState();
    RegExState alt2End = stateFactory.createState();

    nfa.addTransition(
        startState, new SpontaneousTransition(alphabet, alt2Start));

    nfa.addTransition(alt2End, new SpontaneousTransition(alphabet, endState));
    right.addToNFA(nfa, alt2Start, alt2End, stateFactory);
  }

  @override
  bool equals(Object other) {
    if (!(other is RegExAlternative)) return false;
    RegExAlternative that = other;
    return this.left.equals(that.left) && this.right.equals(that.right);
  }

  String toString() {
    return "(" +  left.toString() + "|" + right.toString() + ")";
  }
}
