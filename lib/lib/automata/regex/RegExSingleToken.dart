import '../../scanner/state/FAStateFactory.dart';
import '../../scanner/state/RegExState.dart';
import '../NFA.dart';
import '../alphabet/Alphabet.dart';
import '../transition/SymbolTransition.dart';
import 'RegEx.dart';

///Regular Expression matching single token
class RegExSingleToken<TokenType, AlphabetType extends Alphabet<TokenType>>
    extends RegEx<TokenType, AlphabetType> {

  RegExSingleToken(AlphabetType alphabet, this.token) : super(alphabet);

  TokenType token;

  @override
  void addToNFA(NFA nfa, RegExState startState, RegExState endState,
      FAStateFactory stateFactory) {
    nfa.addTransition(
        startState, new SymbolTransition(alphabet, token, endState));
  }

  @override
  bool equals(Object other) {
    if (!(other is RegExSingleToken<TokenType, AlphabetType>)) return false;
    RegExSingleToken<TokenType, AlphabetType> that = other;
    return alphabet.compare(that.token, this.token) == 0;
  }

  String toString() {
    return alphabet.tokenToEscapedString(token);
  }
}