import '../../scanner/IntervalList.dart';
///Transition for all symbols except the ones specified

import '../alphabet/Alphabet.dart';
import 'Transition.dart';
///Transition for all symbols except the ones specified


class AnyExceptSymbolTransition<StateType, InputType,
AlphabetType extends Alphabet<InputType>>
    extends Transition<StateType, InputType, AlphabetType> {
  //AnyExceptSymbolTransition(List<InputType> this.exceptions, StateType follower) : super(follower);
  AnyExceptSymbolTransition(
      Alphabet<InputType> alphabet, List<InputType> ex, StateType follower)
      : super(alphabet, follower) {
    this.exceptions.addAll(ex);
    this.exceptions.sort((a, b) => alphabet.compare(a, b));
  }

  List<InputType> exceptions = new List<InputType>();

  bool isSpontaneous(AlphabetType alphabet) => false;
  bool matches(AlphabetType alphabet, InputType input) =>
      !exceptions.contains(input);


  InputType getNextEmptyToken(InputType lastToken, List<InputType> sortedExceptions) {

    while (sortedExceptions.isNotEmpty && sortedExceptions.first==lastToken) {
      if (alphabet.hasTokenDirectlyAfter(sortedExceptions.first)) {
        lastToken = alphabet.getTokenDirectlyAfter(sortedExceptions.first);
        sortedExceptions.removeAt(0);
      } else {
        return null; // no emtpy token left!
      }
    }

    return lastToken;
  }

  InputType getNextClosingToken(List<InputType> sortedExceptions) {
    if (sortedExceptions.isNotEmpty) {
      return alphabet.getTokenDirectlyBefore(sortedExceptions.first);
    } else {
      return alphabet.getLast();
    }
  }

  void addToIntervalList(IntervalList<InputType, AlphabetType> list) {
    List<InputType> sortedExceptions = alphabet.sort(exceptions);

    InputType lastToken = alphabet.getFirst();
    InputType currentOpenToken = getNextEmptyToken(lastToken, sortedExceptions);
    if (currentOpenToken==null) return; // nothing left to be added!!!

    while(currentOpenToken!=null && sortedExceptions.isNotEmpty) {
      list.addStartNode(currentOpenToken, this);
      InputType nextClosingToken = getNextClosingToken(sortedExceptions);
      list.addEndNode(nextClosingToken, this);
      currentOpenToken = getNextEmptyToken(sortedExceptions.first, sortedExceptions);
    }

    if (currentOpenToken!=null && sortedExceptions.isEmpty) {
      list.addStartNode(currentOpenToken, this);
      list.addEndNode(alphabet.getLast(), this);
    }
//print("********** printList +++++++++++++");
//    print(list);
  }

}