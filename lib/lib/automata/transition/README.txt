WHY ISN'T THERE ANY REGEX TRANSITION?
=====================================
It seems clear that a RegEx-Transition is missing, i.e. a transition that get a RegEx as a label.
Such a transition would be nice to handle, because the transition itself could already contains more complex means
to evaluate whether or not an input is handled.

BUT(!!!!!)
==========
THERE ARE DAMN GOOD REASONS FOR NOT HAVING SUCH A TRANSITION!!!!
If a single transition would already contain a regular expression, evaluating a single transition would
lead to recursive evaluation of the regular expression ===>>>> BUT THE WHOLE POINT IN WRITING THE SCANNER IN SUCH A
WAY IS TO AVOID RECURSION!!!!!!!


So, if you really wanna to complex stuff on the AUTOMATA, you better write the AUTOMATA IN SUCH A WAY INSTEAD OF
DOING SUCH STUFF WITH THE TRANSITION!

The only reason why AnyExcept and RangeTransition are supported is, because they make life a bit more comfortable.
AND these things do not require recursive code.

AND
===
AnyExceptSymbol saves a lot of reference!!! Let's assume we have a ^A (which means the whole alphabet except one token).
If no AnyExceptSymbol would be available, tousands of transitions would be required, which costs additional time to
iterate them.

