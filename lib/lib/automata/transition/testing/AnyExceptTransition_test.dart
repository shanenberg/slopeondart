import 'package:test/test.dart';

import '../../alphabet/CharAlphabet.dart';
import '../../alphabet/tests/CharAlphabet_test.dart';
import '../AnyExceptSymbolTransition.dart';

CharAlphabet alphabet = new CharAlphabet();

main() {

  test("SymbolTransition<int,int,CharAlphabet>", (){


    AnyExceptSymbolTransition<int, int, CharAlphabet> t =
        new AnyExceptSymbolTransition(alphabet, [char("A")], 1);

    expect(t.matches(alphabet, char("A")), false);
    expect(t.matches(alphabet, char("B")), true);

  });

}