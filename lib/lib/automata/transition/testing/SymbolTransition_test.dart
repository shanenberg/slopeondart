import '../../../testing/TestingFunctions.dart';
import '../../alphabet/CharAlphabet.dart';
import '../../alphabet/tests/CharAlphabet_test.dart';
import '../SymbolTransition.dart';
import 'package:test/test.dart';


main() {
  test("SymbolTransition<int,int,CharAlphabet>", (){

    CharAlphabet alphabet = new CharAlphabet();

    SymbolTransition<int, int, CharAlphabet> t =
        new SymbolTransition(alphabet, char("A"), 1);

    expect(t.matches(alphabet, char("A")), true);
    expect(t.matches(alphabet, char("B")), false);

    expect(t.toString(), "--A-->1");
  });

}