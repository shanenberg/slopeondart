import '../../../testing/TestingFunctions.dart';
import '../../alphabet/CharAlphabet.dart';
import '../../alphabet/tests/CharAlphabet_test.dart';
import '../RangeTransition.dart';
import '../SymbolTransition.dart';
import 'package:test/test.dart';

CharAlphabet alphabet = new CharAlphabet();

main() {
  test("RangeTransition<int,int,CharAlphabet>", (){

    RangeTransition<int, int, CharAlphabet> t =
        new RangeTransition(alphabet, char("M"), char("P"), 1);

    expect(t.matches(alphabet, char("M")), true);
    expect(t.matches(alphabet, char("N")), true);
    expect(t.matches(alphabet, char("O")), true);
    expect(t.matches(alphabet, char("P")), true);

    expect(t.toString(), "--[M-P]-->1");
  });

}