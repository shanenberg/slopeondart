import '../../alphabet/Alphabet.dart';
import '../../alphabet/tests/CharAlphabet_test.dart';
import '../SpontaneousTransition.dart';
import '../SymbolTransition.dart';

symbolTransition(String s, int num) {
  return new SymbolTransition(charAlphabet(), char(s), num);
}

epsTransition(Alphabet alphabet, int num) {
  return new SpontaneousTransition(alphabet, num);
}
