import '../../scanner/IntervalList.dart';
///Spontaneous Transition

import '../alphabet/Alphabet.dart';
import 'Transition.dart';
///Spontaneous Transition


class SpontaneousTransition<StateType, InputType,
AlphabetType extends Alphabet<InputType>>
    extends Transition<StateType, InputType, AlphabetType> {
  SpontaneousTransition(Alphabet<InputType> alphabet, StateType follower)
      : super(alphabet, follower);

  bool isSpontaneous(AlphabetType alphabet) => true;
  bool matches(AlphabetType alphabet, InputType input) =>
      input == alphabet.emptyWord;

  ///Do nothing for spontaneous transitions
  addToIntervalList(IntervalList<InputType, AlphabetType> list) {
    /// DO NOTHING
  }
}