import '../../scanner/IntervalList.dart';
///Transition for a single symbol

import '../alphabet/Alphabet.dart';
import 'Transition.dart';
///Transition for a single symbol


class SymbolTransition<StateType, InputType,
AlphabetType extends Alphabet<InputType>>
    extends Transition<StateType, InputType, AlphabetType> {

  SymbolTransition(
      Alphabet<InputType> alphabet, InputType this.input, StateType follower)
      : super(alphabet, follower);

  InputType input;

  bool isSpontaneous(AlphabetType alphabet) => input == alphabet.emptyWord;

  bool matches(AlphabetType alphabet, InputType input) =>
      alphabet.compare(this.input, input) == 0;

  @override

  ///Adds this Symbol-Transition to the IntervalList
  ///Since only 1 Transition is possible, let's just add a start and end node
  addToIntervalList(IntervalList<InputType, AlphabetType> list) {
    //intervalList.add(this);}
    list.addStartNode(input, this);
    list.addEndNode(input, this);
  }

  String toString() {
    String ret = "--" + alphabet.tokenToUnEscapedString(input) + "-->" + follower.toString();
    return ret;
  }
}