import '../../scanner/IntervalList.dart';
///Transition for symbols between starttoken and endtoken (Definetely for char/int alphabets only)

import '../alphabet/Alphabet.dart';
import 'Transition.dart';
///Transition for symbols between starttoken and endtoken (Definetely for char/int alphabets only)


class RangeTransition<StateType, TokenType,
AlphabetType extends Alphabet<TokenType>>
    extends Transition<StateType, TokenType, AlphabetType> {


  RangeTransition(AlphabetType alphabet, TokenType this.startToken,
      TokenType this.endToken, StateType follower)
      : super(alphabet, follower);

  TokenType startToken;
  TokenType endToken;

  bool matches(AlphabetType alphabet, TokenType input) =>
      alphabet.compare(startToken, input) <= 0 &&
          alphabet.compare(endToken, input) >= 0;

  ///Since this is a transition for a RANGE of symbols, we just add that exact range to the list
  void addToIntervalList(IntervalList<TokenType, AlphabetType> list) {
    list.addStartNode(startToken, this);
    list.addEndNode(endToken, this);
  }

  String toString() {
    return "--[" + alphabet.tokenToUnEscapedString(startToken) + "-" + alphabet.tokenToUnEscapedString(endToken) + "]-->" + follower.toString();
  }
}