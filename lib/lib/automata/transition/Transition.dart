import '../../scanner/IntervalList.dart';
import '../alphabet/Alphabet.dart';
import '../alphabet/AlphabeticElement.dart';

/*
A Transition describes the transition from one state (the State is
NOT contains in the transition object!!!!) to another state. Only
the target state is stored in the transition, NOT the source state.
 */
abstract class Transition<StateType, InputType,
AlphabetType extends Alphabet<InputType>>
    extends AlphabeticElement<InputType, AlphabetType> {
  Transition(Alphabet<InputType> alphabet, this.follower) : super(alphabet);

  StateType follower;
  bool matches(AlphabetType alphabet, InputType input);

  ///wozu "alphabet" param (?)
  bool isSpontaneous(AlphabetType alphabet) => false;

  //addToList(List intervalList);
  addToIntervalList(IntervalList<InputType, AlphabetType> list);
}