import 'dart:collection';

///An Alphabet of CHARS

import 'Alphabet.dart';

class CharAlphabet extends Alphabet<int> {
  ///Basically same constructor
  CharAlphabet([emptyWord, this.escaping]) : super(emptyWord);

  HashMap<String, String> escaping = new HashMap<String, String>();

  ///0 = equals, 1 = e1>e2, -1 = e2 < e1;
  int compare(int e1, int e2) => (e1 < e2 ? -1 : (e1 == e2 ? 0 : 1));

  @override
  int getFirst() {
    return 0;
  }

  int length() {
    return 65535;
  }

  @override
  List<int> sort(Iterable<int> keys) {
    List<int> ret = new List<int>();
    ret.addAll(keys);
    ret.sort((a, b) => compare(a, b));
    return ret;
  }

  @override
  int getNextAfter(currentToken, Iterable keys) {
    // TODO: implement getNextAfter
  }
  @override
  int getTokenDirectlyBefore(int aToken) {
    return aToken - 1;
  }
  @override
  bool hasTokenDirectlyBefore(int aToken) {
    return aToken - 1 >= 0;
  }
  @override
  int getTokenDirectlyAfter(int aToken) {
    return aToken + 1;
  }

  @override
  bool hasTokenDirectlyAfter(int aToken) {
    return aToken + 1 <= length();
  }
  @override
  int getLast() {
    return length();
  }
  @override
  String tokenToEscapedString(int aToken) {
    return escapeIfNecessary(aToken);
  }

  String escapeIfNecessary(int aToken) {
    String ret = new String.fromCharCode(aToken);
    if (escaping==null)
      return ret;
    if (escaping.keys.contains(ret)) {
      return escaping[ret];
    }
    return ret;
  }

  @override
  String tokenToUnEscapedString(int aToken) {
    return new String.fromCharCode(aToken);
  }
}