///Ein Alphabet mit Elementen eines bestimmten Typs

import 'Epsilon.dart';

abstract class Alphabet<InputType> {
  ///Empty Word-Reference
  Object emptyWord = Epsilon.instance;

  ///Default Constructor mit Möglichkeit, die emptyWord-Referenz
  ///zu ändern. (?)
  Alphabet([Object empty]) {
    if (empty != null) {
      this.emptyWord = empty;
    }
  }
  // 0 = equals, 1 = e1>e2, -1 = e2 < e1;

  int compare(InputType e1, InputType e2);

  ///Methode um eine Intervall-Liste zu erstellen.
  List<InputType> createIntervalList() => new List<InputType>();

  //getNext(int zahl);

  int length();

  InputType getFirst();
  InputType getLast();

  List<InputType> sort(Iterable<InputType> keys);

  InputType getNextAfter(currentToken, Iterable keys);
  bool hasTokenDirectlyBefore(InputType aToken);
  InputType getTokenDirectlyBefore(InputType aToken);

  bool hasTokenDirectlyAfter(InputType aToken);
  InputType getTokenDirectlyAfter(InputType aToken);

  String tokenToEscapedString(InputType aToken);
  String tokenToUnEscapedString(InputType aToken);
}