///Abstract class defining an ELEMENT of an ALPHABET.

import 'Alphabet.dart';

abstract class AlphabeticElement<InputType, AlphabetType extends Alphabet<InputType>> {
  ///The alphabet this element belongs to.
  Alphabet<InputType> alphabet;
  AlphabeticElement(this.alphabet);
}
