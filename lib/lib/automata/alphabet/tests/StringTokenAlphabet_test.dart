import '../CharAlphabet.dart';
import '../../../testing/TestingFunctions.dart';
import '../StringTokenAlphabet.dart';
import 'package:test/test.dart';

main() {

  test("StringTokenAlphabet.getFirst/getLast", (){
    StringTokenAlphabet a = new StringTokenAlphabet(["X", "Y"]);
    expect(a.getFirst(), "X");
    expect(a.getLast(), "Y");
  });

  test("CharAlphabet.compare", (){
    StringTokenAlphabet a = new StringTokenAlphabet(["A", "B"]);
    expect(a.compare("A", "B"), -1);
    a = new StringTokenAlphabet(["B", "A"]);
    expect(a.compare("A", "B"), 1);
  });

}