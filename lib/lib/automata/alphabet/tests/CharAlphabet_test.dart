import '../CharAlphabet.dart';
import '../../../testing/TestingFunctions.dart';
import 'package:test/test.dart';

CharAlphabet charAlphabet() {
  return new CharAlphabet();
}

int char(String aString) {
  return aString.codeUnitAt(0);
}

main() {

  test("Simple Alphabet", (){
    expect(0, charAlphabet().getFirst());
  });

  test("CharAlphabet.getFirst", (){
    expect(0, charAlphabet().getFirst());
  });

  test("CharAlphabet.sort", (){
    expect([0,1], charAlphabet().sort([1,0]));
    expect([0,1,2,3], charAlphabet().sort([3,1,2,0]));
  });

  test("CharAlphabet.escaping.01", (){
    expect("A", charAlphabet().tokenToEscapedString(char("A")));
  });

  test("CharAlphabet.escaping.02", (){
    CharAlphabet a = charAlphabet();
    a.escaping = {"A": "?"};
    expect("?", a.tokenToEscapedString(char("A")));
  });

  test("CharAlphabet.tokenToUnEscapedString", (){
    CharAlphabet a = charAlphabet();
    a.escaping = {"A": "?"};
    expect("A", a.tokenToUnEscapedString(char("A")));
  });



}