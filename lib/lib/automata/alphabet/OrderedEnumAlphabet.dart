import 'Alphabet.dart';

class OrderedEnumAlphabet<ElementType> extends Alphabet<ElementType> {
  List<ElementType> tokens;

  OrderedEnumAlphabet(List<ElementType> this.tokens);

  int compare(ElementType e1, ElementType e2) {
    if (tokens.indexOf(e1) < tokens.indexOf(e2)) return -1;
    if (tokens.indexOf(e1) == tokens.indexOf(e2)) return 0;
    if (tokens.indexOf(e1) > tokens.indexOf(e2)) return 1;
  }

  @override
  ElementType getFirst() {
    return tokens[0];
  }

  @override
  int length() {
    return tokens.length;
  }

  @override
  List<ElementType> sort(Iterable<ElementType> keys) {
    // TODO: implement sort
  }
  @override
  ElementType getNextAfter(currentToken, Iterable keys) {
    // TODO: implement getNextAfter
  }
  @override
  ElementType getTokenDirectlyBefore(ElementType aToken) {
    // TODO: implement getTokenDirectlyBefore
  }

  @override
  bool hasTokenDirectlyBefore(ElementType aToken) {
    // TODO: implement hasTokenDirectlyBefore
  }
  @override
  ElementType getTokenDirectlyAfter(ElementType aToken) {
    // TODO: implement getTokenDirectlyAfter
  }

  @override
  bool hasTokenDirectlyAfter(ElementType aToken) {
    // TODO: implement hasTokenDirectlyAfter
  }
  @override
  ElementType getLast() {
    return tokens.last;
  }
  @override
  String tokenToEscapedString(ElementType aToken) {
    // TODO: implement tokenToString
  }
  @override
  String tokenToUnEscapedString(ElementType aToken) {
    // TODO: implement tokenToUnEscapedString
  }
}