import '../dynamicParser/DynamicParser.dart';
import '../grammarparser/GrammarParser.dart';
import '../grammarparser/SlopeGrammar.dart';
import '../grammarparser/tree/GrammarObject.dart';
import '../parser/ResultObject.dart';
import '../parser/tree/TreeNode.dart';
import 'TreeLangNode.dart';

class TreeComparator {

  TreeNode grammarTree;
  TreeNode treelangTree;

  TreeComparator(this.grammarTree, this.treelangTree);

  static doComparison(String grammarString, String word, String treeString) {

    ResultObject<SLOPEGrammarObject> result = new ResultObject<SLOPEGrammarObject>();
    ResultObject<TreeLangNode> treeResult = new ResultObject<TreeLangNode>();

    var g = new GrammarParser();
    g.parseString(grammarString, result);
    SLOPEGrammarObject grammar = result.getResult();

    SlopeGrammar dynamicGrammarParser = new DynamicParser(grammar);
    TreeNode sourceTreeNode = dynamicGrammarParser.parseString(word, null);

    if (sourceTreeNode.treeLangNodeString()!=treeString) {
      throw "UNEQUAL: " + sourceTreeNode.treeLangNodeString() + " <-> " + treeString;
    }

//    TreeLangParser treeLangParser = new TreeLangParser();
//    treeLangParser.parseString(treeString, treeResult);
//
//    treeResult.getResult().doComparisonWith(sourceTreeNode);

  }



}