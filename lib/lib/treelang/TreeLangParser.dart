import '../automata/regex/RegEx.dart';
import '../grammarparser/Separator.dart';
import '../grammarparser/SlopeGrammar.dart';
import '../grammarparser/Tokens.dart';
import '../parser/ResultObject.dart';
import '../parser/parserconstruction/Rule.dart';
import 'TreeLangNode.dart';

SEPARATOR(var element) {
  return new Separator(element);
}

class TreeLangParser extends SlopeGrammar {
  TreeLangParser() : super(null);

  RegEx identifier = Tokens.fromREGEX("([a-z]|[A-Z]|_)([a-z]|[A-Z]|_|[0-9])*");
  RegEx stringLiteral = Tokens.fromREGEX('"((^"~)|~~|~")*"');


  tokens() => [identifier, stringLiteral, "\\(", "\\)", "\\-", ",", SEPARATOR(" "), SEPARATOR("\n"), SEPARATOR("\r"), SEPARATOR("\t")];

  Rule TREENODE(ResultObject<TreeLangNode> result) => BODY(() {

    TreeLangNode retNode = new TreeLangNode();
    ResultObject<TreeLangNode> childNode = new ResultObject<TreeLangNode>();

    ResultObject<String> nodeType = new ResultObject<String>();
    ResultObject<String> nodeName = new ResultObject<String>();

    return
      AND([
        ELEMENT_NAME(nodeType),
        DO(()=>retNode.typeName = nodeType.getResult()),
        OPTIONAL(
          AND([
            TOKEN("("),
            OPTIONAL(
               AND([
                 TOKEN("-"),
                 ELEMENT_NAME(nodeName),
                 DO(()=>retNode.nodeName = nodeName.getResult()),
                 TOKEN("-")
               ])
            ),
            NFOLD(
              AND([
                TREENODE(childNode),
                DO(()=>retNode.children.add(childNode.getResult()))
              ])
            ),
            TOKEN(")")
          ])
        ),
        DO(()=>result.setResult(retNode))
      ]);
  });

  Rule ELEMENT_NAME(ResultObject<String> result) => BODY(() {
    return
      OR([
        REGEXObj(identifier, result),
        REGEXObj(stringLiteral, result)
      ]);
  });
  @override
  Rule Start(ResultObject result) {
    return TREENODE(result);
  }
}