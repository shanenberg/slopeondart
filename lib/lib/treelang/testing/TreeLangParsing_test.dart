import 'package:test/test.dart';

import '../../parser/ResultObject.dart';
import '../TreeLangNode.dart';
import '../TreeLangParser.dart';

s (String s) {
  ResultObject<TreeLangNode> result = new ResultObject<TreeLangNode>();
  var g = new TreeLangParser();
  g.parseString(s, result);
  return result.getResult();
}

main() {

  test("TreeLangParser test - checks lang for trees tests", () {
    s('AND'); s('AND()'); s('AND(AND() OR() STUFF())'); s('A(-test-)');
  });
  test("TreeLangParser test - treenodes", () {
    TreeLangNode n = s('AND');
    expect(n.typeName, "AND");
    expect(n.children.length, 0);

    n = s('AND()');
    expect(n.typeName, "AND");
    expect(n.children.length, 0);

    n = s('AND(AND() OR() STUFF())');
    expect(n.typeName, "AND");
    expect(n.children.length, 3);
    expect(n.children[0].typeName, "AND");
    expect(n.children[1].typeName, "OR");
    expect(n.children[2].typeName, "STUFF");

    n = s('A(-test-)');
    expect(n.typeName, "A");
    expect(n.nodeName, "test");

  });
}