import '../dynamicParser/DynamicParser.dart';
import '../grammarparser/GrammarParser.dart';
import '../grammarparser/SlopeGrammar.dart';
import '../grammarparser/tree/GrammarObject.dart';
import '../parser/ResultObject.dart';
import '../parser/tree/MultiChildrenNode.dart';
import '../parser/tree/SingleChildTreeNode.dart';
import '../parser/tree/TreeNode.dart';
import '../parser/parserconstruction/Rule.dart';

List<TreeNode> childrenOf(TreeNode treeNode) {
  if(treeNode is SingleChildTreeNode)
    return [treeNode.child];
  else if (treeNode is MultiChildrenNode)
    return treeNode.treeNodes;
  else
    return [];
}


TreeNode createTemplateFromGrammar(SLOPEGrammarObject grammarObject) {
  Rule pc = grammarObject.createParser()
      .Start(new ResultObject());

  return pc.createTemplate();
}


SLOPEGrammarObject parseGrammar(String grammarString) {
  ResultObject<SLOPEGrammarObject> result = new ResultObject<SLOPEGrammarObject>();
  var g = new GrammarParser();
  g.parseString(grammarString, result);
  SLOPEGrammarObject grammar = result.getResult();
  return grammar;


}



TreeNode parseStringFromGrammar(String word, String grammarString) {
  SLOPEGrammarObject grammar = parseGrammar(grammarString);

  SlopeGrammar dynamicGrammarParser = new DynamicParser(grammar);
  TreeNode sourceTreeNode = dynamicGrammarParser.parseString(word, null);
  return sourceTreeNode;
}