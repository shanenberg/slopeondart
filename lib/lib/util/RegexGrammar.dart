import '../grammarparser/tree/GrammarObject.dart';
import 'GrammarUtils.dart';
import '../parser/tree/TreeNode.dart';
import '../parser/features/and/AND_TreeNode.dart';
import '../parser/features/optional/OPTIONAL_TreeNode.dart';
import '../parser/features/scannerelement/ScannerElement_TreeNode.dart';
import '../parser/features/nfold/NFOLD_TreeNode.dart';
import '../../slot/runtime/objects/SLOTTreeNode.dart';

SLOPEGrammarObject regexGrammar = parseGrammar('''
   RegEx

  Rules: alternative
    alternative => concatenation ("\\|" concatenation)*.
    concatenation => simpleExpression+.
    simpleExpression =>
      (literalChar | bracketExpression | interval | anyExcept) (star | plus | opt)?.
    
    star => "\\*".
    plus => "\\+".
    opt => "\\?".
    
    literalChar => char.
    
    bracketExpression => "\\(" alternative "\\)".
    interval => "\\[" CHAR "\\-" CHAR "\\]".
    anyExcept => ANY char+.
  
    char => CHAR.
  
  Scanner:
    ANY => "\\^".
    CHAR => "(^\\\\\\|\\(\\)\\*\\+\\?\\^|\\\\(\\\\|\\||\\(|\\)|\\*|\\+|\\?|\\^))". /* everything except escaped chars...that require escaping*/


''');


typedef TreeFunctionAlternative(TreeNode, Map<String, Object> env, MiniTreeProgram);


main() {
/*
  var treeNode = regexGrammar.parseString("([a-b][c-d])?FFF");



  _miniTreeProgram.startInit(treeNode, init);
  _generatedGrammarString += ".";


  print(_generatedGrammarString);
  SLOPEGrammarObject obj = parseGrammar(_generatedGrammarString);


  /* SLOPEGrammarObject obj = parseGrammar('''
    Dingdong
    Rules: start
    start => "(^AB)|ABC".
  ''');*/

  TreeNode node = obj.parseString("adFFF");
  print("hi" + (node.nodeName==null?"null":node.nodeName));




  var grammar = parseGrammar('''
    bla
    Rules: lol
    lol => "hi".
      
  ''');


  SLOTTreeNode dings = grammar.parseString("hi").toSLOTTreeNode();
  print(dings.debug_SLOTTreeString());*/




  var grammar = parseGrammar('''
             Bla
             Rules: start
             start => "A"+ "[B-Z]"+.
  ''');

  //var ding = grammar.parseString("AB");

  var dingsbums = createTemplateFromGrammar(grammar);


  SLOTTreeNode slotDing = dingsbums.toSLOTTreeNode();


  print("hi");


}


/* [A-Z]+
 *  */
TreeNode createRegexParseTree(ScannerElement_TreeNode scannerElement) {
  _generatedGrammarString = "";
  var treeNode = regexGrammar.parseString(scannerElement.keyword);

  _miniTreeProgram.startInit(treeNode, init);
  _generatedGrammarString += ".";


  print(_generatedGrammarString);
  SLOPEGrammarObject obj = parseGrammar(_generatedGrammarString);




  if(scannerElement.scannedWord == "#") {
    return createTemplateFromGrammar(obj);
  } else {
    return obj.parseString(scannerElement.scannedWord);
  }



}




MiniTreeProgram _miniTreeProgram = new MiniTreeProgram({

  "alternative": alternative,
  "concatenation": concatenation,
  "simpleExpression": simpleExpression,
  "star" : star,
  "plus" : plus,
  "opt" : opt,
  "literalChar" : literalChar,
  "bracketExpression" : bracketExpression,
  "interval" : interval,
  "anyExcept" : anyExcept,
  "char" : char,
  "ALL" : ALL


});

String _generatedGrammarString;

init(TreeNode treeNode, Map<String, Object> env, MiniTreeProgram program) {
  _generatedGrammarString = "ReggaeGrammar\n\nRules: start\n\nstart => ";
}
/*
alternative(TreeNode treeNode, Map<String, Object> env, MiniTreeProgram program) {
  program.runOnTreeNode(treeNode, env)
  _continueForChildren(treeNode, env, program);
}*/

alternative(AND_TreeNode treeNode, Map<String, Object> env, MiniTreeProgram program) {
  program.runOnTreeNode(treeNode.treeNodes[0], env);

  for(TreeNode child in (treeNode.treeNodes[1] as NFOLD_TreeNode).treeNodes) {

    _generatedGrammarString += "|";
    program.runOnTreeNode(child, env);

  }
}

concatenation(AND_TreeNode treeNode, Map<String, Object> env, MiniTreeProgram program) {
  program.runOnTreeNode(treeNode.treeNodes[0], env);
}

simpleExpression(AND_TreeNode treeNode, Map<String, Object> env, MiniTreeProgram program) {
  program.runOnTreeNode(treeNode.treeNodes[0], env);
  program.runOnTreeNode(treeNode.treeNodes[1], env);
}


literalChar(AND_TreeNode treeNode, Map<String, Object> env, MiniTreeProgram program) {
  _generatedGrammarString += "\"";
  program.runOnTreeNode(treeNode.treeNodes[0], env);
  _generatedGrammarString += "\"";
}

bracketExpression(AND_TreeNode treeNode, Map<String, Object> env, MiniTreeProgram program) {
  _generatedGrammarString += "(";
  program.runOnTreeNode(treeNode.treeNodes[1], env);
  _generatedGrammarString += ")";
}

interval(AND_TreeNode treeNode, Map<String, Object> env, MiniTreeProgram program) {
  _generatedGrammarString += "\"[";
  _generatedGrammarString += (treeNode.treeNodes[1] as ScannerElement_TreeNode).scannedWord;
  _generatedGrammarString += "-";
  _generatedGrammarString += (treeNode.treeNodes[3] as ScannerElement_TreeNode).scannedWord;
  _generatedGrammarString += "]\"";
}

anyExcept(AND_TreeNode treeNode, Map<String, Object> env, MiniTreeProgram program) {
  _generatedGrammarString += "\"^";
  program.runOnTreeNode(treeNode.treeNodes[1], env);
  _generatedGrammarString += "\"";

}

star(AND_TreeNode treeNode, Map<String, Object> env, MiniTreeProgram program) {
  _generatedGrammarString+="*";
}

plus(AND_TreeNode treeNode, Map<String, Object> env, MiniTreeProgram program) {
  _generatedGrammarString+="+";
}

opt(AND_TreeNode treeNode, Map<String, Object> env, MiniTreeProgram program) {
  _generatedGrammarString+="?";
}

char(AND_TreeNode treeNode, Map<String, Object> env, MiniTreeProgram program) {
  _generatedGrammarString += (treeNode.treeNodes[0] as ScannerElement_TreeNode).scannedWord;
}

ALL(TreeNode treeNode, Map<String, Object> env, MiniTreeProgram program) {
  _continueForChildren(treeNode, env, program);
}

_continueFor(TreeNode treeNode, Map<String, Object> env, MiniTreeProgram program) {
  program.runOnTreeNode(treeNode, env);
}

_continueForChildren(TreeNode treeNode, Map<String, Object> env, MiniTreeProgram program) {
  for(TreeNode child in childrenOf(treeNode)) {
    program.runOnTreeNode(child, env);
  }
}

class MiniTreeProgram {

  Map<String, TreeFunctionAlternative> map;

  MiniTreeProgram(this.map);

  start(TreeNode treeNode) {
    runOnTreeNode(treeNode, {});
  }
  startInit(TreeNode treeNode, TreeFunctionAlternative init) {
    Map<String, Object> env = {};
    init(treeNode, env, this);
    runOnTreeNode(treeNode, env);
  }

  runOnTreeNode(TreeNode treeNode, Map<String, Object> env) {
    TreeFunctionAlternative alternative;

    if(treeNode != null) {
      if((alternative = map[treeNode.nodeName]) != null) {
        alternative((treeNode as AND_TreeNode).treeNodes[0], env, this);
      }
      else if ((alternative = map[      "ALL"      ]) != null) {
        alternative(treeNode, env, this);
      }

    }

  }


}
