import 'SlopeGrammarUser.dart';
import '../../../lib/grammarparser/GrammarRuleExpression.dart';
import '../../../lib/grammarparser/tree/GrammarObject.dart';
import '../../../lib/grammarparser/tree/GrammarRuleObject.dart';
import '../../../lib/grammarparser/tree/rule/GrammarRuleExpressionAND.dart';
import '../../../lib/grammarparser/tree/rule/GrammarRuleExpressionBRACKET.dart';
import '../../../lib/grammarparser/tree/rule/GrammarRuleExpressionOPT.dart';
import '../../../lib/grammarparser/tree/rule/GrammarRuleExpressionOR.dart';
import '../../../lib/grammarparser/tree/rule/GrammarRuleExpressionPLUS.dart';
import '../../../lib/grammarparser/tree/rule/GrammarRuleExpressionRegExString.dart';
import '../../../lib/grammarparser/tree/rule/GrammarRuleExpressionRuleRefOrScannerRef.dart';
import '../../../lib/grammarparser/tree/rule/GrammarRuleExpressionStar.dart';
import '../../../lib/grammarparser/tree/rule/GrammarRuleUnary.dart';
import '../../../lib/grammarparser/ScannerRuleObject.dart';
import '../../../lib/util/GrammarUtils.dart';
import '../../../lib/regex/RegExStringParser.dart';
import '../../../lib/automata/regex/RegEx.dart';
import '../../../lib/automata/regex/RegExAlternative.dart';
import '../../../lib/automata/regex/RegExAnyExceptToken.dart';
import '../../../lib/automata/regex/RegExConcatenation.dart';
import '../../../lib/automata/regex/RegExKleene.dart';
import '../../../lib/automata/regex/RegExOptional.dart';
import '../../../lib/automata/regex/RegExPlus.dart';
import '../../../lib/automata/regex/RegExRange.dart';
import '../../../lib/automata/regex/RegExSingleToken.dart';
import '../../../lib/automata/regex/RegExUnescapedToken.dart';




main() {
    SLOPEGrammarObject obj = parseGrammar('''
      HI
      
      Rules: abc
      
      abc => "DINGDONG"*.
      
      
      start => "D" "i" "n" "g" ...
      
      start {
               AND {
                      "D"
                      "i"
                      
      }
      
      
      
      dings => "Dingsbums+".
      literal => topps BLA.
      
      
      Scanner:
      
        BLA => "BLE+[h-z]".
        whitespace => SEPARATOR " |\\n|\\r".
        bot => "bottomDings" BOTTOM.
        topps => "topDings" TOP.
        lol => SEPARATOR "\\t" BOTTOM.        
      
    ''');


    GrammarStringWriter writer = new GrammarStringWriter();
    writer.use(obj);
    print(writer.grammarString);
}

class GrammarStringRegexWriter extends GrammarStringWriter {

  int regexCounter = 1;
  Map<String, String> regexMap = {};

  @override
  writeGrammarString()
  {
    writeGrammarHeader();
    writeGrammarRules();

  }

  @override
  grammarRuleExpressionRegExString(GrammarRuleExpressionRegExString expression)
  {
    print("hi");

    String savedRegexName = regexMap[expression.regexString];

    if(savedRegexName != null) {
      grammarString += savedRegexName;
    } else {
      String regexName = "REGEX_${regexCounter++}";


    }


    RegEx regex = RegExStringParser.fromString(expression.regexString);


  }


}

class RegExToLiteralRuleStringConverter {

  RegEx regex;
  RegExToLiteralRuleStringConverter(this.regex);

  String _ruleString = "";

  String convertToRuleString() {
    _runOnRegEx(regex);
    return _ruleString;
  }

  _runOnRegEx(RegEx regex) {
    if(regex is RegExUnescapedSingleToken)
      _runOnUnescapedSingleToken(regex);
    else if(regex is RegExSingleToken)
      _runOnSingleToken(regex);
    else if(regex is RegExRange)
      _runOnRange(regex);
    else if(regex is RegExPlus)
      _runOnPlus(regex);
    else if(regex is RegExOptional)
      _runOnOptional(regex);
    else if(regex is RegExKleene)
      _runOnKleene(regex);
    else if(regex is RegExConcatenation)
      _runOnConcatenation(regex);
    else if(regex is RegExAnyExceptToken)
      _runOnAnyExceptToken(regex);
    else if(regex is RegExAlternative)
      _runOnAlternative(regex);
  }

  _runOnUnescapedSingleToken(RegExUnescapedSingleToken regex) {

  }

  _runOnSingleToken(RegExSingleToken regex) {

  }

  _runOnRange(RegExRange regex) {

  }

  _runOnPlus(RegExPlus regex) {

  }

  _runOnOptional(RegExOptional regex) {

  }

  _runOnKleene(RegExKleene regex) {

  }

  _runOnConcatenation(RegExConcatenation regex) {

  }

  _runOnAnyExceptToken(RegExAnyExceptToken regex) {

  }

  _runOnAlternative(RegExAlternative regex) {
    _runOnRegEx(regex.left);
    _ruleString += " | ";

  }

}




class GrammarStringWriter extends SlopeGrammarUser {
  String grammarString = "";

  use(SLOPEGrammarObject obj) {
    grammarObject = obj;
    writeGrammarString();
  }

  writeGrammarString() {
    writeGrammarHeader();


    writeGrammarRules();
    writeScannerRules();
  }

  writeGrammarHeader() {
    grammarString += grammarObject.grammarName+"\n";

  }

  writeGrammarRules() {
    writeGrammarRulesHeader();

    for(GrammarRuleObject rule in grammarObject.rules) {
      writeRuleString(rule);
    }
  }

  writeGrammarRulesHeader() {
    grammarString += "Rules: "+grammarObject.startRuleName+ "\n";
  }


  writeRuleString(GrammarRuleObject rule) {
    grammarString += "${rule.name} => ";
    runOnExpression(rule.body);
    grammarString += ".\n";
  }



  writeScannerRules() {
    writeScannerRulesHeader();
    writeTopScannerRules();
    writeBottomScannerRules();
  }

  writeScannerRulesHeader() {
    grammarString += "Scanner: \n";

  }

  writeTopScannerRules() {
    for(ScannerRuleObject rule in grammarObject.topScanners) {
      writeScannerRuleString(rule);
    }
  }

  writeBottomScannerRules() {
    for(ScannerRuleObject rule in grammarObject.bottomScanners) {
      writeScannerRuleString(rule);
    }
  }

  writeScannerRuleString(ScannerRuleObject rule) {
    grammarString += "${rule.name} => ${rule.isSeparator ? "SEPARATOR" : ""} \"${rule.scannerExpression}\" ${rule.isTop ? "TOP" : "BOTTOM"}.\n";
  }



  @override
  grammarRuleExpressionNAry(GrammarRuleNAry expression) {
  }

  @override
  grammarRuleExpressionSTAR(GrammarRuleExpressionSTAR expression) {
    runOnExpression(expression.element);
    grammarString+= "*";
  }

  @override
  grammarRuleExpressionRegExString(GrammarRuleExpressionRegExString expression) {
    grammarString+="\"${expression.regexString}\"";
  }

  @override
  grammarRuleExpressionRuleRefOrScannerRef(GrammarRuleExpressionRuleRefOrScannerRef expression) {
    grammarString += expression.token;
  }

  @override
  grammarRuleUnary(GrammarRuleUnary expression) {
    // TODO: implement _grammarRuleUnary
  }

  @override
  grammarRuleExpressionPLUS(GrammarRuleExpressionPLUS expression) {
    runOnExpression(expression.element);
    grammarString+= "+";
  }

  @override
  grammarRuleExpressionBRACKET(GrammarRuleExpressionBRACKET expression) {
    grammarString += "(";
    runOnExpression(expression.element);
    grammarString += ")";
  }

  @override
  grammarRuleExpressionOPT(GrammarRuleExpressionOPT expression) {
    runOnExpression(expression.element);
    grammarString+= "?";
  }

  @override
  grammarRuleExpressionOR(GrammarRuleExpressionOR expression) {
    runOnExpression(expression.elements[0]);

    for(int i = 1; i < expression.elements.length ; i++) {
      grammarString+= " | ";
      runOnExpression(expression.elements[i]);
    }
  }

  @override
  grammarRuleExpressionAND(GrammarRuleExpressionAND expression) {
    runOnExpression(expression.elements[0]);

    for(int i = 1; i < expression.elements.length ; i++) {
      grammarString+= " ";
      runOnExpression(expression.elements[i]);
    }
  }
}