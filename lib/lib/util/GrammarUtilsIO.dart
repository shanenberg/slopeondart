import 'dart:io';
import '../grammarparser/tree/GrammarObject.dart';
import '../parser/tree/TreeNode.dart';
import '../../testing/slope/SlopeFileRunner.dart';
import 'GrammarUtils.dart';


SLOPEGrammarObject parseGrammarFromPath(String grammarPath) {
  return parseGrammar(new File(grammarPath).readAsStringSync());
}

TreeNode parseStringFromGrammarPath(String wordPath, String grammarPath) {
  String word = new File(wordPath).readAsStringSync();
  String grammar = new File(grammarPath).readAsStringSync();

  return parseStringFromGrammar(word, grammar);
}

testGrammarFromPath(String path) {
  var grammarString = new File(path).readAsStringSync();
  testGrammar(grammarString);
}