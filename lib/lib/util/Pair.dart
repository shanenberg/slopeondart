class Pair<T1, T2>  {
  T1 left;
  T2 right;

  Pair(this.left, this.right);

}