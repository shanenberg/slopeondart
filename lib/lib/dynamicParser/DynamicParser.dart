import '../automata/alphabet/CharAlphabet.dart';
import '../grammarparser/SlopeGrammar.dart';
import '../grammarparser/tree/GrammarObject.dart';
import '../parser/ResultObject.dart';
import '../parser/parserconstruction/ParserConstructor.dart';
import '../parser/parserconstruction/Rule.dart';
import '../scanner/CharScanner.dart';
import '../scanner/RegExWithKind.dart';
import '../stream/StringTokenStream.dart';
import '../../lib/grammarparser/tree/GrammarRuleObject.dart';

class DynamicParser extends SlopeGrammar {

  Rule startExpression;
  SLOPEGrammarObject grammarObject;

  DynamicParser(SLOPEGrammarObject this.grammarObject) : super(null);

  @override
  Rule Start(ResultObject result) {
//print("DYNAMIC START");
    GrammarRuleObject rule = grammarObject.getRule(grammarObject.startRuleName);
    ParserConstructor r =
      grammarObject.getRule(grammarObject.startRuleName).body.createParserConstructor(grammarObject);
//    r.parserName = grammarObject.startRuleName;

//    Rule rule = new Rule((){
//      Rule ret = new Rule(() {return r;});
//      ret.parserName = r.parserName;
//
//    return r;});
//    rule.parserName = grammarObject.startRuleName;

//    print("START: " + r.parserName.toString());
//    AppliedRule_ParserConstructor ret = new AppliedRule_ParserConstructor(r);
    r.parserName = grammarObject.startRuleName;
    return r;
  }

  @override
  List<Object> tokens() {
    return this.grammarObject.getScannerElements();
  }

  scanString(String toParse) {
    StringTokenStream stream = new StringTokenStream(toParse);

    List<RegExWithKind<int, CharAlphabet>> tokenRegExs = tokensToRegExWithKindList();

//    print("String");
//    print(toParse);
//    print("tokens");
//    for(int i=0; i<tokenRegExs.length; i++) {
//      print(tokenRegExs[i].regex.toString());
//    }

    CharScanner scanner = new CharScanner(stream, tokenRegExs);
    var ret =  scanner.doScanning();

//    print("result");
//    ret.printStream();
//
//  print("DONE");
    return ret;
  }
}