import 'package:test/test.dart';

import '../../grammarparser/GrammarParser.dart';
import '../../grammarparser/SlopeGrammar.dart';
import '../../grammarparser/tree/GrammarObject.dart';
import '../../parser/ResultObject.dart';
import '../../parser/features/and/AND_TreeNode.dart';
import '../../parser/tree/TreeNode.dart';
import '../DynamicParser.dart';

s (String grammarString, String word) {

  ResultObject<SLOPEGrammarObject> result = new ResultObject<SLOPEGrammarObject>();
  ResultObject<SLOPEGrammarObject> realResult = new ResultObject<SLOPEGrammarObject>();
  var g = new GrammarParser();
  g.parseString(grammarString, result);
  SLOPEGrammarObject grammar = result.getResult();

  SlopeGrammar dynamicParser = new DynamicParser(grammar);

  TreeNode n = dynamicParser.parseString(word, null);

  return n;
}

main() {

  test("Grammar test - SingleNode", () {
    TreeNode n = s('AGr Rules: start start=>"A".', 'A');
// AND(name: "XYZ",
    expect(n is AND_TreeNode, true);
    expect((n as AND_TreeNode).nodeName, "start");
//    s('AGr Rules: start start=>"A". Tests: aTest "A".');
//    s('AGr Rules: start start=>"A". Tests: aTest "X" FAIL_PARSING.');
//    s('AGr Rules: start start=>"A". Tests: aTest "X" FAIL_PARSING FAIL_SCANNING.');
//    s('AGr Rules: start start=>"A". Scanner: anA => "A".');
//    s('AGr Rules: start start=>"A". Scanner: anA => "A". nl => SEPARATOR "x". ');
//    s('AAGr Rules: start start=>A. A=>"A".');
//    s('AABMyGrammar Rules: start start=>A|B. A=>"A". B=>"B".');
  });
}